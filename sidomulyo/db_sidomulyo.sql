-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2021 at 04:59 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_aparatur`
--

CREATE TABLE `tb_aparatur` (
  `id_apt` varchar(50) NOT NULL,
  `nama_apt` varchar(100) NOT NULL,
  `jkel_apt` varchar(10) NOT NULL,
  `telp_apt` char(12) NOT NULL,
  `alamat_apt` varchar(255) NOT NULL,
  `jabatan_apt` int(11) NOT NULL,
  `ket_apt` varchar(50) NOT NULL,
  `tugas_pokok` mediumtext NOT NULL,
  `fungsi` mediumtext NOT NULL,
  `foto_apt` varchar(100) DEFAULT NULL,
  `id_atasan` varchar(50) NOT NULL,
  `level` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_aparatur`
--

INSERT INTO `tb_aparatur` (`id_apt`, `nama_apt`, `jkel_apt`, `telp_apt`, `alamat_apt`, `jabatan_apt`, `ket_apt`, `tugas_pokok`, `fungsi`, `foto_apt`, `id_atasan`, `level`) VALUES
('60b0107459f8b', 'Andi Muhammad Yusuf', 'Laki-laki', '081234567890', 'Jl. Raung, Dusun Klanceng RT 003 / RW 003, Kelurahan Ajung, Kecamatan Ajung', 2, 'Aparatur BPD', '<ol>\r\n	<li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>\r\n</ol>\r\n', '60b0107459f8b.jpg', '-', 'Atasan'),
('60b2b9038429e', 'Syahruddin, SE', 'Laki-laki', '081234567890', 'Jl. Raung, Dusun Klanceng RT 003 / RW 003, Kelurahan Ajung, Kecamatan Ajung', 1, 'Aparatur Desa', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '60b2b9038429e.jpg', '-', 'Atasan'),
('60b331b3ed561', 'Aliman, S.M.', 'Laki-laki', '-', '-', 17, 'Aparatur Desa', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '60b331b3ed561.jpg', '60b2b9038429e', 'Atasan'),
('60b332c5c62ae', 'Muhammad Riswan', 'Laki-laki', '-', '-', 3, 'Aparatur Desa', '<ol>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n</ol>\r\n', '60b332c5c62ae.jpg', '60b331b3ed561', 'Atasan'),
('60b333faa919e', 'Malik', 'Laki-laki', '-', '-', 4, 'Aparatur Desa', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '60b333faa919e.jpg', '60b331b3ed561', '-'),
('60b334524ef35', 'Saharuddin', 'Laki-laki', '-', '-', 5, 'Aparatur Desa', '<ol>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n</ol>\r\n', '60b334524ef35.jpg', '60b331b3ed561', 'Atasan'),
('60b334f2db4db', 'A. Rosmini Tidar', 'Laki-laki', '-', '-', 6, 'Aparatur Desa', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '60b334f2db4db.jpg', '60b332c5c62ae', '-'),
('60b33681d3a85', 'Muskamil MG', 'Laki-laki', '-', '-', 7, 'Aparatur Desa', '<ol>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n</ol>\r\n', '60b33681d3a85.jpg', '60b334524ef35', '-'),
('60b3a1b8a03a4', 'Harnida Diniati T, S.Pd.I', 'Laki-laki', '-', '-', 8, 'Aparatur Desa', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li>\r\n	<li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.</li>\r\n	<li>The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '60b3a1b8a03a4.jpg', '60b2b9038429e', 'Atasan'),
('60b3a21eed5f2', 'Salmiati, SH', 'Laki-laki', '-', '-', 9, 'Aparatur Desa', '<ol>\r\n	<li>The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.</li>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>\r\n</ol>\r\n', '60b3a21eed5f2.jpg', '60b2b9038429e', '-'),
('60b3a25862794', 'Israfil, S.Pd.I', 'Laki-laki', '-', '-', 10, 'Aparatur Desa', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '60b3a25862794.jpg', '60b2b9038429e', '-'),
('60b3a613e17e0', 'Ilham, S.Sos', 'Laki-laki', '-', '-', 12, 'Aparatur Desa', '<ol>\r\n	<li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>\r\n</ol>\r\n', '60b3a613e17e0.jpg', '60b2b9038429e', '-'),
('60b3ac91875b6', 'Arifin', 'Laki-laki', '-', '-', 13, 'Aparatur BPD', '<ol>\r\n	<li><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '60b3ac91875b6.jpg', '60b0107459f8b', '-'),
('60b3acf824872', 'Baba', 'Laki-laki', '-', '-', 14, 'Aparatur BPD', '<ol>\r\n	<li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n</ol>\r\n', '60b3acf824872.jpg', '60b0107459f8b', '-'),
('60b3ad5701515', 'H. Sahibuddin', 'Laki-laki', '-', '-', 15, 'Aparatur BPD', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '60b3ad5701515.jpg', '60b0107459f8b', '-'),
('60b3ae178f87a', 'Kurnia', 'Laki-laki', '-', '-', 16, 'Aparatur BPD', '<ol>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.</li>\r\n	<li>The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n</ol>\r\n', '60b3ae178f87a.jpg', '60b0107459f8b', '-'),
('60b3b2aac438d', 'Samsir', 'Laki-laki', '-', '-', 11, 'Aparatur Desa', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</li>\r\n	<li>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n</ol>\r\n', '<ol>\r\n	<li>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable.&nbsp;</li>\r\n</ol>\r\n', '60b3b2aac438d.jpg', '60b3a1b8a03a4', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `id_jbt` int(11) NOT NULL,
  `nama_jbt` varchar(100) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id_jbt`, `nama_jbt`, `keterangan`) VALUES
(1, 'Kepala Desa', 'Aparatur Desa'),
(2, 'Ketua', 'Aparatur BPD'),
(3, 'Kepala Urusan Keuangan', 'Aparatur Desa'),
(4, 'Kepala Urusan Perencanaan', 'Aparatur Desa'),
(5, 'Kepala Urusan Tata Usaha dan Umum', 'Aparatur Desa'),
(6, 'Staf Bendahara', 'Aparatur Desa'),
(7, 'Staf Kebersihan', 'Aparatur Desa'),
(8, 'Kepala Seksi Pemerintahan', 'Aparatur Desa'),
(9, 'Kepala Seksi Kesejahteraan', 'Aparatur Desa'),
(10, 'Kepala Seksi Pelayanan', 'Aparatur Desa'),
(11, 'Staff Trantib', 'Aparatur Desa'),
(12, 'Kepala Dusun', 'Aparatur Desa'),
(13, 'Wakil Ketua', 'Aparatur BPD'),
(14, 'Sekretaris', 'Aparatur BPD'),
(15, 'Bidang Pembangunan dan Pemberdayaan Masyarakat', 'Aparatur BPD'),
(16, 'Bidang Pemerintahan dan Pembinaan Masyarakat', 'Aparatur BPD'),
(17, 'Sekretaris', 'Aparatur Desa'),
(18, 'Pembina', 'PKK'),
(19, 'Ketua', 'PKK'),
(20, 'Sekretaris', 'PKK'),
(21, 'Bendahara', 'PKK'),
(22, 'Pos KB', 'PKK'),
(23, 'Dasawisma', 'PKK'),
(24, 'Ketua', 'LPMD'),
(25, 'Sekretaris', 'LPMD'),
(26, 'Bendahara', 'LPMD'),
(27, 'Seksi Pembangunan', 'LPMD'),
(28, 'Seksi Pendidikan dan Kerohanian', 'LPMD'),
(29, 'Seksi Kesehatan', 'LPMD'),
(30, 'Seksi Kepemudaan', 'LPMD'),
(31, 'Seksi Pemberdayaan Perempuan', 'LPMD'),
(32, 'Pembina Umum', 'Karang Taruna'),
(33, 'Pembina Fungsional', 'Karang Taruna'),
(34, 'Pembina Teknis', 'Karang Taruna'),
(35, 'Penasehat', 'Karang Taruna'),
(36, 'Ketua', 'Karang Taruna'),
(37, 'Sekretaris', 'Karang Taruna'),
(38, 'Bendahara', 'Karang Taruna'),
(39, 'Koordinator', 'Karang Taruna'),
(40, 'Anggota', 'Karang Taruna'),
(41, 'Ketua', 'Posyandu'),
(42, 'Wakil Ketua', 'Posyandu'),
(43, 'Sekretaris', 'Posyandu'),
(44, 'Bendahara', 'Posyandu'),
(52, 'Ketua', 'Karang Werda'),
(53, 'Wakil Ketua', 'Karang Werda'),
(54, 'Sekretaris', 'Karang Werda'),
(55, 'Bendahara', 'Karang Werda'),
(56, 'Anggota', 'Karang Werda'),
(57, 'Ketua', 'Rumah Desa Sehat'),
(58, 'Wakil Ketua', 'Rumah Desa Sehat'),
(59, 'Sekretaris', 'Rumah Desa Sehat'),
(60, 'Bendahara', 'Rumah Desa Sehat'),
(61, 'Anggota', 'Rumah Desa Sehat'),
(62, 'Ketua', 'Muslimat'),
(63, 'Wakil Ketua', 'Muslimat'),
(64, 'Sekretaris', 'Muslimat'),
(65, 'Bendahara', 'Muslimat'),
(66, 'Anggota', 'Muslimat'),
(67, 'Ketua', 'Aisyiyah'),
(68, 'Wakil Ketua', 'Aisyiyah'),
(69, 'Sekretaris', 'Aisyiyah'),
(70, 'Bendahara', 'Aisyiyah'),
(71, 'Anggota', 'Aisyiyah'),
(72, 'Ketua', 'SSB'),
(73, 'Wakil Ketua', 'SSB'),
(74, 'Sekretaris', 'SSB'),
(75, 'Bendahara', 'SSB'),
(76, 'Anggota', 'SSB'),
(77, 'Ketua', 'LSM'),
(78, 'Wakil Ketua', 'LSM'),
(79, 'Sekretaris', 'LSM'),
(80, 'Bendahara', 'LSM'),
(81, 'Anggota', 'LSM'),
(82, 'Ketua', 'KPMD'),
(83, 'Wakil Ketua', 'KPMD'),
(84, 'Sekretaris', 'KPMD'),
(85, 'Bendahara', 'KPMD'),
(86, 'Anggota', 'KPMD'),
(87, 'Ketua', 'KPM'),
(88, 'Wakil Ketua', 'KPM'),
(89, 'Sekretaris', 'KPM'),
(90, 'Bendahara', 'KPM'),
(91, 'Anggota', 'KPM'),
(93, 'Ketua RT', 'RT'),
(94, 'Ketua RW', 'RW'),
(95, 'Anggota', 'Posyandu'),
(96, 'Ketua', 'Kader Teknik'),
(97, 'Wakil Ketua', 'Kader Teknik'),
(98, 'Sekretaris', 'Kader Teknik'),
(99, 'Bendahara', 'Kader Teknik'),
(100, 'Anggota', 'Kader Teknik'),
(101, 'Ketua', 'PPKBD'),
(102, 'Wakil Ketua', 'PPKBD'),
(103, 'Sekretaris', 'PPKBD'),
(104, 'Bendahara', 'PPKBD'),
(105, 'Koordinator', 'PPKBD'),
(106, 'Anggota', 'PPKBD'),
(107, 'Ketua', 'TB'),
(108, 'Wakil Ketua', 'TB'),
(109, 'Sekretaris', 'TB'),
(110, 'Bendahara', 'TB'),
(111, 'Anggota', 'TB'),
(112, 'Kepala Puskesmas', 'Ponkesdes'),
(113, 'Penanggung Jawab Puskesmas Pembantu', 'Ponkesdes'),
(114, 'Koordinator Ponkesdes', 'Ponkesdes'),
(115, 'Pelaksana', 'Ponkesdes');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_konten`
--

CREATE TABLE `tb_jenis_konten` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jenis_konten`
--

INSERT INTO `tb_jenis_konten` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Sejarah Desa'),
(2, 'Struktur Organisasi Desa'),
(3, 'Struktur Organisasi BPD'),
(4, 'PKK'),
(5, 'LPMD'),
(6, 'Karang Taruna'),
(7, 'Posyandu'),
(8, 'RT / RW'),
(9, 'Karang Werda'),
(10, 'Rumah Desa Sehat'),
(11, 'Muslimat'),
(12, 'Aisyiyah'),
(13, 'Sanggar Seni Budaya'),
(14, 'LSM'),
(15, 'KPMD'),
(16, 'KPM'),
(17, 'Kader Teknik'),
(18, 'Kader dan Sub PPKBD'),
(19, 'Kader TB'),
(20, 'UMKM'),
(21, 'Produk Unggulan'),
(22, 'Ponkesdes'),
(23, 'Produk Hukum');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konten`
--

CREATE TABLE `tb_konten` (
  `id_kt` varchar(50) NOT NULL,
  `judul_kt` varchar(100) DEFAULT NULL,
  `isi_kt` mediumtext DEFAULT NULL,
  `gambar_kt` varchar(100) DEFAULT NULL,
  `jenis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_konten`
--

INSERT INTO `tb_konten` (`id_kt`, `judul_kt`, `isi_kt`, `gambar_kt`, `jenis`) VALUES
('60b2cb23a2f70', 'Sejarah Desa Sidomulyo', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '60b2cb23a2f70.PNG', 1),
('60b2cfcff0d8a', 'Struktur Organisasi Pemerintah Desa', '-', '60b2cfcff0d8a.png', 2),
('60b2d0201569b', 'Struktur Organisasi BPD', '-', '60b2d0201569b.jpeg', 3),
('60b794d8cad49', 'Susunan Kepengurusan Tim Penggerak Pemberdayaan Kesejahteraan Keluarga', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60b794d8cad49.PNG', 4),
('60bbfb0c9a0c2', 'Lembaga Pemberdayaan Masyarakat Desa', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n', '60bbfb0c9a0c2.PNG', 5),
('60bc02f11f5a5', 'Karang Taruna Desa', '<ol>\r\n	<li>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</li>\r\n	<li>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</li>\r\n</ol>\r\n', '60bc02f11f5a5.PNG', 6),
('60bc6639aa91e', 'Posyandu', '<ol>\r\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</li>\r\n	<li>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\r\n</ol>\r\n', '60bc6639aa91e.png', 7),
('60bcddbe51956', 'RT / RW', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '60bcddbe51956.jpg', 8),
('60be2e6b14035', 'Karang Werda', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60be2e6b14035.jpg', 9),
('60be2ede150d8', 'Rumah Desa Sehat', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60be2ede150d8.jpg', 10),
('60be479e9bef2', 'Muslimat', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60be479e9bef2.png', 11),
('60beb89b0124a', 'Aisyiyah', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '60beb89b0124a.jpg', 12),
('60bf3ba5177f3', 'Sanggar Seni Budaya', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60bf3ba5177f3.jpg', 13),
('60bf441808c81', 'Lembaga Swadaya Masyarakat', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60bf441808c81.gif', 14),
('60bf45bbbe478', 'Kader Pemberdayaan Masyarakat Desa', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60bf45bbbe478.jpg', 15),
('60bf528dd2ba5', 'Lembaga Pemberdayaan Masyarakat', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60bf528dd2ba5.png', 16),
('60ca699b3fb6e', 'Kader Teknik', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60ca699b3fb6e.jpg', 17),
('60cb6b1ab9986', 'Kader PPKBD dan Sub PPKBD', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60cb6b1ab9986.jpeg', 18),
('60cb6b2a969e5', 'Kader TB', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60cb6b2a969e5.jpg', 19),
('60cb701021194', 'Usaha Mikro Kecil dan Menengah', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60cb701021194.jpg', 20),
('60cb718885667', 'Produk Unggulan', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60cb718885667.jpg', 21),
('60d91ae9ed0a6', 'Pondok Kesehatan Desa', '<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p><strong>Jam Buka:&nbsp;</strong>08.00 - 15.00 WIB</p>\r\n\r\n<p><strong>Fasilitas:&nbsp;</strong>Ruang Pasien, Toilet, Mobil Ambulans</p>\r\n\r\n<p><strong>Jadwal Buka:&nbsp;</strong>Senin - Jumat, Sabtu dan Minggu Libur&nbsp;</p>\r\n', '60d91ae9ed0a6.jpg', 22),
('60dc8699874c4', 'Produk-produk Hukum', '<p>&#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '60dc8699874c4.jpg', 23);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lemmas`
--

CREATE TABLE `tb_lemmas` (
  `id_lm` varchar(50) NOT NULL,
  `nama_lm` varchar(100) NOT NULL,
  `jkel_lm` varchar(10) NOT NULL,
  `no_telp` char(12) DEFAULT NULL,
  `alamat_lm` varchar(255) NOT NULL,
  `jabatan_lm` int(11) NOT NULL,
  `keterangan_jabatan` varchar(255) DEFAULT NULL,
  `asal_organisasi` varchar(255) DEFAULT NULL,
  `lain_lain` varchar(255) DEFAULT NULL,
  `foto_lm` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_lemmas`
--

INSERT INTO `tb_lemmas` (`id_lm`, `nama_lm`, `jkel_lm`, `no_telp`, `alamat_lm`, `jabatan_lm`, `keterangan_jabatan`, `asal_organisasi`, `lain_lain`, `foto_lm`) VALUES
('60b8018dd6a6d', 'Japar Sidik,ST', 'Laki-laki', '081234567890', 'Jl. Jalan', 18, 'PKK', NULL, NULL, '60b8018dd6a6d.jpg'),
('60b801a24a55a', 'Elanda Nurhafizh R,S.Pd', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'PKK', NULL, NULL, '60b801a24a55a.jpg'),
('60b801c101984', 'Herni Kusmiati', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'PKK', NULL, NULL, '60b801c101984.jpg'),
('60b801d51e7f5', 'Imas Sopiah', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'PKK', NULL, NULL, '60b801d51e7f5.jpg'),
('60b801eb51bce', 'Nunung Elia', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 1', NULL, NULL, '60b801eb51bce.jpg'),
('60b80200c7023', 'Siti Mariam', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 1', NULL, NULL, '60b80200c7023.jpg'),
('60b80218cd678', 'Suryati', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 1', NULL, NULL, '60b80218cd678.jpg'),
('60baa37689e5c', 'Sri Mulyati', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 2', NULL, NULL, '60baa37689e5c.jpg'),
('60baa39073b14', 'Evi Riani', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 2', NULL, NULL, '60baa39073b14.jpg'),
('60baa3ab2149a', 'Siti Maesaroh', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 2', NULL, NULL, '60baa3ab2149a.jpg'),
('60baa3bdca9d7', 'Entin Rostini', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 3', NULL, NULL, '60baa3bdca9d7.jpg'),
('60baa3d0a1a8c', 'Imas', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 3', NULL, NULL, '60baa3d0a1a8c.jpg'),
('60baa3e37d93b', 'Titin', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 3', NULL, NULL, '60baa3e37d93b.jpg'),
('60baa3fc210d1', 'Neni', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 4', NULL, NULL, '60baa3fc210d1.jpg'),
('60baa41c574ce', 'Mila Purwantika', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 4', NULL, NULL, '60baa41c574ce.jpg'),
('60baa42d32862', 'Rani', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 4', NULL, NULL, '60baa42d32862.jpg'),
('60baa44327671', 'Enah', 'Perempuan', '081234567890', 'Jl. Jalan', 22, 'PKK', NULL, NULL, '60baa44327671.jpg'),
('60baa45290e65', 'Leni', 'Perempuan', '081234567890', 'Jl. Jalan', 23, 'PKK', NULL, NULL, '60baa45290e65.jpg'),
('60bbf64813943', 'Uyun Sumarna', 'Perempuan', '081234567891', 'Jl. Jalan', 24, 'LPMD', NULL, NULL, '60bbf64813943.jpg'),
('60bbf73a215ff', 'Mansyur Suryana SPd', 'Laki-laki', '081234567891', 'Jl. Jalan', 25, 'LPMD', NULL, NULL, '60bbf73a215ff.jpg'),
('60bbf753d95e7', 'Ohan Setia Permana', 'Laki-laki', '081234567891', 'Jl. Jalan', 26, 'LPMD', NULL, NULL, '60bbf753d95e7.jpg'),
('60bbf7700d764', 'Cecep Edih', 'Laki-laki', '081234567891', 'Jl. Jalan', 27, 'LPMD', NULL, NULL, '60bbf7700d764.jpg'),
('60bbf78781b2e', 'Wardi SPd', 'Laki-laki', '081234567891', 'Jl. Jalan', 28, 'LPMD', NULL, NULL, '60bbf78781b2e.jpg'),
('60bbf796690cb', 'Kadir', 'Laki-laki', '081234567891', 'Jl. Jalan', 29, 'LPMD', NULL, NULL, '60bbf796690cb.jpg'),
('60bbf7b0abf0c', 'Teten Sutendi', 'Laki-laki', '081234567891', 'Jl. Jalan', 30, 'LPMD', NULL, NULL, '60bbf7b0abf0c.jpg'),
('60bbf7c0933c7', 'Enah', 'Laki-laki', '081234567891', 'Jl. Jalan', 31, 'LPMD', NULL, NULL, '60bbf7c0933c7.jpg'),
('60bc4da29ee34', 'Bambang', 'Laki-laki', '081234567890', 'Jl. Jalan', 32, 'Karang Taruna', NULL, NULL, '60bc4da29ee34.jpg'),
('60bc4dc39283a', 'Rudi', 'Laki-laki', '081234567890', 'Jl. Jalan', 33, 'Karang Taruna', NULL, NULL, '60bc4dc39283a.jpg'),
('60bc4e29b1fd7', 'Hartono', 'Laki-laki', '081234567890', 'Jl. Jalan', 34, 'Karang Taruna', NULL, NULL, '60bc4e29b1fd7.jpg'),
('60bc4e3dce807', 'Ali Sunan', 'Laki-laki', '081234567890', 'Jl. Jalan', 35, 'Karang Taruna', NULL, NULL, '60bc4e3dce807.jpg'),
('60bc4e4f8464a', 'Mamat', 'Laki-laki', '081234567890', 'Jl. Jalan', 36, 'Karang Taruna', NULL, 'Dusun A RT 001 RW  001', '60bc4e4f8464a.jpg'),
('60bc4e5ddc129', 'Bahul', 'Laki-laki', '081234567890', 'Jl. Jalan', 37, 'Karang Taruna', NULL, NULL, '60bc4e5ddc129.jpg'),
('60bc4e6e0aad5', 'Yasin', 'Laki-laki', '081234567890', 'Jl. Jalan', 38, 'Karang Taruna', NULL, NULL, '60bc4e6e0aad5.jpg'),
('60bc4e8dc8848', 'Senal', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pendidikan Dan Pelatihan', NULL, NULL, '60bc4e8dc8848.jpg'),
('60bc4e9cd7944', 'Hairul', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pendidikan Dan Pelatihan', NULL, NULL, '60bc4e9cd7944.jpg'),
('60bc51ce4718c', 'Fandi', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pengembangan Usaha Kegiatan Sosial', NULL, NULL, '60bc51ce4718c.jpg'),
('60bc51fc2fac7', 'Holik', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pengembangan Usaha Kegiatan Sosial', NULL, NULL, '60bc51fc2fac7.jpg'),
('60bc521236a30', 'Muhtar', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Usaha Ekonomi Produktif', NULL, NULL, '60bc521236a30.jpg'),
('60bc5222cb29b', 'Hadi', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Usaha Ekonomi Produktif', NULL, NULL, '60bc5222cb29b.jpg'),
('60bc523263ce3', 'Dedi', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pengembangan Kegiatan Kerohanian Dan Pembina', NULL, NULL, '60bc523263ce3.jpg'),
('60bc524013361', 'Heri', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pengembangan Kegiatan Kerohanian Dan Pembina', NULL, NULL, '60bc524013361.jpg'),
('60bc524f06e40', 'Hari', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pengembangan Kegiatan Olahraga Dan Seni Buda', NULL, NULL, '60bc524f06e40.jpg'),
('60bc5264cb6b7', 'Afiff', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pengembangan Kegiatan Olahraga Dan Seni Buda', NULL, NULL, '60bc5264cb6b7.jpg'),
('60bc52882b52a', 'Adit', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Lingkungan Hidup dan Pariwisata', NULL, NULL, '60bc52882b52a.jpg'),
('60bc5296cc0e7', 'Helmi', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Lingkungan Hidup dan Pariwisata', NULL, NULL, '60bc5296cc0e7.jpg'),
('60bc52b216028', 'Disma', 'Perempuan', '081234567890', 'Jl. Jalan', 39, 'Seksi Biro Organisasi, Pengembangan Hubungan Kerja', NULL, NULL, '60bc52b216028.jpg'),
('60bc52c75f201', 'Nanda', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Biro Organisasi, Pengembangan Hubungan Kerja', NULL, NULL, '60bc52c75f201.jpg'),
('60bc52da897ff', 'Angga', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Keamanan dan Pendamping Kegiatan', NULL, NULL, '60bc52da897ff.jpg'),
('60bc52e800c4a', 'Isbi', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Keamanan dan Pendamping Kegiatan', NULL, NULL, '60bc52e800c4a.jpg'),
('60bc52fa4a456', 'Akhyar', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Hubungan Masyarakat', NULL, NULL, '60bc52fa4a456.jpg'),
('60bc530a43db5', 'Wanda', 'Perempuan', '081234567890', 'Jl. Jalan', 40, 'Seksi Hubungan Masyarakat', NULL, NULL, '60bc530a43db5.jpg'),
('60bc535458d7c', 'Ajeng', 'Perempuan', '081234567890', 'Jl. Jalan', 39, 'Seksi Logistik / Perlengkapan', NULL, NULL, '60bc535458d7c.jpg'),
('60bc536452b40', 'Edwiend', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Logistik / Perlengkapan', NULL, NULL, '60bc536452b40.jpg'),
('60bc73cad74b5', 'Menik', 'Perempuan', '081234567890', 'Jl. Jalan', 41, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 001', '60bc73cad74b5.jpg'),
('60bc793a3260c', 'Yuni', 'Perempuan', '081234567890', 'Jl. Jalan', 42, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 002', '60bc793a3260c.jpg'),
('60bc794e38c87', 'Lily', 'Perempuan', '081234567890', 'Jl. Jalan', 43, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 003', '60bc794e38c87.jpg'),
('60bc7962b30d0', 'Puteri', 'Perempuan', '081234567890', 'Jl. Jalan', 44, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 002', '60bc7962b30d0.jpg'),
('60bc798208bee', 'Arum', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Kesehatan Ibu dan Anak', 'Posyandu A', 'Dusun A RT 001 RW 001', '60bc798208bee.jpg'),
('60bc799796959', 'Ratih', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Keluarga Berencana', 'Posyandu A', 'Dusun A RT 001 RW 003', '60bc799796959.jpg'),
('60bc79be9b0ac', 'Fenti', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Imunisasi', 'Posyandu A', 'Dusun A RT 001 RW 004', '60bc79be9b0ac.jpg'),
('60bc79d48275e', 'Feril', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Gizi', 'Posyandu A', 'Dusun A RT 001 RW 001', '60bc79d48275e.jpg'),
('60bc79ec2a58c', 'Ike', 'Perempuan', '081234567890', 'Jl. Jalan', 41, 'Posyandu', 'Posyandu B', 'Dusun A RT 001 RW 004', '60bc79ec2a58c.jpg'),
('60bccab7871f6', 'Bapak Supri', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 001', 'RW 001', '60bccab7871f6.jpg'),
('60bcd3a32bd0e', 'Bapak Aziz', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 002', 'RW 001', '60bcd3a32bd0e.jpg'),
('60bcd3bc2eef8', 'Bapak Fikri', 'Laki-laki', '081234567890', 'Jl. Jalan', 94, 'RW', 'RW 002', 'Dusun A', '60bcd3bc2eef8.jpg'),
('60bcd602088bb', 'Bapak Firza', 'Laki-laki', '081234567890', 'Jl. Jalan', 94, 'RW', 'RW 001', 'Dusun B', '60bcd602088bb.jpg'),
('60bcdbebd725f', 'Bapak Udin', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 001', 'RW 002', '60bcdbebd725f.jpg'),
('60bcdc1dda5f4', 'Bapak Alfi', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 003', 'RW 002', '60bcdc1dda5f4.jpg'),
('60bcdc347151f', 'Bapak Rozak', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 002', 'RW 003', '60bcdc347151f.jpg'),
('60be274bb4431', 'Saiful', 'Laki-laki', '081234567890', 'Jl. Jalan', 52, 'Karang Werda', 'Karang Werda A', NULL, '60be274bb4431.jpg'),
('60be277323e4d', 'Devian', 'Laki-laki', '081234567891', 'Jl. Jalan', 53, 'Karang Werda', 'Karang Werda A', NULL, '60be277323e4d.jpg'),
('60be27872fbe3', 'Lihin', 'Laki-laki', '081234567890', 'Jl. Jalan', 54, 'Karang Werda', 'Karang Werda A', NULL, '60be27872fbe3.jpg'),
('60be27936f998', 'Alfan', 'Laki-laki', '081234567890', 'Jl. Jalan', 55, 'Karang Werda', 'Karang Werda A', NULL, '60be27936f998.jpg'),
('60be279e3ea24', 'Agus', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', 'Karang Werda A', NULL, '60be279e3ea24.jpg'),
('60be27a8de3e5', 'Fendi', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', 'Karang Werda A', NULL, '60be27a8de3e5.jpg'),
('60be27ba818ed', 'Hisbul', 'Laki-laki', '081234567890', 'Jl. Jalan', 52, 'Karang Werda', 'Karang Werda B', NULL, '60be27ba818ed.jpg'),
('60be27ccd60b1', 'Riski', 'Laki-laki', '081234567890', 'Jl. Jalan', 53, 'Karang Werda', 'Karang Werda B', NULL, '60be27ccd60b1.jpg'),
('60be27da02d14', 'Aswin', 'Laki-laki', '081234567890', 'Jl. Jalan', 54, 'Karang Werda', 'Karang Werda B', NULL, '60be27da02d14.jpg'),
('60be27e4be286', 'Bashori', 'Laki-laki', '081234567890', 'Jl. Jalan', 55, 'Karang Werda', 'Karang Werda B', NULL, '60be27e4be286.jpg'),
('60be284e1a8bd', 'Lukman', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', 'Karang Werda B', NULL, '60be284e1a8bd.jpg'),
('60be37f78baa1', 'Bagus', 'Laki-laki', '081234567890', 'Jl. Jalan', 57, 'RDS', NULL, NULL, '60be37f78baa1.jpg'),
('60be3832a9f60', 'Muzakki', 'Laki-laki', '081234567890', 'Jl. Jalan', 58, 'RDS', NULL, NULL, '60be3832a9f60.jpg'),
('60be38451a946', 'Irfan', 'Laki-laki', '081234567890', 'Jl. Jalan', 59, 'RDS', NULL, NULL, '60be38451a946.jpg'),
('60be384d26e30', 'Irvan', 'Laki-laki', '081234567890', 'Jl. Jalan', 60, 'RDS', NULL, NULL, '60be384d26e30.jpg'),
('60be3866b0417', 'Alwin', 'Laki-laki', '081234567890', 'Jl. Jalan', 61, 'RDS', NULL, NULL, '60be3866b0417.jpg'),
('60be409caff43', 'Indah', 'Perempuan', '081234567890', 'Jl. Jalan', 62, 'Muslimat', 'Muslimat A', NULL, '60be409caff43.jpg'),
('60be40ada838f', 'Nur Haqiqi', 'Perempuan', '081234567890', 'Jl. Jalan', 63, 'Muslimat', 'Muslimat A', NULL, '60be40ada838f.jpg'),
('60be40bbb6bac', 'Rizka', 'Perempuan', '081234567890', 'Jl. Jalan', 64, 'Muslimat', 'Muslimat A', NULL, '60be40bbb6bac.jpg'),
('60be40ca09c10', 'Ingka', 'Perempuan', '081234567890', 'Jl. Jalan', 65, 'Muslimat', 'Muslimat A', NULL, '60be40ca09c10.jpg'),
('60be40d627313', 'Mita', 'Perempuan', '081234567890', 'Jl. Jalan', 66, 'Muslimat', 'Muslimat A', NULL, '60be40d627313.jpg'),
('60be4137e0fa0', 'Linda', 'Perempuan', '081234567890', 'Jl. Jalan', 62, 'Muslimat', 'Muslimat B', NULL, '60be4137e0fa0.jpg'),
('60be4145dfb59', 'Evi', 'Perempuan', '081234567890', 'Jl. Jalan', 63, 'Muslimat', 'Muslimat B', NULL, '60be4145dfb59.jpg'),
('60be4153056de', 'Iva', 'Perempuan', '081234567890', 'Jl. Jalan', 64, 'Muslimat', 'Muslimat B', NULL, '60be4153056de.jpg'),
('60be416bd7b6b', 'Sofi', 'Perempuan', '081234567890', 'Jl. Jalan', 65, 'Muslimat', 'Muslimat B', NULL, '60be416bd7b6b.jpg'),
('60be417d255cc', 'Vicky', 'Perempuan', '081234567890', 'Jl. Jalan', 66, 'Muslimat', 'Muslimat B', NULL, '60be417d255cc.jpg'),
('60bec1925a1c0', 'Yunita', 'Perempuan', '081234567890', 'Jl. Jalan', 67, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1925a1c0.jpg'),
('60bec19d18f22', 'Ninik', 'Perempuan', '081234567890', 'Jl. Jalan', 68, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec19d18f22.jpg'),
('60bec1a785277', 'Nuril', 'Laki-laki', '081234567890', 'Jl. Jalan', 69, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1a785277.jpg'),
('60bec1b2003dc', 'Aji', 'Laki-laki', '081234567890', 'Jl. Jalan', 70, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1b2003dc.jpg'),
('60bec1c1923f1', 'Surateno', 'Laki-laki', '081234567890', 'Jl. Jalan', 71, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1c1923f1.jpg'),
('60bec1dfa5188', 'Maya', 'Perempuan', '081234567890', 'Jl. Jalan', 67, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec1dfa5188.jpg'),
('60bec1ef58156', 'Bety', 'Perempuan', '081234567890', 'Jl. Jalan', 68, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec1ef58156.jpg'),
('60bec1fbc8fe2', 'Ulva', 'Perempuan', '081234567890', 'Jl. Jalan', 69, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec1fbc8fe2.jpg'),
('60bec207a5eed', 'Nurma', 'Perempuan', '081234567890', 'Jl. Jalan', 70, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec207a5eed.jpg'),
('60bec21119701', 'Ika', 'Perempuan', '081234567890', 'Jl. Jalan', 71, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec21119701.jpg'),
('60bf3d54f2972', 'Ami', 'Perempuan', '081234567890', 'Jl. Jalan', 72, 'SSB', 'Sanggar Seni A', NULL, '60bf3d54f2972.jpg'),
('60bf3d5f7adf1', 'Isa', 'Perempuan', '081234567890', 'Jl. Jalan', 73, 'SSB', 'Sanggar Seni A', NULL, '60bf3d5f7adf1.jpg'),
('60bf3d6c4ab97', 'Wati', 'Perempuan', '081234567890', 'Jl. Jalan', 74, 'SSB', 'Sanggar Seni A', NULL, '60bf3d6c4ab97.jpg'),
('60bf3d7a82810', 'Hanifa', 'Perempuan', '081234567890', 'Jl. Jalan', 75, 'SSB', 'Sanggar Seni A', NULL, '60bf3d7a82810.jpg'),
('60bf3d88587a7', 'Vira', 'Perempuan', '081234567890', 'Jl. Jalan', 76, 'SSB', 'Sanggar Seni A', NULL, '60bf3d88587a7.jpg'),
('60bf3daa9cce2', 'Safira', 'Perempuan', '081234567890', 'Jl. Jalan', 72, 'SSB', 'Sanggar Seni B', NULL, '60bf3daa9cce2.jpg'),
('60bf3db7122d3', 'Nisa', 'Perempuan', '081234567890', 'Jl. Jalan', 73, 'SSB', 'Sanggar Seni B', NULL, '60bf3db7122d3.jpg'),
('60bf3dd754670', 'Ayin', 'Perempuan', '081234567890', 'Jl. Jalan', 74, 'SSB', 'Sanggar Seni B', NULL, '60bf3dd754670.jpg'),
('60bf3de8982e1', 'Afi', 'Perempuan', '081234567890', 'Jl. Jalan', 75, 'SSB', 'Sanggar Seni B', NULL, '60bf3de8982e1.jpg'),
('60bf3dfb0e57d', 'Eva', 'Perempuan', '081234567890', 'Jl. Jalan', 76, 'SSB', 'Sanggar Seni B', NULL, '60bf3dfb0e57d.jpg'),
('60bf4124d6a8e', 'Hayu', 'Perempuan', '081234567891', 'Jl. Jalan', 77, 'LSM', 'LSM A', NULL, '60bf4124d6a8e.jpg'),
('60bf414dd7b2d', 'Vanny', 'Perempuan', '081234567890', 'Jl. Jalan', 78, 'LSM', 'LSM A', NULL, '60bf414dd7b2d.jpg'),
('60bf4162109e2', 'Rahma', 'Perempuan', '081234567890', 'Jl. Jalan', 79, 'LSM', 'LSM A', NULL, '60bf4162109e2.jpg'),
('60bf41761a8dd', 'Safitri', 'Perempuan', '081234567890', 'Jl. Jalan', 80, 'LSM', 'LSM A', NULL, '60bf41761a8dd.jpg'),
('60bf4182d6864', 'Ayunda', 'Perempuan', '081234567890', 'Jl. Jalan', 81, 'LSM', 'LSM A', NULL, '60bf4182d6864.jpg'),
('60bf419861fe6', 'Lintang', 'Laki-laki', '081234567890', 'Jl. Jalan', 77, 'LSM', 'LSM B', NULL, '60bf419861fe6.jpg'),
('60bf41a72aab9', 'Andys', 'Laki-laki', '081234567890', 'Jl. Jalan', 78, 'LSM', 'LSM B', NULL, '60bf41a72aab9.jpg'),
('60bf41b4c1331', 'Robert', 'Laki-laki', '081234567890', 'Jl. Jalan', 79, 'LSM', 'LSM B', NULL, '60bf41b4c1331.jpg'),
('60bf41c2593ff', 'Habibi', 'Laki-laki', '081234567890', 'Jl. Jalan', 80, 'LSM', 'LSM B', NULL, '60bf41c2593ff.jpg'),
('60bf41cfa1e01', 'Iqbal', 'Laki-laki', '081234567890', 'Jl. Jalan', 81, 'LSM', 'LSM B', NULL, '60bf41cfa1e01.jpg'),
('60bf4ad25e71e', 'Hesti', 'Perempuan', '081234567890', 'Jl. Jalan', 82, 'KPMD', NULL, 'Dusun A', '60bf4ad25e71e.jpg'),
('60bf4b0c93c9f', 'Ela', 'Perempuan', '081234567890', 'Jl. Jalan', 83, 'KPMD', NULL, 'Dusun A', '60bf4b0c93c9f.jpg'),
('60bf4b1d361a5', 'Yuli', 'Perempuan', '081234567890', 'Jl. Jalan', 84, 'KPMD', NULL, 'Dusun B', '60bf4b1d361a5.jpg'),
('60bf4b2d65dc2', 'Ira', 'Perempuan', '081234567890', 'Jl. Jalan', 85, 'KPMD', NULL, 'Dusun C', '60bf4b2d65dc2.jpg'),
('60bf4b451554a', 'Silvi', 'Perempuan', '081234567890', 'Jl. Jalan', 86, 'KPMD', NULL, 'Dusun B', '60bf4b451554a.jpg'),
('60bf55959db9c', 'Inema', 'Perempuan', '081234567890', 'Jl. Jalan', 87, 'KPM', NULL, 'Dusun X', '60bf55959db9c.jpg'),
('60bf55a8e6796', 'Marta', 'Perempuan', '081234567890', 'Jl. Jalan', 88, 'KPM', NULL, 'Dusun Y', '60bf55a8e6796.jpg'),
('60bf55ba85739', 'Brigia', 'Perempuan', '081234567890', 'Jl. Jalan', 89, 'KPM', NULL, 'Dusun X', '60bf55ba85739.jpg'),
('60bf55cc2f8d0', 'Dian', 'Perempuan', '081234567890', 'Jl. Jalan', 90, 'KPM', NULL, 'Dusun Y', '60bf55cc2f8d0.jpg'),
('60bf55e66904f', 'Clara', 'Perempuan', '081234567890', 'Jl. Jalan', 91, 'KPM', NULL, 'Dusun Z', '60bf55e66904f.jpg'),
('60c52d2417c28', 'Bapak Munir', 'Laki-laki', '081234567890', 'Jl. Jalan', 94, 'RW', 'RW 003', 'Dusun C', '60c52d2417c28.jpg'),
('60ca6a4cdd199', 'Yanto', 'Laki-laki', '081234567890', 'Jl. Jalan', 96, 'Kader Teknik', NULL, NULL, '60ca6a4cdd199.jpg'),
('60cb65dcb3c88', 'Holiful', 'Laki-laki', '081234567890', 'Jl. Jalan', 97, 'Kader Teknik', NULL, NULL, '60cb65dcb3c88.jpg'),
('60cb65ffdc7d0', 'Ahmad', 'Laki-laki', '081234567890', 'Jl. Jalan', 98, 'Kader Teknik', NULL, NULL, '60cb65ffdc7d0.jpg'),
('60cb661c2fae4', 'Dista', 'Laki-laki', '081234567890', 'Jl. Jalan', 99, 'Kader Teknik', NULL, NULL, '60cb661c2fae4.jpg'),
('60cb66338f628', 'Teguh', 'Laki-laki', '081234567890', 'Jl. Jalan', 100, 'Kader Teknik', NULL, NULL, '60cb66338f628.jpg'),
('60cb67834599f', 'Uri', 'Laki-laki', '081234567890', 'Jl. Jalan', 101, 'PPKBD', NULL, NULL, '60cb67834599f.jpg'),
('60cb67b8e6929', 'Prasetya', 'Laki-laki', '081234567890', 'Jl. Jalan', 102, 'PPKBD', NULL, NULL, '60cb67b8e6929.jpg'),
('60cb67d06c211', 'Jimas', 'Laki-laki', '081234567890', 'Jl. Jalan', 103, 'PPKBD', NULL, NULL, '60cb67d06c211.jpg'),
('60cb67e7d7fe7', 'Alwan', 'Laki-laki', '081234567890', 'Jl. Jalan', 104, 'PPKBD', NULL, NULL, '60cb67e7d7fe7.jpg'),
('60cb6800b3d93', 'Judin', 'Laki-laki', '081234567890', 'Jl. Jalan', 105, 'Sub A', NULL, NULL, '60cb6800b3d93.jpg'),
('60cb6814e0df6', 'Zelvi', 'Laki-laki', '081234567890', 'Jl. Jalan', 106, 'Sub A', NULL, NULL, '60cb6814e0df6.jpg'),
('60cb696cd81c8', 'Wildan', 'Laki-laki', '081234567890', 'Jl. Jalan', 107, 'Kader TB', NULL, NULL, '60cb696cd81c8.jpg'),
('60cb6987acb58', 'Pandu', 'Laki-laki', '081234567890', 'Jl. Jalan', 108, 'Kader TB', NULL, NULL, '60cb6987acb58.jpg'),
('60cb699aa282c', 'Fahrul', 'Laki-laki', '081234567890', 'Jl. Jalan', 109, 'Kader TB', NULL, NULL, '60cb699aa282c.jpg'),
('60cb69b780fd9', 'Rico', 'Laki-laki', '081234567890', 'Jl. Jalan', 110, 'Kader TB', NULL, NULL, '60cb69b780fd9.jpg'),
('60cb69dacdd4e', 'Juniar', 'Laki-laki', '081234567890', 'Jl. Jalan', 111, 'Kader TB', NULL, NULL, '60cb69dacdd4e.jpg'),
('60cb6bbf8cd18', 'Dayat', 'Laki-laki', '081234567890', 'Jl. Jalan', 105, 'Sub B', NULL, NULL, '60cb6bbf8cd18.jpg'),
('60cb6bd478f3c', 'Roby', 'Laki-laki', '081234567890', 'Jl. Jalan', 106, 'Sub B', NULL, NULL, '60cb6bd478f3c.jpg'),
('60dbb86be87ee', 'Suyoto', 'Laki-laki', '081234567890', 'Jl. Jalan', 112, 'Ponkesdes', NULL, NULL, '60dbb86be87ee.jpg'),
('60dbbaea290f7', 'Sutini', 'Perempuan', '081234567890', 'Jl. Jalan', 113, 'Ponkesdes', NULL, NULL, '60dbbaea290f7.jpg'),
('60dbc069f2f11', 'Sukarni', 'Perempuan', '081234567890', 'Jl. Jalan', 114, 'Ponkesdes', NULL, NULL, '60dbc069f2f11.jpg'),
('60dbc1c88a810', 'Suratna', 'Perempuan', '081234567890', 'Jl. Jalan', 115, 'Ponkesdes', NULL, NULL, '60dbc1c88a810.jpg'),
('60dbc1e168470', 'Heriyanto', 'Laki-laki', '081234567890', 'Jl. Jalan', 115, 'Ponkesdes', NULL, NULL, '60dbc1e168470.jpg'),
('60dbc1ff2a17b', 'Mat', 'Laki-laki', '081234567890', 'Jl. Jalan', 115, 'Ponkesdes', NULL, NULL, '60dbc1ff2a17b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_organisasi`
--

CREATE TABLE `tb_organisasi` (
  `id_org` int(11) NOT NULL,
  `nama_org` varchar(100) NOT NULL,
  `keterangan_org` varchar(100) NOT NULL,
  `lain_lain` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_organisasi`
--

INSERT INTO `tb_organisasi` (`id_org`, `nama_org`, `keterangan_org`, `lain_lain`) VALUES
(1, 'Pokja 1', 'Ket. PKK', NULL),
(2, 'Pokja 2', 'Ket. PKK', NULL),
(3, 'Pokja 3', 'Ket. PKK', NULL),
(4, 'Pokja 4', 'Ket. PKK', NULL),
(5, 'PKK', 'Ket. PKK', NULL),
(8, 'Posyandu A', 'Asal Posyandu', 'Dusun X RT 002 / RW 002'),
(9, 'Posyandu B', 'Asal Posyandu', 'Dusun Z RT 003 / RW 003'),
(10, 'Seksi Pendidikan Dan Pelatihan', 'Ket. Karang Taruna', NULL),
(11, 'Seksi Pengembangan Usaha Kegiatan Sosial', 'Ket. Karang Taruna', NULL),
(12, 'Seksi Usaha Ekonomi Produktif', 'Ket. Karang Taruna', NULL),
(13, 'Seksi Pengembangan Kegiatan Kerohanian Dan Pembinaan Mental', 'Ket. Karang Taruna', NULL),
(14, 'Seksi Pengembangan Kegiatan Olahraga Dan Seni Budaya', 'Ket. Karang Taruna', NULL),
(15, 'Seksi Lingkungan Hidup dan Pariwisata', 'Ket. Karang Taruna', NULL),
(16, 'Seksi Biro Organisasi, Pengembangan Hubungan Kerja', 'Ket. Karang Taruna', NULL),
(17, 'Seksi Keamanan dan Pendamping Kegiatan', 'Ket. Karang Taruna', NULL),
(18, 'Seksi Hubungan Masyarakat', 'Ket. Karang Taruna', NULL),
(19, 'Seksi Logistik / Perlengkapan', 'Ket. Karang Taruna', NULL),
(20, 'Kader Kesehatan Ibu dan Anak', 'Ket. Posyandu', NULL),
(21, 'Kader Keluarga Berencana', 'Ket. Posyandu', NULL),
(22, 'Kader Imunisasi', 'Ket. Posyandu', NULL),
(23, 'Kader Gizi', 'Ket. Posyandu', NULL),
(24, 'RW 001', 'Asal RW', 'Dusun A'),
(25, 'RW 002', 'Asal RW', 'Dusun B'),
(26, 'RW 003', 'Asal RW', 'Dusun C'),
(27, 'RT 001', 'Asal RT', NULL),
(28, 'RT 002', 'Asal RT', NULL),
(29, 'RT 003', 'Asal RT', NULL),
(30, 'RT 004', 'Asal RT', NULL),
(31, 'Karang Werda A', 'Asal Karang Werda', NULL),
(32, 'Karang Werda B', 'Asal Karang Werda', NULL),
(33, 'Muslimat A', 'Asal Muslimat', 'Dusun A RT 001 / RW 001'),
(34, 'Muslimat B', 'Asal Muslimat', NULL),
(35, 'Aisyiyah A', 'Asal Aisyiyah', NULL),
(36, 'Aisyiyah B', 'Asal Aisyiyah', NULL),
(37, 'Sanggar Seni A', 'Asal SSB', NULL),
(38, 'Sanggar Seni B', 'Asal SSB', NULL),
(39, 'LSM A', 'Asal LSM', NULL),
(40, 'LSM B', 'Asal LSM', NULL),
(41, 'Karang Taruna', 'Ket. Karang Taruna', NULL),
(42, 'Posyandu', 'Ket. Posyandu', NULL),
(43, 'PPKBD', 'Ket. PPKBD', NULL),
(44, 'Sub A', 'Ket. PPKBD', NULL),
(45, 'Sub B', 'Ket. PPKBD', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk_unggulan`
--

CREATE TABLE `tb_produk_unggulan` (
  `id_pr` varchar(50) NOT NULL,
  `nama_pr` varchar(255) NOT NULL,
  `umkm_pr` varchar(50) NOT NULL,
  `foto_produk` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_produk_unggulan`
--

INSERT INTO `tb_produk_unggulan` (`id_pr`, `nama_pr`, `umkm_pr`, `foto_produk`) VALUES
('60cb71e2359af', 'Nasi Goreng', '60cb6f974a959', '60cb71e2359af.jpeg'),
('60cb71f34e133', 'Terang Bulan', '60cb6fc518763', '60cb71f34e133.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_umkm`
--

CREATE TABLE `tb_umkm` (
  `id_umkm` varchar(50) NOT NULL,
  `nama_umkm` varchar(100) NOT NULL,
  `alamat_umkm` varchar(255) NOT NULL,
  `telp_umkm` char(12) NOT NULL,
  `pemilik_umkm` varchar(100) NOT NULL,
  `foto_umkm` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_umkm`
--

INSERT INTO `tb_umkm` (`id_umkm`, `nama_umkm`, `alamat_umkm`, `telp_umkm`, `pemilik_umkm`, `foto_umkm`) VALUES
('60cb6f974a959', 'Makanan A', 'Jl. Jalan', '081234567890', 'Tono', '60cb6f974a959.jpg'),
('60cb6fc518763', 'Makanan B', 'Jl. Jalan', '081234567890', 'Bahrus', '60cb6fc518763.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `username_user` varchar(100) NOT NULL,
  `password_user` varchar(100) NOT NULL,
  `level_user` int(11) NOT NULL COMMENT '1 = Admin WP ; 2 = Admin Pendataan ; 3 = Editor Berita',
  `last_login` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `username_user`, `password_user`, `level_user`, `last_login`, `date_created`, `date_updated`) VALUES
(1, 'Supandi', 'supandi98', 'dc9bd5209c9b516eeebab3b9bd127427', 1, '2021-06-30 14:35:39', '2021-05-31 09:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_aparatur`
--
ALTER TABLE `tb_aparatur`
  ADD PRIMARY KEY (`id_apt`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`id_jbt`);

--
-- Indexes for table `tb_jenis_konten`
--
ALTER TABLE `tb_jenis_konten`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tb_konten`
--
ALTER TABLE `tb_konten`
  ADD PRIMARY KEY (`id_kt`);

--
-- Indexes for table `tb_lemmas`
--
ALTER TABLE `tb_lemmas`
  ADD PRIMARY KEY (`id_lm`);

--
-- Indexes for table `tb_organisasi`
--
ALTER TABLE `tb_organisasi`
  ADD PRIMARY KEY (`id_org`);

--
-- Indexes for table `tb_produk_unggulan`
--
ALTER TABLE `tb_produk_unggulan`
  ADD PRIMARY KEY (`id_pr`);

--
-- Indexes for table `tb_umkm`
--
ALTER TABLE `tb_umkm`
  ADD PRIMARY KEY (`id_umkm`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `id_jbt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `tb_jenis_konten`
--
ALTER TABLE `tb_jenis_konten`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tb_organisasi`
--
ALTER TABLE `tb_organisasi`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
