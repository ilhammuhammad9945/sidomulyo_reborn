<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_aparatur extends CI_Model
{

    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    private $_table = "tb_aparatur";

    public $id_apt;
    public $nama_apt;
    public $jkel_apt;
    public $telp_apt;
    public $alamat_apt;
    public $jabatan_apt;
    public $ket_apt;
    public $tugas_pokok;
    public $fungsi;
    public $foto_apt = "default.jpg";
    public $id_atasan;
    public $level;

    public function rules()
    {
        return [
            ['field' => 'fnama',
            'label' => 'Nama',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],

            ['field' => 'fjkel',
            'label' => 'Jenis Kelamin',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],

            ['field' => 'falamat',
            'label' => 'Alamat',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],
            
            ['field' => 'fjabatan',
            'label' => 'Jabatan',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],
            
            ['field' => 'fbawahan',
            'label' => 'Bawahan',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],

            ['field' => 'fpokok',
            'label' => 'Tugas Pokok',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],

            ['field' => 'ffungsi',
            'label' => 'Fungsi',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )],

            ['field' => 'fatasan',
            'label' => 'Atasan',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )]
        ];
    }

    public function getAll()
    {
        // return $this->db->get($this->_table)->result();
        $this->db->select('*');
        $this->db->from('tb_jabatan');
        $this->db->join('tb_aparatur', 'tb_aparatur.jabatan_apt = tb_jabatan.id_jbt');
        $this->db->where('tb_aparatur.ket_apt', 'Aparatur Desa');
        return $this->db->get()->result();
    }

    public function getAll_bpd()
    {
        // return $this->db->get($this->_table)->result();
        $this->db->select('*');
        $this->db->from('tb_jabatan');
        $this->db->join('tb_aparatur', 'tb_aparatur.jabatan_apt = tb_jabatan.id_jbt');
        $this->db->where('tb_aparatur.ket_apt', 'Aparatur BPD');
        return $this->db->get()->result();
    }
    
    public function getById($id)
    {   
        return $this->db->get_where($this->_table, ["id_apt" => $id])->row();
    }

    public function save_desa()
    {
        $post = $this->input->post();
        $this->id_apt = uniqid();
        $this->nama_apt = $post["fnama"];
        $this->jkel_apt = $post["fjkel"];
        $this->telp_apt = $post["ftelp"];
        $this->alamat_apt = $post["falamat"];
        $this->jabatan_apt = $post["fjabatan"];
        $this->ket_apt = "Aparatur Desa";
        $this->tugas_pokok = $post["fpokok"];
        $this->fungsi = $post["ffungsi"];
        $this->foto_apt = $this->_uploadImage();
        $this->id_atasan = $post["fatasan"];
        $this->level = $post["fbawahan"];
        return $this->db->insert($this->_table, $this);
    }

    public function save_bpd()
    {
        $post = $this->input->post();
        $this->id_apt = uniqid();
        $this->nama_apt = $post["fnama"];
        $this->jkel_apt = $post["fjkel"];
        $this->telp_apt = $post["ftelp"];
        $this->alamat_apt = $post["falamat"];
        $this->jabatan_apt = $post["fjabatan"];
        $this->ket_apt = "Aparatur BPD";
        $this->tugas_pokok = $post["fpokok"];
        $this->fungsi = $post["ffungsi"];
        $this->foto_apt = $this->_uploadImage();
        $this->id_atasan = $post["fatasan"];
        $this->level = $post["fbawahan"];
        return $this->db->insert($this->_table, $this);
    }

    public function update_desa()
    {
        $post = $this->input->post();
        $this->id_apt = $post["fid"];
        $this->nama_apt = $post["fnama"];
        $this->jkel_apt = $post["fjkel"];
        $this->telp_apt = $post["ftelp"];
        $this->alamat_apt = $post["falamat"];
        $this->jabatan_apt = $post["fjabatan"];
        $this->ket_apt = "Aparatur Desa";
        $this->tugas_pokok = $post["fpokok"];
        $this->fungsi = $post["ffungsi"];
        if (!empty($_FILES["ffoto"]["name"])) {
            $this->foto_apt = $this->_uploadImage();
        } else {
            $this->foto_apt = $post["ffotolama"];
        }
        $this->id_atasan = $post["fatasan"];
        $this->level = $post["fbawahan"];
        return $this->db->update($this->_table, $this, array('id_apt' => $post['fid']));
    }

    public function update_bpd()
    {
        $post = $this->input->post();
        $this->id_apt = $post["fid"];
        $this->nama_apt = $post["fnama"];
        $this->jkel_apt = $post["fjkel"];
        $this->telp_apt = $post["ftelp"];
        $this->alamat_apt = $post["falamat"];
        $this->jabatan_apt = $post["fjabatan"];
        $this->ket_apt = "Aparatur BPD";
        $this->tugas_pokok = $post["fpokok"];
        $this->fungsi = $post["ffungsi"];
        if (!empty($_FILES["ffoto"]["name"])) {
            $this->foto_apt = $this->_uploadImage();
        } else {
            $this->foto_apt = $post["ffotolama"];
        }
        $this->id_atasan = $post["fatasan"];
        $this->level = $post["fbawahan"];
        return $this->db->update($this->_table, $this, array('id_apt' => $post['fid']));
    }

    public function delete($id)
    {   
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id_apt" => $id));
    }


    private function _uploadImage()
    {
        $config['upload_path']          = './assets/upload/aparatur/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $this->id_apt;
        $config['overwrite']            = true;
        $config['max_size']             = 1024 * 5; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('ffoto')) {
            return $this->upload->data("file_name");
        }
        // print_r($this->upload->display_errors());
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $aparatur = $this->getById($id);
        if ($aparatur->foto_apt != "default.jpg") {
            $filename = explode(".", $aparatur->foto_apt)[0];
            return array_map('unlink', glob(FCPATH."assets/upload/aparatur/$filename.*"));
        }
    }

    public function getQuery($q)
    {
        $query = $this->db->query($q);
        return $query;
    }
}
?>