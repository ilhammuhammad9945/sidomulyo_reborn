<?php

class M_login extends CI_Model
{
    private $_table = "tb_user";

    public function doLogin(){
		$post = $this->input->post();

        // cari user berdasarkan email dan username
        $this->db->where('username_user', $post["username"]);
        $user = $this->db->get($this->_table)->row();

        // jika user terdaftar
        if($user){
          $password_en = md5($post["password"]);
            // periksa password-nya
            if($password_en == $user->password_user){ // jika password sama
                // login sukses yay!
                $array_ses = array(
                  'usr_logged' => 'true',
                  'usr_id' => $user->id_user,
                  'usr_name' => $user->nama_user,
                  'usr_level' => $user->level_user
                );
                $this->session->set_userdata($array_ses);
                $this->_updateLastLogin($user->id_user);
                return true;
            } else {
              $isi_pesan = "Password salah";
            }
        } else {
          $isi_pesan = "Username salah";
        }

        // login gagal
        $_SESSION['type'] = "danger";
        $_SESSION['judul'] = "Gagal Login";
	      $_SESSION['isi'] = $isi_pesan;
		return false;
    }

    public function isNotLogin(){
        return $this->session->userdata('usr_logged') === null;
    }

    private function _updateLastLogin($user_id){
        $sql = "UPDATE {$this->_table} SET last_login=now() WHERE id_user={$user_id}";
        $this->db->query($sql);
    }

}
