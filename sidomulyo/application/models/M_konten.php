<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_konten extends CI_Model
{

    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    private $_table = "tb_konten";

    public $id_kt;
    public $judul_kt;
    public $isi_kt;
    public $gambar_kt;
    public $jenis;

    public function rules()
    {
        return [
            ['field' => 'fjenis',
            'label' => 'Jenis',
            'rules' => 'required',
            'errors' => array(
                'required' => ' %s tidak boleh kosong'
            )]

        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {   
        return $this->db->get_where($this->_table, ["id_kt" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_kt = uniqid();
        $this->judul_kt = $post["fjudul"];
        if ($post["fisi"]=="") {
            $isinya = "-";
        }else{
            $isinya = $post["fisi"];
        }
        $this->isi_kt = $isinya;
        $this->gambar_kt = $this->_uploadImage();
        $this->jenis = $post["fjenis"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_kt = $post["fid"];
        $this->judul_kt = $post["fjudul"];
        if ($post["fisi"]=="") {
            $isinya = "-";
        }else{
            $isinya = $post["fisi"];
        }
        $this->isi_kt = $isinya;
        if (!empty($_FILES["ffoto"]["name"])) {
            $this->gambar_kt = $this->_uploadImage();
        } else {
            $this->gambar_kt = $post["ffotolama"];
        }
        $this->jenis = $post["fjenis"];
        return $this->db->update($this->_table, $this, array('id_kt' => $post['fid']));
    }

    public function delete($id)
    {   
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id_kt" => $id));
    }


    private function _uploadImage()
    {
        $config['upload_path']          = './assets/upload/konten/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $this->id_kt;
        $config['overwrite']            = true;
        $config['max_size']             = 1024 * 5; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('ffoto')) {
            return $this->upload->data("file_name");
        }
        // print_r($this->upload->display_errors());
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $konten = $this->getById($id);
        if ($konten->gambar_kt != "default.jpg") {
            $filename = explode(".", $konten->gambar_kt)[0];
            return array_map('unlink', glob(FCPATH."assets/upload/konten/$filename.*"));
        }
    }

    public function getQuery($q)
    {
        $query = $this->db->query($q);
        return $query;
    }
}
?>