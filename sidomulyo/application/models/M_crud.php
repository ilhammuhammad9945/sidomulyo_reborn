<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function simpanData($table, $fields){
    $this->db->insert($table, $fields);
    return $this->db->insert_id();
  }

  public function updateData($table, $fields, $where){
    $this->db->set($fields);
    $this->db->where($where);
    return $this->db->update($table);
  }

  public function getQuery($q){
    $query = $this->db->query($q);
    return $query;
  }

  public function deleteData($table, $where){
    $this->db->where($where);
    return $this->db->delete($table);
  }

  public function getData($table, $where=null, $order_by=null, $order=null, $limit=null){
		$this->db->from($table);
		if ($where <> null && $where <> '') {
			$this->db->where($where);
		}
		if ($order_by <> null && $order_by <> '') {
			$this->db->order_by($order_by, $order);
		}
		if ($limit <> null && $limit <> '') {
			$this->db->limit($limit);
		}
        $query = $this->db->get();

        return $query;
	}

  public function deleteSoft($table, $fields, $where){
      $this->db->set($fields);
      $this->db->where_in($where);
      return $this->db->update($table);
  }

  function update_counter($slug){
    $this->db->where('id_berita', $slug);
    $this->db->select('views');
    $count = $this->db->get('tb_berita')->row();

    $this->db->where('id_berita', $slug);
    $this->db->set('views', ($count->views + 1));
    $this->db->update('tb_berita');
  }

  function cek_ip($user_ip, $id_berita){
    	$hsl=$this->db->query("SELECT * FROM tb_log_view_berita WHERE ip = '$user_ip' AND id_berita = '$id_berita' AND DATE(tanggal)=CURDATE()");
    	return $hsl;
  }

  function simpan_user_agent($user_ip,$id_berita){
    	$hsl=$this->db->query("INSERT INTO tb_log_view_berita (ip, id_berita) VALUES('$user_ip', '$id_berita')");
    	return $hsl;
  }

  function cek_ip_visitor($user_ip){
    	$hsl = $this->db->query("SELECT * FROM tb_log_pengunjung WHERE ip_pengunjung = '$user_ip' AND DATE(tanggal_kunjungan)=CURDATE()");
    	return $hsl;
  }

  function simpan_ip_visitor ($user_ip){
    	$hsl = $this->db->query("INSERT INTO tb_log_pengunjung (ip_pengunjung) VALUES('$user_ip')");
    	return $hsl;
  }
}
