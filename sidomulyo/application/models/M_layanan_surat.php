<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_layanan_surat extends CI_Model
{

	public function viewByNik($nik)
	{
		$this->db->where('nik_pemohon', $nik);
		$result = $this->db->get('tb_sk_usaha')->row(); // Tampilkan data siswa berdasarkan NIS

		return $result;
	}

	function get_no_invoice()
	{
		$q = $this->db->query("SELECT MAX(RIGHT(id_pengajuan_sk,4)) AS kd_max FROM tb_sk_usaha WHERE DATE(created_at)=CURDATE()");
		$kd = "";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $k) {
				$tmp = ((int)$k->kd_max) + 1;
				$kd = sprintf("%04s", $tmp);
			}
		} else {
			$kd = "0001";
		}
		date_default_timezone_set('Asia/Jakarta');
		return 'US-' . date('dmy') . $kd;
	}

	function get_id_domisili()
	{
		$q = $this->db->query("SELECT MAX(RIGHT(id_pengajuan_skdomisili,4)) AS kd_max FROM tb_sk_domisili WHERE DATE(created_at)=CURDATE()");
		$kd = "";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $k) {
				$tmp = ((int)$k->kd_max) + 1;
				$kd = sprintf("%04s", $tmp);
			}
		} else {
			$kd = "0001";
		}
		date_default_timezone_set('Asia/Jakarta');
		return 'DM-' . date('dmy') . $kd;
	}

	function get_id_identitas()
	{
		$q = $this->db->query("SELECT MAX(RIGHT(id_pengajuan_identitas,4)) AS kd_max FROM tb_beda_identitas WHERE DATE(created_at)=CURDATE()");
		$kd = "";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $k) {
				$tmp = ((int)$k->kd_max) + 1;
				$kd = sprintf("%04s", $tmp);
			}
		} else {
			$kd = "0001";
		}
		date_default_timezone_set('Asia/Jakarta');
		return 'BI-' . date('dmy') . $kd;
	}

	public function get_id_peng()
	{
		$q = $this->db->query("SELECT MAX(RIGHT(id_pengajuan,4)) AS kd_max FROM tb_surat WHERE DATE(created_at)=CURDATE()");
		$kd = "";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $k) {
				$tmp = ((int)$k->kd_max) + 1;
				$kd = sprintf("%04s", $tmp);
			}
		} else {
			$kd = "0001";
		}
		date_default_timezone_set('Asia/Jakarta');
		return 'S-' . date('dmy') . $kd;
	}
}
