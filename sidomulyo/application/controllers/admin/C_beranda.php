<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_beranda extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_login");
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
	}

	public function index()
	{
		$data['title'] = "Beranda";
		$data['menu_aktif'] = 1;
		$data['submenu'] = 10;
		$data['submenu2'] = 0;
		$this->load->view('admin/V_header', $data);
		$this->load->view('admin/V_content');
		$this->load->view('admin/V_footer');
	}
}
