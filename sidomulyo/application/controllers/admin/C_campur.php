<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_campur extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["form_validation", "pdf"]);
	}


	public function printAnakMama1($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');


		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form Anak Mama 1 S-01XXXXX');

		// ngatur judul surat
		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(200, 7, 'SURAT PERNYATAAN', 0, 1, 'C');
		$pdf->Cell(10, 15, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(140, 5, 'Yang bertanda tangan / Cap Jempol jari dibawah ini saya :', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 13);
		// $query = $this->db->get_where('tb_sk_usaha', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, 'NAZRIL IRHAM ARIEL NOAH', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, 'VOKALIS BAND TERKENAL', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, 'Indonesia', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, 'Islam', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat Rumah', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(185, 7, 'Menyatakan bahwa sampai saat ini saya belum pernah mendaftarkan / melaksanakan dan atau mencatatkan perkawinan saya di catatan Sipil / Kantor Urusan Agama (KUA).', 0, 1, 'FJ', false);
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(200, 7, 'Demikian surat pernyataan saya buat dengan sebenarnya untuk keperluan pendaftaran atau', 0, 1, 'C');
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(130, 7, 'Pencatatan Kelahiran Anak saya yang ke 1 yang bernama :', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(175, 7, 'HERKULES KAO KEPLESET', 0, 1, 'C', false);
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Lahir di . JEMBER', 0, 0);
		$pdf->Cell(35, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'tanggal . 02 September 1989', 0, 1);
		$pdf->Cell(10, 2, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(176, 7, 'Di Badan Kependudukan Keluarga Berencana dan Catatan Sipil Kabupaten Jember.', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(168, 7, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(155, 7, 'Yang menyatakan', 0, 1, 'R');
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(150, 7, 'Materai 10.000', 0, 1, 'R');
		$pdf->Cell(10, 15, '', 0, 1);
		$pdf->SetX(130);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(80, 7, 'BAGUS PUTRA GALA SADEWA', 0, 1, 'L');

		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(185, 9, 'Mengetahui', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(185, 5, 'Kepala Desa / Lurah', 0, 0, 'C');
		$pdf->Cell(10, 30, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(185, 5, 'WASISO, S.IP', 0, 0, 'C');

		$pdf->Output();
	}

	public function printAnakMama2($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');


		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form Anak Mama 2 S-01XXXXX');

		// ngatur judul surat
		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(200, 7, 'LAPORAN KELAHIRAN DI LUAR NIKAH', 0, 1, 'C');
		$pdf->Cell(10, 15, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(140, 5, 'Yang bertanda tangan dibawah ini kami pemohon / pelapor :', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 13);
		// $query = $this->db->get_where('tb_sk_usaha', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, 'NAZRIL IRHAM ARIEL NOAH', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, 'VOKALIS BAND TERKENAL', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat Rumah', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(185, 7, 'Menerangkan bahwa telah lahir seorang anak laki-laki / perempuan pada :', 0, 1, 'FJ', false);
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Hari', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(27, 7, 'SELASA /', 0, 0);
		$pdf->Cell(58, 7, 'Tanggal : 31 September 2022 /', 0, 0);
		$pdf->Cell(50, 7, 'Jam : 20.00 WIB', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Lahir di', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, 'JEMBER', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Diberi Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, 'NAZRIL IRHAM ARIEL NOAH', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Anak yang ke', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(55, 7, '1', 0, 0);
		$pdf->Cell(75, 7, 'dilahirkan seorang ibu', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Nama (Ibu)', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, 'BUNGA CITRA LESTARI ANUGERAH', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, '25 Tahun', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat Rumah', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, '......................................................................................', 0, 1);

		$pdf->Cell(10, 3, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(135, 7, 'Demikian laporan kelahiran ini saya buat dengan sebenarnya.', 0, 1, 'C');


		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(168, 7, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(139, 7, 'Pemohon', 0, 1, 'R');
		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetX(130);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(80, 7, 'BAGUS PUTRA GALA SADEWA', 0, 1, 'L');

		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(68, 5, 'SAKSI I', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(103, 5, 'SAKSI II', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(75, 7, 'NAZRIL IRHAM ARIEL NOAH', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(70, 7, 'NAZRIL IRHAM ARIEL NOAH', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(75, 7, '41 TAHUN', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(70, 7, '41 TAHUN', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(75, 7, 'WIRASWASTA', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(70, 7, 'WIRASWASTA', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(75, 7, 'DUSUN KRETA RT. 01 / RW. 01', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(70, 7, 'DUSUN KRETA RT. 01 / RW. 01', 0, 1);

		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, '(..............................................................................)', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(95, 7, '(..............................................................................)', 0, 1);

		$pdf->Output();
	}

	public function printRekamKtp($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Telah Rekam KTP S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini, menerangkan dengan sebenarnya bahwa penduduk :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'NIK', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, '3509070608930001', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Karyawan Swasta', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pendidikan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'S1/D4', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Belum Kawin', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(185, 9, 'Telah melaksanakan perekaman Kartu Tanda Penduduk Elektronik (KTP-el)', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Berdasarkan Surat Edaran Menteri Dalam Negeri Nomor : 471.13/5184/SJ tanggal 13 Desember 2012, bahwa bagi penduduk yang telah melakukan perekaman KTP-el dan sebelum yang bersangkutan menerima KTP-elektronik', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Surat Keterangan ini berfungsi sebagai pengganti Kartu Tanda Penduduk Elektronik (KTP-el) Untuk Pengurusan ke BANK BTN', 0, 1, 'FJ', false);


		$pdf->Cell(10, 3, '', 0, 1);
		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(190, 7, 'Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Pemohon', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, 'BAGUS PUTRA GALA SADEWA', 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 9, 'Mengetahui,', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(185, 5, 'CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(10, 35, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(185, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(185, 5, 'NIP. 19640102 199305 1 001', 0, 0, 'C');


		$pdf->Output();
	}

	public function printHilangKk($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kehilangan KK S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT PENGANTAR KEHILANGAN KK', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'No. KK', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, '3509070608930001', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Karyawan Swasta', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Bertujuan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Polsek Semboro Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keperluan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Permohonan Surat Kehilangan Buku Rekening Bank Jatim', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat pengantar ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 7, 'BAGUS PUTRA GALA SADEWA', 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Output();
	}

	public function printHilangKtp($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kehilangan KTP S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT PENGANTAR KEHILANGAN e-KTP', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Nomor NIK / KTP', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, '3509070608930001', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Karyawan Swasta', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Bertujuan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Polsek Semboro Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keperluan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Permohonan Surat Kehilangan e-KTP', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat pengantar ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 7, 'BAGUS PUTRA GALA SADEWA', 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Output();
	}

	public function printHilangAkte($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kehilangan Akta Kelahiran S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(198, 7, 'SURAT PENGANTAR KEHILANGAN AKTA KELAHIRAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Nomor AKTA', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, '3509070608930001', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Karyawan Swasta', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Bertujuan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Polsek Semboro Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keperluan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, 'Permohonan Surat Kehilangan Akta Kelahiran', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat pengantar ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 7, 'BAGUS PUTRA GALA SADEWA', 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Output();
	}

	public function printBepergian($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Bepergian S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(207, 7, 'SURAT KETERANGAN BEPERGIAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Indonesia', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Karyawan Swasta', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pendidikan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'S1/D4', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Alamat Asal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Nomor KTP', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '7459354305935093', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Tujuan Ke', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Candi Prambanan', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Gepeng', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Glenmore', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Banyuwangi', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. Tanggal Berangkat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '14 Januari 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Alasan Bepergian', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Refreshing, Sumpek Gue', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '14. Pengikut', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '1 Orang', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(55, 6, 'NAMA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'JKEL', 1, 0, 'C');
		$pdf->Cell(20, 6, 'UMUR', 1, 0, 'C');
		$pdf->Cell(25, 6, 'STATUS', 1, 0, 'C');
		$pdf->Cell(15, 6, 'PENDI', 1, 0, 'C');
		$pdf->Cell(50, 6, 'NO. KTP', 1, 0, 'C');
		$pdf->Cell(20, 6, 'KET', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '1', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '2', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '3', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '4', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '5', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '6', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '7', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '8', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '9', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '10', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '11', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat keterangan bepergian ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Camat Semboro', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, '19640102 199305 1 001', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printPindahIkut($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Pindah S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		$image2 = "assets/frame2.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(207, 7, 'SURAT KETERANGAN PINDAH', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Indonesia', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Karyawan Swasta', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pendidikan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'S1/D4', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Alamat Asal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Nomor KTP', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '7459354305935093', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Tujuan Ke', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Candi Prambanan', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Gepeng', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Glenmore', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Banyuwangi', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. Tanggal Berangkat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '14 Januari 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Alasan Pindah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Pindah Tempat Tinggal', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '14. Pengikut', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '1 Orang', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(55, 6, 'NAMA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'JKEL', 1, 0, 'C');
		$pdf->Cell(20, 6, 'UMUR', 1, 0, 'C');
		$pdf->Cell(25, 6, 'STATUS', 1, 0, 'C');
		$pdf->Cell(15, 6, 'PENDI', 1, 0, 'C');
		$pdf->Cell(50, 6, 'NO. KTP', 1, 0, 'C');
		$pdf->Cell(20, 6, 'KET', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '1', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '2', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '3', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '4', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '5', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '6', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '7', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '8', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '9', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '10', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '11', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat keterangan pindah ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 95, 275, -120);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Camat Semboro', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, '19640102 199305 1 001', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printPindahSendiri($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Pindah S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		$image2 = "assets/frame2.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(207, 7, 'SURAT KETERANGAN PINDAH', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jember, 06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Indonesia', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Karyawan Swasta', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pendidikan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'S1/D4', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Alamat Asal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Nomor KTP', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '7459354305935093', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Tujuan Ke', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Candi Prambanan', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Gepeng', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Glenmore', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Banyuwangi', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. Tanggal Berangkat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '14 Januari 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Alasan Pindah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Pindah Tempat Tinggal', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '14. Pengikut', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '1 Orang', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(55, 6, 'NAMA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'JKEL', 1, 0, 'C');
		$pdf->Cell(20, 6, 'UMUR', 1, 0, 'C');
		$pdf->Cell(25, 6, 'STATUS', 1, 0, 'C');
		$pdf->Cell(15, 6, 'PENDI', 1, 0, 'C');
		$pdf->Cell(50, 6, 'NO. KTP', 1, 0, 'C');
		$pdf->Cell(20, 6, 'KET', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '1', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '2', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '3', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '4', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '5', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '6', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '7', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '8', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '9', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '10', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 9);
		$pdf->Cell(10, 6, '11', 1, 0, 'C');
		$pdf->Cell(55, 6, 'BAGUS PUTRA GALA SADEWA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'L', 1, 0, 'C');
		$pdf->Cell(20, 6, '20 TAHUN', 1, 0, 'C');
		$pdf->Cell(25, 6, 'BELUM KAWIN', 1, 0, 'C');
		$pdf->Cell(15, 6, 'S1/D4', 1, 0, 'C');
		$pdf->Cell(50, 6, '3509071208600005', 1, 0, 'C');
		$pdf->Cell(20, 6, '-', 1, 1, 'C');

		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat keterangan pindah ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 95, 275, -120);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Camat Semboro', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, '19640102 199305 1 001', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printLaporanSk($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('L', 'mm', 'Legal');
		$pdf->AliasNbPages();

		// membuat halaman baru
		$pdf->AddPage();

		$pdf->SetFont('Arial', 'I', 9);
		//nomor halaman
		$pdf->Cell(0, 10, 'Halaman ' . $pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Laporan Surat Keluar');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 100, 8, -600);
		$pdf->SetFont('Times', 'B', 18);
		// mencetak string 
		$pdf->Cell(370, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(370, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 20);
		$pdf->Cell(370, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(370, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 42, 350 - 10, 42);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 20);
		$pdf->Cell(368, 7, 'LAPORAN SURAT KELUAR', 0, 1, 'C');

		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(60, 6, 'TANGGAL PENGAJUAN', 1, 0, 'C');
		$pdf->Cell(50, 6, 'ID PENGAJUAN', 1, 0, 'C');
		$pdf->Cell(106, 6, 'NAMA PEMOHON', 1, 0, 'C');
		$pdf->Cell(55, 6, 'JENIS SURAT', 1, 0, 'C');
		$pdf->Cell(40, 6, 'KETERANGAN', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '1', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '2', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '3', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '4', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '5', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '6', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '7', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '8', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '9', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '10', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '11', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '12', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'Akta Kelahiran', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '13', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '14', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '15', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '16', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'Beda Identitas', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '17', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '18', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'Pelaporan Kematian', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '19', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '20', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		// membuat halaman baru
		$pdf->AddPage();
		$pdf->SetFont('Arial', 'I', 9);
		//nomor halaman
		$pdf->Cell(0, 10, 'Halaman ' . $pdf->PageNo() . ' dari {nb}', 0, 0, 'R');

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 20);
		$pdf->Cell(368, 7, 'LAPORAN SURAT KELUAR', 0, 1, 'C');

		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(60, 6, 'TANGGAL PENGAJUAN', 1, 0, 'C');
		$pdf->Cell(50, 6, 'ID PENGAJUAN', 1, 0, 'C');
		$pdf->Cell(106, 6, 'NAMA PEMOHON', 1, 0, 'C');
		$pdf->Cell(55, 6, 'JENIS SURAT', 1, 0, 'C');
		$pdf->Cell(40, 6, 'KETERANGAN', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '21', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '22', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '23', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '24', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '25', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '26', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '27', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '28', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '29', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '30', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '31', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '32', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'Akta Kelahiran', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '33', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '34', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '35', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '36', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'Beda Identitas', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '37', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '38', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'Pelaporan Kematian', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '39', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '40', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '41', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '42', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '43', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '44', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, '45', 1, 0, 'C');
		$pdf->Cell(60, 6, '23 Januari 2022', 1, 0, 'C');
		$pdf->Cell(50, 6, 'S-99103103', 1, 0, 'C');
		$pdf->Cell(106, 6, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(55, 6, 'SKCK', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Selesai', 1, 1, 'C');

		$pdf->Output();
	}

	public function printPindahBpwni($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Biodata Penduduk S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetX(22);
		$pdf->Cell(20, 5, 'No. KK', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, '3509071511800000', 0, 1);
		$pdf->SetX(22);
		$pdf->Cell(20, 5, 'NIK', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, '3509071511800000', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(198, 7, 'BIODATA PENDUDUK WARGA NEGERI INDONESIA', 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(62, 7, 'DATA PERSONAL', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama Lengkap', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, 'BAGUS GALA SADEWA PUTRA', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Tempat Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '06 Agustus 2022', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Golongan Darah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'B', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Islam', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pendidikan Terakhir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'S1/D4', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Karyawan Swasta', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Penyandang Cacat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Tidak', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Belum Kawin', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Status Hubungan dalam Keluarga', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Kepala Keluarga', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. NIK Ibu', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '3509071511800000', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Nama Ibu', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, 'Seo Dalmi', 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '14. NIK Ayah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '3509071511800000', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '15. Nama Ayah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, 'Nam Dosan ', 0, 1);

		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '16. Alamat Sebelumnya', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Dusun Kreta Lor RT. 002 / RW. 001 Desa Sidomulyo', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '17. Alamat Sekarang', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Tanggul Kulon', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Gepeng', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Glenmore', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Banyuwangi', 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, 'Jawa Timur', 0, 1);

		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'DATA KEPEMILIKAN DOKUMEN', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nomor Kartu Keluarga (No. KK)', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '3509071511800000', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '2. Nomor Paspor', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tanggal Berakhir Paspor', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '4. Nomor Akta/Surat Kenal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. No. Akta Perkawinan/Buku Nikah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Tanggal Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. No. Akta Perceraian/Surat Cerai', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Tanggal Perceraian', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, '.......................................................', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 5, 'Mengetahui', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(10, 30, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(185, 5, 'ARYANTOADI S. Sos', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 5, 'NIP. 19670422 199001 1 001', 0, 0, 'C');

		$pdf->Output();
	}

	public function printF108($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form F.1-08 S-01XXXXXX');

		// ngatur judul surat

		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(200, 5, 'SURAT KETERANGAN PINDAH DATANG WNI', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA DAERAH ASAL', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nomor Kartu Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509160908970005', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'BAGUS PUTERA GALA SADEWA', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Alamat', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(50, 5, 'Desa Pucuan', 1, 0);

		$pdf->SetX(133);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RT', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '001', 1, 0);

		$pdf->SetX(166);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RW', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '001', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, 'Sidomulyo', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(30, 5, 'Jember', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, 'Semboro', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(30, 5, 'Jawa Timur', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, '68157', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(30, 5, '089123456789', 1, 1);
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA KEPINDAHAN', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Alasan Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Pekerjaan', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. Alamat Tujuan Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(50, 5, 'Desa Pucuan Krajan', 1, 0);

		$pdf->SetX(133);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RT', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '002', 1, 0);

		$pdf->SetX(166);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RW', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '009', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Tanggul Kulon', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'Jember', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Tanggul', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'Jawa Timur', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, '68157', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, '089123456789', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Klasifikasi Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Dalam Satu Desa/Kelurahan', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '4. Jenis Kepindahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Kep. Keluarga dan Seluruh Angg. Keluarga', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '5. Status No. KK', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Tidak Ada Angg. Keluarga Yang Ditinggal', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '6. Status No. KK Bagi Yang Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Nama Kep. Keluarga Nomor KK Tetap', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '7. Rencana Tanggal Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '28 Januari 2022', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '8. Keluarga Yang Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '5 Orang', 1, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, 'No', 1, 0, 'C');
		$pdf->Cell(50, 4.2, 'Nomor Induk Kependudukan', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Nama', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'SHDK', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '1', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '2', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '3', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '4', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '5', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'Diketahui oleh :', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'Dikeluarkan oleh :', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'Camat', 0, 0, 'C');
		$pdf->Cell(65, 5, 'Pemohon', 0, 0, 'C');
		$pdf->Cell(61, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'No. ........ 22 Januari 2022', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'No. ........ 22 Januari 2022', 0, 1, 'C');
		$pdf->Cell(195, 18, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 10);
		$pdf->Cell(60, 4, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(65, 4, 'BAGUS PUTERA GALA SADEWA', 0, 0, 'C');
		$pdf->Cell(61, 4, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'NIP. 19670422 199001 1 001', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, '', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA DAERAH TUJUAN', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nomor Kartu Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509170809970006', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Bagus Putera Gala Sadewa', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. NIK Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509170809970006', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '4. Status No. KK Bagi Yang Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Nama Kep. Keluarga Nomor KK Tetap', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '5. Tanggal Kedatangan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '22 Januari 2022', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '6. Alamat', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(50, 5, 'Desa Pucuan Krajan', 1, 0);

		$pdf->SetX(133);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RT', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '002', 1, 0);

		$pdf->SetX(166);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RW', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '009', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Tanggul Kulon', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'Jember', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Tanggul', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'Jawa Timur', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '8. Keluarga Yang Datang', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '5 Orang', 1, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, 'No', 1, 0, 'C');
		$pdf->Cell(50, 4.2, 'Nomor Induk Kependudukan', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Nama', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'SHDK', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '1', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '2', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '3', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '4', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, '5', 1, 0, 'C');
		$pdf->Cell(50, 4.2, '3509150908970006', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Bagus Putera Gala Sadewa', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'Saudara Kandung', 1, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'Diketahui oleh : Camat', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'Dikeluarkan oleh : Kepala Desa Sidomulyo', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'No. ........ 22 Januari 2022', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'No. ........ 22 Januari 2022', 0, 1, 'C');
		$pdf->Cell(195, 18, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 10);
		$pdf->Cell(60, 4, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(65, 4, '', 0, 0, 'C');
		$pdf->Cell(61, 4, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'NIP. 19670422 199001 1 001', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printF103($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form F.1-03 S-01XXXXXX');
		$image2 = "assets/frame2.png";
		// ngatur judul surat

		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(200, 5, 'SURAT KETERANGAN PINDAH DATANG WNI', 0, 1, 'C');
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(200, 5, 'Formulir Biodata Penduduk', 0, 1, 'C');
		$pdf->Cell(200, 5, 'Untuk Perubahan Data / Tambahan Anggota Keluarga', 0, 1, 'C');
		$pdf->Cell(200, 5, 'Warga Negara Indonesia', 0, 1, 'C');
		$pdf->Cell(200, 6, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, '.DATA WILAYAH', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Kode-Nama Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(15, 5, '35', 1, 0, 'C');
		$pdf->SetX(90);
		$pdf->Cell(46, 5, 'Jawa Timur', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Kode-Nama Kabupaten/Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(15, 5, '09', 1, 0, 'C');
		$pdf->SetX(90);
		$pdf->Cell(46, 5, 'Jember', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Kode-Nama Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(15, 5, '07', 1, 0, 'C');
		$pdf->SetX(90);
		$pdf->Cell(46, 5, 'Semboro', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Kode-Nama Kelurahan/Desa', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(15, 5, '2003', 1, 0, 'C');
		$pdf->SetX(90);
		$pdf->Cell(46, 5, 'Sidomulyo', 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'I. DATA KELUARGA', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Bagus Putera Gala Sadewa', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. No. Kartu Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509160908970003', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Alamat Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(50, 5, 'Desa Pucuan Krajan', 1, 0);

		$pdf->SetX(133);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RT', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '002', 1, 0);

		$pdf->SetX(166);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RW', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '009', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Tanggul Kulon', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'Jember', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Tanggul', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'Jawa Timur', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, '68157', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, '089123456789', 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'II. DATA INDIVIDU', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nama Lengkap', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Bagus Putera Gala Sadewa', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. No. KTP/NIK', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509160908970003', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Alamat Sebelumnya', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(50, 5, 'Desa Pucuan Krajan', 1, 0);

		$pdf->SetX(133);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RT', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '002', 1, 0);

		$pdf->SetX(166);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 5, 'RW', 1, 0);
		$pdf->Cell(3, 5, ':', 1, 0);
		$pdf->Cell(10, 5, '009', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, '', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(116, 5, 'Desa Sidomulyo, Kecamatan Semboro, Kabupaten Jember, Prov. Jatim', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, '68157', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, '089123456789', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '4. Nomor Paspor', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '453/DFGKL/56540', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '5. Tanggal Berakhir Paspor', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '22 Januari 2022', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '6. Jenis Kelamin', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Laki-laki', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '7. Tempat Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Jember', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '8. Tanggal Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '22 Januari 2022', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '9. Umur', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '40 Tahun', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '10. Akta Kelahiran/Srt. Kenal Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(116, 5, 'ADA', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '11. No. Akta Kelahir./Srt. Kenal Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(116, 5, '453/DFGKL/56540', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '12. Golongan Darah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'AB', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '13. Agama', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Islam', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '14. Status Perkawinan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Belum Kawin', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '15. Akta Perkawin./Buku Nikah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'ADA', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '16. No. Akta Perkawin./Buku Nikah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '453/DFGKL/56540', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '17. Tanggal Perkawinan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '22 Januari 2022', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '18. Akta Percerai./Surat Cerai', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'TIDAK ADA', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '19. No. Akta Percerai./Surat Cerai', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '-', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '20. Tanggal Perceraian', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '22 Januari 2022', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '21. Status Hubungan Dalam Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Orang Tua', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '22. Kelainan Fisik dan Mental', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Tidak Ada', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '23. Penyandang Cacat', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Cacat Fisik dan Mental', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '24. Pendidikan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Akademi/Diploma III', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '25. Jenis Pekerjaan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Psikiater/Psikolog', 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA ORANG TUA', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NIK Ibu', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509160908970003', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Nama Lengkap Ibu', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Puteri Novindira Nasution', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NIK Ayah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '3509160908970003', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Nama Lengkap Ayah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'Bagus Putera Gala Sadewa', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->AddPage();
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA ADMINISTRASI', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NAMA KETUA RT', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'HARI AGUNG SETIAWAN', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NAMA KETUA RW', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, 'BAGUS PUTERA GALA SADEWA', 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'PERNYATAAN', 0, 1, 'L');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 11);
		$pdf->MultiCell(175, 5, 'Demikian formulir ini saya isi dengan sesungguhnya apabila keterangan tersebut tidak sesuai dengan keadaan yang sebenarnya saya bersedia dikenakan sanksi sesuai ketentuan perundang-undangan yang berlaku.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 95, 75, -120);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 5, 'Mengetahui ', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(78, 5, 'Kepala Desa Sidomulyo', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(96, 5, 'Pemohon', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 11);
		$pdf->Cell(78, 5, 'WASISO, S.IP', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 11);
		$pdf->Cell(95, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(78, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(95, 5, '', 0, 1, 'C');
		$pdf->Output();
	}
}
