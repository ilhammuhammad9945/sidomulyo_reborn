<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_produkhukum extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('upload');
	}

	public function perdes()
	{
		$data1['title'] = "Data Peraturan Desa";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 111;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["perdes"] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE jenis_ph = 1")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_perdes", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_perdes()
	{
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 111;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_add-perdes");
		$this->load->view("admin/V_footer");
	}

	public function save_perdes()
	{
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		//upload foto baru
		$config['upload_path']          = './assets/upload/ProdukHukum/Perdes';
		$config['allowed_types']        = 'pdf';
		// $config['max_size']             = 1024;
		// $config['file_name']             = $uuid;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = './assets/upload/ProdukHukum/Perdes' . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			// $config['maintain_ratio']= TRUE;
			// $config['quality']= '60%';
			// $config['width']= 300;
			// $config['height']= 300;
			$config['new_image'] = './assets/upload/ProdukHukum/Perdes' . $gbr['file_name'];
			// $this->load->library('image_lib', $config);
			// $this->image_lib->resize();

			//simpan data
			$fields = array(
				'jenis_ph' => 1,
				'nomor_ph' => $nomor,
				'tahun_ph' => $tahun,
				'tgl_ph' => $tanggal,
				'judul_ph' => $tentang,
				'file_ph' => $gbr['file_name'],
				'date_created' => $tgl_now
			);
		}

		$this->M_crud->simpanData('tb_produk_hukum', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('admin/C_produkhukum/perdes');
	}

	public function edit_perdes($idb)
	{
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 111;
		$data1['submenu2'] = 0;

		$data2['perdes'] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE id_ph = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_edit-perdes", $data2);
		$this->load->view("admin/V_footer");
	}

	public function proses_edit_perdes()
	{
		$id_ph = $this->input->post('id_ph');
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		$where_foto = array(
			'id_ph' => $id_ph
		);

		if (!empty($_FILES['foto'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/ProdukHukum/Perdes';
			$config['allowed_types']        = 'pdf';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/ProdukHukum/Perdes' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				// $config['maintain_ratio']= TRUE;
				// $config['quality']= '60%';
				// $config['width']= 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/ProdukHukum/Perdes' . $gbr['file_name'];
				// $this->load->library('image_lib', $config);
				// $this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id_ph'")->row_array();
				$target = "./assets/upload/ProdukHukum/Perdes/" . $hasil['file_ph'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'file_ph' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_produk_hukum", $fields_foto, $where_foto);
			}
		}

		//update data
		$fields = array(
			'jenis_ph' => 1,
			'nomor_ph' => $nomor,
			'tahun_ph' => $tahun,
			'tgl_ph' => $tanggal,
			'judul_ph' => $tentang,
			'date_updated' => $tgl_now
		);

		$this->M_crud->updateData('tb_produk_hukum', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_produkhukum/perdes');
	}

	public function hapus_perdes($id)
	{
		// hapus foto utama lama
		$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id'")->row_array();
		$target = "./assets/upload/ProdukHukum/Perdes/" . $hasil['file_ph'];
		unlink($target); // hapus data utama

		$where = array('id_ph' => $id);
		$this->M_crud->deleteData("tb_produk_hukum", $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menghapus data';
		redirect(site_url('admin/C_produkhukum/perdes'));
	}

	// -------------------------------------------------- Permakades -----------------------------------------

	public function permakades()
	{
		$data1['title'] = "Data Peraturan Bersama Kepala Desa";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 112;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["permakades"] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE jenis_ph = 2")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_permakades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_permakades()
	{
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 112;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_add-permakades");
		$this->load->view("admin/V_footer");
	}

	public function save_permakades()
	{
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		//upload foto baru
		$config['upload_path']          = './assets/upload/ProdukHukum/Permakades';
		$config['allowed_types']        = 'pdf';
		// $config['max_size']             = 1024;
		// $config['file_name']             = $uuid;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = './assets/upload/ProdukHukum/Pemakades' . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			// $config['maintain_ratio']= TRUE;
			// $config['quality']= '60%';
			// $config['width']= 300;
			// $config['height']= 300;
			$config['new_image'] = './assets/upload/ProdukHukum/Permakades' . $gbr['file_name'];
			// $this->load->library('image_lib', $config);
			// $this->image_lib->resize();

			//simpan data
			$fields = array(
				'jenis_ph' => 2,
				'nomor_ph' => $nomor,
				'tahun_ph' => $tahun,
				'tgl_ph' => $tanggal,
				'judul_ph' => $tentang,
				'file_ph' => $gbr['file_name'],
				'date_created' => $tgl_now
			);
		}

		$this->M_crud->simpanData('tb_produk_hukum', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('admin/C_produkhukum/permakades');
	}

	public function edit_permakades($idb)
	{
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 112;
		$data1['submenu2'] = 0;

		$data2['permakades'] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE id_ph = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_edit-permakades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function proses_edit_permakades()
	{
		$id_ph = $this->input->post('id_ph');
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		$where_foto = array(
			'id_ph' => $id_ph
		);

		if (!empty($_FILES['foto'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/ProdukHukum/Permakades';
			$config['allowed_types']        = 'pdf';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/ProdukHukum/Permakades' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				// $config['maintain_ratio']= TRUE;
				// $config['quality']= '60%';
				// $config['width']= 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/ProdukHukum/Permakades' . $gbr['file_name'];
				// $this->load->library('image_lib', $config);
				// $this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id_ph'")->row_array();
				$target = "./assets/upload/ProdukHukum/Permakades/" . $hasil['file_ph'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'file_ph' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_produk_hukum", $fields_foto, $where_foto);
			}
		}

		//update data
		$fields = array(
			'jenis_ph' => 2,
			'nomor_ph' => $nomor,
			'tahun_ph' => $tahun,
			'tgl_ph' => $tanggal,
			'judul_ph' => $tentang,
			'date_updated' => $tgl_now
		);

		$this->M_crud->updateData('tb_produk_hukum', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_produkhukum/permakades');
	}

	public function hapus_permakades($id)
	{
		// hapus foto utama lama
		$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id'")->row_array();
		$target = "./assets/upload/ProdukHukum/Permakades/" . $hasil['file_ph'];
		unlink($target); // hapus data utama

		$where = array('id_ph' => $id);
		$this->M_crud->deleteData("tb_produk_hukum", $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menghapus data';
		redirect(site_url('admin/C_produkhukum/permakades'));
	}

	// ------------------------------------------------------------- Perkades ---------------------------------------

	public function perkades()
	{
		$data1['title'] = "Data Peraturan Kepala Desa";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 113;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["perkades"] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE jenis_ph = 3")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_perkades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_perkades()
	{
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 113;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_add-perkades");
		$this->load->view("admin/V_footer");
	}

	public function save_perkades()
	{
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		//upload foto baru
		$config['upload_path']          = './assets/upload/ProdukHukum/Perkades';
		$config['allowed_types']        = 'pdf';
		// $config['max_size']             = 1024;
		// $config['file_name']             = $uuid;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = './assets/upload/ProdukHukum/Perkades' . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			// $config['maintain_ratio']= TRUE;
			// $config['quality']= '60%';
			// $config['width']= 300;
			// $config['height']= 300;
			$config['new_image'] = './assets/upload/ProdukHukum/Perkades' . $gbr['file_name'];
			// $this->load->library('image_lib', $config);
			// $this->image_lib->resize();

			//simpan data
			$fields = array(
				'jenis_ph' => 3,
				'nomor_ph' => $nomor,
				'tahun_ph' => $tahun,
				'tgl_ph' => $tanggal,
				'judul_ph' => $tentang,
				'file_ph' => $gbr['file_name'],
				'date_created' => $tgl_now
			);
		}

		$this->M_crud->simpanData('tb_produk_hukum', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('admin/C_produkhukum/perkades');
	}

	public function edit_perkades($idb)
	{
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 113;
		$data1['submenu2'] = 0;

		$data2['perkades'] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE id_ph = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_edit-perkades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function proses_edit_perkades()
	{
		$id_ph = $this->input->post('id_ph');
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		$where_foto = array(
			'id_ph' => $id_ph
		);

		if (!empty($_FILES['foto'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/ProdukHukum/Perkades';
			$config['allowed_types']        = 'pdf';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/ProdukHukum/Perkades' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				// $config['maintain_ratio']= TRUE;
				// $config['quality']= '60%';
				// $config['width']= 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/ProdukHukum/Perkades' . $gbr['file_name'];
				// $this->load->library('image_lib', $config);
				// $this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id_ph'")->row_array();
				$target = "./assets/upload/ProdukHukum/Perkades/" . $hasil['file_ph'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'file_ph' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_produk_hukum", $fields_foto, $where_foto);
			}
		}

		//update data
		$fields = array(
			'jenis_ph' => 3,
			'nomor_ph' => $nomor,
			'tahun_ph' => $tahun,
			'tgl_ph' => $tanggal,
			'judul_ph' => $tentang,
			'date_updated' => $tgl_now
		);

		$this->M_crud->updateData('tb_produk_hukum', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_produkhukum/perkades');
	}

	public function hapus_perkades($id)
	{
		// hapus foto utama lama
		$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id'")->row_array();
		$target = "./assets/upload/ProdukHukum/Perkades/" . $hasil['file_ph'];
		unlink($target); // hapus data utama

		$where = array('id_ph' => $id);
		$this->M_crud->deleteData("tb_produk_hukum", $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menghapus data';
		redirect(site_url('admin/C_produkhukum/perkades'));
	}

	// ------------------------------------------------------- SK kades -----------------------------------------

	public function skkades()
	{
		$data1['title'] = "Data Surat Keputusan Kepala Desa";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 114;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["skkades"] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE jenis_ph = 4")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_skkades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_skkades()
	{
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 114;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_add-skkades");
		$this->load->view("admin/V_footer");
	}

	public function save_skkades()
	{
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		//upload foto baru
		$config['upload_path']          = './assets/upload/ProdukHukum/SKkades';
		$config['allowed_types']        = 'pdf';
		// $config['max_size']             = 1024;
		// $config['file_name']             = $uuid;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = './assets/upload/ProdukHukum/SKkades' . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			// $config['maintain_ratio']= TRUE;
			// $config['quality']= '60%';
			// $config['width']= 300;
			// $config['height']= 300;
			$config['new_image'] = './assets/upload/ProdukHukum/SKkades' . $gbr['file_name'];
			// $this->load->library('image_lib', $config);
			// $this->image_lib->resize();

			//simpan data
			$fields = array(
				'jenis_ph' => 4,
				'nomor_ph' => $nomor,
				'tahun_ph' => $tahun,
				'tgl_ph' => $tanggal,
				'judul_ph' => $tentang,
				'file_ph' => $gbr['file_name'],
				'date_created' => $tgl_now
			);
		}

		$this->M_crud->simpanData('tb_produk_hukum', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('admin/C_produkhukum/skkades');
	}

	public function edit_skkades($idb)
	{
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 114;
		$data1['submenu2'] = 0;

		$data2['skkades'] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE id_ph = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_edit-skkades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function proses_edit_skkades()
	{
		$id_ph = $this->input->post('id_ph');
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		$where_foto = array(
			'id_ph' => $id_ph
		);

		if (!empty($_FILES['foto'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/ProdukHukum/SKkades';
			$config['allowed_types']        = 'pdf';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/ProdukHukum/SKkades' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				// $config['maintain_ratio']= TRUE;
				// $config['quality']= '60%';
				// $config['width']= 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/ProdukHukum/SKkades' . $gbr['file_name'];
				// $this->load->library('image_lib', $config);
				// $this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id_ph'")->row_array();
				$target = "./assets/upload/ProdukHukum/SKkades/" . $hasil['file_ph'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'file_ph' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_produk_hukum", $fields_foto, $where_foto);
			}
		}

		//update data
		$fields = array(
			'jenis_ph' => 4,
			'nomor_ph' => $nomor,
			'tahun_ph' => $tahun,
			'tgl_ph' => $tanggal,
			'judul_ph' => $tentang,
			'date_updated' => $tgl_now
		);

		$this->M_crud->updateData('tb_produk_hukum', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_produkhukum/skkades');
	}

	public function hapus_skkades($id)
	{
		// hapus foto utama lama
		$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id'")->row_array();
		$target = "./assets/upload/ProdukHukum/SKkades/" . $hasil['file_ph'];
		unlink($target); // hapus data utama

		$where = array('id_ph' => $id);
		$this->M_crud->deleteData("tb_produk_hukum", $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menghapus data';
		redirect(site_url('admin/C_produkhukum/skkades'));
	}

	// -------------------------------------------------------------- SE kades ----------------------------------------------

	public function sekades()
	{
		$data1['title'] = "Data Surat Edaran Kepala Desa";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 115;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["sekades"] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE jenis_ph = 5")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_sekades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_sekades()
	{
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 115;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_add-sekades");
		$this->load->view("admin/V_footer");
	}

	public function save_sekades()
	{
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		//upload foto baru
		$config['upload_path']          = './assets/upload/ProdukHukum/SEkades';
		$config['allowed_types']        = 'pdf';
		// $config['max_size']             = 1024;
		// $config['file_name']             = $uuid;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = './assets/upload/ProdukHukum/SEkades' . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			// $config['maintain_ratio']= TRUE;
			// $config['quality']= '60%';
			// $config['width']= 300;
			// $config['height']= 300;
			$config['new_image'] = './assets/upload/ProdukHukum/SEkades' . $gbr['file_name'];
			// $this->load->library('image_lib', $config);
			// $this->image_lib->resize();

			//simpan data
			$fields = array(
				'jenis_ph' => 5,
				'nomor_ph' => $nomor,
				'tahun_ph' => $tahun,
				'tgl_ph' => $tanggal,
				'judul_ph' => $tentang,
				'file_ph' => $gbr['file_name'],
				'date_created' => $tgl_now
			);
		}

		$this->M_crud->simpanData('tb_produk_hukum', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('admin/C_produkhukum/sekades');
	}

	public function edit_sekades($idb)
	{
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 11;
		$data1['submenu'] = 115;
		$data1['submenu2'] = 0;

		$data2['sekades'] = $this->M_crud->getQuery("SELECT * FROM tb_produk_hukum WHERE id_ph = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/produk-hukum/V_edit-sekades", $data2);
		$this->load->view("admin/V_footer");
	}

	public function proses_edit_sekades()
	{
		$id_ph = $this->input->post('id_ph');
		$nomor = $this->input->post('nomor');
		$tentang = $this->input->post('tentang');
		$tahun = $this->input->post('tahun');
		$tanggal = $this->input->post('tanggal');
		$foto = $this->input->post('foto');
		$tgl_now = date('Y-m-d H:i:s');

		$where_foto = array(
			'id_ph' => $id_ph
		);

		if (!empty($_FILES['foto'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/ProdukHukum/SEkades';
			$config['allowed_types']        = 'pdf';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/ProdukHukum/SEkades' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				// $config['maintain_ratio']= TRUE;
				// $config['quality']= '60%';
				// $config['width']= 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/ProdukHukum/SEkades' . $gbr['file_name'];
				// $this->load->library('image_lib', $config);
				// $this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id_ph'")->row_array();
				$target = "./assets/upload/ProdukHukum/SEkades/" . $hasil['file_ph'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'file_ph' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_produk_hukum", $fields_foto, $where_foto);
			}
		}

		//update data
		$fields = array(
			'jenis_ph' => 5,
			'nomor_ph' => $nomor,
			'tahun_ph' => $tahun,
			'tgl_ph' => $tanggal,
			'judul_ph' => $tentang,
			'date_updated' => $tgl_now
		);

		$this->M_crud->updateData('tb_produk_hukum', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_produkhukum/sekades');
	}

	public function hapus_sekades($id)
	{
		// hapus foto utama lama
		$hasil = $this->M_crud->getQuery("SELECT file_ph FROM tb_produk_hukum WHERE id_ph = '$id'")->row_array();
		$target = "./assets/upload/ProdukHukum/SEkades/" . $hasil['file_ph'];
		unlink($target); // hapus data utama

		$where = array('id_ph' => $id);
		$this->M_crud->deleteData("tb_produk_hukum", $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menghapus data';
		redirect(site_url('admin/C_produkhukum/sekades'));
	}
}
