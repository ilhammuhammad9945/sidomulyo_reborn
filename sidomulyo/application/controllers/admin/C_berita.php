<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_berita extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library('upload');
	}

	public function index()
	{
		$data1['title'] = "Berita";
		$data1['menu_aktif'] = 2;
		$data1['submenu'] = 20;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["berita"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user WHERE tb_berita.editor = tb_user.id_user")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/V_berita", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_berita()
	{
		$data1['title'] = "Tambah Berita";
		$data1['menu_aktif'] = 2;
		$data1['submenu'] = 20;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/V_add-berita");
		$this->load->view("admin/V_footer");
	}

	public function save_berita()
	{
		$judul = $this->input->post('judul');
		$foto = $this->input->post('foto');
		$headline = $this->input->post('headline');
		$isi = $this->input->post('isi');
		$tgl_now = date('Y-m-d H:i:s');
		$ids = $this->session->userdata("usr_id");
		if (isset($_POST['btnsimpanDraft'])) {
			$tampil = 0;
		} else {
			$tampil = 1;
		}

		//upload foto baru
		$config['upload_path']          = './assets/upload/berita';
		$config['allowed_types']        = 'jpg|jpeg|png';
		// $config['max_size']             = 1024;
		// $config['file_name']             = $uuid;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$config['encrypt_name'] = true; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if ($this->upload->do_upload('foto')) {
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = './assets/upload/berita' . $gbr['file_name'];
			$config['create_thumb'] = false;
			$config['maintain_ratio'] = true;
			$config['quality'] = '60%';
			$config['width'] = 300;
			// $config['height']= 300;
			$config['new_image'] = './assets/upload/berita' . $gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			// Build id berita
			$cek_data = $this->M_crud->getQuery("SELECT id_berita FROM `tb_berita` order by id_berita DESC limit 1");
			if ($cek_data->num_rows() == 0) {
				$kode = "B0001";
			} else {
				$data = $cek_data->row_array();
				$angka = substr($data['id_berita'], 1, 4);
				$next = $angka + 1;
				$kode = "B" . sprintf("%04s", $next);
			}

			//simpan data
			if ($tampil == 1) {
				$fields = array(
					'id_berita' => $kode,
					'judul' => $judul,
					'headline' => $headline,
					'isi' => $isi,
					'editor' => $ids,
					'gambar' => $gbr['file_name'],
					'tampil' => $tampil,
					'tgl_terbit' => $tgl_now,
					'date_created' => $tgl_now
				);
			} else {
				$fields = array(
					'id_berita' => $kode,
					'judul' => $judul,
					'headline' => $headline,
					'isi' => $isi,
					'editor' => $ids,
					'gambar' => $gbr['file_name'],
					'tampil' => $tampil,
					'date_created' => $tgl_now
				);
			}

			$this->M_crud->simpanData('tb_berita', $fields);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil menyimpan data';
		}

		redirect('admin/C_berita');
	}

	public function prosesStatusOn()
	{
		$id = $_POST['idb'];
		$now = date("Y-m-d H:i:s");
		$fields = array(
			'tampil' => 1,
			'tgl_terbit' => $now
		);
		$where = array('id_berita' => $id);
		$this->M_crud->updateData("tb_berita", $fields, $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mempublish berita';
	}

	public function prosesStatusOff()
	{
		$id = $_POST['idb'];
		$fields = array('tampil' => 0);
		$where = array('id_berita' => $id);
		$this->M_crud->updateData("tb_berita", $fields, $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menonaktifkan berita';
	}

	public function edit($idb)
	{
		$data1['title'] = "Edit Berita";
		$data1['menu_aktif'] = 2;
		$data1['submenu'] = 20;
		$data1['submenu2'] = 0;

		$data2['berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita WHERE id_berita = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/V_edit-berita", $data2);
		$this->load->view("admin/V_footer");
	}

	public function preview($url)
	{
		$data['foto'] = $url;
		$this->load->view('admin/v_preview', $data);
	}

	public function edit_berita()
	{
		$id_berita = $this->input->post('id_berita');
		$judul = $this->input->post('judul');
		$headline = $this->input->post('headline');
		$foto = $this->input->post('foto');
		$isi = $this->input->post('isi');
		$tgl_now = date('Y-m-d H:i:s');
		$ids = $this->session->userdata("usr_id");
		if (isset($_POST['btnsimpanDraft'])) {
			$tampil = 0;
		} else {
			$tampil = 1;
		}

		$where_foto = array(
			'id_berita' => $id_berita
		);

		if (!empty($_FILES['foto']['name'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/berita';
			$config['allowed_types']        = 'jpg|jpeg|png';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = true; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/berita' . $gbr['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/berita' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT gambar FROM tb_berita WHERE id_berita = '$id_berita'")->row_array();
				$target = "./assets/upload/berita/" . $hasil['gambar'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'gambar' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_berita", $fields_foto, $where_foto);
			}
		}

		//update data
		if ($tampil == 1) {
			$fields = array(
				'judul' => $judul,
				'headline' => $headline,
				'isi' => $isi,
				'tampil' => $tampil,
				'tgl_terbit' => $tgl_now,
				'date_updated' => $tgl_now
			);
		} else {
			$fields = array(
				'judul' => $judul,
				'headline' => $headline,
				'isi' => $isi,
				'tampil' => $tampil,
				'date_updated' => $tgl_now
			);
		}

		$this->M_crud->updateData('tb_berita', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_berita');
	}

	public function hapus($id)
	{
		$where = array('id_berita' => $id);
		$this->M_crud->deleteData("tb_berita", $where);
		redirect(site_url('admin/C_berita'));
	}
}
