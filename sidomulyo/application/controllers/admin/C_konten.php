<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_konten extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_konten"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data1['title'] = "Data Konten";
		$data1['menu_aktif'] = 3;
		$data1['submenu'] = 30;
		$data1['submenu2'] = 0;
		$data2["konten"] = $this->M_konten->getQuery("SELECT kt.*, jk.* FROM tb_konten AS kt JOIN `tb_jenis_konten` AS jk ON kt.jenis = jk.id_jenis")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/konten/V_konten", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah()
	{
		$konten = $this->M_konten;
		$validation = $this->form_validation;
		$validation->set_rules($konten->rules());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 3;
		$data1['submenu'] = 30;
		$data1['submenu2'] = 0;
		$data2["jenis"] = $this->M_konten->getQuery("SELECT * FROM tb_jenis_konten")->result();
		if ($validation->run()) {
			$konten->save();
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('konten-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/konten/V_tambah_konten", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect('konten-admin');

		$konten = $this->M_konten;
		$validation = $this->form_validation;
		$validation->set_rules($konten->rules());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 3;
		$data1['submenu'] = 30;
		$data1['submenu2'] = 0;

		if ($validation->run()) {
			$konten->update();
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('konten-admin');
		}
		$data["konten"] = $konten->getQuery("SELECT kt.*, jk.* FROM tb_konten AS kt JOIN `tb_jenis_konten` AS jk ON kt.jenis = jk.id_jenis WHERE kt.id_kt='" . $id . "'")->row();
		$data["jenis"] = $this->M_konten->getQuery("SELECT * FROM tb_jenis_konten")->result();
		// $data["konten"] = $konten->getById($id);
		if (!$data["konten"]) show_404();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/konten/V_edit_konten", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->M_konten->delete($id)) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('konten-admin');
		}
	}
}
