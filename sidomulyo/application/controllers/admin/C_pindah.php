<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_pindah extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library('pdf');
	}

	public function index()
	{
		$data1['title'] = "Data Pindah";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		if(isset($_SESSION['fsuratPindah'])){
			if($_SESSION['fsuratPindah'] == 'Pindah'){
				$data2['pindah'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.jenis_surat = 'pindah' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
			} else if($_SESSION['fsuratPindah'] == 'F108') {
				$data2['pindah'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f108 ON tb_surat.id_surat = tb_f108.id_surat WHERE tb_surat.jenis_surat = 'pindah-f108' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
			} else {
				$data2['pindah'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f103 ON tb_surat.id_surat = tb_f103.id_surat WHERE tb_surat.jenis_surat = 'pindah-f103' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
			}
		} else {
			$_SESSION['fsuratPindah'] = 'Pindah';
			$data2['pindah'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.jenis_surat = 'pindah' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_indexPindah", $data2);
		$this->load->view("admin/V_footer");
	}

	public function filterdataPindah()
	{
		$dataPindah = $_POST['dataPindah'];

		unset($_SESSION['fsuratPindah']);

		$_SESSION['fsuratPindah'] = $dataPindah;
	}

	public function tambahPindah()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_add-pindah", $x);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanAdd()
	{
		if (isset($_POST['btnsimpanPindah'])) {

			$id_peng = $this->input->post('id_peng');
			$fnamapemohon = $this->input->post('fnamapemohon');
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fgenderpemohon = $this->input->post('fgenderpemohon');
			$fagamapemohon = $this->input->post('fagamapemohon');
			$fstatusnikahpemohon = $this->input->post('fstatusnikahpemohon');
			$fpendidikanpemohon = $this->input->post('fpendidikanpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$fkewarganegaraanpemohon = $this->input->post('fkewarganegaraanpemohon');
			$falamatpemohon = $this->input->post('falamatpemohon');
			$fktppemohon = $this->input->post('fktppemohon');
			$ftujuanke = $this->input->post('ftujuanke');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$ftglberangkat = $this->input->post('ftglberangkat');
			$falasanpindah = $this->input->post('falasanpindah');
			$fpengikut = 0;
			$jenis_pindah = 'sendiri';

			if (isset($_POST['tambahNama'])) {
				$fpengikut = $this->input->post('fpengikut');
				$jenis_pindah = 'bersama';
			}


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'pindah',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $fktppemohon,
				'nama_pemohon' => $fnamapemohon,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'jenis_pindah' => $jenis_pindah,
				'pindah_tempat_lahir' => $ftmptlahirpemohon,
				'pindah_tanggal_lahir' => $ftgllahirpemohon,
				'pindah_gender' => $fgenderpemohon,
				'pindah_agama' => $fagamapemohon,
				'pindah_status_kawin' => $fstatusnikahpemohon,
				'pindah_pendidikan' => $fpendidikanpemohon,
				'pindah_pekerjaan' => $fpekerjaanpemohon,
				'pindah_kewarganegaraan' => $fkewarganegaraanpemohon,
				'pindah_alamat_asal' => $falamatpemohon,
				'pindah_tujuan' => $ftujuanke,
				'pindah_desa_tujuan' => $fdesa,
				'pindah_kec_tujuan' => $fkecamatan,
				'pindah_kab_tujuan' => $fkabkota,
				'pindah_provinsi_tujuan' => $fprovinsi,
				'pindah_tanggal_berangkat' => $ftglberangkat,
				'pindah_alasan' => $falasanpindah,
				'pindah_pengikut' => $fpengikut
			);
			$this->M_crud->simpanData('tb_pindah', $fields_p_akte);

			if($jenis_pindah == 'bersama'){
				// input pengikut
				$jumlah = count($_POST['nik_pengikut']);
				for ($i = 0; $i < $jumlah; $i++) {
					$nik_pengikut = $_POST['nik_pengikut'][$i];
					$nama_pengikut = $_POST['nama_pengikut'][$i];
					$gender_pengikut = $_POST['gender_pengikut'][$i];
					$umur_pengikut = $_POST['umur_pengikut'][$i];
					$status_nikah_pengikut = $_POST['status_nikah_pengikut'][$i];
					$pendidikan_pengikut = $_POST['pendidikan_pengikut'][$i];
					$keterangan_pengikut = $_POST['keterangan_pengikut'][$i];

					$fields3 = array(
						'id_surat' => $id_surat,
						'pengikut_nama' => $nama_pengikut,
						'pengikut_gender' => $gender_pengikut,
						'pengikut_umur' => $umur_pengikut,
						'pengikut_status_kawin' => $status_nikah_pengikut,
						'pengikut_pendidikan' => $pendidikan_pengikut,
						'pengikut_nik' => $nik_pengikut,
						'pengikut_keterangan' => $keterangan_pengikut
					);

					$this->M_crud->simpanData('tb_pengikut', $fields3);
				}
			}

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function editPindah($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data2['pindah'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_pindah ON tb_surat.id_surat = tb_pindah.id_surat WHERE tb_surat.jenis_surat = 'pindah' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$data2['pengikut'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = '$id'")->result();

		$data2['no'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_edit-pindah", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanEdit()
	{
		if (isset($_POST['btneditPindah'])) {

			$id_surat = $this->input->post('id_surat');
			$fnamapemohon = $this->input->post('fnamapemohon');
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fgenderpemohon = $this->input->post('fgenderpemohon');
			$fagamapemohon = $this->input->post('fagamapemohon');
			$fstatusnikahpemohon = $this->input->post('fstatusnikahpemohon');
			$fpendidikanpemohon = $this->input->post('fpendidikanpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$fkewarganegaraanpemohon = $this->input->post('fkewarganegaraanpemohon');
			$falamatpemohon = $this->input->post('falamatpemohon');
			$fktppemohon = $this->input->post('fktppemohon');
			$ftujuanke = $this->input->post('ftujuanke');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$ftglberangkat = $this->input->post('ftglberangkat');
			$falasanpindah = $this->input->post('falasanpindah');
			$fpengikut = 0;
			$jenis_pindah = 'sendiri';

			if (isset($_POST['tambahNama'])) {
				$fpengikut = $this->input->post('fpengikut');
				$jenis_pindah = 'bersama';

				echo "tambahNama";
			}

			$where = array('id_surat' => $id_surat);

			// Proses Simpan Surat
			$fields_surat = array(
				'nik_pemohon' => $fktppemohon,
				'nama_pemohon' => $fnamapemohon
			);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'jenis_pindah' => $jenis_pindah,
				'pindah_tempat_lahir' => $ftmptlahirpemohon,
				'pindah_tanggal_lahir' => $ftgllahirpemohon,
				'pindah_gender' => $fgenderpemohon,
				'pindah_agama' => $fagamapemohon,
				'pindah_status_kawin' => $fstatusnikahpemohon,
				'pindah_pendidikan' => $fpendidikanpemohon,
				'pindah_pekerjaan' => $fpekerjaanpemohon,
				'pindah_kewarganegaraan' => $fkewarganegaraanpemohon,
				'pindah_alamat_asal' => $falamatpemohon,
				'pindah_tujuan' => $ftujuanke,
				'pindah_desa_tujuan' => $fdesa,
				'pindah_kec_tujuan' => $fkecamatan,
				'pindah_kab_tujuan' => $fkabkota,
				'pindah_provinsi_tujuan' => $fprovinsi,
				'pindah_tanggal_berangkat' => $ftglberangkat,
				'pindah_alasan' => $falasanpindah,
				'pindah_pengikut' => $fpengikut
			);
			$this->M_crud->updateData('tb_pindah', $fields_p_akte, $where);

			$cekPengikut = $this->M_crud->getQuery("SELECT COUNT(id_surat) as ids FROM tb_pengikut WHERE id_surat = '$id_surat'")->row_array();
			if($cekPengikut['ids'] > 1) {
				//hapus data pengikut
				$this->M_crud->deleteData('tb_pengikut', $where);
			}

			if($jenis_pindah == 'bersama'){
				// input pengikut
				$jumlah = count($_POST['nik_pengikut']);
				for ($i = 0; $i < $jumlah; $i++) {
					$nik_pengikut = $_POST['nik_pengikut'][$i];
					$nama_pengikut = $_POST['nama_pengikut'][$i];
					$gender_pengikut = $_POST['gender_pengikut'][$i];
					$umur_pengikut = $_POST['umur_pengikut'][$i];
					$status_nikah_pengikut = $_POST['status_nikah_pengikut'][$i];
					$pendidikan_pengikut = $_POST['pendidikan_pengikut'][$i];
					$keterangan_pengikut = $_POST['keterangan_pengikut'][$i];

					$fields3 = array(
						'id_surat' => $id_surat,
						'pengikut_nama' => $nama_pengikut,
						'pengikut_gender' => $gender_pengikut,
						'pengikut_umur' => $umur_pengikut,
						'pengikut_status_kawin' => $status_nikah_pengikut,
						'pengikut_pendidikan' => $pendidikan_pengikut,
						'pengikut_nik' => $nik_pengikut,
						'pengikut_keterangan' => $keterangan_pengikut
					);

					$this->M_crud->simpanData('tb_pengikut', $fields3);
				}
			}

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function lihatPindah($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data2['pindah'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_pindah ON tb_surat.id_surat = tb_pindah.id_surat WHERE tb_surat.jenis_surat = 'pindah' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$data2['pengikut'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = '$id'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_lihat-pindah", $data2);
		$this->load->view("admin/V_footer");
	}

	public function donePindah($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		$this->session->set_flashdata('success', 'Data berhasil diupdate');
		redirect('layanan-suket-pindah-admin');
	}

	public function hapusPindah($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-suket-pindah-admin');
	}

	public function bpwni($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data2['id_surat'] = $id;
		$data2['pengikut'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE id_surat = '$id'")->result();
		$data2['no'] = 0;
		$data2['noo'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_add-bpwni", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahBpwni()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_add-bpwni", $x);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanAddBpwni()
	{
		if (isset($_POST['btnsimpanBpwni'])) {

			$jumlah = count($_POST['fnikpemohon']);
			$id_surat = count($_POST['id_surat']);
			for ($i = 0; $i < $jumlah; $i++) {
				$fid_pengikut = $_POST['fid_pengikut'][$i];
				$fnikpemohon = $_POST['fnikpemohon'][$i];
				$fnamapemohon = $_POST['fnamapemohon'][$i];
				$ftmptlahirpemohon = $_POST['ftmptlahirpemohon'][$i];
				$ftgllahirpemohon = $_POST['ftgllahirpemohon'][$i];
				$fgenderpemohon = $_POST['fgenderpemohon'][$i];
				$fgoldarahpemohon = $_POST['fgoldarahpemohon'][$i];
				$fagamapemohon = $_POST['fagamapemohon'][$i];
				$fstatusnikahpemohon = $_POST['fstatusnikahpemohon'][$i];
				$fpendidikanpemohon = $_POST['fpendidikanpemohon'][$i];
				$fpekerjaanpemohon = $_POST['fpekerjaanpemohon'][$i];
				$falamatsebelumnya = $_POST['falamatsebelumnya'][$i];
				$fcacatpemohon = $_POST['fcacatpemohon'][$i];
				$fstatushubunganpemohon = $_POST['fstatushubunganpemohon'][$i];
				$fnikibu = $_POST['fnikibu'][$i];
				$fnamaibu = $_POST['fnamaibu'][$i];
				$fnikayah = $_POST['fnikayah'][$i];
				$fnamaayah = $_POST['fnamaayah'][$i];
				$falamatsekarang = $_POST['falamatsekarang'][$i];
				$fdesa = $_POST['fdesa'][$i];
				$fkecamatan = $_POST['fkecamatan'][$i];
				$fkabkota = $_POST['fkabkota'][$i];
				$fprovinsi = $_POST['fprovinsi'][$i];
				$fnokk = $_POST['fnokk'][$i];
				$fnopaspor = $_POST['fnopaspor'][$i];
				$ftglexpiredpaspor = $_POST['ftglexpiredpaspor'][$i];
				$fnoakta = $_POST['fnoakta'][$i];
				$fbukunikah = $_POST['fbukunikah'][$i];
				$ftglkawin = $_POST['ftglkawin'][$i];
				$fnoaktaperceraian = $_POST['fnoaktaperceraian'][$i];
				$ftglperceraian = $_POST['ftglperceraian'][$i];

				// Proses simpan Form Pindah
				$fields_p_akte = array(
					'id_surat' => $id_surat,
					'id_pengikut' => $id_pengikut,
					'bpwni_no_kk' => $fnokk,
					'bpwni_tempat_lahir' => $ftmptlahirpemohon,
					'bpwni_tanggal_lahir' => $ftgllahirpemohon,
					'bpwni_gender' => $fgenderpemohon,
					'bpwni_gol_darah' => $fgoldarahpemohon,
					'bpwni_agama' => $fagamapemohon,
					'bpwni_pendidikan_terakhir' => $fpendidikanpemohon,
					'bpwni_pekerjaan' => $fpekerjaanpemohon,
					'bpwni_cacat' => $fcacatpemohon,
					'bpwni_status_kawin' => $fstatusnikahpemohon,
					'bpwni_status_hub_keluarga' => $fstatushubunganpemohon,
					'bpwni_nik_ibu' => $fnikibu,
					'bpwni_nama_ibu' => $fnamaibu,
					'bpwni_nik_ayah' => $fnikayah,
					'bpwni_nama_ayah' => $fnamaayah,
					'bpwni_alamat_lama' => $falamatsebelumnya,
					'bpwni_alamat_sekarang' => $falamatsekarang,
					'bpwni_desa_as' => $fdesa,
					'bpwni_kec_as' => $fkecamatan,
					'bpwni_kab_as' => $fkabkota,
					'bpwni_provinsi_as' => $fprovinsi,
					'bpwni_no_paspor' => $fnopaspor,
					'bpwni_tgl_exp_paspor' => $ftglexpiredpaspor,
					'bpwni_no_akta' => $fnoakta,
					'bpwni_no_perkawinan' => $fbukunikah,
					'bpwni_tgl_kawin' => $ftglkawin,
					'bpwni_no_perceraian' => $fnoaktaperceraian,
					'bpwni_tgl_cerai' => $ftglperceraian
				);
				$this->M_crud->simpanData('tb_bpwni', $fields_p_akte);

			}

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function editBpwni($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data2['bpwni'] = $this->M_crud->getQuery("SELECT * FROM tb_bpwni JOIN tb_pengikut ON tb_bpwni.id_pengikut = tb_pengikut.id_pengikut WHERE tb_bpwni.id_pengikut = '$id'")->row_array();

		$data2['no'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_edit-bpwni", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanEditBpwni()
	{
		if (isset($_POST['btneditBpwni'])) {

			$id_surat = $this->input->post('id_surat');
			$id_pengikut = $this->input->post('id_pengikut');
			$fnikpemohon = $this->input->post('fnikpemohon');
			$fnamapemohon = $this->input->post('fnamapemohon');
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fgenderpemohon = $this->input->post('fgenderpemohon');
			$fgoldarahpemohon = $this->input->post('fgoldarahpemohon');
			$fagamapemohon = $this->input->post('fagamapemohon');
			$fstatusnikahpemohon = $this->input->post('fstatusnikahpemohon');
			$fpendidikanpemohon = $this->input->post('fpendidikanpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$falamatsebelumnya = $this->input->post('falamatsebelumnya');
			$fcacatpemohon = $this->input->post('fcacatpemohon');
			$fstatushubunganpemohon = $this->input->post('fstatushubunganpemohon');
			$fnikibu = $this->input->post('fnikibu');
			$fnamaibu = $this->input->post('fnamaibu');
			$fnikayah = $this->input->post('fnikayah');
			$fnamaayah = $this->input->post('fnamaayah');
			$falamatsekarang = $this->input->post('falamatsekarang');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$fnokk = $this->input->post('fnokk');
			$fnopaspor = $this->input->post('fnopaspor');
			$ftglexpiredpaspor = $this->input->post('ftglexpiredpaspor');
			$fnoakta = $this->input->post('fnoakta');
			$fbukunikah = $this->input->post('fbukunikah');
			$ftglkawin = $this->input->post('ftglkawin');
			$fnoaktaperceraian = $this->input->post('fnoaktaperceraian');
			$ftglperceraian = $this->input->post('ftglperceraian');


			// Proses Update tb_pengikut
			$where = array(
				'id_surat' => $id_surat,
				'id_pengikut' => $id_pengikut
			);
			$fields_pengikut = array(
				'pengikut_nik' => $fnikpemohon,
				'pengikut_nama' => $fnamapemohon
			);
			$this->M_crud->updateData('tb_pengikut', $fields_pengikut, $where);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'bpwni_no_kk' => $fnokk,
				'bpwni_tempat_lahir' => $ftmptlahirpemohon,
				'bpwni_tanggal_lahir' => $ftgllahirpemohon,
				'bpwni_gender' => $fgenderpemohon,
				'bpwni_gol_darah' => $fgoldarahpemohon,
				'bpwni_agama' => $fagamapemohon,
				'bpwni_pendidikan_terakhir' => $fpendidikanpemohon,
				'bpwni_pekerjaan' => $fpekerjaanpemohon,
				'bpwni_cacat' => $fcacatpemohon,
				'bpwni_status_kawin' => $fstatusnikahpemohon,
				'bpwni_status_hub_keluarga' => $fstatushubunganpemohon,
				'bpwni_nik_ibu' => $fnikibu,
				'bpwni_nama_ibu' => $fnamaibu,
				'bpwni_nik_ayah' => $fnikayah,
				'bpwni_nama_ayah' => $fnamaayah,
				'bpwni_alamat_lama' => $falamatsebelumnya,
				'bpwni_alamat_sekarang' => $falamatsekarang,
				'bpwni_desa_as' => $fdesa,
				'bpwni_kec_as' => $fkecamatan,
				'bpwni_kab_as' => $fkabkota,
				'bpwni_provinsi_as' => $fprovinsi,
				'bpwni_no_paspor' => $fnopaspor,
				'bpwni_tgl_exp_paspor' => $ftglexpiredpaspor,
				'bpwni_no_akta' => $fnoakta,
				'bpwni_no_perkawinan' => $fbukunikah,
				'bpwni_tgl_kawin' => $ftglkawin,
				'bpwni_no_perceraian' => $fnoaktaperceraian,
				'bpwni_tgl_cerai' => $ftglperceraian
			);
			$this->M_crud->updateData('tb_bpwni', $fields_p_akte, $where);

			redirect('lihat-suket-pindah/'.$id_surat);

		}
	}

	public function lihatBpwni($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data2['bpwni'] = $this->M_crud->getQuery("SELECT * FROM tb_bpwni JOIN tb_pengikut ON tb_bpwni.id_pengikut = tb_pengikut.id_pengikut WHERE tb_bpwni.id_pengikut = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_lihat-bpwni", $data2);
		$this->load->view("admin/V_footer");
	}

	public function doneBpwni($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		$this->session->set_flashdata('success', 'Data berhasil diupdate');
		redirect('layanan-suket-pindah-admin');
	}

	public function hapusBpwni($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-suket-pindah-admin');
	}

	public function addF108()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_add-f1-08", $x);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanF108()
	{
		if (isset($_POST['btnSimpanF108'])) {

			$id_peng = $this->input->post('id_peng');
			$fnokkpemohon = $this->input->post('fnokkpemohon');
			$fnikpemohon = $this->input->post('fnikpemohon');
			$fnamakepalakeluarga = $this->input->post('fnamakepalakeluarga');
			$falamat = $this->input->post('falamat');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$fkodepos = $this->input->post('fkodepos');
			$ftelpon = $this->input->post('ftelpon');

			$falasanpindah = $this->input->post('falasanpindah');
			$falamatpindah = $this->input->post('falamatpindah');
			$fdesatujuan = $this->input->post('fdesatujuan');
			$fkecamatantujuan = $this->input->post('fkecamatantujuan');
			$fkabkotatujuan = $this->input->post('fkabkotatujuan');
			$fprovinsitujuan = $this->input->post('fprovinsitujuan');
			$fkodepostujuan = $this->input->post('fkodepostujuan');
			$ftelpontujuan = $this->input->post('ftelpontujuan');
			$fklasifikasipindah = $this->input->post('fklasifikasipindah');
			$fjenispindah = $this->input->post('fjenispindah');
			$fstatusnokk = $this->input->post('fstatusnokk');
			$fstatusnokkpindah = $this->input->post('fstatusnokkpindah');
			$ftglpindah = $this->input->post('ftglpindah');

			if (isset($_POST['tambahNama'])) {
				$fjmlkeluargapindah = $this->input->post('fjmlkeluargapindah');
				$jenis_pindah = 'bersama';
			} else {
				$fjmlkeluargapindah = 0;
				$jenis_pindah = 'sendiri';
			}

			// Proses simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'pindah-f108',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $fnikpemohon,
				'nama_pemohon' => $fnamakepalakeluarga,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'f108_no_kk' => $fnokkpemohon,
				'f108_nama_kep_kel' => $fnamakepalakeluarga,
				'f108_alamat_asal' => $falamat,
				'f108_desa_asal' => $fdesa,
				'f108_kec_asal' => $fkecamatan,
				'f108_kab_asal' => $fkabkota,
				'f108_prov_asal' => $fprovinsi,
				'f108_kode_pos_asal' => $fkodepos,
				'f108_no_telp_asal' => $ftelpon,
				'f108_alasan_pindah' => $falasanpindah,
				'f108_alamat_pindah' => $falamatpindah,
				'f108_desa_pindah' => $fdesatujuan,
				'f108_kec_pindah' => $fkecamatantujuan,
				'f108_kab_pindah' => $fkabkotatujuan,
				'f108_prov_pindah' => $fprovinsitujuan,
				'f108_kode_pos_pindah' => $fkodepostujuan,
				'f108_no_telp_pindah' => $ftelpontujuan,
				'f108_jml_keluarga_pindah' => $fjmlkeluargapindah,
				'f108_klasifikasi_pindah' => $fklasifikasipindah,
				'f108_jenis_pindah' => $fjenispindah,
				'f108_status_no_kk' => $fstatusnokk,
				'f108_status_no_kk_pindah' => $fstatusnokkpindah,
				'f108_tgl_pindah' => $ftglpindah
			);
			$this->M_crud->simpanData('tb_f108', $fields_p_akte);



			if($jenis_pindah == 'bersama'){
				// input pengikut
				$jumlah = count($_POST['nik_pengikut']);
				for ($i = 0; $i < $jumlah; $i++) {
					$nik_pengikut = $_POST['nik_pengikut'][$i];
					$nama_pengikut = $_POST['nama_pengikut'][$i];
					$shdk_pengikut = $_POST['shdk_pengikut'][$i];

					$fields3 = array(
						'id_surat' => $id_surat,
						'pengikut_nik' => $nik_pengikut,
						'pengikut_nama' => $nama_pengikut,
						'pengikut_shdk' => $shdk_pengikut
					);

					$this->M_crud->simpanData('tb_pengikut', $fields3);
				}
			}

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function editF108($id_surat)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data['f108'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f108 ON tb_surat.id_surat = tb_f108.id_surat WHERE tb_surat.id_surat = '$id_surat'")->row_array();
		$data['detail'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE id_surat = '$id_surat'")->result();
		$data['no'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_edit-f1-08", $data);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanEditF108()
	{
		if (isset($_POST['btnSimpanEditF108'])) {

			$id_surat = $this->input->post('id_surat');
			$fnokkpemohon = $this->input->post('fnokkpemohon');
			$fnikpemohon = $this->input->post('fnikpemohon');
			$fnamakepalakeluarga = $this->input->post('fnamakepalakeluarga');
			$falamat = $this->input->post('falamat');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$fkodepos = $this->input->post('fkodepos');
			$ftelpon = $this->input->post('ftelpon');

			$falasanpindah = $this->input->post('falasanpindah');
			$falamatpindah = $this->input->post('falamatpindah');
			$fdesatujuan = $this->input->post('fdesatujuan');
			$fkecamatantujuan = $this->input->post('fkecamatantujuan');
			$fkabkotatujuan = $this->input->post('fkabkotatujuan');
			$fprovinsitujuan = $this->input->post('fprovinsitujuan');
			$fkodepostujuan = $this->input->post('fkodepostujuan');
			$ftelpontujuan = $this->input->post('ftelpontujuan');
			$fklasifikasipindah = $this->input->post('fklasifikasipindah');
			$fjenispindah = $this->input->post('fjenispindah');
			$fstatusnokk = $this->input->post('fstatusnokk');
			$fstatusnokkpindah = $this->input->post('fstatusnokkpindah');
			$ftglpindah = $this->input->post('ftglpindah');

			if (isset($_POST['tambahNama'])) {
				$fjmlkeluargapindah = $this->input->post('fjmlkeluargapindah');

				$where_jml = array('id_surat' => $id_surat);
				$fields_jml_pindah = array ( 'f108_jml_keluarga_pindah' => $fjmlkeluargapindah);
				$this->M_crud->updateData('tb_f108', $fields_jml_pindah, $where_jml);

				$jenis_pindah = 'bersama';
			} else {
				$fjmlkeluargapindah = 0;
				$jenis_pindah = 'sendiri';
			}

			// Proses simpan Surat
			$fields_surat = array(
				'nik_pemohon' => $fnikpemohon,
				'nama_pemohon' => $fnamakepalakeluarga
			);
			$where = array('id_surat' => $id_surat);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'f108_no_kk' => $fnokkpemohon,
				'f108_nama_kep_kel' => $fnamakepalakeluarga,
				'f108_alamat_asal' => $falamat,
				'f108_desa_asal' => $fdesa,
				'f108_kec_asal' => $fkecamatan,
				'f108_kab_asal' => $fkabkota,
				'f108_prov_asal' => $fprovinsi,
				'f108_kode_pos_asal' => $fkodepos,
				'f108_no_telp_asal' => $ftelpon,
				'f108_alasan_pindah' => $falasanpindah,
				'f108_alamat_pindah' => $falamatpindah,
				'f108_desa_pindah' => $fdesatujuan,
				'f108_kec_pindah' => $fkecamatantujuan,
				'f108_kab_pindah' => $fkabkotatujuan,
				'f108_prov_pindah' => $fprovinsitujuan,
				'f108_kode_pos_pindah' => $fkodepostujuan,
				'f108_no_telp_pindah' => $ftelpontujuan,
				'f108_klasifikasi_pindah' => $fklasifikasipindah,
				'f108_jenis_pindah' => $fjenispindah,
				'f108_status_no_kk' => $fstatusnokk,
				'f108_status_no_kk_pindah' => $fstatusnokkpindah,
				'f108_tgl_pindah' => $ftglpindah
			);
			$this->M_crud->updateData('tb_f108', $fields_p_akte, $where);

			if($jenis_pindah == 'bersama'){
				// hapus pengikut
				$cekPengikut = $this->M_crud->getQuery("SELECT COUNT(id_surat) as ids FROM tb_pengikut WHERE id_surat = '$id_surat'")->row_array();
				if($cekPengikut['ids'] > 0) {
					//hapus data pengikut
					$this->M_crud->deleteData('tb_pengikut', $where);
				}

				// input pengikut
				$jumlah = count($_POST['nik_pengikut']);
				for ($i = 0; $i < $jumlah; $i++) {
					$nik_pengikut = $_POST['nik_pengikut'][$i];
					$nama_pengikut = $_POST['nama_pengikut'][$i];
					$shdk_pengikut = $_POST['shdk_pengikut'][$i];

					$fields3 = array(
						'id_surat' => $id_surat,
						'pengikut_nik' => $nik_pengikut,
						'pengikut_nama' => $nama_pengikut,
						'pengikut_shdk' => $shdk_pengikut
					);

					$this->M_crud->simpanData('tb_pengikut', $fields3);
				}
			}

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function lihatF108($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data['f108'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f108 ON tb_surat.id_surat = tb_f108.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();
		$data['detail'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = '$id'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_lihat-f108", $data);
		$this->load->view("admin/V_footer");
	}

	public function addF103()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_add-f1-03", $x);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanF103()
	{
		if (isset($_POST['btnSimpanF103'])) {

			$id_peng = $this->input->post('id_peng');
			$fnamaprovinsi = $this->input->post('fnamaprovinsi');
			$fnamakabupaten = $this->input->post('fnamakabupaten');
			$fnamakecamatan = $this->input->post('fnamakecamatan');
			$fnamadesa = $this->input->post('fnamadesa');

			$fnamakepalakeluarga = $this->input->post('fnamakepalakeluarga');
			$fnokkkeluarga = $this->input->post('fnokkkeluarga');
			$falamat = $this->input->post('falamat');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$fkodepos = $this->input->post('fkodepos');
			$ftelpon = $this->input->post('ftelpon');

			$find_namalengkap = $this->input->post('find_namalengkap');
			$find_noktp = $this->input->post('find_noktp');
			$find_alamatasal = $this->input->post('find_alamatasal');
			$find_desa = $this->input->post('find_desa');
			$find_kecamatan = $this->input->post('find_kecamatan');
			$find_kabupaten = $this->input->post('find_kabupaten');
			$find_provinsi = $this->input->post('find_provinsi');
			$find_kodepos = $this->input->post('find_kodepos');
			$ftelpontujuan = $this->input->post('ftelpontujuan');
			$find_no_paspor = $this->input->post('find_no_paspor');
			$find_tglberakhirpaspor = $this->input->post('find_tglberakhirpaspor');
			$find_gender = $this->input->post('find_gender');
			$find_tempatlahir = $this->input->post('find_tempatlahir');
			$find_tanggallahir = $this->input->post('find_tanggallahir');
			$find_umur = $this->input->post('find_umur');
			$find_aktalahir = $this->input->post('find_aktalahir');
			$find_noaktalahir = $this->input->post('find_noaktalahir');
			$find_goldarah = $this->input->post('find_goldarah');
			$find_agama = $this->input->post('find_agama');
			$find_statuskawin = $this->input->post('find_statuskawin');
			$find_aktakawin = $this->input->post('find_aktakawin');
			$find_noaktakawin = $this->input->post('find_noaktakawin');
			$find_tanggalkawin = $this->input->post('find_tanggalkawin');
			$find_aktacerai = $this->input->post('find_aktacerai');
			$find_noaktacerai = $this->input->post('find_noaktacerai');
			$find_tanggalcerai = $this->input->post('find_tanggalcerai');
			$find_shdk = $this->input->post('find_shdk');
			$find_cacat = $this->input->post('find_cacat');
			$find_jeniscacat = $this->input->post('find_jeniscacat');
			$find_pendidikan = $this->input->post('find_pendidikan');
			$find_pekerjaan = $this->input->post('find_pekerjaan');

			$fortu_nikibu = $this->input->post('fortu_nikibu');
			$fortu_namaibu = $this->input->post('fortu_namaibu');
			$fortu_nikayah = $this->input->post('fortu_nikayah');
			$fortu_namaayah = $this->input->post('fortu_namaayah');

			$fnamart = $this->input->post('fnamart');
			$fnamarw = $this->input->post('fnamarw');

			// Proses simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'pindah-f103',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $find_noktp,
				'nama_pemohon' => $find_namalengkap,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'wil_nama_prov' => $fnamaprovinsi,
				'wil_nama_kec' => $fnamakecamatan,
				'wil_nama_kab' => $fnamakabupaten,
				'wil_nama_desa' => $fnamadesa,
				'kel_nama_kep_kel' => $fnamakepalakeluarga,
				'kel_no_kk' => $fnokkkeluarga,
				'kel_alamat_kel' => $falamat,
				'kel_desa' => $fdesa,
				'kel_kec' => $fkecamatan,
				'kel_kab' => $fkabkota,
				'kel_prov' => $fprovinsi,
				'kel_kode_pos' => $fkodepos,
				'kel_no_telp' => $ftelpon,
				'ind_nama' => $find_namalengkap,
				'ind_nik' => $find_noktp,
				'ind_alamat' => $find_alamatasal,
				'ind_desa' => $find_desa,
				'ind_kec' => $find_kecamatan,
				'ind_kab' => $find_kabupaten,
				'ind_prov' => $find_provinsi,
				'ind_kode_pos' => $find_kodepos,
				'ind_no_telp' => $ftelpontujuan,
				'ind_no_paspor' => $find_no_paspor,
				'ind_tgl_exp_paspor' => $find_tglberakhirpaspor,
				'ind_gender' => $find_gender,
				'ind_tempat_lahir' => $find_tempatlahir,
				'ind_tanggal_lahir' => $find_tanggallahir,
				'ind_umur' => $find_umur,
				'ind_akte_lahir' => $find_aktalahir,
				'ind_no_akte_lahir' => $find_noaktalahir,
				'ind_gol_darah' => $find_goldarah,
				'ind_agama' => $find_agama,
				'ind_status_kawin' => $find_statuskawin,
				'ind_akta_kawin' => $find_aktakawin,
				'ind_no_akta_kawin' => $find_noaktakawin,
				'ind_tgl_kawin' => $find_tanggalkawin,
				'ind_akta_cerai' => $find_aktacerai,
				'ind_no_akta_cerai' => $find_noaktacerai,
				'ind_tgl_cerai' => $find_tanggalcerai,
				'ind_shdk' => $find_shdk,
				'ind_kelainan_fisik' => $find_cacat,
				'ind_cacat' => $find_jeniscacat,
				'ind_pendidikan' => $find_pendidikan,
				'ind_pekerjaan' => $find_pekerjaan,
				'ortu_nik_ibu' => $fortu_nikibu,
				'ortu_nama_ibu' => $fortu_namaibu,
				'ortu_nik_ayah' => $fortu_nikayah,
				'ortu_nama_ayah' => $fortu_namaayah,
				'adm_ketua_rt' => $fnamart,
				'adm_ketua_rw' => $fnamarw
			);
			$this->M_crud->simpanData('tb_f103', $fields_p_akte);

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function editF103($id_surat)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data['f103'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f103 ON tb_surat.id_surat = tb_f103.id_surat WHERE tb_surat.id_surat = '$id_surat'")->row_array();
		$data['no'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_edit-f1-03", $data);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanEditF103()
	{
		if (isset($_POST['btnSimpanEditF103'])) {

			$id_surat = $this->input->post('id_surat');
			$fnamaprovinsi = $this->input->post('fnamaprovinsi');
			$fnamakabupaten = $this->input->post('fnamakabupaten');
			$fnamakecamatan = $this->input->post('fnamakecamatan');
			$fnamadesa = $this->input->post('fnamadesa');

			$fnamakepalakeluarga = $this->input->post('fnamakepalakeluarga');
			$fnokkkeluarga = $this->input->post('fnokkkeluarga');
			$falamat = $this->input->post('falamat');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$fkodepos = $this->input->post('fkodepos');
			$ftelpon = $this->input->post('ftelpon');

			$find_namalengkap = $this->input->post('find_namalengkap');
			$find_noktp = $this->input->post('find_noktp');
			$find_alamatasal = $this->input->post('find_alamatasal');
			$find_desa = $this->input->post('find_desa');
			$find_kecamatan = $this->input->post('find_kecamatan');
			$find_kabupaten = $this->input->post('find_kabupaten');
			$find_provinsi = $this->input->post('find_provinsi');
			$find_kodepos = $this->input->post('find_kodepos');
			$ftelpontujuan = $this->input->post('ftelpontujuan');
			$find_no_paspor = $this->input->post('find_no_paspor');
			$find_tglberakhirpaspor = $this->input->post('find_tglberakhirpaspor');
			$find_gender = $this->input->post('find_gender');
			$find_tempatlahir = $this->input->post('find_tempatlahir');
			$find_tanggallahir = $this->input->post('find_tanggallahir');
			$find_umur = $this->input->post('find_umur');
			$find_aktalahir = $this->input->post('find_aktalahir');
			$find_noaktalahir = $this->input->post('find_noaktalahir');
			$find_goldarah = $this->input->post('find_goldarah');
			$find_agama = $this->input->post('find_agama');
			$find_statuskawin = $this->input->post('find_statuskawin');
			$find_aktakawin = $this->input->post('find_aktakawin');
			$find_noaktakawin = $this->input->post('find_noaktakawin');
			$find_tanggalkawin = $this->input->post('find_tanggalkawin');
			$find_aktacerai = $this->input->post('find_aktacerai');
			$find_noaktacerai = $this->input->post('find_noaktacerai');
			$find_tanggalcerai = $this->input->post('find_tanggalcerai');
			$find_shdk = $this->input->post('find_shdk');
			$find_cacat = $this->input->post('find_cacat');
			$find_jeniscacat = $this->input->post('find_jeniscacat');
			$find_pendidikan = $this->input->post('find_pendidikan');
			$find_pekerjaan = $this->input->post('find_pekerjaan');

			$fortu_nikibu = $this->input->post('fortu_nikibu');
			$fortu_namaibu = $this->input->post('fortu_namaibu');
			$fortu_nikayah = $this->input->post('fortu_nikayah');
			$fortu_namaayah = $this->input->post('fortu_namaayah');

			$fnamart = $this->input->post('fnamart');
			$fnamarw = $this->input->post('fnamarw');

			// Proses simpan Surat
			$fields_surat = array(
				'nik_pemohon' => $find_noktp,
				'nama_pemohon' => $find_namalengkap
			);
			$where = array('id_surat' => $id_surat);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses simpan Form Pindah
			$fields_p_akte = array(
				'wil_nama_prov' => $fnamaprovinsi,
				'wil_nama_kec' => $fnamakecamatan,
				'wil_nama_kab' => $fnamakabupaten,
				'wil_nama_desa' => $fnamadesa,
				'kel_nama_kep_kel' => $fnamakepalakeluarga,
				'kel_no_kk' => $fnokkkeluarga,
				'kel_alamat_kel' => $falamat,
				'kel_desa' => $fdesa,
				'kel_kec' => $fkecamatan,
				'kel_kab' => $fkabkota,
				'kel_prov' => $fprovinsi,
				'kel_kode_pos' => $fkodepos,
				'kel_no_telp' => $ftelpon,
				'ind_nama' => $find_namalengkap,
				'ind_nik' => $find_noktp,
				'ind_alamat' => $find_alamatasal,
				'ind_desa' => $find_desa,
				'ind_kec' => $find_kecamatan,
				'ind_kab' => $find_kabupaten,
				'ind_prov' => $find_provinsi,
				'ind_kode_pos' => $find_kodepos,
				'ind_no_telp' => $ftelpontujuan,
				'ind_no_paspor' => $find_no_paspor,
				'ind_tgl_exp_paspor' => $find_tglberakhirpaspor,
				'ind_gender' => $find_gender,
				'ind_tempat_lahir' => $find_tempatlahir,
				'ind_tanggal_lahir' => $find_tanggallahir,
				'ind_umur' => $find_umur,
				'ind_akte_lahir' => $find_aktalahir,
				'ind_no_akte_lahir' => $find_noaktalahir,
				'ind_gol_darah' => $find_goldarah,
				'ind_agama' => $find_agama,
				'ind_status_kawin' => $find_statuskawin,
				'ind_akta_kawin' => $find_aktakawin,
				'ind_no_akta_kawin' => $find_noaktakawin,
				'ind_tgl_kawin' => $find_tanggalkawin,
				'ind_akta_cerai' => $find_aktacerai,
				'ind_no_akta_cerai' => $find_noaktacerai,
				'ind_tgl_cerai' => $find_tanggalcerai,
				'ind_shdk' => $find_shdk,
				'ind_kelainan_fisik' => $find_cacat,
				'ind_cacat' => $find_jeniscacat,
				'ind_pendidikan' => $find_pendidikan,
				'ind_pekerjaan' => $find_pekerjaan,
				'ortu_nik_ibu' => $fortu_nikibu,
				'ortu_nama_ibu' => $fortu_namaibu,
				'ortu_nik_ayah' => $fortu_nikayah,
				'ortu_nama_ayah' => $fortu_namaayah,
				'adm_ketua_rt' => $fnamart,
				'adm_ketua_rw' => $fnamarw
			);
			$this->M_crud->updateData('tb_f103', $fields_p_akte, $where);

			redirect('layanan-suket-pindah-admin');

		}
	}

	public function lihatF103($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1019;

		$data['f103'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f103 ON tb_surat.id_surat = tb_f103.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pindah/V_lihat-f103", $data);
		$this->load->view("admin/V_footer");
	}

	public function printPindahIkut($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_pindah ON tb_surat.id_surat = tb_pindah.id_surat WHERE tb_surat.jenis_surat = 'pindah' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();

		$pengikut = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = $query->id_surat")->result();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Pindah [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(207, 7, 'SURAT KETERANGAN PINDAH', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		if ($query->pindah_gender == "L") {
			$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 5, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_tempat_lahir . ', ' . format_indo(date($query->pindah_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_kewarganegaraan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_status_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pendidikan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_pendidikan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Alamat Asal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_alamat_asal, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Nomor KTP', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Tujuan Ke', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_desa_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_kec_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_kab_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_provinsi_tujuan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. Tanggal Berangkat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->pindah_tanggal_berangkat)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Alasan Pindah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_alasan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '14. Pengikut', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_pengikut . ' Orang', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(5);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(55, 6, 'NAMA', 1, 0, 'C');
		$pdf->Cell(12, 6, 'JKEL', 1, 0, 'C');
		$pdf->Cell(20, 6, 'UMUR', 1, 0, 'C');
		$pdf->Cell(25, 6, 'STATUS', 1, 0, 'C');
		$pdf->Cell(15, 6, 'PENDI', 1, 0, 'C');
		$pdf->Cell(50, 6, 'NO. KTP', 1, 0, 'C');
		$pdf->Cell(20, 6, 'KET', 1, 1, 'C');

		$no = 1;
		foreach ($pengikut as $val) :
			$pdf->SetX(5);
			$pdf->SetFont('Times', '', 9);
			$pdf->Cell(10, 6, $no++, 1, 0, 'C');
			$pdf->Cell(55, 6, $val->pengikut_nama, 1, 0, 'C');
			$pdf->Cell(12, 6, $val->pengikut_gender, 1, 0, 'C');
			$pdf->Cell(20, 6, $val->pengikut_umur . ' TAHUN', 1, 0, 'C');
			$pdf->Cell(25, 6, $val->pengikut_status_kawin, 1, 0, 'C');
			$pdf->Cell(15, 6, $val->pengikut_pendidikan, 1, 0, 'C');
			$pdf->Cell(50, 6, $val->pengikut_nik, 1, 0, 'C');
			$pdf->Cell(20, 6, $val->pengikut_keterangan, 1, 1, 'C');
		endforeach;

		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur footer
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat keterangan pindah ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Camat Semboro', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, '19640102 199305 1 001', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printPindahSendiri($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_pindah ON tb_surat.id_surat = tb_pindah.id_surat WHERE tb_surat.jenis_surat = 'pindah' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Pindah [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		$image2 = "assets/frame2.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(207, 7, 'SURAT KETERANGAN PINDAH', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		if ($query->pindah_gender == "L") {
			$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 5, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_tempat_lahir . ', ' . format_indo(date($query->pindah_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_kewarganegaraan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_status_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pendidikan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_pendidikan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Alamat Asal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_alamat_asal, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Nomor KTP', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Tujuan Ke', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_desa_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_kec_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_kab_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_provinsi_tujuan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. Tanggal Berangkat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->pindah_tanggal_berangkat)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Alasan Pindah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_alasan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '14. Pengikut', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pindah_pengikut . ' Orang', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur footer
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(198, 7, 'Demikian surat keterangan pindah ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 95, 205, -120);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Camat Semboro', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, '19640102 199305 1 001', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printPindahBpwni($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_bpwni JOIN tb_pengikut ON tb_bpwni.id_pengikut = tb_pengikut.id_pengikut WHERE tb_bpwni.id_pengikut = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Biodata Penduduk S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetX(22);
		$pdf->Cell(20, 5, 'No. KK', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, $query->bpwni_no_kk, 0, 1);
		$pdf->SetX(22);
		$pdf->Cell(20, 5, 'NIK', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, $query->pengikut_nik, 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(198, 7, 'BIODATA PENDUDUK WARGA NEGERI INDONESIA', 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(62, 7, 'DATA PERSONAL', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);

		// ngatur tabel isi surat
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama Lengkap', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, $query->pengikut_nama, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Tempat Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_tempat_lahir, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->bpwni_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		if ($query->bpwni_gender == "L") {
			$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 5, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Golongan Darah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_gol_darah, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pendidikan Terakhir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_pendidikan_terakhir, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Penyandang Cacat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_cacat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_status_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Status Hubungan dalam Keluarga', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_status_hub_keluarga, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. NIK Ibu', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_nik_ibu, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Nama Ibu', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, $query->bpwni_nama_ibu, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '14. NIK Ayah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_nik_ayah, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '15. Nama Ayah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, $query->bpwni_nama_ayah, 0, 1);

		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '16. Alamat Sebelumnya', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_alamat_lama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '17. Alamat Sekarang', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_alamat_sekarang, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_desa_as, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_kec_as, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_kab_as, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_provinsi_as, 0, 1);

		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'DATA KEPEMILIKAN DOKUMEN', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nomor Kartu Keluarga (No. KK)', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_no_kk, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '2. Nomor Paspor', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_no_paspor, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tanggal Berakhir Paspor', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->bpwni_tgl_exp_paspor)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '4. Nomor Akta/Surat Kenal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_no_akta, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. No. Akta Perkawinan/Buku Nikah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_no_perkawinan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Tanggal Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->bpwni_tgl_kawin)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. No. Akta Perceraian/Surat Cerai', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bpwni_no_perceraian, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Tanggal Perceraian', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->bpwni_tgl_cerai)), 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, $query->pengikut_nama, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 5, 'Mengetahui', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(10, 30, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(185, 5, 'ARYANTOADI S. Sos', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 5, 'NIP. 19670422 199001 1 001', 0, 0, 'C');

		$pdf->Output();
	}

	public function printF108($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f108 ON tb_surat.id_surat = tb_f108.id_surat WHERE tb_surat.id_surat = '$id'")->row();
		$detail = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = '$id'")->result();
		$namakades = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='kades'")->row();
		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form F.1-08 [ ' . $id . ' ]');

		// ngatur judul surat

		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(200, 5, 'SURAT KETERANGAN PINDAH DATANG WNI', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA DAERAH ASAL', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nomor Kartu Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_no_kk, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_nama_kep_kel, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Alamat', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_alamat_asal, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, $query->f108_desa_asal, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(30, 5, $query->f108_kab_asal, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, $query->f108_kec_asal, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(30, 5, $query->f108_prov_asal, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, '-', 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(30, 5, '-', 1, 1);
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA KEPINDAHAN', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Alasan Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_alasan_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. Alamat Tujuan Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_alamat_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, $query->f108_desa_pindah, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->f108_kab_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, $query->f108_kec_pindah, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->f108_prov_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, $query->f108_kode_pos_pindah, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, '-', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Klasifikasi Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_klasifikasi_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '4. Jenis Kepindahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_jenis_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '5. Status No. KK', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_status_no_kk, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '6. Status No. KK Bagi Yang Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_status_no_kk_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '7. Rencana Tanggal Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, format_indo(date($query->f108_tgl_pindah)), 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '8. Keluarga Yang Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_jml_keluarga_pindah . ' Orang', 1, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, 'No', 1, 0, 'C');
		$pdf->Cell(50, 4.2, 'Nomor Induk Kependudukan', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Nama', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'SHDK', 1, 1, 'C');

		$no = 1;
		foreach ($detail as $val) :
			$pdf->SetX(20);
			$pdf->SetFont('Times', '', 10);
			$pdf->Cell(10, 4.2, $no++, 1, 0, 'C');
			$pdf->Cell(50, 4.2, $val->pengikut_nik, 1, 0, 'C');
			$pdf->Cell(70, 4.2, $val->pengikut_nama, 1, 0, 'C');
			$pdf->Cell(46, 4.2, $val->pengikut_shdk, 1, 1, 'C');
		endforeach;
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'Diketahui oleh :', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'Dikeluarkan oleh :', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'Camat', 0, 0, 'C');
		$pdf->Cell(65, 5, 'Pemohon', 0, 0, 'C');
		$pdf->Cell(61, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'No. ........ ' . format_indo(date('Y-m-d')), 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'No. ........ ' . format_indo(date('Y-m-d')), 0, 1, 'C');
		$pdf->Cell(195, 18, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 10);
		$pdf->Cell(60, 4, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(65, 4, $query->f108_nama_kep_kel, 0, 0, 'C');
		$pdf->Cell(61, 4, $namakades->keterangan, 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, '', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA DAERAH TUJUAN', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nomor Kartu Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_no_kk, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_nama_kep_kel, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. NIK Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->nik_pemohon, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '4. Status No. KK Bagi Yang Pindah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_status_no_kk_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '5. Tanggal Kedatangan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, '-', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '6. Alamat', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_alamat_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, $query->f108_desa_pindah, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->f108_kab_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, $query->f108_kec_pindah, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->f108_prov_pindah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '8. Keluarga Yang Datang', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->f108_jml_keluarga_pindah . ' Orang', 1, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(10, 4.2, 'No', 1, 0, 'C');
		$pdf->Cell(50, 4.2, 'Nomor Induk Kependudukan', 1, 0, 'C');
		$pdf->Cell(70, 4.2, 'Nama', 1, 0, 'C');
		$pdf->Cell(46, 4.2, 'SHDK', 1, 1, 'C');

		$no = 1;
		foreach ($detail as $val) :
			$pdf->SetX(20);
			$pdf->SetFont('Times', '', 10);
			$pdf->Cell(10, 4.2, $no++, 1, 0, 'C');
			$pdf->Cell(50, 4.2, $val->pengikut_nik, 1, 0, 'C');
			$pdf->Cell(70, 4.2, $val->pengikut_nama, 1, 0, 'C');
			$pdf->Cell(46, 4.2, $val->pengikut_shdk, 1, 1, 'C');
		endforeach;
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'Diketahui oleh : Camat', 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'Dikeluarkan oleh : Kepala Desa Sidomulyo', 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'No. ........ ' . format_indo(date('Y-m-d')), 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, 'No. ........ ' . format_indo(date('Y-m-d')), 0, 1, 'C');
		$pdf->Cell(195, 18, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 10);
		$pdf->Cell(60, 4, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(65, 4, '', 0, 0, 'C');
		$pdf->Cell(61, 4, $namakades->keterangan, 0, 1, 'C');

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(65, 5, '', 0, 0, 'C');
		$pdf->Cell(61, 5, '', 0, 1, 'C');

		$pdf->Output();
	}

	public function printF103($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$namakades = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='kades'")->row();
		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();
		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_f103 ON tb_surat.id_surat = tb_f103.id_surat WHERE tb_surat.id_surat = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form F.1-03 [ ' . $id . ' ]');
		$image2 = "assets/frame2.png";
		// ngatur judul surat

		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(200, 5, 'SURAT KETERANGAN PINDAH DATANG WNI', 0, 1, 'C');
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(200, 5, 'Formulir Biodata Penduduk', 0, 1, 'C');
		$pdf->Cell(200, 5, 'Untuk Perubahan Data / Tambahan Anggota Keluarga', 0, 1, 'C');
		$pdf->Cell(200, 5, 'Warga Negara Indonesia', 0, 1, 'C');
		$pdf->Cell(200, 6, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, '.DATA WILAYAH', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Nama Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(46, 5, $query->wil_nama_prov, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Nama Kabupaten/Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(46, 5, $query->wil_nama_kab, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Nama Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(46, 5, $query->wil_nama_kec, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '.Nama Kelurahan/Desa', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(46, 5, $query->wil_nama_desa, 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'I. DATA KELUARGA', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->kel_nama_kep_kel, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. No. Kartu Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->kel_no_kk, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Alamat Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->kel_alamat_kel, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa / Kelurahan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, $query->kel_desa, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'c. Kabupaten / Kota', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->kel_kab, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, $query->kel_kec, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'd. Propinsi', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->kel_prov, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, $query->kel_kode_pos, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, '-', 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'II. DATA INDIVIDU', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '1. Nama Lengkap', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_nama, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '2. No. KTP/NIK', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_nik, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '3. Alamat Sebelumnya', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_alamat, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(35);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, '', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(116, 5, 'Desa ' . $query->ind_desa . ', Kecamatan ' . $query->ind_kec . ', Kabupaten ' . $query->ind_kab . ', Prov. ' . $query->ind_prov, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(43);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(27, 5, 'Kode Pos', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(50, 5, $query->ind_kode_pos, 1, 0);

		$pdf->SetX(128);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'Telepon', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, '-', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '4. Nomor Paspor', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_no_paspor, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '5. Tanggal Berakhir Paspor', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, format_indo(date($query->ind_tgl_exp_paspor)), 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '6. Jenis Kelamin', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		if ($query->ind_gender == "L") {
			$pdf->Cell(116, 5, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(116, 5, 'Perempuan', 0, 1);
		}
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '7. Tempat Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_tempat_lahir, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '8. Tanggal Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, format_indo(date($query->ind_tanggal_lahir)), 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '9. Umur', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_umur . ' Tahun', 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '10. Akta Kelahiran/Srt. Kenal Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(116, 5, $query->ind_akte_lahir, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '11. No. Akta Kelahir./Srt. Kenal Lahir', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(116, 5, $query->ind_no_akte_lahir, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '12. Golongan Darah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_gol_darah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '13. Agama', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_agama, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '14. Status Perkawinan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_status_kawin, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '15. Akta Perkawin./Buku Nikah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_akta_kawin, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '16. No. Akta Perkawin./Buku Nikah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_no_akta_kawin, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '17. Tanggal Perkawinan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, format_indo(date($query->ind_tgl_kawin)), 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '18. Akta Percerai./Surat Cerai', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_akta_cerai, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '19. No. Akta Percerai./Surat Cerai', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_no_akta_cerai, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '20. Tanggal Perceraian', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, format_indo(date($query->ind_tgl_cerai)), 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '21. Status Hubungan Dalam Keluarga', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_shdk, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '22. Kelainan Fisik dan Mental', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_kelainan_fisik, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '23. Penyandang Cacat', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_cacat, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '24. Pendidikan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_pendidikan, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(55, 5, '25. Jenis Pekerjaan', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ind_pekerjaan, 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA ORANG TUA', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NIK Ibu', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ortu_nik_ibu, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Nama Lengkap Ibu', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ortu_nama_ibu, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NIK Ayah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ortu_nik_ayah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'Nama Lengkap Ayah', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->ortu_nama_ayah, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->AddPage();
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'DATA ADMINISTRASI', 0, 1, 'L');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NAMA KETUA RT', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->adm_ketua_rt, 1, 1);
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(50, 5, 'NAMA KETUA RW', 0, 0);
		$pdf->Cell(3, 5, ':', 0, 0);
		$pdf->Cell(116, 5, $query->adm_ketua_rw, 1, 1);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(40, 5, 'PERNYATAAN', 0, 1, 'L');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 11);
		$pdf->MultiCell(175, 5, 'Demikian formulir ini saya isi dengan sesungguhnya apabila keterangan tersebut tidak sesuai dengan keadaan yang sebenarnya saya bersedia dikenakan sanksi sesuai ketentuan perundang-undangan yang berlaku.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 95, 75, -120);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 5, 'Mengetahui ', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(78, 5, 'Kepala Desa Sidomulyo', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(96, 5, 'Pemohon', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 11);
		$pdf->Cell(78, 5, $namakades->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 11);
		$pdf->Cell(95, 5, $query->kel_nama_kep_kel, 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(78, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(95, 5, '', 0, 1, 'C');
		$pdf->Output();
	}

}
