<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_kader extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function rules_1()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]
		];
	}

	public function rules_2()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]
		];
	}


	public function index()
	{

		$data1['title'] = "Data Pengurus KPMD";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 81;
		$data1['submenu2'] = 0;
		$data2["kpmd"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='KPMD'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kpmd/V_kpmd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function kpm()
	{

		$data1['title'] = "Data Pengurus KPM";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 82;
		$data1['submenu2'] = 0;
		$data2["kpm"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='KPM'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kpm/V_kpm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function kader_teknik()
	{

		$data1['title'] = "Data Kader Teknik";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 83;
		$data1['submenu2'] = 0;
		$data2["teknik"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Kader Teknik'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kaderteknik/V_teknik", $data2);
		$this->load->view("admin/V_footer");
	}

	public function kader_ppkbd()
	{

		$data1['title'] = "Data Kader PPKBD";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 84;
		$data1['submenu2'] = 0;
		$data2["ppkbd"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='PPKBD' OR lm.keterangan_jabatan LIKE '%Sub%'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/ppkbd/V_ppkbd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function kader_tb()
	{

		$data1['title'] = "Data Kader TB";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 85;
		$data1['submenu2'] = 0;
		$data2["tb"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Kader TB'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kadertb/V_tb", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_kpmd()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 81;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='KPMD'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'KPMD';
			$foto = $this->_uploadImageKp($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('kpmd-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kpmd/V_tambahkpmd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_kpm()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 82;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='KPM'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'KPM';
			$foto = $this->_uploadImageKm($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('kpm-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kpm/V_tambahkpm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_teknik()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 83;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Kader Teknik'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'Kader Teknik';
			$foto = $this->_uploadImageTeknik($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('kaderteknik-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kaderteknik/V_tambahteknik", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_ppkbd()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 84;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='PPKBD'")->result();
		$data2["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. PPKBD'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			$foto = $this->_uploadImagePp($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('ppkbd-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/ppkbd/V_tambahppkbd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_tb()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 85;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='TB'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'Kader TB';
			$foto = $this->_uploadImageTb($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('kadertb-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kadertb/V_tambahtb", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit_kpmd($id = null)
	{
		if (!isset($id)) redirect('kpmd-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 81;
		$data1['submenu2'] = 0;
		$data["kpmd"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='KPMD'")->result();
		if (!$data["kpmd"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageKp($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('kpmd-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kpmd/V_editkpmd", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_kpm($id = null)
	{
		if (!isset($id)) redirect('kpm-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 82;
		$data1['submenu2'] = 0;
		$data["kpm"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='KPM'")->result();
		if (!$data["kpm"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageKm($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('kpm-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kpm/V_editkpm", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_teknik($id = null)
	{
		if (!isset($id)) redirect('kaderteknik-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 83;
		$data1['submenu2'] = 0;
		$data["teknik"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Kader Teknik'")->result();
		if (!$data["teknik"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageTeknik($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('kaderteknik-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kaderteknik/V_editteknik", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_ppkbd($id = null)
	{
		if (!isset($id)) redirect('ppkbd-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 84;
		$data1['submenu2'] = 0;
		$data["ppkbd"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='PPKBD'")->result();
		$data['keterangan'] = $this->M_crud->getQuery("SELECT * FROM `tb_organisasi` WHERE keterangan_org='Ket. PPKBD'")->result();
		if (!$data["ppkbd"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImagePp($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('ppkbd-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/ppkbd/V_editppkbd", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_tb($id = null)
	{
		if (!isset($id)) redirect('kadertb-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 8;
		$data1['submenu'] = 85;
		$data1['submenu2'] = 0;
		$data["tb"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='TB'")->result();
		if (!$data["tb"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageTb($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('kadertb-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kadertb/V_edittb", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus_kpmd($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageKp($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('kpmd-admin');
		}
	}

	public function hapus_kpm($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageKm($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('kpm-admin');
		}
	}

	public function hapus_teknik($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageTeknik($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('kaderteknik-admin');
		}
	}

	public function hapus_ppkbd($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImagePp($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('ppkbd-admin');
		}
	}

	public function hapus_tb($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageTb($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('kadertb-admin');
		}
	}

	private function _uploadImageKp($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/kpmd/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageKm($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/kpm/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageTeknik($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/kaderteknik/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImagePp($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/ppkbd/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageTb($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/kadertb/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _deleteImageKp($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/kpmd/$filename.*"));
		}
	}

	private function _deleteImageKm($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/kpm/$filename.*"));
		}
	}

	private function _deleteImageTeknik($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/kaderteknik/$filename.*"));
		}
	}

	private function _deleteImagePp($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/ppkbd/$filename.*"));
		}
	}

	private function _deleteImageTb($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/kadertb/$filename.*"));
		}
	}
}
