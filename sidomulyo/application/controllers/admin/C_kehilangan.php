<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_kehilangan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["pdf"]);
	}

	public function index()
	{
		$data1['title'] = "Data Kehilangan";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10113;

		$data2['kehilangan'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_kehilangan ON tb_surat.id_surat = tb_kehilangan.id_surat WHERE tb_surat.jenis_surat = 'kehilangan' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kehilangan/V_indexKehilangan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function addKehilanganKtp()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10113;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kehilangan/V_add-kehilangan-ktp", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanHilangKtp()
	{
		if (isset($_POST['btnSimpanHilangKtp'])) {

			$id_peng = $this->input->post('id_peng');
			$nik_pemohon = $this->input->post('nik_pemohon');
			$nama_pemohon = $this->input->post('nama_pemohon');
			$gender_pemohon = $this->input->post('gender_pemohon');
			$tempat_lahir_pemohon = $this->input->post('tempat_lahir_pemohon');
			$tanggal_lahir_pemohon = $this->input->post('tanggal_lahir_pemohon');
			$agamapemohon = $this->input->post('agama_pemohon');
			$status_kawin_pemohon = $this->input->post('status_nikah_pemohon');
			$pekerjaan_pemohon = $this->input->post('pekerjaan_pemohon');
			$alamat_pemohon = $this->input->post('alamat_pemohon');
			$tujuan = $this->input->post('tujuan_pemohon');
			$kebutuhan = $this->input->post('kebutuhan_pemohon');


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'kehilangan',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $nik_pemohon,
				'nama_pemohon' => $nama_pemohon,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'jenis_kehilangan' => 'KTP',
				'hilang_no' => $nik_pemohon,
				'hilang_gender' => $gender_pemohon,
				'hilang_tempat_lahir' => $tempat_lahir_pemohon,
				'hilang_tanggal_lahir' => $tanggal_lahir_pemohon,
				'hilang_agama' => $agamapemohon,
				'hilang_kawin' => $status_kawin_pemohon,
				'hilang_pekerjaan' => $pekerjaan_pemohon,
				'hilang_alamat' => $alamat_pemohon,
				'hilang_tujuan' => $tujuan,
				'hilang_kebutuhan' => $kebutuhan
			);
			$this->M_crud->simpanData('tb_kehilangan', $fields_p_akte);


			redirect('layanan-kehilangan-admin');
		}
	}

	public function addKehilanganKk()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10113;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kehilangan/V_add-kehilangan-kk", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanHilangKk()
	{
		if (isset($_POST['btnSimpanHilangKK'])) {

			$id_peng = $this->input->post('id_peng');
			$no_kk_pemohon = $this->input->post('no_kk_pemohon');
			$nama_pemohon = $this->input->post('nama_pemohon');
			$gender_pemohon = $this->input->post('gender_pemohon');
			$tempat_lahir_pemohon = $this->input->post('tempat_lahir_pemohon');
			$tanggal_lahir_pemohon = $this->input->post('tanggal_lahir_pemohon');
			$agamapemohon = $this->input->post('agama_pemohon');
			$status_kawin_pemohon = $this->input->post('status_nikah_pemohon');
			$pekerjaan_pemohon = $this->input->post('pekerjaan_pemohon');
			$alamat_pemohon = $this->input->post('alamat_pemohon');
			$tujuan = $this->input->post('tujuan');
			$kebutuhan = $this->input->post('kebutuhan');


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'kehilangan',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => '',
				'nama_pemohon' => $nama_pemohon,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'jenis_kehilangan' => 'KK',
				'hilang_no' => $no_kk_pemohon,
				'hilang_gender' => $gender_pemohon,
				'hilang_tempat_lahir' => $tempat_lahir_pemohon,
				'hilang_tanggal_lahir' => $tanggal_lahir_pemohon,
				'hilang_agama' => $agamapemohon,
				'hilang_kawin' => $status_kawin_pemohon,
				'hilang_pekerjaan' => $pekerjaan_pemohon,
				'hilang_alamat' => $alamat_pemohon,
				'hilang_tujuan' => $tujuan,
				'hilang_kebutuhan' => $kebutuhan
			);
			$this->M_crud->simpanData('tb_kehilangan', $fields_p_akte);


			redirect('layanan-kehilangan-admin');
		}
	}

	public function addKehilanganAkte()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10113;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kehilangan/V_add-kehilangan-akte", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanHilangAkte()
	{
		if (isset($_POST['btnSimpanHilangAkte'])) {

			$id_peng = $this->input->post('id_peng');
			$no_akta_pemohon = $this->input->post('no_akta_pemohon');
			$nama_pemohon = $this->input->post('nama_pemohon');
			$gender_pemohon = $this->input->post('gender_pemohon');
			$tempat_lahir_pemohon = $this->input->post('tempat_lahir_pemohon');
			$tanggal_lahir_pemohon = $this->input->post('tanggal_lahir_pemohon');
			$agamapemohon = $this->input->post('agamapemohon');
			$status_kawin_pemohon = $this->input->post('status_nikah_pemohon');
			$pekerjaan_pemohon = $this->input->post('pekerjaan_pemohon');
			$alamat_pemohon = $this->input->post('alamat_pemohon');
			$tujuan = $this->input->post('tujuan');
			$kebutuhan = $this->input->post('kebutuhan');


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'kehilangan',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => '',
				'nama_pemohon' => $nama_pemohon,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'jenis_kehilangan' => 'Akte',
				'hilang_no' => $no_akta_pemohon,
				'hilang_gender' => $gender_pemohon,
				'hilang_tempat_lahir' => $tempat_lahir_pemohon,
				'hilang_tanggal_lahir' => $tanggal_lahir_pemohon,
				'hilang_agama' => $agamapemohon,
				'hilang_kawin' => $status_kawin_pemohon,
				'hilang_pekerjaan' => $pekerjaan_pemohon,
				'hilang_alamat' => $alamat_pemohon,
				'hilang_tujuan' => $tujuan,
				'hilang_kebutuhan' => $kebutuhan
			);
			$this->M_crud->simpanData('tb_kehilangan', $fields_p_akte);


			redirect('layanan-kehilangan-admin');
		}
	}

	public function editKehilangan($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10113;

		$data2['kehilangan'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_kehilangan ON tb_surat.id_surat = tb_kehilangan.id_surat WHERE tb_surat.jenis_surat = 'kehilangan' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kehilangan/V_editKehilangan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesEditSKHilang()
	{
		if (isset($_POST['btnEditHilang'])) {

			$id_surat = $this->input->post('id_surat');
			$jenis_kehilangan = $this->input->post('jenis_kehilangan');
			$nama_pemohon = $this->input->post('nama_pemohon');
			$gender_pemohon = $this->input->post('gender_pemohon');
			$tempat_lahir_pemohon = $this->input->post('tempat_lahir_pemohon');
			$tanggal_lahir_pemohon = $this->input->post('tanggal_lahir_pemohon');
			$agamapemohon = $this->input->post('agama_pemohon');
			$status_kawin_pemohon = $this->input->post('status_nikah_pemohon');
			$pekerjaan_pemohon = $this->input->post('pekerjaan_pemohon');
			$alamat_pemohon = $this->input->post('alamat_pemohon');
			$tujuan = $this->input->post('tujuan_pemohon');
			$kebutuhan = $this->input->post('kebutuhan_pemohon');

			if ($jenis_kehilangan == 'KTP') {

				$nik_pemohon = $this->input->post('nik_pemohon');
				$fields_nik = array(
					'nik_pemohon' => $nik_pemohon
				);
				$where_nik = array('id_surat' => $id_surat);
				$this->M_crud->updateData('tb_surat', $fields_nik, $where_nik);
			} else {
				$hilang_no = $this->input->post('hilang_no');
				$fields_no = array(
					'hilang_no' => $hilang_no
				);
				$where_no = array('id_surat' => $id_surat);
				$this->M_crud->updateData('tb_kehilangan', $fields_no, $where_no);
			}

			$fields2 = array(
				'nama_pemohon' => $nama_pemohon
			);
			$where2 = array('id_surat' => $id_surat);
			$this->M_crud->updateData('tb_surat', $fields2, $where2);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'hilang_gender' => $gender_pemohon,
				'hilang_tempat_lahir' => $tempat_lahir_pemohon,
				'hilang_tanggal_lahir' => $tanggal_lahir_pemohon,
				'hilang_agama' => $agamapemohon,
				'hilang_kawin' => $status_kawin_pemohon,
				'hilang_pekerjaan' => $pekerjaan_pemohon,
				'hilang_alamat' => $alamat_pemohon,
				'hilang_tujuan' => $tujuan,
				'hilang_kebutuhan' => $kebutuhan
			);
			$where_p_akte = array('id_surat' => $id_surat);
			$this->M_crud->updateData('tb_kehilangan', $fields_p_akte, $where_p_akte);


			redirect('layanan-kehilangan-admin');
		}
	}

	public function lihatKehilangan($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10113;

		$data2['kehilangan'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_kehilangan ON tb_surat.id_surat = tb_kehilangan.id_surat WHERE tb_surat.jenis_surat = 'kehilangan' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kehilangan/V_lihatKehilangan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function doneKehilangan($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		$this->session->set_flashdata('success', 'Data berhasil diupdate');
		redirect('layanan-kehilangan-admin');
	}

	public function hapusKehilangan($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-kehilangan-admin');
	}

	public function printHilangAkte($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');
		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_kehilangan ON tb_surat.id_surat = tb_kehilangan.id_surat WHERE tb_surat.jenis_surat = 'kehilangan' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();
		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kehilangan Akta Kelahiran [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(198, 7, 'SURAT PENGANTAR KEHILANGAN AKTA KELAHIRAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Nomor AKTA', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_no, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_tempat_lahir . ', ' . format_indo(date($query->hilang_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		if ($query->hilang_gender == "L") {
			$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 7, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_pekerjaan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Bertujuan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_tujuan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keperluan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_kebutuhan, 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat pengantar ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 7, 'BAGUS PUTRA GALA SADEWA', 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Output();
	}

	public function printHilangKtp($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_kehilangan ON tb_surat.id_surat = tb_kehilangan.id_surat WHERE tb_surat.jenis_surat = 'kehilangan' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kehilangan KTP [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT PENGANTAR KEHILANGAN e-KTP', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Nomor NIK / KTP', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_no, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_tempat_lahir . ', ' . format_indo(date($query->hilang_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		if ($query->hilang_gender == "L") {
			$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 7, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_pekerjaan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Bertujuan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_tujuan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keperluan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_kebutuhan, 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat pengantar ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 7, $query->nama_pemohon, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Output();
	}

	public function printHilangKk($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');
		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_kehilangan ON tb_surat.id_surat = tb_kehilangan.id_surat WHERE tb_surat.jenis_surat = 'kehilangan' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kehilangan KK [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT PENGANTAR KEHILANGAN KK', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'No. KK', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_no, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_tempat_lahir . ', ' . format_indo(date($query->hilang_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		if ($query->hilang_gender == "L") {
			$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 7, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_pekerjaan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Bertujuan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_tujuan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keperluan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->hilang_kebutuhan, 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur.', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat pengantar ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(78, 5, 'Yang bersangkutan', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 7, $query->nama_pemohon, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Output();
	}
}
