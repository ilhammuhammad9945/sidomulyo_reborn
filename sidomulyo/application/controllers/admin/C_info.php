<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_info extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
	}

	public function index()
	{
		$data1['title'] = "Info";
		$data1['menu_aktif'] = 99;
		$data1['submenu'] = 0;
		$data1['submenu2'] = 0;

		$data2["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/info/V_info", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_info()
	{
		$data1['title'] = "Tambah Info";
		$data1['menu_aktif'] = 99;
		$data1['submenu'] = 0;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/info/V_add-info");
		$this->load->view("admin/V_footer");
	}

	public function save_info()
	{
		$id_kt = uniqid();
		$isi = $this->input->post('isi');

		//simpan data
		$fields = array(
			'id_kt' => $id_kt,
			'judul_kt' => '-',
			'isi_kt' => $isi,
			'gambar_kt' => '-',
			'jenis' => 99
		);

		$this->M_crud->simpanData('tb_konten', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('admin/C_info');
	}

	public function edit($idb)
	{
		$data1['title'] = "Edit Info";
		$data1['menu_aktif'] = 99;
		$data1['submenu'] = 0;
		$data1['submenu2'] = 0;

		$data2['info'] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE id_kt = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/info/V_edit-info", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit_info()
	{
		$id_kt = $this->input->post('id_kt');
		$isi = $this->input->post('isi');

		$where = array(
			'id_kt' => $id_kt
		);

		//update data
		$fields = array(
			'isi_kt' => $isi
		);

		$this->M_crud->updateData('tb_konten', $fields, $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_info');
	}

	public function hapus($id)
	{
		$where = array('id_kt' => $id);
		$this->M_crud->deleteData("tb_konten", $where);
		redirect(site_url('admin/C_info'));
	}
}
