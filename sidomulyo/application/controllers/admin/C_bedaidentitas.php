<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_bedaidentitas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}

		$this->load->library(["form_validation", "pdf"]);
	}

	public function rules()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],

			[
				'field' => 'fnokk',
				'label' => 'No. KK Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnik',
				'label' => 'NIK Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Lengkap Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat Data 1',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnokk2',
				'label' => 'No. KK Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnik2',
				'label' => 'NIK Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap2',
				'label' => 'Nama Lengkap Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel2',
				'label' => 'Jenis Kelamin Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir2',
				'label' => 'Tempat Lahir Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir2',
				'label' => 'Tanggal Lahir Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama2',
				'label' => 'Agama Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat2',
				'label' => 'Alamat Data 2',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fdatakk',
				'label' => 'Data KK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fsyarat',
				'label' => 'Syarat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

		];
	}

	public function index()
	{

		$data1['title'] = "Data Surat Keterangan Beda Identitas";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1015;
		$data2["bedaidentitas"] = $this->M_crud->getQuery("SELECT sr.*, bi.* FROM tb_surat AS sr JOIN tb_beda_identitas AS bi ON sr.id_surat=bi.id_surat WHERE sr.jenis_surat ='identitas' AND sr.aktif=1 ORDER BY sr.id_surat DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bedaidentitas/V_indexIdentitas", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ketIdentitas()
	{

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		date_default_timezone_set('Asia/Jakarta');
		// $tglskrg = date('Y-m-d');

		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1015;
		// $x['invoice'] = $this->M_layanan_surat->get_id_identitas();
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$namakades = $this->M_crud->getQuery("SELECT nama_apt FROM tb_aparatur WHERE jabatan_apt=1")->row();
		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();

		if ($validation->run()) {
			$idpengajuan = $this->input->post('fidpengajuanidentitas');
			// $nosurat = $this->input->post('fnosurat');
			$nokk = $this->input->post('fnokk');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$jkel = $this->input->post('fjkel');
			$tempatlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$agama = $this->input->post('fagama');
			$alamat = $this->input->post('falamat');

			$nokk2 = $this->input->post('fnokk2');
			$nik2 = $this->input->post('fnik2');
			$namalengkap2 = $this->input->post('fnamalengkap2');
			$jkel2 = $this->input->post('fjkel2');
			$tempatlahir2 = $this->input->post('ftempatlahir2');
			$tgllahir2 = $this->input->post('ftgllahir2');
			$agama2 = $this->input->post('fagama2');
			$alamat2 = $this->input->post('falamat2');
			$datakk = $this->input->post('fdatakk');
			$syarat = $this->input->post('fsyarat');

			$dataMaster = array(
				'jenis_surat' => 'identitas',
				// 'tgl_surat' => $tglskrg,
				// 'no_surat' => $nosurat,
				'id_pengajuan' => $idpengajuan,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $dataMaster);

			$dataTurunan = array(
				'id_surat' => $id_surat,
				'jkel_lama' => $jkel,
				'tempat_lahir_lama' => $tempatlahir,
				'tgl_lahir_lama' => $tgllahir,
				'agama_lama' => $agama,
				'no_kk_lama' => $nokk,
				'alamat_lama' => $alamat,

				'nama_baru' => $namalengkap2,
				'nik_baru' => $nik2,
				'jkel_baru' => $jkel2,
				'tempat_lahir_baru' => $tempatlahir2,
				'tgl_lahir_baru' => $tgllahir2,
				'agama_baru' => $agama2,
				'no_kk_baru' => $nokk2,
				'alamat_baru' => $alamat2,

				'datakk_dan_data' => $datakk,
				'utk_syarat' => $syarat,

				'kepala_desa' => $namakades->nama_apt,
				'nama_camat' => $namacamat->keterangan,
				'nip_camat' => $namacamat->lain_lain
			);
			$this->M_crud->simpanData('tb_beda_identitas', $dataTurunan);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('layanan-identitas-admin');

			// $cek_nosurat = $this->M_crud->getQuery("SELECT no_surat FROM tb_beda_identitas WHERE no_surat='" . $nosurat . "'")->row();
			// Kalau no surat sudah ada
			// if ($cek_nosurat > 0) 
			// 	$this->session->set_flashdata('error', 'Nomor surat sudah ada, gunakan nomor lain');
			// 	redirect('tambah-identitas');
			// 
			// Kalau no surat tidak ada, inputkan data ke tabel
			// else 
			// 
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bedaidentitas/V_ketIdentitas", $x);
		$this->load->view("admin/V_footer");
	}

	public function editKetIdentitas($id = null)
	{
		if (!isset($id)) redirect('layanan-identitas-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());

		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1015;
		$data2["bedaidentitas"] = $this->M_crud->getQuery("SELECT sr.*, bi.* FROM tb_surat AS sr JOIN tb_beda_identitas AS bi ON sr.id_surat=bi.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["bedaidentitas"]) show_404();

		if ($validation->run()) {
			$idsurat = $this->input->post('fidsurat');
			// $nosurat = $this->input->post('fnosurat');
			$nokk = $this->input->post('fnokk');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$jkel = $this->input->post('fjkel');
			$tempatlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$agama = $this->input->post('fagama');
			$alamat = $this->input->post('falamat');

			$nokk2 = $this->input->post('fnokk2');
			$nik2 = $this->input->post('fnik2');
			$namalengkap2 = $this->input->post('fnamalengkap2');
			$jkel2 = $this->input->post('fjkel2');
			$tempatlahir2 = $this->input->post('ftempatlahir2');
			$tgllahir2 = $this->input->post('ftgllahir2');
			$agama2 = $this->input->post('fagama2');
			$alamat2 = $this->input->post('falamat2');
			$datakk = $this->input->post('fdatakk');
			$syarat = $this->input->post('fsyarat');

			$dataMaster = array(
				// 'no_surat' => $nosurat,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap
			);

			$where = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_surat', $dataMaster, $where);

			$dataTurunan = array(
				'jkel_lama' => $jkel,
				'tempat_lahir_lama' => $tempatlahir,
				'tgl_lahir_lama' => $tgllahir,
				'agama_lama' => $agama,
				'no_kk_lama' => $nokk,
				'alamat_lama' => $alamat,

				'nama_baru' => $namalengkap2,
				'nik_baru' => $nik2,
				'jkel_baru' => $jkel2,
				'tempat_lahir_baru' => $tempatlahir2,
				'tgl_lahir_baru' => $tgllahir2,
				'agama_baru' => $agama2,
				'no_kk_baru' => $nokk2,
				'alamat_baru' => $alamat2,

				'datakk_dan_data' => $datakk,
				'utk_syarat' => $syarat,
			);

			$where2 = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_beda_identitas', $dataTurunan, $where2);

			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('layanan-identitas-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bedaidentitas/V_editKetIdentitas", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ubahKetIdentitas()
	{

		$idsurat = $this->input->post('fid');
		$ket = $this->input->post('fstatus_pembuatan');

		$data = array(
			'tgl_surat' => date('Y-m-d'),
			'status_pembuatan' => $ket

		);

		$where = array(
			'id_surat' => $idsurat
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Berhasil diupdate');
		redirect('layanan-identitas-admin');
	}

	public function lihatKetIdentitas($id = null)
	{
		if (!isset($id)) redirect('layanan-identitas-admin');

		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1015;
		$data2["bedaidentitas"] = $this->M_crud->getQuery("SELECT sr.*, bi.* FROM tb_surat AS sr JOIN tb_beda_identitas AS bi ON sr.id_surat=bi.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["bedaidentitas"]) show_404();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bedaidentitas/V_lihatKetIdentitas", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusKetIdentitas($id = null)
	{
		if (!isset($id)) show_404();

		// ini kodingan hapus soft
		$data = array(
			'aktif' => 0

		);

		$where = array(
			'id_surat' => $id
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		// ini kodingan hapus hard
		// $where1 = array(
		// 	'id_surat' => $id
		// );
		// $where2 = array(
		// 	'id_surat' => $id
		// );
		// $data1 = $this->M_crud->deleteData('tb_surat', $where1);
		// $data2 = $this->M_crud->deleteData('tb_beda_identitas', $where2);

		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		redirect('layanan-identitas-admin');
	}

	public function printKetIdentitas($id = null)
	{
		if (!isset($id)) redirect('layanan-identitas-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_beda_identitas', 'tb_surat.id_surat = tb_beda_identitas.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Beda Identitas [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN BEDA IDENTITAS', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(190, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(91, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 6, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_beda_identitas', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Nama Lengkap', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 6, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 6, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		if ($query->jkel_lama == "L") {
			$pdf->Cell(85, 6, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 6, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->tempat_lahir_lama . ', ' . format_indo(date($query->tgl_lahir_lama)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Agama', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->agama_lama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Data yang ada di No. KK', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->no_kk_lama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'NIK', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(95, 7, $query->alamat_lama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, '', 0, 1);

		// bagian data 2
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Nama Lengkap', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 6, strtoupper($query->nama_baru), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 6, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		if ($query->jkel_baru == "L") {
			$pdf->Cell(85, 6, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 6, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->tempat_lahir_baru . ', ' . format_indo(date($query->tgl_lahir_baru)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Agama', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->agama_baru, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'Data yang ada di No. KK', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->no_kk_baru, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 6, 'NIK', 0, 0);
		$pdf->Cell(5, 6, ':', 0, 0);
		$pdf->Cell(85, 6, $query->nik_baru, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(95, 7, $query->alamat_baru, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 6, '', 0, 0);
		$pdf->Cell(5, 6, '', 0, 0);
		$pdf->Cell(85, 6, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(186, 7, 'Yang bersangkutan adalah benar-benar penduduk Desa Sidomulyo yang beralamat seperti tersebut', 0, 1, 'C');
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$cell = 'di atas dan bahwa berdasarkan data KK dan data ';
		$pdf->Cell($pdf->GetStringWidth($cell), 8, $cell, 0, 'C');
		$pdf->SetFont('Times', 'B', 12);
		$boldCell = $query->datakk_dan_data . ' yang berbeda tersebut ';
		$pdf->Cell($pdf->GetStringWidth($boldCell), 8, $boldCell, 0, 'C');
		$pdf->SetFont('Times', '', 12);
		$cell = 'di atas data (1) ';
		$pdf->Cell($pdf->GetStringWidth($cell), 8, $cell, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(77, 7, 'data (2) adalah Orang yang sama.', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$cell = 'Surat keterangan ini dipergunakan yang bersangkutan untuk melengkapi persyaratan ';
		$pdf->Cell($pdf->GetStringWidth($cell), 8, $cell, 0, 'C');
		$pdf->SetFont('Times', 'B', 12);
		$boldCell = $query->utk_syarat . '.';
		$pdf->Cell($pdf->GetStringWidth($boldCell), 8, $boldCell, 0, 'C');
		$pdf->Ln();
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(192, 7, 'Demikian surat keterangan ini di buat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, strtoupper($query->kepala_desa), 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}
}
