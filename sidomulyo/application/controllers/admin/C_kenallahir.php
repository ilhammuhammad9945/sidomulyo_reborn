<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_kenallahir extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["form_validation", "pdf"]);
	}

	public function rules()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],
			[
				'field' => 'fnik',
				'label' => 'NIK Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamakenal',
				'label' => 'Nama Terlapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fwni',
				'label' => 'Kewarganegaraan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnokk',
				'label' => 'No. KK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnik2',
				'label' => 'NIK Terlapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpendidikan',
				'label' => 'Pendidikan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamaayah',
				'label' => 'Nama Ayah',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamaibu',
				'label' => 'Nama Ibu',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fanakke',
				'label' => 'Anak Ke',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],


		];
	}

	public function rules2()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],
			[
				'field' => 'fnik',
				'label' => 'NIK Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamakenal',
				'label' => 'Nama Terlapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnokk',
				'label' => 'No. KK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnik2',
				'label' => 'NIK Terlapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpendidikan',
				'label' => 'Pendidikan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamaayah',
				'label' => 'Nama Ayah',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamaibu',
				'label' => 'Nama Ibu',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fanakke',
				'label' => 'Anak Ke',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],


		];
	}

	public function index()
	{

		$data1['title'] = "Data Kenal Lahir";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10115;
		$data2["kenal"] = $this->M_crud->getQuery("SELECT sr.*, kl.* FROM tb_surat AS sr JOIN tb_kenal_lahir AS kl ON sr.id_surat=kl.id_surat WHERE sr.jenis_surat ='kenal lahir' AND sr.aktif=1 ORDER BY sr.id_surat DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kenallahir/V_indexKenal", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahKenal()
	{

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		date_default_timezone_set('Asia/Jakarta');
		// $tglskrg = date('Y-m-d');

		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10115;
		// $x['invoice'] = $this->M_layanan_surat->get_id_domisili();
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$namakades = $this->M_crud->getQuery("SELECT nama_apt FROM tb_aparatur WHERE jabatan_apt=1")->row();

		if ($validation->run()) {
			$idpengajuan = $this->input->post('fidpengajuankenal');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$namakenal = $this->input->post('fnamakenal');
			$jkel = $this->input->post('fjkel');
			$kwn = $this->input->post('fnamanegara');
			$agama = $this->input->post('fagama');
			$statuskawin = $this->input->post('fstatuskawin');
			$tmptlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$nokk = $this->input->post('fnokk');
			$nik2 = $this->input->post('fnik2');
			$pekerjaan = $this->input->post('fpekerjaan');
			$pendidikan = $this->input->post('fpendidikan');
			$ayah = $this->input->post('fnamaayah');
			$ibu = $this->input->post('fnamaibu');
			$anakke = $this->input->post('fanakke');
			$alamat = $this->input->post('falamat');
			$ket = $this->input->post('fket');

			$dataMaster = array(
				'jenis_surat' => 'kenal lahir',
				// 'tgl_surat' => $tglskrg,
				// 'no_surat' => $nosurat,
				'id_pengajuan' => $idpengajuan,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $dataMaster);

			$dataTurunan = array(
				'id_surat' => $id_surat,
				'knl_anak' => $namakenal,
				'knl_jkel' => $jkel,
				'knl_kwn' => $kwn,
				'knl_agama' => $agama,
				'knl_status_kwn' => $statuskawin,
				'knl_tempat_lahir' => $tmptlahir,
				'knl_tgl_lahir' => $tgllahir,
				'knl_nokk' => $nokk,
				'knl_nik' => $nik2,
				'knl_pekerjaan' => $pekerjaan,
				'knl_pendidikan' => $pendidikan,
				'knl_ayah' => $ayah,
				'knl_ibu' => $ibu,
				'knl_anakke' => $anakke,
				'knl_alamat' => $alamat,
				'knl_keterangan' => $ket,
				'knl_kades' => $namakades->nama_apt,
			);
			$this->M_crud->simpanData('tb_kenal_lahir', $dataTurunan);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('layanan-kenal-lahir-admin');

			// $cek_nosurat = $this->M_crud->getQuery("SELECT no_surat FROM tb_sk_domisili WHERE no_surat='" . $nosurat . "'")->row();
			// Kalau no surat sudah ada
			// if ($cek_nosurat > 0) 
			// 	$this->session->set_flashdata('error', 'Nomor surat sudah ada, gunakan nomor lain');
			// 	redirect('tambah-domisili');
			// 
			// Kalau no surat tidak ada, inputkan data ke tabel
			// else 


			// 
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kenallahir/V_tambahKenal", $x);
		$this->load->view("admin/V_footer");
	}

	public function editKenal($id = null)
	{
		if (!isset($id)) redirect('layanan-kenal-lahir-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules2());

		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10115;

		$data2["kenal"] = $this->M_crud->getQuery("SELECT sr.*, kl.* FROM tb_surat AS sr JOIN tb_kenal_lahir AS kl ON sr.id_surat=kl.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["kenal"]) show_404();

		if ($validation->run()) {
			$idsurat = $this->input->post('fidsurat');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$namakenal = $this->input->post('fnamakenal');
			$jkel = $this->input->post('fjkel');
			$kwn = $this->input->post('fnamanegara');
			$agama = $this->input->post('fagama');
			$statuskawin = $this->input->post('fstatuskawin');
			$tmptlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$nokk = $this->input->post('fnokk');
			$nik2 = $this->input->post('fnik2');
			$pekerjaan = $this->input->post('fpekerjaan');
			$pendidikan = $this->input->post('fpendidikan');
			$ayah = $this->input->post('fnamaayah');
			$ibu = $this->input->post('fnamaibu');
			$anakke = $this->input->post('fanakke');
			$alamat = $this->input->post('falamat');
			$ket = $this->input->post('fket');

			$dataMaster = array(
				// 'no_surat' => $nosurat,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap
			);

			$where = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_surat', $dataMaster, $where);

			$dataTurunan = array(
				'knl_anak' => $namakenal,
				'knl_jkel' => $jkel,
				'knl_kwn' => $kwn,
				'knl_agama' => $agama,
				'knl_status_kwn' => $statuskawin,
				'knl_tempat_lahir' => $tmptlahir,
				'knl_tgl_lahir' => $tgllahir,
				'knl_nokk' => $nokk,
				'knl_nik' => $nik2,
				'knl_pekerjaan' => $pekerjaan,
				'knl_pendidikan' => $pendidikan,
				'knl_ayah' => $ayah,
				'knl_ibu' => $ibu,
				'knl_anakke' => $anakke,
				'knl_alamat' => $alamat,
				'knl_keterangan' => $ket,
			);

			$where2 = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_kenal_lahir', $dataTurunan, $where2);

			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('layanan-kenal-lahir-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kenallahir/V_editKenal", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ubahKenal()
	{

		$idsurat = $this->input->post('fid');
		$ket = $this->input->post('fstatus_pembuatan');

		$data = array(
			'tgl_surat' => date('Y-m-d'),
			'status_pembuatan' => $ket

		);

		$where = array(
			'id_surat' => $idsurat
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Berhasil diupdate');
		redirect('layanan-kenal-lahir-admin');
	}

	public function lihatKenal($id = null)
	{
		if (!isset($id)) redirect('layanan-kenal-lahir-admin');

		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10115;
		$data2["kenal"] = $this->M_crud->getQuery("SELECT sr.*, kl.* FROM tb_surat AS sr JOIN tb_kenal_lahir AS kl ON sr.id_surat=kl.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["kenal"]) show_404();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kenallahir/V_lihatKenal", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusKenal($id = null)
	{
		if (!isset($id)) show_404();

		// ini kodingan hapus soft
		$data = array(
			'aktif' => 0
		);

		$where = array(
			'id_surat' => $id
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		redirect('layanan-kenal-lahir-admin');
	}

	public function printKenal($id = null)
	{
		if (!isset($id)) redirect('layanan-kenal-lahir-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_kenal_lahir', 'tb_surat.id_surat = tb_kenal_lahir.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Kenal Lahir [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";

		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN KENAL LAHIR', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(80, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama Lengkap', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->knl_anak), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->knl_jkel == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}

		$day = date('D', strtotime($query->knl_tgl_lahir));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Hari', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $dayList[$day], 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_tempat_lahir . ', ' . format_indo(date($query->knl_tgl_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_kwn, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Agama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_status_kwn, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pendidikan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_pendidikan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'No. KK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_nokk, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nomor KTP', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_nik, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama Ayah', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_ayah, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama Ibu', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_ibu, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Anak Ke', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->knl_anakke, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->knl_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 1, '', 0, 0);
		$pdf->Cell(5, 1, '', 0, 0);
		$pdf->Cell(85, 1, '', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Keterangan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->knl_keterangan, 0, 'J', false);

		$pdf->Cell(10, 12, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(174, 7, 'Demikian surat kenal lahir ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Yang Melapor', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(111, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $query->nama_pemohon, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, strtoupper($query->knl_kades), 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}
}
