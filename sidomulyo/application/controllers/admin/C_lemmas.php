<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_lemmas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data1['title'] = "Data Pengurus PKK";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 61;
		$data1['submenu2'] = 0;
		$data2["pkk"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='PKK' OR lm.keterangan_jabatan LIKE '%Pokja%'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pkk/V_pkk", $data2);
		$this->load->view("admin/V_footer");
	}

	public function lpmd()
	{
		$data1['title'] = "Data Pengurus LPMD";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 62;
		$data1['submenu2'] = 0;
		$data2["lpmd"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='LPMD'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/lpmd/V_lpmd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function karang_taruna()
	{
		$data1['title'] = "Data Pengurus Karang Taruna";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 63;
		$data1['submenu2'] = 0;
		$data2["taruna"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Karang Taruna' OR lm.keterangan_jabatan LIKE '%Seksi%'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/karangtaruna/V_karangtaruna", $data2);
		$this->load->view("admin/V_footer");
	}

	public function posyandu()
	{
		$data1['title'] = "Data Pengurus Posyandu";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 64;
		$data1['submenu2'] = 0;
		$data2["posyandu"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan LIKE 'Posyandu' OR lm.asal_organisasi LIKE '%posyandu%'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/posyandu/V_posyandu", $data2);
		$this->load->view("admin/V_footer");
	}

	public function rt()
	{
		$data1['title'] = "Data Ketua RT";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 65;
		$data1['submenu2'] = 0;
		$data2["rt"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan LIKE 'RT'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rtrw/V_rt", $data2);
		$this->load->view("admin/V_footer");
	}

	public function rw()
	{
		$data1['title'] = "Data Ketua RW";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 66;
		$data1['submenu2'] = 0;
		$data2["rt"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan LIKE 'RW'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rtrw/V_rw", $data2);
		$this->load->view("admin/V_footer");
	}

	public function karang_werda()
	{
		$data1['title'] = "Data Pengurus Karang Werda";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 67;
		$data1['submenu2'] = 0;
		$data2["werda"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Karang Werda'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/karangwerda/V_karangwerda", $data2);
		$this->load->view("admin/V_footer");
	}

	public function rumah_desa_sehat()
	{
		$data1['title'] = "Data Pengurus Rumah Desa Sehat";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 68;
		$data1['submenu2'] = 0;
		$data2["rds"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='RDS'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rds/V_rds", $data2);
		$this->load->view("admin/V_footer");
	}

	public function rules_1()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}


	public function rules_2()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fasal',
				'label' => 'Asal',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_3()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fasalrt',
				'label' => 'Asal RT',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fasalrw',
				'label' => 'Asal RW',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_4()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_5()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_6()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]
		];
	}

	public function rules_7()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fasalrw',
				'label' => 'Asal RW',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function tambah_pkk()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 61;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='PKK'")->result();
		$data2["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. PKK'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			$foto = $this->_uploadImagepkk($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('pkk-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pkk/V_tambah_pkk", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_lpmd()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_6());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 62;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='LPMD'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'LPMD';
			$foto = $this->_uploadImagelpmd($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('lpmd-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/lpmd/V_tambah_lpmd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_taruna()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 63;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Karang Taruna'")->result();
		$data2["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. Karang Taruna'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			$foto = $this->_uploadImagetaruna($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('karangtaruna-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/karangtaruna/V_tambah_taruna", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_posyandu()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 64;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Posyandu'")->result();
		$data2["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. Posyandu'")->result();
		$data2["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Posyandu'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			$asal = $this->input->post('fasal');
			$foto = $this->_uploadImageposyandu($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('posyandu-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/posyandu/V_tambahposyandu", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_rt()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_3());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 65;
		$data1['submenu2'] = 0;
		$data2["asalrt"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal RT'")->result();
		$data2["asalrw"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal RW'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = 93;
			$keterangan = 'RT';
			$asalrt = $this->input->post('fasalrt');
			$asalrw = $this->input->post('fasalrw');
			$foto = $this->_uploadImageRtrw($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asalrt,
				'lain_lain' => $asalrw,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('rt-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rtrw/V_tambahrt", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_rw()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_7());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 66;
		$data1['submenu2'] = 0;
		$data2["asalrw"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal RW'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = 94;
			$keterangan = 'RW';
			$asalrw = $this->input->post('fasalrw');
			$foto = $this->_uploadImageRtrw($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asalrw,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('rw-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rtrw/V_tambahrw", $data2);
		$this->load->view("admin/V_footer");
	}


	public function tambah_werda()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_4());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 67;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Karang Werda'")->result();
		//$data2["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Karang Werda'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'Karang Werda';
			//$asal = $this->input->post('fasal');
			$foto = $this->_uploadImageWerda($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				//'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('karangwerda-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/karangwerda/V_tambahwerda", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_rds()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_5());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 68;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Rumah Desa Sehat'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'RDS';
			$foto = $this->_uploadImageRds($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('rds-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rds/V_tambahrds", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit_pkk($id = null)
	{
		if (!isset($id)) redirect('pkk-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 61;
		$data1['submenu2'] = 0;
		$data["pkk"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='PKK'")->result();
		$data["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. PKK'")->result();
		if (!$data["pkk"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImagepkk($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('pkk-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pkk/V_edit_pkk", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_lpmd($id = null)
	{
		if (!isset($id)) redirect('lpmd-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_6());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 62;
		$data1['submenu2'] = 0;
		$data["lpmd"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='LPMD'")->result();

		if (!$data["lpmd"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImagelpmd($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('lpmd-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/lpmd/V_editlpmd", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_taruna($id = null)
	{
		if (!isset($id)) redirect('karangtaruna-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 63;
		$data1['submenu2'] = 0;
		$data["taruna"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		if (!$data["taruna"]) show_404();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Karang Taruna'")->result();
		$data["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. Karang Taruna'")->result();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImagetaruna($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('karangtaruna-admin');
		}



		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/karangtaruna/V_edit_taruna", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_posyandu($id = null)
	{
		if (!isset($id)) redirect('posyandu-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 64;
		$data1['submenu2'] = 0;
		$data["posyandu"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Posyandu'")->result();
		$data["keterangan"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Ket. Posyandu'")->result();
		$data["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Posyandu'")->result();
		if (!$data["posyandu"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = $this->input->post('fket');
			$asal = $this->input->post('fasal');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageposyandu($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('posyandu-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/posyandu/V_editposyandu", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_rt($id = null)
	{
		if (!isset($id)) redirect('rt-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_3());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 65;
		$data1['submenu2'] = 0;
		$data["rt"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data["asalrt"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal RT'")->result();
		$data["asalrw"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal RW'")->result();
		if (!$data["rt"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$asalrt = $this->input->post('fasalrt');
			$asalrw = $this->input->post('fasalrw');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageRtrw($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'asal_organisasi' => $asalrt,
				'lain_lain' => $asalrw,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('rt-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rtrw/V_editrt", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_rw($id = null)
	{
		if (!isset($id)) redirect('rw-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_7());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 66;
		$data1['submenu2'] = 0;
		$data["rw"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data["asalrw"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal RW'")->result();
		if (!$data["rw"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$asalrw = $this->input->post('fasalrw');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageRtrw($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'asal_organisasi' => $asalrw,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('rw-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rtrw/V_editrw", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_werda($id = null)
	{
		if (!isset($id)) redirect('karangwerda-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_4());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 67;
		$data1['submenu2'] = 0;
		$data["werda"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Karang Werda'")->result();
		// $data["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Karang Werda'")->result();
		if (!$data["werda"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			// $asal = $this->input->post('fasal');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageWerda($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				// 'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('karangwerda-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/karangwerda/V_editwerda", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_rds($id = null)
	{
		if (!isset($id)) redirect('rds-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_5());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 6;
		$data1['submenu'] = 68;
		$data1['submenu2'] = 0;
		$data["rds"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Rumah Desa Sehat'")->result();

		if (!$data["rds"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageRds($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('rds-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rds/V_editrds", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus_pkk($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImagepkk($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('pkk-admin');
		}
	}

	public function hapus_lpmd($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImagelpmd($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('lpmd-admin');
		}
	}

	public function hapus_taruna($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImagetaruna($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('karangtaruna-admin');
		}
	}

	public function hapus_posyandu($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageposyandu($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('posyandu-admin');
		}
	}

	public function hapus_rt($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageRtrw($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('rt-admin');
		}
	}

	public function hapus_rw($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageRtrw($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('rw-admin');
		}
	}

	public function hapus_werda($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageWerda($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('karangwerda-admin');
		}
	}

	public function hapus_rds($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageRds($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('rds-admin');
		}
	}

	private function _uploadImagepkk($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/pkk/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImagelpmd($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/lpmd/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImagetaruna($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/karangtaruna/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageposyandu($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/posyandu/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageRtrw($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/rtrw/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageWerda($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/karangwerda/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageRds($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/rds/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _deleteImagepkk($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/pkk/$filename.*"));
		}
	}

	private function _deleteImagelpmd($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/lpmd/$filename.*"));
		}
	}

	private function _deleteImagetaruna($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/karangtaruna/$filename.*"));
		}
	}

	private function _deleteImageposyandu($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/posyandu/$filename.*"));
		}
	}

	private function _deleteImageRtrw($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/rtrw/$filename.*"));
		}
	}

	private function _deleteImageWerda($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/karangwerda/$filename.*"));
		}
	}

	private function _deleteImageRds($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/rds/$filename.*"));
		}
	}
}
