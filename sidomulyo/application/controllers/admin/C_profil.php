<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_profil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(["M_login","M_crud"]);
        if ($this->M_login->isNotLogin()) {
            redirect(site_url('C_login'));
        }
    }

    public function index()
    {
        $data1['title'] = "Profil";
        $data1['menu_aktif'] = 1;
        $data1['submenu'] = 0;
		$data1['submenu2'] = 0;

        $ids = $this->session->userdata("usr_id");
        $data2["biodata"] = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE id_user = '$ids'")->result();
        $this->load->view("admin/V_header", $data1);
        $this->load->view("admin/V_profil", $data2);
        $this->load->view("admin/V_footer");
    }

    public function ubah_profil()
    {
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $pass_enkrip = md5($password);
        $ids = $this->session->userdata("usr_id");

        $cek_pass = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE password_user = '$pass_enkrip'")->num_rows();
        if ($cek_pass > 0) { // jika password benar

            //cek apakah ada perubahan
            $old_username = $this->M_crud->getQuery("SELECT nama_user, username_user FROM tb_user WHERE id_user = '$ids'")->row_array();
            if ($old_username['nama_user'] != $nama) { // apakah ada perubahan di nama

                //update data
                $fields_nama = array(
                "nama_user" => $nama
              );
                $where_nama = array("id_user" => $ids);
                $this->M_crud->updateData("tb_user", $fields_nama, $where_nama);
                $isi_pesan = "Berhasil mengubah profil";
                $type = "success";
                $judul = "Berhasil";
            }
            if ($old_username['username_user'] != $username) { // jika ada perubahan di username

                $cek_username = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE username_user = '$username'")->num_rows();
                if ($cek_username == 0) { // jika username unik

                    //update data
                    $fields_uname = array(
                      "username_user" => $username
                    );
                    $where_uname = array("id_user" => $ids);
                    $this->M_crud->updateData("tb_user", $fields_uname, $where_uname);
                    $isi_pesan = "Berhasil mengubah profil";
                    $type = "success";
                    $judul = "Berhasil";
                } else {  // jika ada kesamaan

                    $isi_pesan = "Username sudah digunakan";
                    $type = "danger";
                    $judul = "Gagal";
                }
            }
        } else { //jika password salah

            $isi_pesan = "Password salah";
            $type = "danger";
            $judul = "Gagal";
        }

        $_SESSION['type2'] = $type;
        $_SESSION['judul2'] = $judul;
        $_SESSION['isi2'] = $isi_pesan;
        if (isset($_SESSION['tab_aktif'])) {
            unset($_SESSION['tab_aktif']);
        }
        $_SESSION['tab_aktif'] = 2;

        redirect("admin/C_profil");
    }

    public function ubah_password()
    {
        $pass_lama = $this->input->post('pass_lama');
        $pass_baru = $this->input->post('pass_baru');
        $konfirm_pass = $this->input->post('konfirm_pass');
        $pass_lama_en = md5($pass_lama);
        $ids = $this->session->userdata("usr_id");

        $cek_pass = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE password_user = '$pass_lama_en'")->num_rows();
        if ($cek_pass > 0) { // jika password benar

            // cek kesamaan password dengan konfirm password
        if ($pass_baru == $konfirm_pass) { // jika sama

          $pass_baru_en = md5($pass_baru);

            //update data
            $fields = array(
            "password_user" => $pass_baru_en
          );
            $where = array("id_user" => $ids);
            $this->M_crud->updateData("tb_user", $fields, $where);

            $isi_pesan = "Berhasil mengubah password";
            $type = "success";
            $judul = "Berhasil";
        } else {  // jika tidak sama

            $isi_pesan = "Password Baru dan Konfirmasi Password tidak sama";
            $type = "danger";
            $judul = "Gagal";
        }
        } else { //jika password salah

            $isi_pesan = "Password lama salah";
            $type = "danger";
            $judul = "Gagal";
        }

        $_SESSION['type3'] = $type;
        $_SESSION['judul3'] = $judul;
        $_SESSION['isi3'] = $isi_pesan;

        if (isset($_SESSION['tab_aktif'])) {
            unset($_SESSION['tab_aktif']);
        }
        $_SESSION['tab_aktif'] = 3;

        redirect("admin/C_profil");
    }
}
