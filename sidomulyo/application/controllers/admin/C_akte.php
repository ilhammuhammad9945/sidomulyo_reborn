<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_akte extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["upload", "pdf"]);
	}

	public function index()
	{
		$data1['title'] = "Data Akte Kelahiran";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		if (isset($_SESSION['fsuratAkte'])) {
			if ($_SESSION['fsuratAkte'] == 'Akte') {
				$data2['surat_akte'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat JOIN tb_akte_f_dua ON tb_surat.id_surat = tb_akte_f_dua.id_surat WHERE tb_surat.jenis_surat = 'akte kelahiran' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
			} else if ($_SESSION['fsuratAkte'] == 'Am') {
				$data2['surat_akte'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_anak_mama ON tb_surat.id_surat = tb_anak_mama.id_surat WHERE tb_surat.jenis_surat = 'anak mama' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
			} else {
				echo "Something Wrong..!!!";
			}
		} else {
			$_SESSION['fsuratAkte'] = 'Akte';
			$data2['surat_akte'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat JOIN tb_akte_f_dua ON tb_surat.id_surat = tb_akte_f_dua.id_surat WHERE tb_surat.jenis_surat = 'akte kelahiran' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_indexAkte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function filterdataAkte()
	{
		$dataAkte = $_POST['dataAkte'];

		unset($_SESSION['fsuratAkte']);

		$_SESSION['fsuratAkte'] = $dataAkte;
	}

	public function pengantarAkte()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_pengantarAkte", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanPengantarAkte()
	{
		if (isset($_POST['btnSimpanPengantarAkte'])) {
			$id_peng = $this->input->post('id_peng');
			//Data Anak
			$fnamaanak = $this->input->post('fnamaanak');
			$fjkelanak = $this->input->post('fjkelanak');
			$ftempatlahiranak = $this->input->post('ftempatlahiranak');
			$ftgllahiranak = $this->input->post('ftgllahiranak');
			$fjenislahiranak = $this->input->post('fjenislahiranak');
			$fanakke = $this->input->post('fanakke');

			//Data Orang Tua
			$fnamaayah = $this->input->post('fnamaayah');
			$fnamaibu = $this->input->post('fnamaibu');
			$fnamasaksi1 = $this->input->post('fnamasaksi1');
			$fnamasaksi2 = $this->input->post('fnamasaksi2');

			//Data Lain lain
			$fnikpemohon = $this->input->post('fnikpemohon');
			$fnamapemohon = $this->input->post('fnamapemohon');
			// $now = date('Y-m-d');


			//Proses upload
			$config['upload_path']          = './assets/upload/layanan/akte';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['encrypt_name'] = true; // enkripsi nama yang terupload nantinya
			$this->upload->initialize($config);


			// upload berkas FC legalisir sesuai aslinya Surat Nikah / Perkawinan
			if ($this->upload->do_upload('fcopynikah')) {
				$gbr2 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/akte' . $gbr2['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/akte' . $gbr2['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}

			// upload berkas FC legalisir sesuai aslinya KTP Suami / Istri
			if ($this->upload->do_upload('fcopyktp')) {
				$gbr3 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/akte' . $gbr3['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/akte' . $gbr3['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}

			// upload berkas FC legalisir sesuai aslinya KK (nama anak sudah tercantum)
			if ($this->upload->do_upload('fcopykk')) {
				$gbr4 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/akte' . $gbr4['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/akte' . $gbr4['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}

			// upload berkas FC legalisir sesuai aslinya Ijazah bagi yang mempunyai
			if ($this->upload->do_upload('fcopyijaz')) {
				$gbr5 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/akte' . $gbr5['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/akte' . $gbr5['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'akte kelahiran',
				// 'tgl_surat' => $now,
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $fnikpemohon,
				'nama_pemohon' => $fnamapemohon,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'ank_nama' => $fnamaanak,
				'ank_gender' => $fjkelanak,
				'ank_tempat_lahir' => $ftempatlahiranak,
				'ank_tgl_lahir' => $ftgllahiranak,
				'ank_jenis_kelahiran' => $fjenislahiranak,
				'ank_anak_ke' => $fanakke,
				'ortu_nama_ayah' => $fnamaayah,
				'ortu_nama_ibu' => $fnamaibu,
				'ortu_nama_saksi1' => $fnamasaksi1,
				'ortu_nama_saksi2' => $fnamasaksi2,
				'brk_surat_nikah' => $gbr2['file_name'],
				'brk_ktp' => $gbr3['file_name'],
				'brk_kk' => $gbr4['file_name'],
				'brk_ijazah' => $gbr5['file_name']
			);
			$this->M_crud->simpanData('tb_akte_pengantar', $fields_p_akte);

			//Update Progres pengisian
			$fields_upd_progres = array('progress_pembuatan' => 30);
			$where_upd_progres =  array('id_surat' => $id_surat);
			$this->M_crud->updateData("tb_surat", $fields_upd_progres, $where_upd_progres);

			// set session id surat untuk session selanjutnya
			$_SESSION['id_surat'] = $id_surat;

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil menyimpan data';

			redirect('tambah-pernyataan-akte');
		}
	}

	public function editPengantarAkte($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['surat_akte'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat JOIN tb_akte_f_dua ON tb_surat.id_surat = tb_akte_f_dua.id_surat WHERE tb_surat.jenis_surat = 'akte kelahiran' AND tb_surat.id_surat = '$id' AND tb_surat.aktif = 1")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_editPengantarAkte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesEditPengantarAkte()
	{
		if (isset($_POST['btnEditPengantarAkte'])) {
			$id_surat = $this->input->post('id_surat');

			//Data Anak
			$fnamaanak = $this->input->post('fnamaanak');
			$fjkelanak = $this->input->post('fjkelanak');
			$ftempatlahiranak = $this->input->post('ftempatlahiranak');
			$ftgllahiranak = $this->input->post('ftgllahiranak');
			$fjenislahiranak = $this->input->post('fjenislahiranak');
			$fanakke = $this->input->post('fanakke');

			//Data Orang Tua
			$fnamaayah = $this->input->post('fnamaayah');
			$fnamaibu = $this->input->post('fnamaibu');
			$fnamasaksi1 = $this->input->post('fnamasaksi1');
			$fnamasaksi2 = $this->input->post('fnamasaksi2');

			//Data Lain lain
			$fnikpemohon = $this->input->post('fnikpemohon');
			$fnamapemohon = $this->input->post('fnamapemohon');
			// $now = date('Y-m-d');


			//Proses upload
			$config['upload_path']          = './assets/upload/layanan/akte';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['encrypt_name'] = true; // enkripsi nama yang terupload nantinya
			$this->upload->initialize($config);

			$where = array('id_surat' => $id_surat);

			if (!empty($_FILES['fcopynikah']['name'])) {
				// upload berkas FC legalisir sesuai aslinya Surat Nikah / Perkawinan
				if ($this->upload->do_upload('fcopynikah')) {
					$gbr2 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/akte' . $gbr2['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/akte' . $gbr2['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}

				$xy2 = $this->M_crud->getQuery("SELECT brk_surat_nikah FROM tb_akte_pengantar WHERE id_surat = '$id_surat'")->row_array();
				$target2 = "./assets/upload/layanan/akte/" . $xy2['brk_surat_nikah'];
				unlink($target2); // hapus data

				$field2 = array('brk_surat_nikah' => $gbr2['file_name']);
				$this->M_crud->updateData('tb_akte_pengantar', $field2, $where);
			}

			if (!empty($_FILES['fcopyktp']['name'])) {
				// upload berkas FC legalisir sesuai aslinya KTP Suami / Istri
				if ($this->upload->do_upload('fcopyktp')) {
					$gbr3 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/akte' . $gbr3['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/akte' . $gbr3['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}

				$xy3 = $this->M_crud->getQuery("SELECT brk_ktp FROM tb_akte_pengantar WHERE id_surat = '$id_surat'")->row_array();
				$target3 = "./assets/upload/layanan/akte/" . $xy3['brk_ktp'];
				unlink($target3); // hapus data

				$field3 = array('brk_ktp' => $gbr3['file_name']);
				$this->M_crud->updateData('tb_akte_pengantar', $field3, $where);
			}

			if (!empty($_FILES['fcopykk']['name'])) {
				// upload berkas FC legalisir sesuai aslinya KK (nama anak sudah tercantum)
				if ($this->upload->do_upload('fcopykk')) {
					$gbr4 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/akte' . $gbr4['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/akte' . $gbr4['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}

				$xy4 = $this->M_crud->getQuery("SELECT brk_kk FROM tb_akte_pengantar WHERE id_surat = '$id_surat'")->row_array();
				$target4 = "./assets/upload/layanan/akte/" . $xy4['brk_kk'];
				unlink($target4); // hapus data

				$field4 = array('brk_kk' => $gbr4['file_name']);
				$this->M_crud->updateData('tb_akte_pengantar', $field4, $where);
			}

			if (!empty($_FILES['fcopyijaz']['name'])) {
				// upload berkas FC legalisir sesuai aslinya Ijazah bagi yang mempunyai
				if ($this->upload->do_upload('fcopyijaz')) {
					$gbr5 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/akte' . $gbr5['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/akte' . $gbr5['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}

				$xy5 = $this->M_crud->getQuery("SELECT brk_ijazah FROM tb_akte_pengantar WHERE id_surat = '$id_surat'")->row_array();
				$target5 = "./assets/upload/layanan/akte/" . $xy5['brk_ijazah'];
				unlink($target5); // hapus data

				$field5 = array('brk_ijazah' => $gbr5['file_name']);
				$this->M_crud->updateData('tb_akte_pengantar', $field5, $where);
			}

			// Proses update Surat
			$fields_surat = array(
				'nik_pemohon' => $fnikpemohon,
				'nama_pemohon' => $fnamapemohon
			);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses update Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'ank_nama' => $fnamaanak,
				'ank_gender' => $fjkelanak,
				'ank_tempat_lahir' => $ftempatlahiranak,
				'ank_tgl_lahir' => $ftgllahiranak,
				'ank_jenis_kelahiran' => $fjenislahiranak,
				'ank_anak_ke' => $fanakke,
				'ortu_nama_ayah' => $fnamaayah,
				'ortu_nama_ibu' => $fnamaibu,
				'ortu_nama_saksi1' => $fnamasaksi1,
				'ortu_nama_saksi2' => $fnamasaksi2
			);
			$this->M_crud->updateData('tb_akte_pengantar', $fields_p_akte, $where);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil mengedit data';

			redirect('layanan-akte-admin');
		}
	}

	public function prosesLihatAkte()
	{
		$lid_surat = $this->input->post('lid_surat');
		$fpilihdata = $this->input->post('fpilihdata');

		if (isset($_SESSION['lid_surat'])) {
			unset($_SESSION['lid_surat']);
			unset($_SESSION['fpilihdata']);
		}


		$_SESSION['lid_surat'] = $lid_surat;
		$_SESSION['fpilihdata'] = $fpilihdata;

		redirect('lihat-akte');
	}

	public function lihatAkte()
	{
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		if ($_SESSION['fpilihdata'] == 'Pengantar') {
			$data_tabel['pengantar'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat WHERE tb_surat.id_surat = '$_SESSION[lid_surat]'")->row_array();

			$this->load->view("admin/V_header", $data1);
			$this->load->view("admin/akte/V_lihatPengantarAkte", $data_tabel);
			$this->load->view("admin/V_footer");
		} elseif ($_SESSION['fpilihdata'] == 'Pernyataan') {
			$data_tabel['pernyataan'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat WHERE tb_surat.id_surat = '$_SESSION[lid_surat]'")->row_array();

			$this->load->view("admin/V_header", $data1);
			$this->load->view("admin/akte/V_lihatPernyataanAkte", $data_tabel);
			$this->load->view("admin/V_footer");
		} else {
			$data_tabel['f21'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_akte_f_dua ON tb_surat.id_surat = tb_akte_f_dua.id_surat JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat WHERE tb_surat.id_surat = '$_SESSION[lid_surat]'")->row_array();

			$this->load->view("admin/V_header", $data1);
			$this->load->view("admin/akte/V_lihatF201Akte", $data_tabel);
			$this->load->view("admin/V_footer");
		}
	}

	public function lihatPengantarAkte($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['pengantar'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_lihatPengantarAkte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function doneAkte($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-akte-admin');
	}


	public function hapusPengantarAkte($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-akte-admin');
	}

	public function pernyataanAkte()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['surat'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE id_surat = '$_SESSION[id_surat]'")->row_array();
		$data2['pengantar'] = $this->M_crud->getQuery("SELECT * FROM tb_akte_pengantar WHERE id_surat = '$_SESSION[id_surat]'")->row_array();
		$data2['invoice'] = $this->M_layanan_surat->get_id_peng();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_pernyataanAkte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function simpanPernyataanAkte()
	{
		if (isset($_POST['btnSimpanPernyataanAkte'])) {

			//Data Pemohon
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$falamatpemohon = $this->input->post('falamatpemohon');

			//Data saksi
			$fnamasaksi1 = $this->input->post('fnamasaksi1');
			$falamatsaksi1 = $this->input->post('falamatsaksi1');
			$fhubsaksi1 = $this->input->post('fhubsaksi1');
			$fnamasaksi2 = $this->input->post('fnamasaksi2');
			$falamatsaksi2 = $this->input->post('falamatsaksi2');
			$fhubsaksi2 = $this->input->post('fhubsaksi2');


			// Proses Simpan Data Pernyataan
			$fields_p_akte = array(
				'id_surat' => $_SESSION['id_surat'],
				'pmh_tempat_lahir' => $ftmptlahirpemohon,
				'pmh_tgl_lahir' => $ftgllahirpemohon,
				'pmh_pekerjaan' => $fpekerjaanpemohon,
				'pmh_alamat' => $falamatpemohon,
				'saksi_nama_1' => $fnamasaksi1,
				'saksi_alamat_1' => $falamatsaksi1,
				'saksi_hub_1' => $fhubsaksi1,
				'saksi_nama_2' => $fnamasaksi2,
				'saksi_alamat_2' => $falamatsaksi2,
				'saksi_hub_2' => $fhubsaksi2
			);
			$this->M_crud->simpanData('tb_akte_pernyataan', $fields_p_akte);

			//Update Progres pengisian
			$fields_upd_progres = array('progress_pembuatan' => 60);
			$where_upd_progres =  array('id_surat' => $_SESSION['id_surat']);
			$this->M_crud->updateData("tb_surat", $fields_upd_progres, $where_upd_progres);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil mengedit data';

			redirect('tambah-f201-akte');
		}
	}

	public function editPernyataanAkte($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_editPernyataanAkte");
		$this->load->view("admin/V_footer");
	}

	public function prosesEditPernyataanAkte()
	{
		if (isset($_POST['btnEditPernyataanAkte'])) {
			$id_surat = $this->input->post('id_surat');

			//Data Pemohon
			$fnamapemohon = $this->input->post('fnamapemohon');
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$falamatpemohon = $this->input->post('falamatpemohon');

			//Data saksi
			$fnamasaksi1 = $this->input->post('fnamasaksi1');
			$falamatsaksi1 = $this->input->post('falamatsaksi1');
			$fhubsaksi1 = $this->input->post('fhubsaksi1');
			$fnamasaksi2 = $this->input->post('fnamasaksi2');
			$falamatsaksi2 = $this->input->post('falamatsaksi2');
			$fhubsaksi2 = $this->input->post('fhubsaksi2');

			$where = array('id_surat' => $id_surat);

			$fields_parent = array('nama_pemohon' => $fnamapemohon);

			// Proses Update Data Pernyataan
			$fields_p_akte = array(
				'pmh_tempat_lahir' => $ftmptlahirpemohon,
				'pmh_tgl_lahir' => $ftgllahirpemohon,
				'pmh_pekerjaan' => $fpekerjaanpemohon,
				'pmh_alamat' => $falamatpemohon,
				'saksi_nama_1' => $fnamasaksi1,
				'saksi_alamat_1' => $falamatsaksi1,
				'saksi_hub_1' => $fhubsaksi1,
				'saksi_nama_2' => $fnamasaksi2,
				'saksi_alamat_2' => $falamatsaksi2,
				'saksi_hub_2' => $fhubsaksi2
			);

			$this->M_crud->updateData('tb_surat', $fields_parent, $where);
			$this->M_crud->updateData('tb_akte_pernyataan', $fields_p_akte, $where);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil mengedit data';

			redirect('layanan-akte-admin');
		}
	}

	public function lihatPernyataanAkte($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['pernyataan'] = $this->M_crud->getQuery(
			"SELECT * FROM tb_surat
          JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat
          JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat
          WHERE tb_surat.id_surat = '$id'"
		)->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_lihatPernyataanAkte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusPernyataanAkte($id = null)
	{
		echo "tes";
	}

	public function f201Akte()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['surat'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE id_surat = '$_SESSION[id_surat]'")->row_array();
		$data2['pengantar'] = $this->M_crud->getQuery("SELECT * FROM tb_akte_pengantar WHERE id_surat = '$_SESSION[id_surat]'")->row_array();
		$data2['pernyataan'] = $this->M_crud->getQuery("SELECT * FROM tb_akte_pernyataan WHERE id_surat = '$_SESSION[id_surat]'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_f201Akte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function simpanf201Akte()
	{
		if (isset($_POST['btnSimpanF2Akte'])) {

			//Data Umum
			$fnamakepala = $this->input->post('fnamakepala');
			$fnokk = $this->input->post('fnokk');

			//Data Anak
			$fpukullahiranak = $this->input->post('fpukullahiranak');
			$fpenolongkelahiran = $this->input->post('fpenolongkelahiran');
			$fberatbayi = $this->input->post('fberatbayi');
			$fpanjangbayi = $this->input->post('fpanjangbayi');

			//Data ibu
			$fnikibu = $this->input->post('fnikibu');
			$ftgllahiribu = $this->input->post('ftgllahiribu');
			$fumuribu = $this->input->post('fumuribu');
			$fpekerjaanibu = $this->input->post('fpekerjaanibu');
			$falamatibu = $this->input->post('falamatibu');
			$fwniibu = $this->input->post('fwniibu');
			$fbangsaibu = $this->input->post('fbangsaibu');
			$fkawinibu = $this->input->post('fkawinibu');
			$fumurkwibu = $this->input->post('fumurkwibu');

			//Data ayah
			$fnikayah = $this->input->post('fnikayah');
			$ftgllahirayah = $this->input->post('ftgllahirayah');
			$fumurayah = $this->input->post('fumurayah');
			$fpekerjaanayah = $this->input->post('fpekerjaanayah');
			$falamatayah = $this->input->post('falamatayah');
			$fwniayah = $this->input->post('fwniayah');
			$fbangsaayah = $this->input->post('fbangsaayah');

			// Data PELAPOR
			$fnikpelapor = $this->input->post('fnikpelapor');
			$fnamapelapor = $this->input->post('fnamapelapor');
			$fumurpelapor = $this->input->post('fumurpelapor');
			$fjkelpelapor = $this->input->post('fjkelpelapor');
			$fpekerjaanpelapor = $this->input->post('fpekerjaanpelapor');
			$falamatpelapor = $this->input->post('falamatpelapor');

			// Data Saksi 1
			$fniksaksi1 = $this->input->post('fniksaksi1');
			$fumursaksi1 = $this->input->post('fumursaksi1');
			$fpekerjaansaksi1 = $this->input->post('fpekerjaansaksi1');
			$falamatsaksi1 = $this->input->post('falamatsaksi1');

			//Data Saksi 2
			$fniksaksi2 = $this->input->post('fniksaksi2');
			$fumursaksi2 = $this->input->post('fumursaksi2');
			$fpekerjaansaksi2 = $this->input->post('fpekerjaansaksi2');
			$falamatsaksi2 = $this->input->post('falamatsaksi2');


			// Proses Simpan Data Pernyataan
			$fields_f02_akte = array(
				'id_surat' => $_SESSION['id_surat'],
				'kep_keluarga_nama' => $fnamakepala,
				'nomor_kk' => $fnokk,
				'ank_pukul_lahir' => $fpukullahiranak,
				'ank_penolong_lahir' => $fpenolongkelahiran,
				'ank_berat' => $fberatbayi,
				'ank_panjang' => $fpanjangbayi,
				'plp_nik' => $fnikpelapor,
				'plp_nama' => $fnamapelapor,
				'plp_umur' => $fumurpelapor,
				'plp_gender' => $fjkelpelapor,
				'plp_pekerjaan' => $fpekerjaanpelapor,
				'plp_alamat' => $falamatpelapor,
				'saksi1_nik' => $fniksaksi1,
				'saksi1_umur' => $fumursaksi1,
				'saksi1_pekerjaan' => $fpekerjaansaksi1,
				'saksi1_alamat' => $falamatsaksi1,
				'saksi2_nik' => $fniksaksi2,
				'saksi2_umur' => $fumursaksi2,
				'saksi2_pekerjaan' => $fpekerjaansaksi2,
				'saksi2_alamat' => $falamatsaksi2,
				'ibu_nik' => $fnikibu,
				'ibu_tgl_lahir' => $ftgllahiribu,
				'ibu_umur' => $fumuribu,
				'ibu_pekerjaan' => $fpekerjaanibu,
				'ibu_alamat' => $falamatibu,
				'ibu_kwn' => $fwniibu,
				'ibu_kebangsaan' => $fbangsaibu,
				'ibu_tgl_kawin' => $fkawinibu,
				'ibu_umur_kawin' => $fumurkwibu,
				'ayah_nik' => $fnikayah,
				'ayah_tgl_lahir' => $ftgllahirayah,
				'ayah_umur' => $fumurayah,
				'ayah_pekerjaan' => $fpekerjaanayah,
				'ayah_alamat' => $falamatayah,
				'ayah_kwn' => $fwniayah,
				'ayah_kebangsaan' => $fbangsaayah
			);
			$this->M_crud->simpanData('tb_akte_f_dua', $fields_f02_akte);

			//Update Progres pengisian
			$fields_upd_progres = array('progress_pembuatan' => 100);
			$where_upd_progres =  array('id_surat' => $_SESSION['id_surat']);
			$this->M_crud->updateData("tb_surat", $fields_upd_progres, $where_upd_progres);

			unset($_SESSION['id_surat']);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil menyimpan data';

			redirect('layanan-akte-admin');
		}
	}

	public function editF201Akte($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_editF201Akte");
		$this->load->view("admin/V_footer");
	}

	public function prosesEditf201Akte()
	{
		if (isset($_POST['btnSimpanF2Akte'])) {
			$id_surat = $this->input->post('id_surat');

			//Data Umum
			$fnamakepala = $this->input->post('fnamakepala');
			$fnokk = $this->input->post('fnokk');

			//Data Anak
			$fpukullahiranak = $this->input->post('fpukullahiranak');
			$fpenolongkelahiran = $this->input->post('fpenolongkelahiran');
			$fberatbayi = $this->input->post('fberatbayi');
			$fpanjangbayi = $this->input->post('fpanjangbayi');

			//Data ibu
			$fnikibu = $this->input->post('fnikibu');
			$ftgllahiribu = $this->input->post('ftgllahiribu');
			$fumuribu = $this->input->post('fumuribu');
			$fpekerjaanibu = $this->input->post('fpekerjaanibu');
			$falamatibu = $this->input->post('falamatibu');
			$fwniibu = $this->input->post('fwniibu');
			$fbangsaibu = $this->input->post('fbangsaibu');
			$fkawinibu = $this->input->post('fkawinibu');
			$fumurkwibu = $this->input->post('fumurkwibu');

			//Data ayah
			$fnikayah = $this->input->post('fnikayah');
			$ftgllahirayah = $this->input->post('ftgllahirayah');
			$fumurayah = $this->input->post('fumurayah');
			$fpekerjaanayah = $this->input->post('fpekerjaanayah');
			$falamatayah = $this->input->post('falamatayah');
			$fwniayah = $this->input->post('fwniayah');
			$fbangsaayah = $this->input->post('fbangsaayah');

			// Data PELAPOR
			$fnikpelapor = $this->input->post('fnikpelapor');
			$fnamapelapor = $this->input->post('fnamapelapor');
			$fumurpelapor = $this->input->post('fumurpelapor');
			$fjkelpelapor = $this->input->post('fjkelpelapor');
			$fpekerjaanpelapor = $this->input->post('fpekerjaanpelapor');
			$falamatpelapor = $this->input->post('falamatpelapor');

			// Data Saksi 1
			$fniksaksi1 = $this->input->post('fniksaksi1');
			$fumursaksi1 = $this->input->post('fumursaksi1');
			$fpekerjaansaksi1 = $this->input->post('fpekerjaansaksi1');
			$falamatsaksi1 = $this->input->post('falamatsaksi1');

			//Data Saksi 2
			$fniksaksi2 = $this->input->post('fniksaksi2');
			$fumursaksi2 = $this->input->post('fumursaksi2');
			$fpekerjaansaksi2 = $this->input->post('fpekerjaansaksi2');
			$falamatsaksi2 = $this->input->post('falamatsaksi2');

			$where = array('id_surat' => $id_surat);

			// Proses Update Data Pernyataan
			$fields_f02_akte = array(
				'kep_keluarga_nama' => $fnamakepala,
				'nomor_kk' => $fnokk,
				'ank_pukul_lahir' => $fpukullahiranak,
				'ank_penolong_lahir' => $fpenolongkelahiran,
				'ank_berat' => $fberatbayi,
				'ank_panjang' => $fpanjangbayi,
				'plp_nik' => $fnikpelapor,
				'plp_nama' => $fnamapelapor,
				'plp_umur' => $fumurpelapor,
				'plp_gender' => $fjkelpelapor,
				'plp_pekerjaan' => $fpekerjaanpelapor,
				'plp_alamat' => $falamatpelapor,
				'saksi1_nik' => $fniksaksi1,
				'saksi1_umur' => $fumursaksi1,
				'saksi1_pekerjaan' => $fpekerjaansaksi1,
				'saksi1_alamat' => $falamatsaksi1,
				'saksi2_nik' => $fniksaksi2,
				'saksi2_umur' => $fumursaksi2,
				'saksi2_pekerjaan' => $fpekerjaansaksi2,
				'saksi2_alamat' => $falamatsaksi2,
				'ibu_nik' => $fnikibu,
				'ibu_tgl_lahir' => $ftgllahiribu,
				'ibu_umur' => $fumuribu,
				'ibu_pekerjaan' => $fpekerjaanibu,
				'ibu_alamat' => $falamatibu,
				'ibu_kwn' => $fwniibu,
				'ibu_kebangsaan' => $fbangsaibu,
				'ibu_tgl_kawin' => $fkawinibu,
				'ibu_umur_kawin' => $fumurkwibu,
				'ayah_nik' => $fnikayah,
				'ayah_tgl_lahir' => $ftgllahirayah,
				'ayah_umur' => $fumurayah,
				'ayah_pekerjaan' => $fpekerjaanayah,
				'ayah_alamat' => $falamatayah,
				'ayah_kwn' => $fwniayah,
				'ayah_kebangsaan' => $fbangsaayah
			);
			$this->M_crud->updateData('tb_akte_f_dua', $fields_f02_akte, $where);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil mengedit data';

			redirect('layanan-akte-admin');
		}
	}

	public function lihatF201Akte($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['f21'] = $this->M_crud->getQuery(
			"SELECT * FROM tb_surat
          JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat
          JOIN tb_akte_pernyataan ON tb_surat.id_surat = tb_akte_pernyataan.id_surat
          JOIN tb_akte_f_dua ON tb_surat.id_surat = tb_akte_f_dua.id_surat
          WHERE tb_surat.id_surat = '$id'"
		)->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_lihatF201Akte", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusF201Akte($id = null)
	{
		echo "tes";
	}

	public function anakmama1Akte()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_anakMama1", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanAm1Akte()
	{
		if (isset($_POST['btnsimpanam1'])) {

			//Data Umum
			$id_peng = $this->input->post('id_peng');
			$fnikayah = $this->input->post('fnikayah');
			$fnamaayah = $this->input->post('fnamaayah');
			$fpekerjaanayah = $this->input->post('fpekerjaanayah');
			$fwniayah = $this->input->post('fwniayah');
			$fagamaayah = $this->input->post('fagamaayah');
			$falamatayah = $this->input->post('falamatayah');
			$fnamaanak = $this->input->post('fnamaanak');
			$ftmptlahiranak = $this->input->post('ftmptlahiranak');
			$ftgllahiranak = $this->input->post('ftgllahiranak');
			$fjamlahiranak = $this->input->post('fjamlahiranak');
			$fanakke = $this->input->post('fanakke');
			$fnamaibu = $this->input->post('fnamaibu');
			$fumuribu = $this->input->post('fumuribu');
			$falamatibu = $this->input->post('falamatibu');
			$fnamasaksi1 = $this->input->post('fnamasaksi1');
			$fumursaksi1 = $this->input->post('fumursaksi1');
			$fpekerjaansaksi1 = $this->input->post('fpekerjaansaksi1');
			$falamatsaksi1 = $this->input->post('falamatsaksi1');
			$fnamasaksi2 = $this->input->post('fnamasaksi2');
			$fumursaksi2 = $this->input->post('fumursaksi2');
			$fpekerjaansaksi2 = $this->input->post('fpekerjaansaksi2');
			$falamatsaksi2 = $this->input->post('falamatsaksi2');

			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'anak mama',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $fnikayah,
				'nama_pemohon' => $fnamaayah,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);


			// Proses Simpan Data Pernyataan
			$fields_f02_akte = array(
				'id_surat' => $id_surat,
				'am1_pekerjaan' => $fpekerjaanayah,
				'am1_kwn' => $fwniayah,
				'am1_agama' => $fagamaayah,
				'am1_alamat' => $falamatayah,
				'am1_nama_anak' => $fnamaanak,
				'am1_tempat_lahir_anak' => $ftmptlahiranak,
				'am1_tanggal_lahir_anak' => $ftgllahiranak,
				'am1_jam_lahir_anak' => $fjamlahiranak,
				'am1_anak_ke' => $fanakke,
				'am1_nama_ibu' => $fnamaibu,
				'am1_umur_ibu' => $fumuribu,
				'am1_alamat_ibu' => $falamatibu,
				'am1_nama_saksi1' => $fnamasaksi1,
				'am1_umur_saksi1' => $fumursaksi1,
				'am1_pekerjaan_saksi1' => $fpekerjaansaksi1,
				'am1_alamat_saksi1' => $falamatsaksi1,
				'am1_nama_saksi2' => $fnamasaksi2,
				'am1_umur_saksi2' => $fumursaksi2,
				'am1_pekerjaan_saksi2' => $fpekerjaansaksi2,
				'am1_alamat_saksi2' => $falamatsaksi2
			);
			$this->M_crud->simpanData('tb_anak_mama', $fields_f02_akte);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil menyimpan data';

			redirect('layanan-akte-admin');
		}
	}

	public function editAnakmama1Akte($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;

		$data2['anakmama'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_anak_mama ON tb_surat.id_surat = tb_anak_mama.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_editAnakmama1", $data2);
		$this->load->view("admin/V_footer");
	}

	public function editAm1Akte()
	{
		if (isset($_POST['btneditAm'])) {

			//Data Umum
			$id_surat = $this->input->post('id_surat');
			$fnikayah = $this->input->post('fnikayah');
			$fnamaayah = $this->input->post('fnamaayah');
			$fpekerjaanayah = $this->input->post('fpekerjaanayah');
			$fwniayah = $this->input->post('fwniayah');
			$fagamaayah = $this->input->post('fagamaayah');
			$falamatayah = $this->input->post('falamatayah');
			$fnamaanak = $this->input->post('fnamaanak');
			$ftmptlahiranak = $this->input->post('ftmptlahiranak');
			$ftgllahiranak = $this->input->post('ftgllahiranak');
			$fjamlahiranak = $this->input->post('fjamlahiranak');
			$fanakke = $this->input->post('fanakke');
			$fnamaibu = $this->input->post('fnamaibu');
			$fumuribu = $this->input->post('fumuribu');
			$falamatibu = $this->input->post('falamatibu');
			$fnamasaksi1 = $this->input->post('fnamasaksi1');
			$fumursaksi1 = $this->input->post('fumursaksi1');
			$fpekerjaansaksi1 = $this->input->post('fpekerjaansaksi1');
			$falamatsaksi1 = $this->input->post('falamatsaksi1');
			$fnamasaksi2 = $this->input->post('fnamasaksi2');
			$fumursaksi2 = $this->input->post('fumursaksi2');
			$fpekerjaansaksi2 = $this->input->post('fpekerjaansaksi2');
			$falamatsaksi2 = $this->input->post('falamatsaksi2');

			// Proses Update Surat
			$where = array('id_surat' => $id_surat);
			$fields_surat = array(

				'nik_pemohon' => $fnikayah,
				'nama_pemohon' => $fnamaayah
			);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);


			// Proses Simpan Data Pernyataan
			$fields_f02_akte = array(
				'am1_pekerjaan' => $fpekerjaanayah,
				'am1_kwn' => $fwniayah,
				'am1_agama' => $fagamaayah,
				'am1_alamat' => $falamatayah,
				'am1_nama_anak' => $fnamaanak,
				'am1_tempat_lahir_anak' => $ftmptlahiranak,
				'am1_tanggal_lahir_anak' => $ftgllahiranak,
				'am1_jam_lahir_anak' => $fjamlahiranak,
				'am1_anak_ke' => $fanakke,
				'am1_nama_ibu' => $fnamaibu,
				'am1_umur_ibu' => $fumuribu,
				'am1_alamat_ibu' => $falamatibu,
				'am1_nama_saksi1' => $fnamasaksi1,
				'am1_umur_saksi1' => $fumursaksi1,
				'am1_pekerjaan_saksi1' => $fpekerjaansaksi1,
				'am1_alamat_saksi1' => $falamatsaksi1,
				'am1_nama_saksi2' => $fnamasaksi2,
				'am1_umur_saksi2' => $fumursaksi2,
				'am1_pekerjaan_saksi2' => $fpekerjaansaksi2,
				'am1_alamat_saksi2' => $falamatsaksi2
			);
			$this->M_crud->updateData('tb_anak_mama', $fields_f02_akte, $where);
			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil mengubah data';

			redirect('layanan-akte-admin');
		}
	}

	public function lihatAnakmama1Akte($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$data2['anakmama'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_anak_mama ON tb_surat.id_surat = tb_anak_mama.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_lihatAnakmama1", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusAnakmama1Akte($id = null)
	{
		echo "tes";
	}


	public function anakmama2Akte()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_anakMama2");
		$this->load->view("admin/V_footer");
	}

	public function editAnakmama2Akte($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_editAnakmama2");
		$this->load->view("admin/V_footer");
	}

	public function lihatAnakmama2Akte($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1011;
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/akte/V_lihatAnakmama2");
		$this->load->view("admin/V_footer");
	}

	public function hapusAnakmama2Akte($id = null)
	{
		echo "tes";
	}

	public function printPengantarAkte($id = null)
	{
		if (!isset($id)) redirect('layanan-akte-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_akte_pengantar ON tb_surat.id_surat = tb_akte_pengantar.id_surat WHERE tb_surat.id_surat = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Pengantar Akte [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar

		$pdf->SetFont('Times', '', 12);
		// mencetak string 
		$pdf->Cell(180, 7, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(12);
		$pdf->Cell(100, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Yth. Bapak Kepala Dinas Kependudukan', 0, 1, 'R');
		$pdf->SetX(12);
		$pdf->SetFont('Times', '', 12);
		$cell = 'Perihal : ';
		$pdf->Cell(18, 7, $cell, 0, 'C');
		$pdf->SetFont('Times', 'B', 12);
		$boldCell = 'PERMOHONAN KEPUTUSAN KEPALA';
		$pdf->Cell(100, 7, $boldCell, 0, 0);
		$pdf->SetFont('Times', '', 12);
		$cell = ' Dan Pencatatan Sipil Kab. Jember  ';
		$pdf->Cell($pdf->GetStringWidth($cell), 7, $cell, 0, 1, 'C');
		$pdf->SetX(31);
		$pdf->SetFont('Times', 'B', 12);
		$cell = 'DISPENDUK DAN PENCAPIL TENTANG ';
		$pdf->Cell($pdf->GetStringWidth($cell), 7, $cell, 0, 0, 'C');
		$pdf->SetFont('Times', '', 12);
		$cell = 'di -';
		$pdf->Cell(45, 7, $cell, 0, 1, 'C');
		$pdf->SetX(31);
		$pdf->SetFont('Times', 'BU', 12);
		$cell = 'KETERLAMBATAN PENCATATAN KELAHIRAN            ';
		$pdf->Cell($pdf->GetStringWidth($cell), 7, $cell, 0, 0, 'C');
		$pdf->SetFont('Times', '', 12);
		$cell = 'JEMBER';
		$pdf->Cell($pdf->GetStringWidth($cell), 7, $cell, 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(168, 7, 'Sehubungan dengan keterlambatan Pelaporan/Pencatatan Kelahiran anak saya :', 0, 1, 'C');

		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'NAMA', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->ank_nama), 0, 1);
		$pdf->SetX(26);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'JENIS KELAMIN', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->ank_gender == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}
		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'TEMPAT KELAHIRAN', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->ank_tempat_lahir, 0, 1);
		$pdf->SetX(26);
		$day = date('D', strtotime($query->ank_tgl_lahir));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		$pdf->Cell(70, 10, 'HARI / TANGGAL', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, strtoupper($dayList[$day]) . ', ' . format_indo(date($query->ank_tgl_lahir)), 0, 1);
		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'JENIS KELAHIRAN', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->ank_jenis_kelahiran, 0, 1);
		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'ANAK KE-', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->ank_anak_ke, 0, 1);
		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'NAMA AYAH', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->ortu_nama_ayah, 0, 1);
		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'NAMA IBU', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->ortu_nama_ibu, 0, 1);
		$pdf->SetX(26);
		$pdf->Cell(70, 10, 'NAMA SAKSI 1', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->ortu_nama_saksi1, 0, 1);
		$pdf->SetX(26);
		$pdf->Cell(70, 5, 'NAMA SAKSI 2', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->ortu_nama_saksi2, 0, 1);
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		// ngatur footer 
		$pdf->SetX(26);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(170, 7, 'Adapun untuk memenuhi persyaratan pelaporan / pencatatan kelahiran anak saya ini telah kami lampirkan data pendukung sebagai berikut :', 0, 'FJ', 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(118, 7, '1. Isian Formulir Kelahiran (F2-01, F2-02, F2-03)', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(82, 7, '2. Isian Formulir Pernyataan', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(116, 7, '3. Surat Keterangan Lahir dari Desa / Kelurahan', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(140, 7, '4. Foto copy legalisir sesuai aslinya Surat Nikah / Perkawinan', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(157, 7, '5. Foto copy legalisir sesuai aslinya Kartu Tanda Penduduk Suami / Istri', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(177, 7, '6. Foto copy legalisir sesuai aslinya Kartu Keluarga (Nama Anak Sudah Tercantum)', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(145, 7, '7. Foto copy legalisir sesuai aslinya Ijazah bagi yang mempunyai', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(149, 7, '8. ........................................................................................................', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		// ngatur footer 
		$pdf->SetX(27);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(168, 7, 'Demikian surat permohonan ini saya buat dengan sebenar-benarnya agar dapatnya Bapak Kepala Dinas Kependudukan dan Pencatatan Sipil Kabupaten Jember berkenan untuk menerbitkan Akta Kelahiran anak saya ini, saya sampaikan terima kasih.', 0, 'FJ', 1);
		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(161, 7, 'PEMOHON', 0, 1, 'R');
		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(175, 7, strtoupper($query->nama_pemohon), 0, 1, 'R');

		$pdf->Output();
	}

	public function printPernyataanAkte($id = null)
	{
		if (!isset($id)) redirect('layanan-akte-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_akte_pengantar', 'tb_surat.id_surat = tb_akte_pengantar.id_surat');
		$this->db->join('tb_akte_pernyataan', 'tb_surat.id_surat = tb_akte_pernyataan.id_surat');
		$this->db->where('tb_surat.id_surat', $id);
		$query = $this->db->get()->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Pernyataan Akte [ ' . $id . ' ]');

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(200, 7, 'SURAT PERNYATAAN', 0, 1, 'C');

		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(120, 7, 'Yang bertanda tangan / Cap Jempol jari di bawah ini saya : ', 0, 1, 'C');

		$pdf->SetX(19);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Tempat, Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_tempat_lahir . ', ' . format_indo(date($query->pmh_tgl_lahir)), 0, 1);
		$pdf->SetX(19);
		$pdf->Cell(70, 10, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_pekerjaan, 0, 1);
		$pdf->SetX(19);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pmh_alamat, 0, 1);

		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(130, 7, 'Dengan ini menyatakan dengan sebenarnya bahwa : ', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);
		$pdf->SetFillColor(255, 255, 255);
		// ngatur footer 
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, '1. Dokumen / Data Pendukung Persyaratan Pencatatan Kelahiran pada Dinas Kependudukan dan Pencatatan Sipil Kabupaten Jember : Surat Keterangan Kelahiran dari Bidan / Dokter / Penolong Kelahiran, Surat Nikah / Akta Perkawinan, Kartu Tanda Penduduk, Kartu Keluarga, dan', 0, 'J', 1);
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, '2. Sampai saat ini saya belum pernah melaporkan dan mencatatkan kelahiran anak saya pada Dinas Kependudukan dan Pencatatan Sipil Kabupaten Jember atau Kantor Pencatatan Sipil di Kabupaten / Kota / Daerah Manapun', 0, 'J', 1);
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, '3. Adapun saksi kelahiran anak saya (Saksi dari keluarga) adalah :', 0, 'J', 1);
		$pdf->Cell(10, 2, '', 0, 1);

		$pdf->SetX(19);
		$pdf->Cell(25, 10, 'a. Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(60, 5, $query->nama_pemohon, 0, 'J', 1);
		$pdf->SetY(148);
		$pdf->SetX(109);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(25, 10, 'b. Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(60, 5, $query->nama_pemohon, 0, 'J', 1);

		$pdf->SetY(158);
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(25, 10, 'Alamat', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(60, 5, $query->saksi_alamat_1, 0, 'J', 0);
		$pdf->SetY(158);
		$pdf->SetX(109);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(25, 10, 'Alamat', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(60, 5, $query->saksi_alamat_2, 0, 'J', 1);

		$pdf->SetY(168);
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(55, 10, 'Status hubungan dengan bayi', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(30, 5, strtoupper($query->saksi_hub_1), 0, 'J', 0);
		$pdf->SetY(168);
		$pdf->SetX(109);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(55, 10, 'Status hubungan dengan bayi', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(30, 5, strtoupper($query->saksi_hub_2), 0, 'J', 1);

		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->SetFillColor(255, 255, 255);
		// ngatur footer 
		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Demikian surat pernyataan ini saya buat dengan sebenar-benarnya untuk keperluan Pelaporan dan Pencatatan Kelahiran anak saya yang ke .. ' . $query->ank_anak_ke, 0, 'J', 1);

		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->SetX(19);
		$pdf->Cell(25, 10, 'Bernama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->MultiCell(80, 5, $query->nama_pemohon, 0, 'J', 1);
		$pdf->SetY(201);
		$pdf->SetX(129);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(25, 10, 'lahir di', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(40, 5, $query->pmh_tempat_lahir, 0, 'J', 1);
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(19);
		$pdf->Cell(25, 5, 'Tanggal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(40, 5, format_indo(date($query->pmh_tgl_lahir)), 0, 'J', 1);
		$pdf->SetY(211);
		$pdf->SetX(89);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(110, 5, 'pada Dinas Kependudukan dan Pencatatan Sipil Kab. Jember', 0, 0);
		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Apabila dikemudian hari ternyata ini tidak benar saya siap bertanggung jawab dan menerima sangsi hukuman segala konsekuensinya sesuai peraturan yang berlaku.', 0, 'J', 1);

		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(175, 7, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(178, 7, 'Yang menyatakan (Orang Tua)', 0, 1, 'R');
		$pdf->Cell(10, 45, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(177, 7, $query->ortu_nama_ayah, 0, 1, 'R');

		$pdf->Output();
	}

	public function printF201Akte($id = null)
	{
		if (!isset($id)) redirect('layanan-akte-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_akte_pengantar', 'tb_surat.id_surat = tb_akte_pengantar.id_surat');
		$this->db->join('tb_akte_pernyataan', 'tb_surat.id_surat = tb_akte_pernyataan.id_surat');
		$this->db->join('tb_akte_f_dua', 'tb_surat.id_surat = tb_akte_f_dua.id_surat');
		$this->db->where('tb_surat.id_surat', $id);
		$query = $this->db->get()->row();

		$namakades = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='kades'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat F-2.01 Akte [ ' . $id . ' ]');

		// mencetak string
		$pdf->SetX(140);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(35, 7, 'Kode . F-2.01', 1, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Pemerintah Desa / Kelurahan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, 'Sidomulyo', 0, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, 'Ket :', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 1 : UPTD / Instansi Pelaksana', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, 'Semboro', 0, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 2 : Untuk yang bersangkutan', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Kabupaten / Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, 'Jember', 0, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 3 : Desa / Kelurahan', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Kode Wilayah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, '', 1, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 4 : Kecamatan', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(76);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(69, 5, 'SURAT KETERANGAN KELAHIRAN', 0, 0, 'C');
		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(120, 5, $query->kep_keluarga_nama, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Nomor Kartu Keluarga', 0, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->nomor_kk, 1, 0);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'BAYI ANAK', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '1. Nama', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(120, 5, $query->ank_nama, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '2. Jenis Kelamin', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		if ($query->ank_gender == "L") {
			$pdf->Cell(120, 5, 'Laki-laki', 1, 0);
		} else {
			$pdf->Cell(120, 5, 'Perempuan', 1, 0);
		}
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '3. Tempat dilahirkan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_tempat_lahir, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '4. Tempat Kelahiran', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_tempat_lahir, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '5. Hari dan Tanggal Lahir', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$day = date('D', strtotime($query->ank_tgl_lahir));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		$pdf->Cell(120, 5, strtoupper($dayList[$day]) . ', ' . format_indo(date($query->ank_tgl_lahir)), 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '6. Pukul', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_pukul_lahir . ' WIB', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '7. Jenis Kelahiran', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_jenis_kelahiran, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '8. Kelahiran ke', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_anak_ke, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '9. Penolong Kelahiran', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_penolong_lahir, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '10. Berat Bayi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_berat . ' kg', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '11.Panjang Bayi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ank_panjang . ' cm', 1, 0);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'IBU', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '1. NIK', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ibu_nik, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '2. Nama Lengkap', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(120, 5, $query->ortu_nama_ibu, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '3. Tanggal Lahir / Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(40, 5, format_indo(date($query->ibu_tgl_lahir)), 1, 0);

		$pdf->SetX(149);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->ibu_umur . ' Tahun', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '4. Pekerjaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ibu_pekerjaan, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '5. Alamat', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ibu_alamat, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa/Kelurahan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Sidomulyo', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'c. Kab/Kota', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jember', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Semboro', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'd. Provinsi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jawa Timur', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '6. Kewarganegaraan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ibu_kwn, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '7. Kebangsaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ibu_kebangsaan, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '8. Tgl. Pencatatan Perkawinan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(40, 5, format_indo(date($query->ibu_tgl_kawin)), 1, 0);
		$pdf->SetX(149);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->ibu_umur_kawin . ' Tahun', 1, 0);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'AYAH', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '1. NIK', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ayah_nik, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '2. Nama Lengkap', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(120, 5, $query->ortu_nama_ayah, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '3. Tanggal Lahir / Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(40, 5, format_indo(date($query->ayah_tgl_lahir)), 1, 0);

		$pdf->SetX(149);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->ayah_umur . ' Tahun', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '4. Pekerjaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ayah_pekerjaan, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '5. Alamat', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ayah_alamat, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa/Kelurahan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Sidomulyo', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'c. Kab/Kota', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jember', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Semboro', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'd. Provinsi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jawa Timur', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '6. Kewarganegaraan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ayah_kwn, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '7. Kebangsaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->ayah_kebangsaan, 1, 0);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'PELAPOR', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '1. NIK', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->plp_nik, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '2. Nama Lengkap', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(120, 5, $query->plp_nama, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '3. Tanggal Lahir / Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(40, 5, format_indo(date($query->pmh_tgl_lahir)), 1, 0);

		$pdf->SetX(149);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, $query->plp_umur . ' Tahun', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '4. Pekerjaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->plp_pekerjaan, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '5. Alamat', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->plp_alamat, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa/Kelurahan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Sidomulyo', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'c. Kab/Kota', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jember', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Semboro', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'd. Provinsi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jawa Timur', 1, 0);
		$pdf->Cell(10, 6, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'SAKSI 1', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '1. NIK', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->saksi1_nik, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '2. Nama Lengkap', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(120, 5, $query->saksi_nama_1, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '3. Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(40, 5, $query->saksi1_umur . ' Tahun', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '4. Pekerjaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->saksi1_pekerjaan, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '5. Alamat', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->saksi1_alamat, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa/Kelurahan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Sidomulyo', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'c. Kab/Kota', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jember', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Semboro', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'd. Provinsi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jawa Timur', 1, 0);

		$pdf->AddPage();

		$pdf->SetX(140);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(35, 7, 'Kode . F-2.01', 1, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Pemerintah Desa / Kelurahan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, 'Sidomulyo', 0, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, 'Ket :', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 1 : UPTD / Instansi Pelaksana', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, 'Semboro', 0, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 2 : Untuk yang bersangkutan', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Kabupaten / Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, 'Jember', 0, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 3 : Desa / Kelurahan', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'Kode Wilayah', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 5, '', 1, 0);
		$pdf->SetX(124);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(15, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Lembar 4 : Kecamatan', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(76);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(69, 5, 'SURAT KETERANGAN KELAHIRAN', 0, 0, 'C');
		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(60, 5, 'SAKSI 2', 0, 1);
		$pdf->Cell(10, 0, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '1. NIK', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->saksi2_nik, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '2. Nama Lengkap', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', 'B', 11);
		$pdf->Cell(120, 5, $query->saksi_nama_2, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '3. Umur', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(40, 5, $query->saksi2_umur . ' Tahun', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '4. Pekerjaan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->saksi2_pekerjaan, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(19);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 5, '5. Alamat', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(120, 5, $query->saksi2_alamat, 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'a. Desa/Kelurahan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Sidomulyo', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'c. Kab/Kota', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jember', 1, 0);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(79);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(35, 5, 'b. Kecamatan', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(20, 5, 'Semboro', 1, 0);
		$pdf->SetX(144);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(30, 5, 'd. Provinsi', 1, 0);
		$pdf->Cell(5, 5, ':', 1, 0);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(25, 5, 'Jawa Timur', 1, 0);
		$pdf->Cell(10, 18, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(80, 5, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 5, 'Kepala Desa/Lurah', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(113, 5, 'Pelapor', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 11);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 11);
		$pdf->Cell(68, 7, $namakades->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 11);
		$pdf->Cell(113, 7, $query->plp_nama, 0, 1, 'C');

		$pdf->Output();
	}

	public function printAnakMama1($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_anak_mama ON tb_surat.id_surat = tb_anak_mama.id_surat WHERE tb_surat.id_pengajuan = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form Anak Mama 1 [ ' . $id . ' ]');

		// ngatur judul surat
		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(200, 7, 'SURAT PERNYATAAN', 0, 1, 'C');
		$pdf->Cell(10, 15, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(140, 5, 'Yang bertanda tangan / Cap Jempol jari dibawah ini saya :', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 13);
		// $query = $this->db->get_where('tb_sk_usaha', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, $query->nama_pemohon, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->am1_pekerjaan, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->am1_kwn, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->am1_agama, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat Rumah', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, $query->am1_alamat, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(185, 7, 'Menyatakan bahwa sampai saat ini saya belum pernah mendaftarkan / melaksanakan dan atau mencatatkan perkawinan saya di catatan Sipil / Kantor Urusan Agama (KUA).', 0, 1, 'FJ', false);
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(200, 7, 'Demikian surat pernyataan saya buat dengan sebenarnya untuk keperluan pendaftaran atau', 0, 1, 'C');
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(130, 7, 'Pencatatan Kelahiran Anak saya yang ke ' . $query->am1_anak_ke . ' yang bernama :', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(175, 7, $query->am1_nama_anak, 0, 1, 'C', false);
		$pdf->Cell(10, 1, '', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Lahir di . ' . $query->am1_tempat_lahir_anak, 0, 0);
		$pdf->Cell(35, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'tanggal . ' . format_indo(date($query->am1_tanggal_lahir_anak)), 0, 1);
		$pdf->Cell(10, 2, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(176, 7, 'Di Badan Kependudukan Keluarga Berencana dan Catatan Sipil Kabupaten Jember.', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(168, 7, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(155, 7, 'Yang menyatakan', 0, 1, 'R');
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(150, 7, 'Materai 10.000', 0, 1, 'R');
		$pdf->Cell(10, 15, '', 0, 1);
		$pdf->SetX(130);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(80, 7, $query->nama_pemohon, 0, 1, 'L');

		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(185, 9, 'Mengetahui', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(185, 5, 'Kepala Desa / Lurah', 0, 0, 'C');
		$pdf->Cell(10, 30, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(185, 5, 'WASISO, S.IP', 0, 0, 'C');

		$pdf->Output();
	}

	public function printAnakMama2($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_anak_mama ON tb_surat.id_surat = tb_anak_mama.id_surat WHERE tb_surat.id_pengajuan = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Form Anak Mama 2 [ ' . $id . ' ]');

		// ngatur judul surat
		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(200, 7, 'LAPORAN KELAHIRAN DI LUAR NIKAH', 0, 1, 'C');
		$pdf->Cell(10, 15, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(140, 5, 'Yang bertanda tangan dibawah ini kami pemohon / pelapor :', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 13);
		// $query = $this->db->get_where('tb_sk_usaha', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, $query->nama_pemohon, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->am1_pekerjaan, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat Rumah', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, $query->am1_alamat, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(185, 7, 'Menerangkan bahwa telah lahir seorang anak laki-laki / perempuan pada :', 0, 1, 'FJ', false);
		$pdf->Cell(10, 2, '', 0, 1);

		$day = date('D', strtotime($query->am1_tanggal_lahir_anak));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);

		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Hari', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(27, 7, $dayList[$day] . ' /', 0, 0);
		$pdf->Cell(58, 7, 'Tanggal : ' . format_indo(date($query->am1_tanggal_lahir_anak)) . ' /', 0, 0);
		$pdf->Cell(50, 7, 'Jam : ' . $query->am1_jam_lahir_anak . ' WIB', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Lahir di', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->am1_tempat_lahir_anak, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Diberi Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, $query->am1_nama_anak, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Anak yang ke', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(55, 7, $query->am1_anak_ke, 0, 0);
		$pdf->Cell(75, 7, 'dilahirkan seorang ibu', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Nama (Ibu)', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, $query->am1_nama_ibu, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->am1_umur_ibu . ' Tahun', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat Rumah', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, $query->am1_alamat_ibu, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->MultiCell(105, 7, '......................................................................................', 0, 1);

		$pdf->Cell(10, 3, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(135, 7, 'Demikian laporan kelahiran ini saya buat dengan sebenarnya.', 0, 1, 'C');


		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(168, 7, 'Jember, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(139, 7, 'Pemohon', 0, 1, 'R');
		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetX(130);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(80, 7, $query->nama_pemohon, 0, 1, 'L');

		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(68, 5, 'SAKSI I', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(103, 5, 'SAKSI II', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(75, 7, $query->am1_nama_saksi1, 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(70, 7, $query->am1_nama_saksi2, 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(75, 7, $query->am1_umur_saksi1 . ' TAHUN', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(70, 7, $query->am1_umur_saksi2 . ' TAHUN', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(75, 7, $query->am1_pekerjaan_saksi1, 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(70, 7, $query->am1_pekerjaan_saksi1, 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$jumlah = 2;
		$hasil = implode(" ", array_slice(explode(" ", $query->am1_alamat_saksi1), 0, $jumlah));
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(75, 7, $hasil, 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(25, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$jumlah2 = 2;
		$hasil2 = implode(" ", array_slice(explode(" ", $query->am1_alamat_saksi2), 0, $jumlah));
		$pdf->Cell(70, 7, $hasil2, 0, 1);

		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetX(10);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, '(..............................................................................)', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(95, 7, '(..............................................................................)', 0, 1);

		$pdf->Output();
	}
}
