<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_laporan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["upload", "pdf"]);
	}

	public function index()
	{
		$data1['title'] = "Data Laporan Surat Keluar";
		$data1['menu_aktif'] = 12;
		$data1['submenu'] = 111;
		$data1['submenu2'] = 0;

		if (!isset($_SESSION['fBulanLapSK'])) {
			$_SESSION['fBulanLapSK'] = date('m');
		}
		if (!isset($_SESSION['fTahunLapSK'])) {
			$_SESSION['fTahunLapSK'] = date('Y');
		}
		if (!isset($_SESSION['fJenisLapSK'])) {
			$_SESSION['fJenisLapSK'] = 'semua';
		}

		if ($_SESSION['fJenisLapSK'] == 'semua') {
			$data2['surat'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.aktif = 1 AND MONTH(tb_surat.created_at) = '$_SESSION[fBulanLapSK]' AND YEAR(tb_surat.created_at) = '$_SESSION[fTahunLapSK]'")->result();
		} else {
			$data2['surat'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.aktif = 1 AND MONTH(tb_surat.created_at) = '$_SESSION[fBulanLapSK]' AND YEAR(tb_surat.created_at) = '$_SESSION[fTahunLapSK]' AND tb_surat.jenis_surat = '$_SESSION[fJenisLapSK]'")->result();
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/laporan/V_lap-surat-keluar", $data2);
		$this->load->view("admin/V_footer");
	}

	public function filter_lapSK()
	{
		$FBulan = $this->input->post('FBulan');
		$FTahun = $this->input->post('FTahun');
		$FJenis = $this->input->post('FJenis');

		unset($_SESSION['fBulanLapSK']);
		unset($_SESSION['fTahunLapSK']);
		unset($_SESSION['fJenisLapSK']);

		$_SESSION['fBulanLapSK'] = $FBulan;
		$_SESSION['fTahunLapSK'] = $FTahun;
		$_SESSION['fJenisLapSK'] = $FJenis;

		redirect('laporan-surat-keluar');
	}

	public function printLaporanSk()
	{
		date_default_timezone_set('Asia/Jakarta');

		if (!isset($_SESSION['fBulanLapSK'])) {
			$_SESSION['fBulanLapSK'] = date('m');
		}
		if (!isset($_SESSION['fTahunLapSK'])) {
			$_SESSION['fTahunLapSK'] = date('Y');
		}
		if (!isset($_SESSION['fJenisLapSK'])) {
			$_SESSION['fJenisLapSK'] = 'semua';
		}

		if ($_SESSION['fJenisLapSK'] == 'semua') {
			$data_surat = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.aktif = 1 AND MONTH(tb_surat.created_at) = '$_SESSION[fBulanLapSK]' AND YEAR(tb_surat.created_at) = '$_SESSION[fTahunLapSK]'")->result();
		} else {
			$data_surat = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.aktif = 1 AND MONTH(tb_surat.created_at) = '$_SESSION[fBulanLapSK]' AND YEAR(tb_surat.created_at) = '$_SESSION[fTahunLapSK]' AND tb_surat.jenis_surat = '$_SESSION[fJenisLapSK]'")->result();
		}
		$no = 1;
		$batas = 20;

		$pdf = new FPDF('L', 'mm', 'Legal');
		$pdf->AliasNbPages();

		// membuat halaman baru
		$pdf->AddPage();

		$pdf->SetFont('Arial', 'I', 9);
		//nomor halaman
		$pdf->Cell(0, 10, 'Halaman ' . $pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Laporan Surat Keluar');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 100, 8, -600);
		$pdf->SetFont('Times', 'B', 18);
		// mencetak string
		$pdf->Cell(370, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(370, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 20);
		$pdf->Cell(370, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(370, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 42, 350 - 10, 42);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 20);
		$pdf->Cell(368, 7, 'LAPORAN SURAT KELUAR', 0, 1, 'C');

		$pdf->Cell(10, 10, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 14);
		$pdf->Cell(15, 6, 'NO', 1, 0, 'C');
		$pdf->Cell(60, 6, 'TANGGAL PENGAJUAN', 1, 0, 'C');
		$pdf->Cell(50, 6, 'ID PENGAJUAN', 1, 0, 'C');
		$pdf->Cell(106, 6, 'NAMA PEMOHON', 1, 0, 'C');
		$pdf->Cell(55, 6, 'JENIS SURAT', 1, 0, 'C');
		$pdf->Cell(40, 6, 'KETERANGAN', 1, 1, 'C');

		foreach ($data_surat as $key => $value) {
			$pdf->SetX(15);
			$pdf->SetFont('Times', '', 14);
			$pdf->Cell(15, 6, $no, 1, 0, 'C');
			$pdf->Cell(60, 6, format_indo(date("Y-m-d", strtotime($value->created_at))), 1, 0, 'C');
			$pdf->Cell(50, 6, $value->id_pengajuan, 1, 0, 'C');
			$pdf->Cell(106, 6, $value->nama_pemohon, 1, 0, 'C');
			$pdf->Cell(55, 6, ucfirst($value->jenis_surat), 1, 0, 'C');
			$pdf->Cell(40, 6, $value->status_pembuatan, 1, 1, 'C');

			if ($pdf->PageNo() == 2) {
				$batas = 25;
			}
			if ($no % $batas == 0) {

				// add baris baru
				$pdf->AddPage();
				$pdf->SetFont('Arial', 'I', 9);
				//nomor halaman
				$pdf->Cell(0, 10, 'Halaman ' . $pdf->PageNo() . ' dari {nb}', 0, 0, 'R');

				// ngatur judul surat
				$pdf->Cell(10, 10, '', 0, 1);
				$pdf->SetFont('Times', 'BU', 20);
				$pdf->Cell(368, 7, 'LAPORAN SURAT KELUAR', 0, 1, 'C');

				$pdf->Cell(10, 10, '', 0, 1);

				$pdf->SetX(15);
				$pdf->SetFont('Times', '', 14);
				$pdf->Cell(15, 6, 'NO', 1, 0, 'C');
				$pdf->Cell(60, 6, 'TANGGAL PENGAJUAN', 1, 0, 'C');
				$pdf->Cell(50, 6, 'ID PENGAJUAN', 1, 0, 'C');
				$pdf->Cell(106, 6, 'NAMA PEMOHON', 1, 0, 'C');
				$pdf->Cell(55, 6, 'JENIS SURAT', 1, 0, 'C');
				$pdf->Cell(40, 6, 'KETERANGAN', 1, 1, 'C');
			}
			$no++;
		}

		// foreach ($data_surat as $key => $value) {
		// 	$pdf->SetX(15);
		// 	$pdf->SetFont('Times', '', 14);
		// 	$pdf->Cell(15, 6, $no, 1, 0, 'C');
		// 	$pdf->Cell(60, 6, format_indo(date("Y-m-d", strtotime($value->created_at))), 1, 0, 'C');
		// 	$pdf->Cell(50, 6, $value->id_pengajuan, 1, 0, 'C');
		// 	$pdf->Cell(106, 6, $value->nama_pemohon, 1, 0, 'C');
		// 	$pdf->Cell(55, 6, ucfirst($value->jenis_surat), 1, 0, 'C');
		// 	$pdf->Cell(40, 6, $value->status_pembuatan, 1, 1, 'C');
		//
		// 	if($pdf->PageNo() > 1){
		// 		$batas = 45;
		// 	}
		// 	if($no % $batas == 0){
		//
		// 		// add baris baru
		// 		$pdf->AddPage();
		// 		$pdf->SetFont('Arial', 'I', 9);
		// 		//nomor halaman
		// 		$pdf->Cell(0, 10, 'Halaman ' . $pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
		//
		// 		// ngatur judul surat
		// 		$pdf->Cell(10, 10, '', 0, 1);
		// 		$pdf->SetFont('Times', 'BU', 20);
		// 		$pdf->Cell(368, 7, 'LAPORAN SURAT KELUAR', 0, 1, 'C');
		//
		// 		$pdf->Cell(10, 10, '', 0, 1);
		//
		// 		$pdf->SetX(15);
		// 		$pdf->SetFont('Times', '', 14);
		// 		$pdf->Cell(15, 6, 'NO', 1, 0, 'C');
		// 		$pdf->Cell(60, 6, 'TANGGAL PENGAJUAN', 1, 0, 'C');
		// 		$pdf->Cell(50, 6, 'ID PENGAJUAN', 1, 0, 'C');
		// 		$pdf->Cell(106, 6, 'NAMA PEMOHON', 1, 0, 'C');
		// 		$pdf->Cell(55, 6, 'JENIS SURAT', 1, 0, 'C');
		// 		$pdf->Cell(40, 6, 'KETERANGAN', 1, 1, 'C');
		//
		// 	} $no++;
		// }
		//
		// foreach ($data_surat as $key => $value) {
		// 	$pdf->SetX(15);
		// 	$pdf->SetFont('Times', '', 14);
		// 	$pdf->Cell(15, 6, $no, 1, 0, 'C');
		// 	$pdf->Cell(60, 6, format_indo(date("Y-m-d", strtotime($value->created_at))), 1, 0, 'C');
		// 	$pdf->Cell(50, 6, $value->id_pengajuan, 1, 0, 'C');
		// 	$pdf->Cell(106, 6, $value->nama_pemohon, 1, 0, 'C');
		// 	$pdf->Cell(55, 6, ucfirst($value->jenis_surat), 1, 0, 'C');
		// 	$pdf->Cell(40, 6, $value->status_pembuatan, 1, 1, 'C');
		//
		// 	if($pdf->PageNo() > 1){
		// 		$batas = 45;
		// 	}
		// 	if($no % $batas == 0){
		//
		// 		// add baris baru
		// 		$pdf->AddPage();
		// 		$pdf->SetFont('Arial', 'I', 9);
		// 		//nomor halaman
		// 		$pdf->Cell(0, 10, 'Halaman ' . $pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
		//
		// 		// ngatur judul surat
		// 		$pdf->Cell(10, 10, '', 0, 1);
		// 		$pdf->SetFont('Times', 'BU', 20);
		// 		$pdf->Cell(368, 7, 'LAPORAN SURAT KELUAR', 0, 1, 'C');
		//
		// 		$pdf->Cell(10, 10, '', 0, 1);
		//
		// 		$pdf->SetX(15);
		// 		$pdf->SetFont('Times', '', 14);
		// 		$pdf->Cell(15, 6, 'NO', 1, 0, 'C');
		// 		$pdf->Cell(60, 6, 'TANGGAL PENGAJUAN', 1, 0, 'C');
		// 		$pdf->Cell(50, 6, 'ID PENGAJUAN', 1, 0, 'C');
		// 		$pdf->Cell(106, 6, 'NAMA PEMOHON', 1, 0, 'C');
		// 		$pdf->Cell(55, 6, 'JENIS SURAT', 1, 0, 'C');
		// 		$pdf->Cell(40, 6, 'KETERANGAN', 1, 1, 'C');
		//
		// 	} $no++;
		// }

		$pdf->Output();
	}
}
