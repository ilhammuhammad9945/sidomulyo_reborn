<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_rekamktp extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library('pdf');
	}

	public function index()
	{
		$data1['title'] = "Data Rekam KTP";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10111;

		$data2['rekam_ktp'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.jenis_surat = 'rekamktp' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rekamktp/V_indexRekamKtp", $data2);
		$this->load->view("admin/V_footer");
	}

	public function addRekamKtp()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10111;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rekamktp/V_add-rekamktp", $x);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanAdd()
	{
		if (isset($_POST['btnSimpanRekamKtp'])) {

			$id_peng = $this->input->post('id_peng');
			$rk_nik = $this->input->post('rk_nik');
			$rk_nama_lengkap = $this->input->post('rk_nama_lengkap');
			$rk_gender = $this->input->post('rk_gender');
			$rk_tempat_lahir = $this->input->post('rk_tempat_lahir');
			$rk_tanggal_lahir = $this->input->post('rk_tanggal_lahir');
			$rk_agama = $this->input->post('rk_agama');
			$rk_pekerjaan = $this->input->post('rk_pekerjaan');
			$rk_pendidikan = $this->input->post('rk_pendidikan');
			$rk_status_kawin = $this->input->post('rk_status_kawin');
			$rk_kebutuhan = $this->input->post('rk_kebutuhan');
			$rk_alamat = $this->input->post('rk_alamat');


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'rekamktp',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $rk_nik,
				'nama_pemohon' => $rk_nama_lengkap,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'rk_gender' => $rk_gender,
				'rk_tempat_lahir' => $rk_tempat_lahir,
				'rk_tanggal_lahir' => $rk_tanggal_lahir,
				'rk_agama' => $rk_agama,
				'rk_pekerjaan' => $rk_pekerjaan,
				'rk_pendidikan' => $rk_pendidikan,
				'rk_status_kawin' => $rk_status_kawin,
				'rk_kebutuhan' => $rk_kebutuhan,
				'rk_alamat' => $rk_alamat
			);
			$this->M_crud->simpanData('tb_rekam_ktp', $fields_p_akte);


			redirect('layanan-rekam-ktp-admin');
		}
	}

	public function editRekamKtp($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10111;

		$data2['rekam_ktp'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_rekam_ktp ON tb_surat.id_surat = tb_rekam_ktp.id_surat WHERE tb_surat.jenis_surat = 'rekamktp' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rekamktp/V_edit-rekamktp", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesSimpanEdit()
	{
		if (isset($_POST['btnEditRekamKtp'])) {

			$id_surat = $this->input->post('id_surat');
			$rk_nik = $this->input->post('rk_nik');
			$rk_nama_lengkap = $this->input->post('rk_nama_lengkap');
			$rk_gender = $this->input->post('rk_gender');
			$rk_tempat_lahir = $this->input->post('rk_tempat_lahir');
			$rk_tanggal_lahir = $this->input->post('rk_tanggal_lahir');
			$rk_agama = $this->input->post('rk_agama');
			$rk_pekerjaan = $this->input->post('rk_pekerjaan');
			$rk_pendidikan = $this->input->post('rk_pendidikan');
			$rk_status_kawin = $this->input->post('rk_status_kawin');
			$rk_kebutuhan = $this->input->post('rk_kebutuhan');
			$rk_alamat = $this->input->post('rk_alamat');

			$where = array('id_surat' => $id_surat);

			// Proses Simpan Surat
			$fields_surat = array(
				'nik_pemohon' => $rk_nik,
				'nama_pemohon' => $rk_nama_lengkap
			);
			$id_surat = $this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'rk_gender' => $rk_gender,
				'rk_tempat_lahir' => $rk_tempat_lahir,
				'rk_tanggal_lahir' => $rk_tanggal_lahir,
				'rk_agama' => $rk_agama,
				'rk_pekerjaan' => $rk_pekerjaan,
				'rk_pendidikan' => $rk_pendidikan,
				'rk_status_kawin' => $rk_status_kawin,
				'rk_kebutuhan' => $rk_kebutuhan,
				'rk_alamat' => $rk_alamat
			);
			$this->M_crud->updateData('tb_rekam_ktp', $fields_p_akte, $where);


			redirect('layanan-rekam-ktp-admin');
		}
	}

	public function lihatRekamKtp($id)
	{
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10111;

		$data2['rekam_ktp'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_rekam_ktp ON tb_surat.id_surat = tb_rekam_ktp.id_surat WHERE tb_surat.jenis_surat = 'rekamktp' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/rekamktp/V_lihatRekamKtp", $data2);
		$this->load->view("admin/V_footer");
	}

	public function doneRekamKtp($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		$this->session->set_flashdata('success', 'Data berhasil diupdate');
		redirect('layanan-rekam-ktp-admin');
	}

	public function hapusRekamKtp($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-rekam-ktp-admin');
	}

	public function printRekamKtp($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_rekam_ktp ON tb_surat.id_surat = tb_rekam_ktp.id_surat WHERE tb_surat.jenis_surat = 'rekamktp' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Telah Rekam KTP [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini, menerangkan dengan sebenarnya bahwa penduduk :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Nama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 7, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'NIK', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		if ($query->rk_gender == "L") {
			$pdf->Cell(85, 7, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 7, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->rk_tempat_lahir . ', ' . format_indo(date($query->rk_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->rk_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->rk_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Pendidikan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->rk_pendidikan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->rk_status_kawin, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->Cell(85, 7, $query->rk_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, '', 0, 0);
		$pdf->Cell(5, 7, '', 0, 0);
		$pdf->Cell(85, 7, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(185, 9, 'Telah melaksanakan perekaman Kartu Tanda Penduduk Elektronik (KTP-el)', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Berdasarkan Surat Edaran Menteri Dalam Negeri Nomor : 471.13/5184/SJ tanggal 13 Desember 2012, bahwa bagi penduduk yang telah melakukan perekaman KTP-el dan sebelum yang bersangkutan menerima KTP-elektronik', 0, 1, 'FJ', false);
		$pdf->Cell(10, 3, '', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Surat Keterangan ini berfungsi sebagai pengganti Kartu Tanda Penduduk Elektronik (KTP-el) Untuk ' . $query->rk_kebutuhan, 0, 1, 'FJ', false);


		$pdf->Cell(10, 3, '', 0, 1);
		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(190, 7, 'Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Pemohon', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $query->nama_pemohon, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, 'WASISO, S.IP', 0, 1, 'C');

		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(185, 9, 'Mengetahui,', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(185, 5, 'CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(10, 35, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(185, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(185, 5, 'NIP. 19640102 199305 1 001', 0, 0, 'C');


		$pdf->Output();
	}
}
