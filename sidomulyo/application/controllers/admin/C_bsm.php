<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_bsm extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["form_validation", "pdf"]);
	}

	public function rules()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],
			[
				'field' => 'fnik',
				'label' => 'NIK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fumur',
				'label' => 'Umur',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpenghasilan',
				'label' => 'Penghasilan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamaanak',
				'label' => 'Nama Anak',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fsekolah',
				'label' => 'Sekolah',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],


		];
	}

	public function index()
	{

		$data1['title'] = "Data BSM";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10112;
		$data2["bsm"] = $this->M_crud->getQuery("SELECT sr.*, bs.* FROM tb_surat AS sr JOIN tb_bsm AS bs ON sr.id_surat=bs.id_surat WHERE sr.jenis_surat ='bsm' AND sr.aktif=1 ORDER BY sr.id_surat DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bsm/V_indexBsm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahBsm()
	{

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		date_default_timezone_set('Asia/Jakarta');
		// $tglskrg = date('Y-m-d');

		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10112;
		// $x['invoice'] = $this->M_layanan_surat->get_id_domisili();
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$namakades = $this->M_crud->getQuery("SELECT nama_apt FROM tb_aparatur WHERE jabatan_apt=1")->row();

		if ($validation->run()) {
			$idpengajuan = $this->input->post('fidpengajuanbsm');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$umur = $this->input->post('fumur');
			$jkel = $this->input->post('fjkel');
			$agama = $this->input->post('fagama');
			$statuskawin = $this->input->post('fstatuskawin');
			$pekerjaan = $this->input->post('fpekerjaan');
			$penghasilan = $this->input->post('fpenghasilan');
			$alamat = $this->input->post('falamat');
			$namaanak = $this->input->post('fnamaanak');
			$sekolah = $this->input->post('fsekolah');

			$dataMaster = array(
				'jenis_surat' => 'bsm',
				// 'tgl_surat' => $tglskrg,
				// 'no_surat' => $nosurat,
				'id_pengajuan' => $idpengajuan,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $dataMaster);

			$dataTurunan = array(
				'id_surat' => $id_surat,
				'bsm_umur' => $umur,
				'bsm_jkel' => $jkel,
				'bsm_agama' => $agama,
				'bsm_status_kwn' => $statuskawin,
				'bsm_pekerjaan' => $pekerjaan,
				'bsm_penghasilan' => $penghasilan,
				'bsm_alamat' => $alamat,
				'bsm_nama_ank' => $namaanak,
				'bsm_sekolah' => $sekolah,
				'bsm_kades' => $namakades->nama_apt,
			);
			$this->M_crud->simpanData('tb_bsm', $dataTurunan);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('layanan-bsm-admin');

			// $cek_nosurat = $this->M_crud->getQuery("SELECT no_surat FROM tb_sk_domisili WHERE no_surat='" . $nosurat . "'")->row();
			// Kalau no surat sudah ada
			// if ($cek_nosurat > 0) 
			// 	$this->session->set_flashdata('error', 'Nomor surat sudah ada, gunakan nomor lain');
			// 	redirect('tambah-domisili');
			// 
			// Kalau no surat tidak ada, inputkan data ke tabel
			// else 


			// 
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bsm/V_tambahBsm", $x);
		$this->load->view("admin/V_footer");
	}

	public function editBsm($id = null)
	{
		if (!isset($id)) redirect('layanan-bsm-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());

		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10112;

		$data2["bsm"] = $this->M_crud->getQuery("SELECT sr.*, bs.* FROM tb_surat AS sr JOIN tb_bsm AS bs ON sr.id_surat=bs.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["bsm"]) show_404();

		if ($validation->run()) {
			$idsurat = $this->input->post('fidsurat');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$umur = $this->input->post('fumur');
			$jkel = $this->input->post('fjkel');
			$agama = $this->input->post('fagama');
			$statuskawin = $this->input->post('fstatuskawin');
			$pekerjaan = $this->input->post('fpekerjaan');
			$penghasilan = $this->input->post('fpenghasilan');
			$alamat = $this->input->post('falamat');
			$namaanak = $this->input->post('fnamaanak');
			$sekolah = $this->input->post('fsekolah');

			$dataMaster = array(
				// 'no_surat' => $nosurat,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap
			);

			$where = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_surat', $dataMaster, $where);

			$dataTurunan = array(
				'bsm_umur' => $umur,
				'bsm_jkel' => $jkel,
				'bsm_agama' => $agama,
				'bsm_status_kwn' => $statuskawin,
				'bsm_pekerjaan' => $pekerjaan,
				'bsm_penghasilan' => $penghasilan,
				'bsm_alamat' => $alamat,
				'bsm_nama_ank' => $namaanak,
				'bsm_sekolah' => $sekolah
			);

			$where2 = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_bsm', $dataTurunan, $where2);

			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('layanan-bsm-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bsm/V_editBsm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ubahBsm()
	{

		$idsurat = $this->input->post('fid');
		$ket = $this->input->post('fstatus_pembuatan');

		$data = array(
			'tgl_surat' => date('Y-m-d'),
			'status_pembuatan' => $ket

		);

		$where = array(
			'id_surat' => $idsurat
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Berhasil diupdate');
		redirect('layanan-bsm-admin');
	}

	public function lihatBsm($id = null)
	{
		if (!isset($id)) redirect('layanan-bsm-admin');

		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10112;
		$data2["bsm"] = $this->M_crud->getQuery("SELECT sr.*, bs.* FROM tb_surat AS sr JOIN tb_bsm AS bs ON sr.id_surat=bs.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["bsm"]) show_404();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/bsm/V_lihatBsm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusBsm($id = null)
	{
		if (!isset($id)) show_404();

		// ini kodingan hapus soft
		$data = array(
			'aktif' => 0
		);

		$where = array(
			'id_surat' => $id
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		redirect('layanan-bsm-admin');
	}

	public function printBsm($id = null)
	{
		if (!isset($id)) redirect('layanan-bsm-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_bsm', 'tb_surat.id_surat = tb_bsm.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Bantuan Siswa Miskin [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(80, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_usaha', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Umur', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, strtoupper($query->bsm_umur) . ' Tahun', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'NIK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->bsm_jkel == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}

		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Agama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->bsm_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->bsm_status_kwn, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->bsm_pekerjaan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Penghasilan per Bulan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, 'Rp. ' . rupiah($query->bsm_penghasilan) . ',-', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->bsm_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		// ngatur footer 
		$pdf->SetX(14);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang bersangkutan adalah benar-benar penduduk Desa Sidomulyo yang beralamat seperti tersebut di atas, dan berkehidupan sehari-hari kurang mampu.', 0, 1, 'FJ', false);
		$pdf->SetX(14);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Surat keterangan ini diberikan untuk Persyaratan mengajukan Bantuan Siswa Miskin (BSM) bagi anaknya', 0, 1, 'FJ', false);

		$pdf->SetX(14);
		$pdf->Cell(26, 7, 'yang bernama', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(115, 7, strtoupper($query->bsm_nama_ank), 0, 1);
		$pdf->SetX(14);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'yang saat ini sekolah di ' . $query->bsm_sekolah, 0, 1, 'FJ', false);
		$pdf->Cell(10, 5, '', 0, 1);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(176, 7, 'Demikian surat keterangan ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(185, 7, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(188, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'R');
		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(172, 7, strtoupper($query->bsm_kades), 0, 1, 'R');

		$pdf->Output();
	}
}
