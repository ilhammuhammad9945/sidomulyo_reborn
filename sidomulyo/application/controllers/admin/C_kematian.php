<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_kematian extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library('pdf');
	}

	public function index()
	{
		$data1['title'] = "Data Surat Kematian";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1014;

		$data2['meninggal'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.jenis_surat = 'kematian' AND tb_surat.aktif = 1")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kematian/V_indexKematian", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahKematian()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1014;
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kematian/V_tambahKematian", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanKematian()
	{
		if (isset($_POST['btnSimpanKematian'])) {
			$id_peng = $this->input->post('id_peng');

			//Data Pemohon
			$kematian_nik = $this->input->post('kematian_nik');
			$kematian_nama_lengkap = $this->input->post('kematian_nama_lengkap');
			$kematian_umur = $this->input->post('kematian_umur');
			$kematian_gender = $this->input->post('kematian_gender');
			$kematian_tanggal = $this->input->post('kematian_tanggal');
			$kematian_tempat = $this->input->post('kematian_tempat');
			$kematian_alamat = $this->input->post('kematian_alamat');
			$kematian_penyebab = $this->input->post('kematian_penyebab');
			// $now = date('Y-m-d');

			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'kematian',
				// 'tgl_surat' => $now,
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $kematian_nik,
				'nama_pemohon' => $kematian_nama_lengkap,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Kematian
			$fields_sktm = array(
				'id_surat' => $id_surat,
				'umur_meninggal' => $kematian_umur,
				'gender_meninggal' => $kematian_gender,
				'tanggal_meninggal' => $kematian_tanggal,
				'tempat_meninggal' => $kematian_tempat,
				'alamat_meninggal' => $kematian_alamat,
				'penyebab_meninggal' => $kematian_penyebab
			);
			$this->M_crud->simpanData('tb_surat_kematian', $fields_sktm);

			redirect('layanan-surat-kematian-admin');
		}
	}

	public function editKematian($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1014;

		$data2['surat'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_surat_kematian ON tb_surat.id_surat = tb_surat_kematian.id_surat WHERE tb_surat.id_surat = '$id' AND tb_surat.jenis_surat = 'kematian' AND tb_surat.aktif = 1")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kematian/V_editKematian", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesEditKematian()
	{
		if (isset($_POST['btnSimpanEditKematian'])) {

			//Data Pemohon
			$id_surat = $this->input->post('id_surat');
			$kematian_nik = $this->input->post('kematian_nik');
			$kematian_nama_lengkap = $this->input->post('kematian_nama_lengkap');
			$kematian_umur = $this->input->post('kematian_umur');
			$kematian_gender = $this->input->post('kematian_gender');
			$kematian_tanggal = $this->input->post('kematian_tanggal');
			$kematian_tempat = $this->input->post('kematian_tempat');
			$kematian_alamat = $this->input->post('kematian_alamat');
			$kematian_penyebab = $this->input->post('kematian_penyebab');

			// Proses update Surat
			$fields_surat = array(
				'nik_pemohon' => $kematian_nik,
				'nama_pemohon' => $kematian_nama_lengkap
			);

			$where = array('id_surat' => $id_surat);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses update Form sktm
			$fields_p_akte = array(
				'umur_meninggal' => $kematian_umur,
				'gender_meninggal' => $kematian_gender,
				'tanggal_meninggal' => $kematian_tanggal,
				'tempat_meninggal' => $kematian_tempat,
				'alamat_meninggal' => $kematian_alamat,
				'penyebab_meninggal' => $kematian_penyebab
			);
			$this->M_crud->updateData('tb_surat_kematian', $fields_p_akte, $where);

			redirect('layanan-surat-kematian-admin');
		}
	}

	public function lihatKematian($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1014;

		$data2['kematian'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_surat_kematian ON tb_surat.id_surat = tb_surat_kematian.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kematian/V_lihatKematian", $data2);
		$this->load->view("admin/V_footer");
	}

	public function doneKematian($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-surat-kematian-admin');
	}


	public function hapusKematian($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-surat-kematian-admin');
	}

	public function printKematian($id = null)
	{
		if (!isset($id)) redirect('layanan-surat-kematian-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_surat_kematian', 'tb_surat.id_surat = tb_surat_kematian.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Kematian [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN KEMATIAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 7, 'menerangkan bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_surat_kematian', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(30);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(30);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'NIK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nik_pemohon, 0, 1);
		$pdf->SetX(30);
		$pdf->Cell(70, 10, 'Umur', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->umur_meninggal . ' Tahun', 0, 1);
		$pdf->SetX(30);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->gender_meninggal == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}

		$pdf->SetX(30);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->alamat_meninggal, 0, 1);
		$pdf->SetX(30);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->SetX(30);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, '', 0, 1);

		$pdf->SetX(30);
		$pdf->Cell(70, 5, 'Telah meninggal pada', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 15, '', 0, 1);

		$pdf->SetX(30);
		$day = date('D', strtotime($query->tanggal_meninggal));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		$pdf->Cell(70, 10, 'Hari', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($dayList[$day]), 0, 1);

		$pdf->SetX(30);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Tanggal', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, format_indo(date($query->tanggal_meninggal)), 0, 1);
		$pdf->SetX(30);
		$pdf->Cell(70, 10, 'Di', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->tempat_meninggal, 0, 1);
		$pdf->SetX(30);
		$pdf->Cell(70, 10, 'Disebabkan karena', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->penyebab_meninggal, 0, 1);

		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(173, 7, 'Demikian surat keterangan ini dibuat dengan sebenarnya untuk menjadikan periksa.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);


		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(185, 7, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(178, 7, 'Kepala Desa Sidomulyo', 0, 1, 'R');
		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(172, 7, 'WASISO, S.IP', 0, 1, 'R');

		$pdf->Output();
	}
}
