<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_berpergian extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["pdf"]);
	}

	public function index()
	{
		$data1['title'] = "Data Berpergian";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10114;

		$data2['berpergian'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_berpergian ON tb_surat.id_surat = tb_berpergian.id_surat WHERE tb_surat.jenis_surat = 'berpergian' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/berpergian/V_indexBerpergian", $data2);
		$this->load->view("admin/V_footer");
	}

	public function addBerpergian()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10114;

		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/berpergian/V_add-berpergian", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanData()
	{
		if (isset($_POST['btnsimpanberpergian'])) {

			$id_peng = $this->input->post('id_peng');
			$fgenderpemohon = $this->input->post('fgenderpemohon');
			$fnamapemohon = $this->input->post('fnamapemohon');
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fkewarganegaraanpemohon = $this->input->post('fkewarganegaraanpemohon');
			$fagamapemohon = $this->input->post('fagamapemohon');
			$fstatusnikahpemohon = $this->input->post('fstatusnikahpemohon');
			$fpendidikanpemohon = $this->input->post('fpendidikanpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$falamatpemohon = $this->input->post('falamatpemohon');
			$fktppemohon = $this->input->post('fktppemohon');
			$ftujuanke = $this->input->post('ftujuanke');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$ftglberangkat = $this->input->post('ftglberangkat');
			$falasanpergi = $this->input->post('falasanpergi');
			$fpengikut = $this->input->post('fpengikut');


			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'berpergian',
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $fktppemohon,
				'nama_pemohon' => $fnamapemohon,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'id_surat' => $id_surat,
				'pergi_gender' => $fgenderpemohon,
				'pergi_tempat_lahir' => $ftmptlahirpemohon,
				'pergi_tanggal_lahir' => $ftgllahirpemohon,
				'pergi_kwn' => $fkewarganegaraanpemohon,
				'pergi_agama' => $fagamapemohon,
				'pergi_status_kawin' => $fstatusnikahpemohon,
				'pergi_pekerjaan' => $fpekerjaanpemohon,
				'pergi_pendidikan' => $fpendidikanpemohon,
				'pergi_alamat_asal' => $falamatpemohon,
				'pergi_tujuan' => $ftujuanke,
				'pergi_tujuan_desa' => $fdesa,
				'pergi_tujuan_kecamatan' => $fkecamatan,
				'pergi_tujuan_kab' => $fkabkota,
				'pergi_tujuan_provinsi' => $fprovinsi,
				'pergi_tanggal_berangkat' => $ftglberangkat,
				'pergi_alasan' => $falasanpergi,
				'pergi_pengikut' => $fpengikut
			);
			$this->M_crud->simpanData('tb_berpergian', $fields_p_akte);

			$jumlah = count($_POST['nik_pengikut']);
			for ($i = 0; $i < $jumlah; $i++) {
				$nik_pengikut = $_POST['nik_pengikut'][$i];
				$nama_pengikut = $_POST['nama_pengikut'][$i];
				$gender_pengikut = $_POST['gender_pengikut'][$i];
				$umur_pengikut = $_POST['umur_pengikut'][$i];
				$status_nikah_pengikut = $_POST['status_nikah_pengikut'][$i];
				$pendidikan_pengikut = $_POST['pendidikan_pengikut'][$i];
				$keterangan_pengikut = $_POST['keterangan_pengikut'][$i];

				$fields3 = array(
					'id_surat' => $id_surat,
					'pengikut_nama' => $nama_pengikut,
					'pengikut_gender' => $gender_pengikut,
					'pengikut_umur' => $umur_pengikut,
					'pengikut_status_kawin' => $status_nikah_pengikut,
					'pengikut_pendidikan' => $pendidikan_pengikut,
					'pengikut_nik' => $nik_pengikut,
					'pengikut_keterangan' => $keterangan_pengikut
				);

				$this->M_crud->simpanData('tb_pengikut', $fields3);
			}

			redirect('layanan-SKberpergian-admin');
		}
	}

	public function editBerpergian($id)
	{
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10114;

		$data2['berpergian'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_berpergian ON tb_surat.id_surat = tb_berpergian.id_surat WHERE tb_surat.jenis_surat = 'berpergian' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$data2['pengikut'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = '$id'")->result();

		$data2['no'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/berpergian/V_editBerpergian", $data2);
		$this->load->view("admin/V_footer");
	}

	public function simpanEditData()
	{
		if (isset($_POST['btneditberpergian'])) {

			$id_surat = $this->input->post('id_surat');
			$fgenderpemohon = $this->input->post('fgenderpemohon');
			$fnamapemohon = $this->input->post('fnamapemohon');
			$ftmptlahirpemohon = $this->input->post('ftmptlahirpemohon');
			$ftgllahirpemohon = $this->input->post('ftgllahirpemohon');
			$fkewarganegaraanpemohon = $this->input->post('fkewarganegaraanpemohon');
			$fagamapemohon = $this->input->post('fagamapemohon');
			$fstatusnikahpemohon = $this->input->post('fstatusnikahpemohon');
			$fpendidikanpemohon = $this->input->post('fpendidikanpemohon');
			$fpekerjaanpemohon = $this->input->post('fpekerjaanpemohon');
			$falamatpemohon = $this->input->post('falamatpemohon');
			$fktppemohon = $this->input->post('fktppemohon');
			$ftujuanke = $this->input->post('ftujuanke');
			$fdesa = $this->input->post('fdesa');
			$fkecamatan = $this->input->post('fkecamatan');
			$fkabkota = $this->input->post('fkabkota');
			$fprovinsi = $this->input->post('fprovinsi');
			$ftglberangkat = $this->input->post('ftglberangkat');
			$falasanpergi = $this->input->post('falasanpergi');
			$fpengikut = $this->input->post('fpengikut');


			$where = array('id_surat' => $id_surat);
			// Proses Simpan Surat
			$fields_surat = array(
				'nik_pemohon' => $fktppemohon,
				'nama_pemohon' => $fnamapemohon
			);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses simpan Form Pengantar Akte
			$fields_p_akte = array(
				'pergi_gender' => $fgenderpemohon,
				'pergi_tempat_lahir' => $ftmptlahirpemohon,
				'pergi_tanggal_lahir' => $ftgllahirpemohon,
				'pergi_kwn' => $fkewarganegaraanpemohon,
				'pergi_agama' => $fagamapemohon,
				'pergi_status_kawin' => $fstatusnikahpemohon,
				'pergi_pekerjaan' => $fpekerjaanpemohon,
				'pergi_pendidikan' => $fpendidikanpemohon,
				'pergi_alamat_asal' => $falamatpemohon,
				'pergi_tujuan' => $ftujuanke,
				'pergi_tujuan_desa' => $fdesa,
				'pergi_tujuan_kecamatan' => $fkecamatan,
				'pergi_tujuan_kab' => $fkabkota,
				'pergi_tujuan_provinsi' => $fprovinsi,
				'pergi_tanggal_berangkat' => $ftglberangkat,
				'pergi_alasan' => $falasanpergi,
				'pergi_pengikut' => $fpengikut
			);
			$this->M_crud->updateData('tb_berpergian', $fields_p_akte, $where);

			// hapus dulu table pengikut
			$this->M_crud->deleteData('tb_pengikut', $where);

			$jumlah = count($_POST['nik_pengikut']);
			for ($i = 0; $i < $jumlah; $i++) {
				$nik_pengikut = $_POST['nik_pengikut'][$i];
				$nama_pengikut = $_POST['nama_pengikut'][$i];
				$gender_pengikut = $_POST['gender_pengikut'][$i];
				$umur_pengikut = $_POST['umur_pengikut'][$i];
				$status_nikah_pengikut = $_POST['status_nikah_pengikut'][$i];
				$pendidikan_pengikut = $_POST['pendidikan_pengikut'][$i];
				$keterangan_pengikut = $_POST['keterangan_pengikut'][$i];

				$fields3 = array(
					'id_surat' => $id_surat,
					'pengikut_nama' => $nama_pengikut,
					'pengikut_gender' => $gender_pengikut,
					'pengikut_umur' => $umur_pengikut,
					'pengikut_status_kawin' => $status_nikah_pengikut,
					'pengikut_pendidikan' => $pendidikan_pengikut,
					'pengikut_nik' => $nik_pengikut,
					'pengikut_keterangan' => $keterangan_pengikut
				);

				$this->M_crud->simpanData('tb_pengikut', $fields3);
			}

			redirect('layanan-SKberpergian-admin');
		}
	}


	public function lihatBerpergian($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10114;

		$data2['berpergian'] = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_berpergian ON tb_surat.id_surat = tb_berpergian.id_surat WHERE tb_surat.jenis_surat = 'berpergian' AND tb_surat.aktif = 1 AND tb_surat.id_surat = '$id'")->row_array();

		$data2['pengikut'] = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = '$id'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/berpergian/V_lihatBerpergian", $data2);
		$this->load->view("admin/V_footer");
	}


	public function doneBerpergian($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		$this->session->set_flashdata('success', 'Data berhasil diupdate');
		redirect('layanan-SKberpergian-admin');
	}

	public function hapusBerpergian($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-SKberpergian-admin');
	}

	public function printBepergian($id = null)
	{
		if (!isset($id)) redirect('beranda-admin');

		date_default_timezone_set('Asia/Jakarta');
		$query = $this->M_crud->getQuery("SELECT * FROM tb_surat join tb_berpergian ON tb_surat.id_surat = tb_berpergian.id_surat WHERE tb_surat.jenis_surat = 'berpergian' AND tb_surat.aktif = 1 AND tb_surat.id_pengajuan = '$id'")->row();

		$pengikut = $this->M_crud->getQuery("SELECT * FROM tb_pengikut WHERE tb_pengikut.id_surat = $query->id_surat")->result();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Bepergian S-01XXXXXX');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(207, 7, 'SURAT KETERANGAN BEPERGIAN', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 4, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(92, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '1. Nama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 5, $query->nama_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '2. Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		if ($query->pergi_gender == "L") {
			$pdf->Cell(85, 5, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 5, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '3. Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_tempat_lahir . ', ' . format_indo(date($query->pergi_tanggal_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 5, '4. Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_kwn, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '5. Agama', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '6. Status Perkawinan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_status_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '7. Pekerjaan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '8. Pendidikan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_pendidikan, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '9. Alamat Asal', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_alamat_asal, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '10. Nomor KTP', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '11. Tujuan Ke', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_tujuan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Desa', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_tujuan_desa, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kecamatan', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_tujuan_kecamatan, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_tujuan_kab, 0, 1);
		$pdf->SetX(35);
		$pdf->Cell(60, 5, '- Provinsi', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_tujuan_provinsi, 0, 1);

		$pdf->SetX(25);
		$pdf->Cell(70, 5, '12. Tanggal Berangkat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, format_indo(date($query->pergi_tanggal_berangkat)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '13. Alasan Bepergian', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_alasan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '14. Pengikut', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pergi_pengikut . ' Orang', 0, 1);
		$pdf->Cell(10, 5, '', 0, 1);

		if ($query->pergi_pengikut == 0) {
		} else {
			$pdf->SetX(5);
			$pdf->SetFont('Times', '', 10);
			$pdf->Cell(10, 6, 'NO', 1, 0, 'C');
			$pdf->Cell(55, 6, 'NAMA', 1, 0, 'C');
			$pdf->Cell(12, 6, 'JKEL', 1, 0, 'C');
			$pdf->Cell(20, 6, 'UMUR', 1, 0, 'C');
			$pdf->Cell(25, 6, 'STATUS', 1, 0, 'C');
			$pdf->Cell(15, 6, 'PENDI', 1, 0, 'C');
			$pdf->Cell(50, 6, 'NO. KTP', 1, 0, 'C');
			$pdf->Cell(20, 6, 'KET', 1, 1, 'C');

			$no = 1;
			foreach ($pengikut as $val) :
				$pdf->SetX(5);
				$pdf->SetFont('Times', '', 9);
				$pdf->Cell(10, 6, $no++, 1, 0, 'C');
				$pdf->Cell(55, 6, $val->pengikut_nama, 1, 0, 'C');
				$pdf->Cell(12, 6, $val->pengikut_gender, 1, 0, 'C');
				$pdf->Cell(20, 6, $val->pengikut_umur . ' TAHUN', 1, 0, 'C');
				$pdf->Cell(25, 6, $val->pengikut_status_kawin, 1, 0, 'C');
				$pdf->Cell(15, 6, $val->pengikut_pendidikan, 1, 0, 'C');
				$pdf->Cell(50, 6, $val->pengikut_nik, 1, 0, 'C');
				$pdf->Cell(20, 6, $val->pengikut_keterangan, 1, 1, 'C');
			endforeach;
			$pdf->Cell(10, 5, '', 0, 1);
		}

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(193, 7, 'Demikian surat keterangan bepergian ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'Mengetahui :', 0, 0, 'C');
		$pdf->Cell(18, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, 'Camat Semboro', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(96, 5, 'Kepala Desa Sidomulyo', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(78, 5, 'MUH. ARIS DERMAWAN, S,Sos', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, 'WASISO, S.IP', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(78, 5, '19640102 199305 1 001', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(95, 5, '', 0, 1, 'C');

		$pdf->Output();
	}
}
