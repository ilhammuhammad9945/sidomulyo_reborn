<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_kesehatan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function rules_1()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function index()
	{
		$data1['title'] = "Data Pengurus Poskesdes";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 95;
		$data1['submenu2'] = 0;
		$data2["kesehatan"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Poskesdes'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kesehatan/V_kesehatan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_kesehatan()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 95;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Poskesdes'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'Poskesdes';
			$foto = $this->_uploadImageKes($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('kesehatan-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kesehatan/V_tambahkes", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit_kesehatan($id = null)
	{
		if (!isset($id)) redirect('kesehatan-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 95;
		$data1['submenu2'] = 0;
		$data["kes"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Poskesdes'")->result();
		if (!$data["kes"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageKes($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('kesehatan-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/kesehatan/V_editkes", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus_kesehatan($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageKes($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('kesehatan-admin');
		}
	}

	private function _uploadImageKes($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/kesehatan/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _deleteImageKes($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/kesehatan/$filename.*"));
		}
	}
}
