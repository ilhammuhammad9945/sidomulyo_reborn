<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_master extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function rules_1()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_2()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function index()
	{
		$data1['title'] = "Data Jenis Konten";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 41;
		$data1['submenu2'] = 0;
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_jenis_konten")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_jeniskonten", $data2);
		$this->load->view("admin/V_footer");
	}

	public function jabatan()
	{
		$data1['title'] = "Data Jabatan";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 42;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_jabatan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function organisasi()
	{
		$data1['title'] = "Data Organisasi";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 43;
		$data1['submenu2'] = 0;
		$data2["organisasi"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_organisasi", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_jenis()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 41;
		$data1['submenu2'] = 0;
		if ($validation->run()) {
			$nama = $this->input->post('fnama');
			$datanya = array(
				'nama_jenis' => $nama
			);
			$this->M_crud->simpanData('tb_jenis_konten', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('jenis-konten-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_tambahjenis");
		$this->load->view("admin/V_footer");
	}

	public function tambah_jabatan()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 42;
		$data1['submenu2'] = 0;
		if ($validation->run()) {
			$nama = $this->input->post('fnama');
			$keterangan = $this->input->post('fket');
			$datanya = array(
				'nama_jbt' => $nama,
				'keterangan' => $keterangan
			);
			$this->M_crud->simpanData('tb_jabatan', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('jabatan-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_tambahjabatan");
		$this->load->view("admin/V_footer");
	}

	public function tambah_organisasi()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 43;
		$data1['submenu2'] = 0;
		if ($validation->run()) {
			$nama = $this->input->post('fnama');
			$keterangan = $this->input->post('fket');
			$lain = $this->input->post('flain');
			$datanya = array(
				'nama_org' => $nama,
				'keterangan_org' => $keterangan,
				'lain_lain' => $lain
			);
			$this->M_crud->simpanData('tb_organisasi', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('organisasi-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_tambahorganisasi");
		$this->load->view("admin/V_footer");
	}

	public function edit_jenis($id = null)
	{
		if (!isset($id)) redirect('jenis-konten-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 41;
		$data1['submenu2'] = 0;

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');

			$datanya = array(
				'nama_jenis' => $nama
			);

			$where = array(
				'id_jenis' => $id
			);
			$this->M_crud->updateData('tb_jenis_konten', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('jenis-konten-admin');
		}
		$data["jenis"] = $this->M_crud->getQuery("SELECT * FROM tb_jenis_konten WHERE id_jenis='" . $id . "'")->row();
		if (!$data["jenis"]) show_404();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_editjenis", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_jabatan($id = null)
	{
		if (!isset($id)) redirect('jabatan-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 42;
		$data1['submenu2'] = 0;
		$data["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE id_jbt='" . $id . "'")->row();
		if (!$data["jabatan"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$keterangan = $this->input->post('fket');
			$datanya = array(
				'nama_jbt' => $nama,
				'keterangan' => $keterangan
			);

			$where = array(
				'id_jbt' => $id
			);
			$this->M_crud->updateData('tb_jabatan', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('jabatan-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_editjabatan", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_organisasi($id = null)
	{
		if (!isset($id)) redirect('organisasi-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 43;
		$data1['submenu2'] = 0;
		$data["organisasi"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE id_org='" . $id . "'")->row();
		if (!$data["organisasi"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$keterangan = $this->input->post('fket');
			$lain = $this->input->post('flain');
			$datanya = array(
				'nama_org' => $nama,
				'keterangan_org' => $keterangan,
				'lain_lain' => $lain
			);

			$where = array(
				'id_org' => $id
			);
			$this->M_crud->updateData('tb_organisasi', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('organisasi-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/master/V_editorganisasi", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus_jenis($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_jenis' => $id
		);
		$datanya = $this->M_crud->deleteData('tb_jenis_konten', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('jenis-konten-admin');
		}
	}

	public function hapus_jabatan($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_jbt' => $id
		);
		$datanya = $this->M_crud->deleteData('tb_jabatan', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('jabatan-admin');
		}
	}

	public function hapus_organisasi($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_org' => $id
		);
		$datanya = $this->M_crud->deleteData('tb_organisasi', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('organisasi-admin');
		}
	}

	public function setting_surat()
	{
		$data1['title'] = "Setting Surat";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 44;
		$data1['submenu2'] = 0;

		$data2["setting_surat"] = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/settingsurat/V_setting-surat", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_setting_surat()
	{
		$data1['title'] = "Tambah Setting Surat";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 44;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/settingsurat/V_add-setting-surat");
		$this->load->view("admin/V_footer");
	}

	public function save_setting_surat()
	{

		$jenis = $this->input->post('jenis');
		$keterangan = $this->input->post('keterangan');
		$lainlain = $this->input->post('lainlain');

		//simpan data
		$fields = array(
			'jenis' => $jenis,
			'keterangan' => $keterangan,
			'lain_lain' => $lainlain
		);

		$this->M_crud->simpanData('tb_setting_surat', $fields);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil menyimpan data';

		redirect('setting-surat-admin');
	}

	public function edit_setting_surat($idb)
	{
		$data1['title'] = "Edit Setting Surat";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 44;
		$data1['submenu2'] = 0;

		$data2["setting_surat"] = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE id_setting = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/settingsurat/V_edit-setting-surat", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesedit_setting_surat()
	{
		$id_setting = $this->input->post('id_setting');
		$jenis = $this->input->post('jenis');
		$keterangan = $this->input->post('keterangan');
		$lainlain = $this->input->post('lainlain');

		$where = array(
			'id_setting' => $id_setting
		);

		//update data
		$fields = array(
			'jenis' => $jenis,
			'keterangan' => $keterangan,
			'lain_lain' => $lainlain
		);

		$this->M_crud->updateData('tb_setting_surat', $fields, $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('setting-surat-admin');
	}

	public function hapus_setting_surat($id)
	{
		$where = array('id_setting' => $id);
		$this->M_crud->deleteData("tb_setting_surat", $where);
		redirect(site_url('setting-surat-admin'));
	}

	public function user()
	{
		$data1['title'] = "User";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 45;
		$data1['submenu2'] = 0;

		$data2["user"] = $this->M_crud->getQuery("SELECT * FROM tb_user")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/user/V_user", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_user()
	{
		$data1['title'] = "Tambah User";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 45;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/user/V_add-user");
		$this->load->view("admin/V_footer");
	}

	public function save_user()
	{

		$nama_user = $this->input->post('nama_user');
		$username_user = $this->input->post('username_user');
		$password_user = $this->input->post('password_user');
		$posisi_user = $this->input->post('posisi_user');
		$now = date('Y-m-d H:i:s');

		$cek_username = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE username_user = '$username_user'")->num_rows();
		if ($cek_username == 0) { // jika username unik

			//simpan data
			$fields = array(
				'nama_user' => $nama_user,
				'username_user' => $username_user,
				'password_user' => md5($password_user),
				'level_user' => $posisi_user,
				'date_created' => $now
			);

			$this->M_crud->simpanData('tb_user', $fields);

			$isi_pesan = "Berhasil mengubah profil";
			$type = "success";
			$judul = "Berhasil";

		} else {  // jika ada kesamaan

			$isi_pesan = "Username sudah digunakan";
			$type = "danger";
			$judul = "Gagal";
		}


		// if($benar == 1){
		// 	//simpan data
		// 	$fields = array(
		// 		'nama_user' => $nama_user,
		// 		'username_user' => $username_user,
		// 		'password_user' => $password_user,
		// 		'level_user' => $posisi_user,
		// 		'date_created' => $now
		// 	);
		//
		// 	$this->M_crud->simpanData('tb_user', $fields);
		// }

		$_SESSION['type'] = $type;
		$_SESSION['judul'] = $judul;
		$_SESSION['isi'] = $isi_pesan;


		redirect('user-admin');
	}

	public function edit_user($idb)
	{
		$data1['title'] = "Edit User";
		$data1['menu_aktif'] = 4;
		$data1['submenu'] = 45;
		$data1['submenu2'] = 0;

		$data2["user"] = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE id_user = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/user/V_edit-user", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesedit_user()
	{
		$id_user = $this->input->post('id_user');
		$nama_user = $this->input->post('nama_user');
		$username_user = $this->input->post('username_user');
		$password_user = $this->input->post('password_user');
		$posisi_user = $this->input->post('posisi_user');
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		$benar = 1;

		$old_username = $this->M_crud->getQuery("SELECT username_user FROM tb_user WHERE id_user = '$id_user'")->row_array();
		if ($old_username['username_user'] != $username_user) { // jika ada perubahan di username

			$cek_username = $this->M_crud->getQuery("SELECT * FROM tb_user WHERE username_user = '$username_user'")->num_rows();
			if ($cek_username == 0) { // jika username unik

				//update data
				$fields_uname = array(
					"username_user" => $username_user
				);
				$where_uname = array("id_user" => $id_user);
				$this->M_crud->updateData("tb_user", $fields_uname, $where_uname);
			} else {  // jika ada kesamaan

				$_SESSION['type'] = "Username sudah digunakan";
				$_SESSION['judul'] = "danger";
				$_SESSION['isi'] = "Gagal";
				$benar = 0;
			}
		}

		if ($benar = 1) {
			if($password_user != ''){
				//simpan data
				$fields = array(
					'nama_user' => $nama_user,
					'password_user' => md5($password_user),
					'level_user' => $posisi_user,
					'date_updated' => $now
				);
			} else {
				$fields = array(
					'nama_user' => $nama_user,
					'level_user' => $posisi_user,
					'date_updated' => $now
				);
			}

			$where = array('id_user' => $id_user);

			$this->M_crud->updateData('tb_user', $fields, $where);

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil mengubah data';
		} else {
			$_SESSION['type'] = 'danger';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Gagal ubah data';
		}

		redirect('user-admin');
	}

	public function hapus_user($id)
	{
		$where = array('id_user' => $id);
		$this->M_crud->deleteData("tb_user", $where);
		redirect(site_url('user-admin'));
	}

	public function statistik()
	{
		$data1['title'] = "Statistik";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 96;
		$data1['submenu2'] = 0;

		$data2["statistik"] = $this->M_crud->getQuery("SELECT * FROM tb_statistik ORDER BY id_statistik DESC")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/statistik/V_statistik", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_statistik()
	{
		$data1['title'] = "Tambah Statistik";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 96;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/statistik/V_add-statistik");
		$this->load->view("admin/V_footer");
	}

	public function save_statistik()
	{

		$kategori_statistik = $this->input->post('kategori_statistik');
		$jumlah = count($_POST['parameter_statistik']);

		for ($i = 0; $i < $jumlah; $i++) {

			$parameter = $_POST['parameter_statistik'][$i];
			$nilai = $_POST['nilai_statistik'][$i];

			//simpan data
			$fields = array(
				'judul_statistik' => $kategori_statistik,
				'parameter_statistik' => $parameter,
				'nilai_statistik' => $nilai
			);

			$this->M_crud->simpanData('tb_statistik', $fields);
		}

		$_SESSION['type'] = "success";
		$_SESSION['judul'] = "Berhasil";
		$_SESSION['isi'] = "Berhasil mengubah data";

		redirect('statistik-admin');
	}

	public function edit_statistik($idb)
	{
		$data1['title'] = "Edit Statistik";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 96;
		$data1['submenu2'] = 0;

		// $cekID = $this->M_crud->getQuery("SELECT judul_statistik FROM tb_statistik WHERE id_statistik = '$idb'")->row_array();

		$data2["statistik"] = $this->M_crud->getQuery("SELECT * FROM tb_statistik WHERE id_statistik = '$idb'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/statistik/V_edit-statistik", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesedit_statistik()
	{
		$id_statistik = $this->input->post('id_statistik');
		$kategori_statistik = $this->input->post('kategori_statistik');
		$parameter_statistik = $this->input->post('parameter_statistik');
		$nilai_statistik = $this->input->post('nilai_statistik');

		//update data
		$fields = array(
			'judul_statistik' => $kategori_statistik,
			'parameter_statistik' => $parameter_statistik,
			'nilai_statistik' => $nilai_statistik
		);

		$where = array('id_statistik' => $id_statistik);

		$this->M_crud->updateData('tb_statistik', $fields, $where);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('statistik-admin');
	}

	public function hapus_statistik($id)
	{
		$where = array('id_statistik' => $id);
		$this->M_crud->deleteData("tb_statistik", $where);
		redirect(site_url('statistik-admin'));
	}
}
