<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_umkm extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function rules_1()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftelp',
				'label' => 'No. Telp',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],


			[
				'field' => 'fpemilik',
				'label' => 'Pemilik',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]
		];
	}

	public function rules_2()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama Produk',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fumkm',
				'label' => 'UMKM',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]
		];
	}


	public function index()
	{

		$data1['title'] = "Data UMKM";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 92;
		$data1['submenu2'] = 0;
		$data2["umkm"] = $this->M_crud->getQuery("SELECT * FROM tb_umkm ORDER BY id_umkm DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/umkm/V_umkm", $data2);
		$this->load->view("admin/V_footer");
	}


	public function produk_unggulan()
	{

		$data1['title'] = "Data Produk Unggulan";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 93;
		$data1['submenu2'] = 0;
		$data2["produk"] = $this->M_crud->getQuery("SELECT pr.*,um.* FROM tb_produk_unggulan AS pr JOIN tb_umkm AS um ON pr.umkm_pr=um.id_umkm ORDER BY pr.id_pr DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/umkm/V_produk", $data2);
		$this->load->view("admin/V_footer");
	}


	public function tambah_umkm()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 92;
		$data1['submenu2'] = 0;

		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$alamat = $this->input->post('falamat');
			$telp = $this->input->post('ftelp');
			$pemilik = $this->input->post('fpemilik');
			$foto = $this->_uploadImageUmkm($id);
			$datanya = array(
				'id_umkm' => $id,
				'nama_umkm' => $nama,
				'alamat_umkm' => $alamat,
				'telp_umkm' => $telp,
				'pemilik_umkm' => $pemilik,
				'foto_umkm' => $foto
			);
			$this->M_crud->simpanData('tb_umkm', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('umkm-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/umkm/V_tambahumkm");
		$this->load->view("admin/V_footer");
	}

	public function tambah_produkunggulan()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 93;
		$data1['submenu2'] = 0;
		$data2["umkm"] = $this->M_crud->getQuery("SELECT * FROM tb_umkm")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$umkm = $this->input->post('fumkm');
			$foto = $this->_uploadImageProduk($id);
			$datanya = array(
				'id_pr' => $id,
				'nama_pr' => $nama,
				'umkm_pr' => $umkm,
				'foto_produk' => $foto
			);
			$this->M_crud->simpanData('tb_produk_unggulan', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('produkunggulan-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/umkm/V_tambahproduk", $data2);
		$this->load->view("admin/V_footer");
	}



	public function edit_umkm($id = null)
	{
		if (!isset($id)) redirect('umkm-admin');
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 92;
		$data1['submenu2'] = 0;
		$data["umkm"] = $this->M_crud->getQuery("SELECT * FROM tb_umkm WHERE id_umkm='" . $id . "'")->row();
		if (!$data["umkm"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$alamat = $this->input->post('falamat');
			$telp = $this->input->post('ftelp');
			$pemilik = $this->input->post('fpemilik');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageUmkm($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_umkm' => $nama,
				'alamat_umkm' => $alamat,
				'telp_umkm' => $telp,
				'pemilik_umkm' => $pemilik,
				'foto_umkm' => $foto
			);

			$where = array(
				'id_umkm' => $id
			);
			$this->M_crud->updateData('tb_umkm', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('umkm-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/umkm/V_editumkm", $data);
		$this->load->view("admin/V_footer");
	}


	public function edit_produkunggulan($id = null)
	{
		if (!isset($id)) redirect('produkunggulan-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 93;
		$data1['submenu2'] = 0;
		$data["produk"] = $this->M_crud->getQuery("SELECT pr.*,um.* FROM tb_produk_unggulan AS pr JOIN tb_umkm AS um ON pr.umkm_pr=um.id_umkm WHERE pr.id_pr='" . $id . "'")->row();
		$data["umkm"] = $this->M_crud->getQuery("SELECT * FROM tb_umkm")->result();
		if (!$data["produk"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$umkm = $this->input->post('fumkm');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageProduk($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_pr' => $nama,
				'umkm_pr' => $umkm,
				'foto_produk' => $foto
			);

			$where = array(
				'id_pr' => $id
			);
			$this->M_crud->updateData('tb_produk_unggulan', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('produkunggulan-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/umkm/V_editproduk", $data);
		$this->load->view("admin/V_footer");
	}


	public function hapus_umkm($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_umkm' => $id
		);
		$this->_deleteImageUmkm($id);
		$datanya = $this->M_crud->deleteData('tb_umkm', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('umkm-admin');
		}
	}

	public function hapus_produkunggulan($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_pr' => $id
		);
		$this->_deleteImageProduk($id);
		$datanya = $this->M_crud->deleteData('tb_produk_unggulan', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('produkunggulan-admin');
		}
	}

	private function _uploadImageUmkm($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/umkm/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageProduk($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/produkunggulan/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}



	private function _deleteImageUmkm($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_umkm WHERE id_umkm='" . $id . "'")->row();
		if ($foto->foto_umkm != "default.jpg") {
			$filename = explode(".", $foto->foto_umkm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/umkm/$filename.*"));
		}
	}

	private function _deleteImageProduk($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_produk_unggulan WHERE id_pr='" . $id . "'")->row();
		if ($foto->foto_produk != "default.jpg") {
			$filename = explode(".", $foto->foto_produk)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/produkunggulan/$filename.*"));
		}
	}
}
