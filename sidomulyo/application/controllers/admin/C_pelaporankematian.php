<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_pelaporankematian extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["form_validation", "pdf"]);
	}

	public function rules()
	{
		return [
			[
				'field' => 'fnik1',
				'label' => 'NIK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamapl',
				'label' => 'Nama Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fumurpl',
				'label' => 'Umur Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaanpl',
				'label' => 'Pekerjaan Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamatpl',
				'label' => 'Alamat Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fhubunganpl',
				'label' => 'Hubungan Pelapor',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamamng',
				'label' => 'Nama yang Meninggal',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnik2',
				'label' => 'NIK yang Meninggal',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkelmng',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahirmng',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fumurmng',
				'label' => 'Umur yang Meninggal',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagamamng',
				'label' => 'Agama yang Meninggal',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamatmng',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftglmng',
				'label' => 'Tanggal Kematian',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpukulmng',
				'label' => 'Pukul',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatmng',
				'label' => 'Tempat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpenyebab',
				'label' => 'Penyebab',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fketerangan',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]
		];
	}

	public function index()
	{

		$data1['title'] = "Data Pelaporan Kematian";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1018;
		$data2["plkm"] = $this->M_crud->getQuery("SELECT sr.*, pm.* FROM tb_surat AS sr JOIN tb_pelaporan_kmt AS pm ON sr.id_surat=pm.id_surat WHERE sr.jenis_surat ='pelaporan kematian' AND sr.aktif=1 ORDER BY sr.id_surat DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pelaporankematian/V_indexPm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahPelaporanKematian()
	{

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		date_default_timezone_set('Asia/Jakarta');

		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1018;
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		if ($validation->run()) {
			$idpengajuan = $this->input->post('fidpengajuanpm');
			// $nosurat = $this->input->post('fnosurat');
			$nik1 = $this->input->post('fnik1');
			$namapl = $this->input->post('fnamapl');
			$umurpl = $this->input->post('fumurpl');
			$pekerjaanpl = $this->input->post('fpekerjaanpl');
			$alamatpl = $this->input->post('falamatpl');
			$hubunganpl = $this->input->post('fhubunganpl');
			$namamng = $this->input->post('fnamamng');
			$nik2 = $this->input->post('fnik2');
			$jkelmng = $this->input->post('fjkelmng');
			$tgllahirmng = $this->input->post('ftgllahirmng');
			$umurmng = $this->input->post('fumurmng');
			$agamamng = $this->input->post('fagamamng');
			$alamatmng = $this->input->post('falamatmng');
			$tglmng = $this->input->post('ftglmng');
			$pukulmng = $this->input->post('fpukulmng');
			$tempatmng = $this->input->post('ftempatmng');
			$penyebab = $this->input->post('fpenyebab');
			$keterangan = $this->input->post('fketerangan');

			$config['upload_path']          = './assets/upload/layanan/pelaporankematian';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 1024 * 5;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('fbuktimng')) {
				$fileData =  $this->upload->data("file_name");
			} else {
				echo "gagal upload berkas";
			}

			$dataMaster = array(
				'jenis_surat' => 'pelaporan kematian',
				// 'tgl_surat' => $tglskrg,
				// 'no_surat' => $nosurat,
				'id_pengajuan' => $idpengajuan,
				'nik_pemohon' => $nik1,
				'nama_pemohon' => $namapl,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $dataMaster);

			$dataTurunan = array(
				'id_surat' => $id_surat,
				'kmt_umur' => $umurpl,
				'kmt_pekerjaan' => $pekerjaanpl,
				'kmt_alamat' => $alamatpl,
				'kmt_hubungan' => $hubunganpl,
				'kmt_nama_mati' => $namamng,
				'kmt_nik_mati' => $nik2,
				'kmt_jkel_mati' => $jkelmng,
				'kmt_tgl_lahir_mati' => $tgllahirmng,
				'kmt_umur_mati' => $umurmng,
				'kmt_agama_mati' => $agamamng,
				'kmt_alamat_mati' => $alamatmng,
				'kmt_tgl_mati' => $tglmng,
				'kmt_pukul_mati' => $pukulmng,
				'kmt_tempat_mati' => $tempatmng,
				'kmt_penyebab' => $penyebab,
				'kmt_bukti_mati' => $fileData,
				'kmt_keterangan' => $keterangan
			);
			$this->M_crud->simpanData('tb_pelaporan_kmt', $dataTurunan);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('layanan-pelaporan-kematian-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pelaporankematian/V_tambahPm", $x);
		$this->load->view("admin/V_footer");
	}

	public function editPelaporanKematian($id = null)
	{
		if (!isset($id)) redirect('layanan-pelaporan-kematian-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());

		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1018;

		$data2["plkm"] = $this->M_crud->getQuery("SELECT sr.*, pm.* FROM tb_surat AS sr JOIN tb_pelaporan_kmt AS pm ON sr.id_surat=pm.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["plkm"]) show_404();

		if ($validation->run()) {
			$idsurat = $this->input->post('fidsurat');
			// $nosurat = $this->input->post('fnosurat');
			$nik1 = $this->input->post('fnik1');
			$namapl = $this->input->post('fnamapl');
			$umurpl = $this->input->post('fumurpl');
			$pekerjaanpl = $this->input->post('fpekerjaanpl');
			$alamatpl = $this->input->post('falamatpl');
			$hubunganpl = $this->input->post('fhubunganpl');
			$namamng = $this->input->post('fnamamng');
			$nik2 = $this->input->post('fnik2');
			$jkelmng = $this->input->post('fjkelmng');
			$tgllahirmng = $this->input->post('ftgllahirmng');
			$umurmng = $this->input->post('fumurmng');
			$agamamng = $this->input->post('fagamamng');
			$alamatmng = $this->input->post('falamatmng');
			$tglmng = $this->input->post('ftglmng');
			$pukulmng = $this->input->post('fpukulmng');
			$tempatmng = $this->input->post('ftempatmng');
			$penyebab = $this->input->post('fpenyebab');
			$fotolama = $this->input->post('ffotolama');
			$keterangan = $this->input->post('fketerangan');

			$config['upload_path']          = './assets/upload/layanan/pelaporankematian';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 1024 * 5;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('fbuktimng')) {
				$fileData =  $this->upload->data("file_name");
				unlink('./assets/upload/layanan/pelaporankematian/' . $fotolama);
			} else {
				$fileData = $fotolama;
			}


			$dataMaster = array(
				// 'no_surat' => $nosurat,
				'nik_pemohon' => $nik1,
				'nama_pemohon' => $namapl
			);

			$where = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_surat', $dataMaster, $where);

			$dataTurunan = array(
				'kmt_umur' => $umurpl,
				'kmt_pekerjaan' => $pekerjaanpl,
				'kmt_alamat' => $alamatpl,
				'kmt_hubungan' => $hubunganpl,
				'kmt_nama_mati' => $namamng,
				'kmt_nik_mati' => $nik2,
				'kmt_jkel_mati' => $jkelmng,
				'kmt_tgl_lahir_mati' => $tgllahirmng,
				'kmt_umur_mati' => $umurmng,
				'kmt_agama_mati' => $agamamng,
				'kmt_alamat_mati' => $alamatmng,
				'kmt_tgl_mati' => $tglmng,
				'kmt_pukul_mati' => $pukulmng,
				'kmt_tempat_mati' => $tempatmng,
				'kmt_penyebab' => $penyebab,
				'kmt_bukti_mati' => $fileData,
				'kmt_keterangan' => $keterangan
			);

			$where2 = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_pelaporan_kmt', $dataTurunan, $where2);

			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('layanan-pelaporan-kematian-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pelaporankematian/V_editPm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ubahPelaporanKematian()
	{

		$idsurat = $this->input->post('fid');
		$ket = $this->input->post('fstatus_pembuatan');

		$data = array(
			'tgl_surat' => date('Y-m-d'),
			'status_pembuatan' => $ket

		);

		$where = array(
			'id_surat' => $idsurat
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Berhasil diupdate');
		redirect('layanan-pelaporan-kematian-admin');
	}

	public function lihatPelaporanKematian($id = null)
	{
		if (!isset($id)) redirect('layanan-pelaporan-kematian-admin');

		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1018;
		$data2["plkm"] = $this->M_crud->getQuery("SELECT sr.*, pm.* FROM tb_surat AS sr JOIN tb_pelaporan_kmt AS pm ON sr.id_surat=pm.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["plkm"]) show_404();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pelaporankematian/V_lihatPm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusPelaporanKematian($id = null)
	{
		if (!isset($id)) show_404();

		// ini kodingan hapus soft
		$data = array(
			'aktif' => 0
		);

		$where = array(
			'id_surat' => $id
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		redirect('layanan-pelaporan-kematian-admin');
	}

	public function printPelaporanKematian($id = null)
	{
		if (!isset($id)) redirect('layanan-pelaporan-kematian-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_pelaporan_kmt', 'tb_surat.id_surat = tb_pelaporan_kmt.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Pelaporan Kematian [ ' . $id . ' ]');

		$pdf->SetFont('Times', 'B', 13);
		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Pemerintah Desa/Kelurahan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(95, 7, 'Sidomulyo', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(60, 7, 'Kecamatan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(95, 7, 'Semboro', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(60, 7, 'Kabupaten/Kota', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(95, 7, 'Jember', 0, 1);

		// ngatur judul surat
		$pdf->Cell(10, 8, '', 0, 1);
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(200, 7, 'FORMULIR PELAPORAN KEMATIAN', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(100, 5, 'Yang bertanda tangan dibawah ini :', 0, 1, 'C');
		$pdf->Cell(10, 2, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 13);
		// $query = $this->db->get_where('tb_sk_usaha', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama Lengkap', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'NIK', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->nik_pemohon, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_umur . ' Tahun', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_pekerjaan, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_alamat, 0, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Hubungan dengan yang mati', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, $query->kmt_hubungan, 0, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(73, 5, 'Melaporkan bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 1, '', 0, 1);

		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama Lengkap', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$pdf->Cell(105, 7, strtoupper($query->kmt_nama_mati), 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'NIK', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_nik_mati, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		if ($query->kmt_jkel_mati == "L") {
			$pdf->Cell(105, 7, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(105, 7, 'Perempuan', 0, 1);
		}
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Tanggal Lahir/Umur', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, format_indo(date($query->kmt_tgl_lahir_mati)) . '/ ' . $query->kmt_umur_mati . ' Tahun', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Agama', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_agama_mati, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_alamat_mati, 0, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(90, 5, 'Telah meninggal dunia pada :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetX(20);
		$pdf->Cell(60, 7, 'Nama Lengkap', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 13);
		$day = date('D', strtotime($query->kmt_tgl_mati));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		$pdf->Cell(105, 7, $dayList[$day], 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Tanggal Kematian', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, format_indo(date($query->kmt_tgl_mati)), 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Pukul', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_pukul_mati . ' WIB', 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Bertempat di', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_tempat_mati, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Penyebab', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_penyebab, 0, 1);
		$pdf->SetX(20);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(60, 7, 'Bukti Kematian', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(105, 7, $query->kmt_keterangan, 0, 1);
		$pdf->Cell(10, 15, '', 0, 1);




		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(175, 7, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->Cell(10, 0, '', 0, 1);
		$pdf->SetFont('Times', '', 13);
		$pdf->Cell(155, 7, 'Pelapor', 0, 1, 'R');
		$pdf->Cell(10, 25, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 13);
		$pdf->Cell(175, 7, 'BAGUS ANANDA PUTRA', 0, 1, 'R');

		$pdf->Output();
	}
}
