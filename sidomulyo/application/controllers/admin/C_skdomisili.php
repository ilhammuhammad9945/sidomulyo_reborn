<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_skdomisili extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["form_validation", "pdf"]);
	}

	public function rules()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],
			[
				'field' => 'fnik',
				'label' => 'NIK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fwni',
				'label' => 'Kewarganegaraan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpendidikan',
				'label' => 'Pendidikan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_2()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],

			[
				'field' => 'fnik',
				'label' => 'NIK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamanegara',
				'label' => 'Kewarganegaraan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpendidikan',
				'label' => 'Pendidikan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fket',
				'label' => 'Keterangan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function index()
	{

		$data1['title'] = "Data Surat Keterangan Domisili";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1016;
		$data2["skdomisili"] = $this->M_crud->getQuery("SELECT sr.*, sk.* FROM tb_surat AS sr JOIN tb_sk_domisili AS sk ON sr.id_surat=sk.id_surat WHERE sr.jenis_surat ='domisili' AND sr.aktif=1 ORDER BY sr.id_surat DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/skdomisili/V_indexDomisili", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ketDomisili()
	{

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		date_default_timezone_set('Asia/Jakarta');
		// $tglskrg = date('Y-m-d');

		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1016;
		// $x['invoice'] = $this->M_layanan_surat->get_id_domisili();
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$namakades = $this->M_crud->getQuery("SELECT nama_apt FROM tb_aparatur WHERE jabatan_apt=1")->row();
		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();

		if ($validation->run()) {
			$idpengajuan = $this->input->post('fidpengajuanskdomisili');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$jkel = $this->input->post('fjkel');
			$tempatlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$namanegara = $this->input->post('fnamanegara');
			$agama = $this->input->post('fagama');
			$statuskawin = $this->input->post('fstatuskawin');
			$pendidikan = $this->input->post('fpendidikan');
			$pekerjaan = $this->input->post('fpekerjaan');
			$alamat = $this->input->post('falamat');
			$ket = $this->input->post('fket');

			$dataMaster = array(
				'jenis_surat' => 'domisili',
				// 'tgl_surat' => $tglskrg,
				// 'no_surat' => $nosurat,
				'id_pengajuan' => $idpengajuan,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $dataMaster);

			$dataTurunan = array(
				'id_surat' => $id_surat,
				'jkel_pemohon' => $jkel,
				'kewarganegaraan' => $namanegara,
				'agama' => $agama,
				'status_perkawinan' => $statuskawin,
				'tempat_lahir' => $tempatlahir,
				'tgl_lahir' => $tgllahir,
				'pendidikan' => $pendidikan,
				'pekerjaan' => $pekerjaan,
				'alamat' => $alamat,
				'keterangan' => $ket,
				'kepala_desa' => $namakades->nama_apt,
				'nama_camat' => $namacamat->keterangan,
				'nip_camat' => $namacamat->lain_lain
			);
			$this->M_crud->simpanData('tb_sk_domisili', $dataTurunan);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('layanan-domisili-admin');

			// $cek_nosurat = $this->M_crud->getQuery("SELECT no_surat FROM tb_sk_domisili WHERE no_surat='" . $nosurat . "'")->row();
			// Kalau no surat sudah ada
			// if ($cek_nosurat > 0) 
			// 	$this->session->set_flashdata('error', 'Nomor surat sudah ada, gunakan nomor lain');
			// 	redirect('tambah-domisili');
			// 
			// Kalau no surat tidak ada, inputkan data ke tabel
			// else 


			// 
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/skdomisili/V_ketDomisili", $x);
		$this->load->view("admin/V_footer");
	}

	public function editKetDomisili($id = null)
	{
		if (!isset($id)) redirect('layanan-domisili-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());

		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1016;
		$data2["skdomisili"] = $this->M_crud->getQuery("SELECT sr.*, sk.* FROM tb_surat AS sr JOIN tb_sk_domisili AS sk ON sr.id_surat=sk.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["skdomisili"]) show_404();

		if ($validation->run()) {
			$idsurat = $this->input->post('fidsurat');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$jkel = $this->input->post('fjkel');
			$tempatlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$namanegara = $this->input->post('fnamanegara');
			$agama = $this->input->post('fagama');
			$statuskawin = $this->input->post('fstatuskawin');
			$pendidikan = $this->input->post('fpendidikan');
			$pekerjaan = $this->input->post('fpekerjaan');
			$alamat = $this->input->post('falamat');
			$ket = $this->input->post('fket');

			$dataMaster = array(
				// 'no_surat' => $nosurat,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap
			);

			$where = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_surat', $dataMaster, $where);

			$dataTurunan = array(
				'jkel_pemohon' => $jkel,
				'kewarganegaraan' => $namanegara,
				'agama' => $agama,
				'status_perkawinan' => $statuskawin,
				'tempat_lahir' => $tempatlahir,
				'tgl_lahir' => $tgllahir,
				'pendidikan' => $pendidikan,
				'pekerjaan' => $pekerjaan,
				'alamat' => $alamat,
				'keterangan' => $ket
			);

			$where2 = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_sk_domisili', $dataTurunan, $where2);

			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('layanan-domisili-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/skdomisili/V_editKetDomisili", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ubahKetDomisili()
	{

		$idsurat = $this->input->post('fid');
		$ket = $this->input->post('fstatus_pembuatan');

		$data = array(
			'tgl_surat' => date('Y-m-d'),
			'status_pembuatan' => $ket

		);

		$where = array(
			'id_surat' => $idsurat
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Berhasil diupdate');
		redirect('layanan-domisili-admin');
	}

	public function lihatKetDomisili($id = null)
	{
		if (!isset($id)) redirect('layanan-domisili-admin');

		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1016;
		$data2["skdomisili"] = $this->M_crud->getQuery("SELECT sr.*, sk.* FROM tb_surat AS sr JOIN tb_sk_domisili AS sk ON sr.id_surat=sk.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["skdomisili"]) show_404();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/skdomisili/V_lihatKetDomisili", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusKetDomisili($id = null)
	{
		if (!isset($id)) show_404();

		// ini kodingan hapus soft
		$data = array(
			'aktif' => 0

		);

		$where = array(
			'id_surat' => $id
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		// ini kodingan hapus hard
		// $where1 = array(
		// 	'id_surat' => $id
		// );
		// $where2 = array(
		// 	'id_surat' => $id
		// );
		// $data1 = $this->M_crud->deleteData('tb_surat', $where1);
		// $data2 = $this->M_crud->deleteData('tb_sk_domisili', $where2);

		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		redirect('layanan-domisili-admin');
	}

	public function printKetDomisili($id = null)
	{
		if (!isset($id)) redirect('layanan-domisili-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_sk_domisili', 'tb_surat.id_surat = tb_sk_domisili.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Domisili [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		$image2 = "assets/frame.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN DOMISILI', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(80, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->jkel_pemohon == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->tempat_lahir . ', ' . format_indo(date($query->tgl_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'NIK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->kewarganegaraan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Agama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->status_perkawinan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pendidikan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pendidikan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, '', 0, 0);
		$pdf->Cell(5, 5, '', 0, 0);
		$pdf->Cell(85, 5, '', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 20, 'Keterangan', 0, 0);
		$pdf->Cell(5, 20, ':', 0, 0);
		$pdf->MultiCell(85, 5, $query->keterangan, 0, 'J', false);

		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Demikian surat keterangan domisili ini di buat dengan sebenarnya untuk dipergunakan sebagaimana', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(25, 7, 'mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 95, 255, -150);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, strtoupper($query->kepala_desa), 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}
}
