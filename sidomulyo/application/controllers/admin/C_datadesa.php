<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_datadesa extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library('upload');
	}

	public function pendidikan()
	{
		$data1['title'] = "Data Desa | Pendidikan";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 94;
		$data1['submenu2'] = 0;

		// $ids = $this->session->userdata("usr_id");
		$data2["pendidikan"] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pendidikan/V_pendidikan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_pendidikan()
	{
		$data1['title'] = "Tambah Pendidikan";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 94;
		$data1['submenu2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pendidikan/V_add-pendidikan");
		$this->load->view("admin/V_footer");
	}

	public function save_pendidikan()
	{
		if (isset($_POST['btnSimpanPend'])) {
			$nama_sekolah = $this->input->post('nama_sekolah');
			$kategori_jenjang = $this->input->post('kategori_jenjang');
			$visi_misi = $this->input->post('visi_misi');
			$alamat = $this->input->post('alamat');
			$telp = $this->input->post('telp');
			$jumlah = $this->input->post('jumlah');
			$fasilitas = $this->input->post('fasilitas');
			$prestasi = $this->input->post('prestasi');
			$tgl_now = date('Y-m-d H:i:s');
			// $ids = $this->session->userdata("usr_id");

			//upload foto baru
			$config['upload_path']          = './assets/upload/pendidikan';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['encrypt_name'] = true; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto_utama')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/pendidikan' . $gbr['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/pendidikan' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}

			if ($this->upload->do_upload('struktur_organisasi')) {
				$gbr2 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/pendidikan' . $gbr2['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/pendidikan' . $gbr2['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}

			//simpan data
			$fields = array(
				'kat_jenjang' => $kategori_jenjang,
				'nama_sekolah' => $nama_sekolah,
				'visi_misi' => $visi_misi,
				'alamat_sekolah' => $alamat,
				'telp' => $telp,
				'foto_utama' => $gbr['file_name'],
				'struktur_organisasi' => $gbr2['file_name'],
				'jumlah_murid' => $jumlah,
				'fasilitas' => $fasilitas,
				'prestasi' => $prestasi,
				'date_created' => $tgl_now
			);

			$id_pend = $this->M_crud->simpanData('tb_pendidikan', $fields);

			// input data pegawai
			$jumlah = count($_POST['nama_pegawai']);
			for ($i = 0; $i < $jumlah; $i++) {
				$nama = $_POST['nama_pegawai'][$i];
				$jabatan = $_POST['jabatan_pegawai'][$i];

				$fields3 = array(
					'id_pend' => $id_pend,
					'nama' => $nama,
					'jabatan' => $jabatan
				);

				$this->M_crud->simpanData('tb_warga_sekolah', $fields3);
			}
			// batas data pegawai



			// upload foto detail
			$jumlah = count($_FILES['foto_detail']['name']);
			for ($i = 0; $i < $jumlah; $i++) {
				if (!empty($_FILES['foto_detail']['name'][$i])) {
					$keterangan = $_POST['keterangan'][$i];

					$_FILES['file']['name'] = $_FILES['foto_detail']['name'][$i];
					$_FILES['file']['type'] = $_FILES['foto_detail']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['foto_detail']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['foto_detail']['error'][$i];
					$_FILES['file']['size'] = $_FILES['foto_detail']['size'][$i];

					$config['upload_path']          = './assets/upload/pendidikan';
					$config['allowed_types']        = 'mp4|jpg|jpeg|png';
					$config['encrypt_name'] = true; //nama yang terupload nantinya
					$this->load->library('upload', $config);

					if ($this->upload->do_upload('file')) {
						$gbr3 = $this->upload->data();
						//Compress Image
						$config['image_library'] = 'gd2';
						$config['source_image'] = './assets/upload/pendidikan' . $gbr3['file_name'];
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = true;
						$config['quality'] = '60%';
						$config['width'] = 300;
						//            $config['height']= 300;
						$config['new_image'] = './assets/upload/pendidikan' . $gbr3['file_name'];
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();

						$fields2 = array(
							'id_pend' => $id_pend,
							'foto_detail_sekolah' => $gbr3['file_name'],
							'caption' => $keterangan
						);

						$this->M_crud->simpanData('tb_foto_sekolah', $fields2);
					}
				}
			}
			// batas upload foto detail

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil menyimpan data';
		}

		redirect('admin/C_datadesa/pendidikan');
	}

	public function edit_pendidikan($idp)
	{
		$data1['title'] = "Edit Pendidikan";
		$data1['menu_aktif'] = 9;
		$data1['submenu'] = 94;
		$data1['submenu2'] = 0;

		$data2['parent'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE id_pend = '$idp'")->result();
		$data2['pegawai'] = $this->M_crud->getQuery("SELECT * FROM tb_warga_sekolah WHERE id_pend = '$idp'")->result();
		$data2['foto'] = $this->M_crud->getQuery("SELECT * FROM tb_foto_sekolah WHERE id_pend = '$idp'")->result();
		$data2['no'] = 0;
		$data2['no2'] = 0;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/pendidikan/V_edit-pendidikan", $data2);
		$this->load->view("admin/V_footer");
	}

	public function proses_edit_pendidikan()
	{
		if (isset($_POST['btnEdtPend'])) {
			$idp = $this->input->post('id_pendidikan');
			$nama_sekolah = $this->input->post('nama_sekolah');
			$kategori_jenjang = $this->input->post('kategori_jenjang');
			$visi_misi = $this->input->post('visi_misi');
			$alamat = $this->input->post('alamat');
			$telp = $this->input->post('telp');
			$jumlah = $this->input->post('jumlah');
			$fasilitas = $this->input->post('fasilitas');
			$prestasi = $this->input->post('prestasi');
			$tgl_now = date('Y-m-d H:i:s');
			// $ids = $this->session->userdata("usr_id");

			//upload foto baru
			$config['upload_path']          = './assets/upload/pendidikan';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['encrypt_name'] = true; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if (!empty($_FILES['foto_utama']['name'])) { // jika ada perubahan di foto utama

				if ($this->upload->do_upload('foto_utama')) {
					$gbr = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/pendidikan' . $gbr['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/pendidikan' . $gbr['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					// hapus foto utama lama
					$hasil = $this->M_crud->getQuery("SELECT foto_utama FROM tb_pendidikan WHERE id_pend = '$idp'")->row_array();
					$target = "./assets/upload/pendidikan/" . $hasil['foto_utama'];
					unlink($target); // hapus data utama

					$fields_fu = array(
						'foto_utama' => $gbr['file_name']
					);

					$where_fu = array(
						'id_pend' => $idp
					);

					$this->M_crud->updateData('tb_pendidikan', $fields_fu, $where_fu);
				}
			}

			if (!empty($_FILES['struktur_organisasi']['name'])) { // jika ada perubahan di foto utama

				if ($this->upload->do_upload('struktur_organisasi')) {
					$gbr2 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/pendidikan' . $gbr2['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/pendidikan' . $gbr2['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					// hapus foto utama lama
					$hasil = $this->M_crud->getQuery("SELECT struktur_organisasi FROM tb_pendidikan WHERE id_pend = '$idp'")->row_array();
					$target = "./assets/upload/pendidikan/" . $hasil['struktur_organisasi'];
					unlink($target); // hapus data utama

					$fields_struktur = array(
						'struktur_organisasi' => $gbr2['file_name']
					);

					$where_struktur = array(
						'id_pend' => $idp
					);

					$this->M_crud->updateData('tb_pendidikan', $fields_struktur, $where_struktur);
				}
			}

			//simpan data
			$fields = array(
				'kat_jenjang' => $kategori_jenjang,
				'nama_sekolah' => $nama_sekolah,
				'visi_misi' => $visi_misi,
				'alamat_sekolah' => $alamat,
				'telp' => $telp,
				'jumlah_murid' => $jumlah,
				'fasilitas' => $fasilitas,
				'prestasi' => $prestasi,
				'date_created' => $tgl_now
			);

			$where = array(
				'id_pend' => $idp
			);

			$id_pend = $this->M_crud->updateData('tb_pendidikan', $fields, $where);

			// input data pegawai

			//hapus all dulu
			$this->M_crud->deleteData('tb_warga_sekolah', $where);

			$jumlah = count($_POST['nama_pegawai']);
			for ($i = 0; $i < $jumlah; $i++) {
				$nama = $_POST['nama_pegawai'][$i];
				$jabatan = $_POST['jabatan_pegawai'][$i];

				$fields3 = array(
					'id_pend' => $id_pend,
					'nama' => $nama,
					'jabatan' => $jabatan
				);

				$this->M_crud->simpanData('tb_warga_sekolah', $fields3);
			}
			// batas data pegawai



			// upload foto detail

			//hapus all dulu
			$q = $this->M_crud->getQuery("SELECT foto_detail_sekolah FROM tb_foto_sekolah WHERE id_pend = '$idp'")->result();
			foreach ($q as $key => $value) {
				$target = "./assets/upload/pendidikan/" . $q->foto_detail_sekolah;
				unlink($target); // hapus data utama
			}

			$jumlah = count($_FILES['foto_detail']['name']);
			for ($i = 0; $i < $jumlah; $i++) {
				if (!empty($_FILES['foto_detail']['name'][$i])) {
					$keterangan = $_POST['keterangan'][$i];

					$_FILES['file']['name'] = $_FILES['foto_detail']['name'][$i];
					$_FILES['file']['type'] = $_FILES['foto_detail']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['foto_detail']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['foto_detail']['error'][$i];
					$_FILES['file']['size'] = $_FILES['foto_detail']['size'][$i];

					$config['upload_path']          = './assets/upload/pendidikan';
					$config['allowed_types']        = 'mp4|jpg|jpeg|png';
					$config['encrypt_name'] = true; //nama yang terupload nantinya
					$this->load->library('upload', $config);

					if ($this->upload->do_upload('file')) {
						$gbr3 = $this->upload->data();
						//Compress Image
						$config['image_library'] = 'gd2';
						$config['source_image'] = './assets/upload/pendidikan' . $gbr3['file_name'];
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = true;
						$config['quality'] = '60%';
						$config['width'] = 300;
						//            $config['height']= 300;
						$config['new_image'] = './assets/upload/pendidikan' . $gbr3['file_name'];
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();

						$fields2 = array(
							'id_pend' => $id_pend,
							'foto_detail_sekolah' => $gbr3['file_name'],
							'caption' => $keterangan
						);

						$this->M_crud->simpanData('tb_foto_sekolah', $fields2);
					}
				}
			}
			// batas upload foto detail

			$_SESSION['type'] = 'success';
			$_SESSION['judul'] = '';
			$_SESSION['isi'] = 'Berhasil menyimpan data';
		}

		redirect('admin/C_datadesa/pendidikan');
	}

	public function preview($url)
	{
		$data['foto'] = $url;
		$this->load->view('admin/pendidikan/v_preview', $data);
	}

	public function edit_berita()
	{
		$id_berita = $this->input->post('id_berita');
		$judul = $this->input->post('judul');
		$headline = $this->input->post('headline');
		$foto = $this->input->post('foto');
		$isi = $this->input->post('isi');
		$tgl_now = date('Y-m-d H:i:s');
		$ids = $this->session->userdata("usr_id");
		if (isset($_POST['btnsimpanDraft'])) {
			$tampil = 0;
		} else {
			$tampil = 1;
		}

		$where_foto = array(
			'id_berita' => $id_berita
		);

		if (!empty($_FILES['foto']['name'])) { // jika ada perubahan di foto utama

			//upload foto baru
			$config['upload_path']          = './assets/upload/berita';
			$config['allowed_types']        = 'jpg|jpeg|png';
			// $config['max_size']             = 1024;
			// $config['file_name']             = $uuid;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			$config['encrypt_name'] = true; //nama yang terupload nantinya
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/berita' . $gbr['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/berita' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				// hapus foto utama lama
				$hasil = $this->M_crud->getQuery("SELECT gambar FROM tb_berita WHERE id_berita = '$id_berita'")->row_array();
				$target = "./assets/upload/berita/" . $hasil['gambar'];
				unlink($target); // hapus data utama

				$fields_foto = array(
					'gambar' => $gbr['file_name']
				);
				$this->M_crud->updateData("tb_berita", $fields_foto, $where_foto);
			}
		}

		//update data
		if ($tampil == 1) {
			$fields = array(
				'judul' => $judul,
				'headline' => $headline,
				'isi' => $isi,
				'tampil' => $tampil,
				'tgl_terbit' => $tgl_now,
				'date_updated' => $tgl_now
			);
		} else {
			$fields = array(
				'judul' => $judul,
				'headline' => $headline,
				'isi' => $isi,
				'tampil' => $tampil,
				'date_updated' => $tgl_now
			);
		}

		$this->M_crud->updateData('tb_berita', $fields, $where_foto);

		$_SESSION['type'] = 'success';
		$_SESSION['judul'] = '';
		$_SESSION['isi'] = 'Berhasil mengubah data';

		redirect('admin/C_berita');
	}

	public function hapus($id)
	{
		$where = array('id_berita' => $id);
		$this->M_crud->deleteData("tb_berita", $where);
		redirect(site_url('admin/C_aparatur'));
	}
}
