<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_sktm extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["upload", "pdf"]);
	}

	public function index()
	{
		$data1['title'] = "Data SKTM";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1012;

		$data2['surat_akte'] = $this->M_crud->getQuery("SELECT * FROM tb_surat WHERE tb_surat.jenis_surat = 'sktm' AND tb_surat.aktif = 1 ORDER BY tb_surat.id_surat DESC")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/sktm/V_indexSktm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahSktm()
	{
		// echo "tes";
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1012;
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/sktm/V_tambahSktm", $x);
		$this->load->view("admin/V_footer");
	}

	public function simpanSktm()
	{
		if (isset($_POST['btnSimpanSktm'])) {
			$id_peng = $this->input->post('id_peng');
			//Data Pemohon
			$sktm_nik = $this->input->post('sktm_nik');
			$sktm_nama_lengkap = $this->input->post('sktm_nama_lengkap');
			$sktm_gender = $this->input->post('sktm_gender');
			$sktm_agama = $this->input->post('sktm_agama');
			$sktm_status_kawin = $this->input->post('sktm_status_kawin');
			$sktm_umur = $this->input->post('sktm_umur');
			$sktm_pekerjaan = $this->input->post('sktm_pekerjaan');
			$sktm_no_kk = $this->input->post('sktm_no_kk');
			$sktm_nama_kepala_keluarga = $this->input->post('sktm_nama_kepala_keluarga');
			$sktm_alamat = $this->input->post('sktm_alamat');
			$sktm_keperluan = $this->input->post('sktm_keperluan');
			// $now = date('Y-m-d');

			// Proses Simpan Surat
			$fields_surat = array(
				'jenis_surat' => 'sktm',
				// 'tgl_surat' => $now,
				'id_pengajuan' => $id_peng,
				'nik_pemohon' => $sktm_nik,
				'nama_pemohon' => $sktm_nama_lengkap,
				'progress_pembuatan' => 0,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $fields_surat);

			//Proses upload
			$config['upload_path']          = './assets/upload/layanan/sktm';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['encrypt_name'] = true; // enkripsi nama yang terupload nantinya
			$this->upload->initialize($config);

			// upload berkas Suket lahir dari Desa/Kelurahan
			if ($this->upload->do_upload('sktm_foto_1')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/sktm' . $gbr['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/sktm' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				// Proses Simpan Foto
				$fields_foto1 = array(
					'id_surat' => $id_surat,
					'keterangan_foto' => 1,
					'foto_sktm' => $gbr['file_name']
				);
				$this->M_crud->simpanData('tb_foto_sktm', $fields_foto1);
			}

			// upload berkas FC legalisir sesuai aslinya Surat Nikah / Perkawinan
			if ($this->upload->do_upload('sktm_foto_2')) {
				$gbr2 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/sktm' . $gbr2['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/sktm' . $gbr2['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				// Proses Simpan Foto
				$fields_foto2 = array(
					'id_surat' => $id_surat,
					'keterangan_foto' => 2,
					'foto_sktm' => $gbr2['file_name']
				);
				$this->M_crud->simpanData('tb_foto_sktm', $fields_foto2);
			}

			// upload berkas FC legalisir sesuai aslinya KTP Suami / Istri
			if ($this->upload->do_upload('sktm_foto_3')) {
				$gbr3 = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/layanan/sktm' . $gbr3['file_name'];
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['quality'] = '60%';
				$config['width'] = 300;
				// $config['height']= 300;
				$config['new_image'] = './assets/upload/layanan/sktm' . $gbr3['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				// Proses Simpan Foto
				$fields_foto3 = array(
					'id_surat' => $id_surat,
					'keterangan_foto' => 3,
					'foto_sktm' => $gbr3['file_name']
				);
				$this->M_crud->simpanData('tb_foto_sktm', $fields_foto3);
			}

			// Proses simpan Form sktm
			$fields_sktm = array(
				'id_surat' => $id_surat,
				'pmh_gender' => $sktm_gender,
				'pmh_agama' => $sktm_agama,
				'pmh_umur' => $sktm_umur,
				'pmh_status_kawin' => $sktm_status_kawin,
				'pmh_pekerjaan' => $sktm_pekerjaan,
				'pmh_no_kk' => $sktm_no_kk,
				'pmh_kebutuhan' => $sktm_keperluan,
				'pmh_alamat' => $sktm_alamat,
				'pmh_kepala_keluarga' => $sktm_nama_kepala_keluarga
			);
			$this->M_crud->simpanData('tb_sktm', $fields_sktm);

			// Prose Simpan Indikator
			for ($i = 1; $i <= 8; $i++) {
				$hasil = $this->input->post('indikator' . $i);
				$fields_ind = array(
					'id_surat' => $id_surat,
					'indikator' => $i,
					'kriteria' => $i,
					'hasil' => $hasil
				);

				$this->M_crud->simpanData('tb_indikator_sktm', $fields_ind);
			}

			//Update Progres pengisian
			$fields_upd_progres = array('progress_pembuatan' => 100);
			$where_upd_progres =  array('id_surat' => $id_surat);
			$this->M_crud->updateData("tb_surat", $fields_upd_progres, $where_upd_progres);

			redirect('layanan-sktm-admin');
		}
	}

	public function editSktm($id)
	{
		// echo "tes";
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1012;

		$data2['surat'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_sktm ON tb_surat.id_surat = tb_sktm.id_surat WHERE tb_surat.jenis_surat = 'sktm' AND tb_surat.id_surat = '$id' AND tb_surat.aktif = 1")->row_array();

		$data2['indikator1'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 1 ")->row_array();
		$data2['indikator2'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 2 ")->row_array();
		$data2['indikator3'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 3 ")->row_array();
		$data2['indikator4'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 4 ")->row_array();
		$data2['indikator5'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 5 ")->row_array();
		$data2['indikator6'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 6 ")->row_array();
		$data2['indikator7'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 7 ")->row_array();
		$data2['indikator8'] = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 8 ")->row_array();

		$data2['foto'] = $this->M_crud->getQuery("SELECT * FROM tb_foto_sktm WHERE id_surat = '$id'")->result();
		$data2['no'] = 1;

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/sktm/V_editSktm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function prosesEditSktm()
	{
		if (isset($_POST['btnSimpanEditSktm'])) {

			//Data Pemohon
			$id_surat = $this->input->post('id_surat');
			$sktm_nik = $this->input->post('sktm_nik');
			$sktm_pekerjaan = $this->input->post('sktm_pekerjaan');
			$sktm_nama_lengkap = $this->input->post('sktm_nama_lengkap');
			$sktm_gender = $this->input->post('sktm_gender');
			$sktm_agama = $this->input->post('sktm_agama');
			$sktm_status_kawin = $this->input->post('sktm_status_kawin');
			$sktm_umur = $this->input->post('sktm_umur');
			$sktm_no_kk = $this->input->post('sktm_no_kk');
			$sktm_nama_kepala_keluarga = $this->input->post('sktm_nama_kepala_keluarga');
			$sktm_alamat = $this->input->post('sktm_alamat');
			$sktm_keperluan = $this->input->post('sktm_keperluan');
			$now = date('Y-m-d H:i:s');


			//Proses upload
			$config['upload_path']          = './assets/upload/layanan/sktm';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['encrypt_name'] = true; // enkripsi nama yang terupload nantinya
			$this->upload->initialize($config);

			// upload berkas Suket lahir dari Desa/Kelurahan
			if (!empty($_FILES['sktm_foto_1']['name'])) {
				if ($this->upload->do_upload('sktm_foto_1')) {
					$gbr = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/sktm' . $gbr['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/sktm' . $gbr['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$xy2 = $this->M_crud->getQuery("SELECT foto_sktm FROM tb_foto_sktm WHERE id_surat = '$id_surat' AND keterangan_foto = 1")->row_array();
					$target2 = "./assets/upload/layanan/sktm/" . $xy2['foto_sktm'];
					unlink($target2); // hapus data

					// Proses Update Foto
					$where_foto1 = array(
						'id_surat' => $id_surat,
						'keterangan_foto' => 1
					);
					$fields_foto1 = array(
						'foto_sktm' => $gbr['file_name']
					);
					$this->M_crud->updateData('tb_foto_sktm', $fields_foto1, $where_foto1);
				}
			}

			// upload berkas FC legalisir sesuai aslinya Surat Nikah / Perkawinan
			if (!empty($_FILES['sktm_foto_2']['name'])) {
				if ($this->upload->do_upload('sktm_foto_2')) {
					$gbr2 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/sktm' . $gbr2['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/sktm' . $gbr2['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$xy3 = $this->M_crud->getQuery("SELECT foto_sktm FROM tb_foto_sktm WHERE id_surat = '$id_surat' AND keterangan_foto = 2")->row_array();
					$target3 = "./assets/upload/layanan/sktm/" . $xy3['foto_sktm'];
					unlink($target3); // hapus data

					// Proses Update Foto
					$where_foto2 = array(
						'id_surat' => $id_surat,
						'keterangan_foto' => 2
					);
					$fields_foto2 = array(
						'foto_sktm' => $gbr2['file_name']
					);
					$this->M_crud->updateData('tb_foto_sktm', $fields_foto2, $where_foto2);
				}
			}

			// upload berkas FC legalisir sesuai aslinya KTP Suami / Istri
			if (!empty($_FILES['sktm_foto_3']['name'])) {
				if ($this->upload->do_upload('sktm_foto_3')) {
					$gbr3 = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/upload/layanan/sktm' . $gbr3['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = true;
					$config['quality'] = '60%';
					$config['width'] = 300;
					// $config['height']= 300;
					$config['new_image'] = './assets/upload/layanan/sktm' . $gbr3['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$xy4 = $this->M_crud->getQuery("SELECT foto_sktm FROM tb_foto_sktm WHERE id_surat = '$id_surat' AND keterangan_foto = 3")->row_array();
					$target4 = "./assets/upload/layanan/sktm/" . $xy4['foto_sktm'];
					unlink($target4); // hapus data

					// Proses Update Foto
					$where_fot31 = array(
						'id_surat' => $id_surat,
						'keterangan_foto' => 3
					);
					$fields_foto3 = array(
						'foto_sktm' => $gbr3['file_name']
					);
					$this->M_crud->updateData('tb_foto_sktm', $fields_foto3, $where_fot31);
				}
			}

			// Proses update Surat
			$fields_surat = array(
				'nik_pemohon' => $sktm_nik,
				'nama_pemohon' => $sktm_nama_lengkap
			);

			$where = array('id_surat' => $id_surat);
			$this->M_crud->updateData('tb_surat', $fields_surat, $where);

			// Proses update Form sktm
			$fields_p_akte = array(
				'pmh_gender' => $sktm_gender,
				'pmh_agama' => $sktm_agama,
				'pmh_umur' => $sktm_umur,
				'pmh_status_kawin' => $sktm_status_kawin,
				'pmh_pekerjaan' => $sktm_pekerjaan,
				'pmh_no_kk' => $sktm_no_kk,
				'pmh_kebutuhan' => $sktm_keperluan,
				'pmh_alamat' => $sktm_alamat,
				'pmh_kepala_keluarga' => $sktm_nama_kepala_keluarga
			);
			$this->M_crud->updateData('tb_sktm', $fields_p_akte, $where);

			// Prose update Indikator
			$hasil1 = $this->input->post('indikator1');
			$hasil2 = $this->input->post('indikator2');
			$hasil3 = $this->input->post('indikator3');
			$hasil4 = $this->input->post('indikator4');
			$hasil5 = $this->input->post('indikator5');
			$hasil6 = $this->input->post('indikator6');
			$hasil7 = $this->input->post('indikator7');
			$hasil8 = $this->input->post('indikator8');

			$fields_ind1 = array(
				'hasil' => $hasil1
			);
			$where_ind1 = array(
				'id_surat' => $id_surat,
				'indikator' => 1,
				'kriteria' => 1
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind1, $where_ind1);

			$fields_ind2 = array(
				'hasil' => $hasil2
			);
			$where_ind2 = array(
				'id_surat' => $id_surat,
				'indikator' => 2,
				'kriteria' => 2
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind2, $where_ind2);

			$fields_ind3 = array(
				'hasil' => $hasil3
			);
			$where_ind3 = array(
				'id_surat' => $id_surat,
				'indikator' => 3,
				'kriteria' => 3
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind3, $where_ind3);

			$fields_ind4 = array(
				'hasil' => $hasil4
			);
			$where_ind4 = array(
				'id_surat' => $id_surat,
				'indikator' => 4,
				'kriteria' => 4
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind4, $where_ind4);

			$fields_ind5 = array(
				'hasil' => $hasil5
			);
			$where_ind5 = array(
				'id_surat' => $id_surat,
				'indikator' => 5,
				'kriteria' => 5
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind5, $where_ind5);

			$fields_ind6 = array(
				'hasil' => $hasil6
			);
			$where_ind6 = array(
				'id_surat' => $id_surat,
				'indikator' => 6,
				'kriteria' => 6
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind6, $where_ind6);

			$fields_ind7 = array(
				'hasil' => $hasil7
			);
			$where_ind7 = array(
				'id_surat' => $id_surat,
				'indikator' => 7,
				'kriteria' => 7
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind7, $where_ind7);

			$fields_ind8 = array(
				'hasil' => $hasil8
			);
			$where_ind8 = array(
				'id_surat' => $id_surat,
				'indikator' => 8,
				'kriteria' => 8
			);
			$this->M_crud->updateData('tb_indikator_sktm', $fields_ind8, $where_ind8);

			redirect('layanan-sktm-admin');
		}
	}

	public function lihatSktm($id)
	{
		// echo "tes";
		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 1012;

		$data2['sktm'] = $this->M_crud->getQuery("SELECT * FROM tb_surat JOIN tb_sktm ON tb_surat.id_surat = tb_sktm.id_surat WHERE tb_surat.id_surat = '$id'")->row_array();

		$data2['foto'] = $this->M_crud->getQuery("SELECT * FROM tb_foto_sktm WHERE id_surat = '$id'")->result();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/sktm/V_lihatSktm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function doneSktm($id = null)
	{
		$fields = array(
			'status_pembuatan' => "Selesai",
			'tgl_surat' => date('Y-m-d')
		);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-sktm-admin');
	}


	public function hapusSktm($id = null)
	{
		$fields = array('aktif' => 0);
		$where = array('id_surat' => $id);

		$this->M_crud->updateData('tb_surat', $fields, $where);

		redirect('layanan-sktm-admin');
	}

	public function printSktm($id = null)
	{
		if (!isset($id)) redirect('layanan-sktm-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_sktm', 'tb_surat.id_surat = tb_sktm.id_surat');
		$this->db->where('tb_surat.id_surat', $id);
		$query = $this->db->get()->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();
		$namakades = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='kades'")->row();
		$indikator1 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 1 ")->row();
		$indikator2 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 2 ")->row();
		$indikator3 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 3 ")->row();
		$indikator4 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 4 ")->row();
		$indikator5 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 5 ")->row();
		$indikator6 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 6 ")->row();
		$indikator7 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 7 ")->row();
		$indikator8 = $this->M_crud->getQuery("SELECT hasil FROM tb_indikator_sktm WHERE id_surat = '$id' AND indikator = 8 ")->row();


		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Tidak Mampu [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN TIDAK MAMPU', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 5, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(80, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 3, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);

		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Nama Kepala Keluarga', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_kepala_keluarga, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pmh_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 4, '', 0, 0);
		$pdf->Cell(5, 4, '', 0, 0);
		$pdf->Cell(85, 4, '', 0, 1);
		$pdf->SetFillColor(255, 255, 255);
		// ngatur footer 
		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Benar-benar sebagai keluarga miskin ( terlantar / tidak memiliki tempat tinggal) yang saat ini ditampung di rumah saudaranya berdasarkan penilaian sesuai indikator / kriteria keluarga miskin tersebut di bawah ini :', 0, 'FJ', 1);
		$pdf->Cell(10, 5, '', 0, 1);

		// untuk ngatur tabel 
		$pdf->SetX(13);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(10, 10, 'NO', 1, 0, 'C');
		$pdf->Cell(55, 10, 'INDIKATOR', 1, 0, 'C');
		$pdf->Cell(85, 10, 'KRITERIA', 1, 0, 'C');
		$pdf->Cell(15, 10, 'YA', 1, 0, 'C');
		$pdf->Cell(20, 10, 'TIDAK', 1, 1, 'C');

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '1', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Luas lantai rumah', 1, 0, 'L');
		$pdf->Cell(85, 5, '40 M2', 1, 0, 'L');
		if ($indikator1->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator1->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '2', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Dinding rumah', 1, 0, 'L');
		$pdf->Cell(85, 5, 'Bambu / tembok tanpa plester', 1, 0, 'L');
		if ($indikator2->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator2->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '3', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Jenis lantai rumah', 1, 0, 'L');
		$pdf->Cell(85, 5, 'Tanah / plester semen', 1, 0, 'L');
		if ($indikator3->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator3->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '4', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Penerangan rumah', 1, 0, 'L');
		$pdf->Cell(85, 5, '40 M2', 1, 0, 'L');
		if ($indikator4->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator4->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '5', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Sumber air bersih', 1, 0, 'L');
		$pdf->Cell(85, 5, 'Sungai / sumber / sumur', 1, 0, 'L');
		if ($indikator5->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator5->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '6', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Tabungan / simpanan', 1, 0, 'L');
		$pdf->Cell(85, 5, 'Nilain < Rp. 1.000.000', 1, 0, 'L');
		if ($indikator6->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator6->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '7', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Pekerjaan', 1, 0, 'L');
		$pdf->Cell(85, 5, 'Tidak bekerja / tenaga kasar', 1, 0, 'L');
		if ($indikator7->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator7->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(10, 5, '8', 1, 0, 'C');
		$pdf->Cell(55, 5, 'Jamban keluarga', 1, 0, 'L');
		$pdf->Cell(85, 5, 'Tidak punya jamban', 1, 0, 'L');
		if ($indikator8->hasil == 1) {
			$pdf->Cell(15, 5, 'V', 1, 0, 'C');
		} else {
			$pdf->Cell(15, 5, '-', 1, 0, 'C');
		}
		if ($indikator8->hasil == 0) {
			$pdf->Cell(20, 5, 'V', 1, 1, 'C');
		} else {
			$pdf->Cell(20, 5, '-', 1, 1, 'C');
		}
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 10, 'Surat keterangan ini diberikan sebagai persyaratan untuk ' . $query->pmh_kebutuhan, 0, 'FJ', 1);
		$pdf->Cell(10, 5, '', 0, 1);

		$pdf->SetX(13);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Demikian SKTM ini dibuat dengan sebenarnya dan apabila keterangan ini tidak benar maka saya selaku Kepala Desa sanggup mengembalikan semua biaya pengobatan yang dikeluarkan rumah sakit.', 0, 'FJ', 1);
		$pdf->Cell(10, 15, '', 0, 1);

		// untuk ngatur footer
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, $namakades->keterangan, 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}

	public function printSktmDomisili($id = null)
	{
		if (!isset($id)) redirect('layanan-sktm-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_sktm', 'tb_surat.id_surat = tb_sktm.id_surat');
		$this->db->where('tb_surat.id_surat', $id);
		$query = $this->db->get()->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();
		$namakades = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='kades'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('SKTM Domisili [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN DOMISILI', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(80, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_sk_domisili', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);

		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Umur', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_umur . ' Tahun', 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'NIK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->pmh_gender == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Agama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_status_kawin, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'No. KK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->pmh_no_kk, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 5, 'Alamat', 0, 0);
		$pdf->Cell(5, 5, ':', 0, 0);
		$pdf->Cell(85, 5, $query->pmh_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro - Kabupaten Jember', 0, 1);
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(13);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang bersangkutan adalah benar-benar orang terlantar dan bukan penduduk Desa Sidomulyo, yang saat ini berdomisili di rumah saudaranya dengan alamat domisili ' . $query->pmh_alamat . ' Kecamatan Semboro - Kabupaten Jember.', 0, 'FJ', 1);
		$pdf->Cell(10, 8, '', 0, 1);


		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(152, 7, 'Demikian surat keterangan ini diberikan untuk dipergunakan sebagaimana mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, $namakades->keterangan, 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}

	public function printSktmFoto($id = null)
	{
		if (!isset($id)) redirect('layanan-sktm-admin');

		date_default_timezone_set('Asia/Jakarta');

		$query1 = $this->M_crud->getQuery("SELECT * FROM tb_foto_sktm WHERE id_surat = '$id' AND keterangan_foto=1")->row();
		$query2 = $this->M_crud->getQuery("SELECT * FROM tb_foto_sktm WHERE id_surat = '$id' AND keterangan_foto=2")->row();
		$query3 = $this->M_crud->getQuery("SELECT * FROM tb_foto_sktm WHERE id_surat = '$id' AND keterangan_foto=3")->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();
		$namakades = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='kades'")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Domisili Foto [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";

		$image2 = "assets/upload/layanan/sktm/" . $query1->foto_sktm;
		$image3 = "assets/upload/layanan/sktm/" . $query2->foto_sktm;
		$image4 = "assets/upload/layanan/sktm/" . $query3->foto_sktm;
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'FOTO / KONDISI', 0, 1, 'C');


		$pdf->Image($image2, 57, 60, 115, 60);
		$pdf->SetY(120);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(210, 7, 'Tampak Depan', 0, 1, 'C');
		$pdf->Image($image3, 57, 130, 115, 60);
		$pdf->SetY(190);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(210, 7, 'Tampak Samping', 0, 1, 'C');
		$pdf->Image($image4, 57, 200, 115, 60);
		$pdf->SetY(260);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(210, 7, 'Tampak Belakang', 0, 1, 'C');

		$pdf->Cell(10, 15, '', 0, 1);
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, $namakades->keterangan, 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}
}
