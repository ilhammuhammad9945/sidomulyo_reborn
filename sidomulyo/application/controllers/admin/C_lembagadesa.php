<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_lembagadesa extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function rules_1()
	{
		return [
			[
				'field' => 'fnama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fasal',
				'label' => 'Asal',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}


	public function index()
	{

		$data1['title'] = "Data Pengurus Muslimat";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 71;
		$data1['submenu2'] = 0;
		$data2["muslimat"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Muslimat'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/muslimat/V_muslimat", $data2);
		$this->load->view("admin/V_footer");
	}

	public function aisyiyah()
	{

		$data1['title'] = "Data Pengurus Aisyiyah";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 72;
		$data1['submenu2'] = 0;
		$data2["aisyiyah"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Aisyiyah'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aisyiyah/V_aisyiyah", $data2);
		$this->load->view("admin/V_footer");
	}

	public function sanggar_seni()
	{

		$data1['title'] = "Data Pengurus Sanggar Seni Budaya";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 73;
		$data1['submenu2'] = 0;
		$data2["ssb"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='SSB'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/ssb/V_ssb", $data2);
		$this->load->view("admin/V_footer");
	}

	public function lsm()
	{

		$data1['title'] = "Data Pengurus LSM";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 74;
		$data1['submenu2'] = 0;
		$data2["lsm"] = $this->M_crud->getQuery("SELECT lm.*,jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='LSM'")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/lsm/V_lsm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_muslimat()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 71;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Muslimat'")->result();
		$data2["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Muslimat'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'Muslimat';
			$asal = $this->input->post('fasal');
			$foto = $this->_uploadImageMus($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('muslimat-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/muslimat/V_tambahmuslimat", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_aisyiyah()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 72;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='Aisyiyah'")->result();
		$data2["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Aisyiyah'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'Aisyiyah';
			$asal = $this->input->post('fasal');
			$foto = $this->_uploadImageAis($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('aisyiyah-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aisyiyah/V_tambahaisyiyah", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_ssb()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 73;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='SSB'")->result();
		$data2["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal SSB'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'SSB';
			$asal = $this->input->post('fasal');
			$foto = $this->_uploadImageSsb($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('ssb-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/ssb/V_tambahssb", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_lsm()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 74;
		$data1['submenu2'] = 0;
		$data2["jabatan"] = $this->M_crud->getQuery("SELECT * FROM tb_jabatan WHERE keterangan='LSM'")->result();
		$data2["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal LSM'")->result();
		if ($validation->run()) {
			$id = uniqid();
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$keterangan = 'LSM';
			$asal = $this->input->post('fasal');
			$foto = $this->_uploadImageLsm($id);
			$datanya = array(
				'id_lm' => $id,
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'keterangan_jabatan' => $keterangan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);
			$this->M_crud->simpanData('tb_lemmas', $datanya);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('lsm-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/lsm/V_tambahlsm", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit_muslimat($id = null)
	{
		if (!isset($id)) redirect('muslimat-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 71;
		$data1['submenu2'] = 0;
		$data["mus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Muslimat'")->result();
		$data["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Muslimat'")->result();
		if (!$data["mus"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$asal = $this->input->post('fasal');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageMus($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('muslimat-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/muslimat/V_editmuslimat", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_aisyiyah($id = null)
	{
		if (!isset($id)) redirect('aisyiyah-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 72;
		$data1['submenu2'] = 0;
		$data["ais"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Aisyiyah'")->result();
		$data["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal Aisyiyah'")->result();
		if (!$data["ais"]) show_404();
		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$asal = $this->input->post('fasal');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageAis($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('aisyiyah-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aisyiyah/V_editaisyiyah", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_ssb($id = null)
	{
		if (!isset($id)) redirect('ssb-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 73;
		$data1['submenu2'] = 0;
		$data["ssb"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='SSB'")->result();
		$data["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal SSB'")->result();
		if (!$data["ssb"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$asal = $this->input->post('fasal');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageSsb($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('ssb-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/ssb/V_editssb", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_lsm($id = null)
	{
		if (!isset($id)) redirect('lsm-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_1());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 7;
		$data1['submenu'] = 74;
		$data1['submenu2'] = 0;
		$data["lsm"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN `tb_jabatan` AS jb ON lm.jabatan_lm = jb.id_jbt WHERE lm.id_lm='" . $id . "'")->row();
		$data['jabatan'] = $this->M_crud->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='LSM'")->result();
		$data["asal"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE keterangan_org='Asal LSM'")->result();
		if (!$data["lsm"]) show_404();

		if ($validation->run()) {
			$id = $this->input->post('fid');
			$nama = $this->input->post('fnama');
			$jkel = $this->input->post('fjkel');
			$telp = $this->input->post('ftelp');
			$alamat = $this->input->post('falamat');
			$jabatan = $this->input->post('fjabatan');
			$asal = $this->input->post('fasal');
			if (!empty($_FILES["ffoto"]["name"])) {
				$foto = $this->_uploadImageLsm($id);
			} else {
				$foto = $this->input->post('ffotolama');
			}

			$datanya = array(
				'nama_lm' => $nama,
				'jkel_lm' => $jkel,
				'no_telp' => $telp,
				'alamat_lm' => $alamat,
				'jabatan_lm' => $jabatan,
				'asal_organisasi' => $asal,
				'foto_lm' => $foto
			);

			$where = array(
				'id_lm' => $id
			);
			$this->M_crud->updateData('tb_lemmas', $datanya, $where);
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('lsm-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/lsm/V_editlsm", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus_muslimat($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageMus($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('muslimat-admin');
		}
	}

	public function hapus_aisyiyah($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageAis($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('aisyiyah-admin');
		}
	}

	public function hapus_ssb($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageSsb($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('ssb-admin');
		}
	}

	public function hapus_lsm($id = null)
	{
		if (!isset($id)) show_404();

		$where = array(
			'id_lm' => $id
		);
		$this->_deleteImageLsm($id);
		$datanya = $this->M_crud->deleteData('tb_lemmas', $where);
		if ($datanya) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('lsm-admin');
		}
	}

	private function _uploadImageMus($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/muslimat/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageAis($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/aisyiyah/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageSsb($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/ssb/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _uploadImageLsm($id = null)
	{

		if (!isset($id)) show_404();
		$config['upload_path']          = './assets/upload/lsm/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = $id;
		$config['overwrite']            = true;
		$config['max_size']             = 1024 * 5; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('ffoto')) {
			return $this->upload->data("file_name");
		}
		// print_r($this->upload->display_errors());
		return "default.jpg";
	}

	private function _deleteImageMus($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/muslimat/$filename.*"));
		}
	}

	private function _deleteImageAis($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/aisyiyah/$filename.*"));
		}
	}

	private function _deleteImageSsb($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/ssb/$filename.*"));
		}
	}

	private function _deleteImageLsm($id)
	{
		$foto = $this->M_crud->getQuery("SELECT * FROM tb_lemmas WHERE id_lm='" . $id . "'")->row();
		if ($foto->foto_lm != "default.jpg") {
			$filename = explode(".", $foto->foto_lm)[0];
			return array_map('unlink', glob(FCPATH . "assets/upload/lsm/$filename.*"));
		}
	}
}
