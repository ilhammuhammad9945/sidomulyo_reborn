<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_aparatur extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_aparatur"]);
		if ($this->M_login->isNotLogin()) redirect(site_url('C_login'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data1['title'] = "Data Aparatur Desa";
		$data1['menu_aktif'] = 5;
		$data1['submenu'] = 51;
		$data1['submenu2'] = 0;
		$data2["aparatur"] = $this->M_aparatur->getAll();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aparatur/V_aparatur", $data2);
		$this->load->view("admin/V_footer");
	}

	public function bpd()
	{
		$data1['title'] = "Data Aparatur BPD";
		$data1['menu_aktif'] = 5;
		$data1['submenu'] = 52;
		$data1['submenu2'] = 0;
		$data2["aparatur_bpd"] = $this->M_aparatur->getAll_bpd();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aparatur/V_aparatur_bpd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah()
	{
		$aparatur = $this->M_aparatur;
		$validation = $this->form_validation;
		$validation->set_rules($aparatur->rules());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 5;
		$data1['submenu'] = 51;
		$data1['submenu2'] = 0;
		$data2['jabatan'] = $aparatur->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Aparatur Desa'")->result();
		$data2['atasan'] = $aparatur->getQuery("SELECT ap.id_apt, ap.nama_apt, jb.nama_jbt FROM `tb_aparatur` AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.level='Atasan' AND ap.ket_apt='Aparatur Desa'")->result();

		if ($validation->run()) {
			$aparatur->save_desa();
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('aparatur-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aparatur/V_tambah_apt", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambah_bpd()
	{
		$aparatur = $this->M_aparatur;
		$validation = $this->form_validation;
		$validation->set_rules($aparatur->rules());
		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 5;
		$data1['submenu'] = 52;
		$data1['submenu2'] = 0;
		$data2['jabatan'] = $aparatur->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Aparatur BPD'")->result();
		$data2['atasan'] = $aparatur->getQuery("SELECT ap.id_apt, ap.nama_apt, jb.nama_jbt FROM `tb_aparatur` AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.level='Atasan' AND ap.ket_apt='Aparatur BPD'")->result();

		if ($validation->run()) {
			$aparatur->save_bpd();
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('bpd-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aparatur/V_tambah_bpd", $data2);
		$this->load->view("admin/V_footer");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect('aparatur-admin');

		$aparatur = $this->M_aparatur;
		$validation = $this->form_validation;
		$validation->set_rules($aparatur->rules());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 5;
		$data1['submenu'] = 51;
		$data1['submenu2'] = 0;
		$data['jabatan'] = $aparatur->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Aparatur Desa'")->result();
		$data["aparatur"] = $aparatur->getQuery("SELECT ap.*, jb.* FROM tb_aparatur AS ap JOIN `tb_jabatan` AS jb ON ap.jabatan_apt = jb.id_jbt WHERE ap.id_apt='" . $id . "'")->row();
		// $data["aparatur"] = $aparatur->getById($id);
		if (!$data["aparatur"]) show_404();
		$data['data_atasan'] = $aparatur->getQuery("SELECT ap.id_apt, ap.nama_apt, jb.id_jbt,jb.nama_jbt FROM `tb_aparatur` AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.level='Atasan' AND ap.ket_apt='Aparatur Desa' AND id_apt NOT LIKE '" . $id . "'")->result();

		if ($validation->run()) {
			$aparatur->update_desa();
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('aparatur-admin');
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aparatur/V_edit_apt", $data);
		$this->load->view("admin/V_footer");
	}

	public function edit_bpd($id = null)
	{
		if (!isset($id)) redirect('bpd-admin');

		$aparatur = $this->M_aparatur;
		$validation = $this->form_validation;
		$validation->set_rules($aparatur->rules());
		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 5;
		$data1['submenu'] = 52;
		$data1['submenu2'] = 0;
		$data['jabatan'] = $aparatur->getQuery("SELECT * FROM `tb_jabatan` WHERE keterangan='Aparatur BPD'")->result();
		$data["aparatur"] = $aparatur->getQuery("SELECT ap.*, jb.* FROM tb_aparatur AS ap JOIN `tb_jabatan` AS jb ON ap.jabatan_apt = jb.id_jbt WHERE ap.id_apt='" . $id . "'")->row();
		// $data["aparatur"] = $aparatur->getById($id);
		if (!$data["aparatur"]) show_404();
		$data['data_atasan'] = $aparatur->getQuery("SELECT ap.id_apt, ap.nama_apt, jb.nama_jbt FROM `tb_aparatur` AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.level='Atasan' AND ap.ket_apt='Aparatur BPD' AND id_apt NOT LIKE '" . $id . "'")->result();

		if ($validation->run()) {
			$aparatur->update_bpd();
			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('bpd-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/aparatur/V_edit_bpd", $data);
		$this->load->view("admin/V_footer");
	}

	public function hapus($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->M_aparatur->delete($id)) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('aparatur-admin');
		}
	}

	public function hapus_bpd($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->M_aparatur->delete($id)) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('bpd-admin');
		}
	}
}
