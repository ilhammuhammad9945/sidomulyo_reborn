<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_belumnikah extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(["M_login", "M_crud", "M_layanan_surat"]);
		if ($this->M_login->isNotLogin()) {
			redirect(site_url('C_login'));
		}
		$this->load->library(["form_validation", "pdf"]);
	}

	public function rules()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],
			[
				'field' => 'fnik',
				'label' => 'NIK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fwni',
				'label' => 'Kewarganegaraan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpendidikan',
				'label' => 'Pendidikan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpersyaratan',
				'label' => 'Persyaratan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function rules_2()
	{
		return [
			// [
			// 	'field' => 'fnosurat',
			// 	'label' => 'No. Surat',
			// 	'rules' => 'required',
			// 	'errors' => array(
			// 		'required' => ' %s tidak boleh kosong'
			// 	)
			// ],

			[
				'field' => 'fnik',
				'label' => 'NIK',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamalengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fjkel',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'ftgllahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fnamanegara',
				'label' => 'Kewarganegaraan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpekerjaan',
				'label' => 'Pekerjaan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fagama',
				'label' => 'Agama',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpendidikan',
				'label' => 'Pendidikan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fstatuskawin',
				'label' => 'Status Kawin',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'falamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			],

			[
				'field' => 'fpersyaratan',
				'label' => 'Persyaratan',
				'rules' => 'required',
				'errors' => array(
					'required' => ' %s tidak boleh kosong'
				)
			]

		];
	}

	public function index()
	{

		$data1['title'] = "Data Surat Belum Nikah";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10110;
		$data2["belumnikah"] = $this->M_crud->getQuery("SELECT sr.*, nkh.* FROM tb_surat AS sr JOIN tb_belum_nikah AS nkh ON sr.id_surat=nkh.id_surat WHERE sr.jenis_surat ='belum nikah' AND sr.aktif=1 ORDER BY sr.id_surat DESC")->result();
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/belumnikah/V_indexBn", $data2);
		$this->load->view("admin/V_footer");
	}

	public function tambahBelumNikah()
	{

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		date_default_timezone_set('Asia/Jakarta');
		// $tglskrg = date('Y-m-d');

		$data1['title'] = "Tambah Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10110;
		// $x['invoice'] = $this->M_layanan_surat->get_id_domisili();
		$x['invoice'] = $this->M_layanan_surat->get_id_peng();

		if ($validation->run()) {
			$idpengajuan = $this->input->post('fidpengajuanbn');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$jkel = $this->input->post('fjkel');
			$tempatlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$namanegara = $this->input->post('fnamanegara');
			$pekerjaan = $this->input->post('fpekerjaan');
			$agama = $this->input->post('fagama');
			$pendidikan = $this->input->post('fpendidikan');
			$statuskawin = $this->input->post('fstatuskawin');
			$alamat = $this->input->post('falamat');
			$persyaratan = $this->input->post('fpersyaratan');

			$dataMaster = array(
				'jenis_surat' => 'belum nikah',
				// 'tgl_surat' => $tglskrg,
				// 'no_surat' => $nosurat,
				'id_pengajuan' => $idpengajuan,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap,
				'progress_pembuatan' => 100,
				'status_pembuatan' => 'Dalam Proses',
				'aktif' => 1,
				'id_user' => $this->session->userdata('usr_id')
			);
			$id_surat = $this->M_crud->simpanData('tb_surat', $dataMaster);

			$dataTurunan = array(
				'id_surat' => $id_surat,
				'nkh_jkel' => $jkel,
				'nkh_tempat_lahir' => $tempatlahir,
				'nkh_tgl_lahir' => $tgllahir,
				'nkh_kwn' => $namanegara,
				'nkh_pekerjaan' => $pekerjaan,
				'nkh_agama' => $agama,
				'nkh_pendidikan' => $pendidikan,
				'nkh_status_kwn' => $statuskawin,
				'nkh_alamat' => $alamat,
				'nkh_persyaratan' => $persyaratan
			);
			$this->M_crud->simpanData('tb_belum_nikah', $dataTurunan);
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('layanan-belum-nikah-admin');

			// $cek_nosurat = $this->M_crud->getQuery("SELECT no_surat FROM tb_belum_nikah WHERE no_surat='" . $nosurat . "'")->row();
			// Kalau no surat sudah ada
			// if ($cek_nosurat > 0) 
			// 	$this->session->set_flashdata('error', 'Nomor surat sudah ada, gunakan nomor lain');
			// 	redirect('tambah-domisili');
			// 
			// Kalau no surat tidak ada, inputkan data ke tabel
			// else 


			// 
		}
		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/belumnikah/V_tambahBn", $x);
		$this->load->view("admin/V_footer");
	}

	public function editBelumNikah($id = null)
	{
		if (!isset($id)) redirect('layanan-belum-nikah-admin');

		$validation = $this->form_validation;
		$validation->set_rules($this->rules_2());

		$data1['title'] = "Edit Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10110;
		$data2["belumnikah"] = $this->M_crud->getQuery("SELECT sr.*, nkh.* FROM tb_surat AS sr JOIN tb_belum_nikah AS nkh ON sr.id_surat=nkh.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["belumnikah"]) show_404();

		if ($validation->run()) {
			$idsurat = $this->input->post('fidsurat');
			// $nosurat = $this->input->post('fnosurat');
			$nik = $this->input->post('fnik');
			$namalengkap = $this->input->post('fnamalengkap');
			$jkel = $this->input->post('fjkel');
			$tempatlahir = $this->input->post('ftempatlahir');
			$tgllahir = $this->input->post('ftgllahir');
			$namanegara = $this->input->post('fnamanegara');
			$pekerjaan = $this->input->post('fpekerjaan');
			$agama = $this->input->post('fagama');
			$pendidikan = $this->input->post('fpendidikan');
			$statuskawin = $this->input->post('fstatuskawin');
			$alamat = $this->input->post('falamat');
			$persyaratan = $this->input->post('fpersyaratan');

			$dataMaster = array(
				// 'no_surat' => $nosurat,
				'nik_pemohon' => $nik,
				'nama_pemohon' => $namalengkap
			);

			$where = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_surat', $dataMaster, $where);

			$dataTurunan = array(
				'nkh_jkel' => $jkel,
				'nkh_tempat_lahir' => $tempatlahir,
				'nkh_tgl_lahir' => $tgllahir,
				'nkh_kwn' => $namanegara,
				'nkh_pekerjaan' => $pekerjaan,
				'nkh_agama' => $agama,
				'nkh_pendidikan' => $pendidikan,
				'nkh_status_kwn' => $statuskawin,
				'nkh_alamat' => $alamat,
				'nkh_persyaratan' => $persyaratan
			);

			$where2 = array(
				'id_surat' => $idsurat
			);
			$this->M_crud->updateData('tb_belum_nikah', $dataTurunan, $where2);

			$this->session->set_flashdata('success', 'Berhasil diupdate');
			redirect('layanan-belum-nikah-admin');
		}

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/belumnikah/V_editBn", $data2);
		$this->load->view("admin/V_footer");
	}

	public function ubahBelumNikah()
	{

		$idsurat = $this->input->post('fid');
		$ket = $this->input->post('fstatus_pembuatan');

		$data = array(
			'tgl_surat' => date('Y-m-d'),
			'status_pembuatan' => $ket

		);

		$where = array(
			'id_surat' => $idsurat
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		$this->session->set_flashdata('success', 'Berhasil diupdate');
		redirect('layanan-belum-nikah-admin');
	}

	public function lihatBelumNikah($id = null)
	{
		if (!isset($id)) redirect('layanan-belum-nikah-admin');

		$data1['title'] = "Lihat Data";
		$data1['menu_aktif'] = 10;
		$data1['submenu'] = 101;
		$data1['submenu2'] = 10110;
		$data2["belumnikah"] = $this->M_crud->getQuery("SELECT sr.*, nkh.* FROM tb_surat AS sr JOIN tb_belum_nikah AS nkh ON sr.id_surat=nkh.id_surat WHERE sr.id_surat ='" . $id . "'")->row();
		if (!$data2["belumnikah"]) show_404();

		$this->load->view("admin/V_header", $data1);
		$this->load->view("admin/belumnikah/V_lihatBn", $data2);
		$this->load->view("admin/V_footer");
	}

	public function hapusBelumNikah($id = null)
	{
		if (!isset($id)) show_404();

		// ini kodingan hapus soft
		$data = array(
			'aktif' => 0

		);

		$where = array(
			'id_surat' => $id
		);
		$this->M_crud->updateData('tb_surat', $data, $where);

		// ini kodingan hapus hard
		// $where1 = array(
		// 	'id_surat' => $id
		// );
		// $where2 = array(
		// 	'id_surat' => $id
		// );
		// $data1 = $this->M_crud->deleteData('tb_surat', $where1);
		// $data2 = $this->M_crud->deleteData('tb_belum_nikah', $where2);

		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		redirect('layanan-belum-nikah-admin');
	}

	public function printBelumNikah($id = null)
	{
		if (!isset($id)) redirect('layanan-belum-nikah-admin');

		date_default_timezone_set('Asia/Jakarta');

		$this->db->select('*');
		$this->db->from('tb_surat');
		$this->db->join('tb_belum_nikah', 'tb_surat.id_surat = tb_belum_nikah.id_surat');
		$this->db->where('tb_surat.id_pengajuan', $id);
		$query = $this->db->get()->row();

		$namacamat = $this->M_crud->getQuery("SELECT * FROM tb_setting_surat WHERE jenis='camat'")->row();
		$namakades = $this->M_crud->getQuery("SELECT nama_apt FROM tb_aparatur WHERE jabatan_apt=1")->row();

		$pdf = new FPDF('P', 'mm', 'Legal');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetTitle('Surat Keterangan Belum Nikah [ ' . $id . ' ]');
		$image1 = "assets/pemkabjember.png";
		$image2 = "assets/frame.png";
		// $image1 = "http://2.bp.blogspot.com/-TtPPgAlY14I/Uaot22gXV1I/AAAAAAAAAUM/YrItpNy443k/s1600/Pemkab+Jember.png";
		// $pdf->Cell(40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 20), 10, 1, 'P', false);
		// ukuran 1 = jarak dari kiri, ukuran 2 = jarak dari atas, ukuran 3 = besar gambar
		$pdf->Image($image1, 30, 10, -680);
		$pdf->SetFont('Times', 'B', 16);
		// mencetak string 
		$pdf->Cell(210, 7, 'PEMERINTAH KABUPATEN JEMBER', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 16);
		$pdf->Cell(210, 7, 'KECAMATAN SEMBORO', 0, 1, 'C');
		$pdf->SetFont('Times', 'B', 18);
		$pdf->Cell(210, 7, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Jalan Merdeka No. 01 Sidomulyo Kode Pos 68157', 0, 1, 'C');
		$pdf->SetLineWidth(0.3);
		$pdf->Line(15, 40, 210 - 10, 40);

		// ngatur judul surat
		$pdf->Cell(10, 10, '', 0, 1);
		$pdf->SetFont('Times', 'BU', 18);
		$pdf->Cell(210, 7, 'SURAT KETERANGAN BELUM PERNAH NIKAH', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(210, 7, 'Nomor : 470/       /35.09.07.2006/' . date("Y"), 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur paragraf pertama
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Yang bertanda tangan dibawah ini Kepala Desa Sidomulyo, Kecamatan Semboro Kabupaten Jember', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(80, 7, 'menerangkan dengan sebenarnya bahwa :', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		// ngatur tabel isi surat 
		$pdf->SetFont('Times', '', 12);
		// $query = $this->db->get_where('tb_belum_nikah', array('id_pengajuan_sk' => '435kdg9fg'), $limit, $offset);
		// $pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Nama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(85, 10, strtoupper($query->nama_pemohon), 0, 1);
		$pdf->SetX(25);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(70, 10, 'Jenis Kelamin', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		if ($query->nkh_jkel == "L") {
			$pdf->Cell(85, 10, 'Laki-laki', 0, 1);
		} else {
			$pdf->Cell(85, 10, 'Perempuan', 0, 1);
		}
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Tempat / Tanggal Lahir', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nkh_tempat_lahir . ', ' . format_indo(date($query->nkh_tgl_lahir)), 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'NIK', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nik_pemohon, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Kewarganegaraan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nkh_kwn, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Agama', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nkh_agama, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Status Perkawinan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nkh_status_kwn, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pendidikan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nkh_pendidikan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, 'Pekerjaan', 0, 0);
		$pdf->Cell(5, 10, ':', 0, 0);
		$pdf->Cell(85, 10, $query->nkh_pekerjaan, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 7, 'Alamat', 0, 0);
		$pdf->Cell(5, 7, ':', 0, 0);
		$pdf->MultiCell(95, 7, $query->nkh_alamat, 0, 1);
		$pdf->SetX(25);
		$pdf->Cell(70, 10, '', 0, 0);
		$pdf->Cell(5, 10, '', 0, 0);
		$pdf->Cell(85, 10, 'Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 0, 1);
		$pdf->Cell(10, 3, '', 0, 1);

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetX(14);
		$pdf->SetFont('Times', '', 12);
		$pdf->MultiCell(185, 7, 'Yang bersangkutan adalah benar-benar penduduk Desa Sidomulyo yang beralamat seperti tersebut di atas dan berdasarkan data kependudukan dan pernyataan yang bersangkutan benar-benar sudah cerai hidup dan surat keterangan ini diberikan sebagai persyaratan ' . $query->nkh_persyaratan . '.', 0, 1, 'FJ', false);

		// ngatur footer 
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(200, 7, 'Demikian surat keterangan domisili ini di buat dengan sebenarnya untuk dipergunakan sebagaimana', 0, 1, 'C');
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(25, 7, 'mestinya.', 0, 1, 'C');
		$pdf->Cell(10, 8, '', 0, 1);

		$pdf->Image($image2, 90, 255, -130);

		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 5, 'Sidomulyo, ' . format_indo(date('Y-m-d')), 0, 1, 'R');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(68, 5, 'Plt. CAMAT SEMBORO', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, 'KEPALA DESA SIDOMULYO', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 10, '', 0, 0, 'C');
		$pdf->Cell(25, 10, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(85, 10, '', 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(68, 7, $namacamat->keterangan, 0, 0, 'C');
		$pdf->Cell(25, 7, '', 0, 0);
		$pdf->SetFont('Times', 'BU', 12);
		$pdf->Cell(113, 7, strtoupper($namakades->nama_apt), 0, 1, 'C');
		$pdf->SetX(15);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(68, 5, 'NIP. ' . $namacamat->lain_lain, 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(113, 5, '', 0, 1, 'C');


		$pdf->Output();
	}
}
