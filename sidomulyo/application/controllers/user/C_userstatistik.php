<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userstatistik extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
	}

	public function index()
	{
		$data['title'] = "Statistik";
		date_default_timezone_set('Asia/Jakarta');
		$data['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data['hrsekarang'] = $this->hari_ini(date('l'));
		$data['wktsekarang'] = date('H:i');

		$data["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();

		$this->count_visitor();

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$data_foot['penduduk'] = $this->M_crud->getQuery("SELECT * FROM tb_statistik WHERE judul_statistik = 'All' ")->row_array();
		$data_foot['gender'] = $this->M_crud->getQuery("SELECT * FROM tb_statistik WHERE judul_statistik = 'Gender' ")->result();
		$data_foot['usia'] = $this->M_crud->getQuery("SELECT * FROM tb_statistik WHERE judul_statistik = 'Usia' ")->result();
		$data_foot['pekerjaan'] = $this->M_crud->getQuery("SELECT * FROM tb_statistik WHERE judul_statistik = 'Pekerjaan' ")->result();

		$this->load->view('user/V_header', $data);
		$this->load->view('user/datadesa/V_statistik');
		$this->load->view('user/V_footer', $data_foot);
	}

	public function count_visitor()
	{
		$ip = $_SERVER['REMOTE_ADDR'];

		$cek_ip = $this->M_crud->cek_ip_visitor($ip);
		$cek = $cek_ip->num_rows();

		if ($cek == 0) {
			$this->M_crud->simpan_ip_visitor($ip);
		}
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
