<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userlembagadesa extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
	}

	public function index()
	{
		$data1['title'] = "Muslimat";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=11")->row();
		$data2["grup"] = $this->M_crud->getQuery("SELECT asal_organisasi FROM `tb_lemmas` WHERE keterangan_jabatan='Muslimat' GROUP BY asal_organisasi")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_muslimat', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function aisyiyah()
	{
		$data1['title'] = "Aisyiyah";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=12")->row();
		$data2["grup"] = $this->M_crud->getQuery("SELECT asal_organisasi FROM `tb_lemmas` WHERE keterangan_jabatan='Aisyiyah' GROUP BY asal_organisasi")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_aisyiyah', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function sanggar_seni()
	{
		$data1['title'] = "Sanggar Seni Budaya";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=13")->row();
		$data2["grup"] = $this->M_crud->getQuery("SELECT asal_organisasi FROM `tb_lemmas` WHERE keterangan_jabatan='SSB' GROUP BY asal_organisasi")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_ssb', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function lsm()
	{
		$data1['title'] = "LSM";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=14")->row();
		$data2["grup"] = $this->M_crud->getQuery("SELECT asal_organisasi FROM `tb_lemmas` WHERE keterangan_jabatan='LSM' GROUP BY asal_organisasi")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_lsm', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}


	public function detail_muslimat($id = null)
	{
		if (!isset($id)) {
			redirect('lembaga-desa/muslimat');
		}
		$ubah = str_replace("_", " ", $id);
		$data1['title'] = "Detail Muslimat";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["namamuslimat"] = ucwords($ubah);
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.asal_organisasi='" . $ubah . "'")->result();
		$data2["alamat"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE nama_org='" . $ubah . "'")->row();
		if (!$data2["pengurus"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_detailmuslimat', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_aisyiyah($id = null)
	{
		if (!isset($id)) {
			redirect('lembaga-desa/aisyiyah');
		}
		$ubah = str_replace("_", " ", $id);
		$data1['title'] = "Detail Aisyiyah";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["namaaisyiyah"] = ucwords($ubah);
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.asal_organisasi='" . $ubah . "'")->result();
		$data2["alamat"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE nama_org='" . $ubah . "'")->row();
		if (!$data2["pengurus"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_detailaisyiyah', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_ssb($id = null)
	{
		if (!isset($id)) {
			redirect('lembaga-desa/sanggar-seni');
		}
		$ubah = str_replace("_", " ", $id);
		$data1['title'] = "Detail Sanggar Seni Budaya";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["namasanggar"] = ucwords($ubah);
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.asal_organisasi='" . $ubah . "'")->result();
		$data2["alamat"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE nama_org='" . $ubah . "'")->row();
		if (!$data2["pengurus"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_detailssb', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_lsm($id = null)
	{
		if (!isset($id)) {
			redirect('lembaga-desa/lsm');
		}
		$ubah = str_replace("_", " ", $id);
		$data1['title'] = "Detail LSM";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["namalsm"] = strtoupper($ubah);
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.asal_organisasi='" . $ubah . "'")->result();
		$data2["alamat"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE nama_org='" . $ubah . "'")->row();
		if (!$data2["pengurus"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lembagadesa/V_detaillsm', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
