<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userberita extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
		$this->load->library('pagination');
	}

	public function index()
	{
		$data1['title'] = "Berita";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');

		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();

		//konfigurasi pagination
		$config['base_url'] = site_url('user/C_userberita/index'); //site url

		if (isset($_SESSION['cari_berita'])) {
			$total_rows = $this->M_crud->getQuery("SELECT COUNT('id_berita') AS jml FROM tb_berita WHERE tampil = 1 AND judul LIKE '%$_SESSION[cari_berita]%'")->row_array(); //total row
			$config['total_rows'] = $total_rows['jml'];
			$config['per_page'] = 3;  //show record per halaman
			$config["uri_segment"] = 4;  // uri parameter
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);

			// Membuat Style pagination untuk BootStrap v4
			$config['next_link']        = '&raquo;';
			$config['prev_link']        = '&laquo;';
			$config['full_tag_open']    = '<div class="row justify-content-center"><div class="pagination pt-1 pb-5">';
			$config['full_tag_close']   = '</div></div>';
			$config['cur_tag_open']     = '<a class="active" href="#">';
			$config['cur_tag_close']    = '</a>';

			$this->pagination->initialize($config);
			$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

			$data2["berita"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 AND tb_berita.judul LIKE '%$_SESSION[cari_berita]%' ORDER BY tb_berita.tgl_terbit DESC LIMIT $data[page], $config[per_page]")->result();

			unset($_SESSION['cari_berita']);
		} else {
			$total_rows = $this->M_crud->getQuery("SELECT COUNT('id_berita') AS jml FROM tb_berita WHERE tampil = 1")->row_array(); //total row
			$config['total_rows'] = $total_rows['jml'];
			$config['per_page'] = 3;  //show record per halaman
			$config["uri_segment"] = 4;  // uri parameter
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);

			$config['next_link']        = '&raquo;';
			$config['prev_link']        = '&laquo;';
			$config['full_tag_open']    = '<div class="row justify-content-center"><div class="pagination pt-1 pb-5">';
			$config['full_tag_close']   = '</div></div>';
			$config['cur_tag_open']     = '<a class="active" href="#">';
			$config['cur_tag_close']    = '</a>';

			// Membuat Style pagination untuk BootStrap v4
			// $config['first_link']       = 'First';
			// $config['last_link']        = 'Last';
			// $config['next_link']        = 'Next';
			// $config['prev_link']        = 'Prev';
			// $config['full_tag_open']    = '<nav class="blog-pagination justify-content-center d-flex"><ul class="pagination">';
			// $config['full_tag_close']   = '</ul></nav>';
			// $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
			// $config['num_tag_close']    = '</span></li>';
			// $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
			// $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
			// $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
			// $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
			// $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
			// $config['prev_tagl_close']  = '</span>Next</li>';
			// $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
			// $config['first_tagl_close'] = '</span></li>';
			// $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
			// $config['last_tagl_close']  = '</span></li>';

			$this->pagination->initialize($config);
			$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

			$data2["berita"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT $data[page], $config[per_page]")->result();
		}

		$data2['pagination'] = $this->pagination->create_links();

		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/berita/V_berita', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function cari_berita()
	{
		$txt_cari = $this->input->post('search');

		$_SESSION['cari_berita'] = $txt_cari;

		redirect('user/C_userberita');
	}

	public function resetcariberita()
	{
		unset($_SESSION['cari_berita']);
		redirect('user/C_userberita');
	}

	public function detail_berita($id_berita, $url)
	{
		$data1['title'] = "Berita";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');

		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();

		$data2["berita"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.id_berita = '$id_berita'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$this->add_count($id_berita);

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/berita/V_detail-berita', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function add_count($id_berita)
	{
		$ip = $_SERVER['REMOTE_ADDR'];

		$cek_ip = $this->M_crud->cek_ip($ip, $id_berita);
		$cek = $cek_ip->num_rows();

		if ($cek == 0) {
			$this->M_crud->simpan_user_agent($ip, $id_berita);
			$this->M_crud->update_counter($id_berita);
		}
	}

	public function next_page()
	{
		$_SESSION['pagination'] = $_SESSION['pagination'] + 4;
		$_SESSION['page'] = $_SESSION['page'] + 1;

		redirect('C_userberita');
	}

	public function prev_page()
	{
		$_SESSION['pagination'] = $_SESSION['pagination'] - 4;
		$_SESSION['page'] = $_SESSION['page'] - 1;

		redirect('C_userberita');
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
