<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userdatadesa extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
		$this->load->library('pagination');
	}

	public function pendidikan()
	{
		$data1['title'] = "Pendidikan";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$paud = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 1");
		$tk = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 2");
		$sd = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 3");
		$smp = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 4");
		$sma = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 5");

		$data2['paud'] = $paud->result();
		$data2['tk'] = $tk->result();
		$data2['sd'] = $sd->result();
		$data2['smp'] = $smp->result();
		$data2['sma'] = $sma->result();

		$data2['jml_paud'] = $paud->num_rows();
		$data2['jml_tk'] = $tk->num_rows();
		$data2['jml_sd'] = $sd->num_rows();
		$data2['jml_smp'] = $smp->num_rows();
		$data2['jml_sma'] = $sma->num_rows();

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/pendidikan/V_pendidikan', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_pendidikan($id_pend)
	{
		$data1['title'] = "Pendidikan";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();

		$data2['parent'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE id_pend = '$id_pend'")->result();
		$data2['pegawai'] = $this->M_crud->getQuery("SELECT * FROM tb_warga_sekolah WHERE id_pend = '$id_pend'")->result();
		$data2['no'] = 1;
		$data2["lengkap_pendidikan"] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan JOIN tb_warga_sekolah ON tb_pendidikan.id_pend = tb_warga_sekolah.id_pend JOIN tb_foto_sekolah ON tb_pendidikan.id_pend = tb_foto_sekolah.id_pend WHERE tb_pendidikan.id_pend = '$id_pend'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data2['jml_paud'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 1")->num_rows();
		$data2['jml_tk'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 2")->num_rows();
		$data2['jml_sd'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 3")->num_rows();
		$data2['jml_smp'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 4")->num_rows();
		$data2['jml_sma'] = $this->M_crud->getQuery("SELECT * FROM tb_pendidikan WHERE kat_jenjang = 5")->num_rows();

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/pendidikan/V_detail-pendidikan', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function next_page()
	{
		$_SESSION['pagination'] = $_SESSION['pagination'] + 4;
		$_SESSION['page'] = $_SESSION['page'] + 1;

		redirect('C_userberita');
	}

	public function prev_page()
	{
		$_SESSION['pagination'] = $_SESSION['pagination'] - 4;
		$_SESSION['page'] = $_SESSION['page'] - 1;

		redirect('C_userberita');
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
