<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userprofil extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
	}

	public function index()
	{
		$data1['title'] = "Sejarah Desa";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=1")->row();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_sejarah', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function struktur_organisasi_desa()
	{
		$data1['title'] = "Struktur Organisasi Pemerintah Desa";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=2")->row();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_str_desa', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function aparatur_desa()
	{
		$data1['title'] = "Aparatur Pemerintah Desa";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		// $data2["aparatur"] = $this->M_crud->getQuery("SELECT ap.*,jb.* FROM tb_aparatur AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.ket_apt='Aparatur Desa' AND ap.level='Atasan'")->result();
		$data2["aparatur"] = $this->M_crud->getQuery("SELECT ap.*,jb.* FROM tb_aparatur AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.ket_apt='Aparatur Desa'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_aparaturdesa', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function struktur_organisasi_bpd()
	{
		$data1['title'] = "Struktur Organisasi BPD";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=3")->row();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_str_bpd', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function aparatur_bpd()
	{
		$data1['title'] = "Aparatur BPD";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		// $data2["aparatur"] = $this->M_crud->getQuery("SELECT ap.*,jb.* FROM tb_aparatur AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.ket_apt='Aparatur BPD' AND ap.level='Atasan'")->result();
		$data2["aparatur"] = $this->M_crud->getQuery("SELECT ap.*,jb.* FROM tb_aparatur AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.ket_apt='Aparatur BPD'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_aparaturbpd', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_aparatur_desa($id = null)
	{
		if (!isset($id)) {
			redirect('profil/aparatur-pemerintah-desa');
		}
		$data1['title'] = "Detail Aparatur Desa";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["aparatur"] = $this->M_crud->getQuery("SELECT ap.*,jb.* FROM tb_aparatur AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.ket_apt='Aparatur Desa' AND ap.id_apt='" . $id . "'")->row();
		if (!$data2["aparatur"]) {
			show_404();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_detaildesa', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_aparatur_bpd($id = null)
	{
		if (!isset($id)) {
			redirect('profil/aparatur-bpd');
		}
		$data1['title'] = "Detail Aparatur BPD";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["aparatur"] = $this->M_crud->getQuery("SELECT ap.*,jb.* FROM tb_aparatur AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.ket_apt='Aparatur BPD' AND ap.id_apt='" . $id . "'")->row();
		if (!$data2["aparatur"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/profil/V_detailbpd', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
