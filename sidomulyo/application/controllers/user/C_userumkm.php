<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userumkm extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
	}

	public function index()
	{
		$data1['title'] = "Daftar UMKM";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=20")->row();
		$data2["isi"] = $this->M_crud->getQuery("SELECT * FROM tb_umkm")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/datadesa/V_umkm', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function produk_unggulan()
	{
		$data1['title'] = "Daftar Produk Unggulan";
		date_default_timezone_set('Asia/Jakarta');
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=21")->row();
		$data2["isi"] = $this->M_crud->getQuery("SELECT pr.*, um.* FROM tb_produk_unggulan AS pr JOIN tb_umkm AS um ON pr.umkm_pr=um.id_umkm")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/datadesa/V_produk', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
