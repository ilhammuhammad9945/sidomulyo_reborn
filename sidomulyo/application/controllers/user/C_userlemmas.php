<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_userlemmas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_crud");
	}

	public function index()
	{
		$data1['title'] = "PKK";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=4")->row();
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='PKK' OR lm.keterangan_jabatan LIKE '%Pokja%'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_pkk', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}


	public function lpmd()
	{
		$data1['title'] = "LPMD";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=5")->row();
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='LPMD'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_lpmd', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function karang_taruna()
	{
		$data1['title'] = "Karang Taruna";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=6")->row();
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Karang Taruna' OR lm.keterangan_jabatan LIKE '%Seksi%'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_karangtaruna', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function posyandu()
	{
		$data1['title'] = "Posyandu";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=7")->row();
		$data2["grup"] = $this->M_crud->getQuery("SELECT asal_organisasi FROM `tb_lemmas` WHERE asal_organisasi LIKE '%Posyandu%' GROUP BY asal_organisasi")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_posyandu', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function rtrw()
	{
		$data1['title'] = "RT / RW";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=8")->row();
		$data2["grup"] = $this->M_crud->getQuery("SELECT * FROM `tb_lemmas` WHERE asal_organisasi LIKE '%RW%' ORDER BY asal_organisasi ASC")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_rtrw', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function karang_werda()
	{
		$data1['title'] = "Karang Werda";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=9")->row();
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='Karang Werda';")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}
		// $data2["grup"] = $this->M_crud->getQuery("SELECT asal_organisasi FROM `tb_lemmas` WHERE keterangan_jabatan LIKE '%Werda%' GROUP BY asal_organisasi")->result();

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_karangwerda', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function rumah_desa_sehat()
	{
		$data1['title'] = "Rumah Desa Sehat";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["konten"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis=10")->row();
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='RDS'")->result();
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_rds', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	// public function detail_werda($id = null)
	// {
	// 	if (!isset($id)) redirect('lembaga-kemasyarakatan/karang-werda');
	// 	$ubah = str_replace("_", " ", $id);
	// 	$data1['title'] = "Detail Karang Werda";
	// 	$data2["namawerda"] = ucwords($ubah);
	// 	$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.asal_organisasi='" . $ubah . "'")->result();
	// 	if (!$data2["pengurus"]) show_404();
	// 	$this->load->view('user/V_header', $data1);
	// 	$this->load->view('user/lemmas/V_detailwerda', $data2);
	// 	$this->load->view('user/V_footer');
	// }


	public function detail_posyandu($id = null)
	{
		if (!isset($id)) {
			redirect('lembaga-kemasyarakatan/posyandu');
		}
		$ubah = str_replace("_", " ", $id);
		$data1['title'] = "Detail Posyandu";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["namapos"] = ucwords($ubah);
		$data2["alamat"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE nama_org='" . $ubah . "'")->row();
		$data2["pengurus"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.asal_organisasi='" . $ubah . "'")->result();
		if (!$data2["pengurus"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_detailposyandu', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	public function detail_rtrw($id = null)
	{
		if (!isset($id)) {
			redirect('lembaga-kemasyarakatan/rt-rw');
		}
		$ubah = str_replace("_", " ", $id);
		$data1['title'] = "Detail RT / RW";
		$data1['tglsekarang'] = $this->tgl_indo(date('Y-m-d'));
		$data1['hrsekarang'] = $this->hari_ini(date('l'));
		$data1['wktsekarang'] = date('H:i');
		$data1["info"] = $this->M_crud->getQuery("SELECT * FROM tb_konten WHERE jenis = 99 ")->result();
		$data2["rt"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='RT' AND lm.lain_lain='" . $ubah . "'")->result();
		$data2["rw"] = $this->M_crud->getQuery("SELECT lm.*, jb.* FROM tb_lemmas AS lm JOIN tb_jabatan AS jb ON lm.jabatan_lm=jb.id_jbt WHERE lm.keterangan_jabatan='RW' AND lm.asal_organisasi='" . $ubah . "'")->row();
		$data2['namarw'] = strtoupper($ubah);
		$data2["alamat"] = $this->M_crud->getQuery("SELECT * FROM tb_organisasi WHERE nama_org='" . $ubah . "'")->row();
		if (!$data2["rt"]) {
			show_404();
		}
		$data2['new_berita'] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.tgl_terbit DESC LIMIT 3")->result();
		$jum_berita = $this->M_crud->getQuery("SELECT count(id_berita) AS jumlah FROM tb_berita WHERE tb_berita.tampil = 1")->row_array();
		if ($jum_berita['jumlah'] > 3) {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC LIMIT 1, 3")->result();
		} else {
			$data2["berita_populer"] = $this->M_crud->getQuery("SELECT * FROM tb_berita JOIN tb_user ON tb_berita.editor = tb_user.id_user WHERE tb_berita.tampil = 1 ORDER BY tb_berita.views DESC")->result();
		}

		$data_foot['pengunjung_today'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_today FROM tb_log_pengunjung WHERE DATE(tanggal_kunjungan) = CURDATE() ")->row_array();
		$data_foot['pengunjung_weekly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_weekly FROM tb_log_pengunjung WHERE YEARWEEK(tanggal_kunjungan) = YEARWEEK(NOW())")->row_array();
		$data_foot['pengunjung_monthly'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_monthly FROM tb_log_pengunjung WHERE MONTH(tanggal_kunjungan) = MONTH(NOW())")->row_array();
		$data_foot['pengunjung_alltime'] = $this->M_crud->getQuery("SELECT COUNT(id_log_pengunjung) AS jml_alltime FROM tb_log_pengunjung")->row_array();

		$this->load->view('user/V_header', $data1);
		$this->load->view('user/lemmas/V_detailrtrw', $data2);
		$this->load->view('user/V_footer', $data_foot);
	}

	function hari_ini($ini)
	{
		$hari = $ini;

		switch ($hari) {
			case 'Sunday':
				$hari_ini = "Minggu";
				break;

			case 'Monday':
				$hari_ini = "Senin";
				break;

			case 'Tuesday':
				$hari_ini = "Selasa";
				break;

			case 'Wednesday':
				$hari_ini = "Rabu";
				break;

			case 'Thursday':
				$hari_ini = "Kamis";
				break;

			case 'Friday':
				$hari_ini = "Jumat";
				break;

			case 'Saturday':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}

	public function tgl_indo($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
