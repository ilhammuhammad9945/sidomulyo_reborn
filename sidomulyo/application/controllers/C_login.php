<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_login");
		$this->load->library('form_validation');
	}


	public function index()
	{
		// jika form login disubmit
		if ($this->input->post()) {
			if ($this->M_login->doLogin()) redirect('beranda-admin');
		}

		$data['title'] = "Login";
		$this->load->view('admin/V_login', $data);
	}

	public function logout()
	{
		// hancurkan semua sesi
		$this->session->sess_destroy();
		redirect('login-admin');
	}
}
