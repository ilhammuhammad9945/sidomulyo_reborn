<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pelaporan Kematian</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php elseif ($this->session->flashdata('error')) : ?>
			<div class="alert alert-error" role="alert">
				<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php endif; ?>

		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" id="nikpemohon" name="fnikpemohon" autofocus tabindex="1" />
						</div>
						
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" id="tglpemohon" name="ftglpemohon" tabindex="2" />
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="box-footer">
				<button class="btn btn-primary" type="button" id="btnCari" name="btnCariPemohon" tabindex="3">Cari Data</button>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Surat Pelaporan Kematian</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('edit-pelaporan-kematian') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" class="form-control" name="fidsurat" value="<?php echo $plkm->id_surat; ?>" readonly>
							<!-- <div class="form-group">
								<label>No. Surat</label>
								<input type="text" class="form-control" id="nosurat" name="fnosurat" tabindex="1" autofocus onkeypress="return hanyaAngka(event)">
							</div> -->
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Pelapor</label>
								<input type="text" class="form-control" id="nik1" name="fnik1" tabindex="1" onkeypress="return hanyaAngka(event)" value="<?php echo $plkm->nik_pemohon; ?>">
								<span style="color: red">
									<?php echo form_error('fnik1') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Pelapor</label>
								<input type="text" class="form-control" id="namapl" name="fnamapl" tabindex="2" value="<?php echo $plkm->nama_pemohon; ?>">
								<span style="color: red">
									<?php echo form_error('fnamapl') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur Pelapor</label>
								<input type="number" class="form-control" id="umurpl" name="fumurpl" tabindex="3" value="<?php echo $plkm->kmt_umur; ?>">
								<span style="color: red">
									<?php echo form_error('fumurpl') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan Pelapor</label>
								<input type="text" class="form-control" id="pekerjaanpl" name="fpekerjaanpl" tabindex="4" value="<?php echo $plkm->kmt_pekerjaan; ?>">
								<span style="color: red">
									<?php echo form_error('fpekerjaanpl') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Pelapor</label>
								<input type="text" class="form-control" id="alamatpl" name="falamatpl" tabindex="5" value="<?php echo $plkm->kmt_alamat; ?>">
								<span style="color: red">
									<?php echo form_error('falamatpl') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Hubungan dengan yang Mati</label>
								<input type="text" class="form-control" id="hubunganpl" name="fhubunganpl" tabindex="6" value="<?php echo $plkm->kmt_hubungan; ?>">
								<span style="color: red">
									<?php echo form_error('fhubunganpl') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Yang Meninggal</label>
								<input type="text" class="form-control" id="namamng" name="fnamamng" tabindex="7" value="<?php echo $plkm->kmt_nama_mati; ?>">
								<span style="color: red">
									<?php echo form_error('fnamamng') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK yang Meninggal</label>
								<input type="text" class="form-control" id="nik2" name="fnik2" tabindex="8" value="<?php echo $plkm->kmt_nik_mati; ?>">
								<span style="color: red">
									<?php echo form_error('fnik2') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin yang Meninggal</label>
								<select class="form-control select2" style="width: 100%;" id="jkelmng" name="fjkelmng" tabindex="9">
									<?php if ($plkm->kmt_jkel_mati == 'L') { ?>
										<option value="<?php echo $plkm->kmt_jkel_mati ?>">Laki-laki</option>
									<?php } else { ?>
										<option value="<?php echo $plkm->kmt_jkel_mati ?>">Perempuan</option>
									<?php } ?>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkelmng') ?>
								</span>
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Lahir yang Meninggal</label>
								<input type="date" class="form-control" id="tgllahirmng" name="ftgllahirmng" tabindex="10" value="<?php echo $plkm->kmt_tgl_lahir_mati; ?>">
								<span style="color: red">
									<?php echo form_error('ftgllahirmng') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur yang Meninggal</label>
								<input type="number" class="form-control" id="umurmng" name="fumurmng" tabindex="11" value="<?php echo $plkm->kmt_umur_mati; ?>">
								<span style="color: red">
									<?php echo form_error('fumurmng') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Agama yang Meninggal</label>
								<select class="form-control select2" style="width: 100%;" id="agamamng" name="fagamamng" tabindex="12">
									<option value="<?php echo $plkm->kmt_agama_mati ?>"><?php echo $plkm->kmt_agama_mati ?></option>
									<option value="Islam">Islam</option>
									<option value="Protestan">Protestan</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Khonghucu">Khonghucu</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fagamamng') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat yang Meninggal</label>
								<input type="text" class="form-control" id="alamatmng" name="falamatmng" tabindex="13" value="<?php echo $plkm->kmt_alamat_mati ?>">
								<span style="color: red">
									<?php echo form_error('falamatmng') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Kematian</label>
										<input type="date" class="form-control" id="tglmng" name="ftglmng" tabindex="14" value="<?php echo $plkm->kmt_tgl_mati ?>">
										<span style="color: red">
											<?php echo form_error('ftglmng') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-lg-6 -->
								<div class="col-lg-6">
									<div class="form-group">
										<label>Pukul Kematian</label>
										<input type="time" class="form-control" id="pukulmng" name="fpukulmng" tabindex="15" value="<?php echo $plkm->kmt_pukul_mati ?>">
										<span style="color: red">
											<?php echo form_error('fpukulmng') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-lg-6 -->
							</div>
							<!-- /.row -->

							<div class="form-group">
								<label>Bertempat Di</label>
								<input type="text" class="form-control" id="tempatmng" name="ftempatmng" tabindex="16" value="<?php echo $plkm->kmt_tempat_mati ?>">
								<span style="color: red">
									<?php echo form_error('ftempatmng') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Penyebab Kematian</label>
								<input type="text" class="form-control" id="penyebab" name="fpenyebab" tabindex="17" value="<?php echo $plkm->kmt_penyebab ?>">
								<span style="color: red">
									<?php echo form_error('fpenyebab') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Upload Ulang Bukti Kematian / </label>
								<i class="fa fa-eye"></i>
								<a href="<?php echo base_url('assets/upload/layanan/pelaporankematian/' . $plkm->kmt_bukti_mati) ?>" target="_blank">Lihat Gambar</a>
								<input type="file" class="form-control" id="buktimng" name="fbuktimng" tabindex="18">
								<input type="hidden" name="ffotolama" value="<?php echo $plkm->kmt_bukti_mati ?>" />
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Keterangan Bukti Kematian</label>
								<input type="text" class="form-control" id="keterangan" name="fketerangan" tabindex="19" value="<?php echo $plkm->kmt_keterangan ?>">
								<span style="color: red">
									<?php echo form_error('fketerangan') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" id="btnSimpan" name="btnSimpan" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-pelaporan-kematian-admin') ?>" class="btn btn-default" tabindex="21">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
