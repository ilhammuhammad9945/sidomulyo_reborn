<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pelaporan Kematian</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Pelaporan Kematian</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>ID Pengajuan</td>
										<td>:</td>
										<td><?php echo $plkm->id_pengajuan ?></td>
									</tr>
									<tr>
										<td>Tgl Pengajuan</td>
										<td>:</td>
										<td><?php echo format_indo(date('Y-m-d'), strtotime($plkm->created_at)) ?></td>
									</tr>
									<tr>
										<td>Nomor Surat</td>
										<td>:</td>
										<td><?php echo '470/......./35.09.07.2006/' . date("Y")  ?></td>
									</tr>
									<tr>
										<td>NIK Pelapor</td>
										<td>:</td>
										<td><?php echo $plkm->nik_pemohon ?></td>
									</tr>
									<tr>
										<td>Nama Pelapor</td>
										<td>:</td>
										<td><?php echo $plkm->nama_pemohon ?></td>
									</tr>
									<tr>
										<td>Umur Pelapor</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_umur . ' Tahun' ?></td>
									</tr>
									<tr>
										<td>Pekerjaan Pelapor</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_pekerjaan ?></td>
									</tr>
									<tr>
										<td>Alamat Pelapor</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_alamat ?></td>
									</tr>
									<tr>
										<td>Hubungan Dengan Yang Mati</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_hubungan ?></td>
									</tr>
									<tr>
										<td>Nama Yang Meninggal</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_nama_mati ?></td>
									</tr>
									<tr>
										<td>NIK Yang Meninggal</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_nik_mati ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin Yang Meninggal</td>
										<td>:</td>
										<?php if ($plkm->kmt_jkel_mati) { ?>
											<td>Laki-laki</td>
										<?php } else { ?>
											<td>Perempuan</td>
										<?php } ?>
									</tr>
									<tr>
										<td>Tanggal Lahir/Umur Yang Meninggal</td>
										<td>:</td>
										<td><?php echo format_indo(date($plkm->kmt_tgl_lahir_mati)) . ', ' . $plkm->kmt_umur_mati . ' Tahun' ?></td>
									</tr>
									<tr>
										<td>Agama Yang Meninggal</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_agama_mati ?></td>
									</tr>
									<tr>
										<td>Alamat Yang Meninggal</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_alamat_mati ?></td>
									</tr>
									<tr>
										<td>Hari Meninggal</td>
										<td>:</td>
										<?php
										$day = date('D', strtotime($plkm->kmt_tgl_mati));
										$dayList = array(
											'Sun' => 'Minggu',
											'Mon' => 'Senin',
											'Tue' => 'Selasa',
											'Wed' => 'Rabu',
											'Thu' => 'Kamis',
											'Fri' => 'Jumat',
											'Sat' => 'Sabtu'
										);
										?>
										<td><?php echo $dayList[$day] ?></td>
									</tr>
									<tr>
										<td>Tanggal Kematian</td>
										<td>:</td>
										<td><?php echo format_indo(date($plkm->kmt_tgl_mati)) ?></td>
									</tr>
									<tr>
										<td>Pukul Kematian</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_pukul_mati . ' WIB' ?></td>
									</tr>
									<tr>
										<td>Bertempat di</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_tempat_mati ?></td>
									</tr>
									<tr>
										<td>Penyebab Kematian</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_penyebab ?></td>
									</tr>
									<tr>
										<td>Bukti Kematian</td>
										<td>:</td>
										<td><?php echo $plkm->kmt_keterangan ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<div class="box-footer">
						<h4>Bukti :</h4>
						<ul class="mailbox-attachments clearfix">
							<li>
								<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>assets/upload/layanan/pelaporankematian/<?php echo $plkm->kmt_bukti_mati ?>" alt="Attachment"></span>
								<div class="mailbox-attachment-info">
									<a href="<?php echo base_url(); ?>assets/upload/layanan/pelaporankematian/<?php echo $plkm->kmt_bukti_mati ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i> Lihat Berkas</a>
								</div>
							</li>

						</ul>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('print-pelaporan-kematian/' . $plkm->id_pengajuan) ?>" class="btn btn-primary" target="_blank">Cetak Surat Pelaporan Kematian</a>
						<a href="<?php echo site_url('layanan-pelaporan-kematian-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
