<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li>Permohonan Surat</li>
			<li class="active">Ket. Usaha</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success" role="alert">
						<?php echo $this->session->flashdata('success'); ?>
					</div>
				<?php endif; ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('tambah-usaha') ?>" class="btn btn-block btn-primary">Tambah Data</a></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<!-- <th>Nomor Surat</th> -->
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($skusaha as $sku) : ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo format_indo(date('Y-m-d', strtotime($sku->created_at)))  ?></td>
											<!-- <td><?php //echo $sku->no_surat; 
														?></td> -->
											<td><?php echo $sku->id_pengajuan; ?></td>
											<td><?php echo $sku->nama_pemohon; ?></td>
											<?php if ($sku->status_pembuatan == 'Selesai') { ?>
												<td><small class="label label-success"> Selesai</small></td>
											<?php } else { ?>
												<td><small class="label label-danger"> Dalam Proses</small></td>
											<?php } ?>


											<td>
												<a href="<?php echo site_url('edit-usaha/' . $sku->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<a href="<?php echo site_url('lihat-usaha/' . $sku->id_surat) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>
												<a data-toggle="modal" data-target="#UpdateModal<?php echo $sku->id_surat; ?>" class="btn btn-warning btn-sm" title="Konfirmasi Selesai"><i class="fa fa-check"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-usaha/' . $sku->id_surat) ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<!-- <th>Nomor Surat</th> -->
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<?php foreach ($skusaha as $sku) { ?>
	<div class="modal fade" id="UpdateModal<?php echo $sku->id_surat; ?>" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Ubah Status Pembuatan</h4>
				</div>
				<form action="<?php echo base_url('ubah-usaha') ?>" method="post">
					<div class="modal-body">

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Keterangan</label>
									<input type="hidden" class="form-control" value="<?php echo $sku->id_surat; ?>" name="fid" readonly />
									<select class="form-control select2" style="width: 100%;" name="fstatus_pembuatan" required>
										<option value="">--- Pilih ---</option>
										<option value="Selesai">Selesai</option>
										<option value="Dalam Proses">Dalam Proses</option>
									</select>
								</div>
								<!-- /.form-group -->
							</div>
							<!-- /.col -->
						</div>

					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Simpan" id="btnsimpan" name="btnsimpan" />
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php } ?>
