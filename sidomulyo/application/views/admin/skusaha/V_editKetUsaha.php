<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Usaha</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php elseif ($this->session->flashdata('error')) : ?>
			<div class="alert alert-error" role="alert">
				<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php endif; ?>
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" id="nikpemohon" name="fnikpemohon" autofocus tabindex="1" />
						</div>
						
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" id="tglpemohon" name="ftglpemohon" tabindex="2" />
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="box-footer">
				<button class="btn btn-primary" type="button" id="btnCari" name="btnCariPemohon" tabindex="3">Cari Data</button>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Surat Keterangan Usaha</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('edit-usaha') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" class="form-control" name="fidsurat" value="<?php echo $skusaha->id_surat; ?>" readonly>
							<!-- <div class="form-group">
								<label>No. Surat</label>
								<input type="text" class="form-control" id="nosurat" name="fnosurat" tabindex="1" value="<?php //echo $skusaha->no_surat 
																															?>">
							</div> -->
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK</label>
								<input type="text" class="form-control" id="nik" name="fnik" tabindex="2" value="<?php echo $skusaha->nik_pemohon ?>">
								<span style="color: red">
									<?php echo form_error('fnik') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap</label>
								<input type="text" class="form-control" id="namalengkap" name="fnamalengkap" tabindex="3" value="<?php echo $skusaha->nama_pemohon ?>">
								<span style="color: red">
									<?php echo form_error('fnamalengkap') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" id="jkel" name="fjkel" tabindex="4">
									<?php if ($skusaha->jkel_pemohon == 'L') { ?>
										<option value="<?php echo $skusaha->jkel_pemohon ?>">Laki-laki</option>
									<?php } else { ?>
										<option value="<?php echo $skusaha->jkel_pemohon ?>">Perempuan</option>
									<?php } ?>

									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir</label>
										<input type="text" class="form-control" id="tempatlahir" name="ftempatlahir" tabindex="5" value="<?php echo $skusaha->tempat_lahir ?>">
										<span style="color: red">
											<?php echo form_error('ftempatlahir') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" class="form-control" id="tgllahir" name="ftgllahir" tabindex="6" value="<?php echo $skusaha->tgl_lahir ?>">
										<span style="color: red">
											<?php echo form_error('ftgllahir') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="form-group">
								<label>Kewarganegaraan</label>
								<input type="text" class="form-control" id="namanegara" name="fnamanegara" tabindex="7" value="<?php echo $skusaha->kewarganegaraan ?>">
								<span style="color: red">
									<?php echo form_error('fnamanegara') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Agama</label>
								<select class="form-control select2" style="width: 100%;" id="agama" name="fagama" tabindex="8">
									<option value="<?php echo $skusaha->agama ?>"><?php echo $skusaha->agama ?></option>
									<option value="Islam">Islam</option>
									<option value="Protestan">Protestan</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Khonghucu">Khonghucu</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fagama') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">

							<div class="form-group">
								<label>Status Perkawinan</label>
								<select class="form-control select2" style="width: 100%;" id="statuskawin" name="fstatuskawin" tabindex="9">
									<option value="<?php echo $skusaha->status_perkawinan ?>"><?php echo $skusaha->status_perkawinan ?></option>
									<option value="Kawin">Kawin</option>
									<option value="Belum Kawin">Belum Kawin</option>
									<option value="Cerai Hidup">Cerai Hidup</option>
									<option value="Cerai Mati">Cerai Mati</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fstatuskawin') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pendidikan Terakhir</label>
								<select class="form-control select2" style="width: 100%;" id="pendidikan" name="fpendidikan" tabindex="10">
									<option value="<?php echo $skusaha->pendidikan ?>"><?php echo $skusaha->pendidikan ?></option>
									<option value="SD">SD</option>
									<option value="SMP">SMP</option>
									<option value="SMA/SMK">SMA/SMK</option>
									<option value="D1">D1</option>
									<option value="D2">D2</option>
									<option value="D3">D3</option>
									<option value="S1/D4">S1/D4</option>
									<option value="S2">S2</option>
									<option value="S3">S3</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fpendidikan') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" id="pekerjaan" name="fpekerjaan" class="form-control" tabindex="11" value="<?php echo $skusaha->pekerjaan ?>">
								<span style="color: red">
									<?php echo form_error('fpekerjaan') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" id="alamat" name="falamat" class="form-control" tabindex="12" value="<?php echo $skusaha->alamat ?>">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Keterangan Usaha</label>
								<input type="text" class="form-control" id="ket" name="fket" tabindex="13" value="<?php echo $skusaha->keterangan ?>">
								<span style="color: red">
									<?php echo form_error('fket') ?>
								</span>
							</div>

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" id="btnSimpan" name="btnSimpan" value="Simpan" tabindex="14" />
					<a href="<?php echo site_url('layanan-usaha-admin') ?>" class="btn btn-default" tabindex="15">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
