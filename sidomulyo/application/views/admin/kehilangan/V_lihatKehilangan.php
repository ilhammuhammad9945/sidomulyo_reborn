<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Keterangan Kehilangan</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Keterangan Kehilangan</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($kehilangan['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<?php if ($kehilangan['jenis_kehilangan'] == 'KTP') { ?>
											<td>NIK Pemohon</td>
											<td>: <?= $kehilangan['nik_pemohon']; ?></td>
										<?php } else if ($kehilangan['jenis_kehilangan'] == 'KK') { ?>
											<td>No KK Pemohon</td>
											<td>: <?= $kehilangan['hilang_no']; ?></td>
										<?php } else if ($kehilangan['jenis_kehilangan'] == 'Akte') { ?>
											<td>No Akta Pemohon</td>
											<td>: <?= $kehilangan['hilang_no']; ?></td>
										<?php } else {
											echo "Something Wrong..!!!";
										} ?>
									</tr>
									<tr>
										<td>Nama Pemohon</td>
										<td>: <?= $kehilangan['nama_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($kehilangan['hilang_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Lahir </td>
										<td>: <?= $kehilangan['hilang_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($kehilangan['hilang_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $kehilangan['hilang_agama']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $kehilangan['hilang_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $kehilangan['hilang_kawin']; ?></td>
									</tr>
									<tr>
										<td>Alamat Lengkap</td>
										<td>: <?= $kehilangan['hilang_alamat']; ?></td>
									</tr>
									<tr>
										<td>Bertujuan</td>
										<td>: <?= $kehilangan['hilang_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Kebutuhan </td>
										<td>: <?= $kehilangan['hilang_kebutuhan']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<?php if ($kehilangan['jenis_kehilangan'] == 'KTP') { ?>
							<a href="<?php echo site_url('print-kehilangan-ktp/' . $kehilangan['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Suket Kehilangan</a>
						<?php } else if ($kehilangan['jenis_kehilangan'] == 'Akte') { ?>
							<a href="<?php echo site_url('print-kehilangan-akte/' . $kehilangan['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Suket Kehilangan</a>
						<?php } else { ?>
							<a href="<?php echo site_url('print-kehilangan-kk/' . $kehilangan['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Suket Kehilangan</a>
						<?php } ?>

						<a href="<?php echo site_url('layanan-kehilangan-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
