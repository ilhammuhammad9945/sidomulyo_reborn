<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Kehilangan</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Surat Pengantar Kehilangan</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_kehilangan/prosesEditSKHilang') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PEMOHON :</label>
							</div>
						</div>
					</div>
					<?php foreach ($kehilangan as $key => $value): ?>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="hidden" class="form-control" name="id_surat" id="id_surat" value="<?= $value->id_surat ?>" tabindex="4">
								<input type="hidden" class="form-control" name="jenis_kehilangan" id="jenis_kehilangan" value="<?= $value->jenis_kehilangan ?>" tabindex="4">
								<?php if($value->jenis_kehilangan == 'KTP'){ ?>
									<label>NIK Pemohon</label>
									<input type="text" class="form-control" name="nik_pemohon" id="nik_pemohon" value="<?= $value->nik_pemohon ?>" tabindex="4">
								<?php } else if($value->jenis_kehilangan == 'KK') { ?>
									<label>No KK Pemohon</label>
									<input type="text" class="form-control" name="hilang_no" id="no_kk_pemohon" value="<?= $value->hilang_no ?>" tabindex="4">
								<?php } else if($value->jenis_kehilangan == 'Akte') { ?>
									<label>No Akta Pemohon</label>
									<input type="text" class="form-control" name="hilang_no" id="no_akta_pemohon" value="<?= $value->hilang_no ?>" tabindex="4">
								<?php } else { echo "Something Wrong..!!!"; } ?>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Lengkap Pemohon</label>
								<input type="text" class="form-control" name="nama_pemohon" id="nama_pemohon" value="<?= $value->nama_pemohon ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" name="gender_pemohon" id="gender_pemohon" value="<?= $value->hilang_gender ?>" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L" <?php if($value->hilang_gender == "L"){ echo "selected"; }?>>Laki-laki</option>
									<option value="P" <?php if($value->hilang_gender == "P"){ echo "selected"; }?>>Perempuan</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tempat Lahir</label>
								<input type="text" class="form-control" name="tempat_lahir_pemohon" id="tempat_lahir_pemohon" value="<?= $value->hilang_tempat_lahir ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Lahir</label>
								<input type="date" class="form-control" name="tanggal_lahir_pemohon" id="tanggal_lahir_pemohon" value="<?= $value->hilang_tanggal_lahir ?>" tabindex="7">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Agama</label><br>
								<select class="form-control select2" style="width: 100%;" name="agama_pemohon" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Islam" <?php if($value->hilang_agama == "Islam"){ echo "selected"; }?>>Islam</option>
									<option value="Kristen" <?php if($value->hilang_agama == "Kristen"){ echo "selected"; }?>>Kristen</option>
									<option value="Hindu" <?php if($value->hilang_agama == "Hindu"){ echo "selected"; }?>>Hindu</option>
									<option value="Budha" <?php if($value->hilang_agama == "Budha"){ echo "selected"; }?>>Budha</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" class="form-control" name="pekerjaan_pemohon" id="pekerjaan_pemohon" value="<?= $value->hilang_pekerjaan ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Status Perkawinan</label><br>
								<select class="form-control select2" style="width: 100%;" name="status_nikah_pemohon" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Belum Kawin" <?php if($value->hilang_kawin == "Belum Kawin"){ echo "selected"; }?>>Belum Kawin</option>
									<option value="Kawin" <?php if($value->hilang_kawin == "Kawin"){ echo "selected"; }?>>Kawin</option>
									<option value="Cerai Hidup" <?php if($value->hilang_kawin == "Cerai Hidup"){ echo "selected"; }?>>Cerai Hidup</option>
									<option value="Cerai Mati" <?php if($value->hilang_kawin == "Cerai Mati"){ echo "selected"; }?>>Cerai Mati</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Alamat Lengkap</label>
						<input type="text" class="form-control" name="alamat_pemohon" id="alamat_pemohon" value="<?= $value->hilang_alamat ?>" tabindex="4">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Bertujuan</label>
								<input type="text" class="form-control" name="tujuan_pemohon" id="tujuan" value="<?= $value->hilang_tujuan ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Kebutuhan</label>
								<input type="text" class="form-control" name="kebutuhan_pemohon" id="kebutuhan" value="<?= $value->hilang_kebutuhan ?>" tabindex="4">
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnEditHilang" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-kehilangan-admin') ?>" class="btn btn-default" tabindex="16">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
