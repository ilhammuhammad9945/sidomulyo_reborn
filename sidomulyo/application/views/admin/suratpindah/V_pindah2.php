<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pindah Ada yang Ikut</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<!-- /.box -->

		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Progress Pengisian Form</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="progress">
							<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
								<span class="sr-only">60% Complete (success)</span>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (left) -->
		</div>

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Pindah Ada yang Ikut</h3>
			</div>
			<!-- /.box-header -->
			<form action="#" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Pemohon</label>
								<input type="text" name="fnamapemohon" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->
							<!-- checkbox -->
							<div class="form-group">
								<label>Jenis Kelamin</label><br>
								<label style="margin-right: 15px;">
									<input type="checkbox" class="minimal" name="fjkelpemohon[]" value="L">
									Laki-laki
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fjkelpemohon[]" value="P">
									Perempuan
								</label>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir</label>
										<input type="text" name="ftmptlahirpemohon" class="form-control" tabindex="5">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" name="ftgllahirpemohon" class="form-control" tabindex="6">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<!-- checkbox -->
							<div class="form-group">
								<label>Kewarganegaraan</label><br>
								<label style="margin-right: 43px;">
									<input type="checkbox" class="minimal" name="fwnipemohon[]" value="WNI">
									WNI
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fwnipemohon[]" value="WNA">
									Orang Asing
								</label>
							</div>
							<!-- checkbox -->
							<div class="form-group">
								<label>Agama</label><br>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fagamapemohon[]" value="Islam">
									Islam
								</label>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fagamapemohon[]" value="Kristen">
									Kristen
								</label>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fagamapemohon[]" value="Hindu">
									Hindu
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fagamapemohon[]" value="Budha">
									Budha
								</label>
							</div>
							<!-- checkbox -->
							<div class="form-group">
								<label>Status Perkawinan</label><br>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fkawinpemohon[]" value="Belum Kawin">
									Belum Kawin
								</label>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fkawinpemohon[]" value="Kawin">
									Kawin
								</label>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fkawinpemohon[]" value="Cerai Hidup">
									Cerai Hidup
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fkawinpemohon[]" value="Cerai Mati">
									Cerai Mati
								</label>
							</div>

							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" name="fpekerjaanpemohon" class="form-control" tabindex="7">
							</div>
							<!-- /.form-group -->
							<!-- checkbox -->
							<div class="form-group">
								<label>Pendidikan (Tamat)</label><br>
								<label style="margin-right: 54px;">
									<input type="checkbox" class="minimal" name="fpendidikanpemohon[]" value="SD">
									SD
								</label>
								<label style="margin-right: 51px;">
									<input type="checkbox" class="minimal" name="fpendidikanpemohon[]" value="SMP">
									SMP
								</label>
								<label style="margin-right: 46px;">
									<input type="checkbox" class="minimal" name="fpendidikanpemohon[]" value="SMA">
									SMA
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fpendidikanpemohon[]" value="Akademik">
									Akademik
								</label>
							</div>
							<div class="form-group">
								<label>Alamat Asal</label>
								<input type="text" name="falamatpemohon" class="form-control" tabindex="8">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>No. KTP</label>
								<input type="text" name="fktppemohon" class="form-control" tabindex="13">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Tujuan Ke-</label>
								<input type="text" name="ftujuanke" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" class="form-control" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" class="form-control" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="form-group">
								<label>Tanggal Berangkat</label>
								<input type="date" name="ftglberangkat" class="form-control" tabindex="13">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alasan Pindah</label>
								<input type="text" name="falasanpindah" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jumlah Pengikut (Orang)</label>
								<input type="number" name="fpengikut" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
					<div class="alert alert-info alert-dismissible" style="margin-top:30px">
						<h4><i class="icon fa fa-info"></i> Tambah Pengikut</h4>
						Masukkan data pengikut
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>NIK</label>
								<input type="text" name="fnikpengikut" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="fnamapengikut" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->
							<!-- checkbox -->
							<div class="form-group">
								<label>Jenis Kelamin</label><br>
								<label style="margin-right: 15px;">
									<input type="checkbox" class="minimal" name="fjkelpengikut[]" value="L">
									Laki-laki
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fjkelpengikut[]" value="P">
									Perempuan
								</label>
							</div>
							<div class="form-group">
								<label>Umur</label>
								<input type="number" name="fumurpengikut" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->
						</div>
						<div class="col-md-6">
							<!-- checkbox -->
							<div class="form-group">
								<label>Status Perkawinan</label><br>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fkawinpengikut[]" value="Belum Kawin">
									Belum Kawin
								</label>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fkawinpengikut[]" value="Kawin">
									Kawin
								</label>
								<label style="margin-right: 35px;">
									<input type="checkbox" class="minimal" name="fkawinpengikut[]" value="Cerai Hidup">
									Cerai Hidup
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fkawinpengikut[]" value="Cerai Mati">
									Cerai Mati
								</label>
							</div>
							<!-- checkbox -->
							<div class="form-group">
								<label>Pendidikan (Tamat)</label><br>
								<label style="margin-right: 30px;">
									<input type="checkbox" class="minimal" name="fpendidikanpengikut[]" value="SD">
									SD
								</label>
								<label style="margin-right: 30px;">
									<input type="checkbox" class="minimal" name="fpendidikanpengikut[]" value="SMP">
									SMP
								</label>
								<label style="margin-right: 30px;">
									<input type="checkbox" class="minimal" name="fpendidikanpengikut[]" value="SMA">
									SMA
								</label>
								<label>
									<input type="checkbox" class="minimal" name="fpendidikanpengikut[]" value="Akademik">
									Akademik
								</label>
							</div>
							<div class="form-group">
								<label>Keterangan</label>
								<input type="text" name="fketpengikut" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->
							<input class="btn btn-primary" type="button" name="addPengikut" onclick="tambahPengikut()" value="Tambah" style="margin-top: 25px" />
							<button class="btn btn-secondary" type="reset" style="margin-top: 25px">Reset</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Detail Pengikut</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="table-responsive">
										<table id="example1" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>No</th>
													<th>NIK</th>
													<th>Nama</th>
													<th>Jenis Kelamin</th>
													<th>Umur</th>
													<th>Status Perkawinan</th>
													<th>Pendidikan</th>
													<th>Keterangan</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>347593459034530</td>
													<td>Stoner</td>
													<td>Laki-laki</td>
													<td>35 Tahun</td>
													<td>Kawin</td>
													<td>Akademik</td>
													<td>-</td>
													<td>
														<a href="<?php echo site_url('edit-pengantar-akte/0') ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
														<a onclick="deleteConfirm('<?php echo site_url('hapus-pengantar-akte/0') ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<th>No</th>
													<th>Pemohon</th>
													<th>Nama Anak</th>
													<th>Status</th>
													<th>Aksi</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
					<a href="<?php echo site_url('tambah-f201-akte') ?>" class="btn btn-danger" tabindex="21">Tombol Sementara ke Form F-2.01</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
