<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Suket Berpergian</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Suket Berpergian</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($berpergian['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $berpergian['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Nama Pemohon</td>
										<td>: <?= $berpergian['nama_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($berpergian['pergi_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Lahir </td>
										<td>: <?= $berpergian['pergi_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($berpergian['pergi_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan</td>
										<td>: <?= $berpergian['pergi_kwn']; ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $berpergian['pergi_agama']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $berpergian['pergi_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>Pendidikan</td>
										<td>: <?= $berpergian['pergi_pendidikan']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $berpergian['pergi_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Asal</td>
										<td>: <?= $berpergian['pergi_alamat_asal']; ?></td>
									</tr>
									<tr>
										<td>Tujuan Ke</td>
										<td>: <?= $berpergian['pergi_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Desa</td>
										<td>: <?= $berpergian['pergi_tujuan_desa']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $berpergian['pergi_tujuan_kecamatan']; ?></td>
									</tr>
									<tr>
										<td>Kabupaten / Kota</td>
										<td>: <?= $berpergian['pergi_tujuan_kab']; ?></td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?= $berpergian['pergi_tujuan_provinsi']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Berangkat</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($berpergian['pergi_tanggal_berangkat']))) ?></td>
									</tr>
									<tr>
										<td>Alasan Berpergian</td>
										<td>: <?= $berpergian['pergi_alasan']; ?></td>
									</tr>
									<tr>
										<td>Jumlah Pengikut</td>
										<td>: <?= $berpergian['pergi_pengikut']; ?></td>
									</tr>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header">
											<h3 class="box-title">Detail Pengikut</h3>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table id="tbPengikut" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
													<thead>
														<tr>
															<th>NIK</th>
															<th>Nama</th>
															<th>Jenis Kelamin</th>
															<th>Umur</th>
															<th>Status Perkawinan</th>
															<th>Pendidikan</th>
															<th>Keterangan</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($pengikut as $key => $value) : ?>
															<tr>
																<td><?= $value->pengikut_nik ?></td>
																<td><?= $value->pengikut_nama ?></td>
																<td><?= $value->pengikut_gender ?></td>
																<td><?= $value->pengikut_umur ?></td>
																<td><?= $value->pengikut_status_kawin ?></td>
																<td><?= $value->pengikut_pendidikan ?></td>
																<td><?= $value->pengikut_keterangan ?></td>
															</tr>
														<?php endforeach; ?>
													</tbody>
													<tfoot>
														<tr>
															<th>NIK</th>
															<th>Nama</th>
															<th>Jenis Kelamin</th>
															<th>Umur</th>
															<th>Status Perkawinan</th>
															<th>Pendidikan</th>
															<th>Keterangan</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
									<!-- /.box -->
								</div>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-bepergian/' . $berpergian['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Surat Bepergian</a>
						<a href="<?php echo site_url('layanan-skck-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
