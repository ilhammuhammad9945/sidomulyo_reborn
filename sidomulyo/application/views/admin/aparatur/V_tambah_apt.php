<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Menu Aktif</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-tv"></i> Profil</a></li>
      <li><a href="#">Aparatur Desa</a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if ($this->session->flashdata('success')) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php endif; ?>
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Aparatur Desa</h3>
      </div>
      <!-- /.box-header -->
      <form action="<?php base_url('tambah-aparatur') ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="fnama" tabindex="1">
                <span style="color: red">
                  <?php echo form_error('fnama') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Jenis Kelamin</label>
                <select class="form-control select2" style="width: 100%;" name="fjkel" tabindex="2">
                  <option value="">--- Pilih ---</option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fjkel') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Nomor Telepon</label>
                <input type="text" class="form-control" name="ftelp" tabindex="3">
                <span style="color: red">
                  <?php echo form_error('ftelp') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" name="falamat" tabindex="4">
                <span style="color: red">
                  <?php echo form_error('falamat') ?>
                </span>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Jabatan</label>
                <select class="form-control select2" style="width: 100%;" name="fjabatan" tabindex="5">
                  <option value="">--- Pilih ---</option>
                  <?php foreach ($jabatan as $jbt) : ?>
                    <option value="<?php echo $jbt->id_jbt; ?>"><?php echo $jbt->nama_jbt; ?></option>
                  <?php endforeach; ?>
                </select>
                <span style="color: red">
                  <?php echo form_error('fjabatan') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Atasan</label>
                <select class="form-control select2" style="width: 100%;" name="fatasan" tabindex="6">
                  <option value="">--- Pilih ---</option>
                  <?php foreach ($atasan as $at) : ?>
                    <option value="<?php echo $at->id_apt; ?>"><?php echo $at->nama_apt . " - [ " . $at->nama_jbt . " ]"; ?></option>
                  <?php endforeach; ?>
                  <option value="-">Tidak Ada</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fatasan') ?>
                </span>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label>Punya Bawahan?</label>
                <select class="form-control select2" style="width: 100%;" name="fbawahan" tabindex="7">
                  <option value="">--- Pilih ---</option>
                  <option value="Atasan">Ya</option>
                  <option value="-">Tidak</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fbawahan') ?>
                </span>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label>Foto</label>
                <input type="file" name="ffoto" class="form-control" tabindex="8">
                <!-- <span style="color: red">
                    <?php //echo form_error('ffoto') 
                    ?>
                  </span> -->
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label>Tugas Pokok</label>
                <textarea id="editor1" name="fpokok" rows="10" cols="80" tabindex="9"></textarea>
                <span style="color: red">
                  <?php echo form_error('fpokok') ?>
                </span>
              </div>

              <div class="form-group">
                <label>Fungsi</label>
                <textarea id="editor2" name="ffungsi" rows="10" cols="80" tabindex="10"></textarea>
                <span style="color: red">
                  <?php echo form_error('ffungsi') ?>
                </span>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer">
          <input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="11" />
          <a href="<?php echo site_url('aparatur-admin') ?>" class="btn btn-default" tabindex="12">Kembali</a>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->