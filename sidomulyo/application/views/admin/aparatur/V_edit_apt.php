<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Menu Aktif</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-tv"></i> Profil</a></li>
      <li><a href="#">Aparatur Desa</a></li>
      <li class="active">Edit Data</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if ($this->session->flashdata('success')) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php endif; ?>
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Aparatur Desa</h3>
      </div>
      <!-- /.box-header -->
      <form action="<?php base_url('edit-aparatur') ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <input type="hidden" name="fid" value="<?php echo $aparatur->id_apt ?>" />
              <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control <?php echo form_error('fnama') ? 'is-invalid' : '' ?>" name="fnama" value="<?php echo $aparatur->nama_apt ?>" tabindex="1">
                <span style="color: red">
                  <?php echo form_error('fnama') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Jenis Kelamin</label>
                <select class="form-control select2 <?php echo form_error('fjkel') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fjkel" tabindex="2">
                  <option value="<?php echo $aparatur->jkel_apt ?>"><?php echo $aparatur->jkel_apt ?></option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fjkel') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Nomor Telepon</label>
                <input type="text" class="form-control" name="ftelp" value="<?php echo $aparatur->telp_apt ?>" tabindex="3">
                <span style="color: red">
                  <?php echo form_error('ftelp') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control <?php echo form_error('falamat') ? 'is-invalid' : '' ?>" name="falamat" value="<?php echo $aparatur->alamat_apt ?>" tabindex="4">
                <span style="color: red">
                  <?php echo form_error('falamat') ?>
                </span>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Jabatan</label>
                <select class="form-control select2 <?php echo form_error('fjabatan') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fjabatan" tabindex="5">
                  <option value="<?php echo $aparatur->jabatan_apt ?>"><?php echo $aparatur->nama_jbt ?></option>
                  <?php foreach ($jabatan as $jbt) : ?>
                    <option value="<?php echo $jbt->id_jbt; ?>"><?php echo $jbt->nama_jbt; ?></option>
                  <?php endforeach; ?>
                </select>
                <span style="color: red">
                  <?php echo form_error('fjabatan') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Atasan</label>
                <select class="form-control select2 <?php echo form_error('fatasan') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fatasan" tabindex="6">
                  <?php
                  $ats = $aparatur->id_atasan;
                  $idnya = $aparatur->id_apt;
                  if ($ats == "-") {
                    $atasannya = "Tidak Ada";
                    $jabatannya = "-";
                  } else {
                    //$atasannya = $aparatur->id_atasan;
                    $query = $this->db->query("SELECT ap.*, jb.* FROM tb_aparatur AS ap JOIN `tb_jabatan` AS jb ON ap.jabatan_apt = jb.id_jbt WHERE ap.id_apt='" . $ats . "'")->row();
                    $tampil_atasan = $this->db->query("SELECT ap.id_apt, ap.nama_apt, jb.nama_jbt FROM `tb_aparatur` AS ap JOIN tb_jabatan AS jb ON ap.jabatan_apt=jb.id_jbt WHERE ap.level='Atasan' AND ap.ket_apt='Aparatur Desa' AND ap.id_apt = '$ats'")->result();
                    $atasannya = $query->nama_apt;
                    $jabatannya = $query->nama_jbt;
                  }
                  ?>
                  <option value="<?php echo $aparatur->id_atasan ?>"><?php echo $atasannya . " - [" . $jabatannya . " ]"; ?></option>
                  <?php foreach ($data_atasan as $at) : ?>
                    <option value="<?php echo $at->id_apt; ?>"><?php echo $at->nama_apt . " - [ " . $at->nama_jbt . " ]"; ?></option>
                  <?php endforeach; ?>
                  <option value="-">Tidak Ada - [ - ]</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fatasan') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Punya Bawahan?</label>
                <select class="form-control select2" style="width: 100%;" name="fbawahan" tabindex="7">
                  <?php
                  $lv = $aparatur->level;
                  if ($lv == "-") {
                    $levelnya = "Tidak";
                  } else {
                    $levelnya = "Ya";
                  }
                  ?>
                  <option value="<?php echo $aparatur->level ?>" selected><?php echo $levelnya ?></option>
                  <option value="Atasan">Ya</option>
                  <option value="-">Tidak</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fbawahan') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Upload Ulang Foto / </label>
                <i class="fa fa-eye"></i>
                <a href="<?php echo base_url('assets/upload/aparatur/' . $aparatur->foto_apt) ?>" target="_blank">Lihat Foto</a>
                <input type="file" name="ffoto" class="form-control" tabindex="8">
                <input type="hidden" name="ffotolama" value="<?php echo $aparatur->foto_apt ?>" />
                <!-- <span style="color: red">
                    <?php //echo form_error('ffoto') 
                    ?>
                  </span> -->
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label>Tugas Pokok</label>
                <textarea class="form-control <?php echo form_error('fpokok') ? 'is-invalid' : '' ?>" id="editor1" name="fpokok" rows="10" cols="80" tabindex="9">
                       <?php echo $aparatur->tugas_pokok ?>
                    </textarea>
                <span style="color: red">
                  <?php echo form_error('fpokok') ?>
                </span>
              </div>

              <div class="form-group">
                <label>Fungsi</label>
                <textarea class="form-control <?php echo form_error('ffungsi') ? 'is-invalid' : '' ?>" id="editor2" name="ffungsi" rows="10" cols="80" tabindex="10">
                      <?php echo $aparatur->fungsi ?>
                    </textarea>
                <span style="color: red">
                  <?php echo form_error('ffungsi') ?>
                </span>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer">
          <input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="11" />
          <a href="<?php echo site_url('aparatur-admin') ?>" class="btn btn-default" tabindex="12">Kembali</a>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->