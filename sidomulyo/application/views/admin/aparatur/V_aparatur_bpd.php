<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-tv"></i> Profil</a></li>
			<li class="active">Data Aparatur BPD</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success" role="alert">
						<?php echo $this->session->flashdata('success'); ?>
					</div>
				<?php endif; ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('tambah-bpd') ?>" class="btn btn-block btn-primary">Tambah Data</a></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Foto</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($aparatur_bpd as $bpd) : ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo $bpd->nama_apt; ?></td>
											<td><?php echo $bpd->nama_jbt; ?></td>
											<td>
												<img src="<?php echo base_url('assets/upload/aparatur/' . $bpd->foto_apt) ?>" width="64" height="64" />
											</td>
											<td>
												<a href="<?php echo site_url('edit-bpd/' . $bpd->id_apt) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-bpd/' . $bpd->id_apt) ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Foto</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>
