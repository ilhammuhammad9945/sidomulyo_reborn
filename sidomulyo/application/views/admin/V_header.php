<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?> | Admin Website Desa Sidomulyo</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/bower_components/Ionicons/css/ionicons.min.css">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/plugins/iCheck/all.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/bower_components/select2/dist/css/select2.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/dist/css/skins/_all-skins.min.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->

<body class="hold-transition skin-blue fixed sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo base_url('beranda-admin') ?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>SDM</b></span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Web</b> Sidomulyo</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo base_url(); ?>assets/template_admin/dist/img/admin.png" class="user-image" alt="User Image">
								<span class="hidden-xs"><?php echo $this->session->userdata("usr_name"); ?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="<?php echo base_url(); ?>assets/template_admin/dist/img/admin.png" class="img-circle" alt="User Image">

									<p>
										<?php echo $this->session->userdata("usr_name"); ?> -
										<?php if ($this->session->userdata("usr_level") == 1) {
											echo "Admin Web Profil";
										} else if ($this->session->userdata("usr_level") == 2) {
											echo "Admin Pendataan";
										} else {
											echo "Editor Berita";
										} ?>
									</p>
								</li>

								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?php echo base_url() . 'admin/C_Profil' ?>" class="btn btn-default btn-flat">Profil</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo base_url() . 'logout-admin' ?>" class="btn btn-default btn-flat">Keluar</a>
									</div>
								</li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>
		</header>

		<!-- ===============================================-->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?php echo base_url(); ?>assets/template_admin/dist/img/admin.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p><?php echo $this->session->userdata("usr_name"); ?></p>
						<a href="#">
							<?php if ($this->session->userdata("usr_level") == 1) {
								echo "Admin Web Profil";
							} else if ($this->session->userdata("usr_level") == 2) {
								echo "Admin Pendataan";
							} else {
								echo "Editor Berita";
							} ?>
						</a>
					</div>
				</div>
				<!-- search form -->

				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">PILIHAN MENU</li>
					<?php if ($menu_aktif == 1) { ?>
						<li class="active">
						<?php } else { ?>
						<li>
						<?php } ?>
						<a href="<?php echo base_url() . 'beranda-admin' ?>"><i class="fa fa-dashboard"></i> <span>Beranda</span></a>
						</li>
						<?php if ($this->session->userdata("usr_level") == 3) { ?>
							<li <?php if ($menu_aktif == 2) {
									echo 'class="active"';
								} ?>><a href="<?= site_url('admin/C_berita') ?>"><i class="fa fa-newspaper-o"></i> <span>Berita</span></a></li>
						<?php } ?>

						<li <?php if ($menu_aktif == 99) {
								echo 'class="active"';
							} ?>><a href="<?= site_url('admin/C_info') ?>"><i class="fa fa-bullhorn"></i> <span>Info</span></a></li>

						<?php if ($menu_aktif == 3) { ?>
							<li class="active">
							<?php } else { ?>
							<li>
							<?php } ?>
							<a href="<?php echo base_url() . 'konten-admin' ?>"><i class="fa fa-th-large"></i> <span>Konten</span></a>
							</li>

							<?php if ($menu_aktif == 4) { ?>
								<li class="treeview active">
								<?php } else { ?>
								<li class="treeview">
								<?php } ?>
								<a href="#">
									<i class="fa fa-check-square-o"></i> <span>Data Master</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									<?php if ($submenu == 41) { ?>
										<li class="active">
										<?php } else { ?>
										<li>
										<?php } ?>
										<a href="<?php echo base_url() . 'jenis-konten-admin' ?>"><i class="fa fa-circle-o"></i> Jenis Konten</a>
										</li>
										<?php if ($submenu == 42) { ?>
											<li class="active">
											<?php } else { ?>
											<li>
											<?php } ?>
											<a href="<?php echo base_url() . 'jabatan-admin' ?>"><i class="fa fa-circle-o"></i> Jabatan</a>
											</li>
											<?php if ($submenu == 43) { ?>
												<li class="active">
												<?php } else { ?>
												<li>
												<?php } ?>
												<a href="<?php echo base_url() . 'organisasi-admin' ?>"><i class="fa fa-circle-o"></i> Organisasi</a>
												</li>
												<?php if ($submenu == 44) { ?>
													<li class="active">
													<?php } else { ?>
													<li>
													<?php } ?>
													<a href="<?php echo base_url() . 'setting-surat-admin' ?>"><i class="fa fa-circle-o"></i> Setting Surat</a>
													</li>
													<?php if ($submenu == 45) { ?>
														<li class="active">
														<?php } else { ?>
														<li>
														<?php } ?>
														<a href="<?php echo base_url() . 'user-admin' ?>"><i class="fa fa-circle-o"></i> User</a>
														</li>
								</ul>
								</li>

								<?php if ($menu_aktif == 5) { ?>
									<li class="treeview active">
									<?php } else { ?>
									<li class="treeview">
									<?php } ?>
									<a href="#">
										<i class="fa fa-tv"></i> <span>Profil</span>
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
									<ul class="treeview-menu">
										<?php if ($submenu == 51) { ?>
											<li class="active">
											<?php } else { ?>
											<li>
											<?php } ?>
											<a href="<?php echo base_url() . 'aparatur-admin' ?>"><i class="fa fa-circle-o"></i> Aparatur Pemerintah Desa</a>
											</li>
											<?php if ($submenu == 52) { ?>
												<li class="active">
												<?php } else { ?>
												<li>
												<?php } ?>
												<a href="<?php echo base_url() . 'bpd-admin' ?>"><i class="fa fa-circle-o"></i> Aparatur BPD</a>
												</li>
									</ul>
									</li>

									<?php if ($menu_aktif == 6) { ?>
										<li class="treeview active">
										<?php } else { ?>
										<li class="treeview">
										<?php } ?>
										<a href="#">
											<i class="fa fa-university"></i> <span>Lemb. Kemasyarakatan</span>
											<span class="pull-right-container">
												<i class="fa fa-angle-left pull-right"></i>
											</span>
										</a>
										<ul class="treeview-menu">
											<?php if ($submenu == 61) { ?>
												<li class="active">
												<?php } else { ?>
												<li>
												<?php } ?>
												<a href="<?php echo base_url() . 'pkk-admin' ?>"><i class="fa fa-circle-o"></i> PKK</a>
												</li>
												<?php if ($submenu == 62) { ?>
													<li class="active">
													<?php } else { ?>
													<li>
													<?php } ?>
													<a href="<?php echo base_url() . 'lpmd-admin' ?>"><i class="fa fa-circle-o"></i> LPMD</a>
													</li>
													<?php if ($submenu == 63) { ?>
														<li class="active">
														<?php } else { ?>
														<li>
														<?php } ?>
														<a href="<?php echo base_url() . 'karangtaruna-admin' ?>"><i class="fa fa-circle-o"></i> Karang Taruna</a>
														</li>
														<?php if ($submenu == 64) { ?>
															<li class="active">
															<?php } else { ?>
															<li>
															<?php } ?>
															<a href="<?php echo base_url() . 'posyandu-admin' ?>"><i class="fa fa-circle-o"></i> Posyandu</a>
															</li>
															<?php if ($submenu == 65) { ?>
																<li class="active">
																<?php } else { ?>
																<li>
																<?php } ?>
																<a href="<?php echo base_url() . 'rt-admin' ?>"><i class="fa fa-circle-o"></i> Rukun Tetangga</a>
																</li>
																<?php if ($submenu == 66) { ?>
																	<li class="active">
																	<?php } else { ?>
																	<li>
																	<?php } ?>
																	<a href="<?php echo base_url() . 'rw-admin' ?>"><i class="fa fa-circle-o"></i> Rukun Warga</a>
																	</li>
																	<?php if ($submenu == 67) { ?>
																		<li class="active">
																		<?php } else { ?>
																		<li>
																		<?php } ?>
																		<a href="<?php echo base_url() . 'karangwerda-admin' ?>"><i class="fa fa-circle-o"></i> Karang Werda</a>
																		</li>
																		<?php if ($submenu == 68) { ?>
																			<li class="active">
																			<?php } else { ?>
																			<li>
																			<?php } ?>
																			<a href="<?php echo base_url() . 'rds-admin' ?>"><i class="fa fa-circle-o"></i> Rumah Desa Sehat</a>
																			</li>
										</ul>
										</li>

										<?php if ($menu_aktif == 7) { ?>
											<li class="treeview active">
											<?php } else { ?>
											<li class="treeview">
											<?php } ?>
											<a href="#">
												<i class="fa fa-industry"></i> <span>Lembaga Desa</span>
												<span class="pull-right-container">
													<i class="fa fa-angle-left pull-right"></i>
												</span>
											</a>
											<ul class="treeview-menu">
												<?php if ($submenu == 71) { ?>
													<li class="active">
													<?php } else { ?>
													<li>
													<?php } ?>
													<a href="<?php echo base_url() . 'muslimat-admin' ?>"><i class="fa fa-circle-o"></i> Muslimat</a>
													</li>
													<?php if ($submenu == 72) { ?>
														<li class="active">
														<?php } else { ?>
														<li>
														<?php } ?>
														<a href="<?php echo base_url() . 'aisyiyah-admin' ?>"><i class="fa fa-circle-o"></i> Aisyiyah</a>
														</li>
														<?php if ($submenu == 73) { ?>
															<li class="active">
															<?php } else { ?>
															<li>
															<?php } ?>
															<a href="<?php echo base_url() . 'ssb-admin' ?>"><i class="fa fa-circle-o"></i> Sanggar Seni Budaya</a>
															</li>
															<?php if ($submenu == 74) { ?>
																<li class="active">
																<?php } else { ?>
																<li>
																<?php } ?>
																<a href="<?php echo base_url() . 'lsm-admin' ?>"><i class="fa fa-circle-o"></i> LSM</a>
																</li>
											</ul>
											</li>

											<?php if ($menu_aktif == 8) { ?>
												<li class="treeview active">
												<?php } else { ?>
												<li class="treeview">
												<?php } ?>
												<a href="#">
													<i class="fa fa-graduation-cap"></i> <span>Kader Desa</span>
													<span class="pull-right-container">
														<i class="fa fa-angle-left pull-right"></i>
													</span>
												</a>
												<ul class="treeview-menu">
													<?php if ($submenu == 81) { ?>
														<li class="active">
														<?php } else { ?>
														<li>
														<?php } ?>
														<a href="<?php echo base_url() . 'kpmd-admin' ?>"><i class="fa fa-circle-o"></i> KPMD</a>
														</li>
														<?php if ($submenu == 82) { ?>
															<li class="active">
															<?php } else { ?>
															<li>
															<?php } ?>
															<a href="<?php echo base_url() . 'kpm-admin' ?>"><i class="fa fa-circle-o"></i> KPM</a>
															</li>
															<?php if ($submenu == 83) { ?>
																<li class="active">
																<?php } else { ?>
																<li>
																<?php } ?>
																<a href="<?php echo base_url() . 'kaderteknik-admin' ?>"><i class="fa fa-circle-o"></i> Kader Teknik</a>
																</li>
																<?php if ($submenu == 84) { ?>
																	<li class="active">
																	<?php } else { ?>
																	<li>
																	<?php } ?>
																	<a href="<?php echo base_url() . 'ppkbd-admin' ?>"><i class="fa fa-circle-o"></i> Kader dan Sub PPKBD</a>
																	</li>
																	<?php if ($submenu == 85) { ?>
																		<li class="active">
																		<?php } else { ?>
																		<li>
																		<?php } ?>
																		<a href="<?php echo base_url() . 'kadertb-admin' ?>"><i class="fa fa-circle-o"></i> Kader TB</a>
																		</li>
												</ul>
												</li>

												<?php if ($menu_aktif == 9) { ?>
													<li class="treeview active">
													<?php } else { ?>
													<li class="treeview">
													<?php } ?>
													<a href="#">
														<i class="fa fa-database"></i> <span>Data Desa</span>
														<span class="pull-right-container">
															<i class="fa fa-angle-left pull-right"></i>
														</span>
													</a>
													<ul class="treeview-menu">
														<?php if ($submenu == 91) { ?>
															<li class="active">
															<?php } else { ?>
															<li>
															<?php } ?>
															<a href="<?php echo base_url() . 'bumdes-admin' ?>"><i class="fa fa-circle-o"></i> BUM Desa</a>
															</li>
															<?php if ($submenu == 92) { ?>
																<li class="active">
																<?php } else { ?>
																<li>
																<?php } ?>
																<a href="<?php echo base_url() . 'umkm-admin' ?>"><i class="fa fa-circle-o"></i> UMKM </a>
																</li>
																<?php if ($submenu == 93) { ?>
																	<li class="active">
																	<?php } else { ?>
																	<li>
																	<?php } ?>
																	<a href="<?php echo base_url() . 'produkunggulan-admin' ?>"><i class="fa fa-circle-o"></i> Produk Unggulan</a>
																	</li>
																	<?php if ($submenu == 94) { ?>
																		<li class="active">
																		<?php } else { ?>
																		<li>
																		<?php } ?>
																		<a href="<?= site_url('admin/C_datadesa/pendidikan'); ?>"><i class="fa fa-circle-o"></i> Pendidikan</a>
																		</li>
																		<?php if ($submenu == 95) { ?>
																			<li class="active">
																			<?php } else { ?>
																			<li>
																			<?php } ?>
																			<a href="<?php echo base_url() . 'kesehatan-admin' ?>"><i class="fa fa-circle-o"></i> Kesehatan</a>
																			</li>
																			<?php if ($submenu == 96) { ?>
																				<li class="active">
																				<?php } else { ?>
																				<li>
																				<?php } ?>
																				<a href="<?php echo base_url() . 'statistik-admin' ?>"><i class="fa fa-circle-o"></i> Statistik</a>
																				</li>
													</ul>
													</li>

													<?php if ($menu_aktif == 10) { ?>
														<li class="treeview active">
														<?php } else { ?>
														<li class="treeview">
														<?php } ?>
														<a href="#">
															<i class="fa fa-thumbs-o-up"></i> <span>Layanan</span>
															<span class="pull-right-container">
																<i class="fa fa-angle-left pull-right"></i>
															</span>
														</a>
														<ul class="treeview-menu">
															<?php if ($submenu == 101) { ?>
																<li class="treeview active">
																<?php } else { ?>
																<li class="treeview">
																<?php } ?>
																<a href="#"><i class="fa fa-circle-o"></i> Permohonan Surat
																	<span class="pull-right-container">
																		<i class="fa fa-angle-left pull-right"></i>
																	</span>
																</a>
																<ul class="treeview-menu">
																	<?php if ($submenu2 == 1011) { ?>
																		<li class="active">
																		<?php } else { ?>
																		<li>
																		<?php } ?>
																		<a href="<?php echo base_url() . 'layanan-akte-admin' ?>"><i class="fa fa-circle-o"></i> Akte Kelahiran</a>
																		</li>
																		<?php if ($submenu2 == 1012) { ?>
																			<li class="active">
																			<?php } else { ?>
																			<li>
																			<?php } ?>
																			<a href="<?php echo base_url() . 'layanan-sktm-admin' ?>"><i class="fa fa-circle-o"></i> SKTM</a>
																			</li>
																			<?php if ($submenu2 == 1013) { ?>
																				<li class="active">
																				<?php } else { ?>
																				<li>
																				<?php } ?>
																				<a href="<?php echo base_url() . 'layanan-skck-admin' ?>"><i class="fa fa-circle-o"></i> SKCK</a>
																				</li>
																				<?php if ($submenu2 == 1014) { ?>
																					<li class="active">
																					<?php } else { ?>
																					<li>
																					<?php } ?>
																					<a href="<?php echo base_url() . 'layanan-surat-kematian-admin' ?>"><i class="fa fa-circle-o"></i> Ket. Kematian</a>
																					</li>
																					<?php if ($submenu2 == 1015) { ?>
																						<li class="active">
																						<?php } else { ?>
																						<li>
																						<?php } ?>
																						<a href="<?php echo base_url() . 'layanan-identitas-admin' ?>"><i class="fa fa-circle-o"></i> Ket. Beda Identitas</a>
																						</li>
																						<?php if ($submenu2 == 1016) { ?>
																							<li class="active">
																							<?php } else { ?>
																							<li>
																							<?php } ?>
																							<a href="<?php echo base_url() . 'layanan-domisili-admin' ?>"><i class="fa fa-circle-o"></i> Ket. Domisili</a>
																							</li>
																							<?php if ($submenu2 == 1017) { ?>
																								<li class="active">
																								<?php } else { ?>
																								<li>
																								<?php } ?>
																								<a href="<?php echo base_url() . 'layanan-usaha-admin' ?>"><i class="fa fa-circle-o"></i> Ket. Usaha</a>
																								</li>
																								<?php if ($submenu2 == 1018) { ?>
																									<li class="active">
																									<?php } else { ?>
																									<li>
																									<?php } ?>
																									<a href="<?php echo base_url('layanan-pelaporan-kematian-admin') ?>"><i class="fa fa-circle-o"></i> Pelaporan Kematian</a>
																									</li>
																									<?php if ($submenu2 == 1019) { ?>
																										<li class="active">
																										<?php } else { ?>
																										<li>
																										<?php } ?>
																										<a href="<?php echo base_url() . 'layanan-suket-pindah-admin' ?>"><i class="fa fa-circle-o"></i> Pindah</a>
																										</li>
																										<?php if ($submenu2 == 10110) { ?>
																											<li class="active">
																											<?php } else { ?>
																											<li>
																											<?php } ?>
																											<a href="<?php echo base_url('layanan-belum-nikah-admin') ?>"><i class="fa fa-circle-o"></i> Ket. Belum Menikah</a>
																											</li>
																											<?php if ($submenu2 == 10111) { ?>
																												<li class="active">
																												<?php } else { ?>
																												<li>
																												<?php } ?>
																												<a href="<?php echo base_url() . 'layanan-rekam-ktp-admin' ?>"><i class="fa fa-circle-o"></i> Ket. Telah Rekam KTP</a>
																												</li>
																												<?php if ($submenu2 == 10112) { ?>
																													<li class="active">
																													<?php } else { ?>
																													<li>
																													<?php } ?>
																													<a href="<?php echo base_url('layanan-bsm-admin') ?>"><i class="fa fa-circle-o"></i> BSM</a>
																													</li>
																													<?php if ($submenu2 == 10113) { ?>
																														<li class="active">
																														<?php } else { ?>
																														<li>
																														<?php } ?>
																														<a href="<?php echo base_url() . 'layanan-kehilangan-admin' ?>"><i class="fa fa-circle-o"></i> Kehilangan</a>
																														</li>
																														<?php if ($submenu2 == 10114) { ?>
																															<li class="active">
																															<?php } else { ?>
																															<li>
																															<?php } ?>
																															<a href="<?php echo base_url() . 'layanan-SKberpergian-admin' ?>"><i class="fa fa-circle-o"></i> Ket. Bepergian</a>
																															</li>
																															<?php if ($submenu2 == 10115) { ?>
																																<li class="active">
																																<?php } else { ?>
																																<li>
																																<?php } ?>
																																<a href="<?php echo base_url('layanan-kenal-lahir-admin') ?>"><i class="fa fa-circle-o"></i> Ket. Kenal Lahir</a>
																																</li>
																</ul>
																</li>
														</ul>
														</li>

														<?php if ($menu_aktif == 11) { ?>
															<li class="treeview active">
															<?php } else { ?>
															<li class="treeview">
															<?php } ?>
															<a href="#">
																<i class="fa fa-balance-scale"></i> <span>Produk Hukum</span>
																<span class="pull-right-container">
																	<i class="fa fa-angle-left pull-right"></i>
																</span>
															</a>
															<ul class="treeview-menu">
																<?php if ($submenu == 111) { ?>
																	<li class="active">
																	<?php } else { ?>
																	<li>
																	<?php } ?>
																	<a href="<?php echo base_url() . 'admin/C_produkhukum/perdes' ?>"><i class="fa fa-circle-o"></i> Perdes</a>
																	</li>
																	<?php if ($submenu == 112) { ?>
																		<li class="active">
																		<?php } else { ?>
																		<li>
																		<?php } ?>
																		<a href="<?php echo base_url() . 'admin/C_produkhukum/permakades' ?>"><i class="fa fa-circle-o"></i> Permakades</a>
																		</li>
																		<?php if ($submenu == 113) { ?>
																			<li class="active">
																			<?php } else { ?>
																			<li>
																			<?php } ?>
																			<a href="<?php echo base_url() . 'admin/C_produkhukum/perkades' ?>"><i class="fa fa-circle-o"></i> Perkades</a>
																			</li>
																			<?php if ($submenu == 114) { ?>
																				<li class="active">
																				<?php } else { ?>
																				<li>
																				<?php } ?>
																				<a href="<?php echo base_url() . 'admin/C_produkhukum/skkades' ?>"><i class="fa fa-circle-o"></i> SK Kades</a>
																				</li>
																				<?php if ($submenu == 115) { ?>
																					<li class="active">
																					<?php } else { ?>
																					<li>
																					<?php } ?>
																					<a href="<?php echo base_url() . 'admin/C_produkhukum/sekades' ?>"><i class="fa fa-circle-o"></i> SE Kades</a>
																					</li>
															</ul>
															</li>

															<?php if ($menu_aktif == 12) { ?>
																<li class="treeview active">
																<?php } else { ?>
																<li class="treeview">
																<?php } ?>
																<a href="#">
																	<i class="fa fa-file-text-o"></i> <span>Laporan</span>
																	<span class="pull-right-container">
																		<i class="fa fa-angle-left pull-right"></i>
																	</span>
																</a>
																<ul class="treeview-menu">
																	<?php if ($submenu == 111) { ?>
																		<li class="active">
																		<?php } else { ?>
																		<li>
																		<?php } ?>
																		<a href="<?php echo base_url() . 'laporan-surat-keluar' ?>"><i class="fa fa-circle-o"></i> Surat Keluar</a>
																		</li>
																</ul>
																</li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>
