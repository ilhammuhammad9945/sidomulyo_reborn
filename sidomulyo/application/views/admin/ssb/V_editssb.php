<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-university"></i> Lembaga Desa</a></li>
			<li><a href="#">Sanggar Seni Budaya</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Pengurus Sanggar Seni Budaya</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('edit-ssb') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="fid" value="<?php echo $ssb->id_lm ?>" />
							<div class="form-group">
								<label>Nama Lengkap</label>
								<input type="text" class="form-control <?php echo form_error('fnama') ? 'is-invalid' : '' ?>" name="fnama" value="<?php echo $ssb->nama_lm ?>" tabindex="1">
								<span style="color: red">
									<?php echo form_error('fnama') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2 <?php echo form_error('fjkel') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fjkel" tabindex="2">
									<option value="<?php echo $ssb->jkel_lm ?>"><?php echo $ssb->jkel_lm ?></option>
									<option value="Laki-laki">Laki-laki</option>
									<option value="Perempuan">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nomor Telepon</label>
								<input type="text" class="form-control" name="ftelp" value="<?php echo $ssb->no_telp ?>" tabindex="3">
								<span style="color: red">
									<?php echo form_error('ftelp') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" class="form-control <?php echo form_error('falamat') ? 'is-invalid' : '' ?>" name="falamat" value="<?php echo $ssb->alamat_lm ?>" tabindex="4">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Jabatan</label>
								<select class="form-control select2 <?php echo form_error('fjabatan') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fjabatan" tabindex="5">
									<option value="<?php echo $ssb->jabatan_lm ?>"><?php echo $ssb->nama_jbt ?></option>
									<?php foreach ($jabatan as $jbt) : ?>
										<option value="<?php echo $jbt->id_jbt; ?>"><?php echo $jbt->nama_jbt; ?></option>
									<?php endforeach; ?>
								</select>
								<span style="color: red">
									<?php echo form_error('fjabatan') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Asal Sanggar Seni</label>
								<select class="form-control select2 <?php echo form_error('fasal') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fasal" tabindex="6">
									<?php
									$ket =  $ssb->asal_organisasi;
									if ($ket == "-") {
										$tampilkan = "Tidak Ada";
									} else {
										$tampilkan = $ket;
									} ?>
									<option value="<?php echo $ssb->asal_organisasi ?>"><?php echo $tampilkan ?></option>
									<?php foreach ($asal as $as) : ?>
										<option value="<?php echo $as->nama_org; ?>"><?php echo $as->nama_org; ?></option>
									<?php endforeach; ?>
									<option value="-">Tidak Ada</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fasal') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Upload Ulang Foto / </label>
								<i class="fa fa-eye"></i>
								<a href="<?php echo base_url('assets/upload/ssb/' . $ssb->foto_lm) ?>" target="_blank">Lihat Foto</a>
								<input type="file" name="ffoto" class="form-control" tabindex="7">
								<input type="hidden" name="ffotolama" value="<?php echo $ssb->foto_lm ?>" />
								<!-- <span style="color: red">
                    <?php //echo form_error('ffoto') 
										?>
                  </span> -->
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="8" />
					<a href="<?php echo site_url('ssb-admin') ?>" class="btn btn-default" tabindex="9">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
