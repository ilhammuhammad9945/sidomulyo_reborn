<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Suket Kematian</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Surat Kematian</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('proses-edit-kematian') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PEMOHON :</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>NIK</label>
								<input type="hidden" name="id_surat" value="<?= $surat['id_surat'] ?>" readonly required>
								<input type="text" class="form-control" name="kematian_nik" id="kematian_nik" value="<?= $surat['nik_pemohon'] ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Lengkap</label>
								<input type="text" class="form-control" name="kematian_nama_lengkap" id="kematian_nama_lengkap" value="<?= $surat['nama_pemohon'] ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Umur</label>
								<input type="text" class="form-control" name="kematian_umur" id="kematian_umur" value="<?= $surat['umur_meninggal'] ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" name="kematian_gender" id="kematian_gender" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L" <?php if ($surat['gender_meninggal'] == 'L') {
															echo "selected";
														} ?>>Laki-laki</option>
									<option value="P" <?php if ($surat['gender_meninggal'] == 'P') {
															echo "selected";
														} ?>>Perempuan</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Meninggal</label>
								<input type="date" class="form-control" name="kematian_tanggal" id="kematian_tanggal" value="<?= $surat['tanggal_meninggal'] ?>" tabindex="7">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tempat</label>
								<input type="text" class="form-control" name="kematian_tempat" id="kematian_tempat" value="<?= $surat['tempat_meninggal'] ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" class="form-control" name="kematian_alamat" id="kematian_alamat" value="<?= $surat['alamat_meninggal'] ?>" tabindex="7">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Penyebab</label>
								<input type="text" class="form-control" name="kematian_penyebab" id="kematian_penyebab" value="<?= $surat['penyebab_meninggal'] ?>" tabindex="4">
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanEditKematian" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-surat-kematian-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
