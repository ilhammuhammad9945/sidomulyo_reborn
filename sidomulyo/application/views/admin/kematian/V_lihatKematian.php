<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pengantar Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat SKCK</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Pemohon :</h3>
							<h4> <?= $kematian['nama_pemohon']; ?>
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : <?= format_indo(date("Y-m-d H:i", strtotime($kematian['created_at']))) ?> WIB</span>
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $kematian['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($kematian['gender_meninggal'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Umur </td>
										<td>: <?= $kematian['umur_meninggal'] . ' Tahun'; ?></td>
									</tr>
									<tr>
										<td>Tanggal Meninggal</td>
										<td>: <?= format_indo(date($kematian['tanggal_meninggal'])); ?></td>
									</tr>
									<tr>
										<td>Tempat Meninggal</td>
										<td>: <?= $kematian['tempat_meninggal']; ?></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>: <?= $kematian['alamat_meninggal']; ?></td>
									</tr>
									<tr>
										<td>Penyebab Meninggal</td>
										<td>: <?= $kematian['penyebab_meninggal']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-kematian/' . $kematian['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Suket. Kematian</a>
						<a href="<?php echo site_url('layanan-surat-kematian-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
