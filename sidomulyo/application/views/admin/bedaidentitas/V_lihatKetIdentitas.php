<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Beda Identitas</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Surat Keterangan Beda Identitas</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>ID Pengajuan</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->id_pengajuan ?></td>
									</tr>
									<tr>
										<td>Tgl Pengajuan</td>
										<td>:</td>
										<td><?php echo format_indo(date('Y-m-d'), strtotime($bedaidentitas->created_at)) ?></td>
									</tr>
									<tr>
										<td>Nomor Surat</td>
										<td>:</td>
										<td><?php echo '470/......./35.09.07.2006/' . date("Y")  ?></td>
									</tr>
									<tr>
										<td>No. KK [ Data 1 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->no_kk_lama ?></td>
									</tr>
									<tr>
										<td>NIK [ Data 1 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->nik_pemohon ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap [ Data 1 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->nama_pemohon ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin [ Data 1 ]</td>
										<td>:</td>
										<?php if ($bedaidentitas->jkel_lama == 'L') { ?>
											<td>Laki-laki</td>
										<?php } else { ?>
											<td>Perempuan</td>
										<?php } ?>

									</tr>
									<tr>
										<td>Tempat/Tgl Lahir [ Data 1 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->tempat_lahir_lama . ' , ' . format_indo(date('Y-m-d'), strtotime($bedaidentitas->tgl_lahir_lama)) ?></td>
									</tr>
									<tr>
										<td>Agama [ Data 1 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->agama_lama ?></td>
									</tr>
									<tr>
										<td>Alamat [ Data 1 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->alamat_lama ?></td>
									</tr>


									<tr>
										<td>No. KK [ Data 2 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->no_kk_baru ?></td>
									</tr>
									<tr>
										<td>NIK [ Data 2 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->nik_baru ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap [ Data 2 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->nama_baru ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin [ Data 2 ]</td>
										<td>:</td>
										<?php if ($bedaidentitas->jkel_baru == 'L') { ?>
											<td>Laki-laki</td>
										<?php } else { ?>
											<td>Perempuan</td>
										<?php } ?>

									</tr>
									<tr>
										<td>Tempat/Tgl Lahir [ Data 2 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->tempat_lahir_baru . ' , ' . format_indo(date('Y-m-d'), strtotime($bedaidentitas->tgl_lahir_lama)) ?></td>
									</tr>
									<tr>
										<td>Agama [ Data 2 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->agama_baru ?></td>
									</tr>
									<tr>
										<td>Alamat [ Data 2 ]</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->alamat_baru ?></td>
									</tr>

									<tr>
										<td>Data KK dan Data</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->datakk_dan_data ?></td>
									</tr>
									<tr>
										<td>Untuk Persyaratan</td>
										<td>:</td>
										<td><?php echo $bedaidentitas->utk_syarat ?></td>
									</tr>

								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-sk-identitas/' . $bedaidentitas->id_pengajuan) ?>" class="btn btn-primary" target="_blank">Cetak SK Identitas</a>
						<a href="<?php echo site_url('layanan-identitas-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
