<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Beda Identitas</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php elseif ($this->session->flashdata('error')) : ?>
			<div class="alert alert-error" role="alert">
				<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php endif; ?>

		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" id="nikpemohon" name="fnikpemohon" autofocus tabindex="1" />
						</div>
						
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" id="tglpemohon" name="ftglpemohon" tabindex="2" />
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="box-footer">
				<button class="btn btn-primary" type="button" id="btnCari" name="btnCariPemohon" tabindex="3">Cari Data</button>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Surat Keterangan Beda Identitas</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('tambah-identitas') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" class="form-control" name="fidpengajuanidentitas" value="<?php echo $invoice; ?>" readonly>
							<!-- <div class="form-group">
								<label>No. Surat</label>
								<input type="text" class="form-control" id="nosurat" name="fnosurat" tabindex="1" autofocus onkeypress="return hanyaAngka(event)">
							</div> -->
							<!-- /.form-group -->
							<div class="form-group">
								<label>No. KK [ Data 1 ]</label>
								<input type="text" class="form-control" id="nokkidentitas" name="fnokk" tabindex="2" onkeypress="return hanyaAngka(event)" onkeyup="copynokk();">
								<span style="color: red">
									<?php echo form_error('fnokk') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK [ Data 1 ]</label>
								<input type="text" class="form-control" id="nikidentitas" name="fnik" tabindex="2" onkeypress="return hanyaAngka(event)" onkeyup="copynik();">
								<span style="color: red">
									<?php echo form_error('fnik') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap [ Data 1 ]</label>
								<input type="text" class="form-control" id="namalengkapidentitas" name="fnamalengkap" tabindex="3" onkeyup="copynama();">
								<span style="color: red">
									<?php echo form_error('fnamalengkap') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin [ Data 1 ]</label>
								<select class="form-control select2" style="width: 100%;" id="jkel" name="fjkel" tabindex="4">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir [ Data 1 ]</label>
										<input type="text" class="form-control" id="tempatlahiridentitas" name="ftempatlahir" tabindex="5" onkeyup="copytempatlahir();">
										<span style="color: red">
											<?php echo form_error('ftempatlahir') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir [ Data 1 ]</label>
										<input type="date" class="form-control" id="tgllahir" name="ftgllahir" tabindex="6">
										<span style="color: red">
											<?php echo form_error('ftgllahir') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="form-group">
								<label>Agama [ Data 1 ]</label>
								<select class="form-control select2" style="width: 100%;" id="agama" name="fagama" tabindex="9">
									<option value="">--- Pilih ---</option>
									<option value="Islam">Islam</option>
									<option value="Protestan">Protestan</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Khonghucu">Khonghucu</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fagama') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat [ Data 1 ]</label>
								<input type="text" id="alamatidentitas" name="falamat" class="form-control" tabindex="13" onkeyup="copyalamat();">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>No. KK [ Data 2 ]</label>
								<input type="text" class="form-control" id="nokk2identitas" name="fnokk2" tabindex="2" onkeypress="return hanyaAngka(event)">
								<span style="color: red">
									<?php echo form_error('fnokk2') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK [ Data 2 ]</label>
								<input type="text" class="form-control" id="nik2identitas" name="fnik2" tabindex="2" onkeypress="return hanyaAngka(event)">
								<span style="color: red">
									<?php echo form_error('fnik2') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap [ Data 2 ]</label>
								<input type="text" class="form-control" id="namalengkap2identitas" name="fnamalengkap2" tabindex="3">
								<span style="color: red">
									<?php echo form_error('fnamalengkap2') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin [ Data 2 ]</label>
								<select class="form-control select2" style="width: 100%;" id="jkel2" name="fjkel2" tabindex="4">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel2') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir [ Data 2 ]</label>
										<input type="text" class="form-control" id="tempatlahir2identitas" name="ftempatlahir2" tabindex="5">
										<span style="color: red">
											<?php echo form_error('ftempatlahir2') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir [ Data 2 ]</label>
										<input type="date" class="form-control" id="tgllahir2" name="ftgllahir2" tabindex="6">
										<span style="color: red">
											<?php echo form_error('ftgllahir2') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="form-group">
								<label>Agama [ Data 2 ]</label>
								<select class="form-control select2" style="width: 100%;" id="agama2" name="fagama2" tabindex="9">
									<option value="">--- Pilih ---</option>
									<option value="Islam">Islam</option>
									<option value="Protestan">Protestan</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Khonghucu">Khonghucu</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fagama2') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat [ Data 2 ]</label>
								<input type="text" id="alamat2identitas" name="falamat2" class="form-control" tabindex="13">
								<span style="color: red">
									<?php echo form_error('falamat2') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Data KK dan Data</label>
										<input type="text" id="datakk" name="fdatakk" class="form-control" tabindex="13">
										<span style="color: red">
											<?php echo form_error('fdatakk') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Untuk Persyaratan</label>
										<input type="text" id="syarat" name="fsyarat" class="form-control" tabindex="13">
										<span style="color: red">
											<?php echo form_error('fsyarat') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
							</div>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" id="btnSimpan" name="btnSimpan" value="Simpan" tabindex="15" />
					<a href="<?php echo site_url('layanan-identitas-admin') ?>" class="btn btn-default" tabindex="16">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
