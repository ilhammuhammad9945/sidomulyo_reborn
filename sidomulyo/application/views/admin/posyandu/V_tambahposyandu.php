<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Menu Aktif</h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-university"></i> Lembaga Kemasyarakatan</a></li>
        <li><a href="#">Posyandu</a></li>
        <li class="active">Tambah Data</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <?php if ($this->session->flashdata('success')): ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
      <?php endif; ?>
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Pengurus Posyandu</h3>
        </div>
        <!-- /.box-header -->
        <form action="<?php base_url('C_lemmas/tambah_posyandu') ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="fnama" >
                  <span style="color: red">
                    <?php echo form_error('fnama') ?>
                  </span>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control select2" style="width: 100%;" name="fjkel">
                    <option value="">--- Pilih ---</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                  <span style="color: red">
                    <?php echo form_error('fjkel') ?>
                  </span>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Nomor Telepon</label>
                  <input type="text" class="form-control" name="ftelp" >
                  <span style="color: red">
                    <?php echo form_error('ftelp') ?>
                  </span>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" class="form-control" name="falamat" placeholder="Contoh: Dusun A RT 001 RW 001">
                  <span style="color: red">
                    <?php echo form_error('falamat') ?>
                  </span>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jabatan</label>
                  <select class="form-control select2" style="width: 100%;" name="fjabatan">
                    <option value="">--- Pilih ---</option>
                    <?php foreach($jabatan as $jbt):?>
                    <option value="<?php echo $jbt->id_jbt;?>"><?php echo $jbt->nama_jbt;?></option>
                    <?php endforeach;?>
                  </select>
                  <span style="color: red">
                    <?php echo form_error('fjabatan') ?>
                  </span>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Keterangan</label>
                  <select class="form-control select2" style="width: 100%;" name="fket">
                    <option value="">--- Pilih ---</option>
                    <?php foreach($keterangan as $kt):?>
                    <option value="<?php echo $kt->nama_org;?>"><?php echo $kt->nama_org;?></option>
                    <?php endforeach;?>
                  </select>
                  <span style="color: red">
                    <?php echo form_error('fket') ?>
                  </span>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Asal Posyandu</label>
                  <select class="form-control select2" style="width: 100%;" name="fasal">
                    <option value="">--- Pilih ---</option>
                    <?php foreach($asal as $as):?>
                    <option value="<?php echo $as->nama_org;?>"><?php echo $as->nama_org;?></option>
                    <?php endforeach;?>
                    <option value="-">Tidak Ada</option>
                  </select>
                  <span style="color: red">
                    <?php echo form_error('fasal') ?>
                  </span>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Foto</label>
                  <input type="file" name="ffoto" class="form-control">
                  <!-- <span style="color: red">
                    <?php //echo form_error('ffoto') ?>
                  </span> -->
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <div class="box-footer">
            <input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan"/>
            <a href="<?php echo site_url('posyandu-admin') ?>" class="btn btn-default">Kembali</a>
          </div>
        </form>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->