<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>User Profile</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-user"></i> Profil</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-3">
				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/template_admin/dist/img/admin.png" alt="User profile picture">

						<h3 class="profile-username text-center"><?php echo $this->session->userdata("usr_name"); ?></h3>

						<p class="text-muted text-center">
							<?php if ($this->session->userdata("usr_level") == 1) {
								echo "Admin Web Profil";
							} else if ($this->session->userdata("usr_level") == 2) {
								echo "Admin Pendataan";
							} else {
								echo "Editor Berita";
							} ?>
						</p>

						<a href="#" onclick="pindah_biodata()" class="btn btn-primary btn-block"><b>Biodata Diri</b></a>
						<a href="#" onclick="ubah_profil()" class="btn btn-primary btn-block"><b>Ubah Profil</b></a>
						<a href="#" onclick="ubah_password()" class="btn btn-primary btn-block"><b>Ubah Password</b></a>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
			<div class="col-md-9">
				<div class="box box-primary">
					<div id="biodata">
						<div class="box-header with-border">
							<h3 class="box-title">Biodata Diri</h3>
						</div>
						<!-- Table row -->
						<div class="box-body">
							<div class="col-xs-12 table-responsive">
								<table class="table table-striped">
									<tbody>
										<tr>
											<td><b>Nama</b></td>
											<td>: <?php echo $this->session->userdata("usr_name"); ?></td>
										</tr>
										<tr>
											<td><b>Level</b></td>
											<td>:
												<?php if ($this->session->userdata("usr_level") == 1) {
													echo "Admin Web Profil";
												} else if ($this->session->userdata("usr_level") == 2) {
													echo "Admin Pendataan";
												} else {
													echo "Editor Berita";
												} ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<div id="ubah_bio" style="display:none">
						<div class="box-header with-border">
							<h3 class="box-title">Ubah Profil</h3>
						</div>
						<form class="form-horizontal" action="<?php echo base_url() . 'admin/C_profil/ubah_profil' ?>" method="post">
							<?php foreach ($biodata as $key => $value) { ?>
								<div class="box-body">
									<?php if (isset($_SESSION['type2'])) { ?>
										<div class="alert alert-<?php echo $_SESSION['type2']; ?> alert-dismissible">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											<h4><?php echo $_SESSION['judul2']; ?></h4>
											<?php echo $_SESSION['isi2']; ?>
										</div>
										<?php
										unset($_SESSION['type2']);
										unset($_SESSION['isi2']);
										unset($_SESSION['judul2']);
										?>
									<?php } ?>
									<div class="form-group">
										<label for="inputName" class="col-sm-2 control-label">Nama</label>

										<div class="col-sm-10">
											<input type="text" class="form-control" name="nama" id="inputName" placeholder="Masukkan Nama Anda" value="<?= $value->nama_user; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail" class="col-sm-2 control-label">Username</label>

										<div class="col-sm-10">
											<input type="text" class="form-control" name="username" id="inputEmail" placeholder="Masukkan Username Anda" value="<?= $value->username_user; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label for="inputName" class="col-sm-2 control-label">Password Lama</label>

										<div class="col-sm-10">
											<input type="password" class="form-control" name="password" id="inputName" placeholder="Masukkan Password" required>
										</div>
									</div>
								</div>
							<?php } ?>


							<div class="box-footer">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-danger">Simpan</button>
								</div>
							</div>
						</form>
					</div>

					<div id="ubah_pass" style="display:none">
						<div class="box-header with-border">
							<h3 class="box-title">Ubah Password</h3>
						</div>
						<form class="form-horizontal" action="<?php echo base_url() . 'admin/C_profil/ubah_password' ?>" method="post">
							<div class="box-body">
								<?php if (isset($_SESSION['type3'])) { ?>
									<div class="alert alert-<?php echo $_SESSION['type3']; ?> alert-dismissible">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<h4><?php echo $_SESSION['judul3']; ?></h4>
										<?php echo $_SESSION['isi3']; ?>
									</div>
									<?php
									unset($_SESSION['type3']);
									unset($_SESSION['isi3']);
									unset($_SESSION['judul3']);
									?>
								<?php } ?>
								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Password Lama</label>

									<div class="col-sm-10">
										<input type="password" class="form-control" name="pass_lama" id="inputName" placeholder="Masukkan Password Lama" required>
									</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Password Baru</label>

									<div class="col-sm-10">
										<input type="password" class="form-control" name="pass_baru" id="inputName" placeholder="Masukkan Password Baru">
									</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Konfirmasi Password Baru</label>

									<div class="col-sm-10">
										<input type="password" class="form-control" name="konfirm_pass" id="inputName" placeholder="Masukkan Kembali Password Baru">
									</div>
								</div>
							</div>

							<div class="box-footer">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-danger">Simpan</button>
								</div>
							</div>
						</form>
					</div>

				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
	function pindah_biodata() {
		var ubah_bio = document.getElementById("ubah_bio");
		var biodata = document.getElementById("biodata");
		var ubah_pass = document.getElementById("ubah_pass");
		biodata.style.display = "block";
		ubah_bio.style.display = "none";
		ubah_pass.style.display = "none";
	}

	function ubah_profil() {
		var ubah_bio = document.getElementById("ubah_bio");
		var biodata = document.getElementById("biodata");
		var ubah_pass = document.getElementById("ubah_pass");
		biodata.style.display = "none";
		ubah_bio.style.display = "block";
		ubah_pass.style.display = "none";
	}

	function ubah_password() {
		var ubah_bio = document.getElementById("ubah_bio");
		var biodata = document.getElementById("biodata");
		var ubah_pass = document.getElementById("ubah_pass");
		biodata.style.display = "none";
		ubah_bio.style.display = "none";
		ubah_pass.style.display = "block";
	}

	<?php if (isset($_SESSION['tab_aktif'])) {
		if ($_SESSION['tab_aktif'] == 2) { ?>
			var ubah_bio = document.getElementById("ubah_bio");
			var biodata = document.getElementById("biodata");
			var ubah_pass = document.getElementById("ubah_pass");
			biodata.style.display = "none";
			ubah_bio.style.display = "block";
			ubah_pass.style.display = "none";
		<?php } else { ?>
			var ubah_bio = document.getElementById("ubah_bio");
			var biodata = document.getElementById("biodata");
			var ubah_pass = document.getElementById("ubah_pass");
			biodata.style.display = "none";
			ubah_bio.style.display = "none";
			ubah_pass.style.display = "block";
	<?php }
	} ?>
</script>
