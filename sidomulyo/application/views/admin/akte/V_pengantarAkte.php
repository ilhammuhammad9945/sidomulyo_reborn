<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pengantar Akte Kelahiran</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Progress Pengisian Form</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="progress">
							<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
								<span class="sr-only">0% Complete (success)</span>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (left) -->
		</div>

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Pengantar Akte Kelahiran</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('proses-simpan-pengantar-akte') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA ANAK :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<input type="hidden" class="form-control" name="id_peng" id="id_peng" value="<?= $invoice; ?>" tabindex="4">
								<label>Nama Lengkap Anak</label>
								<input type="text" class="form-control" name="fnamaanak" id="fnamaanak" tabindex="4">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin Anak</label>
								<select class="form-control select2" style="width: 100%;" name="fjkelanak" id="fjkelanak" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir Anak</label>
										<input type="text" class="form-control" name="ftempatlahiranak" id="ftempatlahiranak" tabindex="6">
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Anak</label>
										<input type="date" class="form-control" name="ftgllahiranak" id="ftgllahiranak" tabindex="7">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Jenis Kelahiran Anak</label>
										<select class="form-control select2" style="width: 100%;" name="fjenislahiranak" id="fjenislahiranak" tabindex="8">
											<option value="">-- Pilih --</option>
											<option value="Tunggal">Tunggal</option>
											<option value="Kembar">Kembar</option>
										</select>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-lg-6 -->
								<div class="col-lg-6">
									<div class="form-group">
										<label>Anak ke-</label>
										<input type="number" class="form-control" name="fanakke" id="fanakke" tabindex="9">
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-lg-6 -->
							</div>
							<!-- /.row -->

							<br>
							<div class="form-group">
								<label>DATA ORANG TUA :</label>
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama Ayah</label>
										<input type="text" name="fnamaayah" id="fnamaayah" class="form-control" tabindex="10">
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama Ibu</label>
										<input type="text" name="fnamaibu" id="fnamaibu" class="form-control" tabindex="11">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama Saksi 1</label>
										<input type="text" name="fnamasaksi1" id="fnamasaksi1" class="form-control" tabindex="12">
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama Saksi 2</label>
										<input type="text" name="fnamasaksi2" class="form-control" tabindex="13">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<br>
							<div class="form-group">
								<label>LAIN-LAIN :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Pemohon</label>
								<input type="text" name="fnikpemohon" class="form-control">
							</div>

							<div class="form-group">
								<label>Nama Pemohon</label>
								<input type="text" name="fnamapemohon" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>KELENGKAPAN BERKAS :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>FC legalisir sesuai aslinya Surat Nikah / Perkawinan</label>
								<input type="file" name="fcopynikah" class="form-control" tabindex="16">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>FC legalisir sesuai aslinya KTP Suami / Istri</label>
								<input type="file" name="fcopyktp" class="form-control" tabindex="17">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>FC legalisir sesuai aslinya KK (nama anak sudah tercantum)</label>
								<input type="file" name="fcopykk" class="form-control" tabindex="18">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>FC legalisir sesuai aslinya Ijazah bagi yang mempunyai</label>
								<input type="file" name="fcopyijaz" class="form-control" tabindex="19">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanPengantarAkte" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="21">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
