<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pengantar Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Pengantar Akte Kelahiran</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Pemohon :</h3>
							<h4> <?= $pengantar['nama_pemohon']; ?>
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : <?= format_indo(date("Y-m-d H:i", strtotime($pengantar['created_at']))) ?> WIB</span>
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>Nama Lengkap Anak</td>
										<td>:</td>
										<td><?= $pengantar['ank_nama']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:</td>
										<td>
											<?php if ($pengantar['ank_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Kelahiran </td>
										<td>:</td>
										<td><?= $pengantar['ank_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Hari/Tanggal</td>
										<td>:</td>
										<td><?= format_hari(date("D", strtotime($pengantar['ank_tgl_lahir']))) . ", " . format_indo(date("Y-m-d", strtotime($pengantar['ank_tgl_lahir']))); ?></td>
									</tr>
									<tr>
										<td>Jenis Kelahiran</td>
										<td>:</td>
										<td><?= $pengantar['ank_jenis_kelahiran']; ?></td>
									</tr>
									<tr>
										<td>Anak ke-</td>
										<td>:</td>
										<td><?= $pengantar['ank_anak_ke']; ?></td>
									</tr>
									<tr>
										<td>Nama Ayah</td>
										<td>:</td>
										<td><?= $pengantar['ortu_nama_ayah']; ?></td>
									</tr>
									<tr>
										<td>Nama Ibu</td>
										<td>:</td>
										<td><?= $pengantar['ortu_nama_ibu']; ?></td>
									</tr>
									<tr>
										<td>Nama Saksi 1</td>
										<td>:</td>
										<td><?= $pengantar['ortu_nama_saksi1']; ?></td>
									</tr>
									<tr>
										<td>Nama Saksi 2</td>
										<td>:</td>
										<td><?= $pengantar['ortu_nama_saksi2']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<h4>Berkas-berkas :</h4>
						<ul class="mailbox-attachments clearfix">
							<li>
								<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_surat_nikah'] ?>" alt="Attachment"></span>

								<div class="mailbox-attachment-info">
									<a href="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_surat_nikah'] ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i> FC Surat Nikah</a>
								</div>
							</li>
							<li>
								<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_ktp'] ?>" alt="Attachment"></span>

								<div class="mailbox-attachment-info">
									<a href="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_ktp'] ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i> FC KTP Suami/Istri</a>
								</div>
							</li>
							<li>
								<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_kk'] ?>" alt="Attachment"></span>

								<div class="mailbox-attachment-info">
									<a href="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_kk'] ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i>FC KK</a>
								</div>
							</li>
							<li>
								<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_ijazah'] ?>" alt="Attachment"></span>

								<div class="mailbox-attachment-info">
									<a href="<?php echo base_url(); ?>assets/upload/layanan/akte/<?= $pengantar['brk_ijazah'] ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i>FC Ijazah</a>
								</div>
							</li>

						</ul>
					</div>
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-pengantar-akte/' . $pengantar['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak Surat Pengantar</a>
						<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
