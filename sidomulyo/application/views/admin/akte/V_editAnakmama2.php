<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Akte Kelahiran</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<!-- /.box -->

		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div>

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Form Anak Mama 2 (Akte Kelahiran)</h3>
			</div>
			<!-- /.box-header -->
			<form action="#" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="fnamaayah" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" name="fpekerjaan" class="form-control" tabindex="5">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Rumah</label>
								<input type="text" name="falamatayah" class="form-control" tabindex="6">
							</div>
							<!-- /.form-group -->

							<br>
							<div class="form-group">
								<label>DATA ANAK :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Anak</label>
								<input type="text" class="form-control" name="fnamaanak" tabindex="7">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir</label>
										<input type="text" class="form-control" name="ftmptlahiranak" tabindex="8">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" class="form-control" name="ftgllahiranak" tabindex="9">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>Pukul</label>
								<input type="time" class="form-control" name="fpukulkelahiran" tabindex="10">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Anak ke-</label>
								<input type="number" class="form-control" name="fanakke" tabindex="11">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama Ibu</label>
										<input type="text" name="fnamaibu" class="form-control" tabindex="12">

									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Umur</label>
										<input type="number" name="fumuribu" class="form-control" tabindex="13">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>Alamat Rumah</label>
								<input type="text" name="falamatibu" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA SAKSI 1 :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Saksi 1</label>
								<input type="text" name="fnamasaksi1" class="form-control" tabindex="15">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur Saksi 1</label>
								<input type="number" name="fumursaksi1" class="form-control" tabindex="16">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan Saksi 1</label>
								<input type="text" name="fpekerjaansaksi1" class="form-control" tabindex="17">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Saksi 1</label>
								<input type="text" name="falamatsaksi1" class="form-control" tabindex="18">
							</div>
							<!-- /.form-group -->

							<br>
							<div class="form-group">
								<label>DATA SAKSI 2 :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Saksi 2</label>
								<input type="text" name="fnamasaksi2" class="form-control" tabindex="19">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur Saksi 2</label>
								<input type="number" name="fumursaksi2" class="form-control" tabindex="20">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" name="fpekerjaansaksi2" class="form-control" tabindex="21">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Saksi 2</label>
								<input type="text" name="falamatsaksi2" class="form-control" tabindex="22">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="23" />
					<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="24">Kembali</a>
					<a href="<?php echo site_url('lihat-anakmama2-akte/0') ?>" class="btn btn-danger" tabindex="25">Tombol Sementara Lihat Anak Mama 2</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
