<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pernyataan Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Pernyataan Akte Kelahiran</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Pemohon :</h3>
							<h4> <?= $pernyataan['nama_pemohon']; ?>
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : <?= format_indo(date("Y-m-d H:i", strtotime($pernyataan['created_at']))) ?> WIB</span>
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>Tempat Kelahiran Pemohon</td>
										<td>:</td>
										<td><?= $pernyataan['pmh_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Hari/Tanggal Lahir Pemohon</td>
										<td>:</td>
										<td><?= format_indo(date("Y-m-d", strtotime($pernyataan['pmh_tgl_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Pekerjaan Pemohon</td>
										<td>:</td>
										<td><?= $pernyataan['pmh_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Pemohon</td>
										<td>:</td>
										<td><?= $pernyataan['pmh_alamat']; ?></td>
									</tr>
									<tr>
										<td>DATA ANAK</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Anak</td>
										<td>:</td>
										<td><?= $pernyataan['ank_nama']; ?></td>
									</tr>
									<tr>
										<td>Tempat Kelahiran </td>
										<td>:</td>
										<td><?= $pernyataan['ank_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Hari/Tanggal</td>
										<td>:</td>
										<td><?= format_indo(date("Y-m-d", strtotime($pernyataan['ank_tgl_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Anak ke-</td>
										<td>:</td>
										<td><?= $pernyataan['ank_anak_ke']; ?></td>
									</tr>
									<tr>
										<td>SAKSI-SAKSI</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Saksi 1</td>
										<td>:</td>
										<td><?= $pernyataan['saksi_nama_1']; ?></td>
									</tr>
									<tr>
										<td>Alamat Saksi 1</td>
										<td>:</td>
										<td><?= $pernyataan['saksi_alamat_1']; ?></td>
									</tr>
									<tr>
										<td>Status Hubungan Saksi 1 dengan Bayi</td>
										<td>:</td>
										<td><?= $pernyataan['saksi_hub_1']; ?></td>
									</tr>
									<tr>
										<td>Nama Saksi 2</td>
										<td>:</td>
										<td><?= $pernyataan['saksi_nama_2']; ?></td>
									</tr>
									<tr>
										<td>Alamat Saksi 2</td>
										<td>:</td>
										<td><?= $pernyataan['saksi_alamat_2']; ?></td>
									</tr>
									<tr>
										<td>Status Hubungan Saksi 2 dengan Bayi</td>
										<td>:</td>
										<td><?= $pernyataan['saksi_hub_2']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-pernyataan-akte/' . $pernyataan['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak Surat Pernyataan</a>
						<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
