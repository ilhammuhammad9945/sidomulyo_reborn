<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li>Permohonan Surat</li>
			<li class="active">Data Akte Kelahiran</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('tambah-pengantar-akte') ?>" class="btn btn-block btn-primary">Tambah Data Akte</a></h3>
						<h3 class="box-title"><a href="<?php echo site_url('tambah-anakmama-akte') ?>" class="btn btn-block btn-primary">Tambah Data Anak Mama</a></h3>
						<!-- <h3 class="box-title"><a href="<?php echo site_url('tambah-anakmama2-akte') ?>" class="btn btn-block btn-primary">Tambah Data Anak Mama 2</a></h3> -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<?php if (isset($_SESSION['type'])) { ?>
							<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $_SESSION['isi']; ?>
							</div>
							<?php
							unset($_SESSION['type']);
							unset($_SESSION['isi']);
							unset($_SESSION['judul']);
							?>
						<?php } ?>

						<div class="row">
							<div class="col-md-3">
								<strong>Filter Data</strong>
								<div class="form-group">
									<select class="form-control select2" name="FdataAkte" id="FdataAkte" onchange="itemFtrAkte()">
										<option value="Akte" <?php if ($_SESSION['fsuratAkte'] == 'Akte') echo "selected"; ?>>Akte</option>
										<option value="Am" <?php if ($_SESSION['fsuratAkte'] == 'Am') echo "selected"; ?>>Anak Mama</option>
										<!-- <option value="semua" <?php if ($_SESSION['fKatGudang'] == 'semua') echo "selected"; ?>>Semua</option> -->
									</select>
								</div>
							</div>
						</div>

						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal Pengajuan</th>
									<th>ID Pengajuan</th>
									<th>Nama Pemohon</th>
									<th>Nama Anak</th>
									<th>Status Pembuatan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($_SESSION['fsuratAkte'] == 'Akte') { ?>
									<?php $no = 1;
									foreach ($surat_akte as $val) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?php echo format_indo(date("Y-m-d", strtotime($val->created_at)))  ?></td>
											<td><?php echo $val->id_pengajuan; ?></td>
											<td><?= $val->nama_pemohon ?></td>
											<td><?= $val->ank_nama ?></td>
											<?php if ($val->status_pembuatan == 'Selesai') { ?>
												<td><small class="label label-success"> Selesai</small></td>
											<?php } else { ?>
												<td><small class="label label-danger"> Dalam Proses</small></td>
											<?php } ?>
											<td>
												<a href="<?php echo site_url('edit-pengantar-akte/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<!-- <a onclick="lihatConfirm('<?php echo site_url('lihat-pengantar-akte/' . $val->id_surat) ?>')" href="#" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a> -->
												<a class="lihatAkte" data-toggle="modal" data-target="#modal-lihatakte" href="#" id_surat="<?= $val->id_surat ?>">
													<button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="Lihat Data"><i class="fa fa-eye"></i></button>
												</a>
												<a onclick="doneConfirm('<?php echo site_url('done-akte/' . $val->id_surat) ?>')" href="#" class="btn btn-warning btn-sm" title="Konfirmasi Selesai"><i class="fa fa-check"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-pengantar-akte/' . $val->id_surat) ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
								<?php } else { ?>
									<?php $no = 1;
									foreach ($surat_akte as $val) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?php echo format_indo(date("Y-m-d", strtotime($val->created_at)))  ?></td>
											<td><?php echo $val->id_pengajuan; ?></td>
											<td><?= $val->nama_pemohon ?></td>
											<td><?= $val->am1_nama_anak ?></td>
											<?php if ($val->status_pembuatan == 'Selesai') { ?>
												<td><small class="label label-success"> Selesai</small></td>
											<?php } else { ?>
												<td><small class="label label-danger"> Dalam Proses</small></td>
											<?php } ?>
											<td>
												<a href="<?php echo site_url('edit-anakmama-akte/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<a href="<?php echo site_url('lihat-anakmama-akte/' . $val->id_surat) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>
												<a onclick="doneConfirm('<?php echo site_url('done-akte/' . $val->id_surat) ?>')" href="#" class="btn btn-warning btn-sm" title="Konfirmasi Selesai"><i class="fa fa-check"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-pengantar-akte/' . $val->id_surat) ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
								<?php } ?>

							</tbody>
							<tfoot>
								<tr>
									<th>No</th>
									<th>Tanggal Pengajuan</th>
									<th>ID Pengajuan</th>
									<th>Nama Pemohon</th>
									<th>Nama Anak</th>
									<th>Status Pembuatan</th>
									<th>Aksi</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
	$(document).ready(function() {
		$(".lihatAkte").click(function(event) {
			var id_surat = $(this).attr("id_surat");
			$("#lid_surat").val(id_surat);
		});
	});

	function itemFtrAkte() {
		var dataAkte = $('#FdataAkte').val();
		$.ajax({
			url: '<?php echo base_url('admin/C_akte/filterdataAkte') ?>',
			method: "POST",
			data: {
				dataAkte: dataAkte
			},
			async: false,
			dataType: 'json',
			success: function() {
				// $('#dataProduk').html('');
				// $('#dataProduk').append(data);
				// $("#dataPengguna").DataTable({
				//   "responsive": true,
				//   "autoWidth": false,
				// });
			}
		});
		window.location.href = "<?php echo site_url('layanan-akte-admin') ?>";
	}
</script>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="doneModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Konfirmasi ke pemohon bahwa proses pembuatan surat telah selesai?</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-done" class="btn btn-success" href="#">Iya</a>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-lihatakte" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" action="<?= base_url('proses-lihat-akte') ?>">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Lihat Detail Data</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" name="lid_surat" id="lid_surat" readonly required>
								<label>Data yang Ingin Dilihat</label>
								<select class="form-control select2" style="width: 100%;" name="fpilihdata" required>
									<option value="">--- Pilih ---</option>
									<option value="Pengantar">Pengantar</option>
									<option value="Pernyataan">Pernyataan</option>
									<option value="F201">F-2.01</option>
								</select>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input class="btn btn-primary" type="submit" name="btnLihatAkte" value="Lihat" tabindex="20" />
				</div>
			</form>
		</div>
	</div>
</div>
