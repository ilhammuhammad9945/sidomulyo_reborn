<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Akte Kelahiran</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->


		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Form Anak Mama (Akte Kelahiran)</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_akte/simpanAm1Akte') ?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>NIK Pemohon</label>
										<input type="hidden" name="id_peng" value="<?= $invoice ?>" class="form-control" tabindex="4" required readonly>
										<input type="text" name="fnikayah" class="form-control" tabindex="4">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama Pemohon</label>
										<input type="text" name="fnamaayah" class="form-control" tabindex="4">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Pekerjaan</label>
										<input type="text" name="fpekerjaanayah" class="form-control" tabindex="5">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Kewarganegaraan</label>
										<select class="form-control select2" style="width: 100%;" name="fwniayah" tabindex="7">
											<option value="">--- Pilih ---</option>
											<option value="WNI">WNI</option>
											<option value="WNA">WNA / Asing</option>
										</select>
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Agama</label>
										<select class="form-control select2" style="width: 100%;" name="fagamaayah" tabindex="7">
											<option value="">--- Pilih ---</option>
											<option value="Islam">Islam</option>
											<option value="Kristen">Kristen</option>
											<option value="Hindu">Hindu</option>
											<option value="Budha">Budha</option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>Alamat Pemohon</label>
								<input type="text" name="falamatayah" class="form-control" tabindex="8">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama Ibu</label>
										<input type="text" name="fnamaibu" class="form-control" tabindex="8">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Umur Ibu</label>
										<input type="number" name="fumuribu" class="form-control" tabindex="8">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Alamat Ibu</label>
								<input type="text" name="falamatibu" class="form-control" tabindex="8">
							</div>
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Anak</label>
								<input type="text" class="form-control" name="fnamaanak" tabindex="9">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir Anak</label>
										<input type="text" class="form-control" name="ftmptlahiranak" tabindex="10">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Anak</label>
										<input type="date" class="form-control" name="ftgllahiranak" tabindex="11">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Jam Lahir Anak</label>
										<input type="time" class="form-control" name="fjamlahiranak" tabindex="11">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Anak ke-</label>
										<input type="number" class="form-control" name="fanakke" tabindex="12">
									</div>
								</div>
							</div>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
					<h4><b>DATA SAKSI-SAKSI</b></h4>
					<div class="row">
						<div class="col-md-6">
								<div class="form-group">
									<label>Nama Saksi 1</label>
									<input type="text" name="fnamasaksi1" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="form-group">
									<label>Umur Saksi 1</label>
									<input type="number" name="fumursaksi1" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="form-group">
									<label>Pekerjaan Saksi 1</label>
									<input type="text" name="fpekerjaansaksi1" class="form-control" tabindex="4">
								</div>
								<div class="form-group">
									<label>Alamat Saksi 1</label>
									<input type="text" name="falamatsaksi1" class="form-control" tabindex="4">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama Saksi 2</label>
									<input type="text" name="fnamasaksi2" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="form-group">
									<label>Umur Saksi 2</label>
									<input type="number" name="fumursaksi2" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="form-group">
									<label>Pekerjaan Saksi 2</label>
									<input type="text" name="fpekerjaansaksi2" class="form-control" tabindex="4">
								</div>
								<div class="form-group">
									<label>Alamat Saksi 2</label>
									<input type="text" name="falamatsaksi2" class="form-control" tabindex="4">
								</div>
							</div>
					</div>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpanam1" value="Simpan" tabindex="13" />
					<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="14">Kembali</a>

				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
