<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Form Anak Mama 2 (Akte Kelahiran)</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Pemohon :</h3>
							<h4> Toprak
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : 15 Februari 2016 11:03 WIB</span>
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>Pekerjaan Pemohon</td>
										<td>:</td>
										<td>Petani</td>
									</tr>
									<tr>
										<td>Alamat Rumah Pemohon</td>
										<td>:</td>
										<td>Jember</td>
									</tr>
									<tr>
										<td>DATA ANAK</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Anak</td>
										<td>:</td>
										<td>Hercules</td>
									</tr>
									<tr>
										<td>Tempat / Tanggal Lahir Anak</td>
										<td>:</td>
										<td>Jember, 15 Januari 2000</td>
									</tr>
									<tr>
										<td>Hari / Jam Kelahiran Anak</td>
										<td>:</td>
										<td>Selasa, 13.00 WIB</td>
									</tr>
									<tr>
										<td>Anak ke-</td>
										<td>:</td>
										<td>1</td>
									</tr>
									<tr>
										<td>DATA IBU</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Ibu</td>
										<td>:</td>
										<td>Sutini</td>
									</tr>
									<tr>
										<td>Umur Ibu</td>
										<td>:</td>
										<td>xx Tahun</td>
									</tr>
									<tr>
										<td>Alamat Rumah Ibu</td>
										<td>:</td>
										<td>Jember</td>
									</tr>
									<tr>
										<td>SAKSI 1</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Saksi 1</td>
										<td>:</td>
										<td>Adhitya</td>
									</tr>
									<tr>
										<td>Umur Saksi 1</td>
										<td>:</td>
										<td>xx Tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Saksi 1</td>
										<td>:</td>
										<td>Pedagang</td>
									</tr>
									<tr>
										<td>Alamat Saksi 1</td>
										<td>:</td>
										<td>Jember</td>
									</tr>

									<tr>
										<td>SAKSI 2</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Saksi 2</td>
										<td>:</td>
										<td>Wisnu</td>
									</tr>
									<tr>
										<td>Umur Saksi 2</td>
										<td>:</td>
										<td>xx Tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Saksi 2</td>
										<td>:</td>
										<td>Petani</td>
									</tr>
									<tr>
										<td>Alamat Saksi 2</td>
										<td>:</td>
										<td>Jember</td>
									</tr>

								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="#" class="btn btn-primary">Cetak Form Anak Mama 2</a>
						<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
