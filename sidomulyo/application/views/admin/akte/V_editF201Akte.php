<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Form F-2.01 Akte Kelahiran</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<!-- /.box -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Form F-2.01 Akte Kelahiran</h3>
			</div>
			<!-- /.box-header -->
			<form action="#" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kepala Keluarga</label>
								<input type="text" class="form-control" name="fnamakepala" tabindex="4">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nomor Kartu Keluarga</label>
								<input type="text" class="form-control" name="fnokk" tabindex="5">
							</div>
							<!-- /.form-group -->

							<br>
							<div class="form-group">
								<label>DATA ANAK :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Anak</label>
								<input type="text" class="form-control" name="fnamaanak" tabindex="6">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin Anak</label>
								<select class="form-control select2" style="width: 100%;" name="fjkelanak" tabindex="7">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Anak Dilahirkan</label>
										<input type="text" class="form-control" name="ftmptdilahirkananak" tabindex="8">
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir Anak</label>
										<input type="text" class="form-control" name="ftmptlahiranak" tabindex="9">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" class="form-control" name="ftgllahiranak" tabindex="10">
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-lg-6 -->
								<div class="col-lg-6">
									<div class="form-group">
										<label>Pukul</label>
										<input type="time" class="form-control" name="fpukullahiranak" tabindex="11">
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-lg-6 -->
							</div>
							<!-- /.row -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Jenis Kelahiran</label>
										<select class="form-control select2" style="width: 100%;" name="fjeniskelahirananak" tabindex="12">
											<option value="">-- Pilih --</option>
											<option value="Tunggal">Tunggal</option>
											<option value="Kembar">Kembar</option>
										</select>
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Anak ke-</label>
										<input type="number" class="form-control" name="fanakke" tabindex="13">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>Penolong Kelahiran</label>
								<select class="form-control select2" style="width: 100%;" name="fpenolongkelahiran" tabindex="14">
									<option value="">-- Pilih --</option>
									<option value="Bidan/Perawat">Bidan/Perawat</option>
									<option value="Dokter">Dokter</option>
								</select>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Berat Bayi (Kg)</label>
										<input type="text" class="form-control" name="fberatbayi" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Panjang Bayi (cm)</label>
										<input type="text" class="form-control" name="fpanjangbayi" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<br>
							<div class="form-group">
								<label>DATA IBU :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Ibu</label>
								<input type="text" name="fnikibu" class="form-control" tabindex="17">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Ibu</label>
								<input type="text" name="fnamaibu" class="form-control" tabindex="18">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Ibu</label>
										<input type="date" name="ftgllahiribu" class="form-control" tabindex="19">

									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Umur Ibu</label>
										<input type="text" name="fumuribu" class="form-control" tabindex="20" readonly>
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>Pekerjaan Ibu</label>
								<input type="text" name="fpekerjaanibu" class="form-control" tabindex="21">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Ibu</label>
								<input type="text" name="falamatibu" class="form-control" tabindex="22">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kewarganegaraan Ibu</label>
										<select class="form-control select2" style="width: 100%;" name="fwniibu" tabindex="23">
											<option value="">-- Pilih --</option>
											<option value="WNI">WNI</option>
											<option value="WNA">WNA</option>
										</select>
										<!-- <input type="text" name="fwniibu" class="form-control" tabindex="23"> -->
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Kebangsaan Ibu</label>
										<input type="text" name="fbangsaibu" class="form-control" tabindex="24">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Pencatatan Perkawinan Ibu</label>
										<input type="date" name="fkawinibu" class="form-control" tabindex="25">

									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Umur Ibu Saat Perkawinan</label>
										<input type="text" name="fumurkwibu" class="form-control" tabindex="26" readonly>
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<br>
							<div class="form-group">
								<label>DATA AYAH :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Ayah</label>
								<input type="text" name="fnikayah" class="form-control" tabindex="27">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Ayah</label>
								<input type="text" name="fnamaayah" class="form-control" tabindex="28">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Ayah</label>
										<input type="date" name="ftgllahirayah" class="form-control" tabindex="29">

									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Umur Ayah</label>
										<input type="text" name="fumurayah" class="form-control" tabindex="30" readonly>
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>Pekerjaan Ayah</label>
								<input type="text" name="fpekerjaanayah" class="form-control" tabindex="31">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Ayah</label>
								<input type="text" name="falamatayah" class="form-control" tabindex="32">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kewarganegaraan Ayah</label>
										<select class="form-control select2" style="width: 100%;" name="fwniayah" tabindex="33">
											<option value="">-- Pilih --</option>
											<option value="WNI">WNI</option>
											<option value="WNA">WNA</option>
										</select>
										<!-- <input type="text" name="fwniayah" class="form-control" tabindex="33"> -->
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Kebangsaan Ayah</label>
										<input type="text" name="fbangsaayah" class="form-control" tabindex="34">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<br>
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PELAPOR :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Pelapor</label>
								<input type="text" name="fnikpelapor" class="form-control" tabindex="35">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Pelapor</label>
								<input type="text" name="fnamapelapor" class="form-control" tabindex="36">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur Pelapor</label>
								<input type="number" name="fumurpelapor" class="form-control" tabindex="37">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin Pelapor</label>
								<select class="form-control select2" style="width: 100%;" name="fjkelpelapor" tabindex="38">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan Pelapor</label>
								<input type="text" name="fpekerjaanpelapor" class="form-control" tabindex="39">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Pelapor</label>
								<input type="text" name="falamatpelapor" class="form-control" tabindex="40">
							</div>
							<!-- /.form-group -->

							<br>
							<div class="form-group">
								<label>DATA SAKSI 1 :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Saksi 1</label>
								<input type="text" name="fniksaksi1" class="form-control" tabindex="41">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Saksi 1</label>
								<input type="text" name="fnamasaksi1" class="form-control" tabindex="42">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur Saksi 1</label>
								<input type="number" name="fumursaksi1" class="form-control" tabindex="43">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan Saksi 1</label>
								<input type="text" name="fpekerjaansaksi1" class="form-control" tabindex="44">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Saksi 1</label>
								<input type="text" name="falamatsaksi1" class="form-control" tabindex="45">
							</div>
							<!-- /.form-group -->

							<br>
							<div class="form-group">
								<label>DATA SAKSI 2 :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Saksi 2</label>
								<input type="text" name="fniksaksi2" class="form-control" tabindex="46">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Saksi 2</label>
								<input type="text" name="fnamasaksi2" class="form-control" tabindex="47">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Umur Saksi 2</label>
								<input type="number" name="fumursaksi2" class="form-control" tabindex="48">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Pekerjaan Saksi 2</label>
								<input type="text" name="fpekerjaansaksi2" class="form-control" tabindex="49">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Saksi 2</label>
								<input type="text" name="falamatsaksi2" class="form-control" tabindex="50">
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="51" />
					<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="52">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
