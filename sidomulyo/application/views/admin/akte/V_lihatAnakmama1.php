<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Form Anak Mama (Akte Kelahiran)</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Pemohon :</h3>
							<h4> <?= $anakmama['nama_pemohon'] ?>
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : <?= format_indo(date("Y-m-d H:i", strtotime($anakmama['created_at']))) ?> WIB</span>
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>Pekerjaan</td>
										<td>:</td>
										<td><?= $anakmama['am1_pekerjaan'] ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan</td>
										<td>:</td>
										<td><?= $anakmama['am1_kwn'] ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>:</td>
										<td><?= $anakmama['am1_agama'] ?></td>
									</tr>
									<tr>
										<td>Alamat Rumah</td>
										<td>:</td>
										<td><?= $anakmama['am1_alamat'] ?></td>
									</tr>
									<tr>
										<td>DATA ANAK</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Anak</td>
										<td>:</td>
										<td><?= $anakmama['am1_nama_anak'] ?></td>
									</tr>
									<tr>
										<td>Anak ke-</td>
										<td>:</td>
										<td><?= $anakmama['am1_anak_ke'] ?></td>
									</tr>
									<tr>
										<td>Tempat / Tanggal Lahir Anak</td>
										<td>:</td>
										<td><?= $anakmama['am1_tempat_lahir_anak'] ?>, <?= format_indo(date("Y-m-d", strtotime($anakmama['am1_tanggal_lahir_anak']))) ?></td>
									</tr>
									<tr>
										<td>Pukul </td>
										<td>:</td>
										<td><?= $anakmama['am1_jam_lahir_anak'] ?> WIB</td>
									</tr>
									<tr>
										<td>DATA IBU</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Ibu</td>
										<td>:</td>
										<td><?= $anakmama['am1_nama_ibu'] ?></td>
									</tr>
									<tr>
										<td>Umur Ibu</td>
										<td>:</td>
										<td><?= $anakmama['am1_umur_ibu'] ?> tahun</td>
									</tr>
									<tr>
										<td>Alamat Ibu</td>
										<td>:</td>
										<td><?= $anakmama['am1_alamat_ibu'] ?></td>
									</tr>
									<tr>
										<td>DATA SAKSI</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Saksi 1</td>
										<td>:</td>
										<td><?= $anakmama['am1_nama_saksi1'] ?></td>
									</tr>
									<tr>
										<td>Umur Saksi 1</td>
										<td>:</td>
										<td><?= $anakmama['am1_umur_saksi1'] ?> tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Saksi 1</td>
										<td>:</td>
										<td><?= $anakmama['am1_pekerjaan_saksi1'] ?></td>
									</tr>
									<tr>
										<td>Alamat Saksi 1</td>
										<td>:</td>
										<td><?= $anakmama['am1_alamat_saksi1'] ?></td>
									</tr>
									<tr>
										<td>Nama Saksi 2</td>
										<td>:</td>
										<td><?= $anakmama['am1_nama_saksi2'] ?></td>
									</tr>
									<tr>
										<td>Umur Saksi 2</td>
										<td>:</td>
										<td><?= $anakmama['am1_umur_saksi2'] ?> tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Saksi 2</td>
										<td>:</td>
										<td><?= $anakmama['am1_pekerjaan_saksi2'] ?></td>
									</tr>
									<tr>
										<td>Alamat Saksi 2</td>
										<td>:</td>
										<td><?= $anakmama['am1_alamat_saksi2'] ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-anak-mama1/' . $anakmama['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Form Anak Mama 1</a>
						<a href="<?php echo site_url('print-anak-mama2/' . $anakmama['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Form Anak Mama 2</a>
						<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
