<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">F-2.01 Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat F-2.01 Akte Kelahiran</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Kepala Keluarga :</h3>
							<h4> <?= $f21['kep_keluarga_nama']; ?>
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : <?= format_indo(date("Y-m-d H:i", strtotime($f21['created_at']))) ?> WIB</span>
							</h4>
							<h3>Nomor Kartu Keluarga :</h3>
							<h4> <?= $f21['nomor_kk']; ?></h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>BAYI ANAK</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Lengkap Anak</td>
										<td>:</td>
										<td><?= $f21['ank_nama']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin Anak</td>
										<td>:</td>
										<td>
											<?php if ($f21['ank_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Anak Dilahirkan</td>
										<td>:</td>
										<td><?= $f21['ank_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tempat Kelahiran Anak</td>
										<td>:</td>
										<td><?= $f21['ank_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Hari/Tanggal Anak</td>
										<td>:</td>
										<td><?= format_indo(date("Y-m-d", strtotime($f21['ank_tgl_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Pukul Kelahiran</td>
										<td>:</td>
										<td><?= $f21['ank_pukul_lahir']; ?> WIB</td>
									</tr>
									<tr>
										<td>Jenis Kelahiran</td>
										<td>:</td>
										<td><?= $f21['ank_jenis_kelahiran']; ?></td>
									</tr>
									<tr>
										<td>Kelahiran Ke-</td>
										<td>:</td>
										<td><?= $f21['ank_anak_ke']; ?></td>
									</tr>
									<tr>
										<td>Penolong Kelahiran</td>
										<td>:</td>
										<td><?= $f21['ank_penolong_lahir']; ?></td>
									</tr>
									<tr>
										<td>Berat Bayi</td>
										<td>:</td>
										<td><?= $f21['ank_berat']; ?> Kg</td>
									</tr>
									<tr>
										<td>Panjang Bayi</td>
										<td>:</td>
										<td><?= $f21['ank_panjang']; ?> cm</td>
									</tr>
									<tr>
										<td>DATA IBU</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>NIK Ibu</td>
										<td>:</td>
										<td><?= $f21['ibu_nik']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Ibu</td>
										<td>:</td>
										<td><?= $f21['ortu_nama_ibu']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir / Umur Ibu</td>
										<td>:</td>
										<td><?= format_indo(date("Y-m-d", strtotime($f21['ibu_tgl_lahir']))); ?>, <?= $f21['ibu_umur'] ?> Tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Ibu</td>
										<td>:</td>
										<td><?= $f21['ibu_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Ibu</td>
										<td>:</td>
										<td><?= $f21['ibu_alamat']; ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan Ibu</td>
										<td>:</td>
										<td><?= $f21['ibu_kwn']; ?></td>
									</tr>
									<tr>
										<td>Kebangsaan Ibu</td>
										<td>:</td>
										<td><?= $f21['ibu_kebangsaan']; ?></td>
									</tr>
									<tr>
										<td>Tgl Pencatatan Perkawinan Ibu</td>
										<td>:</td>
										<td><?= format_indo(date("Y-m-d", strtotime($f21['ibu_tgl_kawin']))) ?></td>
									</tr>
									<tr>
										<td>Umur Ibu Saat Perkawinan</td>
										<td>:</td>
										<td><?= $f21['ibu_umur_kawin']; ?> Tahun</td>
									</tr>
									<tr>
										<td>DATA AYAH</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>NIK Ayah</td>
										<td>:</td>
										<td><?= $f21['ayah_nik']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Ayah</td>
										<td>:</td>
										<td><?= $f21['ortu_nama_ayah']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir / Umur Ayah</td>
										<td>:</td>
										<td><?= format_indo(date("Y-m-d", strtotime($f21['ayah_tgl_lahir']))) ?> / <?= $f21['ayah_umur']; ?> Tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Ayah</td>
										<td>:</td>
										<td><?= $f21['ayah_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Ayah</td>
										<td>:</td>
										<td><?= $f21['ayah_alamat']; ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan Ayah</td>
										<td>:</td>
										<td><?= $f21['ayah_kwn']; ?></td>
									</tr>
									<tr>
										<td>Kebangsaan Ayah</td>
										<td>:</td>
										<td><?= $f21['ayah_kebangsaan']; ?></td>
									</tr>
									<tr>
										<td>DATA PELAPOR</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>NIK Pelapor</td>
										<td>:</td>
										<td><?= $f21['plp_nik']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Pelapor</td>
										<td>:</td>
										<td><?= $f21['plp_nama']; ?></td>
									</tr>
									<tr>
										<td>Umur Pelapor</td>
										<td>:</td>
										<td><?= $f21['plp_umur']; ?> Tahun</td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:</td>
										<td>
											<?php if ($f21['plp_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Pekerjaan Pelapor</td>
										<td>:</td>
										<td><?= $f21['plp_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Pelapor</td>
										<td>:</td>
										<td><?= $f21['plp_alamat']; ?></td>
									</tr>

									<tr>
										<td>SAKSI 1</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>NIK Saksi 1</td>
										<td>:</td>
										<td><?= $f21['saksi1_nik']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Saksi 1</td>
										<td>:</td>
										<td><?= $f21['saksi_nama_1']; ?></td>
									</tr>
									<tr>
										<td>Umur Saksi 1</td>
										<td>:</td>
										<td><?= $f21['saksi1_umur']; ?> Tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Saksi 1</td>
										<td>:</td>
										<td><?= $f21['saksi1_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Saksi 1</td>
										<td>:</td>
										<td><?= $f21['saksi1_alamat']; ?></td>
									</tr>

									<tr>
										<td>SAKSI 2</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>NIK Saksi 2</td>
										<td>:</td>
										<td><?= $f21['saksi2_nik']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Saksi 2</td>
										<td>:</td>
										<td><?= $f21['saksi_nama_2']; ?></td>
									</tr>
									<tr>
										<td>Umur Saksi 2</td>
										<td>:</td>
										<td><?= $f21['saksi2_umur']; ?> Tahun</td>
									</tr>
									<tr>
										<td>Pekerjaan Saksi 2</td>
										<td>:</td>
										<td><?= $f21['saksi2_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Saksi 2</td>
										<td>:</td>
										<td><?= $f21['saksi1_alamat']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-f201-akte/' . $f21['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak Form F-2.01</a>
						<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
