<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pengantar Akte Kelahiran</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="row">
			<div class="col-md-12">
				<!-- Custom Tabs -->
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab">Form Pengantar</a></li>
						<li><a href="#tab_2" data-toggle="tab">Form Pernyataan</a></li>
						<li><a href="#tab_3" data-toggle="tab">Form F-2.01</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<!-- SELECT2 EXAMPLE -->
							<!-- <div class="box box-default"> -->
							<div class="box-header with-border">
								<h3 class="box-title">Edit Pengantar Akte Kelahiran</h3>
							</div>
							<!-- /.box-header -->
							<form action="<?= base_url('proses-edit-pengantar-akte') ?>" method="post" enctype="multipart/form-data">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>DATA ANAK :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap</label>
												<input type="hidden" name="id_surat" value="<?= $surat_akte['id_surat'] ?>" readonly>
												<input type="text" class="form-control" name="fnamaanak" value="<?= $surat_akte['ank_nama'] ?>" tabindex="4">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Jenis Kelamin</label>
												<select class="form-control select2" style="width: 100%;" name="fjkelanak" tabindex="5">
													<option value="">--- Pilih ---</option>
													<option value="L" <?php if ($surat_akte['ank_gender'] == 'L') {
																			echo "selected";
																		} ?>>Laki-laki</option>
													<option value="P" <?php if ($surat_akte['ank_gender'] == 'P') {
																			echo "selected";
																		} ?>>Perempuan</option>
												</select>
											</div>
											<!-- /.form-group -->


											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tempat Lahir</label>
														<input type="text" class="form-control" name="ftempatlahiranak" value="<?= $surat_akte['ank_tempat_lahir'] ?>" tabindex="6">
													</div>
													<!-- /.form-group -->
												</div>
												<!-- /.col-lg-6 -->
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Lahir</label>
														<input type="date" class="form-control" name="ftgllahiranak" value="<?= $surat_akte['ank_tgl_lahir'] ?>" tabindex="7">
													</div>
													<!-- /.form-group -->
												</div>
												<!-- /.col-lg-6 -->
											</div>
											<!-- /.row -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Jenis Kelahiran</label>
														<select class="form-control select2" style="width: 100%;" name="fjenislahiranak" tabindex="8">
															<option value="">-- Pilih --</option>
															<option value="Tunggal" <?php if ($surat_akte['ank_jenis_kelahiran'] == 'Tunggal') {
																						echo "selected";
																					} ?>>Tunggal</option>
															<option value="Kembar" <?php if ($surat_akte['ank_jenis_kelahiran'] == 'Kembar') {
																						echo "selected";
																					} ?>>Kembar</option>
														</select>
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Anak ke-</label>
														<input type="number" class="form-control" name="fanakke" value="<?= $surat_akte['ank_anak_ke'] ?>" tabindex="9">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<br>
											<div class="form-group">
												<label>DATA ORANG TUA :</label>
											</div>
											<!-- /.form-group -->
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Nama Ayah</label>
														<input type="text" name="fnamaayah" value="<?= $surat_akte['ortu_nama_ayah'] ?>" class="form-control" tabindex="10">
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Nama Ibu</label>
														<input type="text" name="fnamaibu" value="<?= $surat_akte['ortu_nama_ibu'] ?>" class="form-control" tabindex="11">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Nama Saksi 1</label>
														<input type="text" name="fnamasaksi1" value="<?= $surat_akte['ortu_nama_saksi1'] ?>" class="form-control" tabindex="12">
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Nama Saksi 2</label>
														<input type="text" name="fnamasaksi2" value="<?= $surat_akte['ortu_nama_saksi2'] ?>" class="form-control" tabindex="13">
													</div>
													<!-- /.form-group -->
												</div>
											</div>
											<br>
											<div class="form-group">
												<label>LAIN-LAIN :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>NIK Pemohon</label>
												<input type="text" name="fnikpemohon" value="<?= $surat_akte['nik_pemohon'] ?>" class="form-control">
											</div>

											<div class="form-group">
												<label>Nama Pemohon</label>
												<input type="text" name="fnamapemohon" value="<?= $surat_akte['nama_pemohon'] ?>" class="form-control" tabindex="14">
											</div>
											<!-- /.form-group -->
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											<div class="form-group">
												<label>KELENGKAPAN BERKAS :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>FC legalisir sesuai aslinya Surat Nikah/Perkawinan</label>
												<input type="file" name="fcopynikah" class="form-control" tabindex="16">
												<i class="fa fa-eye"></i>
												<a href="<?php echo base_url('assets/upload/layanan/akte/' . $surat_akte['brk_surat_nikah']) ?>" target="_blank">Lihat Berkas</a>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>FC legalisir sesuai aslinya KTP Suami/Istri</label>
												<input type="file" name="fcopyktp" class="form-control" tabindex="17">
												<i class="fa fa-eye"></i>
												<a href="<?php echo base_url('assets/upload/layanan/akte/' . $surat_akte['brk_ktp']) ?>" target="_blank">Lihat Berkas</a>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>FC legalisir sesuai aslinya KK (nama anak sudah tercantum)</label>
												<input type="file" name="fcopykk" class="form-control" tabindex="18">
												<i class="fa fa-eye"></i>
												<a href="<?php echo base_url('assets/upload/layanan/akte/' . $surat_akte['brk_kk']) ?>" target="_blank">Lihat Berkas</a>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>FC legalisir sesuai aslinya Ijazah bagi yang mempunyai</label>
												<input type="file" name="fcopyijaz" class="form-control" tabindex="19">
												<i class="fa fa-eye"></i>
												<a href="<?php echo base_url('assets/upload/layanan/akte/' . $surat_akte['brk_ijazah']) ?>" target="_blank">Lihat Berkas</a>
											</div>
											<!-- /.form-group -->

										</div>
										<!-- /.col -->
									</div>
									<!-- /.row -->
								</div>
								<div class="box-footer">
									<input class="btn btn-primary" type="submit" name="btnEditPengantarAkte" value="Simpan" tabindex="20" />
									<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="21">Kembali</a>
								</div>
							</form>
							<!-- /.box-body -->
							<!-- </div> -->
							<!-- /.box -->
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="tab_2">
							<!-- SELECT2 EXAMPLE -->
							<!-- <div class="box box-primary"> -->
							<div class="box-header with-border">
								<h3 class="box-title">Edit Pernyataan Akte Kelahiran</h3>
							</div>
							<!-- /.box-header -->
							<form action="<?= base_url('proses-edit-pernyataan-akte') ?>" method="post" enctype="multipart/form-data">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Nama Pemohon</label>
												<input type="hidden" name="id_surat" value="<?= $surat_akte['id_surat'] ?>" readonly>
												<input type="text" name="fnamapemohon" value="<?= $surat_akte['nama_pemohon'] ?>" class="form-control" tabindex="4">
											</div>
											<!-- /.form-group -->
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tempat Lahir</label>
														<input type="text" name="ftmptlahirpemohon" value="<?= $surat_akte['pmh_tempat_lahir'] ?>" class="form-control" tabindex="5">
													</div>
													<!-- /.form-group -->
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Lahir</label>
														<input type="date" name="ftgllahirpemohon" value="<?= $surat_akte['pmh_tgl_lahir'] ?>" class="form-control" tabindex="6">
													</div>
													<!-- /.form-group -->
												</div>
											</div>


											<div class="form-group">
												<label>Pekerjaan</label>
												<input type="text" name="fpekerjaanpemohon" value="<?= $surat_akte['pmh_pekerjaan'] ?>" class="form-control" tabindex="7">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat</label>
												<input type="text" name="falamatpemohon" value="<?= $surat_akte['pmh_alamat'] ?>" class="form-control" tabindex="8">
											</div>
											<!-- /.form-group -->

											<br>

											<div class="form-group">
												<label>DATA ANAK : </label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Anak</label>
												<input type="text" class="form-control" name="fnamaanak" value="<?= $surat_akte['ank_nama'] ?>" tabindex="9">
											</div>
											<!-- /.form-group -->
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tempat Lahir Anak</label>
														<input type="text" class="form-control" name="ftmptlahiranak" value="<?= $surat_akte['ank_tempat_lahir'] ?>" tabindex="10">
													</div>
													<!-- /.form-group -->
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Lahir Anak</label>
														<input type="date" class="form-control" name="ftgllahiranak" value="<?= $surat_akte['ank_tgl_lahir'] ?>" tabindex="11">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="form-group">
												<label>Anak ke-</label>
												<input type="number" class="form-control" name="fanakke" value="<?= $surat_akte['ank_anak_ke'] ?>" tabindex="12">
											</div>
											<!-- /.form-group -->

										</div>
										<!-- /.col -->
										<div class="col-md-6">
											<div class="form-group">
												<label>Nama Saksi 1</label>
												<input type="text" name="fnamasaksi1" value="<?= $surat_akte['saksi_nama_1'] ?>" class="form-control" tabindex="13">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Saksi 1</label>
												<input type="text" name="falamatsaksi1" value="<?= $surat_akte['saksi_alamat_1'] ?>" class="form-control" tabindex="14">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Status Hubungan Saksi 1 dengan Bayi</label>
												<input type="text" name="fhubsaksi1" value="<?= $surat_akte['saksi_hub_1'] ?>" class="form-control" tabindex="15">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Saksi 2</label>
												<input type="text" name="fnamasaksi2" value="<?= $surat_akte['saksi_nama_2'] ?>" class="form-control" tabindex="16">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Saksi 2</label>
												<input type="text" name="falamatsaksi2" value="<?= $surat_akte['saksi_alamat_2'] ?>" class="form-control" tabindex="17">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Status Hubungan Saksi 2 dengan Bayi</label>
												<input type="text" name="fhubsaksi2" value="<?= $surat_akte['saksi_hub_2'] ?>" class="form-control" tabindex="18">
											</div>
											<!-- /.form-group -->

										</div>
										<!-- /.col -->
									</div>
									<!-- /.row -->
								</div>
								<div class="box-footer">
									<input class="btn btn-primary" type="submit" name="btnEditPernyataanAkte" value="Simpan" tabindex="19" />
									<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
								</div>
							</form>
							<!-- /.box-body -->
							<!-- </div> -->
							<!-- /.box -->
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="tab_3">
							<!-- SELECT2 EXAMPLE -->
							<!-- <div class="box box-primary"> -->
							<div class="box-header with-border">
								<h3 class="box-title">Edit Form F-2.01 Akte Kelahiran</h3>
							</div>
							<!-- /.box-header -->
							<form action="<?= base_url('proses-edit-f201-akte') ?>" method="post" enctype="multipart/form-data">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Nama Kepala Keluarga</label>
												<input type="hidden" name="id_surat" value="<?= $surat_akte['id_surat'] ?>" readonly>
												<input type="text" class="form-control" name="fnamakepala" value="<?= $surat_akte['kep_keluarga_nama'] ?>" tabindex="4">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nomor Kartu Keluarga</label>
												<input type="text" class="form-control" name="fnokk" value="<?= $surat_akte['nomor_kk'] ?>" tabindex="5">
											</div>
											<!-- /.form-group -->

											<br>
											<div class="form-group">
												<label>DATA ANAK :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap Anak</label>
												<input type="text" class="form-control" name="fnamaanak" value="<?= $surat_akte['ank_nama'] ?>" tabindex="6">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Jenis Kelamin Anak</label>
												<select class="form-control select2" style="width: 100%;" name="fjkelanak" tabindex="7">
													<option value="">--- Pilih ---</option>
													<option value="L" <?php if ($surat_akte['ank_gender'] == 'L') {
																			echo "selected";
																		} ?>>Laki-laki</option>
													<option value="P" <?php if ($surat_akte['ank_gender'] == 'P') {
																			echo "selected";
																		} ?>>Perempuan</option>
												</select>
											</div>
											<!-- /.form-group -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tempat Anak dilahirkan</label>
														<input type="text" class="form-control" name="ftmptdilahirkananak" value="<?= $surat_akte['ank_tempat_lahir'] ?>" tabindex="8">
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Tempat Lahir Anak</label>
														<input type="text" class="form-control" name="ftmptlahiranak" value="<?= $surat_akte['ank_tempat_lahir'] ?>" tabindex="9">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Lahir</label>
														<input type="date" class="form-control" name="ftgllahiranak" value="<?= $surat_akte['ank_tgl_lahir'] ?>" tabindex="10">
													</div>
													<!-- /.form-group -->
												</div>
												<!-- /.col-lg-6 -->
												<div class="col-lg-6">
													<div class="form-group">
														<label>Pukul</label>
														<input type="time" class="form-control" name="fpukullahiranak" value="<?= $surat_akte['ank_pukul_lahir'] ?>" tabindex="11">
													</div>
													<!-- /.form-group -->
												</div>
												<!-- /.col-lg-6 -->
											</div>
											<!-- /.row -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Jenis Kelahiran</label>
														<select class="form-control select2" style="width: 100%;" name="fjeniskelahirananak" tabindex="12">
															<option value="">-- Pilih --</option>
															<option value="Tunggal" <?php if ($surat_akte['ank_jenis_kelahiran'] == 'Tunggal') {
																						echo "selected";
																					} ?>>Tunggal</option>
															<option value="Kembar" <?php if ($surat_akte['ank_jenis_kelahiran'] == 'Kembar') {
																						echo "selected";
																					} ?>>Kembar</option>
														</select>
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Anak ke-</label>
														<input type="number" class="form-control" name="fanakke" value="<?= $surat_akte['ank_anak_ke'] ?>" tabindex="13">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="form-group">
												<label>Penolong Kelahiran</label>
												<select class="form-control select2" style="width: 100%;" name="fpenolongkelahiran" tabindex="14">
													<option value="">-- Pilih --</option>
													<option value="Bidan/Perawat" <?php if ($surat_akte['ank_penolong_lahir'] == 'Bidan/Perawat') {
																						echo "selected";
																					} ?>>Bidan/Perawat</option>
													<option value="Dokter" <?php if ($surat_akte['ank_penolong_lahir'] == 'Dokter') {
																				echo "selected";
																			} ?>>Dokter</option>
												</select>
											</div>
											<!-- /.form-group -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Berat Bayi (Kg)</label>
														<input type="text" class="form-control" name="fberatbayi" value="<?= $surat_akte['ank_berat'] ?>" tabindex="15">
													</div>
													<!-- /.form-group -->
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label>Panjang Bayi (cm)</label>
														<input type="text" class="form-control" name="fpanjangbayi" value="<?= $surat_akte['ank_panjang'] ?>" tabindex="16">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<br>
											<div class="form-group">
												<label>DATA IBU :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>NIK Ibu</label>
												<input type="text" name="fnikibu" value="<?= $surat_akte['ibu_nik'] ?>" class="form-control" tabindex="17">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap Ibu</label>
												<input type="text" name="fnamaibu" value="<?= $surat_akte['ortu_nama_ibu'] ?>" class="form-control" tabindex="18">
											</div>
											<!-- /.form-group -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Lahir Ibu</label>
														<input type="date" name="ftgllahiribu" value="<?= $surat_akte['ibu_tgl_lahir'] ?>" class="form-control" tabindex="19">

													</div>
													<!-- /.form-group -->
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label>Umur Ibu</label>
														<input type="text" name="fumuribu" value="<?= $surat_akte['ibu_umur'] ?>" class="form-control" tabindex="20" readonly>
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="form-group">
												<label>Pekerjaan Ibu</label>
												<input type="text" name="fpekerjaanibu" value="<?= $surat_akte['ibu_pekerjaan'] ?>" class="form-control" tabindex="21">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Ibu</label>
												<input type="text" name="falamatibu" value="<?= $surat_akte['ibu_alamat'] ?>" class="form-control" tabindex="22">
											</div>
											<!-- /.form-group -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Kewarganegaraan Ibu</label>
														<select class="form-control select2" style="width: 100%;" name="fwniibu" tabindex="23">
															<option value="">-- Pilih --</option>
															<option value="WNI" <?php if ($surat_akte['ibu_kwn'] == 'WNI') {
																					echo "selected";
																				} ?>>WNI</option>
															<option value="WNA" <?php if ($surat_akte['ibu_kwn'] == 'WNA') {
																					echo "selected";
																				} ?>>WNA</option>
														</select>
														<!-- <input type="text" name="fwniibu" class="form-control" tabindex="23"> -->
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Kebangsaan Ibu</label>
														<input type="text" name="fbangsaibu" value="<?= $surat_akte['ibu_kebangsaan'] ?>" class="form-control" tabindex="24">
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Pencatatan Perkawinan Ibu</label>
														<input type="date" name="fkawinibu" value="<?= $surat_akte['ibu_tgl_kawin'] ?>" class="form-control" tabindex="25">

													</div>
													<!-- /.form-group -->
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label>Umur Ibu Saat Perkawinan</label>
														<input type="text" name="fumurkwibu" value="<?= $surat_akte['ibu_umur_kawin'] ?>" class="form-control" tabindex="26" readonly>
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<br>
											<div class="form-group">
												<label>DATA AYAH :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>NIK Ayah</label>
												<input type="text" name="fnikayah" value="<?= $surat_akte['ayah_nik'] ?>" class="form-control" tabindex="27">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap Ayah</label>
												<input type="text" name="fnamaayah" value="<?= $surat_akte['ortu_nama_ayah'] ?>" class="form-control" tabindex="28">
											</div>
											<!-- /.form-group -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Tanggal Lahir Ayah</label>
														<input type="date" name="ftgllahirayah" value="<?= $surat_akte['ayah_tgl_lahir'] ?>" class="form-control" tabindex="29">

													</div>
													<!-- /.form-group -->
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label>Umur Ayah</label>
														<input type="text" name="fumurayah" value="<?= $surat_akte['ayah_umur'] ?>" class="form-control" tabindex="30" readonly>
													</div>
													<!-- /.form-group -->
												</div>
											</div>

											<div class="form-group">
												<label>Pekerjaan Ayah</label>
												<input type="text" name="fpekerjaanayah" value="<?= $surat_akte['ayah_pekerjaan'] ?>" class="form-control" tabindex="31">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Ayah</label>
												<input type="text" name="falamatayah" value="<?= $surat_akte['ayah_alamat'] ?>" class="form-control" tabindex="32">
											</div>
											<!-- /.form-group -->

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label>Kewarganegaraan Ayah</label>
														<select class="form-control select2" style="width: 100%;" name="fwniayah" tabindex="33">
															<option value="">-- Pilih --</option>
															<option value="WNI" <?php if ($surat_akte['ayah_kwn'] == 'WNI') {
																					echo "selected";
																				} ?>>WNI</option>
															<option value="WNA" <?php if ($surat_akte['ayah_kwn'] == 'WNA') {
																					echo "selected";
																				} ?>>WNA</option>
														</select>
														<!-- <input type="text" name="fwniayah" class="form-control" tabindex="33"> -->
													</div>
													<!-- /.form-group -->
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>Kebangsaan Ayah</label>
														<input type="text" name="fbangsaayah" value="<?= $surat_akte['ayah_kebangsaan'] ?>" class="form-control" tabindex="34">
													</div>
													<!-- /.form-group -->
												</div>
											</div>
											<br>
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											<div class="form-group">
												<label>DATA PELAPOR :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>NIK Pelapor</label>
												<input type="text" name="fnikpelapor" value="<?= $surat_akte['plp_nik'] ?>" class="form-control" tabindex="35">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap Pelapor</label>
												<input type="text" name="fnamapelapor" value="<?= $surat_akte['plp_nama'] ?>" class="form-control" tabindex="36">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Umur Pelapor</label>
												<input type="number" name="fumurpelapor" value="<?= $surat_akte['plp_umur'] ?>" class="form-control" tabindex="37">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Jenis Kelamin Pelapor</label>
												<select class="form-control select2" style="width: 100%;" name="fjkelpelapor" tabindex="38">
													<option value="">--- Pilih ---</option>
													<option value="L" <?php if ($surat_akte['plp_gender'] == 'L') {
																			echo "selected";
																		} ?>>Laki-laki</option>
													<option value="P" <?php if ($surat_akte['plp_gender'] == 'P') {
																			echo "selected";
																		} ?>>Perempuan</option>
												</select>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Pekerjaan Pelapor</label>
												<input type="text" name="fpekerjaanpelapor" value="<?= $surat_akte['plp_pekerjaan'] ?>" class="form-control" tabindex="39">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Pelapor</label>
												<input type="text" name="falamatpelapor" value="<?= $surat_akte['plp_alamat'] ?>" class="form-control" tabindex="40">
											</div>
											<!-- /.form-group -->

											<br>
											<div class="form-group">
												<label>DATA SAKSI 1 :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>NIK Saksi 1</label>
												<input type="text" name="fniksaksi1" value="<?= $surat_akte['saksi1_nik'] ?>" class="form-control" tabindex="41">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap Saksi 1</label>
												<input type="text" name="fnamasaksi1" value="<?= $surat_akte['saksi_nama_1'] ?>" class="form-control" tabindex="42">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Umur Saksi 1</label>
												<input type="number" name="fumursaksi1" value="<?= $surat_akte['saksi1_umur'] ?>" class="form-control" tabindex="43">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Pekerjaan Saksi 1</label>
												<input type="text" name="fpekerjaansaksi1" value="<?= $surat_akte['saksi1_pekerjaan'] ?>" class="form-control" tabindex="44">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Saksi 1</label>
												<input type="text" name="falamatsaksi1" value="<?= $surat_akte['saksi1_alamat'] ?>" class="form-control" tabindex="45">
											</div>
											<!-- /.form-group -->

											<br>
											<div class="form-group">
												<label>DATA SAKSI 2 :</label>
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>NIK Saksi 2</label>
												<input type="text" name="fniksaksi2" value="<?= $surat_akte['saksi2_nik'] ?>" class="form-control" tabindex="46">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Nama Lengkap Saksi 2</label>
												<input type="text" name="fnamasaksi2" value="<?= $surat_akte['saksi_nama_2'] ?>" class="form-control" tabindex="47">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Umur Saksi 2</label>
												<input type="number" name="fumursaksi2" value="<?= $surat_akte['saksi2_umur'] ?>" class="form-control" tabindex="48">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Pekerjaan Saksi 2</label>
												<input type="text" name="fpekerjaansaksi2" value="<?= $surat_akte['saksi2_pekerjaan'] ?>" class="form-control" tabindex="49">
											</div>
											<!-- /.form-group -->
											<div class="form-group">
												<label>Alamat Saksi 2</label>
												<input type="text" name="falamatsaksi2" value="<?= $surat_akte['saksi2_alamat'] ?>" class="form-control" tabindex="50">
											</div>
											<!-- /.form-group -->
										</div>
										<!-- /.col -->
									</div>
									<!-- /.row -->
								</div>
								<div class="box-footer">
									<input class="btn btn-primary" type="submit" name="btnSimpanF2Akte" value="Simpan" tabindex="51" />
									<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="52">Kembali</a>
								</div>
							</form>
							<!-- /.box-body -->
							<!-- </div> -->
							<!-- /.box -->
						</div>
						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div>
				<!-- nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
