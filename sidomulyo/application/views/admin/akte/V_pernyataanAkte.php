<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pernyataan Akte Kelahiran</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
			<!-- /.box-header -->
			<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Progress Pengisian Form</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="progress">
							<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="<?= $surat['progress_pembuatan']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $surat['progress_pembuatan']; ?>%">
								<span class="sr-only">60% Complete (success)</span>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (left) -->
		</div>

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Pernyataan Akte Kelahiran</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('proses-simpan-pernyataan-akte') ?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Pemohon</label>
								<input type="text" name="fnamapemohon" class="form-control" value="<?= $surat['nama_pemohon']; ?>" tabindex="4" readonly>
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir Pemohon</label>
										<input type="text" name="ftmptlahirpemohon" class="form-control" tabindex="5">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Pemohon</label>
										<input type="date" name="ftgllahirpemohon" class="form-control" tabindex="6">
									</div>
									<!-- /.form-group -->
								</div>
							</div>


							<div class="form-group">
								<label>Pekerjaan Pemohon</label>
								<input type="text" name="fpekerjaanpemohon" class="form-control" tabindex="7">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Pemohon</label>
								<input type="text" name="falamatpemohon" class="form-control" tabindex="8">
							</div>
							<!-- /.form-group -->

							<br>

							<div class="form-group">
								<label>DATA ANAK : </label>
							</div>
							<!-- /.form-group -->
							<div class="form-group" style="padding-top:15px">
								<label>Nama Anak</label>
								<input type="text" class="form-control" name="fnamaanak" value="<?= $pengantar['ank_nama']; ?>" tabindex="9" readonly>
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir Anak</label>
										<input type="text" class="form-control" name="ftmptlahiranak" value="<?= $pengantar['ank_tempat_lahir']; ?>" tabindex="10" readonly>
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Anak</label>
										<input type="date" class="form-control" name="ftgllahiranak" value="<?= $pengantar['ank_tgl_lahir']; ?>" tabindex="11" readonly>
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>Anak ke-</label>
								<input type="number" class="form-control" name="fanakke" value="<?= $pengantar['ank_anak_ke']; ?>" tabindex="12" readonly>
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Saksi 1</label>
								<input type="text" name="fnamasaksi1" class="form-control" tabindex="13" value="<?= $pengantar['ortu_nama_saksi1']; ?>" readonly>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Saksi 1</label>
								<input type="text" name="falamatsaksi1" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Status Hubungan Saksi 1 dengan Bayi</label>
								<input type="text" name="fhubsaksi1" class="form-control" tabindex="15">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Saksi 2</label>
								<input type="text" name="fnamasaksi2" class="form-control" value="<?= $pengantar['ortu_nama_saksi2']; ?>" tabindex="16" readonly>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat Saksi 2</label>
								<input type="text" name="falamatsaksi2" class="form-control" tabindex="17">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Status Hubungan Saksi 2 dengan Bayi</label>
								<input type="text" name="fhubsaksi2" class="form-control" tabindex="18">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanPernyataanAkte" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-akte-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
