<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-database"></i> Data Desa</a></li>
			<li><a href="#">Produk Unggulan</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Produk Unggulan</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('edit-produkunggulan') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="fid" value="<?php echo $produk->id_pr ?>" />
							<div class="form-group">
								<label>Nama Produk</label>
								<input type="text" class="form-control <?php echo form_error('fnama') ? 'is-invalid' : '' ?>" name="fnama" value="<?php echo $produk->nama_pr ?>" tabindex="1">
								<span style="color: red">
									<?php echo form_error('fnama') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>UMKM</label>
								<select class="form-control select2 <?php echo form_error('fumkm') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fumkm" tabindex="2">
									<option value="<?php echo $produk->umkm_pr ?>"><?php echo $produk->nama_umkm ?></option>
									<?php foreach ($umkm as $um) : ?>
										<option value="<?php echo $um->id_umkm; ?>"><?php echo $um->nama_umkm; ?></option>
									<?php endforeach; ?>
								</select>
								<span style="color: red">
									<?php echo form_error('fumkm') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Upload Ulang Gambar / </label>
								<i class="fa fa-eye"></i>
								<a href="<?php echo base_url('assets/upload/produkunggulan/' . $produk->foto_produk) ?>" target="_blank">Lihat Gambar</a>
								<input type="file" name="ffoto" class="form-control" tabindex="3">
								<input type="hidden" name="ffotolama" value="<?php echo $produk->foto_produk ?>" />
								<!-- <span style="color: red">
                    <?php //echo form_error('ffoto') 
										?>
                  </span> -->
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="4" />
					<a href="<?php echo site_url('produkunggulan-admin') ?>" class="btn btn-default" tabindex="5">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
