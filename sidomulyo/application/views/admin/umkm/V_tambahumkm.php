<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-database"></i> Data Desa</a></li>
			<li><a href="#">UMKM</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah UMKM</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('tambah-umkm') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Usaha</label>
								<input type="text" class="form-control" name="fnama" tabindex="1">
								<span style="color: red">
									<?php echo form_error('fnama') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" class="form-control" name="falamat" placeholder="Contoh: Dusun A RT 001 RW 001" tabindex="2">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nomor Telepon</label>
								<input type="text" class="form-control" name="ftelp" tabindex="3">
								<span style="color: red">
									<?php echo form_error('ftelp') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Pemilik</label>
								<input type="text" class="form-control" name="fpemilik" tabindex="4">
								<span style="color: red">
									<?php echo form_error('fpemilik') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Foto</label>
								<input type="file" name="ffoto" class="form-control" tabindex="5">
								<!-- <span style="color: red">
                    <?php //echo form_error('ffoto') 
										?>
                  </span> -->
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="6" />
					<a href="<?php echo site_url('umkm-admin') ?>" class="btn btn-default" tabindex="7">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
