<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">Statistik</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Statistik</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_master/prosesedit_statistik') ?>" method="post">
				<div class="box-body">
					<?php foreach ($statistik as $key => $value) : ?>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Kategori</label>
									<input type="hidden" name="id_statistik" id="id_statistik" value="<?= $value->id_statistik ?>" readonly required>
									<input type="text" class="form-control" name="kategori_statistik" id="kategori_statistik" value="<?= $value->judul_statistik ?>" tabindex="4" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Parameter</label>
									<input type="text" class="form-control" name="parameter_statistik" id="parameter_statistik" value="<?= $value->parameter_statistik ?>" tabindex="4" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Nilai</label>
									<input type="text" class="form-control" name="nilai_statistik" id="nilai_statistik" tabindex="4" value="<?= $value->nilai_statistik ?>" required>
								</div>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanInfo" value="Simpan" />
					<a href="<?php echo site_url('statistik-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
