<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-tv"></i> Master</a></li>
			<li class="active">Data Statistik</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if (isset($_SESSION['type'])) { ?>
					<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $_SESSION['isi']; ?>
					</div>
					<?php
					unset($_SESSION['type']);
					unset($_SESSION['isi']);
					unset($_SESSION['judul']);
					?>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Data Statistik</h3>
						<!-- <h3 class="box-title"><a href="<?php //echo site_url('tambah-statistik') 
																								?>" class="btn btn-block btn-primary">Tambah Data</a></h3> -->
					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Kategori</th>
										<th>Parameter</th>
										<th>Nilai</th>
										<th style="width:50px">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($statistik as $val) : ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo $val->judul_statistik; ?></td>
											<td><?php echo $val->parameter_statistik; ?></td>
											<td><?php echo $val->nilai_statistik; ?></td>
											<td>
												<a href="<?php echo site_url('edit-statistik/' . $val->id_statistik) ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-statistik/' . $val->id_statistik) ?>')" href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>

								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Kategori</th>
										<th>Parameter</th>
										<th>Nilai</th>
										<th style="width:50px">Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>
