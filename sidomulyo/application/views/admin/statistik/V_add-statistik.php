<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">User</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah User</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_master/save_statistik') ?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Kategori</label>
								<input type="text" class="form-control" name="kategori_statistik" id="kategori_statistik" tabindex="4" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<input class="btn btn-danger" type="button" name="addPegawai" onclick="tambahFoto()" value="Tambah" style="margin-top: 25px" />
						</div>
					</div>

					<table id="dataFotoAdd" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Parameter</th>
								<th>Nilai</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanInfo" value="Simpan" />
					<a href="<?php echo site_url('statistik-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

	function tambahFoto() {
		var tbProduk = $('#dataFotoAdd').DataTable();
		var indexed = 0;

		if (tbProduk.rows().count() > 0) { // jika ada isi

			var data = tbProduk.rows().data();
			data.each(function(value, index) {
				indexed = indexed + 1;
			});

			tbProduk.row.add([
				'<div class="form-group"><input type="text" class="form-control" name="parameter_statistik[]" id="parameter_statistik" tabindex="4" required></div>',
				'<div class="form-group"><input type="text" class="form-control" name="nilai_statistik[]" id="nilai_statistik" tabindex="4" required></div>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_foto(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		} else {
			tbProduk.row.add([
				'<div class="form-group"><input type="text" class="form-control" name="parameter_statistik[]" id="parameter_statistik" tabindex="4" required></div>',
				'<div class="form-group"><input type="text" class="form-control" name="nilai_statistik[]" id="nilai_statistik" tabindex="4" required></div>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_foto(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		}

	}

	function hps_foto(index) {
		// hapus row
		var tbProduk = $('#dataFotoAdd').DataTable();
		tbProduk.row(index).remove().draw();
	}

</script>
