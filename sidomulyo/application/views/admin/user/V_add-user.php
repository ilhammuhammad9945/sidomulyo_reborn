<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">User</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah User</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_master/save_user') ?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" name="nama_user" id="nama_user" tabindex="4" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Username</label>
								<input type="text" class="form-control" name="username_user" id="username_user" tabindex="4" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Password</label>
								<input type="text" class="form-control" name="password_user" id="password_user" tabindex="4" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Atur Posisi</label>
								<select class="form-control select2" style="width: 100%;" name="posisi_user" id="posisi_user" tabindex="5" required>
									<option value="">--- Pilih ---</option>
									<option value="2">Admin Pendataan</option>
									<option value="3">Editor Berita</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanInfo" value="Simpan" />
					<a href="<?php echo site_url('user-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
