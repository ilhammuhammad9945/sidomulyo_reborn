<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-tv"></i> Profil</a></li>
			<li><a href="#">Berita</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Berita</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_berita/save_berita') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Judul</label>
								<input type="text" class="form-control" name="judul" required>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Foto</label>
								<input type="file" name="foto" class="form-control">
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->

						<div class="col-md-12">
							<div class="form-group">
								<label>Headline Berita (Maksimal 130 karakter)</label>
								<textarea class="form-control" name="headline" rows="5" cols="80" maxlength="250" placeholder="Maksimal 250 karakter"></textarea>
							</div>

						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Isi Berita</label>
								<textarea id="editor1" name="isi" rows="10" cols="80"></textarea>
							</div>

						</div>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-danger" type="submit" name="btnsimpanDraft" value="Simpan sebagai Draft" />
					<input class="btn btn-primary" type="submit" name="btnsimpanShow" value="Simpan & Tampilkan" />
					<a href="<?php echo site_url('admin/C_berita') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
