<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Menu Aktif</h1>
    <ol class="breadcrumb">
      <li><a><i class="fa fa-check-square-o"></i> Data Master</a></li>
      <li><a href="#">Jenis Konten</a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if ($this->session->flashdata('success')) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php endif; ?>
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Jenis Konten</h3>
      </div>
      <!-- /.box-header -->
      <form action="<?php base_url('tambah-jenis-konten') ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nama Jenis</label>
                <input type="text" class="form-control" name="fnama" tabindex="1">
                <span style="color: red">
                  <?php echo form_error('fnama') ?>
                </span>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer">
          <input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="2" />
          <a href="<?php echo site_url('jenis-konten-admin') ?>" class="btn btn-default" tabindex="3">Kembali</a>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->