<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Menu Aktif</h1>
    <ol class="breadcrumb">
      <li><a><i class="fa fa-check-square-o"></i> Data Master</a></li>
      <li><a href="#">Organisasi</a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if ($this->session->flashdata('success')) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php endif; ?>
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Organisasi</h3>
      </div>
      <!-- /.box-header -->
      <form action="<?php base_url('tambah-organisasi') ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nama Organisasi</label>
                <input type="text" class="form-control" name="fnama" tabindex="1">
                <span style="color: red">
                  <?php echo form_error('fnama') ?>
                </span>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Keterangan</label>
                <select class="form-control select2" style="width: 100%;" name="fket" tabindex="2">
                  <option value="">--- Pilih ---</option>
                  <option value="Ket. PKK">Ket. PKK</option>
                  <option value="Asal Karang Taruna">Asal Karang Taruna</option>
                  <option value="Ket. Karang Taruna">Ket. Karang Taruna</option>
                  <option value="Asal Posyandu">Asal Posyandu</option>
                  <option value="Ket. Posyandu">Ket. Posyandu</option>
                  <option value="Asal RW">Asal RW</option>
                  <option value="Asal RT">Asal RT</option>
                  <option value="Asal Karang Werda">Asal Karang Werda</option>
                  <option value="Asal Muslimat">Asal Muslimat</option>
                  <option value="Asal Aisyiyah">Asal Aisyiyah</option>
                  <option value="Asal SSB">Asal Sanggar Seni Budaya</option>
                  <option value="Asal LSM">Asal LSM</option>
                  <option value="Ket. PPKBD">Ket. PPKBD</option>
                </select>
                <span style="color: red">
                  <?php echo form_error('fket') ?>
                </span>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Lain-lain</label>
                <input type="text" class="form-control" name="flain" tabindex="3">
              </div>
              <!-- /.form-group -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer">
          <input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="4" />
          <a href="<?php echo site_url('organisasi-admin') ?>" class="btn btn-default" tabindex="5">Kembali</a>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->