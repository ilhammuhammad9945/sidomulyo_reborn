<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-check-square-o"></i> Data Master</a></li>
			<li><a href="#">Jabatan</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Jabatan</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('edit-jabatan') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="fid" value="<?php echo $jabatan->id_jbt ?>" />
							<div class="form-group">
								<label>Nama Jabatan</label>
								<input type="text" class="form-control <?php echo form_error('fnama') ? 'is-invalid' : '' ?>" name="fnama" value="<?php echo $jabatan->nama_jbt ?>" tabindex="1">
								<span style="color: red">
									<?php echo form_error('fnama') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Keterangan</label>
								<select class="form-control select2" style="width: 100%;" name="fket" tabindex="2">
									<option value="<?php echo $jabatan->keterangan ?>"><?php echo $jabatan->keterangan ?></option>
									<option value="Aparatur Desa">Aparatur Desa</option>
									<option value="Aparatur BPD">Aparatur BPD</option>
									<option value="PKK">PKK</option>
									<option value="LPMD">LPMD</option>
									<option value="Karang Taruna">Karang Taruna</option>
									<option value="Posyandu">Posyandu</option>
									<option value="RT">RT</option>
									<option value="RW">RW</option>
									<option value="Karang Werda">Karang Werda</option>
									<option value="Rumah Desa Sehat">Rumah Desa Sehat</option>
									<option value="Muslimat">Muslimat</option>
									<option value="Aisyiyah">Aisyiyah</option>
									<option value="SSB">Sanggar Seni Budaya</option>
									<option value="LSM">LSM</option>
									<option value="KPMD">KPMD</option>
									<option value="KPM">KPM</option>
									<option value="Kader Teknik">Kader Teknik</option>
									<option value="PPKBD">Kader PPKBD</option>
									<option value="TB">Kader TB</option>
									<option value="Poskesdes">Poskesdes</option>
									<option value="Bumdes">Bumdes</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fket') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="3" />
					<a href="<?php echo site_url('jabatan-admin') ?>" class="btn btn-default" tabindex="4">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
