<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">SKTM</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah SKTM</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('proses-simpan-sktm') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PEMOHON :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Pemohon</label>
								<input type="hidden" class="form-control" name="id_peng" id="id_peng" value="<?= $invoice; ?>" tabindex="4">
								<input type="text" class="form-control" name="sktm_nik" id="sktm_nik" tabindex="4">
							</div>
							<div class="form-group">
								<label>Nama Lengkap Pemohon</label>
								<input type="text" class="form-control" name="sktm_nama_lengkap" id="sktm_nama_lengkap" tabindex="4">
							</div>
							<!-- jenis kelamin  -->
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" name="sktm_gender" id="sktm_gender" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
							<!-- Agama -->
							<div class="form-group">
								<label>Agama</label>
								<input type="text" class="form-control" name="sktm_agama" id="sktm_agama" tabindex="4">
							</div>
							<!-- status perkawinan -->
							<div class="form-group">
								<label>Status Perkawinan</label>
								<input type="text" class="form-control" name="sktm_status_kawin" id="sktm_status_kawin" tabindex="4">
							</div>
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" class="form-control" name="sktm_pekerjaan" id="sktm_pekerjaan" tabindex="4">
							</div>
							<!-- umur -->
							<div class="form-group">
								<label>Umur</label>
								<input type="text" class="form-control" name="sktm_umur" id="sktm_umur" tabindex="4">
							</div>
							<div class="form-group">
								<label>Alamat Lengkap</label>
								<input type="text" class="form-control" name="sktm_alamat" id="sktm_alamat" tabindex="4">
							</div>
							<!-- No KK -->
							<div class="form-group">
								<label>No KK</label>
								<input type="text" class="form-control" name="sktm_no_kk" id="sktm_no_kk" tabindex="4">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Kepala Keluarga</label>
								<input type="text" class="form-control" name="sktm_nama_kepala_keluarga" id="sktm_nama_kepala_keluarga" tabindex="4">
							</div>
							<div class="form-group">
								<label>Keperluan Pembuatan</label>
								<input type="text" class="form-control" name="sktm_keperluan" id="sktm_keperluan" tabindex="4">
							</div>
						</div>


						<div class="col-md-6">
							<div class="form-group">
								<label>FOTO RUMAH :</label>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Foto 1 <i>(Tampak Depan)</i></label>
								<input type="file" name="sktm_foto_1" class="form-control" tabindex="15">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Foto 2 <i>(Tampak Samping)</i></label>
								<input type="file" name="sktm_foto_2" class="form-control" tabindex="16">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Foto 3 <i>(Tampak Belakang)</i></label>
								<input type="file" name="sktm_foto_3" class="form-control" tabindex="17">
							</div>

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>INDIKATOR / KRITERIA :</label>
							</div>
							<table id="tb_indikator" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Indikator</th>
										<th>Kriteria</th>
										<th>Ya</th>
										<th>Tidak</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Luas Lantai Rumah</td>
										<td>40M2</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator1" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator1" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Dinding Rumah</td>
										<td>Bambu/Tembok tanpa plester</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator2" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator2" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>3</td>
										<td>Jenis lantai rumah</td>
										<td>Tanah/plester semen</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator3" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator3" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Penerangan rumah</td>
										<td>Lampu templok / listrik saluran</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator4" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator4" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>5</td>
										<td>Sumber air bersih</td>
										<td>Sungai / Sumber / Sumur</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator5" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator5" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>6</td>
										<td>Tabungan / Simpanan</td>
										<td>Nilainya < Rp. 1.000.000</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator6" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator6" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>7</td>
										<td>Pekerjaan</td>
										<td>Tidak bekerja / tenaga kasar</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator7" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator7" value="0">
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>8</td>
										<td>Jamban Keluarga</td>
										<td>Tidak punya jamban</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator8" value="1">
												</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label>
													<input type="radio" name="indikator8" value="0">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanSktm" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-sktm-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
