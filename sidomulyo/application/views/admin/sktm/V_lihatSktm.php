<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pengantar Akte Kelahiran</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat SKTM</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Nama Pemohon :</h3>
							<h4> <?= $sktm['nama_pemohon']; ?>
								<span class="mailbox-read-time pull-right text-black">Tanggal Pengajuan : <?= format_indo(date("Y-m-d H:i", strtotime($sktm['created_at']))) ?> WIB</span>
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $sktm['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($sktm['pmh_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Agama </td>
										<td>: <?= $sktm['pmh_agama']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $sktm['pmh_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $sktm['pmh_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Umur</td>
										<td>: <?= $sktm['pmh_umur']; ?></td>
									</tr>
									<tr>
										<td>Alamat Lengkap</td>
										<td>: <?= $sktm['pmh_alamat']; ?></td>
									</tr>
									<tr>
										<td>No KK</td>
										<td>: <?= $sktm['pmh_no_kk']; ?></td>
									</tr>
									<tr>
										<td>Nama Kepala Keluarga</td>
										<td>: <?= $sktm['pmh_kepala_keluarga']; ?></td>
									</tr>
									<tr>
										<td>Keperluan Pembuatan </td>
										<td>: <?= $sktm['pmh_kebutuhan']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<h4>Foto Rumah :</h3>
							<ul class="mailbox-attachments clearfix">

								<?php foreach ($foto as $key => $value) : ?>
									<li>
										<span class="mailbox-attachment-icon has-img"><img src="<?php echo base_url(); ?>assets/upload/layanan/sktm/<?= $value->foto_sktm ?>" alt="Attachment"></span>

										<div class="mailbox-attachment-info">

											<span class="mailbox-attachment-size">
												<?php if ($value->keterangan_foto == 1) { ?>
													<a href="<?php echo base_url(); ?>assets/upload/layanan/sktm/<?= $value->foto_sktm ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i> Tampak Depan </a>
												<?php } elseif ($value->keterangan_foto == 2) { ?>
													<a href="<?php echo base_url(); ?>assets/upload/layanan/sktm/<?= $value->foto_sktm ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i> Tampak Samping </a>
												<?php	} else { ?>
													<a href="<?php echo base_url(); ?>assets/upload/layanan/sktm/<?= $value->foto_sktm ?>" class="mailbox-attachment-name" target="_blank"><i class="fa fa-camera"></i> Tampak Belakang </a>
												<?php 	} ?>
											</span>
										</div>
									</li>
								<?php endforeach; ?>

							</ul>
					</div>
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-sktm/' . $sktm['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak SKTM</a>
						<a href="<?php echo site_url('print-sktm-domisili/' . $sktm['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak SKTM Domisili</a>
						<a href="<?php echo site_url('print-sktm-foto/' . $sktm['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak SKTM Foto</a>
						<a href="<?php echo site_url('layanan-sktm-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
