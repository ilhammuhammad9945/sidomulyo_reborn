<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-tv"></i> Profil</a></li>
			<li><a href="#">Berita</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Berita</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_berita/edit_berita') ?>" method="post" enctype="multipart/form-data">
				<?php foreach ($berita as $key => $value) { ?>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Judul</label>
									<input type="hidden" value="<?= $value->id_berita; ?>" name="id_berita" readonly required>
									<input type="text" class="form-control" name="judul" value="<?= $value->judul; ?>" required>
								</div>
								<!-- /.form-group -->
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Foto</label>
									<input type="file" name="foto" class="form-control">
									<p class="help-block"><i><?= $value->gambar; ?></i> <a href="<?= site_url('admin/C_berita/preview/' . $value->gambar) ?>" target="_blank">Lihat</a></p>
								</div>
								<!-- /.form-group -->
							</div>
							<!-- /.col -->

							<div class="col-md-12">
								<div class="form-group">
									<label>Headline Berita (Maksimal 130 karakter)</label>
									<textarea class="form-control" name="headline" rows="5" cols="80" placeholder="Maksimal 250 karakter"><?= $value->headline; ?></textarea>
								</div>

							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label>Isi Berita</label>
									<textarea id="editor1" name="isi" rows="10" cols="80" required><?= $value->isi; ?></textarea>
								</div>

							</div>
						</div>
						<!-- /.row -->
					</div>
				<?php } ?>

				<div class="box-footer">
					<input class="btn btn-danger" type="submit" name="btnsimpanDraft" value="Simpan sebagai Draft" />
					<input class="btn btn-primary" type="submit" name="btnsimpanShow" value="Simpan & Tampilkan" />
					<a href="<?php echo site_url('admin/C_berita') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
	function maksKarakter() {
		console.log("heh");
		var maxlength = 100;
		var length = document.getElementById("editor1").value.length;
		if (length > maxlength) {
			console.log("h");
			var txt = document.getElementById("editor1");
			txt.parentNode.innerHTML = txt.parentNode.innerHTML + "<p style='color:red'>Melebihi 100 karakter</p>";
		}
	}
</script>
