<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Menu Aktif</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-th-large"></i> Konten</a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if ($this->session->flashdata('success')) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php endif; ?>
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Konten</h3>
      </div>
      <!-- /.box-header -->
      <form action="<?php base_url('tambah-konten') ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Judul</label>
                <input type="text" name="fjudul" class="form-control" tabindex="1">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Jenis</label>
                <select class="form-control select2" style="width: 100%;" name="fjenis" tabindex="2">
                  <option value="">--- Pilih ---</option>
                  <?php foreach ($jenis as $jn) : ?>
                    <option value="<?php echo $jn->id_jenis; ?>"><?php echo $jn->nama_jenis; ?></option>
                  <?php endforeach; ?>
                </select>
                <span style="color: red">
                  <?php echo form_error('fjenis') ?>
                </span>
              </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="ffoto" class="form-control" tabindex="3">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label>Paragraf Isi</label>
                <textarea id="editor1" name="fisi" rows="10" cols="80" tabindex="4"></textarea>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer">
          <input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="5" />
          <a href="<?php echo site_url('konten-admin') ?>" class="btn btn-default" tabindex="6">Kembali</a>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->