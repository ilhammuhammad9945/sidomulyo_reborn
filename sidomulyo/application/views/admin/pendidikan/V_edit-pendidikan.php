<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-tv"></i> Data Desa</a></li>
			<li><a href="#">Pendidikan</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Pendidikan</h3>
			</div>
			<!-- /.box-header -->
			<?php foreach ($parent as $key => $value) { ?>

				<form action="<?= base_url('admin/C_datadesa/proses_edit_pendidikan') ?>" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama Sekolah</label>
									<input type="hidden" value="<?= $value->id_pend; ?>" name="id_pendidikan" readonly>
									<input type="text" class="form-control" name="nama_sekolah" value="<?= $value->nama_sekolah; ?>" required>
								</div>
								<!-- /.form-group -->
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Kategori Jenjang</label>
									<select class="form-control select2" style="width: 100%;" name="kategori_jenjang" required>
										<option value="">--- Pilih ---</option>
										<option value="1" <?php if ($value->kat_jenjang == 1) {
    echo "selected";
} ?>)>PAUD</option>
										<option value="2" <?php if ($value->kat_jenjang == 2) {
    echo "selected";
} ?>>TK</option>
										<option value="3" <?php if ($value->kat_jenjang == 3) {
    echo "selected";
} ?>>SD / MI</option>
										<option value="4" <?php if ($value->kat_jenjang == 4) {
    echo "selected";
} ?>>SMP / MTs</option>
										<option value="5" <?php if ($value->kat_jenjang == 5) {
    echo "selected";
} ?>>SMA / MA</option>
										<option value="6" <?php if ($value->kat_jenjang == 6) {
    echo "selected";
} ?>>Pondok Pesantren</option>
										<option value="7" <?php if ($value->kat_jenjang == 7) {
    echo "selected";
} ?>>TPA / TPQ</option>
									</select>
								</div>
							</div>
							<!-- /.col -->
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Visi Misi</label>
									<textarea id="editor1" name="visi_misi" rows="5" cols="80">
                  <?= $value->visi_misi; ?>
                </textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Alamat</label>
									<textarea id="editor2" name="alamat" rows="5" cols="80">
                  <?= $value->alamat_sekolah; ?>
                </textarea>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nomor Telp</label>
									<input type="text" class="form-control" name="telp" value="<?= $value->telp; ?>" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Jumlah Murid</label>
									<input type="number" class="form-control" name="jumlah" value="<?= $value->jumlah_murid; ?>" required>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Foto Utama</label>
									<input type="file" name="foto_utama" class="form-control">
									<p class="help-block"><i><?= $value->foto_utama; ?></i> <a href="<?= site_url('admin/C_datadesa/preview/' . $value->foto_utama) ?>" target="_blank">Lihat</a></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Struktur Organisasi</label>
									<input type="file" name="struktur_organisasi" class="form-control">
									<p class="help-block"><i><?= $value->struktur_organisasi; ?></i> <a href="<?= site_url('admin/C_datadesa/preview/' . $value->struktur_organisasi) ?>" target="_blank">Lihat</a></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Fasilitas</label>
									<textarea id="editor3" name="fasilitas" rows="5" cols="80">
                  <?= $value->fasilitas; ?>
                </textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Prestasi</label>
									<textarea id="editor4" name="prestasi" rows="5" cols="80">
                  <?= $value->prestasi; ?>
                </textarea>
								</div>
							</div>
						</div>

						<div class="alert alert-info alert-dismissible" style="margin-top:30px">
							<h4><i class="icon fa fa-info"></i> Data Pegawai</h4>
							Masukkan pegawai-pegawai sekolah.
						</div>

						<!-- Data Pegawai di Sekolah -->
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" class="form-control" id="nama_pegawai">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Jabatan</label>
									<select class="form-control select2" id="jabatan_pegawai" style="width: 100%;">
										<option value="">--- Pilih ---</option>
										<option value="kepsek">Kepala Sekolah</option>
										<option value="wakepsek">Wakil Kepala Sekolah</option>
										<option value="pengajar">Staf Pengajar</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-danger" type="button" name="addPegawai" onclick="tambahPegawai()" value="Tambah" style="margin-top: 25px" />
							</div>
						</div>

						<table id="dataTransAdd" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Jabatan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($pegawai as $key => $val) : ?>
									<tr>
										<td><?= $val->nama; ?><input type="hidden" value="<?php echo $val->nama; ?>" name="nama_pegawai[]" readonly></td>
										<td><?= $val->jabatan; ?><input type="hidden" value="<?php echo $val->jabatan; ?>" name="jabatan_pegawai[]" readonly></td>
										<td><button type="button" class="btn btn-danger btn-sm" onclick="hps_pegawai(<?= $no++; ?>)" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button></td>
									</tr>
								<?php endforeach; ?>

							</tbody>
						</table>
						<!-- Batas data pegawai -->

						<div class="alert alert-info alert-dismissible" style="margin-top:30px">
							<h4><i class="icon fa fa-info"></i> Data Foto</h4>
							Masukkan foto-foto sekolah.
						</div>

						<!-- Data Pegawai di Sekolah -->
						<div class="row">
							<div class="col-md-4">
								<input class="btn btn-danger" type="button" name="addPegawai" onclick="tambahFoto()" value="Tambah" style="margin-top: 25px" />
							</div>
						</div>

						<table id="dataFotoAdd" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Keterangan</th>
									<th>Form Upload</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($foto as $key => $val) : ?>
									<tr>
										<td>
											<div class="form-group"><input type="text" class="form-control" id="keterangan" name="keterangan[]" value="<?= $val->caption; ?>" required></div>
										</td>
										<td>
											<div class="form-group">
												<input type="file" name="foto_detail[]" class="form-control">
												<p class="help-block"><i><?= $val->foto_detail_sekolah; ?></i> <a href="<?= site_url('admin/C_datadesa/preview/' . $val->foto_detail_sekolah) ?>" target="_blank">Lihat</a></p>
											</div>
										</td>
										<td><button type="button" class="btn btn-danger btn-sm" onclick="hps_foto(<?= $no2++; ?>)" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<!-- Batas data pegawai -->

					</div>
					<div class="box-footer">
						<input class="btn btn-primary" type="submit" name="btnEdtPend" value="Simpan" />
						<a href="<?php echo site_url('admin/C_datadesa/pendidikan') ?>" class="btn btn-default">Kembali</a>
					</div>
				</form>
			<?php } ?>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	function tambahPegawai() {
		var nama = $('#nama_pegawai').val();
		var jabatan = $('#jabatan_pegawai').val();
		var tbProduk = $('#dataTransAdd').DataTable();
		var indexed = 0;

		if (tbProduk.rows().count() > 0) { // jika ada isi

			var data = tbProduk.rows().data();
			data.each(function(value, index) {
				indexed = indexed + 1;
			});

			tbProduk.row.add([
				nama + '<input type="hidden" value="' + nama + '" name="nama_pegawai[]" readonly>',
				jabatan + '<input type="hidden" value="' + jabatan + '" name="jabatan_pegawai[]" readonly>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pegawai(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		} else {
			tbProduk.row.add([
				nama + '<input type="hidden" value="' + nama + '" name="nama_pegawai[]" readonly>',
				jabatan + '<input type="hidden" value="' + jabatan + '" name="jabatan_pegawai[]" readonly>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pegawai(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		}

	}

	function hps_pegawai(index) {
		// hapus row
		var tbProduk = $('#dataTransAdd').DataTable();
		tbProduk.row(index).remove().draw();
	}


	function tambahFoto() {
		var tbProduk = $('#dataFotoAdd').DataTable();
		var indexed = 0;

		if (tbProduk.rows().count() > 0) { // jika ada isi

			var data = tbProduk.rows().data();
			data.each(function(value, index) {
				indexed = indexed + 1;
			});

			tbProduk.row.add([
				'<input type="text" class="form-control" id="keterangan" name="keterangan[]" required>',
				'<div class="form-group"><input type="file" name="foto_detail[]" class="form-control"></div>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_foto(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		} else {
			tbProduk.row.add([
				'<input type="text" class="form-control" id="keterangan" name="keterangan[]" required>',
				'<div class="form-group"><input type="file" name="foto_detail[]" class="form-control"></div>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_foto(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		}

	}

	function hps_foto(index) {
		// hapus row
		var tbProduk = $('#dataFotoAdd').DataTable();
		tbProduk.row(index).remove().draw();
	}
</script>
