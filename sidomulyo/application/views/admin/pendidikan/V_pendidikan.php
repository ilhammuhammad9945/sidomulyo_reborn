<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-tv"></i> Berita</a></li>
			<li class="active">Data Berita</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if (isset($_SESSION['type'])) { ?>
					<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $_SESSION['isi']; ?>
					</div>
					<?php
					unset($_SESSION['type']);
					unset($_SESSION['isi']);
					unset($_SESSION['judul']);
					?>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('admin/C_datadesa/tambah_pendidikan') ?>" class="btn btn-block btn-primary">Tambah Data</a></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Jenjang</th>
										<th>Nama</th>
										<th>Alamat</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($pendidikan as $val) : ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td>
												<?php if ($val->kat_jenjang == 1) {
													echo "PAUD";
												} else if ($val->kat_jenjang == 2) {
													echo "TK";
												} else if ($val->kat_jenjang == 3) {
													echo "SD";
												} else if ($val->kat_jenjang == 4) {
													echo "SMP";
												} else {
													echo "SMA";
												} ?>
											</td>
											<td><?php echo $val->nama_sekolah; ?></td>
											<td>
												<div class="limitText<?= $val->id_pend; ?>"><?php echo $val->alamat_sekolah; ?></div>
											</td>
											<td><a href="<?php echo site_url('admin/C_datadesa/edit_pendidikan/' . $val->id_pend) ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('admin/C_berita/hapus_pendidikan/' . $val->id_pend) ?>')" href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>

								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Jenjang</th>
										<th>Nama</th>
										<th>Alamat</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($pendidikan as $val) : ?>
		var no = "<?= $val->id_pend; ?>";
		var text = $(".limitText" + no).text();
		var len = text.length;

		if (len > 40) {
			$(".limitText" + no).text($(".limitText" + no).text().substr(0, 39) + '... ');
		}
	<?php endforeach; ?>

	function ubahStatusOff(id) {
		var A = document.getElementById("rA" + id);
		var B = document.getElementById("rB" + id);
		var txt = confirm("Apakah Anda yakin untuk menonaktifkan berita ini?");
		if (txt) {

			$.ajax({
				url: '<?php echo base_url('admin/C_berita/prosesStatusOff') ?>',
				method: "POST",
				data: {
					idb: id
				},
				async: false,
				dataType: 'json',
				success: function(data) {

					A.style.display = "none";
					B.style.display = "inline";
				}
			});
		}
		window.location = "<?= base_url('admin/C_berita') ?>";
	}

	function ubahStatusOn(id) {
		var A = document.getElementById("rA" + id);
		var B = document.getElementById("rB" + id);
		var txt = confirm("Apakah Anda yakin untuk mempublish berita ini?");
		if (txt) {

			$.ajax({
				url: '<?php echo base_url('admin/C_berita/prosesStatusOn') ?>',
				method: "POST",
				data: {
					idb: id
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					A.style.display = "inline";
					B.style.display = "none";
				}
			});

		}
		window.location = "<?= base_url('admin/C_berita') ?>";
	}
</script>
