<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">Info</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Info</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_master/prosesedit_setting_surat') ?>" method="post">
				<div class="box-body">
					<?php foreach ($setting_surat as $key => $value) : ?>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Jenis</label>
									<input type="hidden" class="form-control" name="id_setting" id="id_setting" value="<?= $value->id_setting ?>" tabindex="4" required readonly>
									<input type="text" class="form-control" name="jenis" id="jenis" value="<?= $value->jenis ?>" tabindex="4" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" class="form-control" name="keterangan" id="keterangan" value="<?= $value->keterangan ?>" tabindex="4" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Lain - Lain</label>
									<input type="text" class="form-control" name="lainlain" id="lainlain" value="<?= $value->lain_lain ?>" tabindex="4" required>
								</div>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanInfo" value="Simpan" />
					<a href="<?php echo site_url('setting-surat-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
