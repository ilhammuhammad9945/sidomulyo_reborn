<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">Setting Surat</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Setting Surat</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_master/save_setting_surat') ?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Jenis</label>
								<input type="text" class="form-control" name="jenis" id="jenis" tabindex="4" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Keterangan</label>
								<input type="text" class="form-control" name="keterangan" id="keterangan" tabindex="4" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Lain - Lain</label>
								<input type="text" class="form-control" name="lainlain" id="lainlain" tabindex="4" required>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanInfo" value="Simpan" />
					<a href="<?php echo site_url('setting-surat-admin') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
