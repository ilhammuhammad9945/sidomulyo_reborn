<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-balance-scale"></i> Produk Hukum</a></li>
			<li><a href="#">Peraturan Kepala Desa</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Peraturan Kepala Desa</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_produkhukum/save_perkades') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nomor Perkades</label>
								<input type="text" class="form-control" name="nomor" required>
							</div>
							<!-- /.form-group -->
						</div>
					</div>
					<div class="row">
						<div class="col-md-10">
							<div class="form-group">
								<label>Tentang</label>
								<input type="text" class="form-control" name="tentang" required>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>Tahun</label>
								<select class="form-control" name="tahun" required>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal</label>
								<input type="date" class="form-control" name="tanggal" required>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>File</label>
								<input type="file" name="foto" class="form-control">
								<p class="help-block">File di upload dalam format .pdf</p>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanPerdes" value="Simpan" />
					<a href="<?php echo site_url('admin/C_produkhukum/perkades') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
