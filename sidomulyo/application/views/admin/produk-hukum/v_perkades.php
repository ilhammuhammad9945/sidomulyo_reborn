<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-balance-scale"></i> Produk Hukum</a></li>
			<li class="active">Data Peraturan Kepala Desa</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if (isset($_SESSION['type'])) { ?>
					<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $_SESSION['isi']; ?>
					</div>
					<?php
					unset($_SESSION['type']);
					unset($_SESSION['isi']);
					unset($_SESSION['judul']);
					?>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('admin/C_produkhukum/tambah_perkades') ?>" class="btn btn-block btn-primary">Tambah Data</a></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Nomor</th>
										<th>Tahun</th>
										<th>Tanggal</th>
										<th style="width:500px">Tentang</th>
										<th>File</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($perkades as $val) : ?>
										<tr>
											<td><?= $val->nomor_ph ?></td>
											<td><?= $val->tahun_ph; ?></td>
											<td><?= date("d/m/Y", strtotime($val->tgl_ph)); ?></td>
											<td>
												<div class="limitText<?= $val->id_ph; ?>"><?php echo $val->judul_ph; ?></div>
											</td>
											<td><a href="<?= base_url('./assets/upload/ProdukHukum/Perkades/' . $val->file_ph) ?>" target="_blank"> Lihat </a></td>
											<td><a href="<?php echo site_url('admin/C_produkhukum/edit_perkades/' . $val->id_ph) ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('admin/C_produkhukum/hapus_perkades/' . $val->id_ph) ?>')" href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>

								</tbody>
								<tfoot>
									<tr>
										<th>Nomor</th>
										<th>Tahun</th>
										<th>Tanggal</th>
										<th>Tentang</th>
										<th>File</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>
