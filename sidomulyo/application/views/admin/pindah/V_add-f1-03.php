<script src="<?php echo base_url()?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">F.1-03</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
			<!-- /.box-header -->
			<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div> -->

		<!-- <div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Form F.1-03 (Formulir Biodata Surat Keterangan Pindah Datang WNI)</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_pindah/prosesSimpanF103') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<h4><b>DATA WILAYAH</b></h4>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Provinsi</label>
								<input type="hidden" name="id_peng" class="form-control" value="<?= $invoice; ?>" tabindex="4" readonly required>
								<input type="text" name="fnamaprovinsi" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kabupaten / Kota</label>
								<input type="text" name="fnamakabupaten" class="form-control" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kecamatan</label>
								<input type="text" name="fnamakecamatan" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kelurahan / Desa</label>
								<input type="text" name="fnamadesa" class="form-control" tabindex="4">
							</div>
						</div>
					</div>
					<h4><b>DATA KELUARGA</b></h4>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kepala Keluarga</label>
								<input type="text" name="fnamakepalakeluarga" class="form-control" tabindex="4">
							</div>
							<div class="form-group">
								<label>Nomor Kartu Keluarga</label>
								<input type="text" name="fnokkkeluarga" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Alamat Keluarga</label>
								<input type="text" name="falamat" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" class="form-control" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" class="form-control" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-5">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										<label>Kode Pos</label>
										<input type="text" name="fkodepos" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>No Telp</label>
										<input type="text" name="ftelpon" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							</div>
						</div>

						<h4><b>DATA INDIVIDU</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama Lengkap</label>
									<input type="text" name="find_namalengkap" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>No. KTP / NIK</label>
									<input type="text" name="find_noktp" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>Alamat Sebelumnya</label>
									<input type="text" name="find_alamatasal" class="form-control" tabindex="14">
								</div>
								<!-- /.form-group -->
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Desa</label>
											<input type="text" name="find_desa" class="form-control" tabindex="15">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Kecamatan</label>
											<input type="text" name="find_kecamatan" class="form-control" tabindex="16">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5">
										<div class="form-group">
											<label>Kabupaten / Kota</label>
											<input type="text" name="find_kabupaten" class="form-control" tabindex="17">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Provinsi</label>
											<input type="text" name="find_provinsi" class="form-control" tabindex="18">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label>Kode pos</label>
											<input type="text" name="find_kodepos" class="form-control" tabindex="18">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>No Telp</label>
											<input type="text" name="ftelpontujuan" class="form-control" tabindex="17">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Nomor Paspor</label>
											<input type="text" name="find_no_paspor" class="form-control" tabindex="18">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Tgl Berakhir Paspor</label>
											<input type="date" name="find_tglberakhirpaspor" class="form-control" tabindex="18">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Jenis Kelamin</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_gender" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="L">Laki-laki</option>
										<option value="P">Perempuan</option>
									</select>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Tempat Lahir</label>
											<input type="text" name="find_tempatlahir" class="form-control" tabindex="18">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<input type="date" name="find_tanggallahir" class="form-control" tabindex="18">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Umur</label>
									<input type="number" name="find_umur" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Akta Kelahiran / Surat Kenal Lahir</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_aktalahir" tabindex="7" >
										<option value="">--- Pilih ---</option>
										<option value="Ada">Ada</option>
										<option value="Tidak Ada">Tidak Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nomor Akta Kelahiran / Surat Kelahiran</label>
									<input type="text" name="find_noaktalahir" class="form-control" tabindex="18">
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Golongan Darah</label><br>
			                <select class="form-control select2" style="width: 100%;" name="find_goldarah" tabindex="7" required>
												<option value="">--- Pilih ---</option>
												<option value="A">A</option>
												<option value="B">B</option>
												<option value="AB">AB</option>
												<option value="O">O</option>
												<option value="A+">A+</option>
												<option value="A-">A-</option>
												<option value="B+">B+</option>
												<option value="B-">B-</option>
												<option value="O+">O+</option>
												<option value="O-">O-</option>
												<option value="Tidak tahu">Tidak tahu</option>
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Agama</label><br>
											<select class="form-control select2" style="width: 100%;" name="find_agama" tabindex="7" required>
												<option value="">--- Pilih ---</option>
												<option value="Islam">Islam</option>
												<option value="Kristen">Kristen</option>
												<option value="Katolik">Katolik</option>
												<option value="Hindu">Hindu</option>
												<option value="Budha">Budha</option>
												<option value="Kong Hucu">Kong Hucu</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<label>Status Perkawinan</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_statuskawin" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Belum Kawin">Belum Kawin</option>
										<option value="Kawin">Kawin</option>
										<option value="Cerai Hidup">Cerai Hidup</option>
										<option value="Cerai Mati">Cerai Mati</option>
									</select>
								</div>
								<div class="form-group">
									<label>Akta Perkawinan / Buku Nikah</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_aktakawin" tabindex="7" >
										<option value="">--- Pilih ---</option>
										<option value="Ada">Ada</option>
										<option value="Tidak Ada">Tidak Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nomor Akta Perkawinan / Buku Nikah</label>
									<input type="text" name="find_noaktakawin" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Tanggal Perkawinan</label>
	                <input type="date" name="find_tanggalkawin" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Akta Perceraian / Surat Cerai</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_aktacerai" tabindex="7" >
										<option value="">--- Pilih ---</option>
										<option value="Ada">Ada</option>
										<option value="Tidak Ada">Tidak Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nomor Akta Cerai / Surat Cerai</label>
									<input type="text" name="find_noaktacerai" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Tanggal Perceraian</label>
	                <input type="date" name="find_tanggalcerai" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Status Hubungan Dalam Keluarga</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_shdk" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="Kepala Keluarga">Kepala Keluarga</option>
										<option value="Suami">Suami</option>
										<option value="Istri">Istri</option>
										<option value="Anak">Anak</option>
										<option value="Menantu">Menantu</option>
										<option value="Cucu">Cucu</option>
										<option value="Orang Tua">Orang Tua</option>
										<option value="Mertua">Mertua</option>
										<option value="Famili Lain">Famili Lain</option>
										<option value="Pembantu">Pembantu</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>
								<div class="form-group">
									<label>Kelainan Fisik dan Mental</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_cacat" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Tidak Ada">Tidak Ada</option>
										<option value="Ada">Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Penyandang Cacat</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_jeniscacat" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="Cacat Fisik">Cacat Fisik</option>
										<option value="Cacat Netra Buta">Cacat Netra Buta</option>
										<option value="Cacat Rungu Wicara">Cacat Rungu Wicara</option>
										<option value="Cacat Mental Jiwa">Cacat Mental Jiwa</option>
										<option value="Cacat Fisik dan Mental">Cacat Fisik dan Mental</option>
										<option value="Cacat Lainnya">Cacat Lainnya</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pendidikan (Tamat)</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_pendidikan" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Tidak Sekolah">Tidak Sekolah</option>
										<option value="Tidak Tamat SD">Tidak Tamat SD</option>
										<option value="SD">SD</option>
										<option value="SMP">SMP</option>
										<option value="SMA">SMA</option>
										<option value="Diploma I/II">Diploma I/II</option>
										<option value="Diploma II/S">Diploma II/S</option>
										<option value="Diploma IV/S1">Diploma IV/S1</option>
										<option value="Strata II">Strata II</option>
										<option value="Strata III">Strata III</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pekerjaan</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_pekerjaan" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Belum / Tidak Bekerja">Belum / Tidak Bekerja</option>
										<option value="Mengurus Rumah Tangga">Mengurus Rumah Tangga</option>
										<option value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>
										<option value="Pensiunan">Pensiunan</option>
										<option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
										<option value="Tentara Nasional Indonesia">Tentara Nasional Indonesia</option>
										<option value="Kepolisian RI">Kepolisian RI</option>
										<option value="Perdagangan">Perdagangan</option>
										<option value="Petani / Pekebun">Petani / Pekebun</option>
										<option value="Peternak">Peternak</option>
										<option value="Nelayan / Perikanan">Nelayan / Perikanan</option>
										<option value="Industri">Industri</option>
										<option value="Konstruksi">Konstruksi</option>
										<option value="Transportasi">Transportasi</option>
										<option value="Karyawan Swasta">Karyawan Swasta</option>
										<option value="Karyawan BUMN">Karyawan BUMN</option>
										<option value="Karyawan BUMD">Karyawan BUMD</option>
										<option value="Karyawan Honorer">Karyawan Honorer</option>
										<option value="Buruh Harian Lepas">Buruh Harian Lepas</option>
										<option value="Buruh Tani / Perkebunan">Buruh Tani / Perkebunan</option>
										<option value="Buruh Nelayan / Perikanan">Buruh Nelayan / Perikanan</option>
										<option value="Buruh Peternakan">Buruh Peternakan</option>
										<option value="Pembantu Rumah Tangga">Pembantu Rumah Tangga</option>
										<option value="Tukang Cukur">Tukang Cukur</option>
										<option value="Tukang Listrik">Tukang Listrik</option>
										<option value="Tukang Batu">Tukang Batu</option>
										<option value="Tukang Kayu">Tukang Kayu</option>
										<option value="Tukang Sol Sepatu">Tukang Sol Sepatu</option>
										<option value="Tukang Las / Pandai Besi">Tukang Las / Pandai Besi</option>
										<option value="Tukang Jahit">Tukang Jahit</option>
										<option value="Penata Rambut">Penata Rambut</option>
										<option value="Penata Rias">Penata Rias</option>
										<option value="Penata Busana">Penata Busana </option>
										<option value="Mekanik">Mekanik</option>
										<option value="Tukang Gigi">Tukang Gigi</option>
										<option value="Seniman Tabib">Seniman Tabib</option>
										<option value="Tabib">Tabib</option>
										<option value="Pengrajin">Pengrajin</option>
										<option value="Perancang Busana">Perancang Busana</option>
										<option value="Peterjemah">Peterjemah</option>
										<option value="Imam Masjid">Imam Masjid</option>
										<option value="Pendeta">Pendeta</option>
										<option value="Pastur">Pastur</option>
										<option value="Wartawan">Wartawan</option>
										<option value="Ustad / Mubaligh">Ustad / Mubaligh</option>
										<option value="Juru Masak">Juru Masak</option>
										<option value="Promotor Acara">Promotor Acara</option>
										<option value="Anggota DPR-RI">Peternak</option>
										<option value="Anggota DPD">Anggota DPD</option>
										<option value="Anggota BPK">Anggota BPK</option>
										<option value="Presiden">Presiden</option>
										<option value="Wakil Presiden">Wakil Presiden</option>
										<option value="Anggota Mahkamah Konstitusi">Anggota Mahkamah Konstitusi</option>
										<option value="Anggt Kabinet / Kementrian">Anggt Kabinet / Kementrian</option>
										<option value="Duta Besar">Duta Besar</option>
										<option value="Gubernur">Gubernur</option>
										<option value="Wakil Gubernur">Wakil Gubernur</option>
										<option value="Bupati">Bupati</option>
										<option value="Wakil Bupati">Wakil Bupati</option>
										<option value="Wali Kota">Wali Kota</option>
										<option value="Wakil Walikota">Wakil Walikota</option>
										<option value="Anggt DPRD Pro">Anggt DPRD Pro</option>
										<option value="Anggt DPRD Kab/Kota">Anggt DPRD Kab/Kota</option>
										<option value="Dosen">Dosen</option>
										<option value="Guru">Guru</option>
										<option value="Pilot">Pilot</option>
										<option value="Pengacara">Pengacara</option>
										<option value="Notaris">Notaris</option>
										<option value="Arsitek">Arsitek</option>
										<option value="Akuntan">Akuntan</option>
										<option value="Konsultan">Konsultan</option>
										<option value="Dokter">Dokter</option>
										<option value="Bidan">Bidan</option>
										<option value="Perawat">Perawat</option>
										<option value="Apoteker">Apoteker</option>
										<option value="Psikiater / Psikolog">Psikiater / Psikolog</option>
										<option value="Penyiar Televisi">Penyiar Televisi</option>
										<option value="Penyiar Radio">Pendeta</option>
										<option value="Pelaut">Pelaut</option>
										<option value="Peneliti">Peneliti</option>
										<option value="Sopir">Sopir</option>
										<option value="Pialang">Pialang</option>
										<option value="Paranormal">Paranormal</option>
										<option value="Pedagang">Pedagang</option>
										<option value="Perangkat Desa">Perangkat Desa</option>
										<option value="Kepala Desa">Kepala Desa</option>
										<option value="Biarawati">Biarawati</option>
										<option value="Wiraswasta">Wiraswasta</option>
									</select>
								</div>
							</div>
						</div>

						<h4><b>DATA ORANG TUA</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>NIK Ibu</label>
									<input type="text" name="fortu_nikibu" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>Nama Lengkap Ibu</label>
									<input type="text" name="fortu_namaibu" class="form-control" tabindex="14">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>NIK Ayah</label>
									<input type="text" name="fortu_nikayah" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>Nama Lengkap Ayah</label>
									<input type="text" name="fortu_namaayah" class="form-control" tabindex="14">
								</div>
							</div>
						</div>

						<h4><b>DATA ADMINISTRASI</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama Ketua RT</label>
									<input type="text" name="fnamart" class="form-control" tabindex="14">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama Ketua RW</label>
									<input type="text" name="fnamarw" class="form-control" tabindex="14">
								</div>
							</div>
						</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanF103" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

$(function() {
	enable_cb();
	$("#cbTambahNama").click(enable_cb);
});

function enable_cb() {
	if (this.checked) {
		$("#fpengikut").removeAttr("readonly");
		$('#form_pengikut').show();
	} else {
		$("#fpengikut").attr("readonly", true);
		$('#form_pengikut').hide();
	}
}

function tambahPengikut() {
	var nik = $('#fnikpengikut').val();
	var nama = $('#fnamapengikut').val();
	var shdk = $('#fshdkpengikut').val();
	var tbProduk = $('#tbPengikut').DataTable();
	var indexed = 0;

	if (tbProduk.rows().count() > 0) { // jika ada isi

		var data = tbProduk.rows().data();
		data.each(function(value, index) {
			indexed = indexed + 1;
		});

		tbProduk.row.add([
			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
		]).draw(false);

	} else {
		tbProduk.row.add([
			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
		]).draw(false);

	}

	$('#fnikpengikut').val('');
	$('#fnamapengikut').val('');
	$('#fshdkpengikut').find('option:empty');

}

function hps_pengikut(index) {
	// hapus row
	var tbProduk = $('#tbPengikut').DataTable();
	tbProduk.row(index).remove().draw();
}

// function tambahPengikut2() {
// 	var nik = $('#fnikpengikut2').val();
// 	var nama = $('#fnamapengikut2').val();
// 	var shdk = $('#fshdkpengikut2').val();
// 	var tbProduk = $('#tbPengikut2').DataTable();
// 	var indexed = 0;
//
// 	if (tbProduk.rows().count() > 0) { // jika ada isi
//
// 		var data = tbProduk.rows().data();
// 		data.each(function(value, index) {
// 			indexed = indexed + 1;
// 		});
//
// 		tbProduk.row.add([
// 			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
// 			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
// 			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
// 			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut2(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
// 		]).draw(false);
//
// 	} else {
// 		tbProduk.row.add([
// 			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
// 			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
// 			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
// 			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut2(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
// 		]).draw(false);
//
// 	}
//
// 	$('#fnikpengikut').val('');
// 	$('#fnamapengikut').val('');
// 	$('#fshdkpengikut').find('option:empty');
//
// }
//
// function hps_pengikut2(index) {
// 	// hapus row
// 	var tbProduk = $('#tbPengikut2').DataTable();
// 	tbProduk.row(index).remove().draw();
// }

</script>
