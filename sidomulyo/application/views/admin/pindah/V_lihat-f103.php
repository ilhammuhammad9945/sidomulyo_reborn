<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Lihat Form F-1-08</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Form F-1-08</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($f103['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>DATA WILAYAH</td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Provinsi</td>
										<td>: <?= $f103['wil_nama_prov']; ?></td>
									</tr>
									<tr>
										<td>Nama Kabupaten / Kota</td>
										<td>: <?= $f103['wil_nama_kec']; ?></td>
									</tr>
									<tr>
										<td>Nama Kecamatan</td>
										<td>: <?= $f103['wil_nama_kab']; ?></td>
									</tr>
									<tr>
										<td>Nama Desa</td>
										<td>: <?= $f103['wil_nama_desa']; ?></td>
									</tr>
									<tr>
										<td>DATA KELUARGA</td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Kepala Keluarga</td>
										<td>: <?= $f103['kel_nama_kep_kel']; ?></td>
									</tr>
									<tr>
										<td>No. Kartu Keluarga</td>
										<td>: <?= $f103['kel_no_kk']; ?></td>
									</tr>
									<tr>
										<td>Alamat Keluarga</td>
										<td>: <?= $f103['kel_alamat_kel']; ?></td>
									</tr>
									<tr>
										<td>Desa / Kelurahan</td>
										<td>: <?= $f103['kel_desa']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $f103['kel_kec']; ?></td>
									</tr>
									<tr>
										<td>Kab / Kota</td>
										<td>: <?= $f103['kel_kab']; ?></td>
									</tr>
									<tr>
										<td>Kab / Kota</td>
										<td>: <?= $f103['kel_prov']; ?></td>
									</tr>
									<tr>
										<td>Kode Pos</td>
										<td>: <?= $f103['kel_kode_pos']; ?></td>
									</tr>
									<tr>
										<td>No Telp</td>
										<td>: <?= $f103['kel_no_telp']; ?></td>
									</tr>
									<tr>
										<td>DATA INDIVIDU</td>
										<td>:</td>
									</tr>
									<tr>
										<td>Nama Lengkap</td>
										<td>: <?= $f103['ind_nama']; ?></td>
									</tr>
									<tr>
										<td>No. KTP / NIK</td>
										<td>: <?= $f103['ind_nik']; ?></td>
									</tr>
									<tr>
										<td>Alamat Sebelumnya</td>
										<td>: <?= $f103['ind_alamat']; ?></td>
									</tr>
									<tr>
										<td>Desa</td>
										<td>: <?= $f103['ind_desa']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $f103['ind_kec']; ?></td>
									</tr>
									<tr>
										<td>Kabupaten</td>
										<td>: <?= $f103['ind_kab']; ?></td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?= $f103['ind_prov']; ?></td>
									</tr>
									<tr>
										<td>Kode Pos</td>
										<td>: <?= $f103['ind_kode_pos']; ?></td>
									</tr>
									<tr>
										<td>No Telp</td>
										<td>: <?= $f103['ind_no_telp']; ?></td>
									</tr>
									<tr>
										<td>Nomor Paspor</td>
										<td>: <?= $f103['ind_no_paspor']; ?></td>
									</tr>
									<tr>
										<td>Tgl Berakhir Paspor</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($f103['ind_tgl_exp_paspor']))) ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>: <?php if($f103['ind_gender'] == 'L') { echo "Laki-laki"; } else { echo "Perempuan"; } ?></td>
									</tr>
									<tr>
										<td>Tempat, Tanggal Lahir</td>
										<td>: <?= $f103['ind_tempat_lahir']; ?>, <?= format_indo(date('Y-m-d', strtotime($f103['ind_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Umur</td>
										<td>: <?= $f103['ind_umur']; ?></td>
									</tr>
									<tr>
										<td>Akta Kelahiran / Surat Kenal Lahir</td>
										<td>: <?= $f103['ind_akte_lahir']; ?></td>
									</tr>
									<tr>
										<td>No Akta Kelahiran / Surat Kelahiran</td>
										<td>: <?= $f103['ind_no_akte_lahir']; ?></td>
									</tr>
									<tr>
										<td>Golongan Darah</td>
										<td>: <?= $f103['ind_gol_darah']; ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $f103['ind_agama']; ?></td>
									</tr>
									<tr>
										<td>Status</td>
										<td>: <?= $f103['ind_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>Akta Perkawinan / Buku Nikah</td>
										<td>: <?= $f103['ind_akta_kawin']; ?></td>
									</tr>
									<tr>
										<td>No Akta Perkawinan / Buku Nikah</td>
										<td>: <?= $f103['ind_no_akta_kawin']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Perkawinan</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($f103['ind_tgl_kawin']))) ?></td>
									</tr>
									<tr>
										<td>Akta Perceraian / Surat Cerai</td>
										<td>: <?= $f103['ind_akta_cerai']; ?></td>
									</tr>
									<tr>
										<td>No Akta Perceraian / Surat Cerai</td>
										<td>: <?= $f103['ind_no_akta_cerai']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Perceraian</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($f103['ind_tgl_cerai']))) ?></td>
									</tr>
									<tr>
										<td>Status Hubungan Dalam Keluarga</td>
										<td>: <?= $f103['ind_shdk']; ?></td>
									</tr>
									<tr>
										<td>Kelainan Fisik</td>
										<td>: <?= $f103['ind_kelainan_fisik']; ?></td>
									</tr>
									<tr>
										<td>Penyandang Cacat</td>
										<td>: <?= $f103['ind_cacat']; ?></td>
									</tr>
									<tr>
										<td>Pendidikan</td>
										<td>: <?= $f103['ind_pendidikan']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $f103['ind_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>DATA ORANG TUA</td>
										<td>:</td>
									</tr>
									<tr>
										<td>NIK Ibu</td>
										<td>: <?= $f103['ortu_nik_ibu']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Ibu</td>
										<td>: <?= $f103['ortu_nama_ibu']; ?></td>
									</tr>
									<tr>
										<td>NIK Ayah</td>
										<td>: <?= $f103['ortu_nik_ayah']; ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap Ayah</td>
										<td>: <?= $f103['ortu_nama_ayah']; ?></td>
									</tr>
									<tr>
										<td>DATA ADMINISTRASI</td>
										<td></td>
									</tr>
									<tr>
										<td>Nama Ketua RT</td>
										<td>: <?= $f103['adm_ketua_rt']; ?></td>
									</tr>
									<tr>
										<td>Nama Ketua RW</td>
										<td>: <?= $f103['adm_ketua_rw']; ?></td>
									</tr>
								</table>
							</div>

						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-pindah-f103/' . $f103['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak Surat</a>
						<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
