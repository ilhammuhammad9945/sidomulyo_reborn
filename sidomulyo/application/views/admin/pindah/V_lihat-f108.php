<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Lihat Form F-1-08</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Form F-1-08</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($f108['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $f108['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Nomor Kartu Keluarga</td>
										<td>: <?= $f108['f108_no_kk']; ?></td>
									</tr>
									<tr>
										<td>Nama Kepala Keluarga</td>
										<td>: <?= $f108['f108_nama_kep_kel']; ?></td>
									</tr>
									<tr>
										<td>Alamat </td>
										<td>: <?= $f108['f108_alamat_asal']; ?></td>
									</tr>
									<tr>
										<td>Desa</td>
										<td>: <?= $f108['f108_desa_asal']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $f108['f108_kec_asal']; ?></td>
									</tr>
									<tr>
										<td>Kota/Kab</td>
										<td>: <?= $f108['f108_kab_asal']; ?></td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?= $f108['f108_prov_asal']; ?></td>
									</tr>
									<tr>
										<td>Kode Pos</td>
										<td>: <?= $f108['f108_kode_pos_asal']; ?></td>
									</tr>
									<tr>
										<td>No Telp</td>
										<td>: <?= $f108['f108_no_telp_asal']; ?></td>
									</tr>
									<tr>
										<td>Alasan Pindah</td>
										<td>: <?= $f108['f108_alasan_pindah']; ?></td>
									</tr>
									<tr>
										<td>Alamat Tujuan Pindah</td>
										<td>: <?= $f108['f108_alamat_pindah']; ?></td>
									</tr>
									<tr>
										<td>Desa</td>
										<td>: <?= $f108['f108_desa_pindah']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $f108['f108_kec_pindah']; ?></td>
									</tr>
									<tr>
										<td>Kab/Kota</td>
										<td>: <?= $f108['f108_kab_pindah']; ?></td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?= $f108['f108_prov_pindah']; ?></td>
									</tr>
									<tr>
										<td>Kode Pos</td>
										<td>: <?= $f108['f108_kode_pos_pindah']; ?></td>
									</tr>
									<tr>
										<td>No Telp</td>
										<td>: <?= $f108['f108_no_telp_pindah']; ?></td>
									</tr>
									<tr>
										<td>Klasifikasi Pindah</td>
										<td>: <?= $f108['f108_klasifikasi_pindah']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kepindahan</td>
										<td>: <?= $f108['f108_jenis_pindah']; ?></td>
									</tr>
									<tr>
										<td>Status Nomor KK</td>
										<td>: <?= $f108['f108_status_no_kk']; ?></td>
									</tr>
									<tr>
										<td>Status Nomor KK Bagi yang Pindah</td>
										<td>: <?= $f108['f108_status_no_kk_pindah']; ?></td>
									</tr>
									<tr>
										<td>Rencana Tanggal Pindah</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($f108['f108_tgl_pindah']))) ?></td>
									</tr>
									<tr>
										<td>Keluarga Yang Pindah</td>
										<td>: <?= $f108['f108_jml_keluarga_pindah']; ?></td>
									</tr>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header">
											<h3 class="box-title">Detail Pengikut</h3>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table id="tbPengikut" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
													<thead>
														<tr>
															<th>NIK</th>
															<th>Nama</th>
															<th>SHDK</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($detail as $key => $value): ?>
															<tr>
																<td><?= $value->pengikut_nik ?> </td>
																<td><?= $value->pengikut_nama ?> </td>
																<td><?= $value->pengikut_shdk ?> </td>
															</tr>
														<?php endforeach; ?>
													</tbody>
													<tfoot>
														<tr>
															<th>NIK</th>
															<th>Nama</th>
															<th>SHDK</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
									<!-- /.box -->
								</div>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-pindah-f108/' . $f108['id_surat']) ?>" class="btn btn-primary" target="_blank">Cetak Surat</a>
						<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
