<script src="<?php echo base_url()?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">F.1-08</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
			<!-- /.box-header -->
			<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div> -->

		<!-- <div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Form F.1-08</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_pindah/prosesSimpanEditF108') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<h4><b>DATA DAERAH ASAL</b></h4>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nomor Kartu Keluarga</label>
								<input type="hidden" name="id_surat" class="form-control" value="<?= $f108['id_surat'] ?>" tabindex="4" readonly required>
								<input type="text" name="fnokkpemohon" value="<?= $f108['f108_no_kk'] ?>" class="form-control" tabindex="4">
							</div>
							<div class="form-group">
								<label>NIK Kepala Keluarga</label>
								<input type="text" name="fnikpemohon" value="<?= $f108['nik_pemohon'] ?>" class="form-control" tabindex="4">
							</div>
							<div class="form-group">
								<label>Nama Kepala Keluarga</label>
								<input type="text" name="fnamakepalakeluarga" value="<?= $f108['nama_pemohon'] ?>" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" name="falamat" value="<?= $f108['f108_alamat_asal'] ?>" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" value="<?= $f108['f108_desa_asal'] ?>" class="form-control" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" value="<?= $f108['f108_kec_asal'] ?>" class="form-control" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-5">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" value="<?= $f108['f108_kab_asal'] ?>" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" value="<?= $f108['f108_prov_asal'] ?>" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										<label>Kode pos</label>
										<input type="text" name="fkodepos" value="<?= $f108['f108_kode_pos_asal'] ?>" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>No Telp</label>
										<input type="text" name="ftelpon" value="<?= $f108['f108_no_telp_asal'] ?>" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<!-- checkbox -->
							</div>
						</div>

						<h4><b>DATA KEPINDAHAN</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Alasan Pindah</label><br>
	                <select class="form-control select2" style="width: 100%;" name="falasanpindah" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Pekerjaan" <?php if($f108['f108_alasan_pindah'] == 'Pekerjaan') { echo 'selected'; } ?>>Pekerjaan</option>
										<option value="Pendidikan" <?php if($f108['f108_alasan_pindah'] == 'Pendidikan') { echo 'selected'; } ?>>Pendidikan</option>
										<option value="Keamanan" <?php if($f108['f108_alasan_pindah'] == 'Keamanan') { echo 'selected'; } ?>>Keamanan</option>
										<option value="Kesehatan" <?php if($f108['f108_alasan_pindah'] == 'Kesehatan') { echo 'selected'; } ?>>Kesehatan</option>
										<option value="Perumahan" <?php if($f108['f108_alasan_pindah'] == 'Perumahan') { echo 'selected'; } ?>>Perumahan</option>
										<option value="Keluarga" <?php if($f108['f108_alasan_pindah'] == 'Keluarga') { echo 'selected'; } ?>>Keluarga</option>
										<option value="Lainnya" <?php if($f108['f108_alasan_pindah'] == 'Lainnya') { echo 'selected'; } ?>>Lainnya</option>
									</select>
								</div>
								<div class="form-group">
									<label>Alamat Pindah</label>
									<input type="text" name="falamatpindah" value="<?= $f108['f108_alamat_pindah'] ?>" class="form-control" tabindex="14">
								</div>
								<!-- /.form-group -->
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Desa</label>
											<input type="text" name="fdesatujuan" value="<?= $f108['f108_desa_pindah'] ?>" class="form-control" tabindex="15">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Kecamatan</label>
											<input type="text" name="fkecamatantujuan" value="<?= $f108['f108_kec_pindah'] ?>" class="form-control" tabindex="16">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5">
										<div class="form-group">
											<label>Kabupaten / Kota</label>
											<input type="text" name="fkabkotatujuan" value="<?= $f108['f108_kab_pindah'] ?>" class="form-control" tabindex="17">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Provinsi</label>
											<input type="text" name="fprovinsitujuan" value="<?= $f108['f108_prov_pindah'] ?>" class="form-control" tabindex="18">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label>Kode pos</label>
											<input type="text" name="fkodepostujuan" value="<?= $f108['f108_kode_pos_pindah'] ?>" class="form-control" tabindex="18">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>No Telp</label>
											<input type="text" name="ftelpontujuan" value="<?= $f108['f108_no_telp_pindah'] ?>" class="form-control" tabindex="17">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<label>Klasifikasi Pindah</label><br>
	                <select class="form-control select2" style="width: 100%;" name="fklasifikasipindah" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Dalam Satu Desa / Kelurahan" <?php if($f108['f108_klasifikasi_pindah'] == 'Dalam Satu Desa / Kelurahan') { echo 'selected'; } ?>>Dalam Satu Desa / Kelurahan</option>
										<option value="Antar Desa / Kelurahan" <?php if($f108['f108_klasifikasi_pindah'] == 'Antar Desa / Kelurahan') { echo 'selected'; } ?>>Antar Desa / Kelurahan</option>
										<option value="Antar Kecamatan" <?php if($f108['f108_klasifikasi_pindah'] == 'Antar Kecamatan') { echo 'selected'; } ?>>Antar Kecamatan</option>
										<option value="Antar Kota / Kabupaten" <?php if($f108['f108_klasifikasi_pindah'] == 'Antar Kota / Kabupaten') { echo 'selected'; } ?>>Antar Kota / Kabupaten</option>
										<option value="Antar Provinsi" <?php if($f108['f108_klasifikasi_pindah'] == 'Antar Provinsi') { echo 'selected'; } ?>>Antar Provinsi</option>
									</select>
								</div>
								<div class="form-group">
									<label>Jenis Pindah</label><br>
	                <select class="form-control select2" style="width: 100%;" name="fjenispindah" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Kepala Keluarga" <?php if($f108['f108_jenis_pindah'] == 'Kepala Keluarga') { echo 'selected'; } ?>>Kepala Keluarga</option>
										<option value="Kep. Keluarga dan Seluruh Angg. Keluarga" <?php if($f108['f108_jenis_pindah'] == 'Kep. Keluarga dan Seluruh Angg. Keluarga') { echo 'selected'; } ?>>Kep. Keluarga dan Seluruh Angg. Keluarga</option>
										<option value="Kep. Keluarga dan Anggota Keluarga" <?php if($f108['f108_jenis_pindah'] == 'Kep. Keluarga dan Anggota Keluarga') { echo 'selected'; } ?>>Kep. Keluarga dan Anggota Keluarga</option>
										<option value="Anggota Keluarga" <?php if($f108['f108_jenis_pindah'] == 'Anggota Keluarga') { echo 'selected'; } ?>>Anggota Keluarga</option>
									</select>
								</div>
								<div class="form-group">
									<label>Status Nomor KK</label><br>
	                <select class="form-control select2" style="width: 100%;" name="fstatusnokk" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Numpang KK" <?php if($f108['f108_status_no_kk'] == 'Numpang KK') { echo 'selected'; } ?>>Numpang KK</option>
										<option value="Membuat KK Baru" <?php if($f108['f108_status_no_kk'] == 'Membuat KK Baru') { echo 'selected'; } ?>>Membuat KK Baru</option>
										<option value="Tidak ada Angg. Keluarga yang Ditinggal" <?php if($f108['f108_status_no_kk'] == 'Tidak ada Angg. Keluarga yang Ditinggal') { echo 'selected'; } ?>>Tidak ada Angg. Keluarga yang Ditinggal</option>
										<option value="Nomor KK Tetap" <?php if($f108['f108_status_no_kk'] == 'Nomor KK Tetap') { echo 'selected'; } ?>>Nomor KK Tetap</option>
									</select>
								</div>
								<div class="form-group">
									<label>Status Nomor KK bagi yang Pindah</label><br>
	                <select class="form-control select2" style="width: 100%;" name="fstatusnokkpindah" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Numpang KK" <?php if($f108['f108_status_no_kk_pindah'] == 'Numpang KK') { echo 'selected'; } ?>>Numpang KK</option>
										<option value="Membuat KK Baru" <?php if($f108['f108_status_no_kk_pindah'] == 'Membuat KK Baru') { echo 'selected'; } ?>>Membuat KK Baru</option>
										<option value="Tidak ada Angg. Keluarga yang Ditinggal" <?php if($f108['f108_status_no_kk_pindah'] == 'Tidak ada Angg. Keluarga yang Ditinggal') { echo 'selected'; } ?>>Tidak ada Angg. Keluarga yang Ditinggal</option>
										<option value="Nomor KK Tetap" <?php if($f108['f108_status_no_kk_pindah'] == 'Nomor KK Tetap') { echo 'selected'; } ?>>Nomor KK Tetap</option>
									</select>
								</div>
								<div class="form-group">
									<label>Rencana Tanggal Pindah</label>
	                <input type="date" name="ftglpindah" value="<?= $f108['f108_tgl_pindah'] ?>" class="form-control" tabindex="18">
								</div>
							</div>
						</div>
					<!-- /.row -->

					<div class="checkbox">
						<label style="font-size:12pt;padding-top:20px">
							<input type="checkbox" name="tambahNama" id="cbTambahNama" style="margin-right:5px"> Centang jika ingin merubah data pengikut (Jika tidak, maka abaikan saja).
						</label>
					</div>
					<div class="form-group">
						<label>Keluarga yang Pindah</label>
						<input type="number" name="fjmlkeluargapindah" value="<?= $f108['f108_jml_keluarga_pindah'] ?>" class="form-control" tabindex="18">
					</div>
					<div id="form_pengikut" style="display:none">
						<div class="alert alert-info alert-dismissible" style="margin-top:30px">
							<h4><i class="icon fa fa-info"></i> Tambah Pengikut</h4>
							Masukkan data pengikut
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>NIK</label>
									<input type="text" id="fnikpengikut" class="form-control" tabindex="4">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" id="fnamapengikut" class="form-control" tabindex="4">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>SHDK</label>
									<select class="form-control select2" style="width: 100%;" id="fshdkpengikut" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="Kepala Keluarga">Kepala Keluarga</option>
										<option value="Suami">Suami</option>
										<option value="Istri">Istri</option>
										<option value="Anak">Anak</option>
										<option value="Menantu">Menantu</option>
										<option value="Cucu">Cucu</option>
										<option value="Orang Tua">Orang Tua</option>
										<option value="Mertua">Mertua</option>
										<option value="Famili Lain">Famili Lain</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>
							</div>
						</div>
	          <input class="btn btn-primary" type="button" name="addPengikut" onclick="tambahPengikut()" value="Tambah" style="margin-top: 25px" />
						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="box-header">
										<h3 class="box-title">Detail Pengikut</h3>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="table-responsive">
											<table id="tbPengikut" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>NIK</th>
														<th>Nama</th>
														<th>SHDK</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($detail as $key => $value): ?>
														<tr>
															<td><?= $value->pengikut_nik ?> <input type="hidden" value="<?= $value->pengikut_nik ?>" name="nik_pengikut[]" readonly></td>
															<td><?= $value->pengikut_nama ?> <input type="hidden" value="<?= $value->pengikut_nama ?>" name="nama_pengikut[]" readonly></td>
															<td><?= $value->pengikut_shdk ?> <input type="hidden" value="<?= $value->pengikut_shdk ?>" name="shdk_pengikut[]" readonly></td>
															<td><button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(<?= $no++; ?>)" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button></td>
														</tr>
													<?php endforeach; ?>
												</tbody>
												<tfoot>
													<tr>
														<th>NIK</th>
														<th>Nama</th>
														<th>SHDK</th>
														<th>Aksi</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
						</div>
					</div>

					<!-- <h4><b>DATA DAERAH TUJUAN</b></h4>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nomor Kartu Keluarga</label>
								<input type="text" name="fnokktujuan" class="form-control" tabindex="14">
							</div>
							<div class="form-group">
								<label>Nama Kepala Keluarga</label>
								<input type="text" name="fnamakktujuan" class="form-control" tabindex="14">
							</div>
							<div class="form-group">
								<label>NIK Kepala Keluarga</label>
								<input type="text" name="fnikkktujuan" class="form-control" tabindex="14">
							</div>
							<div class="form-group">
								<label>Status Nomor KK bagi yang Pindah</label><br>
								<select class="form-control select2" style="width: 100%;" name="fstatusnokk" tabindex="7" required>
									<option value="">- Pilih -</option>
									<option value="1">Numpang KK</option>
									<option value="2">Membuat KK Baru</option>
									<option value="3">Nama Kep. keluarga Nomor KK Tetap</option>
								</select>
							</div>
							<div class="form-group">
								<label>Keluarga yang akan datang</label>
								<input type="number" name="ftujuanke" class="form-control" tabindex="14">
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label>Tanggal Kedatangan</label>
								<input type="date" name="ftujuanke" class="form-control" tabindex="14">
							</div>
							<div class="form-group">
								<label>Alamat Pindah</label>
								<input type="text" name="ftujuanke" class="form-control" tabindex="14">
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" class="form-control" tabindex="15">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" class="form-control" tabindex="16">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" class="form-control" tabindex="17">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" class="form-control" tabindex="18">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="alert alert-info alert-dismissible" style="margin-top:30px">
						<h4><i class="icon fa fa-info"></i> Tambah Pengikut</h4>
						Masukkan data pengikut
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>NIK</label>
								<input type="text" name="fnikpengikut2" id="fnikpengikut2" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="fnamapengikut2" id="fnamapengikut2" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>SHDK</label>
								<select class="form-control select2" style="width: 100%;" name="fshdkpengikut2" id="fshdkpengikut2" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Kepala Keluarga">Kepala Keluarga</option>
									<option value="Suami">Suami</option>
									<option value="Istri">Istri</option>
									<option value="Anak">Anak</option>
									<option value="Menantu">Menantu</option>
									<option value="Cucu">Cucu</option>
									<option value="Orang Tua">Orang Tua</option>
									<option value="Mertua">Mertua</option>
									<option value="Famili Lain">Famili Lain</option>
									<option value="Lainnya">Lainnya</option>
								</select>
							</div>
						</div>
					</div>
          <input class="btn btn-primary" type="button" name="addPengikut" onclick="tambahPengikut2()" value="Tambah" style="margin-top: 25px" />
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Detail Pengikut</h3>
								</div>
								<div class="box-body">
									<div class="table-responsive">
										<table id="tbPengikut2" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>No</th>
													<th>NIK</th>
													<th>Nama</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody>

											</tbody>
											<tfoot>
												<tr>
													<th>No</th>
													<th>NIK</th>
													<th>Nama</th>
													<th>Aksi</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div> -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanEditF108" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

$(function() {
	enable_cb();
	$("#cbTambahNama").click(enable_cb);
});

function enable_cb() {
	if (this.checked) {
		$("#fpengikut").removeAttr("readonly");
		$('#form_pengikut').show();
	} else {
		$("#fpengikut").attr("readonly", true);
		$('#form_pengikut').hide();
	}
}

function tambahPengikut() {
	var nik = $('#fnikpengikut').val();
	var nama = $('#fnamapengikut').val();
	var shdk = $('#fshdkpengikut').val();
	var tbProduk = $('#tbPengikut').DataTable();
	var indexed = 0;

	if (tbProduk.rows().count() > 0) { // jika ada isi

		var data = tbProduk.rows().data();
		data.each(function(value, index) {
			indexed = indexed + 1;
		});

		tbProduk.row.add([
			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
		]).draw(false);

	} else {
		tbProduk.row.add([
			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
		]).draw(false);

	}

	$('#fnikpengikut').val('');
	$('#fnamapengikut').val('');
	$('#fshdkpengikut').find('option:empty');

}

function hps_pengikut(index) {
	// hapus row
	var tbProduk = $('#tbPengikut').DataTable();
	tbProduk.row(index).remove().draw();
}

// function tambahPengikut2() {
// 	var nik = $('#fnikpengikut2').val();
// 	var nama = $('#fnamapengikut2').val();
// 	var shdk = $('#fshdkpengikut2').val();
// 	var tbProduk = $('#tbPengikut2').DataTable();
// 	var indexed = 0;
//
// 	if (tbProduk.rows().count() > 0) { // jika ada isi
//
// 		var data = tbProduk.rows().data();
// 		data.each(function(value, index) {
// 			indexed = indexed + 1;
// 		});
//
// 		tbProduk.row.add([
// 			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
// 			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
// 			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
// 			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut2(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
// 		]).draw(false);
//
// 	} else {
// 		tbProduk.row.add([
// 			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
// 			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
// 			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
// 			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut2(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
// 		]).draw(false);
//
// 	}
//
// 	$('#fnikpengikut').val('');
// 	$('#fnamapengikut').val('');
// 	$('#fshdkpengikut').find('option:empty');
//
// }
//
// function hps_pengikut2(index) {
// 	// hapus row
// 	var tbProduk = $('#tbPengikut2').DataTable();
// 	tbProduk.row(index).remove().draw();
// }

</script>
