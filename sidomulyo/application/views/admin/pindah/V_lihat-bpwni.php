<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Lihat BPWNI</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail BPWNI</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<!-- <div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($bpwni['created_at']))) ?> WIB
							</h4>
						</div> -->
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $bpwni['pengikut_nik']; ?></td>
									</tr>
									<tr>
										<td>Nama Pemohon</td>
										<td>: <?= $bpwni['pengikut_nama']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($bpwni['bpwni_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Lahir </td>
										<td>: <?= $bpwni['bpwni_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($bpwni['bpwni_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Gol Darah</td>
										<td>: <?= $bpwni['bpwni_gol_darah']; ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $bpwni['bpwni_agama']; ?></td>
									</tr>
									<tr>
										<td>Pendidikan Terakhir</td>
										<td>: <?= $bpwni['bpwni_pendidikan_terakhir']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $bpwni['bpwni_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Sebelumnya</td>
										<td>: <?= $bpwni['bpwni_alamat_lama']; ?></td>
									</tr>
									<tr>
										<td>Penyandang Cacat</td>
										<td>: <?= $bpwni['bpwni_cacat']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $bpwni['bpwni_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>Status Hub. Dalam Keluarga</td>
										<td>: <?= $bpwni['bpwni_status_hub_keluarga']; ?></td>
									</tr>
									<tr>
										<td>NIK Ibu</td>
										<td>: <?= $bpwni['bpwni_nik_ibu']; ?></td>
									</tr>
									<tr>
										<td>Nama Ibu</td>
										<td>: <?= $bpwni['bpwni_nama_ibu']; ?></td>
									</tr>
									<tr>
										<td>NIK Ayah</td>
										<td>: <?= $bpwni['bpwni_nik_ayah']; ?></td>
									</tr>
									<tr>
										<td>Nama Ayah</td>
										<td>: <?= $bpwni['bpwni_nama_ayah']; ?></td>
									</tr>
									<tr>
										<td>Alamat Sekarang</td>
										<td>: <?= $bpwni['bpwni_alamat_sekarang']; ?></td>
									</tr>
									<tr>
										<td>Desa</td>
										<td>: <?= $bpwni['bpwni_desa_as']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $bpwni['bpwni_kec_as']; ?></td>
									</tr>
									<tr>
										<td>Kabupaten / Kota</td>
										<td>: <?= $bpwni['bpwni_kab_as']; ?></td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?= $bpwni['bpwni_provinsi_as']; ?></td>
									</tr>
									<tr>
										<td>No KK</td>
										<td>: <?= $bpwni['bpwni_no_kk']; ?></td>
									</tr>
									<tr>
										<td>No Paspor</td>
										<td>: <?= $bpwni['bpwni_no_paspor']; ?></td>
									</tr>
									<tr>
										<td>Tgl Berakhir Paspor</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($bpwni['bpwni_tgl_exp_paspor']))) ?></td>
									</tr>
									<tr>
										<td>No Akta / Surat Kenal Lahir</td>
										<td>: <?= $bpwni['bpwni_no_akta']; ?></td>
									</tr>
									<tr>
										<td>Nomo Buku Nikah</td>
										<td>: <?= $bpwni['bpwni_no_perkawinan']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Perkawinan</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($bpwni['bpwni_tgl_kawin']))) ?></td>
									</tr>
									<tr>
										<td>No Akta Perceraian</td>
										<td>: <?= $bpwni['bpwni_no_perceraian']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Perceraian</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($bpwni['bpwni_tgl_cerai']))) ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-pindah-bpwni/' . $bpwni['id_pengikut']) ?>" class="btn btn-primary" target="_blank">Cetak Surat</a>
						<a href="<?php echo site_url("lihat-suket-pindah/" . $bpwni['id_surat']) ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
