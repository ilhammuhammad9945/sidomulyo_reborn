<script src="<?php echo base_url()?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Pindah Domisili</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
			<!-- /.box-header -->
			<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div> -->

		<!-- <div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Progress Pengisian Form</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="progress">
							<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
								<span class="sr-only">60% Complete (success)</span>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (left) -->
		</div>

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Suket Pindah</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_pindah/prosesSimpanAdd') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Pemohon</label>
								<input type="hidden" name="id_peng" class="form-control" value="<?= $invoice; ?>" tabindex="4" readonly required>
								<input type="text" name="fnamapemohon" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir</label>
										<input type="text" name="ftmptlahirpemohon" class="form-control" tabindex="5">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" name="ftgllahirpemohon" class="form-control" tabindex="6">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<!-- checkbox -->
              <div class="row">
                <div class="col-lg-6">
									<div class="form-group">
										<label>Jenis Kelamin</label><br>
		                <select class="form-control select2" style="width: 100%;" name="fgenderpemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="L">Laki-laki</option>
											<option value="P">Perempuan</option>
										</select>
									</div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
    								<label>Agama</label><br>
                    <select class="form-control select2" style="width: 100%;" name="fagamapemohon" tabindex="7" required>
    									<option value="">--- Pilih ---</option>
    									<option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Hindu">Hindu</option>
    									<option value="Budha">Budha</option>
    								</select>
    							</div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
    								<label>Status Perkawinan</label><br>
                    <select class="form-control select2" style="width: 100%;" name="fstatusnikahpemohon" tabindex="7" required>
    									<option value="">--- Pilih ---</option>
    									<option value="Belum Kawin">Belum Kawin</option>
                      <option value="Kawin">Kawin</option>
                      <option value="Cerai Hidup">Cerai Hidup</option>
    									<option value="Cerai Mati">Cerai Mati</option>
    								</select>
    							</div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
    								<label>Pendidikan (Tamat)</label><br>
                    <select class="form-control select2" style="width: 100%;" name="fpendidikanpemohon" tabindex="7" required>
    									<option value="">--- Pilih ---</option>
    									<option value="SD">SD</option>
                      <option value="SMP">SMP</option>
                      <option value="SMA">SMA</option>
    									<option value="Akademik">Akademik</option>
    								</select>
    							</div>
                </div>
              </div>
							<div class="row">
                <div class="col-lg-6">
									<div class="form-group">
										<label>Pekerjaan</label>
										<input type="text" name="fpekerjaanpemohon" class="form-control" tabindex="7">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kewarganegaraan</label><br>
										<select class="form-control select2" style="width: 100%;" name="fkewarganegaraanpemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="WNI">WNI</option>
											<option value="WNA">WNA / Orang Asing</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Alamat Asal</label>
								<input type="text" name="falamatpemohon" class="form-control" tabindex="8">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>No. KTP</label>
								<input type="text" name="fktppemohon" class="form-control" tabindex="13">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Tujuan Ke-</label>
								<input type="text" name="ftujuanke" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" class="form-control" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" class="form-control" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="form-group">
								<label>Tanggal Berangkat</label>
								<input type="date" name="ftglberangkat" class="form-control" tabindex="13">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alasan Pindah</label>
								<input type="text" name="falasanpindah" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
					<div class="checkbox">
						<label style="font-size:12pt;padding-top:20px">
							<input type="checkbox" name="tambahNama" id="cbTambahNama" style="margin-right:5px"> Centang jika ingin menambah data pengikut
						</label>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jumlah Pengikut (Orang)</label>
								<input type="number" name="fpengikut" id="fpengikut" class="form-control" tabindex="14" readonly>
							</div>
						</div>
					</div>

					<div id="form_pengikut" style="display:none">
						<div class="alert alert-info alert-dismissible" style="margin-top:30px">
							<h4><i class="icon fa fa-info"></i> Data Pengikut</h4>
							Masukkan data pengikut
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>NIK</label>
									<input type="text" id="fnikpengikut" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="form-group">
									<label>Nama</label>
									<input type="text" id="fnamapengikut" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
	              <div class="row">
	                <div class="col-lg-6">
	                  <div class="form-group">
	    								<label>Jenis Kelamin</label><br>
	                    <select class="form-control select2" style="width: 100%;" id="fgenderpengikut" tabindex="7">
	    									<option value="">--- Pilih ---</option>
	    									<option value="L">Laki-laki</option>
	    									<option value="P">Perempuan</option>
	    								</select>
	    							</div>
	                </div>
	                <div class="col-lg-6">
	                  <div class="form-group">
	    								<label>Umur</label>
	    								<input type="number" id="fumurpengikut" class="form-control" tabindex="4">
	    							</div>
	                </div>
	              </div>
							</div>
							<div class="col-md-6">
								<!-- checkbox -->
								<div class="form-group">
									<label>Status Perkawinan</label><br>
	                <select class="form-control select2" style="width: 100%;" id="fstatusnikahpengikut" tabindex="7">
	                  <option value="">--- Pilih ---</option>
	                  <option value="Belum Kawin">Belum Kawin</option>
	                  <option value="Kawin">Kawin</option>
	                  <option value="Cerai Hidup">Cerai Hidup</option>
	                  <option value="Cerai Mati">Cerai Mati</option>
	                </select>
								</div>
								<!-- checkbox -->
								<div class="form-group">
									<label>Pendidikan (Tamat)</label><br>
	                <select class="form-control select2" style="width: 100%;" id="fpendidikanpengikut" tabindex="7">
	                  <option value="">--- Pilih ---</option>
	                  <option value="SD">SD</option>
	                  <option value="SMP">SMP</option>
	                  <option value="SMA">SMA</option>
	                  <option value="Akademik">Akademik</option>
	                </select>
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" id="fketpengikut" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
							</div>
						</div>
	          <input class="btn btn-primary" type="button" name="addPengikut" onclick="tambahPengikut()" value="Tambah" style="margin-top: 25px" />
	          <button class="btn btn-danger" type="reset" style="margin-top: 25px">Reset</button>
						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="box-header">
										<h3 class="box-title">Detail Pengikut</h3>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="table-responsive">
											<table id="tbPengikut" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>NIK</th>
														<th>Nama</th>
														<th>Jenis Kelamin</th>
														<th>Umur</th>
														<th>Status Perkawinan</th>
														<th>Pendidikan</th>
														<th>Keterangan</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
												<tfoot>
													<tr>
														<th>NIK</th>
														<th>Nama</th>
														<th>Jenis Kelamin</th>
														<th>Umur</th>
														<th>Status Perkawinan</th>
														<th>Pendidikan</th>
														<th>Keterangan</th>
														<th>Aksi</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpanPindah" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
	$(function() {
		enable_cb();
		$("#cbTambahNama").click(enable_cb);
	});

	function enable_cb() {
		if (this.checked) {
			$("#fpengikut").removeAttr("readonly");
			$('#form_pengikut').show();
		} else {
			$("#fpengikut").attr("readonly", true);
			$('#form_pengikut').hide();
		}
	}

	function tambahPengikut() {
		var nik = $('#fnikpengikut').val();
		var nama = $('#fnamapengikut').val();
		var gender = $('#fgenderpengikut').val();
		var umur = $('#fumurpengikut').val();
		var statusnikah = $('#fstatusnikahpengikut').val();
		var pendidikan = $('#fpendidikanpengikut').val();
		var keterangan = $('#fketpengikut').val();
		var tbProduk = $('#tbPengikut').DataTable();
		var indexed = 0;

		if (tbProduk.rows().count() > 0) { // jika ada isi

			var data = tbProduk.rows().data();
			data.each(function(value, index) {
				indexed = indexed + 1;
			});

			tbProduk.row.add([
				nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
				nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
				gender + '<input type="hidden" value="' + gender + '" name="gender_pengikut[]" readonly>',
				umur + '<input type="hidden" value="' + umur + '" name="umur_pengikut[]" readonly>',
				statusnikah + '<input type="hidden" value="' + statusnikah + '" name="status_nikah_pengikut[]" readonly>',
				pendidikan + '<input type="hidden" value="' + pendidikan + '" name="pendidikan_pengikut[]" readonly>',
				keterangan + '<input type="hidden" value="' + keterangan + '" name="keterangan_pengikut[]" readonly>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		} else {
			tbProduk.row.add([
				nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
				nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
				gender + '<input type="hidden" value="' + gender + '" name="gender_pengikut[]" readonly>',
				umur + '<input type="hidden" value="' + umur + '" name="umur_pengikut[]" readonly>',
				statusnikah + '<input type="hidden" value="' + statusnikah + '" name="status_nikah_pengikut[]" readonly>',
				pendidikan + '<input type="hidden" value="' + pendidikan + '" name="pendidikan_pengikut[]" readonly>',
				keterangan + '<input type="hidden" value="' + keterangan + '" name="keterangan_pengikut[]" readonly>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		}

		$('#fnikpengikut').val('');
		$('#fnamapengikut').val('');
		$('#fgenderpengikut').find('option:empty');
		$('#fumurpengikut').val('');
		$('#fstatusnikahpengikut').val('');
		$('#fpendidikanpengikut').val('');
		$('#fketpengikut').val('');

	}

	function hps_pengikut(index) {
		// hapus row
		var tbProduk = $('#tbPengikut').DataTable();
		tbProduk.row(index).remove().draw();
	}
</script>
