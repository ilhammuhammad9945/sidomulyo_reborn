<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Lihat Ket. Pindah</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Ket. Pindah</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($pindah['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $pindah['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Nama Pemohon</td>
										<td>: <?= $pindah['nama_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($pindah['pindah_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Lahir </td>
										<td>: <?= $pindah['pindah_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($pindah['pindah_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan</td>
										<td>: <?= $pindah['pindah_kewarganegaraan']; ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $pindah['pindah_agama']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $pindah['pindah_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>Pendidikan</td>pindah
										<td>: <?= $pindah['pindah_pendidikan']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $pindah['pindah_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Alamat Asal</td>
										<td>: <?= $pindah['pindah_alamat_asal']; ?></td>
									</tr>
									<tr>
										<td>Tujuan Ke</td>
										<td>: <?= $pindah['pindah_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Desa</td>
										<td>: <?= $pindah['pindah_desa_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Kecamatan</td>
										<td>: <?= $pindah['pindah_kec_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Kabupaten / Kota</td>
										<td>: <?= $pindah['pindah_kab_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Provinsi</td>
										<td>: <?= $pindah['pindah_provinsi_tujuan']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Berangkat</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($pindah['pindah_tanggal_berangkat']))) ?></td>
									</tr>
									<tr>
										<td>Alasan Berpergian</td>
										<td>: <?= $pindah['pindah_alasan']; ?></td>
									</tr>
									<tr>
										<td>Jumlah Pengikut</td>
										<td>: <?= $pindah['pindah_pengikut']; ?></td>
									</tr>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header">
											<h3 class="box-title">Detail Pengikut</h3>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table id="tbPengikut" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
													<thead>
														<tr>
															<th>NIK</th>
															<th>Nama</th>
															<th>Jenis Kelamin</th>
															<th>Umur</th>
															<th>Status Perkawinan</th>
															<th>Pendidikan</th>
															<th>Keterangan</th>
															<th>Aksi</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($pengikut as $key => $value) : ?>
															<tr>
																<td><?= $value->pengikut_nik ?></td>
																<td><?= $value->pengikut_nama ?></td>
																<td><?= $value->pengikut_gender ?></td>
																<td><?= $value->pengikut_umur ?></td>
																<td><?= $value->pengikut_status_kawin ?></td>
																<td><?= $value->pengikut_pendidikan ?></td>
																<td><?= $value->pengikut_keterangan ?></td>
																<td>
																	<a href="<?php echo site_url('edit-bpwni/' . $value->id_pengikut) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
																	<a href="<?php echo site_url('lihat-bpwni/' . $value->id_pengikut) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>

																</td>

															</tr>
														<?php endforeach; ?>
													</tbody>
													<tfoot>
														<tr>
															<th>NIK</th>
															<th>Nama</th>
															<th>Jenis Kelamin</th>
															<th>Umur</th>
															<th>Status Perkawinan</th>
															<th>Pendidikan</th>
															<th>Keterangan</th>
															<th>Aksi</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
									<!-- /.box -->
								</div>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<?php if ($pindah['pindah_pengikut'] == 0) { ?>
							<a href="<?php echo site_url('print-pindah-sendiri/' . $pindah['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Surat Pindah</a>
						<?php } else { ?>
							<a href="<?php echo site_url('print-pindah-ikut/' . $pindah['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Surat Pindah</a>
						<?php } ?>
						<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
