<script src="<?php echo base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Pindah</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Ket. Pindah</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_pindah/prosesSimpanEdit') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Pemohon</label>
								<input type="hidden" name="id_surat" class="form-control" value="<?= $pindah['id_surat'] ?>" tabindex="4" readonly required>
								<input type="text" name="fnamapemohon" value="<?= $pindah['nama_pemohon'] ?>" class="form-control" tabindex="4">
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir</label>
										<input type="text" name="ftmptlahirpemohon" value="<?= $pindah['pindah_tempat_lahir'] ?>" class="form-control" tabindex="5">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" name="ftgllahirpemohon" value="<?= $pindah['pindah_tanggal_lahir'] ?>" class="form-control" tabindex="6">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<!-- checkbox -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Jenis Kelamin</label><br>
										<select class="form-control select2" style="width: 100%;" name="fgenderpemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="L" <?php if ($pindah['pindah_gender'] == 'L') {
																	echo "selected";
																} ?>>Laki-laki</option>
											<option value="P" <?php if ($pindah['pindah_gender'] == 'P') {
																	echo "selected";
																} ?>>Perempuan</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Agama</label><br>
										<select class="form-control select2" style="width: 100%;" name="fagamapemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="Islam" <?php if ($pindah['pindah_agama'] == 'Islam') {
																		echo "selected";
																	} ?>>Islam</option>
											<option value="Kristen" <?php if ($pindah['pindah_agama'] == 'Kristen') {
																		echo "selected";
																	} ?>>Kristen</option>
											<option value="Hindu" <?php if ($pindah['pindah_agama'] == 'Hindu') {
																		echo "selected";
																	} ?>>Hindu</option>
											<option value="Budha" <?php if ($pindah['pindah_agama'] == 'Budha') {
																		echo "selected";
																	} ?>>Budha</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Status Perkawinan</label><br>
										<select class="form-control select2" style="width: 100%;" name="fstatusnikahpemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="Belum Kawin" <?php if ($pindah['pindah_status_kawin'] == 'Belum Kawin') {
																			echo "selected";
																		} ?>>Belum Kawin</option>
											<option value="Kawin" <?php if ($pindah['pindah_status_kawin'] == 'Kawin') {
																		echo "selected";
																	} ?>>Kawin</option>
											<option value="Cerai Hidup" <?php if ($pindah['pindah_status_kawin'] == 'Cerai Hidup') {
																			echo "selected";
																		} ?>>Cerai Hidup</option>
											<option value="Cerai Mati" <?php if ($pindah['pindah_status_kawin'] == 'Cerai Mati') {
																			echo "selected";
																		} ?>>Cerai Mati</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Pendidikan (Tamat)</label><br>
										<select class="form-control select2" style="width: 100%;" name="fpendidikanpemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="SD" <?php if ($pindah['pindah_pendidikan'] == 'SD') {
																	echo "selected";
																} ?>>SD</option>
											<option value="SMP" <?php if ($pindah['pindah_pendidikan'] == 'SMP') {
																	echo "selected";
																} ?>>SMP</option>
											<option value="SMA" <?php if ($pindah['pindah_pendidikan'] == 'SMA') {
																	echo "selected";
																} ?>>SMA</option>
											<option value="Akademik" <?php if ($pindah['pindah_pendidikan'] == 'Akademik') {
																			echo "selected";
																		} ?>>Akademik</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Pekerjaan</label>
										<input type="text" name="fpekerjaanpemohon" value="<?= $pindah['pindah_pekerjaan'] ?>" class="form-control" tabindex="7">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kewarganegaraan</label><br>
										<select class="form-control select2" style="width: 100%;" name="fkewarganegaraanpemohon" tabindex="7" required>
											<option value="">--- Pilih ---</option>
											<option value="WNI" <?php if ($pindah['pindah_kewarganegaraan'] == 'WNI') {
																	echo "selected";
																} ?>>WNI</option>
											<option value="WNA" <?php if ($pindah['pindah_kewarganegaraan'] == 'WNA') {
																	echo "selected";
																} ?>>WNA / Orang Asing</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Alamat Asal</label>
								<input type="text" name="falamatpemohon" value="<?= $pindah['pindah_alamat_asal'] ?>" class="form-control" tabindex="8">
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>No. KTP</label>
								<input type="text" name="fktppemohon" value="<?= $pindah['nik_pemohon'] ?>" class="form-control" tabindex="13">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Tujuan Ke-</label>
								<input type="text" name="ftujuanke" value="<?= $pindah['pindah_tujuan'] ?>" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" value="<?= $pindah['pindah_desa_tujuan'] ?>" class="form-control" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" value="<?= $pindah['pindah_kec_tujuan'] ?>" class="form-control" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" value="<?= $pindah['pindah_kab_tujuan'] ?>" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" value="<?= $pindah['pindah_provinsi_tujuan'] ?>" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="form-group">
								<label>Tanggal Berangkat</label>
								<input type="date" name="ftglberangkat" value="<?= $pindah['pindah_tanggal_berangkat'] ?>" class="form-control" tabindex="13">
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alasan Pindah</label>
								<input type="text" name="falasanpindah" value="<?= $pindah['pindah_alasan'] ?>" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
					<div class="checkbox">
						<label style="font-size:12pt;padding-top:20px">
							<input type="checkbox" name="tambahNama" id="cbTambahNama" style="margin-right:5px"> Centang jika ingin merubah data pengikut (Jika tidak, maka abaikan saja).
						</label>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jumlah Pengikut (Orang)</label>
								<input type="number" name="fpengikut" id="fpengikut" value="<?= $pindah['pindah_pengikut'] ?>" class="form-control" tabindex="14" <?php if ($pindah['jenis_pindah'] == 'sendiri') {
																																										echo 'readonly';
																																									} ?>>
							</div>
						</div>
					</div>

					<div id="form_pengikut" style="display:block">
						<div class="alert alert-info alert-dismissible" style="margin-top:30px">
							<h4><i class="icon fa fa-info"></i> Data Pengikut</h4>
							Masukkan data pengikut
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>NIK</label>
									<input type="text" id="fnikpengikut" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="form-group">
									<label>Nama</label>
									<input type="text" id="fnamapengikut" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Jenis Kelamin</label><br>
											<select class="form-control select2" style="width: 100%;" id="fgenderpengikut" tabindex="7">
												<option value="">--- Pilih ---</option>
												<option value="L">Laki-laki</option>
												<option value="P">Perempuan</option>
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Umur</label>
											<input type="number" id="fumurpengikut" class="form-control" tabindex="4">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<!-- checkbox -->
								<div class="form-group">
									<label>Status Perkawinan</label><br>
									<select class="form-control select2" style="width: 100%;" id="fstatusnikahpengikut" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="Belum Kawin">Belum Kawin</option>
										<option value="Kawin">Kawin</option>
										<option value="Cerai Hidup">Cerai Hidup</option>
										<option value="Cerai Mati">Cerai Mati</option>
									</select>
								</div>
								<!-- checkbox -->
								<div class="form-group">
									<label>Pendidikan (Tamat)</label><br>
									<select class="form-control select2" style="width: 100%;" id="fpendidikanpengikut" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="SD">SD</option>
										<option value="SMP">SMP</option>
										<option value="SMA">SMA</option>
										<option value="Akademik">Akademik</option>
									</select>
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" id="fketpengikut" class="form-control" tabindex="4">
								</div>
								<!-- /.form-group -->
							</div>
						</div>
						<input class="btn btn-primary" type="button" name="addPengikut" onclick="tambahPengikut()" value="Tambah" style="margin-top: 25px" />
						<!-- <button class="btn btn-danger" onclick="hapus()" type="button" style="margin-top: 25px">Hapus Data Pengikut</button> -->
						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="box-header">
										<h3 class="box-title">Detail Pengikut</h3>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="table-responsive">
											<table id="tbPengikut" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>NIK</th>
														<th>Nama</th>
														<th>Jenis Kelamin</th>
														<th>Umur</th>
														<th>Status Perkawinan</th>
														<th>Pendidikan</th>
														<th>Keterangan</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($pengikut as $key => $value) : ?>
														<tr>
															<td><?= $value->pengikut_nik ?><input type="hidden" value="<?= $value->pengikut_nik ?>" name="nik_pengikut[]" readonly></td>
															<td><?= $value->pengikut_nama ?><input type="hidden" value="<?= $value->pengikut_nama ?>" name="nama_pengikut[]" readonly></td>
															<td><?= $value->pengikut_gender ?><input type="hidden" value="<?= $value->pengikut_gender ?>" name="gender_pengikut[]" readonly></td>
															<td><?= $value->pengikut_umur ?><input type="hidden" value="<?= $value->pengikut_umur ?>" name="umur_pengikut[]" readonly></td>
															<td><?= $value->pengikut_status_kawin ?><input type="hidden" value="<?= $value->pengikut_status_kawin ?>" name="status_nikah_pengikut[]" readonly></td>
															<td><?= $value->pengikut_pendidikan ?><input type="hidden" value="<?= $value->pengikut_pendidikan ?>" name="pendidikan_pengikut[]" readonly></td>
															<td><?= $value->pengikut_keterangan ?><input type="hidden" value="<?= $value->pengikut_keterangan ?>" name="keterangan_pengikut[]" readonly></td>
															<td><button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(<?= $no++; ?>)" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button></td>
														</tr>
													<?php endforeach; ?>
												</tbody>
												<tfoot>
													<tr>
														<th>NIK</th>
														<th>Nama</th>
														<th>Jenis Kelamin</th>
														<th>Umur</th>
														<th>Status Perkawinan</th>
														<th>Pendidikan</th>
														<th>Keterangan</th>
														<th>Aksi</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btneditPindah" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	$(function() {
		enable_cb();
		$("#cbTambahNama").click(enable_cb);
	});

	function enable_cb() {
		if (this.checked) {
			$("#fpengikut").removeAttr("readonly");
			$('#form_pengikut').show();
		} else {
			$("#fpengikut").attr("readonly", true);
			$('#form_pengikut').hide();
		}
	}

	function tambahPengikut() {
		var nik = $('#fnikpengikut').val();
		var nama = $('#fnamapengikut').val();
		var gender = $('#fgenderpengikut').val();
		var umur = $('#fumurpengikut').val();
		var statusnikah = $('#fstatusnikahpengikut').val();
		var pendidikan = $('#fpendidikanpengikut').val();
		var keterangan = $('#fketpengikut').val();
		var tbProduk = $('#tbPengikut').DataTable();
		var indexed = 0;

		if (tbProduk.rows().count() > 0) { // jika ada isi

			var data = tbProduk.rows().data();
			data.each(function(value, index) {
				indexed = indexed + 1;
			});

			tbProduk.row.add([
				nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
				nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
				gender + '<input type="hidden" value="' + gender + '" name="gender_pengikut[]" readonly>',
				umur + '<input type="hidden" value="' + umur + '" name="umur_pengikut[]" readonly>',
				statusnikah + '<input type="hidden" value="' + statusnikah + '" name="status_nikah_pengikut[]" readonly>',
				pendidikan + '<input type="hidden" value="' + pendidikan + '" name="pendidikan_pengikut[]" readonly>',
				keterangan + '<input type="hidden" value="' + keterangan + '" name="keterangan_pengikut[]" readonly>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		} else {
			tbProduk.row.add([
				nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
				nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
				gender + '<input type="hidden" value="' + gender + '" name="gender_pengikut[]" readonly>',
				umur + '<input type="hidden" value="' + umur + '" name="umur_pengikut[]" readonly>',
				statusnikah + '<input type="hidden" value="' + statusnikah + '" name="status_nikah_pengikut[]" readonly>',
				pendidikan + '<input type="hidden" value="' + pendidikan + '" name="pendidikan_pengikut[]" readonly>',
				keterangan + '<input type="hidden" value="' + keterangan + '" name="keterangan_pengikut[]" readonly>',
				'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
			]).draw(false);

		}

		$('#fnikpengikut').val('');
		$('#fnamapengikut').val('');
		$('#fgenderpengikut').find('option:empty');
		$('#fumurpengikut').val('');
		$('#fstatusnikahpengikut').val('');
		$('#fpendidikanpengikut').val('');
		$('#fketpengikut').val('');

	}

	function hps_pengikut(index) {
		// hapus row
		var tbProduk = $('#tbPengikut').DataTable();
		tbProduk.row(index).remove().draw();
		// tbProduk.clear();
	}

	// function hapus() {
	// 	var tbProduk = $('#tbPengikut').DataTable();
	// 	tbProduk.clear();
	// 	console.log("hehe");
	// }
</script>
