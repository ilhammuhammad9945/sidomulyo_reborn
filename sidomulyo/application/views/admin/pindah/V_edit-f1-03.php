<script src="<?php echo base_url()?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">F.1-03</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
			<!-- /.box-header -->
			<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div> -->

		<!-- <div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Form F.1-03</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_pindah/prosesSimpanEditF103') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<h4><b>DATA WILAYAH</b></h4>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Provinsi</label>
								<input type="hidden" name="id_surat" class="form-control" value="<?= $f103['id_surat'] ?>" tabindex="4" readonly required>
								<input type="text" name="fnamaprovinsi" value="<?= $f103['wil_nama_prov'] ?>" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kabupaten / Kota</label>
								<input type="text" name="fnamakabupaten" value="<?= $f103['wil_nama_kab'] ?>" class="form-control" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kecamatan</label>
								<input type="text" name="fnamakecamatan" value="<?= $f103['wil_nama_kec'] ?>" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kelurahan / Desa</label>
								<input type="text" name="fnamadesa" value="<?= $f103['wil_nama_desa'] ?>" class="form-control" tabindex="4">
							</div>
						</div>
					</div>
					<h4><b>DATA KELUARGA</b></h4>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Kepala Keluarga</label>
								<input type="text" name="fnamakepalakeluarga" value="<?= $f103['kel_nama_kep_kel'] ?>" class="form-control" tabindex="4">
							</div>
							<div class="form-group">
								<label>Nomor Kartu Keluarga</label>
								<input type="text" name="fnokkkeluarga" value="<?= $f103['kel_no_kk'] ?>" class="form-control" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Alamat Keluarga</label>
								<input type="text" name="falamat" value="<?= $f103['kel_alamat_kel'] ?>" class="form-control" tabindex="14">
							</div>
							<!-- /.form-group -->
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Desa</label>
										<input type="text" name="fdesa" value="<?= $f103['kel_desa'] ?>" class="form-control" tabindex="15">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<input type="text" name="fkecamatan" value="<?= $f103['kel_kec'] ?>" class="form-control" tabindex="16">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-5">
									<div class="form-group">
										<label>Kabupaten / Kota</label>
										<input type="text" name="fkabkota" value="<?= $f103['kel_kab'] ?>" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Provinsi</label>
										<input type="text" name="fprovinsi" value="<?= $f103['kel_prov'] ?>" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										<label>Kode Pos</label>
										<input type="text" name="fkodepos" value="<?= $f103['kel_kode_pos'] ?>" class="form-control" tabindex="18">
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>No Telp</label>
										<input type="text" name="ftelpon" value="<?= $f103['kel_no_telp'] ?>" class="form-control" tabindex="17">
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							<!-- checkbox -->
							</div>
						</div>

						<h4><b>DATA INDIVIDU</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama Lengkap</label>
									<input type="text" name="find_namalengkap" value="<?= $f103['ind_nama'] ?>" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>No. KTP / NIK</label>
									<input type="text" name="find_noktp" value="<?= $f103['ind_nik'] ?>" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>Alamat Sebelumnya</label>
									<input type="text" name="find_alamatasal" value="<?= $f103['ind_alamat'] ?>" class="form-control" tabindex="14">
								</div>
								<!-- /.form-group -->
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Desa</label>
											<input type="text" name="find_desa" value="<?= $f103['ind_desa'] ?>" class="form-control" tabindex="15">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Kecamatan</label>
											<input type="text" name="find_kecamatan" value="<?= $f103['ind_kec'] ?>" class="form-control" tabindex="16">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-5">
										<div class="form-group">
											<label>Kabupaten / Kota</label>
											<input type="text" name="find_kabupaten" value="<?= $f103['ind_kab'] ?>" class="form-control" tabindex="17">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Provinsi</label>
											<input type="text" name="find_provinsi" value="<?= $f103['ind_prov'] ?>" class="form-control" tabindex="18">
										</div>
										<!-- /.form-group -->
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											<label>Kode pos</label>
											<input type="text" name="find_kodepos" value="<?= $f103['ind_kode_pos'] ?>" class="form-control" tabindex="18">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>No Telp</label>
											<input type="text" name="ftelpontujuan" value="<?= $f103['ind_no_telp'] ?>" class="form-control" tabindex="17">
										</div>
										<!-- /.form-group -->
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Nomor Paspor</label>
											<input type="text" name="find_no_paspor" value="<?= $f103['ind_no_paspor'] ?>" class="form-control" tabindex="18">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Tgl Berakhir Paspor</label>
											<input type="date" name="find_tglberakhirpaspor" value="<?= $f103['ind_tgl_exp_paspor'] ?>" class="form-control" tabindex="18">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Jenis Kelamin</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_gender" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="L" <?php if($f103['ind_gender'] == 'L') { echo 'selected'; } ?>>Laki-laki</option>
										<option value="P" <?php if($f103['ind_gender'] == 'P') { echo 'selected'; } ?>>Perempuan</option>
									</select>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Tempat Lahir</label>
											<input type="text" name="find_tempatlahir" value="<?= $f103['ind_tempat_lahir'] ?>" class="form-control" tabindex="18">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<input type="date" name="find_tanggallahir" value="<?= $f103['ind_tanggal_lahir'] ?>" class="form-control" tabindex="18">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Umur</label>
									<input type="number" name="find_umur" value="<?= $f103['ind_umur'] ?>" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Akta Kelahiran / Surat Kenal Lahir</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_aktalahir" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Ada" <?php if($f103['ind_akte_lahir'] == 'Ada') { echo 'selected'; } ?>>Ada</option>
										<option value="Tidak Ada" <?php if($f103['ind_akte_lahir'] == 'Tidak Ada') { echo 'selected'; } ?>>Tidak Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nomor Akta Kelahiran / Surat Kelahiran</label>
									<input type="text" name="find_noaktalahir" value="<?= $f103['ind_no_akte_lahir'] ?>" class="form-control" tabindex="18">
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Golongan Darah</label><br>
			                <select class="form-control select2" style="width: 100%;" name="find_goldarah" tabindex="7" required>
												<option value="">--- Pilih ---</option>
												<option value="A" <?php if($f103['ind_gol_darah'] == 'A') { echo 'selected'; } ?>>A</option>
												<option value="B" <?php if($f103['ind_gol_darah'] == 'B') { echo 'selected'; } ?>>B</option>
												<option value="AB" <?php if($f103['ind_gol_darah'] == 'AB') { echo 'selected'; } ?>>AB</option>
												<option value="O" <?php if($f103['ind_gol_darah'] == '="') { echo 'selected'; } ?>>O</option>
												<option value="A+" <?php if($f103['ind_gol_darah'] == 'A+') { echo 'selected'; } ?>>A+</option>
												<option value="A-" <?php if($f103['ind_gol_darah'] == 'A-') { echo 'selected'; } ?>>A-</option>
												<option value="B+" <?php if($f103['ind_gol_darah'] == 'B+') { echo 'selected'; } ?>>B+</option>
												<option value="B-" <?php if($f103['ind_gol_darah'] == 'B-') { echo 'selected'; } ?>>B-</option>
												<option value="O+" <?php if($f103['ind_gol_darah'] == 'O+') { echo 'selected'; } ?>>O+</option>
												<option value="O-" <?php if($f103['ind_gol_darah'] == 'O-') { echo 'selected'; } ?>>O-</option>
												<option value="Tidak tahu" <?php if($f103['ind_gol_darah'] == 'Tidak tahu') { echo 'selected'; } ?>>Tidak tahu</option>
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Agama</label><br>
											<select class="form-control select2" style="width: 100%;" name="find_agama" tabindex="7" required>
												<option value="">--- Pilih ---</option>
												<option value="Islam" <?php if($f103['ind_agama'] == 'Islam') { echo 'selected'; } ?>>Islam</option>
												<option value="Kristen" <?php if($f103['ind_agama'] == 'Kristen') { echo 'selected'; } ?>>Kristen</option>
												<option value="Katolik" <?php if($f103['ind_agama'] == 'Katolik') { echo 'selected'; } ?>>Katolik</option>
												<option value="Hindu" <?php if($f103['ind_agama'] == 'Hindu') { echo 'selected'; } ?>>Hindu</option>
												<option value="Budha" <?php if($f103['ind_agama'] == 'Budha') { echo 'selected'; } ?>>Budha</option>
												<option value="Kong Hucu" <?php if($f103['ind_agama'] == 'Kong Hucu') { echo 'selected'; } ?>>Kong Hucu</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<label>Status Perkawinan</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_statuskawin" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Belum Kawin" <?php if($f103['ind_status_kawin'] == 'Belum Kawin') { echo 'selected'; } ?>>Belum Kawin</option>
										<option value="Kawin" <?php if($f103['ind_status_kawin'] == 'Kawin') { echo 'selected'; } ?>>Kawin</option>
										<option value="Cerai Hidup" <?php if($f103['ind_status_kawin'] == 'Cerai Hidup') { echo 'selected'; } ?>>Cerai Hidup</option>
										<option value="Cerai Mati" <?php if($f103['ind_status_kawin'] == 'Cerai Mati') { echo 'selected'; } ?>>Cerai Mati</option>
									</select>
								</div>
								<div class="form-group">
									<label>Akta Perkawinan / Buku Nikah</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_aktakawin" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Ada" <?php if($f103['ind_akta_kawin'] == 'Ada') { echo 'selected'; } ?>>Ada</option>
										<option value="Tidak Ada" <?php if($f103['ind_akta_kawin'] == 'Tidak Ada') { echo 'selected'; } ?>>Tidak Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nomor Akta Perkawinan / Buku Nikah</label>
									<input type="text" name="find_noaktakawin" value="<?= $f103['ind_no_akta_kawin'] ?>" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Tanggal Perkawinan</label>
	                <input type="date" name="find_tanggalkawin" value="<?= $f103['ind_tgl_kawin'] ?>" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Akta Perceraian / Surat Cerai</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_aktacerai" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Ada" <?php if($f103['ind_akta_cerai'] == 'Ada') { echo 'selected'; } ?>>Ada</option>
										<option value="Tidak Ada" <?php if($f103['ind_akta_cerai'] == 'Tidak Ada') { echo 'selected'; } ?>>Tidak Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nomor Akta Cerai / Surat Cerai</label>
									<input type="text" name="find_noaktacerai" value="<?= $f103['ind_no_akta_cerai'] ?>" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Tanggal Perceraian</label>
	                <input type="date" name="find_tanggalcerai" value="<?= $f103['ind_tgl_cerai'] ?>" class="form-control" tabindex="18">
								</div>
								<div class="form-group">
									<label>Status Hubungan Dalam Keluarga</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_shdk" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="Kepala Keluarga" <?php if($f103['ind_shdk'] == 'Kepala Keluarga') { echo 'selected'; } ?>>Kepala Keluarga</option>
										<option value="Suami" <?php if($f103['ind_shdk'] == 'Suami') { echo 'selected'; } ?>>Suami</option>
										<option value="Istri" <?php if($f103['ind_shdk'] == 'Istri') { echo 'selected'; } ?>>Istri</option>
										<option value="Anak" <?php if($f103['ind_shdk'] == 'Anak') { echo 'selected'; } ?>>Anak</option>
										<option value="Menantu" <?php if($f103['ind_shdk'] == 'Menantu') { echo 'selected'; } ?>>Menantu</option>
										<option value="Cucu" <?php if($f103['ind_shdk'] == 'Cucu') { echo 'selected'; } ?>>Cucu</option>
										<option value="Orang Tua" <?php if($f103['ind_shdk'] == 'Orang Tua') { echo 'selected'; } ?>>Orang Tua</option>
										<option value="Mertua" <?php if($f103['ind_shdk'] == 'Mertua') { echo 'selected'; } ?>>Mertua</option>
										<option value="Famili Lain" <?php if($f103['ind_shdk'] == 'Famili Lain') { echo 'selected'; } ?>>Famili Lain</option>
										<option value="Pembantu" <?php if($f103['ind_shdk'] == 'Pembantu') { echo 'selected'; } ?>>Pembantu</option>
										<option value="Lainnya" <?php if($f103['ind_shdk'] == 'Lainnya') { echo 'selected'; } ?>>Lainnya</option>
									</select>
								</div>
								<div class="form-group">
									<label>Kelainan Fisik dan Mental</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_cacat" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Tidak Ada" <?php if($f103['ind_kelainan_fisik'] == 'Ada') { echo 'selected'; } ?>>Tidak Ada</option>
										<option value="Ada" <?php if($f103['ind_kelainan_fisik'] == 'Tidak Ada') { echo 'selected'; } ?>>Ada</option>
									</select>
								</div>
								<div class="form-group">
									<label>Penyandang Cacat</label><br>
	                <select class="form-control select2" style="width: 100%;" name="find_jeniscacat" tabindex="7">
										<option value="">--- Pilih ---</option>
										<option value="Cacat Fisik" <?php if($f103['ind_cacat'] == 'Cacat Fisik') { echo 'selected'; } ?>>Cacat Fisik</option>
										<option value="Cacat Netra Buta" <?php if($f103['ind_cacat'] == 'Cacat Netra Buta') { echo 'selected'; } ?>>Cacat Netra Buta</option>
										<option value="Cacat Rungu Wicara" <?php if($f103['ind_cacat'] == 'Cacat Rungu Wicara') { echo 'selected'; } ?>>Cacat Rungu Wicara</option>
										<option value="Cacat Mental Jiwa" <?php if($f103['ind_cacat'] == 'Cacat Mental Jiwa') { echo 'selected'; } ?>>Cacat Mental Jiwa</option>
										<option value="Cacat Fisik dan Mental" <?php if($f103['ind_cacat'] == 'Cacat Fisik dan Menta') { echo 'selected'; } ?>>Cacat Fisik dan Mental</option>
										<option value="Cacat Lainnya" <?php if($f103['ind_cacat'] == 'Cacat Lainnya') { echo 'selected'; } ?>>Cacat Lainnya</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pendidikan (Tamat)</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_pendidikan" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Tidak Sekolah" <?php if($f103['ind_pendidikan'] == 'Tidak Sekolah') { echo 'selected'; } ?>>Tidak Sekolah</option>
										<option value="Tidak Tamat SD" <?php if($f103['ind_pendidikan'] == 'Tidak Tamat SD') { echo 'selected'; } ?>>Tidak Tamat SD</option>
										<option value="SD" <?php if($f103['ind_pendidikan'] == 'SD') { echo 'selected'; } ?>>SD</option>
										<option value="SMP" <?php if($f103['ind_pendidikan'] == 'SMP') { echo 'selected'; } ?>>SMP</option>
										<option value="SMA" <?php if($f103['ind_pendidikan'] == 'SMA') { echo 'selected'; } ?>>SMA</option>
										<option value="Diploma I/II" <?php if($f103['ind_pendidikan'] == 'Diploma I/II') { echo 'selected'; } ?>>Diploma I/II</option>
										<option value="Diploma II/S" <?php if($f103['ind_pendidikan'] == 'Diploma II/S') { echo 'selected'; } ?>>Diploma II/S</option>
										<option value="Diploma IV/S1" <?php if($f103['ind_pendidikan'] == 'Diploma IV/S1') { echo 'selected'; } ?>>Diploma IV/S1</option>
										<option value="Strata II" <?php if($f103['ind_pendidikan'] == 'Strata II') { echo 'selected'; } ?>>Strata II</option>
										<option value="Strata III" <?php if($f103['ind_pendidikan'] == 'Strata III') { echo 'selected'; } ?>>Strata III</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pekerjaan</label><br>
									<select class="form-control select2" style="width: 100%;" name="find_pekerjaan" tabindex="7" required>
										<option value="">--- Pilih ---</option>
										<option value="Belum / Tidak Bekerja" <?php if($f103['ind_pekerjaan'] == 'Belum / Tidak Bekerja') { echo 'selected'; } ?>>Belum / Tidak Bekerja</option>
										<option value="Mengurus Rumah Tangga" <?php if($f103['ind_pekerjaan'] == 'Mengurus Rumah Tangga') { echo 'selected'; } ?>>Mengurus Rumah Tangga</option>
										<option value="Pelajar / Mahasiswa" <?php if($f103['ind_pekerjaan'] == 'Pelajar / Mahasiswa') { echo 'selected'; } ?>>Pelajar / Mahasiswa</option>
										<option value="Pensiunan" <?php if($f103['ind_pekerjaan'] == 'Pensiunan') { echo 'selected'; } ?>>Pensiunan</option>
										<option value="Pegawai Negeri Sipil" <?php if($f103['ind_pekerjaan'] == 'Pegawai Negeri Sipil') { echo 'selected'; } ?>>Pegawai Negeri Sipil</option>
										<option value="Tentara Nasional Indonesia" <?php if($f103['ind_pekerjaan'] == 'Tentara Nasional Indonesia') { echo 'selected'; } ?>>Tentara Nasional Indonesia</option>
										<option value="Kepolisian RI" <?php if($f103['ind_pekerjaan'] == 'Kepolisian RI') { echo 'selected'; } ?>>Kepolisian RI</option>
										<option value="Perdagangan" <?php if($f103['ind_pekerjaan'] == 'Perdagangan') { echo 'selected'; } ?>>Perdagangan</option>
										<option value="Petani / Pekebun" <?php if($f103['ind_pekerjaan'] == 'Petani / Pekebun') { echo 'selected'; } ?>>Petani / Pekebun</option>
										<option value="Peternak" <?php if($f103['ind_pekerjaan'] == 'Peternak') { echo 'selected'; } ?>>Peternak</option>
										<option value="Nelayan / Perikanan" <?php if($f103['ind_pekerjaan'] == 'Nelayan / Perikanan') { echo 'selected'; } ?>>Nelayan / Perikanan</option>
										<option value="Industri" <?php if($f103['ind_pekerjaan'] == 'Industri') { echo 'selected'; } ?>>Industri</option>
										<option value="Konstruksi" <?php if($f103['ind_pekerjaan'] == 'Konstruksi') { echo 'selected'; } ?>>Konstruksi</option>
										<option value="Transportasi" <?php if($f103['ind_pekerjaan'] == 'Transportasi') { echo 'selected'; } ?>>Transportasi</option>
										<option value="Karyawan Swasta" <?php if($f103['ind_pekerjaan'] == 'Karyawan Swasta') { echo 'selected'; } ?>>Karyawan Swasta</option>
										<option value="Karyawan BUMN" <?php if($f103['ind_pekerjaan'] == 'Karyawan BUMN') { echo 'selected'; } ?>>Karyawan BUMN</option>
										<option value="Karyawan BUMD" <?php if($f103['ind_pekerjaan'] == 'Karyawan BUMD') { echo 'selected'; } ?>>Karyawan BUMD</option>
										<option value="Karyawan Honorer" <?php if($f103['ind_pekerjaan'] == 'Karyawan Honorer') { echo 'selected'; } ?>>Karyawan Honorer</option>
										<option value="Buruh Harian Lepas" <?php if($f103['ind_pekerjaan'] == 'Buruh Harian Lepas') { echo 'selected'; } ?>>Buruh Harian Lepas</option>
										<option value="Buruh Tani / Perkebunan" <?php if($f103['ind_pekerjaan'] == 'Buruh Tani / Perkebuna') { echo 'selected'; } ?>>Buruh Tani / Perkebunan</option>
										<option value="Buruh Nelayan / Perikanan" <?php if($f103['ind_pekerjaan'] == 'Buruh Nelayan / Perikanan') { echo 'selected'; } ?>>Buruh Nelayan / Perikanan</option>
										<option value="Buruh Peternakan" <?php if($f103['ind_pekerjaan'] == 'Buruh Peternakan') { echo 'selected'; } ?>>Buruh Peternakan</option>
										<option value="Pembantu Rumah Tangga" <?php if($f103['ind_pekerjaan'] == 'Pembantu Rumah Tangga') { echo 'selected'; } ?>>Pembantu Rumah Tangga</option>
										<option value="Tukang Cukur" <?php if($f103['ind_pekerjaan'] == 'Tukang Cuku') { echo 'selected'; } ?>>Tukang Cukur</option>
										<option value="Tukang Listrik" <?php if($f103['ind_pekerjaan'] == 'Tukang Listrik') { echo 'selected'; } ?>>Tukang Listrik</option>
										<option value="Tukang Batu" <?php if($f103['ind_pekerjaan'] == 'Tukang Batu') { echo 'selected'; } ?>>Tukang Batu</option>
										<option value="Tukang Kayu" <?php if($f103['ind_pekerjaan'] == 'Tukang Kayu') { echo 'selected'; } ?>>Tukang Kayu</option>
										<option value="Tukang Sol Sepatu" <?php if($f103['ind_pekerjaan'] == 'Tukang Sol Sepat') { echo 'selected'; } ?>>Tukang Sol Sepatu</option>
										<option value="Tukang Las / Pandai Besi" <?php if($f103['ind_pekerjaan'] == 'Tukang Las / Pandai Besi') { echo 'selected'; } ?>>Tukang Las / Pandai Besi</option>
										<option value="Tukang Jahit" <?php if($f103['ind_pekerjaan'] == 'Tukang Jahit') { echo 'selected'; } ?>>Tukang Jahit</option>
										<option value="Penata Rambut" <?php if($f103['ind_pekerjaan'] == 'Penata Rambut') { echo 'selected'; } ?>>Penata Rambut</option>
										<option value="Penata Rias" <?php if($f103['ind_pekerjaan'] == 'Penata Rias') { echo 'selected'; } ?>>Penata Rias</option>
										<option value="Penata Busana" <?php if($f103['ind_pekerjaan'] == 'Penata Busana') { echo 'selected'; } ?>>Penata Busana </option>
										<option value="Mekanik" <?php if($f103['ind_pekerjaan'] == 'Mekanik') { echo 'selected'; } ?>>Mekanik</option>
										<option value="Tukang Gigi" <?php if($f103['ind_pekerjaan'] == 'Tukang Gigi') { echo 'selected'; } ?>>Tukang Gigi</option>
										<option value="Seniman Tabib" <?php if($f103['ind_pekerjaan'] == 'Seniman Tabib') { echo 'selected'; } ?>>Seniman Tabib</option>
										<option value="Tabib" <?php if($f103['ind_pekerjaan'] == 'Tabib') { echo 'selected'; } ?>>Tabib</option>
										<option value="Pengrajin" <?php if($f103['ind_pekerjaan'] == 'Pengrajin') { echo 'selected'; } ?>>Pengrajin</option>
										<option value="Perancang Busana" <?php if($f103['ind_pekerjaan'] == 'Perancang Busana') { echo 'selected'; } ?>>Perancang Busana</option>
										<option value="Peterjemah" <?php if($f103['ind_pekerjaan'] == 'Peterjemah') { echo 'selected'; } ?>>Peterjemah</option>
										<option value="Imam Masjid" <?php if($f103['ind_pekerjaan'] == 'Imam Masjid') { echo 'selected'; } ?>>Imam Masjid</option>
										<option value="Pendeta" <?php if($f103['ind_pekerjaan'] == 'Pendeta') { echo 'selected'; } ?>>Pendeta</option>
										<option value="Pastur" <?php if($f103['ind_pekerjaan'] == 'Pastur') { echo 'selected'; } ?>>Pastur</option>
										<option value="Wartawan" <?php if($f103['ind_pekerjaan'] == 'Wartawan') { echo 'selected'; } ?>>Wartawan</option>
										<option value="Ustad / Mubaligh" <?php if($f103['ind_pekerjaan'] == 'Ustad / Mubaligh') { echo 'selected'; } ?>>Ustad / Mubaligh</option>
										<option value="Juru Masak" <?php if($f103['ind_pekerjaan'] == 'Juru Masak') { echo 'selected'; } ?>>Juru Masak</option>
										<option value="Promotor Acara" <?php if($f103['ind_pekerjaan'] == 'Promotor Acara') { echo 'selected'; } ?>>Promotor Acara</option>
										<option value="Anggota DPR-RI" <?php if($f103['ind_pekerjaan'] == 'Anggota DPR-RI') { echo 'selected'; } ?>>Peternak</option>
										<option value="Anggota DPD" <?php if($f103['ind_pekerjaan'] == 'Anggota DPD') { echo 'selected'; } ?>>Anggota DPD</option>
										<option value="Anggota BPK" <?php if($f103['ind_pekerjaan'] == 'Anggota BPK') { echo 'selected'; } ?>>Anggota BPK</option>
										<option value="Presiden" <?php if($f103['ind_pekerjaan'] == 'Presiden') { echo 'selected'; } ?>>Presiden</option>
										<option value="Wakil Presiden" <?php if($f103['ind_pekerjaan'] == 'Wakil Presiden') { echo 'selected'; } ?>>Wakil Presiden</option>
										<option value="Anggota Mahkamah Konstitusi" <?php if($f103['ind_pekerjaan'] == 'Anggota Mahkamah Konstitusi') { echo 'selected'; } ?>>Anggota Mahkamah Konstitusi</option>
										<option value="Anggt Kabinet / Kementrian" <?php if($f103['ind_pekerjaan'] == 'Anggt Kabinet / Kementrian') { echo 'selected'; } ?>>Anggt Kabinet / Kementrian</option>
										<option value="Duta Besar" <?php if($f103['ind_pekerjaan'] == 'Duta Besa') { echo 'selected'; } ?>>Duta Besar</option>
										<option value="Gubernur" <?php if($f103['ind_pekerjaan'] == 'Gubernur') { echo 'selected'; } ?>>Gubernur</option>
										<option value="Wakil Gubernur" <?php if($f103['ind_pekerjaan'] == 'Wakil Gubernur') { echo 'selected'; } ?>>Wakil Gubernur</option>
										<option value="Bupati" <?php if($f103['ind_pekerjaan'] == 'Bupati') { echo 'selected'; } ?>>Bupati</option>
										<option value="Wakil Bupati" <?php if($f103['ind_pekerjaan'] == 'Wakil Bupati') { echo 'selected'; } ?>>Wakil Bupati</option>
										<option value="Wali Kota" <?php if($f103['ind_pekerjaan'] == 'Wali Kota') { echo 'selected'; } ?>>Wali Kota</option>
										<option value="Wakil Walikota" <?php if($f103['ind_pekerjaan'] == 'Wakil Walikota') { echo 'selected'; } ?>>Wakil Walikota</option>
										<option value="Anggt DPRD Pro" <?php if($f103['ind_pekerjaan'] == 'Anggt DPRD Pro') { echo 'selected'; } ?>>Anggt DPRD Pro</option>
										<option value="Anggt DPRD Kab/Kota" <?php if($f103['ind_pekerjaan'] == 'Anggt DPRD Kab/Kota') { echo 'selected'; } ?>>Anggt DPRD Kab/Kota</option>
										<option value="Dosen" <?php if($f103['ind_pekerjaan'] == 'Dosen') { echo 'selected'; } ?>>Dosen</option>
										<option value="Guru" <?php if($f103['ind_pekerjaan'] == 'Guru') { echo 'selected'; } ?>>Guru</option>
										<option value="Pilot" <?php if($f103['ind_pekerjaan'] == 'Pilot') { echo 'selected'; } ?>>Pilot</option>
										<option value="Pengacara" <?php if($f103['ind_pekerjaan'] == 'Pengacarak') { echo 'selected'; } ?>>Pengacara</option>
										<option value="Notaris" <?php if($f103['ind_pekerjaan'] == 'Notaris') { echo 'selected'; } ?>>Notaris</option>
										<option value="Arsitek" <?php if($f103['ind_pekerjaan'] == 'Arsitek') { echo 'selected'; } ?>>Arsitek</option>
										<option value="Akuntan" <?php if($f103['ind_pekerjaan'] == 'Akuntan') { echo 'selected'; } ?>>Akuntan</option>
										<option value="Konsultan" <?php if($f103['ind_pekerjaan'] == 'Konsultan') { echo 'selected'; } ?>>Konsultan</option>
										<option value="Dokter" <?php if($f103['ind_pekerjaan'] == 'Dokter') { echo 'selected'; } ?>>Dokter</option>
										<option value="Bidan" <?php if($f103['ind_pekerjaan'] == 'Bidan') { echo 'selected'; } ?>>Bidan</option>
										<option value="Perawat" <?php if($f103['ind_pekerjaan'] == 'Perawat') { echo 'selected'; } ?>>Perawat</option>
										<option value="Apoteker" <?php if($f103['ind_pekerjaan'] == 'Apoteker') { echo 'selected'; } ?>>Apoteker</option>
										<option value="Psikiater / Psikolog" <?php if($f103['ind_pekerjaan'] == 'Psikiater / Psikolog') { echo 'selected'; } ?>>Psikiater / Psikolog</option>
										<option value="Penyiar Televisi" <?php if($f103['ind_pekerjaan'] == 'Penyiar Televisi') { echo 'selected'; } ?>>Penyiar Televisi</option>
										<option value="Penyiar Radio" <?php if($f103['ind_pekerjaan'] == 'Penyiar Radio') { echo 'selected'; } ?>>Pendeta</option>
										<option value="Pelaut" <?php if($f103['ind_pekerjaan'] == 'Pelaut') { echo 'selected'; } ?>>Pelaut</option>
										<option value="Peneliti" <?php if($f103['ind_pekerjaan'] == 'Peneliti') { echo 'selected'; } ?>>Peneliti</option>
										<option value="Sopir" <?php if($f103['ind_pekerjaan'] == 'Sopir') { echo 'selected'; } ?>>Sopir</option>
										<option value="Pialang" <?php if($f103['ind_pekerjaan'] == 'Pialang') { echo 'selected'; } ?>>Pialang</option>
										<option value="Paranormal" <?php if($f103['ind_pekerjaan'] == 'Paranormal') { echo 'selected'; } ?>>Paranormal</option>
										<option value="Pedagang" <?php if($f103['ind_pekerjaan'] == 'Pedagang') { echo 'selected'; } ?>>Pedagang</option>
										<option value="Perangkat Desa" <?php if($f103['ind_pekerjaan'] == 'Perangkat Desa') { echo 'selected'; } ?>>Perangkat Desa</option>
										<option value="Kepala Desa" <?php if($f103['ind_pekerjaan'] == 'Kepala Desa') { echo 'selected'; } ?>>Kepala Desa</option>
										<option value="Biarawati" <?php if($f103['ind_pekerjaan'] == 'Biarawati') { echo 'selected'; } ?>>Biarawati</option>
										<option value="Wiraswasta" <?php if($f103['ind_pekerjaan'] == 'Wiraswasta') { echo 'selected'; } ?>>Wiraswasta</option>
									</select>
								</div>
							</div>
						</div>

						<h4><b>DATA ORANG TUA</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>NIK Ibu</label>
									<input type="text" name="fortu_nikibu" value="<?= $f103['ortu_nik_ibu'] ?>" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>Nama Lengkap Ibu</label>
									<input type="text" name="fortu_namaibu" value="<?= $f103['ortu_nama_ibu'] ?>" class="form-control" tabindex="14">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>NIK Ayah</label>
									<input type="text" name="fortu_nikayah" value="<?= $f103['ortu_nik_ayah'] ?>" class="form-control" tabindex="14">
								</div>
								<div class="form-group">
									<label>Nama Lengkap Ayah</label>
									<input type="text" name="fortu_namaayah" value="<?= $f103['ortu_nama_ayah'] ?>" class="form-control" tabindex="14">
								</div>
							</div>
						</div>

						<h4><b>DATA ADMINISTRASI</b></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama Ketua RT</label>
									<input type="text" name="fnamart" value="<?= $f103['adm_ketua_rt'] ?>" class="form-control" tabindex="14">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama Ketua RW</label>
									<input type="text" name="fnamarw" value="<?= $f103['adm_ketua_rw'] ?>" class="form-control" tabindex="14">
								</div>
							</div>
						</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanEditF103" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

$(function() {
	enable_cb();
	$("#cbTambahNama").click(enable_cb);
});

function enable_cb() {
	if (this.checked) {
		$("#fpengikut").removeAttr("readonly");
		$('#form_pengikut').show();
	} else {
		$("#fpengikut").attr("readonly", true);
		$('#form_pengikut').hide();
	}
}

function tambahPengikut() {
	var nik = $('#fnikpengikut').val();
	var nama = $('#fnamapengikut').val();
	var shdk = $('#fshdkpengikut').val();
	var tbProduk = $('#tbPengikut').DataTable();
	var indexed = 0;

	if (tbProduk.rows().count() > 0) { // jika ada isi

		var data = tbProduk.rows().data();
		data.each(function(value, index) {
			indexed = indexed + 1;
		});

		tbProduk.row.add([
			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
		]).draw(false);

	} else {
		tbProduk.row.add([
			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
		]).draw(false);

	}

	$('#fnikpengikut').val('');
	$('#fnamapengikut').val('');
	$('#fshdkpengikut').find('option:empty');

}

function hps_pengikut(index) {
	// hapus row
	var tbProduk = $('#tbPengikut').DataTable();
	tbProduk.row(index).remove().draw();
}

// function tambahPengikut2() {
// 	var nik = $('#fnikpengikut2').val();
// 	var nama = $('#fnamapengikut2').val();
// 	var shdk = $('#fshdkpengikut2').val();
// 	var tbProduk = $('#tbPengikut2').DataTable();
// 	var indexed = 0;
//
// 	if (tbProduk.rows().count() > 0) { // jika ada isi
//
// 		var data = tbProduk.rows().data();
// 		data.each(function(value, index) {
// 			indexed = indexed + 1;
// 		});
//
// 		tbProduk.row.add([
// 			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
// 			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
// 			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
// 			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut2(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
// 		]).draw(false);
//
// 	} else {
// 		tbProduk.row.add([
// 			nik + '<input type="hidden" value="' + nik + '" name="nik_pengikut[]" readonly>',
// 			nama + '<input type="hidden" value="' + nama + '" name="nama_pengikut[]" readonly>',
// 			shdk + '<input type="hidden" value="' + shdk + '" name="shdk_pengikut[]" readonly>',
// 			'<button type="button" class="btn btn-danger btn-sm" onclick="hps_pengikut2(\'' + indexed + '\')" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>'
// 		]).draw(false);
//
// 	}
//
// 	$('#fnikpengikut').val('');
// 	$('#fnamapengikut').val('');
// 	$('#fshdkpengikut').find('option:empty');
//
// }
//
// function hps_pengikut2(index) {
// 	// hapus row
// 	var tbProduk = $('#tbPengikut2').DataTable();
// 	tbProduk.row(index).remove().draw();
// }

</script>
