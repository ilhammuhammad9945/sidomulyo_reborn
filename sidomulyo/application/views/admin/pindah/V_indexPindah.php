<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li>Permohonan Surat</li>
			<li class="active">Data Ket. Pindah</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('tambah-suket-pindah') ?>" class="btn btn-block btn-primary">Tambah Data Ket. Pindah</a></h3>
						<!-- <h3 class="box-title"><a href="<?php echo site_url('tambah-bpwni') ?>" class="btn btn-block btn-primary">Tambah Data BPWNI</a></h3> -->
						<h3 class="box-title"><a href="<?php echo site_url('tambah-f108') ?>" class="btn btn-block btn-primary">Tambah Data Form F-1-08</a></h3>
						<h3 class="box-title"><a href="<?php echo site_url('tambah-f103') ?>" class="btn btn-block btn-primary">Tambah Data Form F-1-03</a></h3>
					</div>
					<!-- /.box-header -->
					<?php if (isset($_SESSION['type'])) { ?>
						<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $_SESSION['isi']; ?>
						</div>
						<?php
						unset($_SESSION['type']);
						unset($_SESSION['isi']);
						unset($_SESSION['judul']);
						?>
					<?php } ?>

					<div class="box-body">
						<div class="row">
							<div class="col-md-3">
								<strong>Filter Data</strong>
								<div class="form-group">
									<select class="form-control select2" name="FdataPindah" id="FdataPindah" onchange="itemFtrAkte()">
										<option value="Pindah" <?php if ($_SESSION['fsuratPindah'] == 'Pindah') echo "selected"; ?>>Pindah</option>
										<option value="F108" <?php if ($_SESSION['fsuratPindah'] == 'F108') echo "selected"; ?>>F.1-08</option>
										<option value="F103" <?php if ($_SESSION['fsuratPindah'] == 'F103') echo "selected"; ?>>F.1-03</option>
										<!-- <option value="semua" <?php if ($_SESSION['fKatGudang'] == 'semua') echo "selected"; ?>>Semua</option> -->
									</select>
								</div>
							</div>
						</div>

						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($pindah as $val) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?php echo format_indo(date("Y-m-d", strtotime($val->created_at)))  ?></td>
											<td><?php echo $val->id_pengajuan; ?></td>
											<td><?= $val->nama_pemohon ?></td>
											<?php if ($val->status_pembuatan == 'Selesai') { ?>
												<td><small class="label label-success"> Selesai</small></td>
											<?php } else { ?>
												<td><small class="label label-danger"> Dalam Proses</small></td>
											<?php } ?>
											<td>
												<?php if ($val->jenis_surat == 'pindah') { ?>
													<a href="<?php echo site_url('bpwni/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Data BPWNI"><i class="fa fa-plus"></i></a>
												<?php } ?>

												<?php if ($val->jenis_surat == 'pindah') { ?>
													<a href="<?php echo site_url('edit-suket-pindah/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<?php } else if ($val->jenis_surat == 'pindah-f108') { ?>
													<a href="<?php echo site_url('edit-f108/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<?php } else { ?>
													<a href="<?php echo site_url('edit-f103/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<?php } ?>

												<?php if ($val->jenis_surat == 'pindah') { ?>
													<a href="<?php echo site_url('lihat-suket-pindah/' . $val->id_surat) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>
												<?php } else if ($val->jenis_surat == 'pindah-f108') { ?>
													<a href="<?php echo site_url('lihat-f108/' . $val->id_surat) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>
												<?php } else { ?>
													<a href="<?php echo site_url('lihat-f103/' . $val->id_surat) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>
												<?php } ?>

												<a onclick="doneConfirm('<?php echo site_url('done-suket-pindah/' . $val->id_surat) ?>')" href="#" class="btn btn-warning btn-sm" title="Konfirmasi Selesai"><i class="fa fa-check"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-suket-pindah/' . $val->id_surat) ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
	function itemFtrAkte() {
		var dataPindah = $('#FdataPindah').val();
		$.ajax({
			url: '<?php echo base_url('admin/C_Pindah/filterdataPindah') ?>',
			method: "POST",
			data: {
				dataPindah: dataPindah
			},
			async: false,
			dataType: 'json',
			success: function() {
				// $('#dataProduk').html('');
				// $('#dataProduk').append(data);
				// $("#dataPengguna").DataTable({
				//   "responsive": true,
				//   "autoWidth": false,
				// });
			}
		});
		window.location.href = "<?php echo site_url('layanan-suket-pindah-admin') ?>";
	}
</script>


<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="doneModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Konfirmasi ke pemohon bahwa proses pembuatan surat telah selesai?</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-done" class="btn btn-success" href="#">Iya</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="lihatModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lihat Detail Data</h4>
			</div>
			<div class="modal-body">
				<form method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Data yang Ingin Dilihat</label>
								<select class="form-control select2" style="width: 100%;" name="fpilihdata">
									<option value="">--- Pilih ---</option>
									<option value="Pengantar">Pengantar</option>
									<option value="Pernyataan">Pernyataan</option>
									<option value="F201">F-2.01</option>
								</select>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-lihat" class="btn btn-danger" href="#">Lihat</a>
			</div>
		</div>
	</div>
</div>
