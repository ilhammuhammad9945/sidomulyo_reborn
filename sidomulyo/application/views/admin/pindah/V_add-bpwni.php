<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">BPWNI</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" placeholder="Masukkan NIK Pemohon" tabindex="1" autofocus />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div> -->

		<!-- <div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->



		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah BPWNI</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_pindah/prosesSimpanAddBpwni') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">

					<div class="row">
						<div class="col-md-12">
							<input type="hidden" name="id_surat" value="<?= $id_surat ?>" class="form-control" tabindex="5" readonly required>
							<!-- Custom Tabs -->
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<?php foreach ($pengikut as $key => $value) : $no++; ?>
										<li <?php if ($no == 1) {
												echo 'class="active"';
											} ?>><a href="#tab_<?= $no; ?>" data-toggle="tab">Pengikut <?= $no; ?></a></li>
									<?php endforeach; ?>

									<!-- <li class="dropdown">
			                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
			                  Dropdown <span class="caret"></span>
			                </a>
			                <ul class="dropdown-menu">
			                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
			                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
			                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
			                  <li role="presentation" class="divider"></li>
			                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
			                </ul>
			              </li> -->
									<li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
								</ul>
								<div class="tab-content">
									<?php foreach ($pengikut as $key => $value) : $noo++; ?>
										<div class="tab-pane <?php if ($noo == 1) {
																	echo 'active';
																} ?>" id="tab_<?= $noo; ?>">
											<h4><b>Tambah BPWNI Pengikut <?= $noo; ?></b></h4>
											<p>Harap mengisi secara lengkap dan teliti data setiap pengikut. </p><br>

											<!-- Form nya -->

											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label>NIK Pemohon</label>
														<input type="hidden" name="fid_pengikut[]" value="<?= $value->id_pengikut ?>" class="form-control" tabindex="5">
														<input type="text" name="fnikpemohon[]" value="<?= $value->pengikut_nik ?>" class="form-control" tabindex="5">
													</div>
													<div class="form-group">
														<label>Nama Lengkap</label>
														<input type="text" name="fnamapemohon[]" value="<?= $value->pengikut_nama ?>" class="form-control" tabindex="4">
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>Tempat Lahir</label>
																<input type="text" name="ftmptlahirpemohon[]" class="form-control" tabindex="5">
															</div>
															<!-- /.form-group -->
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Tanggal Lahir</label>
																<input type="date" name="ftgllahirpemohon[]" class="form-control" tabindex="6">
															</div>
															<!-- /.form-group -->
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>Jenis Kelamin</label><br>
																<select class="form-control select2" style="width: 100%;" name="fgenderpemohon[]" tabindex="7" required>
																	<option value="">--- Pilih ---</option>
																	<option value="L" <?php if ($value->pengikut_gender == 'L') {
																							echo 'selected';
																						} ?>>Laki-laki</option>
																	<option value="P" <?php if ($value->pengikut_gender == 'P') {
																							echo 'selected';
																						} ?>>Perempuan</option>
																</select>
															</div>
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Gol Darah</label>
																<input type="text" name="fgoldarahpemohon[]" class="form-control" tabindex="5">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>Agama</label><br>
																<select class="form-control select2" style="width: 100%;" name="fagamapemohon[]" tabindex="7" required>
																	<option value="">--- Pilih ---</option>
																	<option value="Islam">Islam</option>
																	<option value="Kristen">Kristen</option>
																	<option value="Hindu">Hindu</option>
																	<option value="Budha">Budha</option>
																</select>
															</div>
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Pendidikan Terakhir</label><br>
																<select class="form-control select2" style="width: 100%;" name="fpendidikanpemohon[]" tabindex="7" required>
																	<option value="">--- Pilih ---</option>
																	<option value="SD" <?php if ($value->pengikut_pendidikan == 'SD') {
																							echo 'selected';
																						} ?>>SD</option>
																	<option value="SMP" <?php if ($value->pengikut_pendidikan == 'SMP') {
																							echo 'selected';
																						} ?>>SMP</option>
																	<option value="SMA" <?php if ($value->pengikut_pendidikan == 'SMA') {
																							echo 'selected';
																						} ?>>SMA</option>
																	<option value="Akademik" <?php if ($value->pengikut_pendidikan == 'Akademik') {
																									echo 'selected';
																								} ?>>Akademik</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label>Pekerjaan</label>
														<input type="text" name="fpekerjaanpemohon[]" class="form-control" tabindex="14">
													</div>
													<div class="form-group">
														<label>Alamat Sebelumnya</label>
														<input type="text" name="falamatsebelumnya[]" class="form-control" tabindex="14">
													</div>
												</div>
												<!-- /.col -->
												<div class="col-md-6">
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>Penyandang Cacat</label>
																<input type="text" name="fcacatpemohon[]" class="form-control" tabindex="13">
															</div>
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Status Perkawinan</label><br>
																<select class="form-control select2" style="width: 100%;" name="fstatusnikahpemohon[]" tabindex="7" required>
																	<option value="">--- Pilih ---</option>
																	<option value="Belum Kawin" <?php if ($value->pengikut_status_kawin == 'Belum Kawin') {
																									echo 'selected';
																								} ?>>Belum Kawin</option>
																	<option value="Kawin" <?php if ($value->pengikut_status_kawin == 'Kawin') {
																								echo 'selected';
																							} ?>>Kawin</option>
																	<option value="Cerai Hidup" <?php if ($value->pengikut_status_kawin == 'Cerai Hidup') {
																									echo 'selected';
																								} ?>>Cerai Hidup</option>
																	<option value="Cerai Mati" <?php if ($value->pengikut_status_kawin == 'Cerai Mati') {
																									echo 'selected';
																								} ?>>Cerai Mati</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label>Status Hubungan Dalam Keluarga</label>
														<input type="text" name="fstatushubunganpemohon[]" class="form-control" tabindex="13" required>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>NIK Ibu</label>
																<input type="text" name="fnikibu[]" class="form-control" tabindex="17" required>
															</div>
															<!-- /.form-group -->
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Nama Ibu</label>
																<input type="text" name="fnamaibu[]" class="form-control" tabindex="18" required>
															</div>
															<!-- /.form-group -->
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>NIK Ayah</label>
																<input type="text" name="fnikayah[]" class="form-control" tabindex="17" required>
															</div>
															<!-- /.form-group -->
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Nama Ayah</label>
																<input type="text" name="fnamaayah[]" class="form-control" tabindex="18" required>
															</div>
															<!-- /.form-group -->
														</div>
													</div>
													<!-- /.form-group -->
													<div class="form-group">
														<label>Alamat Sekarang</label>
														<input type="text" name="falamatsekarang[]" class="form-control" tabindex="14">
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>Desa</label>
																<input type="text" name="fdesa[]" class="form-control" tabindex="15">
															</div>
															<!-- /.form-group -->
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Kecamatan</label>
																<input type="text" name="fkecamatan[]" class="form-control" tabindex="16">
															</div>
															<!-- /.form-group -->
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<div class="form-group">
																<label>Kabupaten / Kota</label>
																<input type="text" name="fkabkota[]" class="form-control" tabindex="17">
															</div>
															<!-- /.form-group -->
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>Provinsi</label>
																<input type="text" name="fprovinsi[]" class="form-control" tabindex="18">
															</div>
															<!-- /.form-group -->
														</div>
													</div>
												</div>
												<!-- /.col -->
											</div>
											<!-- /.row -->

											<h4><b>DATA KEPEMILIKAN DOKUMEN</b></h4>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label>No KK</label>
														<input type="text" name="fnokk[]" class="form-control" tabindex="4">
													</div>
													<!-- /.form-group -->
													<div class="form-group">
														<label>No Paspor <i>(opsional)</i></label>
														<input type="text" name="fnopaspor[]" class="form-control" tabindex="4">
													</div>
													<!-- /.form-group -->
													<div class="form-group">
														<label>Tanggal Berakhir Paspor <i>(opsional)</i></label>
														<input type="date" name="ftglexpiredpaspor[]" class="form-control" tabindex="4">
													</div>
													<div class="form-group">
														<label>Nomor Akta / Surat Kenal Lahir</label>
														<input type="text" name="fnoakta[]" class="form-control" tabindex="4">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label>Nomor Buku Nikah</label>
														<input type="text" name="fbukunikah[]" class="form-control" tabindex="4">
													</div>
													<div class="form-group">
														<label>Tanggal Perkawinan</label>
														<input type="date" name="ftglkawin[]" class="form-control" tabindex="4">
													</div>
													<div class="form-group">
														<label>Nomor Akta Perceraian <i>(opsional)</i></label>
														<input type="text" name="fnoaktaperceraian[]" class="form-control" tabindex="4">
													</div>
													<div class="form-group">
														<label>Tanggal Perceraian <i>(opsional)</i></label>
														<input type="date" name="ftglperceraian[]" class="form-control" tabindex="4">
													</div>
												</div>
											</div>

											<!-- Batas Form nya -->
										</div>
										<!-- /.tab-pane -->
									<?php endforeach; ?>
								</div>
								<!-- /.tab-content -->
							</div>
							<!-- nav-tabs-custom -->
						</div>
						<!-- /.col (left) -->
					</div>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpanBpwni" value="Simpan" tabindex="19" />
					<a href="<?php echo site_url('layanan-suket-pindah-admin') ?>" class="btn btn-default" tabindex="20">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
