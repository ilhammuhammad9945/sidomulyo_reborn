<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-tv"></i> Berita</a></li>
			<li class="active">Data Berita</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if (isset($_SESSION['type'])) { ?>
					<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $_SESSION['isi']; ?>
					</div>
					<?php
					unset($_SESSION['type']);
					unset($_SESSION['isi']);
					unset($_SESSION['judul']);
					?>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('admin/C_berita/tambah_berita') ?>" class="btn btn-block btn-primary">Tambah Data</a></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Judul</th>
										<th>Isi</th>
										<th>Editor</th>
										<th>Status</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($berita as $val) : ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td>
												<?php
												if ($val->tampil == 0) {
													echo "-";
												} else {
													echo date("d/m/Y", strtotime($val->tgl_terbit));
												}
												?>
											</td>
											<td><?php echo $val->judul; ?></td>
											<td>
												<div class="limitText<?= $val->id_berita; ?>"><?php echo $val->isi; ?></div>
											</td>
											<td><?php echo $val->nama_user; ?></td>
											<td>
												<button type="button" id="rA<?= $val->id_berita; ?>" onclick="ubahStatusOff('<?= $val->id_berita; ?>')" class="btn btn-danger btn-sm" <?php if ($val->tampil == 0) {
																																															echo 'style="display:none"';
																																														} ?> data-toggle="tooltip" title="Nonaktifkan"><i class="fa fa-power-off"></i></button>
												<button type="button" id="rB<?= $val->id_berita; ?>" onclick="ubahStatusOn('<?= $val->id_berita; ?>')" class="btn btn-success btn-sm" <?php if ($val->tampil == 1) {
																																															echo 'style="display:none"';
																																														} ?> data-toggle="tooltip" title="Publish"><i class="fa fa-power-off"></i></button>
												<?php if ($val->tampil == 1) {
													echo '<small class="label bg-green">Publish</small>';
												} else {
													echo '<small class="label bg-red">Draft</small>';
												} ?>
											</td>
											<td><a href="<?php echo site_url('admin/C_berita/edit/' . $val->id_berita) ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('admin/C_berita/hapus/' . $val->id_berita) ?>')" href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>

								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Judul</th>
										<th>Isi</th>
										<th>Editor</th>
										<th>Status</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($berita as $val) : ?>
		var no = "<?= $val->id_berita; ?>";
		var text = $(".limitText" + no).text();
		var len = text.length;

		if (len > 20) {
			$(".limitText" + no).text($(".limitText" + no).text().substr(0, 19) + '... ');
		}
	<?php endforeach; ?>

	function ubahStatusOff(id) {
		var A = document.getElementById("rA" + id);
		var B = document.getElementById("rB" + id);
		var txt = confirm("Apakah Anda yakin untuk menonaktifkan berita ini?");
		if (txt) {

			$.ajax({
				url: '<?php echo base_url('admin/C_berita/prosesStatusOff') ?>',
				method: "POST",
				data: {
					idb: id
				},
				async: false,
				dataType: 'json',
				success: function(data) {

					A.style.display = "none";
					B.style.display = "inline";
				}
			});
		}
		window.location = "<?= base_url('admin/C_berita') ?>";
	}

	function ubahStatusOn(id) {
		var A = document.getElementById("rA" + id);
		var B = document.getElementById("rB" + id);
		var txt = confirm("Apakah Anda yakin untuk mempublish berita ini?");
		if (txt) {

			$.ajax({
				url: '<?php echo base_url('admin/C_berita/prosesStatusOn') ?>',
				method: "POST",
				data: {
					idb: id
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					A.style.display = "inline";
					B.style.display = "none";
				}
			});

		}
		window.location = "<?= base_url('admin/C_berita') ?>";
	}
</script>
