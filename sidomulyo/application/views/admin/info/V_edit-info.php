<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Aktif
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">Info</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Info</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_info/edit_info') ?>" method="post">
				<div class="box-body">
					<?php foreach ($info as $key => $value) : ?>
						<div class="row">

							<div class="col-md-12">
								<div class="form-group">
									<label>Isi Info</label>
									<input type="hidden" name="id_kt" value="<?= $value->id_kt ?>">
									<textarea class="form-control" name="isi" rows="5" maxlength="250" placeholder="Maksimal 250 karakter"><?= $value->isi_kt; ?></textarea>
								</div>

							</div>
						</div>
						<!-- /.row -->
					<?php endforeach; ?>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanInfo" value="Simpan" />
					<a href="<?php echo site_url('admin/C_info') ?>" class="btn btn-default">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->

		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper
