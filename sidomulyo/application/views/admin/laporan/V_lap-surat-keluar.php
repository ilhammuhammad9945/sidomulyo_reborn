<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Laporan</a></li>
			<li class="active">Surat Keluar</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Data Laporan Surat Keluar</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
            <form method="post" action="<?= site_url('admin/C_laporan/filter_lapSK')?>">
              <div class="row" style="margin-bottom:10px">
                <div class="col-md-2">
                  <strong>Filter Bulan</strong>
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" name="FBulan" id="FBulan">
                      <option value="01" <?php if ($_SESSION['fBulanLapSK'] == '01') { echo "selected"; } ?>>Januari</option>
                      <option value="02" <?php if ($_SESSION['fBulanLapSK'] == '02') { echo "selected"; } ?>>Februari</option>
                      <option value="03" <?php if ($_SESSION['fBulanLapSK'] == '03') { echo "selected"; } ?>>Maret</option>
                      <option value="04" <?php if ($_SESSION['fBulanLapSK'] == '04') { echo "selected"; } ?>>April</option>
                      <option value="05" <?php if ($_SESSION['fBulanLapSK'] == '05') { echo "selected"; } ?>>Mei</option>
                      <option value="06" <?php if ($_SESSION['fBulanLapSK'] == '06') { echo "selected"; } ?>>Juni</option>
                      <option value="07" <?php if ($_SESSION['fBulanLapSK'] == '07') { echo "selected"; } ?>>Juli</option>
                      <option value="08" <?php if ($_SESSION['fBulanLapSK'] == '08') { echo "selected"; } ?>>Agustus</option>
                      <option value="09" <?php if ($_SESSION['fBulanLapSK'] == '09') { echo "selected"; } ?>>September</option>
                      <option value="10" <?php if ($_SESSION['fBulanLapSK'] == '10') { echo "selected"; } ?>>Oktober</option>
                      <option value="11" <?php if ($_SESSION['fBulanLapSK'] == '11') { echo "selected"; } ?>>November</option>
                      <option value="12" <?php if ($_SESSION['fBulanLapSK'] == '12') { echo "selected"; } ?>>Desember</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                  <strong>Filter Tahun</strong>
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" name="FTahun" id="FTahun">
											<option value="2021" <?php if ($_SESSION['fTahunLapSK'] == '2021') { echo "selected"; } ?>>2021</option>
                      <option value="2022" <?php if ($_SESSION['fTahunLapSK'] == '2022') { echo "selected"; } ?>>2022</option>
                      <option value="2023" <?php if ($_SESSION['fTahunLapSK'] == '2023') { echo "selected"; } ?>>2023</option>
                      <option value="2024" <?php if ($_SESSION['fTahunLapSK'] == '2024') { echo "selected"; } ?>>2024</option>
                      <option value="2025" <?php if ($_SESSION['fTahunLapSK'] == '2025') { echo "selected"; } ?>>2025</option>
                      <option value="2026" <?php if ($_SESSION['fTahunLapSK'] == '2026') { echo "selected"; } ?>>2026</option>
                      <option value="2027" <?php if ($_SESSION['fTahunLapSK'] == '2027') { echo "selected"; } ?>>2027</option>
                      <option value="2028" <?php if ($_SESSION['fTahunLapSK'] == '2028') { echo "selected"; } ?>>2028</option>
                      <option value="2029" <?php if ($_SESSION['fTahunLapSK'] == '2029') { echo "selected"; } ?>>2029</option>
                      <option value="2030" <?php if ($_SESSION['fTahunLapSK'] == '2030') { echo "selected"; } ?>>2030</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                  <strong>Filter Jenis</strong>
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" name="FJenis" id="FJenis">
                      <option value="semua" <?php if($_SESSION['fJenisLapSK'] == 'semua') { echo "selected"; }?>>Semua</option>
                      <option value="akte" <?php if($_SESSION['fJenisLapSK'] == 'akte') { echo "selected"; }?> >Akte Kelahiran</option>
                      <option value="sktm" <?php if($_SESSION['fJenisLapSK'] == 'sktm') { echo "selected"; }?> >SKTM</option>
                      <option value="skck" <?php if($_SESSION['fJenisLapSK'] == 'skck') { echo "selected"; }?> >SKCK</option>
                      <option value="kematian" <?php if($_SESSION['fJenisLapSK'] == 'kematian') { echo "selected"; }?> >Ket. Kematian</option>
                      <option value="identitas" <?php if($_SESSION['fJenisLapSK'] == 'identitas') { echo "selected"; }?> >Ket. Beda Identitas</option>
                      <option value="domisili" <?php if($_SESSION['fJenisLapSK'] == 'domisili') { echo "selected"; }?> >Ket. Domisili</option>
                      <option value="usaha" <?php if($_SESSION['fJenisLapSK'] == 'usaha') { echo "selected"; }?> >Ket. Usaha</option>
                      <option value="pindah" <?php if($_SESSION['fJenisLapSK'] == 'pindah') { echo "selected"; }?> >Ket. Pindah</option>
                      <option value="rekamktp" <?php if($_SESSION['fJenisLapSK'] == 'rekamktp') { echo "selected"; }?> >Ket. Telah rekam KTP</option>
                      <option value="kehilangan" <?php if($_SESSION['fJenisLapSK'] == 'kehilangan') { echo "selected"; }?> >Ket. Kehilangan</option>
											<option value="berpergian" <?php if($_SESSION['fJenisLapSK'] == 'berpergian') { echo "selected"; }?> >Ket. Berpergian</option>
											<option value="bsm" <?php if($_SESSION['fJenisLapSK'] == 'bsm') { echo "selected"; }?> >BSM</option>
											<option value="kenal lahir" <?php if($_SESSION['fJenisLapSK'] == 'kenal lahir') { echo "selected"; }?> >Kenal Lahir</option>
											<option value="pelaporan kematian" <?php if($_SESSION['fJenisLapSK'] == 'pelaporan kematian') { echo "selected"; }?> >Pelaporan Kematian</option>
                      <option value="belum nikah" <?php if($_SESSION['fJenisLapSK'] == 'belum nikah') { echo "selected"; }?> >Belum Nikah</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                  <strong style="opacity: 0">Tombol</strong>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-md" name="filterTgl" data-toggle="tooltip" title="Filter"><i class="fa fa-filter"></i></button>
                  </div>
                </div>
                </div>
              </form>
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($surat as $val) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?php echo format_indo(date("Y-m-d", strtotime($val->created_at)))  ?></td>
											<td><?php echo $val->id_pengajuan; ?></td>
											<td><?= $val->nama_pemohon ?></td>
											<?php if ($val->status_pembuatan == 'Selesai') { ?>
												<td><small class="label label-success"> Selesai</small></td>
											<?php } else { ?>
												<td><small class="label label-danger"> Dalam Proses</small></td>
											<?php } ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
									</tr>
								</tfoot>
							</table><br>
							<a href="<?php echo site_url('print-laporan') ?>" class="btn btn-primary" target="_blank">Cetak Laporan Surat</a>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="doneModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Konfirmasi ke pemohon bahwa proses pembuatan surat telah selesai?</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-done" class="btn btn-success" href="#">Iya</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="lihatModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lihat Detail Data</h4>
			</div>
			<div class="modal-body">
				<form method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Data yang Ingin Dilihat</label>
								<select class="form-control select2" style="width: 100%;" name="fpilihdata">
									<option value="">--- Pilih ---</option>
									<option value="Pengantar">Pengantar</option>
									<option value="Pernyataan">Pernyataan</option>
									<option value="F201">F-2.01</option>
								</select>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-lihat" class="btn btn-danger" href="#">Lihat</a>
			</div>
		</div>
	</div>
</div>
