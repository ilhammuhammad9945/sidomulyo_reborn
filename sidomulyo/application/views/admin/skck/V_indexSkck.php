<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li>Permohonan Surat</li>
			<li class="active">Data SKCK</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><a href="<?php echo site_url('tambah-skck') ?>" class="btn btn-block btn-primary">Tambah Data SKCK</a></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped nowrap" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($skck as $val) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?php echo format_indo(date("Y-m-d", strtotime($val->created_at)))  ?></td>
											<td><?php echo $val->id_pengajuan; ?></td>
											<td><?= $val->nama_pemohon ?></td>
											<?php if ($val->status_pembuatan == 'Selesai') { ?>
												<td><small class="label label-success"> Selesai</small></td>
											<?php } else { ?>
												<td><small class="label label-danger"> Dalam Proses</small></td>
											<?php } ?>
											<td>
												<a href="<?php echo site_url('edit-skck/' . $val->id_surat) ?>" class="btn btn-success btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>
												<a href="<?php echo site_url('lihat-skck/' . $val->id_surat) ?>" class="btn btn-info btn-sm" title="Lihat Data"><i class="fa fa-eye"></i></a>
												<a onclick="doneConfirm('<?php echo site_url('done-skck/' . $val->id_surat) ?>')" href="#" class="btn btn-warning btn-sm" title="Konfirmasi Selesai"><i class="fa fa-check"></i></a>
												<a onclick="deleteConfirm('<?php echo site_url('hapus-skck/' . $val->id_surat) ?>')" href="#" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tanggal Pengajuan</th>
										<th>ID Pengajuan</th>
										<th>Nama Pemohon</th>
										<th>Status Pembuatan</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Data yang dihapus tidak akan bisa dikembalikan</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="doneModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Anda yakin?</h4>
			</div>
			<div class="modal-body">
				<p><strong>Konfirmasi ke pemohon bahwa proses pembuatan surat telah selesai?</strong></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-done" class="btn btn-success" href="#">Iya</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="lihatModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lihat Detail Data</h4>
			</div>
			<div class="modal-body">
				<form method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Data yang Ingin Dilihat</label>
								<select class="form-control select2" style="width: 100%;" name="fpilihdata">
									<option value="">--- Pilih ---</option>
									<option value="Pengantar">Pengantar</option>
									<option value="Pernyataan">Pernyataan</option>
									<option value="F201">F-2.01</option>
								</select>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a id="btn-lihat" class="btn btn-danger" href="#">Lihat</a>
			</div>
		</div>
	</div>
</div>
