<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">SKCK</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit SKCK</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('proses-edit-skck') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PEMOHON :</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>NIK Pemohon</label>
								<input type="hidden" name="id_surat" value="<?= $surat['id_surat']; ?>" readonly required>
								<input type="text" class="form-control" name="skck_nik" id="skck_nik" value="<?= $surat['nik_pemohon']; ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Lengkap Pemohon</label>
								<input type="text" class="form-control" name="skck_nama_lengkap" id="skck_nama_lengkap" value="<?= $surat['nama_pemohon']; ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" name="skck_gender" id="skck_gender" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L" <?php if ($surat['skck_gender'] == 'L') {
															echo "selected";
														} ?>>Laki-laki</option>
									<option value="P" <?php if ($surat['skck_gender'] == 'P') {
															echo "selected";
														} ?>>Perempuan</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tempat Lahir</label>
								<input type="text" class="form-control" name="skck_tempat_lahir" id="skck_tempat_lahir" value="<?= $surat['skck_tempat_lahir']; ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Lahir</label>
								<input type="date" class="form-control" name="skck_tanggal_lahir" id="skck_tanggal_lahir" value="<?= $surat['skck_tanggal_lahir']; ?>" tabindex="7">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Kewarganegaraan</label>
								<input type="text" class="form-control" name="skck_kwn" id="skck_kwn" value="<?= $surat['skck_kwn']; ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Agama</label>
								<input type="text" class="form-control" name="skck_agama" id="skck_agama" value="<?= $surat['skck_agama']; ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Status Perkawinan</label>
								<input type="text" class="form-control" name="skck_status_kawin" id="skck_status_kawin" value="<?= $surat['skck_status_kawin']; ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>No KTP atau KK</label>
								<input type="text" class="form-control" name="skck_ktp_kk" id="skck_ktp_kk" value="<?= $surat['skck_ktp_kk']; ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" class="form-control" name="skck_pekerjaan" id="skck_pekerjaan" value="<?= $surat['skck_pekerjaan']; ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Pendidikan</label>
								<input type="text" class="form-control" name="skck_pendidikan" id="skck_pendidikan" value="<?= $surat['skck_pendidikan']; ?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Keperluan Pembuatan</label>
								<input type="text" class="form-control" name="skck_keperluan" id="sskck_keperluan" value="<?= $surat['skck_keperluan']; ?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Alamat Lengkap</label>
						<input type="text" class="form-control" name="skck_alamat" id="skck_alamat" value="<?= $surat['skck_alamat']; ?>" tabindex="4">
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanEditSkck" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-skck-admin') ?>" class="btn btn-default" tabindex="16">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
