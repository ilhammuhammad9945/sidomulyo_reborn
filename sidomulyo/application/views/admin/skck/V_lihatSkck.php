<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">SKCK</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail SKCK</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($skck['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $skck['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Nama Pemohon</td>
										<td>: <?= $skck['nama_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($skck['skck_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Lahir </td>
										<td>: <?= $skck['skck_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($skck['skck_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan</td>
										<td>: <?= $skck['skck_kwn']; ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $skck['skck_agama']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $skck['skck_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>No KTP / KK</td>
										<td>: <?= $skck['skck_ktp_kk']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $skck['skck_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Pendidikan </td>
										<td>: <?= $skck['skck_pendidikan']; ?></td>
									</tr>
									<tr>
										<td>Alamat </td>
										<td>: <?= $skck['skck_alamat']; ?></td>
									</tr>
									<tr>
										<td>Keperluan </td>
										<td>: <?= $skck['skck_keperluan']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-skck/' . $skck['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak SKCK</a>
						<a href="<?php echo site_url('layanan-skck-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
