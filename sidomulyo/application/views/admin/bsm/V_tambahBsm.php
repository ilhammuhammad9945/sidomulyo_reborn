<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">BSM</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php elseif ($this->session->flashdata('error')) : ?>
			<div class="alert alert-error" role="alert">
				<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php endif; ?>

		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" id="nikpemohon" name="fnikpemohon" autofocus tabindex="1" />
						</div>
						
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" id="tglpemohon" name="ftglpemohon" tabindex="2" />
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="box-footer">
				<button class="btn btn-primary" type="button" id="btnCari" name="btnCariPemohon" tabindex="3">Cari Data</button>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Bantuan Siswa Miskin</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('tambah-bsm') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" class="form-control" name="fidpengajuanbsm" value="<?php echo $invoice; ?>" readonly>
							<!-- <div class="form-group">
								<label>No. Surat</label>
								<input type="text" class="form-control" id="nosurat" name="fnosurat" tabindex="1" autofocus onkeypress="return hanyaAngka(event)">
							</div> -->
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Pemohon</label>
								<input type="text" class="form-control" id="nik" name="fnik" tabindex="1" autofocus onkeypress="return hanyaAngka(event)">
								<span style="color: red">
									<?php echo form_error('fnik') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Pemohon</label>
								<input type="text" class="form-control" id="namalengkap" name="fnamalengkap" tabindex="2">
								<span style="color: red">
									<?php echo form_error('fnamalengkap') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Umur Pemohon</label>
								<input type="number" class="form-control" id="umur" name="fumur" tabindex="3">
								<span style="color: red">
									<?php echo form_error('fumur') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Jenis Kelamin Pemohon</label>
								<select class="form-control select2" style="width: 100%;" id="jkel" name="fjkel" tabindex="4">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Agama Pemohon</label>
								<select class="form-control select2" style="width: 100%;" id="agama" name="fagama" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="Islam">Islam</option>
									<option value="Protestan">Protestan</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Khonghucu">Khonghucu</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fagama') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Status Perkawinan Pemohon</label>
								<select class="form-control select2" style="width: 100%;" id="statuskawin" name="fstatuskawin" tabindex="6">
									<option value="">--- Pilih ---</option>
									<option value="Kawin">Kawin</option>
									<option value="Belum Kawin">Belum Kawin</option>
									<option value="Cerai Hidup">Cerai Hidup</option>
									<option value="Cerai Mati">Cerai Mati</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fstatuskawin') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">

							<div class="form-group">
								<label>Pekerjaan Pemohon</label>
								<input type="text" id="pekerjaan" name="fpekerjaan" class="form-control" tabindex="7">
								<span style="color: red">
									<?php echo form_error('fpekerjaan') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Penghasilan per Bulan</label>
								<input type="text" class="form-control" id="penghasilan" name="fpenghasilan" tabindex="8" onkeypress="return hanyaAngka(event)">
								<span style="color: red">
									<?php echo form_error('fpenghasilan') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Alamat Pemohon</label>
								<input type="text" id="alamat" name="falamat" class="form-control" tabindex="9">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Anak</label>
								<input type="text" id="namaanak" name="fnamaanak" class="form-control" tabindex="10">
								<span style="color: red">
									<?php echo form_error('fnamaanak') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Nama Sekolah Anak</label>
								<input type="text" id="sekolah" name="fsekolah" class="form-control" tabindex="11">
								<span style="color: red">
									<?php echo form_error('fsekolah') ?>
								</span>
							</div>
							<!-- /.form-group -->

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" id="btnSimpan" name="btnSimpan" value="Simpan" tabindex="12" />
					<a href="<?php echo site_url('layanan-bsm-admin') ?>" class="btn btn-default" tabindex="13">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
