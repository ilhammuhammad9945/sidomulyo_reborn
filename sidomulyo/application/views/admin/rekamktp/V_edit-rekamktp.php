<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">SKCK</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit SKCK</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_rekamktp/prosesSimpanEdit') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PEMOHON :</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>NIK Pemohon</label>
								<input type="hidden" class="form-control" name="id_surat" id="id_surat" value="<?= $rekam_ktp['id_surat']?>" tabindex="4">
								<input type="text" class="form-control" name="rk_nik" id="rk_nik" value="<?= $rekam_ktp['nik_pemohon']?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Lengkap Pemohon</label>
								<input type="text" class="form-control" name="rk_nama_lengkap" id="rk_nama_lengkap" value="<?= $rekam_ktp['nama_pemohon']?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" name="rk_gender" id="rk_gender" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L" <?php if($rekam_ktp['rk_gender'] == 'L'){ echo "selected"; } ?>>Laki-laki</option>
									<option value="P" <?php if($rekam_ktp['rk_gender'] == 'P'){ echo "selected"; } ?>>Perempuan</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tempat Lahir</label>
								<input type="text" class="form-control" name="rk_tempat_lahir" id="rk_tempat_lahir" value="<?= $rekam_ktp['rk_tempat_lahir']?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Lahir</label>
								<input type="date" class="form-control" name="rk_tanggal_lahir" id="rk_tanggal_lahir" value="<?= $rekam_ktp['rk_tanggal_lahir']?>" tabindex="7">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Agama</label><br>
								<select class="form-control select2" style="width: 100%;" name="rk_agama" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Islam" <?php if($rekam_ktp['rk_agama'] == 'Islam'){ echo "selected"; } ?>>Islam</option>
									<option value="Kristen" <?php if($rekam_ktp['rk_agama'] == 'Kristen'){ echo "selected"; } ?>>Kristen</option>
									<option value="Hindu" <?php if($rekam_ktp['rk_agama'] == 'Hindu'){ echo "selected"; } ?>>Hindu</option>
									<option value="Budha" <?php if($rekam_ktp['rk_agama'] == 'Budha'){ echo "selected"; } ?>>Budha</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" class="form-control" name="rk_pekerjaan" id="rk_pekerjaan" value="<?= $rekam_ktp['rk_pekerjaan']?>" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Pendidikan</label>
								<input type="text" class="form-control" name="rk_pendidikan" id="rk_pendidikan" value="<?= $rekam_ktp['rk_pendidikan']?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Status Perkawinan</label><br>
								<select class="form-control select2" style="width: 100%;" name="rk_status_kawin" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Belum Kawin" <?php if($rekam_ktp['rk_status_kawin'] == 'Belum Kawin'){ echo "selected"; } ?>>Belum Kawin</option>
									<option value="Kawin" <?php if($rekam_ktp['rk_status_kawin'] == 'Kawin'){ echo "selected"; } ?>>Kawin</option>
									<option value="Cerai Hidup" <?php if($rekam_ktp['rk_status_kawin'] == 'Cerai Hidup'){ echo "selected"; } ?>>Cerai Hidup</option>
									<option value="Cerai Mati" <?php if($rekam_ktp['rk_status_kawin'] == 'Cerai Mati'){ echo "selected"; } ?>>Cerai Mati</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Kebutuhan</label>
								<input type="text" class="form-control" name="rk_kebutuhan" id="rk_kebutuhan" value="<?= $rekam_ktp['rk_kebutuhan']?>" tabindex="4">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Alamat Lengkap</label>
						<input type="text" class="form-control" name="rk_alamat" id="rk_alamat" value="<?= $rekam_ktp['rk_alamat']?>" tabindex="4">
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnEditRekamKtp" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-rekam-ktp-admin') ?>" class="btn btn-default" tabindex="16">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
