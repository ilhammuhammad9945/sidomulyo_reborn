<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Telah Rekam KTP</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-info">
							<h3>Tanggal Pengajuan Surat:</h3>
							<h4> <?= format_indo(date("Y-m-d H:i", strtotime($rekam_ktp['created_at']))) ?> WIB
							</h4>
						</div>
						<!-- /.mailbox-read-info -->

						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">

									<tr>
										<td>NIK Pemohon</td>
										<td>: <?= $rekam_ktp['nik_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Nama Pemohon</td>
										<td>: <?= $rekam_ktp['nama_pemohon']; ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:
											<?php if ($rekam_ktp['rk_gender'] == 'L') {
												echo "Laki-Laki";
											} else {
												echo "Perempuan";
											} ?>
										</td>
									</tr>
									<tr>
										<td>Tempat Lahir </td>
										<td>: <?= $rekam_ktp['rk_tempat_lahir']; ?></td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td>
										<td>: <?= format_indo(date('Y-m-d', strtotime($rekam_ktp['rk_tanggal_lahir']))) ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>: <?= $rekam_ktp['rk_agama']; ?></td>
									</tr>
									<tr>
										<td>Status Perkawinan</td>
										<td>: <?= $rekam_ktp['rk_status_kawin']; ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>: <?= $rekam_ktp['rk_pekerjaan']; ?></td>
									</tr>
									<tr>
										<td>Pendidikan </td>
										<td>: <?= $rekam_ktp['rk_pendidikan']; ?></td>
									</tr>
									<tr>
										<td>Alamat </td>
										<td>: <?= $rekam_ktp['rk_alamat']; ?></td>
									</tr>
									<tr>
										<td>Keperluan </td>
										<td>: <?= $rekam_ktp['rk_kebutuhan']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>

					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-rekam-ktp/' . $rekam_ktp['id_pengajuan']) ?>" class="btn btn-primary" target="_blank">Cetak Surat</a>
						<a href="<?php echo site_url('layanan-rekam-ktp-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
function format_hari($data)
{
	switch ($data) {
		case 'Sun':
			$data = "Minggu";
			break;

		case 'Mon':
			$data = "Senin";
			break;

		case 'Tue':
			$data = "Selasa";
			break;

		case 'Wed':
			$data = "Rabu";
			break;

		case 'Thu':
			$data = "Kamis";
			break;

		case 'Fri':
			$data = "Jumat";
			break;

		case 'Sat':
			$data = "Sabtu";
			break;

		default:
			$data = "Tidak di ketahui";
			break;
	}

	return $data;
}

?>
