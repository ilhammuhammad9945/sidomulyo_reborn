<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Telah Rekam KTP</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div> -->
		<!-- /.box-header -->
		<!-- <div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" name="fnikpemohon" autofocus tabindex="1" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" name="ftglpemohon" tabindex="2" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input class="btn btn-primary" type="submit" name="btnCariPemohon" value="Cari Data" tabindex="3" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" onclick="isManual()"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Data Ket. Telah Rekam KTP</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?= base_url('admin/C_rekamktp/prosesSimpanAdd') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>DATA PEMOHON :</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>NIK Pemohon</label>
								<input type="hidden" class="form-control" name="id_peng" id="id_peng" value="<?= $invoice ?>" tabindex="4">
								<input type="text" class="form-control" name="rk_nik" id="rk_nik" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama Lengkap Pemohon</label>
								<input type="text" class="form-control" name="rk_nama_lengkap" id="rk_nama_lengkap" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2" style="width: 100%;" name="rk_gender" id="rk_gender" tabindex="5">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tempat Lahir</label>
								<input type="text" class="form-control" name="rk_tempat_lahir" id="rk_tempat_lahir" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Lahir</label>
								<input type="date" class="form-control" name="rk_tanggal_lahir" id="rk_tanggal_lahir" tabindex="7">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Agama</label><br>
								<select class="form-control select2" style="width: 100%;" name="rk_agama" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Islam">Islam</option>
									<option value="Kristen">Kristen</option>
									<option value="Hindu">Hindu</option>
									<option value="Budha">Budha</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Pekerjaan</label>
								<input type="text" class="form-control" name="rk_pekerjaan" id="rk_pekerjaan" tabindex="4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Pendidikan</label>
								<input type="text" class="form-control" name="rk_pendidikan" id="rk_pendidikan" tabindex="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Status Perkawinan</label><br>
								<select class="form-control select2" style="width: 100%;" name="rk_status_kawin" tabindex="7" required>
									<option value="">--- Pilih ---</option>
									<option value="Belum Kawin">Belum Kawin</option>
									<option value="Kawin">Kawin</option>
									<option value="Cerai Hidup">Cerai Hidup</option>
									<option value="Cerai Mati">Cerai Mati</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Kebutuhan</label>
								<input type="text" class="form-control" name="rk_kebutuhan" id="rk_kebutuhan" tabindex="4">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Alamat Lengkap</label>
						<input type="text" class="form-control" name="rk_alamat" id="rk_alamat" tabindex="4">
					</div>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnSimpanRekamKtp" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-rekam-ktp-admin') ?>" class="btn btn-default" tabindex="16">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
	// $(function() {
	// 	$('#')
	// })
</script>
