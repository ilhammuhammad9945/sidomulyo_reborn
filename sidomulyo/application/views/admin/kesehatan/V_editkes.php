<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-database"></i> Data Desa</a></li>
			<li><a href="#">Kesehatan</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<!-- SELECT2 EXAMPLE -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Pengurus Poskesdes</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('edit-kesehatan') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="fid" value="<?php echo $kes->id_lm ?>" />
							<div class="form-group">
								<label>Nama Lengkap</label>
								<input type="text" class="form-control <?php echo form_error('fnama') ? 'is-invalid' : '' ?>" name="fnama" value="<?php echo $kes->nama_lm ?>" tabindex="1">
								<span style="color: red">
									<?php echo form_error('fnama') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<select class="form-control select2 <?php echo form_error('fjkel') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fjkel" tabindex="2">
									<option value="<?php echo $kes->jkel_lm ?>"><?php echo $kes->jkel_lm ?></option>
									<option value="Laki-laki">Laki-laki</option>
									<option value="Perempuan">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nomor Telepon</label>
								<input type="text" class="form-control" name="ftelp" value="<?php echo $kes->no_telp ?>" tabindex="3">
								<span style="color: red">
									<?php echo form_error('ftelp') ?>
								</span>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" class="form-control <?php echo form_error('falamat') ? 'is-invalid' : '' ?>" name="falamat" value="<?php echo $kes->alamat_lm ?>" tabindex="4">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jabatan</label>
								<select class="form-control select2 <?php echo form_error('fjabatan') ? 'is-invalid' : '' ?>" style="width: 100%;" name="fjabatan" tabindex="5">
									<option value="<?php echo $kes->jabatan_lm ?>"><?php echo $kes->nama_jbt ?></option>
									<?php foreach ($jabatan as $jbt) : ?>
										<option value="<?php echo $jbt->id_jbt; ?>"><?php echo $jbt->nama_jbt; ?></option>
									<?php endforeach; ?>
								</select>
								<span style="color: red">
									<?php echo form_error('fjabatan') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Upload Ulang Foto / </label>
								<i class="fa fa-eye"></i>
								<a href="<?php echo base_url('assets/upload/kesehatan/' . $kes->foto_lm) ?>" target="_blank">Lihat Foto</a>
								<input type="file" name="ffoto" class="form-control" tabindex="6">
								<input type="hidden" name="ffotolama" value="<?php echo $kes->foto_lm ?>" />
								<!-- <span style="color: red">
                  </span> -->
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" name="btnsimpan" value="Simpan" tabindex="7" />
					<a href="<?php echo site_url('kesehatan-admin') ?>" class="btn btn-default" tabindex="8">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
