<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 2.4.0
	</div>
	<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
	reserved.
</footer>

<!-- Control Sidebar -->

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/template_admin/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/fastclick/lib/fastclick.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url(); ?>assets/template_admin/bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/template_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/template_admin/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/template_admin/dist/js/demo.js"></script>

<script>
	$(function() {
		$('#dataTransAdd').dataTable({
			"bSort": false,
			"searching": false,
			"lengthChange": false,
			"paging": false
		});
		$('#dataFotoAdd').dataTable({
			"bSort": false,
			"searching": false,
			"lengthChange": false,
			"paging": false
		});
		$('.select2').select2()

		$('#example1').DataTable()
		$('#example2').DataTable({
			'paging': true,
			'lengthChange': false,
			'searching': false,
			'ordering': true,
			'info': true,
			'autoWidth': false
		})

		$('#tb_indikator').DataTable({
			'paging': false,
			'lengthChange': false,
			'searching': false,
			'ordering': true,
			'info': false,
			'autoWidth': true
		})


		CKEDITOR.replace('editor1')
		CKEDITOR.replace('editor2')
		CKEDITOR.replace('editor3')
		CKEDITOR.replace('editor4')
		//bootstrap WYSIHTML5 - text editor
		$('.textarea').wysihtml5()

	})
</script>
<script>
	function deleteConfirm(url) {
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}

	function doneConfirm(url) {
		$('#btn-done').attr('href', url);
		$('#doneModal').modal();
	}

	function lihatConfirm(url) {
		$('#btn-lihat').attr('href', url);
		$('#lihatModal').modal();
	}

	$(function() {
		//iCheck for checkbox and radio inputs
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue'
		})
		//Red color scheme for iCheck
		$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
			checkboxClass: 'icheckbox_minimal-red',
			radioClass: 'iradio_minimal-red'
		})
		//Flat red color scheme for iCheck
		$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
			checkboxClass: 'icheckbox_flat-green',
			radioClass: 'iradio_flat-green'
		})
	})

	function hanyaAngka(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))

			return false;
		return true;
	}

	function copynokk() {
		var n1 = document.getElementById('nokkidentitas');
		var n2 = document.getElementById('nokk2identitas');
		n2.value = n1.value;
		// document.getElementById('nokkidentitas').value = document.getElementById('nokk2identitas').value;
	}

	function copynik() {
		var n3 = document.getElementById('nikidentitas');
		var n4 = document.getElementById('nik2identitas');
		n4.value = n3.value;
		// document.getElementById('nokkidentitas').value = document.getElementById('nokk2identitas').value;
	}

	function copynama() {
		var n5 = document.getElementById('namalengkapidentitas');
		var n6 = document.getElementById('namalengkap2identitas');
		n6.value = n5.value;
		// document.getElementById('nokkidentitas').value = document.getElementById('nokk2identitas').value;
	}

	function copytempatlahir() {
		var n7 = document.getElementById('tempatlahiridentitas');
		var n8 = document.getElementById('tempatlahir2identitas');
		n8.value = n7.value;
		// document.getElementById('nokkidentitas').value = document.getElementById('nokk2identitas').value;
	}

	function copyalamat() {
		var n9 = document.getElementById('alamatidentitas');
		var n10 = document.getElementById('alamat2identitas');
		n10.value = n9.value;
		// document.getElementById('nokkidentitas').value = document.getElementById('nokk2identitas').value;
	}

	$("#wniskusaha").change(function() {
		var sel = $("#wniskusaha option:selected").val();
		if (sel == 'WNI') {
			document.getElementById("formnamanegara").style.display = "none";
			document.getElementById("namanegara").value = "Indonesia";
			document.getElementById("namanegara").readOnly = true;

		} else if (sel == 'WNA') {
			document.getElementById("formnamanegara").style.display = "block";
			document.getElementById("namanegara").readOnly = false;
			document.getElementById("namanegara").value = "";

		}
	});
</script>
</body>

</html>
