<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Kenal Lahir</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php elseif ($this->session->flashdata('error')) : ?>
			<div class="alert alert-error" role="alert">
				<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php endif; ?>

		<!-- <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Pemohon</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" type="text" id="nikpemohon" name="fnikpemohon" autofocus tabindex="1" />
						</div>
						
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" type="date" id="tglpemohon" name="ftglpemohon" tabindex="2" />
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="box-footer">
				<button class="btn btn-primary" type="button" id="btnCari" name="btnCariPemohon" tabindex="3">Cari Data</button>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Centang jika ingin menginputkan data secara manual
					</label>
				</div>
			</div>
		</div> -->

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah Surat Kenal Lahir</h3>
			</div>
			<!-- /.box-header -->
			<form action="<?php base_url('tambah-kenal-lahir') ?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" class="form-control" name="fidpengajuankenal" value="<?php echo $invoice; ?>" readonly>
							<!-- <div class="form-group">
								<label>No. Surat</label>
								<input type="text" class="form-control" id="nosurat" name="fnosurat" tabindex="1" autofocus onkeypress="return hanyaAngka(event)">
							</div> -->
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Pelapor</label>
								<input type="text" class="form-control" id="nik" name="fnik" tabindex="1" onkeypress="return hanyaAngka(event)">
								<span style="color: red">
									<?php echo form_error('fnik') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Pelapor</label>
								<input type="text" class="form-control" id="namalengkap" name="fnamalengkap" tabindex="2">
								<span style="color: red">
									<?php echo form_error('fnamalengkap') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Lengkap Terlapor</label>
								<input type="text" class="form-control" id="namakenal" name="fnamakenal" tabindex="3">
								<span style="color: red">
									<?php echo form_error('fnamakenal') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Jenis Kelamin Terlapor</label>
								<select class="form-control select2" style="width: 100%;" id="jkel" name="fjkel" tabindex="4">
									<option value="">--- Pilih ---</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fjkel') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Kewarganegaraan Terlapor</label>
								<select class="form-control select2" style="width: 100%;" id="wniskusaha" name="fwni" tabindex="5">
									<option value="">-- Pilih --</option>
									<option value="WNI">WNI</option>
									<option value="WNA">WNA</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fwni') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group" id="formnamanegara" style="display: none;">
								<label>Nama Negara</label>
								<input type="text" class="form-control" id="namanegara" name="fnamanegara" tabindex="6">
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Agama Terlapor</label>
								<select class="form-control select2" style="width: 100%;" id="agama" name="fagama" tabindex="7">
									<option value="">--- Pilih ---</option>
									<option value="Islam">Islam</option>
									<option value="Protestan">Protestan</option>
									<option value="Katolik">Katolik</option>
									<option value="Hindu">Hindu</option>
									<option value="Buddha">Buddha</option>
									<option value="Khonghucu">Khonghucu</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fagama') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Status Perkawinan Terlapor</label>
								<select class="form-control select2" style="width: 100%;" id="statuskawin" name="fstatuskawin" tabindex="8">
									<option value="">--- Pilih ---</option>
									<option value="Kawin">Kawin</option>
									<option value="Belum Kawin">Belum Kawin</option>
									<option value="Cerai Hidup">Cerai Hidup</option>
									<option value="Cerai Mati">Cerai Mati</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fstatuskawin') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tempat Lahir Terlapor</label>
										<input type="text" class="form-control" id="tempatlahir" name="ftempatlahir" tabindex="9">
										<span style="color: red">
											<?php echo form_error('ftempatlahir') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir Terlapor</label>
										<input type="date" class="form-control" id="tgllahir" name="ftgllahir" tabindex="10">
										<span style="color: red">
											<?php echo form_error('ftgllahir') ?>
										</span>
									</div>
									<!-- /.form-group -->
								</div>
							</div>

							<div class="form-group">
								<label>No. KK Terlapor</label>
								<input type="text" class="form-control" id="nokk" name="fnokk" tabindex="11">
								<span style="color: red">
									<?php echo form_error('fnokk') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>NIK Terlapor</label>
								<input type="text" class="form-control" id="nik2" name="fnik2" tabindex="12" onkeypress="return hanyaAngka(event)">
								<span style="color: red">
									<?php echo form_error('fnik2') ?>
								</span>
							</div>
							<!-- /.form-group -->


						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Pekerjaan Terlapor</label>
								<input type="text" id="pekerjaan" name="fpekerjaan" class="form-control" tabindex="13">
								<span style="color: red">
									<?php echo form_error('fpekerjaan') ?>
								</span>
							</div>
							<!-- /.form-group -->

							<div class="form-group">
								<label>Pendidikan Terlapor</label>
								<select class="form-control select2" style="width: 100%;" id="pendidikan" name="fpendidikan" tabindex="14">
									<option value="">--- Pilih ---</option>
									<option value="-">Belum Sekolah</option>
									<option value="SD">SD</option>
									<option value="SMP">SMP</option>
									<option value="SMA/SMK">SMA/SMK</option>
									<option value="D1">D1</option>
									<option value="D2">D2</option>
									<option value="D3">D3</option>
									<option value="S1/D4">S1/D4</option>
									<option value="S2">S2</option>
									<option value="S3">S3</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fpendidikan') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Ayah</label>
								<input type="text" id="namaayah" name="fnamaayah" class="form-control" tabindex="15">
								<span style="color: red">
									<?php echo form_error('fnamaayah') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Nama Ibu</label>
								<input type="text" id="namaibu" name="fnamaibu" class="form-control" tabindex="16">
								<span style="color: red">
									<?php echo form_error('fnamaibu') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Anak ke-</label>
								<select class="form-control select2" style="width: 100%;" id="anakke" name="fanakke" tabindex="17">
									<option value="">--- Pilih ---</option>
									<option value="1 (Satu)">1 (Satu)</option>
									<option value="2 (Dua)">2 (Dua)</option>
									<option value="3 (Tiga)">3 (Tiga)</option>
									<option value="4 (Empat)">4 (Empat)</option>
									<option value="5 (Lima)">5 (Lima)</option>
									<option value="6 (Enam)">6 (Enam)</option>
									<option value="7 (Tujuh)">7 (Tujuh)</option>
									<option value="8 (Delapan)">8 (Delapan)</option>
									<option value="9 (Sembilan)">9 (Sembilan)</option>
									<option value="10 (Sepuluh)">10 (Sepuluh)</option>
								</select>
								<span style="color: red">
									<?php echo form_error('fanakke') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" id="alamat" name="falamat" class="form-control" tabindex="18">
								<span style="color: red">
									<?php echo form_error('falamat') ?>
								</span>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Keterangan</label>
								<textarea class="form-control" id="ket" name="fket" tabindex="19" rows="2"></textarea>
								<span style="color: red">
									<?php echo form_error('fket') ?>
								</span>
							</div>

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" id="btnSimpan" name="btnSimpan" value="Simpan" tabindex="20" />
					<a href="<?php echo site_url('layanan-kenal-lahir-admin') ?>" class="btn btn-default" tabindex="21">Kembali</a>
				</div>
			</form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
