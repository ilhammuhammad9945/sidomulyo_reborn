<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Kenal Lahir</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Surat Kenal Lahir</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>ID Pengajuan</td>
										<td>:</td>
										<td><?php echo $kenal->id_pengajuan ?></td>
									</tr>
									<tr>
										<td>Tgl Pengajuan</td>
										<td>:</td>
										<td><?php echo format_indo(date('Y-m-d'), strtotime($kenal->created_at)) ?></td>
									</tr>
									<tr>
										<td>Nomor Surat</td>
										<td>:</td>
										<td><?php echo '470/......./35.09.07.2006/' . date("Y")  ?></td>
									</tr>
									<tr>
										<td>NIK Pelapor</td>
										<td>:</td>
										<td><?php echo $kenal->nik_pemohon ?></td>
									</tr>
									<tr>
										<td>Nama Pelapor</td>
										<td>:</td>
										<td><?php echo $kenal->nama_pemohon ?></td>
									</tr>
									<tr>
										<td>Nama Terlapor</td>
										<td>:</td>
										<td><?php echo $kenal->knl_anak ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin Terlapor</td>
										<td>:</td>
										<?php if ($kenal->knl_jkel == 'L') { ?>
											<td>Laki-laki</td>
										<?php } else { ?>
											<td>Perempuan</td>
										<?php } ?>
									</tr>
									<tr>
										<td>Kewarganegaraan Terlapor</td>
										<td>:</td>
										<td><?php echo $kenal->knl_kwn ?></td>
									</tr>
									<tr>
										<td>Agama Terlapor</td>
										<td>:</td>
										<td><?php echo $kenal->knl_agama ?></td>
									</tr>
									<tr>
										<td>Status</td>
										<td>:</td>
										<td><?php echo $kenal->knl_status_kwn ?></td>
									</tr>
									<tr>
										<td>Tempat/Tgl Lahir</td>
										<td>:</td>
										<td><?php echo $kenal->knl_tempat_lahir . ' , ' . format_indo(date('Y-m-d'), strtotime($kenal->knl_tgl_lahir)) ?></td>
									</tr>
									<tr>
										<td>No. KK</td>
										<td>:</td>
										<td><?php echo $kenal->knl_nokk ?></td>
									</tr>
									<tr>
										<td>NIK Terlapor</td>
										<td>:</td>
										<td><?php echo $kenal->knl_nik ?></td>
									</tr>
									<tr>
										<td>Pekerjaan Terlapor</td>
										<td>:</td>
										<td><?php echo $kenal->knl_pekerjaan ?></td>
									</tr>
									<tr>
										<td>Pendidikan Terlapor</td>
										<td>:</td>
										<td><?php echo $kenal->knl_pendidikan ?></td>
									</tr>
									<tr>
										<td>Nama Ayah</td>
										<td>:</td>
										<td><?php echo $kenal->knl_ayah ?></td>
									</tr>
									<tr>
										<td>Nama Ibu</td>
										<td>:</td>
										<td><?php echo $kenal->knl_ibu ?></td>
									</tr>
									<tr>
										<td>Anak ke-</td>
										<td>:</td>
										<td><?php echo $kenal->knl_anakke ?></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>:</td>
										<td><?php echo $kenal->knl_alamat ?></td>
									</tr>
									<tr>
										<td>Keterangan</td>
										<td>:</td>
										<td><?php echo $kenal->knl_keterangan ?></td>
									</tr>

								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-kenal-lahir/' . $kenal->id_pengajuan) ?>" class="btn btn-primary" target="_blank">Cetak Surat Kenal Lahir</a>
						<a href="<?php echo site_url('layanan-kenal-lahir-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
