<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Menu Aktif</h1>
		<ol class="breadcrumb">
			<li><a><i class="fa fa-thumbs-o-up"></i> Layanan</a></li>
			<li><a href="#">Permohonan Surat</a></li>
			<li><a href="#">Ket. Domisili</a></li>
			<li class="active">Lihat Data</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lihat Detail Surat Keterangan Domisili</h3>

					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-read-message">
							<div class="table-responsive">
								<table style="font-size: 15px; font-weight:bold" class="table table-striped nowrap">
									<tr>
										<td>ID Pengajuan</td>
										<td>:</td>
										<td><?php echo $skdomisili->id_pengajuan ?></td>
									</tr>
									<tr>
										<td>Tgl Pengajuan</td>
										<td>:</td>
										<td><?php echo format_indo(date('Y-m-d'), strtotime($skdomisili->created_at)) ?></td>
									</tr>
									<tr>
										<td>Nomor Surat</td>
										<td>:</td>
										<td><?php echo '470/......./35.09.07.2006/' . date("Y")  ?></td>
									</tr>
									<tr>
										<td>NIK</td>
										<td>:</td>
										<td><?php echo $skdomisili->nik_pemohon ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap</td>
										<td>:</td>
										<td><?php echo $skdomisili->nama_pemohon ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>:</td>
										<?php if ($skdomisili->jkel_pemohon == 'L') { ?>
											<td>Laki-laki</td>
										<?php } else { ?>
											<td>Perempuan</td>
										<?php } ?>

									</tr>
									<tr>
										<td>Tempat/Tgl Lahir</td>
										<td>:</td>
										<td><?php echo $skdomisili->tempat_lahir . ' , ' . format_indo(date('Y-m-d'), strtotime($skdomisili->tgl_lahir)) ?></td>
									</tr>
									<tr>
										<td>Kewarganegaraan</td>
										<td>:</td>
										<td><?php echo $skdomisili->kewarganegaraan ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td>:</td>
										<td><?php echo $skdomisili->agama ?></td>
									</tr>
									<tr>
										<td>Status</td>
										<td>:</td>
										<td><?php echo $skdomisili->status_perkawinan ?></td>
									</tr>
									<tr>
										<td>Pendidikan</td>
										<td>:</td>
										<td><?php echo $skdomisili->pendidikan ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>:</td>
										<td><?php echo $skdomisili->pekerjaan ?></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>:</td>
										<td><?php echo $skdomisili->alamat ?></td>
									</tr>
									<tr>
										<td>Keterangan</td>
										<td>:</td>
										<td><?php echo $skdomisili->keterangan ?></td>
									</tr>

								</table>
							</div>
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<div class="box-footer">
						<a href="<?php echo site_url('print-sk-domisili/' . $skdomisili->id_pengajuan) ?>" class="btn btn-primary" target="_blank">Cetak SK Domisili</a>
						<a href="<?php echo site_url('layanan-domisili-admin') ?>" class="btn btn-default">Kembali</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
