<!-- partial:partials/_footer.html -->
<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-sm-5 mb-5">
					<!-- <img src="assets/images/logo.svg" class="footer-logo" alt="" />  -->
					<a class="ini-judul-footer" href="<?php echo base_url() ?>">
						DESA SIDOMULYO
					</a>
					<h5 class="font-weight-bold mt-4" style="text-align: justify;">
						Fakta Singkat :
					</h5>
					<h5 class="font-weight-normal mt-4" style="text-align: justify;">
						Sidomulyo adalah desa di kecamatan Semboro, Jember, Jawa Timur, Indonesia. Desa ini terdiri dari dua dusun yaitu dusun pucu'an dan rowotengu. Lihat Peta <a href="https://goo.gl/maps/9R4ccNM2SLTpTqFT9" target="_blank">disini</a> atau salin link peta dibawah ini :
					</h5>
					<button type="button" class="btn btn-light" onclick="copy_text()">Salin Link</button>
					<input type="text" value="https://goo.gl/maps/9R4ccNM2SLTpTqFT9" id="pilih" readonly />

				</div>
				<div class="col-sm-4">
					<h3 class="font-weight-bold mb-3">KONTAK</h3>
					<div class="row">
						<div class="col-sm-12">
							<div class="footer-border-bottom pb-2">
								<div class="row">
									<div class="col-3">
										<img src="<?php echo base_url(); ?>assets/template_user/images/email-logo.png" alt="thumb" class="img-fluid" style="width: 45px;" />
									</div>
									<div class="col-9">
										<span class="font-weight-600 ini-tulisan-kontak">
											Email : <br>
											dessidomulyo&#64;
										</span><span class="font-weight-600 ini-tulisan-kontak" style="margin-left: -3px">gmail.com</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="footer-border-bottom pb-2 pt-2">
								<div class="row">
									<div class="col-3">
										<img src="<?php echo base_url(); ?>assets/template_user/images/telephone.png" alt="thumb" class="img-fluid" style="width: 33px;" />
									</div>
									<div class="col-9">
										<h5 class="font-weight-600 ini-tulisan-kontak">
											Telepon Kantor : <br>
											-
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="pb-2 pt-2 ini-icon-wa">
								<div class="row">
									<div class="col-3">
										<img src="<?php echo base_url(); ?>assets/template_user/images/wa.png" alt="thumb" class="img-fluid" style="width: 47px;" />
									</div>
									<div class="col-9">
										<h5 class="font-weight-600 mb-3 ini-tulisan-kontak">
											WA Operator : <br>
											0857-0768-9346
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<ul class="social-media mt-2 mb-5" style="margin-left: -3px">
						<li>
							<a href="#">
								<i class="mdi mdi-facebook"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="mdi mdi-youtube"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="mdi mdi-twitter"></i>
							</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<h3 class="font-weight-bold mb-3">PENGUNJUNG</h3>
					<div class="footer-border-bottom pb-2">
						<div class="d-flex justify-content-between align-items-center">
							<h5 class="mb-0 font-weight-600">Hari Ini</h5>
							<div class="count"><?= $pengunjung_today['jml_today']; ?> </div>
						</div>
					</div>
					<div class="footer-border-bottom pb-2 pt-2">
						<div class="d-flex justify-content-between align-items-center">
							<h5 class="mb-0 font-weight-600">Minggu Ini</h5>
							<div class="count"><?= $pengunjung_weekly['jml_weekly']; ?></div>
						</div>
					</div>
					<div class="footer-border-bottom pb-2 pt-2">
						<div class="d-flex justify-content-between align-items-center">
							<h5 class="mb-0 font-weight-600">Bulan Ini</h5>
							<div class="count"><?= $pengunjung_monthly['jml_monthly']; ?></div>
						</div>
					</div>
					<div class="footer-border-bottom pb-2 pt-2">
						<div class="d-flex justify-content-between align-items-center">
							<h5 class="mb-0 font-weight-600">Total</h5>
							<div class="count"><?= $pengunjung_alltime['jml_alltime']; ?></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="d-sm-flex justify-content-between align-items-center">
						<div class="fs-14 font-weight-600">
							© 2020 @ <a href="https://www.bootstrapdash.com/" target="_blank" class="text-white"> BootstrapDash</a>. All rights reserved.
						</div>
						<div class="fs-14 font-weight-600">
							Handcrafted by <a href="https://www.bootstrapdash.com/" target="_blank" class="text-white">BootstrapDash</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- partial -->
</div>
</div>

<a href="#" id="toTopBtn" class="cd-top text-replace js-cd-top cd-top--is-visible cd-top--fade-out" data-abc="true"></a>
<!-- inject:js -->
<script src="<?php echo base_url(); ?>assets/template_user/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="<?php echo base_url(); ?>assets/template_user/vendors/aos/dist/aos.js/aos.js"></script>
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="<?php echo base_url(); ?>assets/template_user/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/template_user/js/jquery.easeScroll.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
<script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>

<!-- End custom js for this page-->
<script type="text/javascript">
	const Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});

	function copy_text() {
		document.getElementById("pilih").select();
		document.execCommand("copy");
		Toast.fire({
			type: 'success',
			title: 'Link berhasil dicopy'
		});
		// alert("Link berhasil dicopy");
	}

	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
		if (!$(this).next().hasClass('show')) {
			$(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
		}
		var $subMenu = $(this).next('.dropdown-menu');
		$subMenu.toggleClass('show');

		$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
			$('.dropdown-submenu .show').removeClass('show');
		});
		return false;
	});

	$(document).ready(function() {
		$(window).scroll(function() {
			if ($(this).scrollTop() > 20) {
				$('#toTopBtn').fadeIn();
			} else {
				$('#toTopBtn').fadeOut();
			}
		});

		$('#toTopBtn').click(function() {
			$("html, body").animate({
				scrollTop: 0
			}, 1000);
			return false;
		});
	});
</script>

<script>
	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [
				"Total Penduduk",
				"Laki - laki",
				"Perempuan"
			],

			datasets: [{
				label: 'Jumlah Total Penduduk',
				data: [
					<?= $penduduk['nilai_statistik']; ?>,
					<?php
					foreach ($gender as $key => $value) {
						echo $value->nilai_statistik . ", ";
					}
					?>
				],

				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)'

				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)'

				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	var ctx = document.getElementById("myChart2").getContext('2d');
	var myChart2 = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: [
				<?php
				foreach ($pekerjaan as $key => $value) {
					echo '"' . $value->parameter_statistik . '", ';
				}
				?>
			],

			datasets: [{
				label: '# of Votes',
				data: [
					<?php
					foreach ($pekerjaan as $key => $value) {
						echo $value->nilai_statistik . ", ";
					}
					?>
				],

				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)'
				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	var ctx = document.getElementById("myChart3").getContext('2d');
	var myChart3 = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: [
				<?php
				foreach ($usia as $key => $value) {
					echo '"' . $value->parameter_statistik . '", ';
				}
				?>
			],

			datasets: [{
				label: '# of Votes',
				data: [12, 19, 3, 23, 2, 3],
				data: [
					<?php
					foreach ($usia as $key => $value) {
						echo $value->nilai_statistik . ", ";
					}
					?>
				],

				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)'
				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
</script>
</body>

</html>
