<!DOCTYPE html>
<html lang="zxx">

<head>
	<!-- Required meta tags -->
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge" />
	<title><?php echo $title ?> | Website Desa Sidomulyo</title>
	<!-- plugin css for this page -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_user/vendors/mdi/css/materialdesignicons.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_user/vendors/aos/dist/aos.css/aos.css" />

	<!-- End plugin css for this page -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/template_user/images/favicon.png" />

	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_user/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_user/css/custom.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<script type="text/javascript">
		function blinktext() {
			var f = document.getElementById('announcement');
			setInterval(function() {
				f.style.visibility = (f.style.visibility == 'hidden' ? '' : 'hidden');
			}, 800);
		}
	</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/template_user/chartjs/Chart.js"></script>
	<!-- endinject -->
</head>

<body>
	<div class="container-scroller">
		<div class="main-panel">
			<!-- partial:partials/_navbar.html -->
			<header id="header">
				<div class="container">
					<nav class="navbar navbar-expand-lg navbar-light">
						<div class="navbar-top">
							<div class="d-flex justify-content-between align-items-center">
								<div class="navbar-top-left-menu">
									<span class="mr-3 text-white"><?php echo $hrsekarang . ", " . $tglsekarang . " - " .  $wktsekarang . " WIB"; ?></span>
								</div>
								<ul class="navbar-top-right-menu">
									<form method="post" action="<?= base_url('user/C_userberita/cari_berita'); ?>">
										<input type="text" name="search" placeholder="Masukkan kata kunci berita..">
										<button type="submit" class="btn btn-light font-weight-bold">Cari</button>
									</form>
									<li class="nav-item">
										<a href="<?php echo base_url('login-admin') ?>" class="nav-link">Login</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="navbar-bottom">
							<div class="d-flex justify-content-between align-items-center">
								<div>
									<a class="navbar-brand" href="<?php echo base_url() ?>">
										<!-- <img src="assets/images/logo.svg" alt=""/> -->
										DESA SIDOMULYO
									</a>
								</div>
								<div>
									<button class="navbar-toggler" type="button" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon"></span>
									</button>
									<div class="navbar-collapse justify-content-center collapse" id="navbarSupportedContent">
										<ul class="navbar-nav d-lg-flex justify-content-between align-items-center">
											<li>
												<button class="navbar-close">
													<i class="mdi mdi-close"></i>
												</button>
											</li>
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													UTAMA
												</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="<?php echo base_url() ?>">BERANDA</a></li>
													<!-- <li class="dropdown-submenu">
														<a class="dropdown-item dropdown-toggle" href="#">PROFIL</a>
														<ul class="dropdown-menu">
															<li><a class="dropdown-item" href="//echo base_url . 'profil/sejarah-desa'">SEJARAH DESA</a></li>
															<li><a class="dropdown-item" href="//echo base_url . 'profil/struktur-organisasi-pemerintah-desa'">STRUKTUR ORGANISASI PEMDES</a></li>
															<li><a class="dropdown-item" href="//echo base_url . 'profil/aparatur-pemerintah-desa'">APARATUR PEMDES</a></li>
															<li><a class="dropdown-item" href="//echo base_url . 'profil/struktur-organisasi-bpd'">STRUKTUR ORGANISASI BPD</a></li>
															<li><a class="dropdown-item" href="//echo base_url . 'profil/aparatur-bpd'">APARATUR BPD</a></li>
														</ul>
													</li> -->
													<li><a class="dropdown-item" href="<?php echo base_url() . 'berita' ?>">BERITA</a></li>
													<!-- <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">LAYANAN</a>
														<ul class="dropdown-menu">
															<li><a class="dropdown-item" href="#">PERMOHONAN SURAT</a></li>
															<li><a class="dropdown-item" href="#">PENGADUAN MASYARAKAT</a></li>
														</ul>
													</li> -->
												</ul>
											</li>
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PROFIL</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="<?php echo base_url() . 'profil/sejarah-desa' ?>">SEJARAH DESA</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'profil/struktur-organisasi-pemerintah-desa' ?>">STRUKTUR ORGANISASI PEMDES</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'profil/aparatur-pemerintah-desa' ?>">APARATUR PEMDES</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'profil/struktur-organisasi-bpd' ?>">STRUKTUR ORGANISASI BPD</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'profil/aparatur-bpd' ?>">APARATUR BPD</a></li>
												</ul>
											</li>
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													LEMMAS
												</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="<?php echo base_url() . 'lembaga-kemasyarakatan/pkk' ?>">PKK</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'lembaga-kemasyarakatan/lpmd' ?>">LPMD</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'lembaga-kemasyarakatan/karang-taruna' ?>">KARANG TARUNA</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'lembaga-kemasyarakatan/posyandu' ?>">POSYANDU</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'lembaga-kemasyarakatan/rt-rw' ?>">RT/RW</a></li>
													<!-- <li><a class="dropdown-item" href="#">KARANG WERDA</a></li> -->
													<li><a class="dropdown-item" href="<?php echo base_url() . 'lembaga-kemasyarakatan/rumah-desa-sehat' ?>">RUMAH DESA SEHAT</a></li>
												</ul>
											</li>
											<!-- <li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="pages/business.html" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LEMBAGA DESA</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="//echo base_url. 'lembaga-desa/muslimat'">MUSLIMAT</a></li>
													<li><a class="dropdown-item" href="//echo base_url. 'lembaga-desa/aisyiyah'">AISYIYAH</a></li>
													<li><a class="dropdown-item" href="//echo base_url. 'lembaga-desa/sanggar-seni' ?>">SANGGAR SENI BUDAYA</a></li>
													<li><a class="dropdown-item" href="//echo base_url. 'lembaga-desa/lsm'">LSM</a></li>
												</ul>
											</li> -->
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="pages/sports.html" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">KADER DESA</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="<?php echo base_url() . 'kader-desa/kpmd' ?>">KPMD</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'kader-desa/kpm' ?>">KPM</a></li>
													<!-- <li><a class="dropdown-item" href="echo base_url. 'kader-desa/kader-teknik'">KADER TEKNIK</a></li>
													<li><a class="dropdown-item" href="echo base_url. 'kader-desa/kader-ppkbd'">KADER DAN SUB PPKBD</a></li>
													<li><a class="dropdown-item" href="echo base_url. 'kader-desa/kader-tb'">KADER TB</a></li> -->
												</ul>
											</li>
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="pages/art.html" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">DATA DESA</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="<?php echo base_url() . 'data-desa/bumdes' ?>">BUMDES</a></li>
													<!-- <li><a class="dropdown-item" href="echo base_url . 'data-desa/umkm'">UMKM</a></li> -->
													<li><a class="dropdown-item" href="<?php echo base_url() . 'data-desa/produkunggulan' ?>">PRODUK UNGGULAN</a></li>
													<li><a class="dropdown-item" href="<?= site_url('user/C_userdatadesa/pendidikan'); ?>">PENDIDIKAN</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'data-desa/kesehatan' ?>">KESEHATAN</a></li>
													<li><a class="dropdown-item" href="<?php echo base_url() . 'data-desa/statistik' ?>">STATISTIK</a></li>
												</ul>
											</li>
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="pages/travel.html" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUK HUKUM</a>
												<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
													<li><a class="dropdown-item" href="<?= site_url('user/C_userprodukhukum/peraturan_desa'); ?>">PERDES</a></li>
													<li><a class="dropdown-item" href="<?= site_url('user/C_userprodukhukum/peraturan_bersama_kepala_desa'); ?>">PERMAKADES</a></li>
													<li><a class="dropdown-item" href="<?= site_url('user/C_userprodukhukum/peraturan_kepala_desa'); ?>">PERKADES</a></li>
													<li><a class="dropdown-item" href="<?= site_url('user/C_userprodukhukum/surat_keputusan_kepala_desa'); ?>">SK KADES</a></li>
													<li><a class="dropdown-item" href="<?= site_url('user/C_userprodukhukum/surat_edaran_kepala_desa'); ?>">SE KADES</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								<ul class="social-media">
									<li>
										<a href="#">
											<i class="mdi mdi-facebook"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="mdi mdi-youtube"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="mdi mdi-twitter"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</header>

			<!-- partial -->
			<div class="flash-news-banner">
				<div class="container">
					<div class="d-lg-flex align-items-center justify-content-between">
						<div class="d-flex align-items-center">
							<span class="badge badge-dark mr-3"><b>Info Terbaru</b></span><br>
							<marquee>
								<p class="mb-0">
									<?php foreach ($info as $key => $value) : ?>
										<?= $value->isi_kt ?>
										|
									<?php endforeach; ?>
								</p>
							</marquee>
						</div>
						<!--  <div class="d-flex infowaktu">
							<span class="mr-3 text-danger">Minggu, 24 Oktober, 2021</span>
							<span class="text-danger">30°C, Jember</span>
						</div> -->
					</div>
				</div>
			</div>
