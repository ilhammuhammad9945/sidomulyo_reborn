<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="col-sm-12">
			<div class="card" data-aos="fade-up">
				<div class="card-body">

					<div class="row">
						<div class="col-lg-8 mb-5">
							<div class="row">

								<div class="col-sm-12 grid-margin">
									<h2 class="font-weight-600 mb-2 ini-judul-konten">
										Peraturan Kepala Desa
									</h2>
									<div class="mt-3 fs-15 konten-berita">
										<p>
											Peraturan Desa adalah peraturan perundang-undangan yang ditetapkan oleh Kepala Desa bersama Badan Permusyawaratan Desa.
										</p>
										<p>
											Peraturan ini berlaku di wilayah desa tertentu. Peraturan Desa merupakan penjabaran lebih lanjut dari peraturan perundangundangan yang lebih tinggi dengan memperhatikan kondisi sosial budaya masyarakat desa setempat.
										</p>
										<p>
											Peraturan Desa dilarang bertentangan dengan kepentingan umum dan/atau peraturan perundang-undangan yang lebih tinggi. Masyarakat berhak memberikan masukan secara lisan atau tertulis dalam rangka penyiapan atau pembahasan Rancangan Peraturan Desa.
										</p>
										<p>
											Peraturan Desa adalah peraturan perundang-undangan yang ditetapkan oleh Kepala Desa bersama Badan Permusyawaratan Desa.
										</p>
										<p>
											Untuk melaksanakan Peraturan Desa, Kepala Desa menetapkan Peraturan Kepala Desa dan Keputusan Kepala Desa. Nama istilah Peraturan Desa dapat bervariasi di Indonesia.
										</p>
									</div>
								</div>
							</div>
							<strong>Daftar Peraturan Kepala Desa</strong><br><br>
							<div class="table-responsive">
								<table id="mytable" class="table nowrap" width="100%" cellspacing="0">
									<thead class="thead-dark">
										<tr>
											<th scope="col">No</th>
											<th scope="col">Nomor</th>
											<th scope="col">Tahun</th>
											<th scope="col">Tanggal</th>
											<th scope="col">Tentang</th>
											<th scope="col">Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1;
										foreach ($perkades as $val) : ?>
											<tr>
												<td scope="row"><?php echo $no++ ?></td>
												<td><?php echo $val->nomor_ph; ?></td>
												<td><?php echo $val->tahun_ph; ?></td>
												<td><?= date("d/m/Y", strtotime($val->tgl_ph)); ?></td>
												<td><?php echo $val->judul_ph; ?></td>
												<td class="ini-tombol"><a href="<?= base_url('./assets/upload/ProdukHukum/Perkades/' . $val->file_ph); ?>" target="_blank">Download</a>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>

						</div>
						<div class="col-lg-4">
							<h2 class="mb-0 text-primary font-weight-600">
								Berita Terbaru
							</h2>

							<?php foreach ($new_berita as $key => $value) : ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="border-bottom pb-4 pt-4">
											<div class="row">
												<div class="col-sm-8">
													<h5 class="font-weight-600 mb-1">
														<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
															<?php echo $value->judul ?>
														</a>
													</h5>
													<p class="fs-13 text-muted mb-2">
														<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
													</p>
												</div>
												<div class="col-sm-4">
													<div class="rotate-img">
														<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

							<div class="trending">
								<h2 class="mb-4 text-primary font-weight-600">
									Berita Terpopuler
								</h2>

								<?php foreach ($berita_populer as $key => $value) : ?>
									<div class="mb-4">
										<div class="rotate-img">
											<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
										</div>
										<h3 class="mt-3 mb-0 font-weight-600">
											<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText2<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?php echo $value->judul ?>
											</a>
										</h3>
										<p class="fs-13 text-muted mb-0">
											<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); 
																	?></span> -->
											<span class="mr-2"><?php echo $value->views ?> kali dilihat</span>
										</p>
									</div>
								<?php endforeach; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($new_berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText" + no).text($(".limitText" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>

	<?php foreach ($berita_populer as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText2" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText2" + no).text($(".limitText2" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>
</script>
