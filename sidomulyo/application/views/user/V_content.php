<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">

		<div class="row" data-aos="fade-up">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-xl-8 mb-5">
								<div id="carouselExampleIndicators" class="carousel slide slider-atas" data-ride="carousel">
									<div class="carousel-inner">
										<div class="carousel-item active">
											<img class="d-block w-100" src="<?php echo base_url('assets/template_user/images/perangkatdesa2.jpg') ?>" alt="First slide" style="height: 500px;">
											<div class="carousel-caption" style="background: rgba(0, 0, 0, 0.4); border-radius: 7px;">
												<h2>Selamat Datang di Website Desa Sidomulyo</h2>
												<p class="d-none d-md-block">Bersinergi, berkolaborasi membangun desa menjadi lebih baik</p>
											</div>
										</div>
										<div class="carousel-item">
											<img class="d-block w-100" src="<?php echo base_url('assets/template_user/images/kantordesa.jpg') ?>" alt="Second slide" style="height: 500px;">
											<div class="carousel-caption" style="background: rgba(0, 0, 0, 0.4); border-radius: 7px;">
												<h2>Selamat Datang di Website Desa Sidomulyo</h2>
												<p class="d-none d-md-block">Bersinergi, berkolaborasi membangun desa menjadi lebih baik</p>
											</div>
										</div>
										<div class="carousel-item">
											<img class="d-block w-100" src="<?php echo base_url('assets/template_user/images/perangkatdesa1.jpg') ?>" alt="Third slide" style="height: 500px;">
											<div class="carousel-caption" style="background: rgba(0, 0, 0, 0.4); border-radius: 7px;">
												<h2>Selamat Datang di Website Desa Sidomulyo</h2>
												<p class="d-none d-md-block">Bersinergi, berkolaborasi membangun desa menjadi lebih baik</p>
											</div>
										</div>
									</div>
									<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
								<br>
								<div class="card-title judul-sambutan-kades">
									<?= $sambutan['judul_kt'] ?>
								</div>
								<div class="row isi-sambutan-kades">
									<!-- <div class="col-xl-5 col-lg-5 col-sm-5">
										<img src="base_url('assets/template_user/images/f.jpg')" alt="thumb" class="img-fluid" style="height: 360px;" />
									</div> -->
									<div class="col-xl-12 col-lg-12 col-sm-12">
										<p class="text-center salam-sambutan-kades" style="font-size: 18px; font-weight: bold">
											Assalamualaikum Wr.Wb.
										</p>
										<div style="text-align: justify;">
											<?= $sambutan['isi_kt'] ?>
										</div>
										<p class="text-center salam-sambutan-kades" style="font-size: 18px; font-weight: bold">
											Wassalamualaikum Wr.Wb.
										</p>

									</div>
								</div>
							</div>
							<div class="col-xl-4">
								<div class="card-title heading-sidebar-kanan">
									Pelayanan Masyarakat
								</div>
								<a href="<?php echo base_url() . 'layanan/permohonan-surat' ?>"><img class="d-block w-100 mb-2 gambar-pelayanan-masyarakat" src="<?php echo base_url('assets/template_user/images/layanansurat.png') ?>" alt="First slide" style="height: 130px;"></a>
								<a href="https://api.whatsapp.com/send?phone=6285707689346"><img class="d-block w-100 gambar-pelayanan-masyarakat" src="<?php echo base_url('assets/template_user/images/pengaduan.png') ?>" alt="First slide" style="height: 130px;"></a>
								<br><br><br>
								<div class="card-title heading-sidebar-kanan">
									Info COVID-19
								</div>
								<div id="gpr-kominfo-widget-container">

								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<br><br>

		<div class="row" data-aos="fade-up">
			<div class="col-xl-8 stretch-card grid-margin">
				<div class="position-relative">
					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<?php $i = 0; ?>
							<?php foreach ($utama as $key => $value) { ?>
								<div class="carousel-item <?php if ($i < 1) {
																echo 'active';
															} ?>">
									<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="banner" class="img-fluid rounded-lg" />
									<div class="banner-content">
										<!-- <div class="badge badge-danger fs-12 font-weight-bold mb-1">
											Berita Terbaru
										</div> -->
										<h1 class="mb-0">Berita Terbaru :</h1>
										<h2 class="mb-1">
											<a class="text-white" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?= $value->judul; ?>
											</a>
										</h2>
										<div class="fs-14 ini-tgl-terbit">
											<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
										</div>
									</div>
								</div>
								<?php $i++; ?>
							<?php } ?>
						</div>

					</div>
				</div>
			</div>
			<div class="col-xl-4 stretch-card grid-margin">
				<div class="card bg-dark text-white">
					<div class="card-body">
						<h2 class="heading-sidebar-kanan">Berita Terpopuler</h2>
						<?php foreach ($berita_populer as $key => $value) { ?>

							<div class="d-flex pt-3 pb-3 align-items-center justify-content-between">
								<div class="pr-3">
									<h5><a class="text-white judul-berita-sidebar-kanan limitText<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>"><?= $value->judul; ?> </a></h5>
									<div class="fs-12">
										<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); 
																?></span> -->
										<span class="mr-2 views-sidebar-kanan"><?php echo $value->views ?> kali dilihat</span>

									</div>
								</div>
								<div class="rotate-img">
									<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid img-lg" />
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>



		<div class="row" data-aos="fade-up">
			<div class="col-lg-3 stretch-card grid-margin">
				<div class="card tabmenus">
					<div class="card-body">
						<h2 class="heading-sidebar-kanan">Pelayanan Surat</h2>
						<ul class="vertical-menu">
							<li><a class="tablinks" href="#akta" onclick="SuratApa(event, 'Akta')" id="defaultOpen">Akta Kelahiran</a></li>
							<li><a class="tablinks" href="#sktm" onclick="SuratApa(event, 'Sktm')">SKTM</a></li>
							<li><a class="tablinks" href="#skck" onclick="SuratApa(event, 'Skck')">SKCK</a></li>
							<li><a class="tablinks" href="#sk-kematian" onclick="SuratApa(event, 'Kematian')">Suket. Kematian</a></li>
							<li><a class="tablinks" href="#sk-BedaIdentitas" onclick="SuratApa(event, 'BedaIdentitas')">Suket. Beda Identitas</a></li>
							<li><a class="tablinks" href="#sk-domisili" onclick="SuratApa(event, 'Domisili')">Suket. Domisili</a></li>
							<li><a class="tablinks" href="#sk-usaha" onclick="SuratApa(event, 'Usaha')">Suket. Usaha</a></li>

							<li><a class="tablinks" href="#sk-pindah" onclick="SuratApa(event, 'Pindah')">Surat Pindah</a></li>
							<li><a class="tablinks" href="#sk-belum-menikah" onclick="SuratApa(event, 'BelumNikah')">Suket. Belum Menikah</a></li>
							<li><a class="tablinks" href="#sk-rekam-ktp" onclick="SuratApa(event, 'RekamKTP')">Rekam KTP</a></li>
							<li><a class="tablinks" href="#sk-bsm" onclick="SuratApa(event, 'Bsm')">BSM</a></li>
							<li><a class="tablinks" href="#sk-kehilangan" onclick="SuratApa(event, 'Kehilangan')">Surat Kehilangan</a></li>
							<li><a class="tablinks" href="#sk-bepergian" onclick="SuratApa(event, 'Bepergian')">Suket. Bepergian</a></li>
							<li><a class="tablinks" href="#sk-kenal-lahir" onclick="SuratApa(event, 'KenalLahir')">Suket. Kenal Lahir</a></li>
							<li><a class="tablinks" href="#pelaporan-kematian" onclick="SuratApa(event, 'PelaporanKematian')">Pelaporan Kematian</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-9 stretch-card grid-margin">
				<div class="card tabcontent" id="Akta">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12 grid-margin">
								<p class="text-justify"><b>Surat Pengantar Membuat Akta Kelahiran</b> adalah surat keterangan dari desa yang dapat digunakan untuk syarat pengajuan pembuatan Akta Kelahiran di Dispendukcapil.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Akta Kelahiran
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Ayah
								</p>
								<p class="mb-0">
									2. Fotocopy KK dan KTP Ibu
								</p>
								<p class="mb-0">
									3. Fotocopy KTP Saksi 1
								</p>
								<p class="mb-0">
									4. Fotocopy KTP Saksi 2
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Akta Kelahiran
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Kematian">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Kematian</b> adalah surat Keterangan yang diterbitkan oleh desa untuk menerangkan bahwa warga yang namanya tersebut di dalam surat telah meninggal dunia.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Kematian
								</h2>
								<p class="mb-0">
									1. Fotocopy KTP dan KK Almarhum / Almarhumah
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Kematian
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Pindah">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Pindah</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Pindah
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Pindah
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="BelumNikah">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Belum Menikah</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Belum Menikah
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Belum Menikah
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="RekamKTP">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Rekam KTP</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Rekam KTP
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Rekam KTP
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Sktm">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Tidak Mampu</b> adalah surat keterangan yang dikeluarkan oleh desa bahwa warga yang namanya tersebut di dalam surat merupakan warga yang tidak mampu sesuai dengan persyaratan yang harus dipenuhi.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Tidak Mampu
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
								<p class="mb-0">
									2. Foto Rumah Tampak Depan
								</p>
								<p class="mb-0">
									3. Foto Rumah Tampak Samping
								</p>
								<p class="mb-0">
									4. Foto Rumah Tampak Belakang
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Tidak Mampu
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Bsm">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Bantuan Siswa Miskin</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Bantuan Siswa Miskin
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Bantuan Siswa Miskin
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Skck">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Catatan Kepolisian (SKCK)</b> surat keterangan dari desa yang dapat digunakan untuk syarat pengajuan SKCK (Surat Keterangan Catatan Kepolisian) di Polres Kabupaten Setempat.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Catatan Kepolisian (SKCK)
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Catatan Kepolisian
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Kehilangan">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Kehilangan</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Kehilangan
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Kehilangan
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="BedaIdentitas">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Beda Identitas</b> adalah surat keterangan yang berisi bahwa data warga yang tercantum namanya tersebut di dalam surat berbeda dengan identitas aslinya.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Beda Identitas
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Beda Identitas
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Bepergian">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Bepergian</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Bepergian
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Bepergian
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Domisili">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Domisili</b> adalah surat Keterangan yang menerangkan bahwa warga yang namanya tercantum di dalam surat tersebut benar warga Desa Sidomulyo.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Domisili
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Domisili
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="KenalLahir">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Kenal Lahir</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Kenal Lahir
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Kenal Lahir
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Usaha">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Surat Keterangan Usaha</b> adalah surat keterangan yang dikeluarkan oleh desa yang menerangkan bahwa nama warga yang tercantum di dalam surat tersebut memang benar memiliki usaha.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Usaha
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemilik Usaha
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Surat Keterangan Usaha
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="PelaporanKematian">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<p class="text-justify"><b>Pelaporan Kematian</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Pelaporan Kematian
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cara Pengajuan Pelaporan Kematian
								</h2>
								<h4 class="mb-2 font-weight-600">
									A. Offline
								</h4>
								<p class="mb-0 text-justify">
									Silahkan membawa berkas persyaratan yang dibutuhkan ke kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
								</p><br>

								<h4 class="mb-2 font-weight-600">
									B. Online
								</h4>
								<p class="mb-0 text-justify">
									Silahkan klik gambar <b>Layanan Pembuatan Surat Online</b> di bagian samping kanan atas halaman ini
								</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

	</div>
</div>


<script type="text/javascript">
	<?php foreach ($berita_populer as $val) : ?>
		var no = "<?= $val->id_berita; ?>";
		var text = $(".limitText" + no).text();
		var len = text.length;

		if (len > 20) {
			$(".limitText" + no).text($(".limitText" + no).text().substr(0, 19) + '... ');
		}
	<?php endforeach; ?>

	function SuratApa(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
