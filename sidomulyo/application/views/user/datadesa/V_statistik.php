<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="card" data-aos="fade-up">
					<div class="card-body">
						<div class="aboutus-wrapper">
							<h2 class="mt-5 ini-statistik-atas">
								Statistik Jumlah Penduduk
							</h2>
							<div class="ini-statistik">
								<canvas id="myChart"></canvas>
							</div>

							<h2 class="mt-5">
								Statistik Jumlah Penduduk Berdasarkan Pembagian Usia
							</h2>
							<div class="ini-statistik">
								<canvas id="myChart3"></canvas>
							</div>

							<!-- <h2 class="mt-5">
								Statistik Jumlah Sumber Air Minum
							</h2>
							<div >
								<canvas id="myChart4"></canvas>
							</div> -->

							<h2 class="mt-5">
								Statistik Jumlah Penduduk Menurut Mata Pencaharian
							</h2>
							<div class="ini-statistik">
								<canvas id="myChart2"></canvas>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
