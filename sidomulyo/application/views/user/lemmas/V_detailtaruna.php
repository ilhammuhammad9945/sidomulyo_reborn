<section class="blog_area single-post-area section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 posts-list">
              <h4 class="mb-30">Detail Pengurus <?php echo $namataruna;?></h4>
               <p>
                     Alamat :  <?php echo $alamat->lain_lain?>
                     </p>
             
               <div class="table-responsive">
               <table class="table table-striped" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                        <?php $no = 1; foreach ($pengurus as $pg): ?>

                <tr>
                  <td><?php echo $no++ ?></td>
                   <td><?php echo $pg->nama_lm;?></td>
                  <td><?php echo $pg->nama_jbt;?></td>
                  <td><?php echo $pg->keterangan_lm;?></td>
                  <td><a data-toggle="modal" data-target="#lihatFoto<?php echo $pg->id_lm;?>" style="color: green">Lihat Foto</a>
                  </td>
                </tr>
                  <?php endforeach; ?>
                
                </tbody>
              </table>
           </div>
           <a href="<?php echo site_url('lembaga-kemasyarakatan/karang-taruna')?>" style="color: green">&lt;&lt; Kembali</a>
               <div class="navigation-top">
                  <div class="d-sm-flex justify-content-between text-center">
                     <p class="like-info"><span class="align-middle"></span> Bagikan melalui media sosial</p>
                     <div class="col-sm-4 text-center my-2 my-sm-0">
                        <!-- <p class="comment-count"><span class="align-middle"><i class="fa fa-comment"></i></span> 06 Comments</p> -->
                     </div>
                     <ul class="social-icons">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-telegram"></i></a></li>
                        <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                     </ul>
                  </div>
                  
               </div>
               
            </div>
            <div class="col-lg-4">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget search_widget">
                     <form action="#">
                        <div class="form-group">
                           <div class="input-group mb-3">
                              <input type="text" class="form-control" placeholder='Search Keyword'
                                 onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                              <div class="input-group-append">
                                 <button class="btns" type="button"><i class="ti-search"></i></button>
                              </div>
                           </div>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
                           type="submit">Search</button>
                     </form>
                  </aside>
                  
                  <aside class="single_sidebar_widget popular_post_widget">
                     <h3 class="widget_title">Recent Post</h3>
                     <div class="media post_item">
                        <img src="<?php echo base_url('assets/template_user/img/post/post_1.png') ?>" alt="post">
                        <div class="media-body">
                           <a href="single-blog.html">
                              <h3>From life was you fish...</h3>
                           </a>
                           <p>January 12, 2019</p>
                        </div>
                     </div>
                     <div class="media post_item">
                        <img src="<?php echo base_url('assets/template_user/img/post/post_2.png') ?>" alt="post">
                        <div class="media-body">
                           <a href="single-blog.html">
                              <h3>The Amazing Hubble</h3>
                           </a>
                           <p>02 Hours ago</p>
                        </div>
                     </div>
                     <div class="media post_item">
                        <img src="<?php echo base_url('assets/template_user/img/post/post_3.png') ?>" alt="post">
                        <div class="media-body">
                           <a href="single-blog.html">
                              <h3>Astronomy Or Astrology</h3>
                           </a>
                           <p>03 Hours ago</p>
                        </div>
                     </div>
                     <div class="media post_item">
                        <img src="<?php echo base_url('assets/template_user/img/post/post_4.png') ?>" alt="post">
                        <div class="media-body">
                           <a href="single-blog.html">
                              <h3>Asteroids telescope</h3>
                           </a>
                           <p>01 Hours ago</p>
                        </div>
                     </div>
                  </aside>

               </div>
            </div>
         </div>
      </div>
   </section>

    <?php 
      foreach($pengurus as $pg){
    ?>

   <div class="modal fade" id="lihatFoto<?php echo $pg->id_lm;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail Biodata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <table>
          <tr>
            <td>Nama</td>
            <td>:</td>
            <td>Abcde</td>
          </tr>

        </table> -->
         <div class="table-responsive">
        <table border="1" cellspacing="0" width="100%">
<tr style="text-align: center; font-weight: bold; color: white;" bgcolor="#5cc593">
<td>DATA DIRI</td>
<td>KETERANGAN</td>
<td>FOTO</td>
</tr>
<tr style="text-align: center;">
<td>Nama</td>
<td><?php echo $pg->nama_lm;?></td>
<td rowspan="2" align="center"><img src="<?php echo base_url('assets/upload/karangtaruna/'.$pg->foto_lm) ?>" alt="" width="210" height="313"></td>
</tr>
<tr style="text-align: center;">
<td>Jabatan</td>
<td><?php echo $pg->nama_jbt." [ ".$pg->keterangan_lm." ]";?></td>
</tr>
</table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="button rounded-0 primary-bg text-white btn_1 boxed-btn" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

  <?php } ?>