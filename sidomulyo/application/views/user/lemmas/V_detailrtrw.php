<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="col-sm-12">
			<div class="card" data-aos="fade-up">
				<div class="card-body">

					<div class="row">
						<div class="col-lg-8 mb-5">
							<div class="row">

								<div class="col-sm-12 grid-margin">
									<h2 class="font-weight-600 mb-3 ini-judul-konten">
										Detail RW
									</h2>
									<center><img class="ini-gambar" src="<?php echo base_url('assets/upload/rtrw/' . $rw->foto_lm) ?>" alt="" width="250" height="330"></center>
								</div>
							</div>
							<strong>Biodata</strong>
							<div class="table-responsive mt-2">
								<table border="0">
									<tr>
										<td>Nama&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $rw->nama_lm ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $rw->jkel_lm ?></td>
									</tr>
									<tr>
										<td>Jabatan&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $rw->nama_jbt . " - " . $rw->asal_organisasi ?></td>
									</tr>
									<tr>
										<td>No. Telp&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $rw->no_telp ?></td>
									</tr>
									<tr>
										<td>Alamat&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $rw->alamat_lm ?></td>
									</tr>
								</table>
							</div>
							<br>
							<strong>Daftar RT di <?php echo $namarw . " - " . $alamat->lain_lain; ?></strong>
							<div class="table-responsive" style="margin-top: 20px;">
								<table id="mytable" class="table nowrap" width="100%" cellspacing="0">
									<thead class="thead-dark">
										<tr>
											<th scope="col">No</th>
											<th scope="col">Nama RT</th>
											<th scope="col">Keterangan</th>
											<th scope="col">Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1;
										foreach ($rt as $r) : ?>

											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $r->nama_lm; ?></td>
												<td><?php echo $r->asal_organisasi; ?></td>
												<td class="ini-tombol"><a href="#" data-toggle="modal" data-target="#lihatFoto<?php echo $r->id_lm; ?>">Lihat</a>
												</td>
											</tr>
										<?php endforeach; ?>

									</tbody>
								</table>
							</div>
							<a class="btn btn-primary mt-3" href="<?php echo site_url('lembaga-kemasyarakatan/rt-rw') ?>">Kembali</a>
						</div>
						<div class="col-lg-4">
							<h2 class="mb-0 text-primary font-weight-600">
								Berita Terbaru
							</h2>

							<?php foreach ($new_berita as $key => $value) : ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="border-bottom pb-4 pt-4">
											<div class="row">
												<div class="col-sm-8">
													<h5 class="font-weight-600 mb-1">
														<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
															<?php echo $value->judul ?>
														</a>
													</h5>
													<p class="fs-13 text-muted mb-2">
														<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
													</p>
												</div>
												<div class="col-sm-4">
													<div class="rotate-img">
														<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

							<div class="trending">
								<h2 class="mb-4 text-primary font-weight-600">
									Berita Terpopuler
								</h2>

								<?php foreach ($berita_populer as $key => $value) : ?>
									<div class="mb-4">
										<div class="rotate-img">
											<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
										</div>
										<h3 class="mt-3 mb-0 font-weight-600">
											<a class="text-decoration-none text-secondary limitText2<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?php echo $value->judul ?>
											</a>
										</h3>
										<p class="fs-13 text-muted mb-0">
											<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); 
																	?></span> -->
											<span class="mr-2"><?php echo $value->views ?> kali dilihat</span>
										</p>
									</div>
								<?php endforeach; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
foreach ($rt as $w) {
?>
	<div class="modal fade" id="lihatFoto<?php echo $w->id_lm; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLongTitle">Detail Pengurus</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 28px; padding-right: 25px;">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<center> <img src="<?php echo base_url('assets/upload/rtrw/' . $w->foto_lm) ?>" width="210" height="313"></center><br>

					<div class="table-responsive">
						<table class="table nowrap" width="100%" cellspacing="0" align="center">
							<tr style="font-weight: bold; color: white;" bgcolor="#032a63">
								<td>DATA DIRI</td>
								<td>KETERANGAN</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td><?php echo $w->nama_lm; ?></td>
							</tr>
							<tr>
								<td>Jenis Kelamin</td>
								<td><?php echo $w->jkel_lm; ?></td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td><?php echo $w->nama_jbt . " - " . $w->asal_organisasi; ?></td>
							</tr>
							<tr>
								<td>No. Telp</td>
								<td><?php echo $w->no_telp; ?></td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td><?php echo $w->alamat_lm; ?></td>
							</tr>
						</table>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
<?php
} ?>

<script type="text/javascript">
	<?php foreach ($new_berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText" + no).text($(".limitText" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>

	<?php foreach ($berita_populer as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText2" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText2" + no).text($(".limitText2" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>
</script>
