<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="col-sm-12">
			<div class="card" data-aos="fade-up">
				<div class="card-body">
					<div class="row">
						<div class="col-sm-12">
							<h1 class="font-weight-600 mb-4 ini-judul-konten">
								Daftar Berita
							</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-8">
							<?php if ($berita == null) { ?>
								<h5 class="mb-5 text-center" style="color: red;">Tidak ditemukan hasil pencarian, gunakan kata kunci lain</h5>
							<?php } else { ?>
								<?php foreach ($berita as $key => $value) { ?>
									<div class="row">
										<div class="col-sm-4 grid-margin mb-3">
											<div class="rotate-img">
												<img class="ini-gambar-khusus" src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="banner" class="img-fluid" style="height: 160px;" />
											</div>
										</div>
										<div class="col-sm-8 grid-margin">
											<h2 class="font-weight-600 mb-2 fs-17 ini-judul-berita">
												<a class="text-decoration-none text-secondary" href="<?= base_url('detail-berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
													<div class="limitText<?= $value->id_berita; ?>">
														<?= $value->judul ?>
													</div>
												</a>
											</h2>
											<p class="fs-13 text-muted mb-0">
												<span class="mr-2"><?= date("d", strtotime($value->tgl_terbit)); ?> <?= date("F", strtotime($value->tgl_terbit)); ?> <?= date("Y", strtotime($value->tgl_terbit)); ?> | <?= $value->views; ?> kali dilihat</span>
											</p>

											<div class="mt-3 fs-15 konten-berita">
												<!-- <div class="limitText4//$value->id_berita;">
													//echo $value->headline;
												</div> -->
												<?php
												$jumlahkarakter = 140;
												$cetak = substr($value->headline, $jumlahkarakter, 1);
												if ($cetak != " ") {
													while ($cetak != " ") {
														$i = 1;
														$jumlahkarakter = $jumlahkarakter + $i;
														$cetak = substr($value->headline, $jumlahkarakter, 1);
													}
												}
												$cetak = substr($value->headline, 0, $jumlahkarakter);
												echo $cetak . "<br>";
												?>
												<a style="text-decoration: none;" href="<?= base_url('detail-berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>"> Baca Selengkapnya ... </a>
											</div>

										</div>
									</div>
								<?php } ?>
								<?php echo $pagination; ?>
							<?php } ?>
						</div>
						<div class="col-lg-4">
							<h2 class="mb-3 text-primary font-weight-600">
								Cari Berita
							</h2>
							<form class="ini-pencarian" action="<?= base_url('user/C_userberita/cari_berita') ?>" method="post">
								<input type="text" name="search" placeholder="Masukkan kata kunci ..">
								<button class="btn btn-primary" type="submit">Cari</button>
							</form><br><br>
							<h2 class="mb-0 text-primary font-weight-600">
								Berita Terbaru
							</h2>
							<?php foreach ($new_berita as $key => $value) : ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="border-bottom pb-4 pt-4">
											<div class="row">
												<div class="col-sm-8">
													<h5 class="font-weight-600 mb-1">
														<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText3<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
															<?php echo $value->judul ?>
														</a>
													</h5>
													<p class="fs-13 text-muted mb-2">
														<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
													</p>
												</div>
												<div class="col-sm-4">
													<div class="rotate-img">
														<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
							<div class="trending">
								<h2 class="mb-4 text-primary font-weight-600">
									Berita Terpopuler
								</h2>
								<?php foreach ($berita_populer as $key => $value) : ?>
									<div class="mb-4">
										<div class="rotate-img">
											<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
										</div>
										<h3 class="mt-3 mb-0 font-weight-600">
											<a class="text-decoration-none text-secondary limitText2<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?php echo $value->judul ?>
											</a>
										</h3>
										<p class="fs-13 text-muted mb-0">
											<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); 
																	?></span> -->
											<span class="mr-2"><?php echo $value->views ?> kali dilihat</span>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText" + no).text($(".limitText" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>

	<?php foreach ($new_berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText3" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText3" + no).text($(".limitText3" + no).html().trim().substr(0, 34) + ' ...');
		}

	<?php } ?>

	<?php foreach ($berita_populer as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText2" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText2" + no).text($(".limitText2" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>
</script>
