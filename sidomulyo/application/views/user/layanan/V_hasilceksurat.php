<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="col-sm-12">
			<div class="card" data-aos="fade-up">
				<div class="card-body">

					<div class="row">
						<div class="col-lg-8 mb-5">
							<div class="row">
								<div class="col-sm-12  grid-margin">
									<h2 class="mb-2 font-weight-600 ini-judul-konten">
										Cek Status Pengajuan Surat
									</h2>
									<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
										<div class="row">
											<div class="col-sm-9">
												<div class="form-group">
													<!-- <input type="hidden" value="1" name="jenis_surat"> -->
													<input type="text" class="form-control" id="idpengajuan" name="idpengajuan" placeholder="Masukkan ID Pengajuan Anda" />
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<!-- <a href="#" class="btn btn-lg btn-primary font-weight-bold">Cari</a> -->
													<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
												</div>
											</div>
										</div>
									</form>
									<?php if ($hasil == null) { ?>
										<!-- kondisi jika tidak ada data -->
										<h4 class="mb-2 font-weight-600 text-danger text-center">
											Data Tidak Ditemukan
										</h4>
									<?php } else { ?>
										<div class="table-responsive">
											<table id="mytable" class="table nowrap" width="100%" cellspacing="0">
												<thead class="thead-dark">
													<tr>
														<th scope="col">No</th>
														<th scope="col">ID Pengajuan</th>
														<th scope="col">Pemohon</th>
														<th scope="col">Jenis Surat</th>
														<th scope="col">Status Pembuatan</th>
													</tr>
												</thead>
												<tbody>

													<?php foreach ($hasil as $key => $value) : ?>
														<tr>
															<td scope="row"><?= $no++; ?></td>
															<td><?= $value->id_pengajuan; ?></td>
															<td><?= $value->nama_pemohon; ?></td>
															<td><?= ucfirst($value->jenis_surat); ?></td>
															<td>
																<span class="badge badge-<?php if ($value->status_pembuatan == 'Dalam Proses') {
																								echo "danger";
																							} else {
																								echo "success";
																							} ?>"><?= $value->status_pembuatan ?></span>
															</td>
														</tr>
													<?php endforeach; ?>

												</tbody>
											</table>
										</div>
										<p class="mb-0 text-justify">
											<b>Catatan :</b> Berkas yang sudah selesai dapat diambil di Kantor Desa Sidomulyo pada jam kerja : Senin-Jumat, Pukul 08.00 s/d 15.00 WIB
										</p>
									<?php } ?>
								</div>
							</div>

						</div>
						<div class="col-lg-4">
							<h2 class="mb-0 text-primary font-weight-600">
								Berita Terbaru
							</h2>

							<?php foreach ($new_berita as $key => $value) : ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="border-bottom pb-4 pt-4">
											<div class="row">
												<div class="col-sm-8">
													<h5 class="font-weight-600 mb-1">
														<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
															<?php echo $value->judul ?>
														</a>
													</h5>
													<p class="fs-13 text-muted mb-2">
														<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
													</p>
												</div>
												<div class="col-sm-4">
													<div class="rotate-img">
														<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

							<div class="trending">
								<h2 class="mb-4 text-primary font-weight-600">
									Berita Terpopuler
								</h2>

								<?php foreach ($berita_populer as $key => $value) : ?>
									<div class="mb-4">
										<div class="rotate-img">
											<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
										</div>
										<h3 class="mt-3 mb-0 font-weight-600">
											<a class="text-decoration-none text-secondary limitText2<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?php echo $value->judul ?>
											</a>
										</h3>
										<p class="fs-13 text-muted mb-0">
											<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit)));
																	?></span> -->
											<span class="mr-2"><?php echo $value->views ?> kali dilihat</span>
										</p>
									</div>
								<?php endforeach; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($new_berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText" + no).text($(".limitText" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>

	<?php foreach ($berita_populer as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText2" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText2" + no).text($(".limitText2" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>
</script>
