<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">

		<div class="row" data-aos="fade-up">
			<div class="col-lg-3 stretch-card grid-margin">
				<div class="card tabmenus">
					<div class="card-body">
						<h2>Pelayanan Surat</h2>
						<ul class="vertical-menu">
							<li><a class="tablinks" href="#akta" onclick="SuratApa(event, 'Akta')" id="defaultOpen">Akta Kelahiran</a></li>
							<li><a class="tablinks" href="#sktm" onclick="SuratApa(event, 'Sktm')">SKTM</a></li>
							<li><a class="tablinks" href="#skck" onclick="SuratApa(event, 'Skck')">SKCK</a></li>
							<li><a class="tablinks" href="#sk-kematian" onclick="SuratApa(event, 'Kematian')">Suket. Kematian</a></li>
							<li><a class="tablinks" href="#sk-BedaIdentitas" onclick="SuratApa(event, 'BedaIdentitas')">Suket. Beda Identitas</a></li>
							<li><a class="tablinks" href="#sk-domisili" onclick="SuratApa(event, 'Domisili')">Suket. Domisili</a></li>
							<li><a class="tablinks" href="#sk-usaha" onclick="SuratApa(event, 'Usaha')">Suket. Usaha</a></li>

							<li><a class="tablinks" href="#sk-pindah" onclick="SuratApa(event, 'Pindah')">Surat Pindah</a></li>
							<li><a class="tablinks" href="#sk-belum-menikah" onclick="SuratApa(event, 'BelumNikah')">Suket. Belum Menikah</a></li>
							<li><a class="tablinks" href="#sk-rekam-ktp" onclick="SuratApa(event, 'RekamKTP')">Rekam KTP</a></li>
							<li><a class="tablinks" href="#sk-bsm" onclick="SuratApa(event, 'Bsm')">BSM</a></li>
							<li><a class="tablinks" href="#sk-kehilangan" onclick="SuratApa(event, 'Kehilangan')">Surat Kehilangan</a></li>
							<li><a class="tablinks" href="#sk-bepergian" onclick="SuratApa(event, 'Bepergian')">Suket. Bepergian</a></li>
							<li><a class="tablinks" href="#sk-kenal-lahir" onclick="SuratApa(event, 'KenalLahir')">Suket. Kenal Lahir</a></li>
							<li><a class="tablinks" href="#pelaporan-kematian" onclick="SuratApa(event, 'PelaporanKematian')">Pelaporan Kematian</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-9 stretch-card grid-margin">
				<div class="card tabcontent" id="Akta">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Akta Kelahiran
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Ayah
								</p>
								<p class="mb-0">
									2. Fotocopy KK dan KTP Ibu
								</p>
								<p class="mb-0">
									3. Fotocopy KTP Saksi 1
								</p>
								<p class="mb-0">
									4. Fotocopy KTP Saksi 2
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Akta%20Kelahiran" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Akta Kelahiran
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<!-- <input type="hidden" value="1" name="jenis_surat"> -->
												<input type="text" class="form-control" id="idpengajuankelahiran" name="idpengajuan" placeholder="Masukkan ID Pengajuan Akta Kelahiran" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Kematian">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Kematian
								</h2>
								<p class="mb-0">
									1. Fotocopy KTP dan KK Almarhum / Almarhumah
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Kematian" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Kematian
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuankematian" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Kematian" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Pindah">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Pindah
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Pindah" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Pindah
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanpindah" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Pindah" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="BelumNikah">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Belum Menikah
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Belum%20Menikah" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Belum Menikah
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanbelumnikah" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Belum Menikah" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="RekamKTP">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Rekam KTP
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Rekam%20KTP" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Rekam KTP
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanrekamktp" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Rekam KTP" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Sktm">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Tidak Mampu
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
								<p class="mb-0">
									2. Foto Rumah Tampak Depan
								</p>
								<p class="mb-0">
									3. Foto Rumah Tampak Samping
								</p>
								<p class="mb-0">
									4. Foto Rumah Tampak Belakang
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Tidak%20Mampu" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Tidak Mampu
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuansktm" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Tidak Mampu" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Bsm">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Bantuan Siswa Miskin
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Bantuan%20Siswa%20Miskin" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Bantuan Siswa Miskin
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanbsm" name="idpengajuan" placeholder="Masukkan ID Pengajuan Bantuan Siswa Miskin" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Skck">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Catatan Kepolisian (SKCK)
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Catatan%20Kepolisian" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Catatan Kepolisian
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanskck" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Catatan Kepolisian" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Kehilangan">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Kehilangan
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Kehilangan" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Kehilangan
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuankehilangan" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Kehilangan" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="BedaIdentitas">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Beda Identitas
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Beda%20Identitas" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Beda Identitas
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanbedaidentitas" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Beda Identitas" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Bepergian">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Bepergian
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Bepergian" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Bepergian
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanbepergian" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Bepergian" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Domisili">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Domisili
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemohon
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Domisili" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Domisili
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuandomisili" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Domisili" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="KenalLahir">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Kenal Lahir
								</h2>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
								<p class="mb-0">
									1. Scan / Fotocopy KTP (1 lembar)
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Kenal%20Lahir" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Kenal Lahir
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuankenallahir" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Kenal Lahir" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="Usaha">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Surat Keterangan Usaha
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP Pemilik Usaha
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Surat%20Keterangan%20Usaha" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Surat Keterangan Usaha
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanusaha" name="idpengajuan" placeholder="Masukkan ID Pengajuan Surat Keterangan Usaha" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

				<div class="card tabcontent" id="PelaporanKematian">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Persyaratan Membuat Pelaporan Kematian
								</h2>
								<p class="mb-0">
									1. Fotocopy KK dan KTP
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Hubungi Admin
								</h2>
								<a href="https://api.whatsapp.com/send?phone=6285707689346&text=Saya%20ingin%20membuat%20Pelaporan%20Kematian" class="btn btn-success" style="padding-left: 10px; padding-right: 10px" target="_blank">
									<i class="mdi mdi-whatsapp"></i> Klik disini untuk menuju Whatsapp
								</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12  grid-margin">
								<h2 class="mb-2 font-weight-600">
									Cek Status Pengajuan Pelaporan Kematian
								</h2>
								<form action="<?= base_url('layanan/proses-cari-surat-user') ?>" method="POST">
									<div class="row">
										<div class="col-sm-9">
											<div class="form-group">
												<input type="text" class="form-control" id="idpengajuanpelaporankmt" name="idpengajuan" placeholder="Masukkan ID Pengajuan Pelaporan Kematian" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-lg btn-primary font-weight-bold" value="Cari" name="btnCariSurat">
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>

			</div>

		</div>
	</div>
</div>


<script type="text/javascript">
	function SuratApa(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
