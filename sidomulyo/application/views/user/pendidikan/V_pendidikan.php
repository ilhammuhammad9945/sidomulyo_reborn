<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="col-sm-12">
			<div class="card" data-aos="fade-up">
				<div class="card-body">

					<div class="row">
						<div class="col-lg-8 mb-5">
							<div class="row">

								<div class="col-sm-12 grid-margin">
									<h2 class="font-weight-600 mb-0 ini-judul-konten">
										Data Sekolah-sekolah
									</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 grid-margin">
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" href="#paud" role="tab" data-toggle="tab">PAUD</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#tk" role="tab" data-toggle="tab">TK</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#sd" role="tab" data-toggle="tab">SD/MI</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#smp" role="tab" data-toggle="tab">SMP/MTS</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#sma" role="tab" data-toggle="tab">SMA/MA/SMK</a>
										</li>
									</ul>
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane active" id="paud">
											<div class="table-responsive">
												<table id="mytable1" class="table nowrap" width="100%" cellspacing="0">
													<thead class="thead-dark">
														<tr>
															<th scope="col">No</th>
															<th scope="col">Nama Sekolah</th>
															<th scope="col">Aksi</th>
														</tr>
													</thead>
													<tbody>
														<?php $no = 1;
														foreach ($paud as $key => $value) { ?>
															<tr>
																<th scope="row"><?php echo $no++ ?></th>
																<td><?= $value->nama_sekolah ?></td>
																<td class="ini-tombol"><a href="<?= site_url('user/C_userdatadesa/detail_pendidikan/' . $value->id_pend); ?>">Lihat Detail</a></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="tk">
											<div class="table-responsive">
												<table id="mytable2" class="table nowrap" width="100%" cellspacing="0">
													<thead class="thead-dark">
														<tr>
															<th scope="col">No</th>
															<th scope="col">Nama Sekolah</th>
															<th scope="col">Aksi</th>
														</tr>
													</thead>
													<tbody>
														<?php $no = 1;
														foreach ($tk as $key => $value) { ?>
															<tr>
																<th scope="row"><?php echo $no++ ?></th>
																<td><?= $value->nama_sekolah ?></td>
																<td class="ini-tombol"><a href="<?= site_url('user/C_userdatadesa/detail_pendidikan/' . $value->id_pend); ?>">Lihat Detail</a></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="sd">
											<div class="table-responsive">
												<table id="mytable3" class="table nowrap" width="100%" cellspacing="0">
													<thead class="thead-dark">
														<tr>
															<th scope="col">No</th>
															<th scope="col">Nama Sekolah</th>
															<th scope="col">Aksi</th>
														</tr>
													</thead>
													<tbody>
														<?php $no = 1;
														foreach ($sd as $key => $value) { ?>
															<tr>
																<th scope="row"><?php echo $no++ ?></th>
																<td><?= $value->nama_sekolah ?></td>
																<td class="ini-tombol"><a href="<?= site_url('user/C_userdatadesa/detail_pendidikan/' . $value->id_pend); ?>">Lihat Detail</a></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="smp">
											<div class="table-responsive">
												<table id="mytable4" class="table nowrap" width="100%" cellspacing="0">
													<thead class="thead-dark">
														<tr>
															<th scope="col">No</th>
															<th scope="col">Nama Sekolah</th>
															<th scope="col">Aksi</th>
														</tr>
													</thead>
													<tbody>
														<?php $no = 1;
														foreach ($smp as $key => $value) { ?>
															<tr>
																<th scope="row"><?php echo $no++ ?></th>
																<td><?= $value->nama_sekolah ?></td>
																<td class="ini-tombol"><a href="<?= site_url('user/C_userdatadesa/detail_pendidikan/' . $value->id_pend); ?>">Lihat Detail</a></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="sma">
											<div class="table-responsive">
												<table id="mytable5" class="table nowrap" width="100%" cellspacing="0">
													<thead class="thead-dark">
														<tr>
															<th scope="col">No</th>
															<th scope="col">Nama Sekolah</th>
															<th scope="col">Aksi</th>
														</tr>
													</thead>
													<tbody>
														<?php $no = 1;
														foreach ($sma as $key => $value) { ?>
															<tr>
																<th scope="row"><?php echo $no++ ?></th>
																<td><?= $value->nama_sekolah ?></td>
																<td class="ini-tombol"><a href="<?= site_url('user/C_userdatadesa/detail_pendidikan/' . $value->id_pend); ?>">Lihat Detail</a></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
						<div class="col-lg-4">
							<h2 class="mb-0 text-primary font-weight-600">
								Berita Terbaru
							</h2>

							<?php foreach ($new_berita as $key => $value) : ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="border-bottom pb-4 pt-4">
											<div class="row">
												<div class="col-sm-8">
													<h5 class="font-weight-600 mb-1">
														<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
															<?php echo $value->judul ?>
														</a>
													</h5>
													<p class="fs-13 text-muted mb-2">
														<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
													</p>
												</div>
												<div class="col-sm-4">
													<div class="rotate-img">
														<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

							<div class="trending">
								<h2 class="mb-4 text-primary font-weight-600">
									Berita Terpopuler
								</h2>

								<?php foreach ($berita_populer as $key => $value) : ?>
									<div class="mb-4">
										<div class="rotate-img">
											<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
										</div>
										<h3 class="mt-3 mb-0 font-weight-600">
											<a class="text-decoration-none text-secondary limitText2<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?php echo $value->judul ?>
											</a>
										</h3>
										<p class="fs-13 text-muted mb-0">
											<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); 
																	?></span> -->
											<span class="mr-2"><?php echo $value->views ?> kali dilihat</span>
										</p>
									</div>
								<?php endforeach; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($new_berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText" + no).text($(".limitText" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>

	<?php foreach ($berita_populer as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText2" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText2" + no).text($(".limitText2" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>
</script>
