<script src="<?= base_url() ?>assets/template_admin/bower_components/jquery/dist/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
	<div class="container">
		<div class="col-sm-12">
			<div class="card" data-aos="fade-up">
				<div class="card-body">

					<div class="row">
						<div class="col-lg-8 mb-5">
							<?php foreach ($parent as $key => $value) : ?>
								<div class="row">

									<div class="col-sm-12 grid-margin">
										<h2 class="font-weight-600 mb-2 ini-judul-konten">
											<?= $value->nama_sekolah; ?>
										</h2>
										<img src="<?= base_url('assets/upload/pendidikan/' . $value->foto_utama) ?>" alt="" class="img-fluid ini-gambar-detail" width="100%" />
										<br><br><br>
										<strong>Profil</strong>
										<div class="mt-4 fs-15 konten-berita">
											<?= $value->visi_misi; ?>
										</div>
									</div>
								</div>
								<strong>Detail</strong><br><br>
								<div class="table-responsive">
									<table id="mytable" class="table nowrap fs-15" width="100%" cellspacing="0">

										<tbody class="align-middle">
											<tr>
												<td><b>Nama Sekolah</b></td>
												<td>:</td>
												<td><?= $value->nama_sekolah; ?></td>
											</tr>
											<tr>
												<td><b>No Telp</b></td>
												<td>:</td>
												<td><?= $value->telp; ?></td>
											</tr>
											<tr>
												<td><b>Jumlah Murid</b></td>
												<td>:</td>
												<td><?= $value->jumlah_murid; ?></td>
											</tr>
											<tr>
												<td><b>Fasilitas</b></td>
												<td>:</td>
												<td class="ini-konten-list"><?= $value->fasilitas; ?></td>
											</tr>
											<tr>
												<td><b>Prestasi</b></td>
												<td>:</td>
												<td class="ini-konten-list"><?= $value->prestasi; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<img src="<?= base_url('assets/upload/pendidikan/' . $value->struktur_organisasi) ?>" alt="" class="img-fluid ini-gambar-detail" width="100%" />
							<?php endforeach; ?>
							<br><br>
							<strong>Lain-lain</strong><br><br>
							<div class="table-responsive">
								<table id="mytable2" class="table nowrap" width="100%" cellspacing="0">
									<thead class="thead-dark">
										<th scope="col">No</th>
										<th scope="col">Nama</th>
										<th scope="col">Jabatan</th>
									</thead>
									<tbody>
										<?php foreach ($pegawai as $key => $value_p) : ?>
											<tr>
												<td scope="row"><?= $no++; ?></td>
												<td><?= $value_p->nama; ?></td>
												<td><?= $value_p->jabatan; ?></td>
											</tr>
										<?php endforeach; ?>

									</tbody>
								</table>
							</div>

							<a class="btn btn-primary mt-3" href="<?php echo site_url('user/C_userdatadesa/pendidikan') ?>">Kembali</a>
						</div>
						<div class="col-lg-4">
							<h2 class="mb-0 text-primary font-weight-600">
								Berita Terbaru
							</h2>

							<?php foreach ($new_berita as $key => $value) : ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="border-bottom pb-4 pt-4">
											<div class="row">
												<div class="col-sm-8">
													<h5 class="font-weight-600 mb-1">
														<a class="text-decoration-none text-secondary ini-judul-terbaru-kanan limitText<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
															<?php echo $value->judul ?>
														</a>
													</h5>
													<p class="fs-13 text-muted mb-2">
														<span class="mr-2"><?= format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); ?> </span>
													</p>
												</div>
												<div class="col-sm-4">
													<div class="rotate-img">
														<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

							<div class="trending">
								<h2 class="mb-4 text-primary font-weight-600">
									Berita Terpopuler
								</h2>

								<?php foreach ($berita_populer as $key => $value) : ?>
									<div class="mb-4">
										<div class="rotate-img">
											<img src="<?= base_url('assets/upload/berita/' . $value->gambar) ?>" alt="thumb" class="img-fluid" />
										</div>
										<h3 class="mt-3 mb-0 font-weight-600">
											<a class="text-decoration-none text-secondary limitText2<?= $value->id_berita; ?>" href="<?= base_url('user/C_userberita/detail_berita/' . $value->id_berita . '/' . preg_replace("~[^\pL\d]+~u", "-", $value->judul)) ?>">
												<?php echo $value->judul ?>
											</a>
										</h3>
										<p class="fs-13 text-muted mb-0">
											<!-- <span class="mr-2"><?php //format_indo(date("Y-m-d", strtotime($value->tgl_terbit))); 
																	?></span> -->
											<span class="mr-2"><?php echo $value->views ?> kali dilihat</span>
										</p>
									</div>
								<?php endforeach; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php foreach ($new_berita as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText" + no).text($(".limitText" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>

	<?php foreach ($berita_populer as $key => $value) { ?>
		var no = "<?= $value->id_berita; ?>";
		var text = $(".limitText2" + no).html().trim();
		var len = text.length;

		if (len > 35) {
			$(".limitText2" + no).text($(".limitText2" + no).html().trim().substr(0, 34) + ' ...');
		}
	<?php } ?>
</script>
