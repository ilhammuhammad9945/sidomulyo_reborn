<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'C_userberanda';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Halaman User
// Menu Beranda
$route['beranda'] = 'C_userberanda';
// Menu Berita
$route['berita'] = 'user/C_userberita';
$route['detail-berita/(:any)/(:any)'] = 'user/C_userberita/detail_berita/$1/$1';

// Menu Layanan
$route['layanan/permohonan-surat'] = 'user/C_userlayanansurat';
$route['layanan/proses-cari-surat-user'] = 'user/C_userlayanansurat/prosesCekIdPengajuan';
$route['layanan/hasil-cari-surat-user'] = 'user/C_userlayanansurat/cekIdPengajuan';

// Menu profil
$route['profil/sejarah-desa'] = 'user/C_userprofil';
$route['profil/struktur-organisasi-pemerintah-desa'] = 'user/C_userprofil/struktur_organisasi_desa';
$route['profil/aparatur-pemerintah-desa'] = 'user/C_userprofil/aparatur_desa';
$route['detail-aparatur-desa/(:any)'] = 'user/C_userprofil/detail_aparatur_desa/$1';
$route['profil/struktur-organisasi-bpd'] = 'user/C_userprofil/struktur_organisasi_bpd';
$route['profil/aparatur-bpd'] = 'user/C_userprofil/aparatur_bpd';
$route['detail-aparatur-bpd/(:any)'] = 'user/C_userprofil/detail_aparatur_bpd/$1';

// Menu Lembaga Kemasyarakatan
$route['lembaga-kemasyarakatan/pkk'] = 'user/C_userlemmas';
$route['lembaga-kemasyarakatan/lpmd'] = 'user/C_userlemmas/lpmd';
$route['lembaga-kemasyarakatan/karang-taruna'] = 'user/C_userlemmas/karang_taruna';
$route['lembaga-kemasyarakatan/posyandu'] = 'user/C_userlemmas/posyandu';
$route['detail-posyandu/(:any)'] = 'user/C_userlemmas/detail_posyandu/$1';
$route['lembaga-kemasyarakatan/rt-rw'] = 'user/C_userlemmas/rtrw';
$route['detail-rt-rw/(:any)'] = 'user/C_userlemmas/detail_rtrw/$1';
$route['lembaga-kemasyarakatan/karang-werda'] = 'user/C_userlemmas/karang_werda';
$route['detail-karang-werda/(:any)'] = 'user/C_userlemmas/detail_werda/$1';
$route['lembaga-kemasyarakatan/rumah-desa-sehat'] = 'user/C_userlemmas/rumah_desa_sehat';

// Menu Lembaga Desa
$route['lembaga-desa/muslimat'] = 'user/C_userlembagadesa';
$route['detail-muslimat/(:any)'] = 'user/C_userlembagadesa/detail_muslimat/$1';
$route['lembaga-desa/aisyiyah'] = 'user/C_userlembagadesa/aisyiyah';
$route['detail-aisyiyah/(:any)'] = 'user/C_userlembagadesa/detail_aisyiyah/$1';
$route['lembaga-desa/sanggar-seni'] = 'user/C_userlembagadesa/sanggar_seni';
$route['detail-sanggar/(:any)'] = 'user/C_userlembagadesa/detail_ssb/$1';
$route['lembaga-desa/lsm'] = 'user/C_userlembagadesa/lsm';
$route['detail-lsm/(:any)'] = 'user/C_userlembagadesa/detail_lsm/$1';


// Menu Kader Desa
$route['kader-desa/kpmd'] = 'user/C_userkaderdesa';
$route['kader-desa/kpm'] = 'user/C_userkaderdesa/kpm';
$route['kader-desa/kader-teknik'] = 'user/C_userkaderdesa/kader_teknik';
$route['kader-desa/kader-ppkbd'] = 'user/C_userkaderdesa/kader_ppkbd';
$route['kader-desa/kader-tb'] = 'user/C_userkaderdesa/kader_tb';

// Menu Data Desa
$route['data-desa/bumdes'] = 'user/C_userbumdes';
$route['data-desa/umkm'] = 'user/C_userumkm';
$route['data-desa/produkunggulan'] = 'user/C_userumkm/produk_unggulan';
$route['data-desa/kesehatan'] = 'user/C_userkesehatan';
$route['data-desa/statistik'] = 'user/C_userstatistik';

// Halaman Admin
// Menu Beranda
$route['login-admin'] = 'C_login';
$route['logout-admin'] = 'C_login/logout';

// Menu Beranda
$route['beranda-admin'] = 'admin/C_beranda';

// Menu Konten
$route['konten-admin'] = 'admin/C_konten';
$route['tambah-konten'] = 'admin/C_konten/tambah';
$route['edit-konten/(:any)'] = 'admin/C_konten/edit/$1';
$route['hapus-konten/(:any)'] = 'admin/C_konten/hapus/$1';

// Menu Data Master
$route['jenis-konten-admin'] = 'admin/C_master';
$route['tambah-jenis-konten'] = 'admin/C_master/tambah_jenis';
$route['edit-jenis-konten/(:num)'] = 'admin/C_master/edit_jenis/$1';
$route['hapus-jenis-konten/(:num)'] = 'admin/C_master/hapus_jenis/$1';
$route['jabatan-admin'] = 'admin/C_master/jabatan';
$route['tambah-jabatan'] = 'admin/C_master/tambah_jabatan';
$route['edit-jabatan/(:num)'] = 'admin/C_master/edit_jabatan/$1';
$route['hapus-jabatan/(:num)'] = 'admin/C_master/hapus_jabatan/$1';
$route['organisasi-admin'] = 'admin/C_master/organisasi';
$route['tambah-organisasi'] = 'admin/C_master/tambah_organisasi';
$route['edit-organisasi/(:num)'] = 'admin/C_master/edit_organisasi/$1';
$route['hapus-organisasi/(:num)'] = 'admin/C_master/hapus_organisasi/$1';
$route['setting-surat-admin'] = 'admin/C_master/setting_surat';
$route['tambah-setting-surat'] = 'admin/C_master/tambah_setting_surat';
$route['edit-setting-surat/(:num)'] = 'admin/C_master/edit_setting_surat/$1';
$route['hapus-setting-surat/(:num)'] = 'admin/C_master/hapus_setting_surat/$1';
$route['user-admin'] = 'admin/C_master/user';
$route['tambah-user'] = 'admin/C_master/tambah_user';
$route['edit-user/(:num)'] = 'admin/C_master/edit_user/$1';
$route['hapus-user/(:num)'] = 'admin/C_master/hapus_user/$1';
$route['statistik-admin'] = 'admin/C_master/statistik';
$route['tambah-statistik'] = 'admin/C_master/tambah_statistik';
$route['edit-statistik/(:num)'] = 'admin/C_master/edit_statistik/$1';
$route['hapus-statistik/(:num)'] = 'admin/C_master/hapus_statistik/$1';

// Menu Aparatur
$route['aparatur-admin'] = 'admin/C_aparatur';
$route['tambah-aparatur'] = 'admin/C_aparatur/tambah';
$route['edit-aparatur/(:any)'] = 'admin/C_aparatur/edit/$1';
$route['hapus-aparatur/(:any)'] = 'admin/C_aparatur/hapus/$1';
$route['bpd-admin'] = 'admin/C_aparatur/bpd';
$route['tambah-bpd'] = 'admin/C_aparatur/tambah_bpd';
$route['edit-bpd/(:any)'] = 'admin/C_aparatur/edit_bpd/$1';
$route['hapus-bpd/(:any)'] = 'admin/C_aparatur/hapus_bpd/$1';

// Menu Lembaga Kemasyarakatan
$route['pkk-admin'] = 'admin/C_lemmas';
$route['tambah-pkk'] = 'admin/C_lemmas/tambah_pkk';
$route['edit-pkk/(:any)'] = 'admin/C_lemmas/edit_pkk/$1';
$route['hapus-pkk/(:any)'] = 'admin/C_lemmas/hapus_pkk/$1';

$route['lpmd-admin'] = 'admin/C_lemmas/lpmd';
$route['tambah-lpmd'] = 'admin/C_lemmas/tambah_lpmd';
$route['edit-lpmd/(:any)'] = 'admin/C_lemmas/edit_lpmd/$1';
$route['hapus-lpmd/(:any)'] = 'admin/C_lemmas/hapus_lpmd/$1';

$route['karangtaruna-admin'] = 'admin/C_lemmas/karang_taruna';
$route['tambah-karangtaruna'] = 'admin/C_lemmas/tambah_taruna';
$route['edit-karangtaruna/(:any)'] = 'admin/C_lemmas/edit_taruna/$1';
$route['hapus-karangtaruna/(:any)'] = 'admin/C_lemmas/hapus_taruna/$1';

$route['posyandu-admin'] = 'admin/C_lemmas/posyandu';
$route['tambah-posyandu'] = 'admin/C_lemmas/tambah_posyandu';
$route['edit-posyandu/(:any)'] = 'admin/C_lemmas/edit_posyandu/$1';
$route['hapus-posyandu/(:any)'] = 'admin/C_lemmas/hapus_posyandu/$1';

$route['rt-admin'] = 'admin/C_lemmas/rt';
$route['tambah-rt'] = 'admin/C_lemmas/tambah_rt';
$route['edit-rt/(:any)'] = 'admin/C_lemmas/edit_rt/$1';
$route['hapus-rt/(:any)'] = 'admin/C_lemmas/hapus_rt/$1';

$route['rw-admin'] = 'admin/C_lemmas/rw';
$route['tambah-rw'] = 'admin/C_lemmas/tambah_rw';
$route['edit-rw/(:any)'] = 'admin/C_lemmas/edit_rw/$1';
$route['hapus-rw/(:any)'] = 'admin/C_lemmas/hapus_rw/$1';

$route['karangwerda-admin'] = 'admin/C_lemmas/karang_werda';
$route['tambah-karangwerda'] = 'admin/C_lemmas/tambah_werda';
$route['edit-karangwerda/(:any)'] = 'admin/C_lemmas/edit_werda/$1';
$route['hapus-karangwerda/(:any)'] = 'admin/C_lemmas/hapus_werda/$1';

$route['rds-admin'] = 'admin/C_lemmas/rumah_desa_sehat';
$route['tambah-rds'] = 'admin/C_lemmas/tambah_rds';
$route['edit-rds/(:any)'] = 'admin/C_lemmas/edit_rds/$1';
$route['hapus-rds/(:any)'] = 'admin/C_lemmas/hapus_rds/$1';

// Menu Lembaga Desa
$route['muslimat-admin'] = 'admin/C_lembagadesa';
$route['tambah-muslimat'] = 'admin/C_lembagadesa/tambah_muslimat';
$route['edit-muslimat/(:any)'] = 'admin/C_lembagadesa/edit_muslimat/$1';
$route['hapus-muslimat/(:any)'] = 'admin/C_lembagadesa/hapus_muslimat/$1';

$route['aisyiyah-admin'] = 'admin/C_lembagadesa/aisyiyah';
$route['tambah-aisyiyah'] = 'admin/C_lembagadesa/tambah_aisyiyah';
$route['edit-aisyiyah/(:any)'] = 'admin/C_lembagadesa/edit_aisyiyah/$1';
$route['hapus-aisyiyah/(:any)'] = 'admin/C_lembagadesa/hapus_aisyiyah/$1';

$route['ssb-admin'] = 'admin/C_lembagadesa/sanggar_seni';
$route['tambah-ssb'] = 'admin/C_lembagadesa/tambah_ssb';
$route['edit-ssb/(:any)'] = 'admin/C_lembagadesa/edit_ssb/$1';
$route['hapus-ssb/(:any)'] = 'admin/C_lembagadesa/hapus_ssb/$1';

$route['lsm-admin'] = 'admin/C_lembagadesa/lsm';
$route['tambah-lsm'] = 'admin/C_lembagadesa/tambah_lsm';
$route['edit-lsm/(:any)'] = 'admin/C_lembagadesa/edit_lsm/$1';
$route['hapus-lsm/(:any)'] = 'admin/C_lembagadesa/hapus_lsm/$1';

// Menu Data Desa
$route['bumdes-admin'] = 'admin/C_bumdes';
$route['tambah-bumdes'] = 'admin/C_bumdes/tambah_bumdes';
$route['edit-bumdes/(:any)'] = 'admin/C_bumdes/edit_bumdes/$1';
$route['hapus-bumdes/(:any)'] = 'admin/C_bumdes/hapus_bumdes/$1';

$route['umkm-admin'] = 'admin/C_umkm';
$route['tambah-umkm'] = 'admin/C_umkm/tambah_umkm';
$route['edit-umkm/(:any)'] = 'admin/C_umkm/edit_umkm/$1';
$route['hapus-umkm/(:any)'] = 'admin/C_umkm/hapus_umkm/$1';

$route['produkunggulan-admin'] = 'admin/C_umkm/produk_unggulan';
$route['tambah-produkunggulan'] = 'admin/C_umkm/tambah_produkunggulan';
$route['edit-produkunggulan/(:any)'] = 'admin/C_umkm/edit_produkunggulan/$1';
$route['hapus-produkunggulan/(:any)'] = 'admin/C_umkm/hapus_produkunggulan/$1';

$route['kesehatan-admin'] = 'admin/C_kesehatan';
$route['tambah-kesehatan'] = 'admin/C_kesehatan/tambah_kesehatan';
$route['edit-kesehatan/(:any)'] = 'admin/C_kesehatan/edit_kesehatan/$1';
$route['hapus-kesehatan/(:any)'] = 'admin/C_kesehatan/hapus_kesehatan/$1';


// Menu Kader Desa
$route['kpmd-admin'] = 'admin/C_kader';
$route['tambah-kpmd'] = 'admin/C_kader/tambah_kpmd';
$route['edit-kpmd/(:any)'] = 'admin/C_kader/edit_kpmd/$1';
$route['hapus-kpmd/(:any)'] = 'admin/C_kader/hapus_kpmd/$1';

$route['kpm-admin'] = 'admin/C_kader/kpm';
$route['tambah-kpm'] = 'admin/C_kader/tambah_kpm';
$route['edit-kpm/(:any)'] = 'admin/C_kader/edit_kpm/$1';
$route['hapus-kpm/(:any)'] = 'admin/C_kader/hapus_kpm/$1';

$route['kaderteknik-admin'] = 'admin/C_kader/kader_teknik';
$route['tambah-kaderteknik'] = 'admin/C_kader/tambah_teknik';
$route['edit-kaderteknik/(:any)'] = 'admin/C_kader/edit_teknik/$1';
$route['hapus-kaderteknik/(:any)'] = 'admin/C_kader/hapus_teknik/$1';

$route['ppkbd-admin'] = 'admin/C_kader/kader_ppkbd';
$route['tambah-ppkbd'] = 'admin/C_kader/tambah_ppkbd';
$route['edit-ppkbd/(:any)'] = 'admin/C_kader/edit_ppkbd/$1';
$route['hapus-ppkbd/(:any)'] = 'admin/C_kader/hapus_ppkbd/$1';

$route['kadertb-admin'] = 'admin/C_kader/kader_tb';
$route['tambah-kadertb'] = 'admin/C_kader/tambah_tb';
$route['edit-kadertb/(:any)'] = 'admin/C_kader/edit_tb/$1';
$route['hapus-kadertb/(:any)'] = 'admin/C_kader/hapus_tb/$1';

// Menu Layanan Surat - Akta Kelahiran
$route['layanan-akte-admin'] = 'admin/C_akte';
$route['tambah-pengantar-akte'] = 'admin/C_akte/pengantarAkte';
$route['proses-simpan-pengantar-akte'] = 'admin/C_akte/simpanPengantarAkte';
$route['edit-pengantar-akte/(:any)'] = 'admin/C_akte/editPengantarAkte/$1';
$route['proses-edit-pengantar-akte'] = 'admin/C_akte/prosesEditPengantarAkte';
$route['lihat-pengantar-akte/(:any)'] = 'admin/C_akte/lihatPengantarAkte/$1';
$route['done-akte/(:any)'] = 'admin/C_akte/doneAkte/$1';
$route['hapus-pengantar-akte/(:any)'] = 'admin/C_akte/hapusPengantarAkte/$1';
$route['print-pengantar-akte/(:any)'] = 'admin/C_akte/printPengantarAkte/$1';

$route['tambah-pernyataan-akte'] = 'admin/C_akte/pernyataanAkte';
$route['proses-simpan-pernyataan-akte'] = 'admin/C_akte/simpanPernyataanAkte';
$route['edit-pernyataan-akte/(:any)'] = 'admin/C_akte/editPernyataanAkte/$1';
$route['proses-edit-pernyataan-akte'] = 'admin/C_akte/prosesEditPernyataanAkte';
$route['proses-lihat-akte'] = 'admin/C_akte/prosesLihatAkte';
$route['lihat-akte'] = 'admin/C_akte/LihatAkte';
$route['lihat-pernyataan-akte/(:any)'] = 'admin/C_akte/lihatPernyataanAkte/$1';
$route['hapus-pernyataan-akte/(:any)'] = 'admin/C_akte/hapusPernyataanAkte/$1';
$route['print-pernyataan-akte/(:any)'] = 'admin/C_akte/printPernyataanAkte/$1';

$route['tambah-f201-akte'] = 'admin/C_akte/f201Akte';
$route['proses-simpan-f201-akte'] = 'admin/C_akte/simpanf201Akte';
$route['edit-f201-akte/(:any)'] = 'admin/C_akte/editF201Akte/$1';
$route['proses-edit-f201-akte'] = 'admin/C_akte/prosesEditf201Akte';
$route['lihat-f201-akte/(:any)'] = 'admin/C_akte/lihatF201Akte/$1';
$route['hapus-f201-akte/(:any)'] = 'admin/C_akte/hapusF201Akte/$1';
$route['print-f201-akte/(:any)'] = 'admin/C_akte/printF201Akte/$1';

$route['tambah-anakmama-akte'] = 'admin/C_akte/anakmama1Akte';
$route['edit-anakmama-akte/(:any)'] = 'admin/C_akte/editAnakmama1Akte/$1';
$route['lihat-anakmama-akte/(:any)'] = 'admin/C_akte/lihatAnakmama1Akte/$1';
$route['hapus-anakmama-akte/(:any)'] = 'admin/C_akte/hapusAnakmama1Akte/$1';

$route['tambah-anakmama2-akte'] = 'admin/C_akte/anakmama2Akte';
$route['edit-anakmama2-akte/(:any)'] = 'admin/C_akte/editAnakmama2Akte/$1';
$route['lihat-anakmama2-akte/(:any)'] = 'admin/C_akte/lihatAnakmama2Akte/$1';
$route['hapus-anakmama2-akte/(:any)'] = 'admin/C_akte/hapusAnakmama2Akte/$1';

// Menu Layanan Surat - Pindah
$route['layanan-pindah-admin'] = 'admin/C_akte';
$route['tambah-pindah-ikut'] = 'admin/C_pindah/tambahPindah2';
$route['edit-pindah-ikut/(:any)'] = 'admin/C_pindah/editPindah2/$1';
$route['lihat-pindah-ikut/(:any)'] = 'admin/C_pindah/lihatPindah2/$1';
$route['hapus-pindah-ikut/(:any)'] = 'admin/C_pindah/hapusPindah2/$1';

$route['layanan-suket-pindah-admin'] = 'admin/C_pindah';
$route['tambah-suket-pindah'] = 'admin/C_pindah/tambahPindah';
$route['bpwni/(:any)'] = 'admin/C_pindah/bpwni/$1';
$route['edit-suket-pindah/(:any)'] = 'admin/C_pindah/editPindah/$1';
$route['lihat-suket-pindah/(:any)'] = 'admin/C_pindah/lihatPindah/$1';
$route['done-suket-pindah/(:any)'] = 'admin/C_pindah/donePindah/$1';
$route['hapus-suket-pindah/(:any)'] = 'admin/C_pindah/hapusPindah/$1';
$route['tambah-bpwni'] = 'admin/C_pindah/tambahBpwni';
$route['edit-bpwni/(:any)'] = 'admin/C_pindah/editBpwni/$1';
$route['lihat-bpwni/(:any)'] = 'admin/C_pindah/lihatBpwni/$1';
$route['done-bpwni/(:any)'] = 'admin/C_pindah/doneBpwni/$1';
$route['hapus-bpwni/(:any)'] = 'admin/C_pindah/hapusBpwni/$1';
$route['tambah-f108'] = 'admin/C_pindah/addF108';
$route['edit-f108/(:any)'] = 'admin/C_pindah/editF108/$1';
$route['lihat-f108/(:any)'] = 'admin/C_pindah/lihatF108/$1';
$route['tambah-f103'] = 'admin/C_pindah/addF103';
$route['edit-f103/(:any)'] = 'admin/C_pindah/editF103/$1';
$route['lihat-f103/(:any)'] = 'admin/C_pindah/lihatF103/$1';

// Menu Layanan sktm
$route['layanan-sktm-admin'] = 'admin/C_sktm';
$route['tambah-sktm'] = 'admin/C_sktm/tambahSktm';
$route['proses-simpan-sktm'] = 'admin/C_sktm/simpanSktm';
$route['edit-sktm/(:any)'] = 'admin/C_sktm/editSktm/$1';
$route['proses-edit-sktm'] = 'admin/C_sktm/prosesEditSktm';
$route['lihat-sktm/(:any)'] = 'admin/C_sktm/lihatSktm/$1';
$route['done-sktm/(:any)'] = 'admin/C_sktm/doneSktm/$1';
$route['hapus-sktm/(:any)'] = 'admin/C_sktm/hapusSktm/$1';
$route['print-sktm/(:any)'] = 'admin/C_sktm/printSktm/$1';
$route['print-sktm-domisili/(:any)'] = 'admin/C_sktm/printSktmDomisili/$1';
$route['print-sktm-foto/(:any)'] = 'admin/C_sktm/printSktmFoto/$1';


// Menu Layanan Surat - Keterangan Usaha
$route['layanan-usaha-admin'] = 'admin/C_skusaha';
$route['tambah-usaha'] = 'admin/C_skusaha/KetUsaha';
$route['edit-usaha/(:any)'] = 'admin/C_skusaha/editKetUsaha/$1';
$route['ubah-usaha'] = 'admin/C_skusaha/ubahKetUsaha';
$route['lihat-usaha/(:any)'] = 'admin/C_skusaha/lihatKetUsaha/$1';
$route['print-sk-usaha/(:any)'] = 'admin/C_skusaha/printKetUsaha/$1';
$route['hapus-usaha/(:any)'] = 'admin/C_skusaha/hapusKetUsaha/$1';

// Menu Layanan Surat - Keterangan Domisili
$route['layanan-domisili-admin'] = 'admin/C_skdomisili';
$route['tambah-domisili'] = 'admin/C_skdomisili/KetDomisili';
$route['edit-domisili/(:any)'] = 'admin/C_skdomisili/editKetDomisili/$1';
$route['ubah-domisili'] = 'admin/C_skdomisili/ubahKetDomisili';
$route['lihat-domisili/(:any)'] = 'admin/C_skdomisili/lihatKetDomisili/$1';
$route['print-sk-domisili/(:any)'] = 'admin/C_skdomisili/printKetDomisili/$1';
$route['hapus-domisili/(:any)'] = 'admin/C_skdomisili/hapusKetDomisili/$1';

// Menu Layanan Surat - Keterangan Beda Identitas
$route['layanan-identitas-admin'] = 'admin/C_bedaidentitas';
$route['tambah-identitas'] = 'admin/C_bedaidentitas/KetIdentitas';
$route['edit-identitas/(:any)'] = 'admin/C_bedaidentitas/editKetIdentitas/$1';
$route['ubah-identitas'] = 'admin/C_bedaidentitas/ubahKetIdentitas';
$route['lihat-identitas/(:any)'] = 'admin/C_bedaidentitas/lihatKetIdentitas/$1';
$route['print-sk-identitas/(:any)'] = 'admin/C_bedaidentitas/printKetIdentitas/$1';
$route['hapus-identitas/(:any)'] = 'admin/C_bedaidentitas/hapusKetIdentitas/$1';

// Menu Layanan skck
$route['layanan-skck-admin'] = 'admin/C_skck';
$route['tambah-skck'] = 'admin/C_skck/tambahSkck';
$route['proses-simpan-skck'] = 'admin/C_skck/simpanSkck';
$route['edit-skck/(:any)'] = 'admin/C_skck/editSkck/$1';
$route['proses-edit-skck'] = 'admin/C_skck/prosesEditSkck';
$route['lihat-skck/(:any)'] = 'admin/C_skck/lihatSkck/$1';
$route['done-skck/(:any)'] = 'admin/C_skck/doneSkck/$1';
$route['hapus-skck/(:any)'] = 'admin/C_skck/hapusSkck/$1';
$route['print-skck/(:any)'] = 'admin/C_skck/printSkck/$1';

// Menu Layanan surat kematian
$route['layanan-surat-kematian-admin'] = 'admin/C_kematian';
$route['tambah-surat-kematian'] = 'admin/C_kematian/tambahKematian';
$route['proses-simpan-kematian'] = 'admin/C_kematian/simpanKematian';
$route['edit-kematian/(:any)'] = 'admin/C_kematian/editKematian/$1';
$route['proses-edit-kematian'] = 'admin/C_kematian/prosesEditKematian';
$route['lihat-kematian/(:any)'] = 'admin/C_kematian/lihatKematian/$1';
$route['done-kematian/(:any)'] = 'admin/C_kematian/doneKematian/$1';
$route['hapus-kematian/(:any)'] = 'admin/C_kematian/hapusKematian/$1';
$route['print-kematian/(:any)'] = 'admin/C_kematian/printKematian/$1';

// Menu Layanan Surat - BSM
$route['layanan-bsm-admin'] = 'admin/C_bsm';
$route['tambah-bsm'] = 'admin/C_bsm/TambahBsm';
$route['edit-bsm/(:any)'] = 'admin/C_bsm/editBsm/$1';
$route['ubah-bsm'] = 'admin/C_bsm/ubahBsm';
$route['lihat-bsm/(:any)'] = 'admin/C_bsm/lihatBsm/$1';
$route['print-bsm/(:any)'] = 'admin/C_bsm/printBsm/$1';
$route['hapus-bsm/(:any)'] = 'admin/C_bsm/hapusBsm/$1';

// Menu Layanan Surat - Kenal Lahir
$route['layanan-kenal-lahir-admin'] = 'admin/C_kenallahir';
$route['tambah-kenal-lahir'] = 'admin/C_kenallahir/TambahKenal';
$route['edit-kenal-lahir/(:any)'] = 'admin/C_kenallahir/editKenal/$1';
$route['ubah-kenal-lahir'] = 'admin/C_kenallahir/ubahKenal';
$route['lihat-kenal-lahir/(:any)'] = 'admin/C_kenallahir/lihatKenal/$1';
$route['print-kenal-lahir/(:any)'] = 'admin/C_kenallahir/printKenal/$1';
$route['hapus-kenal-lahir/(:any)'] = 'admin/C_kenallahir/hapusKenal/$1';

// Menu Layanan Surat - Pelaporan Kematian
$route['layanan-pelaporan-kematian-admin'] = 'admin/C_pelaporankematian';
$route['tambah-pelaporan-kematian'] = 'admin/C_pelaporankematian/tambahPelaporanKematian';
$route['edit-pelaporan-kematian/(:any)'] = 'admin/C_pelaporankematian/editPelaporanKematian/$1';
$route['ubah-pelaporan-kematian'] = 'admin/C_pelaporankematian/ubahPelaporanKematian';
$route['lihat-pelaporan-kematian/(:any)'] = 'admin/C_pelaporankematian/lihatPelaporanKematian/$1';
$route['print-pelaporan-kematian/(:any)'] = 'admin/C_pelaporankematian/printPelaporanKematian/$1';
$route['hapus-pelaporan-kematian/(:any)'] = 'admin/C_pelaporankematian/hapusPelaporanKematian/$1';

// Menu Layanan Surat - Belum Nikah
$route['layanan-belum-nikah-admin'] = 'admin/C_belumnikah';
$route['tambah-belum-nikah'] = 'admin/C_belumnikah/tambahBelumNikah';
$route['edit-belum-nikah/(:any)'] = 'admin/C_belumnikah/editBelumNikah/$1';
$route['ubah-belum-nikah'] = 'admin/C_belumnikah/ubahBelumNikah';
$route['lihat-belum-nikah/(:any)'] = 'admin/C_belumnikah/lihatBelumNikah/$1';
$route['print-belum-nikah/(:any)'] = 'admin/C_belumnikah/printBelumNikah/$1';
$route['hapus-belum-nikah/(:any)'] = 'admin/C_belumnikah/hapusBelumNikah/$1';

// Menu Layanan Surat - Telah Rekam KTP
$route['layanan-rekam-ktp-admin'] = 'admin/C_rekamktp';
$route['tambah-rekam-ktp'] = 'admin/C_rekamktp/addRekamKtp';
$route['edit-rekam-ktp/(:any)'] = 'admin/C_rekamktp/editRekamKtp/$1';
$route['lihat-rekam-ktp/(:any)'] = 'admin/C_rekamktp/lihatRekamKtp/$1';
$route['print-sk-usaha/(:any)'] = 'admin/C_skusaha/printKetUsaha/$1';
$route['done-rekam-ktp/(:any)'] = 'admin/C_rekamktp/doneRekamKtp/$1';
$route['hapus-rekam-ktp/(:any)'] = 'admin/C_rekamktp/hapusRekamKtp/$1';

$route['layanan-kehilangan-admin'] = 'admin/C_kehilangan';
$route['tambah-kehilangan-akte'] = 'admin/C_kehilangan/addKehilanganAkte';
$route['tambah-kehilangan-ktp'] = 'admin/C_kehilangan/addKehilanganKTP';
$route['tambah-kehilangan-kk'] = 'admin/C_kehilangan/addKehilanganKK';
$route['edit-SKkehilangan/(:any)'] = 'admin/C_kehilangan/editKehilangan/$1';
$route['lihat-SKkehilangan/(:any)'] = 'admin/C_kehilangan/lihatKehilangan/$1';
$route['print-kehilangan/(:any)'] = 'admin/C_skusaha/printKetUsaha/$1';
$route['done-kehilangan/(:any)'] = 'admin/C_kehilangan/doneKehilangan/$1';
$route['hapus-kehilangan/(:any)'] = 'admin/C_kehilangan/hapusKehilangan/$1';

$route['layanan-SKberpergian-admin'] = 'admin/C_berpergian';
$route['tambah-SKberpergian'] = 'admin/C_berpergian/addBerpergian';
$route['edit-berpergian/(:any)'] = 'admin/C_berpergian/editBerpergian/$1';
$route['lihat-SKberpergian/(:any)'] = 'admin/C_berpergian/lihatBerpergian/$1';
$route['print-kehilangan/(:any)'] = 'admin/C_skusaha/printKetUsaha/$1';
$route['done-berpergian/(:any)'] = 'admin/C_berpergian/doneBerpergian/$1';
$route['hapus-berpergian/(:any)'] = 'admin/C_berpergian/hapusBerpergian/$1';

$route['print-anak-mama1/(:any)'] = 'admin/C_akte/printAnakMama1/$1';
$route['print-anak-mama2/(:any)'] = 'admin/C_akte/printAnakMama2/$1';

$route['print-rekam-ktp/(:any)'] = 'admin/C_rekamktp/printRekamKtp/$1';

$route['print-kehilangan-kk/(:any)'] = 'admin/C_kehilangan/printHilangKk/$1';
$route['print-kehilangan-ktp/(:any)'] = 'admin/C_kehilangan/printHilangKtp/$1';
$route['print-kehilangan-akte/(:any)'] = 'admin/C_kehilangan/printHilangAkte/$1';

$route['print-bepergian/(:any)'] = 'admin/C_berpergian/printBepergian/$1';

$route['print-pindah-ikut/(:any)'] = 'admin/C_pindah/printPindahIkut/$1';
$route['print-pindah-sendiri/(:any)'] = 'admin/C_pindah/printPindahSendiri/$1';
$route['print-pindah-bpwni/(:any)'] = 'admin/C_campur/printPindahBpwni/$1';
$route['print-pindah-f108/(:any)'] = 'admin/C_campur/printF108/$1';
$route['print-pindah-f103/(:any)'] = 'admin/C_campur/printF103/$1';

$route['print-laporan'] = 'admin/C_laporan/printLaporanSk';

// Menu Laporan
$route['laporan-surat-keluar'] = 'admin/C_laporan';
