-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:03 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk_hukum`
--

CREATE TABLE `tb_produk_hukum` (
  `id_ph` int(11) NOT NULL,
  `jenis_ph` int(11) NOT NULL COMMENT '1 = Perdes ; 2 = Permakades ; 3 = Perkades ; 4 = SK kades ; 5 = SE Kades',
  `nomor_ph` varchar(100) NOT NULL,
  `tahun_ph` int(11) NOT NULL,
  `tgl_ph` date NOT NULL,
  `judul_ph` varchar(500) NOT NULL,
  `file_ph` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_produk_hukum`
--

INSERT INTO `tb_produk_hukum` (`id_ph`, `jenis_ph`, `nomor_ph`, `tahun_ph`, `tgl_ph`, `judul_ph`, `file_ph`, `date_created`, `date_updated`) VALUES
(1, 1, 'PD/001', 2020, '2020-01-13', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2020 edit', '88f5fe17eeeac2cdbf892103f1868154.pdf', '2021-06-13 10:12:57', '2021-06-13 10:57:48'),
(2, 2, 'PMKD/001', 2021, '2021-06-13', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2020 edit', 'dcb18987b089afccb0c54c3528ae77b4.pdf', '2021-06-13 18:23:23', '0000-00-00 00:00:00'),
(3, 2, 'PMKD/002', 2021, '2021-06-13', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2020 edit', 'ed0eda429bf1b5071f7455f2b1ec3130.pdf', '2021-06-13 18:26:27', '0000-00-00 00:00:00'),
(4, 3, 'PKD/001', 2021, '2021-06-13', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2020 edit', '2a5e45df170072e4f616498594794afd.pdf', '2021-06-13 18:28:25', '0000-00-00 00:00:00'),
(5, 4, 'SKKD/001', 2021, '2021-06-13', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2020 edit', 'e568f5a824668263af3e49ddbd2b4dc9.pdf', '2021-06-13 18:29:06', '0000-00-00 00:00:00'),
(6, 5, 'SEKD/001', 2021, '2021-06-13', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2020 edit', '5132a6515acea6c64193753cdfb5663e.pdf', '2021-06-13 18:29:32', '0000-00-00 00:00:00'),
(7, 1, 'PD/002', 2021, '2021-06-21', 'Laporan Pertanggungjawaban Pelaksanaan Realisasi Anggaran Pendapatan dan Belanja Desa Sidomulyo Tahun Anggaran 2021', 'b6382997221219ce88d8e86da9ba7b92.pdf', '2021-06-21 20:29:53', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_produk_hukum`
--
ALTER TABLE `tb_produk_hukum`
  ADD PRIMARY KEY (`id_ph`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_produk_hukum`
--
ALTER TABLE `tb_produk_hukum`
  MODIFY `id_ph` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
