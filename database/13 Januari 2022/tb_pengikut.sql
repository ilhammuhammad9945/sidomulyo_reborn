-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengikut`
--

CREATE TABLE `tb_pengikut` (
  `id_surat` int(11) NOT NULL,
  `pengikut_nama` varchar(100) NOT NULL,
  `pengikut_gender` varchar(100) NOT NULL,
  `pengikut_umur` int(11) NOT NULL,
  `pengikut_status_kawin` varchar(100) NOT NULL,
  `pengikut_pendidikan` varchar(100) NOT NULL,
  `pengikut_nik` varchar(100) NOT NULL,
  `pengikut_keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengikut`
--

INSERT INTO `tb_pengikut` (`id_surat`, `pengikut_nama`, `pengikut_gender`, `pengikut_umur`, `pengikut_status_kawin`, `pengikut_pendidikan`, `pengikut_nik`, `pengikut_keterangan`) VALUES
(11, 'asd', 'L', 12, 'Belum Kawin', 'SMA', '12', 'asd'),
(11, 'asdas', 'L', 21, 'Belum Kawin', 'Akademik', '1231', 'asda'),
(11, 'zdczsdf', 'P', 32, 'Kawin', 'SMA', '12312', 'xdcfs'),
(1, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd'),
(1, 'asd', 'L', 22, 'Belum Kawin', 'SMA', '13', 'asd'),
(1, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd'),
(1, 'asd', 'L', 10, 'Belum Kawin', 'SMP', '13', 'asd'),
(15, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd'),
(15, 'asd', 'L', 10, 'Belum Kawin', 'SMP', '13', 'asd'),
(15, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd'),
(15, 'asd', 'L', 10, 'Belum Kawin', 'SMP', '13', 'asd');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
