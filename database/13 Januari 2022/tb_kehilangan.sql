-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kehilangan`
--

CREATE TABLE `tb_kehilangan` (
  `id_kehilangan` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `jenis_kehilangan` varchar(100) NOT NULL,
  `hilang_no` varchar(100) NOT NULL,
  `hilang_gender` varchar(100) NOT NULL,
  `hilang_tempat_lahir` varchar(100) NOT NULL,
  `hilang_tanggal_lahir` date NOT NULL,
  `hilang_agama` varchar(100) NOT NULL,
  `hilang_kawin` varchar(100) NOT NULL,
  `hilang_pekerjaan` varchar(100) NOT NULL,
  `hilang_alamat` varchar(500) NOT NULL,
  `hilang_tujuan` varchar(100) NOT NULL,
  `hilang_kebutuhan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kehilangan`
--

INSERT INTO `tb_kehilangan` (`id_kehilangan`, `id_surat`, `jenis_kehilangan`, `hilang_no`, `hilang_gender`, `hilang_tempat_lahir`, `hilang_tanggal_lahir`, `hilang_agama`, `hilang_kawin`, `hilang_pekerjaan`, `hilang_alamat`, `hilang_tujuan`, `hilang_kebutuhan`) VALUES
(1, 5, 'Akte', '1212', 'L', 'Jember', '2022-01-06', 'Islam', 'Belum Kawin', 'wewe', 'wewe', 'wewe', 'wewe'),
(2, 9, 'KTP', '12312', 'L', 'Jember', '2022-01-08', 'Islam', 'Belum Kawin', 'wewe', 'wewe', 'qjwkj', 'asdal'),
(3, 10, 'KK', '1234123', 'L', 'Jember', '2022-01-08', 'Islam', 'Belum Kawin', 'wewe', 'wewe', 'wewe', 'wewe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kehilangan`
--
ALTER TABLE `tb_kehilangan`
  ADD PRIMARY KEY (`id_kehilangan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kehilangan`
--
ALTER TABLE `tb_kehilangan`
  MODIFY `id_kehilangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
