-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_anak_mama`
--

CREATE TABLE `tb_anak_mama` (
  `id_am1` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `am1_pekerjaan` varchar(100) NOT NULL,
  `am1_kwn` varchar(100) NOT NULL,
  `am1_agama` varchar(100) NOT NULL,
  `am1_alamat` varchar(500) NOT NULL,
  `am1_nama_anak` varchar(500) NOT NULL,
  `am1_tempat_lahir_anak` varchar(100) NOT NULL,
  `am1_tanggal_lahir_anak` date NOT NULL,
  `am1_jam_lahir_anak` time NOT NULL,
  `am1_anak_ke` int(11) NOT NULL,
  `am1_nama_ibu` varchar(100) NOT NULL,
  `am1_umur_ibu` int(11) NOT NULL,
  `am1_alamat_ibu` varchar(500) NOT NULL,
  `am1_nama_saksi1` varchar(100) NOT NULL,
  `am1_umur_saksi1` int(11) NOT NULL,
  `am1_pekerjaan_saksi1` varchar(100) NOT NULL,
  `am1_alamat_saksi1` varchar(500) NOT NULL,
  `am1_nama_saksi2` varchar(100) NOT NULL,
  `am1_umur_saksi2` int(11) NOT NULL,
  `am1_pekerjaan_saksi2` varchar(100) NOT NULL,
  `am1_alamat_saksi2` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_anak_mama`
--
ALTER TABLE `tb_anak_mama`
  ADD PRIMARY KEY (`id_am1`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_anak_mama`
--
ALTER TABLE `tb_anak_mama`
  MODIFY `id_am1` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
