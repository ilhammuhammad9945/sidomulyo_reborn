-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_pindah`
--

CREATE TABLE `tb_pindah` (
  `id_pindah` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `jenis_pindah` varchar(100) NOT NULL,
  `pindah_tempat_lahir` varchar(100) NOT NULL,
  `pindah_tanggal_lahir` date NOT NULL,
  `pindah_gender` varchar(100) NOT NULL,
  `pindah_agama` varchar(100) NOT NULL,
  `pindah_status_kawin` varchar(100) NOT NULL,
  `pindah_pendidikan` varchar(100) NOT NULL,
  `pindah_pekerjaan` varchar(100) NOT NULL,
  `pindah_kewarganegaraan` varchar(100) NOT NULL,
  `pindah_alamat_asal` varchar(500) NOT NULL,
  `pindah_tujuan` varchar(100) NOT NULL,
  `pindah_desa_tujuan` varchar(100) NOT NULL,
  `pindah_kec_tujuan` varchar(100) NOT NULL,
  `pindah_kab_tujuan` varchar(500) NOT NULL,
  `pindah_provinsi_tujuan` varchar(100) NOT NULL,
  `pindah_tanggal_berangkat` date NOT NULL,
  `pindah_alasan` varchar(100) NOT NULL,
  `pindah_pengikut` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pindah`
--

INSERT INTO `tb_pindah` (`id_pindah`, `id_surat`, `jenis_pindah`, `pindah_tempat_lahir`, `pindah_tanggal_lahir`, `pindah_gender`, `pindah_agama`, `pindah_status_kawin`, `pindah_pendidikan`, `pindah_pekerjaan`, `pindah_kewarganegaraan`, `pindah_alamat_asal`, `pindah_tujuan`, `pindah_desa_tujuan`, `pindah_kec_tujuan`, `pindah_kab_tujuan`, `pindah_provinsi_tujuan`, `pindah_tanggal_berangkat`, `pindah_alasan`, `pindah_pengikut`) VALUES
(1, 15, 'bersama', 'Jember', '2022-01-10', 'L', 'Islam', 'Belum Kawin', 'SD', 'PNS', 'WNI', 'Ajung', 'Rambi', 'asda', 'asda', 'asd', 'asda', '2022-01-10', 'asdfas', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_pindah`
--
ALTER TABLE `tb_pindah`
  ADD PRIMARY KEY (`id_pindah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_pindah`
--
ALTER TABLE `tb_pindah`
  MODIFY `id_pindah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
