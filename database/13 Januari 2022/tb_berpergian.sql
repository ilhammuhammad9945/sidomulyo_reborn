-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_berpergian`
--

CREATE TABLE `tb_berpergian` (
  `id_berpergian` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `pergi_gender` varchar(100) NOT NULL,
  `pergi_tempat_lahir` varchar(100) NOT NULL,
  `pergi_tanggal_lahir` date NOT NULL,
  `pergi_kwn` varchar(100) NOT NULL,
  `pergi_agama` varchar(100) NOT NULL,
  `pergi_status_kawin` varchar(100) NOT NULL,
  `pergi_pekerjaan` varchar(100) NOT NULL,
  `pergi_pendidikan` varchar(100) NOT NULL,
  `pergi_alamat_asal` varchar(500) NOT NULL,
  `pergi_tujuan` varchar(100) NOT NULL,
  `pergi_tujuan_desa` varchar(100) NOT NULL,
  `pergi_tujuan_kecamatan` varchar(100) NOT NULL,
  `pergi_tujuan_kab` varchar(100) NOT NULL,
  `pergi_tujuan_provinsi` varchar(100) NOT NULL,
  `pergi_tanggal_berangkat` date NOT NULL,
  `pergi_alasan` varchar(100) NOT NULL,
  `pergi_pengikut` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_berpergian`
--

INSERT INTO `tb_berpergian` (`id_berpergian`, `id_surat`, `pergi_gender`, `pergi_tempat_lahir`, `pergi_tanggal_lahir`, `pergi_kwn`, `pergi_agama`, `pergi_status_kawin`, `pergi_pekerjaan`, `pergi_pendidikan`, `pergi_alamat_asal`, `pergi_tujuan`, `pergi_tujuan_desa`, `pergi_tujuan_kecamatan`, `pergi_tujuan_kab`, `pergi_tujuan_provinsi`, `pergi_tanggal_berangkat`, `pergi_alasan`, `pergi_pengikut`) VALUES
(1, 11, 'L', 'Jember', '2022-01-08', 'WNI', 'Islam', 'Belum Kawin', 'PNS', 'Akademik', 'Ajung', 'Rambi', 'asda', 'asda', 'asd', 'asda', '2022-01-08', 'asda', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_berpergian`
--
ALTER TABLE `tb_berpergian`
  ADD PRIMARY KEY (`id_berpergian`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_berpergian`
--
ALTER TABLE `tb_berpergian`
  MODIFY `id_berpergian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
