-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_rekam_ktp`
--

CREATE TABLE `tb_rekam_ktp` (
  `id_rk` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `rk_gender` varchar(100) NOT NULL,
  `rk_tempat_lahir` varchar(100) NOT NULL,
  `rk_tanggal_lahir` date NOT NULL,
  `rk_pekerjaan` varchar(100) NOT NULL,
  `rk_agama` varchar(100) NOT NULL,
  `rk_pendidikan` varchar(100) NOT NULL,
  `rk_status_kawin` varchar(100) NOT NULL,
  `rk_alamat` varchar(500) NOT NULL,
  `rk_kebutuhan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_rekam_ktp`
--

INSERT INTO `tb_rekam_ktp` (`id_rk`, `id_surat`, `rk_gender`, `rk_tempat_lahir`, `rk_tanggal_lahir`, `rk_pekerjaan`, `rk_agama`, `rk_pendidikan`, `rk_status_kawin`, `rk_alamat`, `rk_kebutuhan`) VALUES
(1, 1, 'L', 'asda', '2022-01-10', 'dasdas', 'Islam', 'sdas', 'Belum Kawin', 'asdasd', 'dasd'),
(2, 13, 'L', 'asda', '2022-01-10', 'dasdas', 'Islam', 'sdas', 'Belum Kawin', 'asdasd', 'dasd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_rekam_ktp`
--
ALTER TABLE `tb_rekam_ktp`
  ADD PRIMARY KEY (`id_rk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_rekam_ktp`
--
ALTER TABLE `tb_rekam_ktp`
  MODIFY `id_rk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
