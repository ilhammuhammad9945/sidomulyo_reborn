-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2022 at 08:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_skck`
--

CREATE TABLE `tb_skck` (
  `id_skck` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `skck_gender` varchar(100) NOT NULL,
  `skck_tempat_lahir` varchar(100) NOT NULL,
  `skck_tanggal_lahir` date NOT NULL,
  `skck_kwn` varchar(100) NOT NULL,
  `skck_agama` varchar(100) NOT NULL,
  `skck_status_kawin` varchar(100) NOT NULL,
  `skck_ktp_kk` varchar(100) NOT NULL,
  `skck_pekerjaan` varchar(100) NOT NULL,
  `skck_pendidikan` varchar(100) NOT NULL,
  `skck_alamat` varchar(500) NOT NULL,
  `skck_keperluan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_skck`
--

INSERT INTO `tb_skck` (`id_skck`, `id_surat`, `skck_gender`, `skck_tempat_lahir`, `skck_tanggal_lahir`, `skck_kwn`, `skck_agama`, `skck_status_kawin`, `skck_ktp_kk`, `skck_pekerjaan`, `skck_pendidikan`, `skck_alamat`, `skck_keperluan`) VALUES
(1, 13, 'L', 'Jember', '2021-12-19', 'Indonesia', 'Islam', 'Belum Kawin', '986987', 'Guru', 'S2', 'Jember', 'Melamar Pekerjaan'),
(2, 3, 'L', 'Jember', '2021-12-24', 'Indonesia', 'Islam', 'Belum Kawin', '986987', 'Guru', 'S2', 'Jember', 'Melamar Pekerjaan'),
(3, 6, 'L', 'Jember', '2022-01-08', 'Indonesia', 'Islam', 'Belum Kawin', '986987', 'Guru', 'S2', 'Jember', 'Melamar Pekerjaan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_skck`
--
ALTER TABLE `tb_skck`
  ADD PRIMARY KEY (`id_skck`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_skck`
--
ALTER TABLE `tb_skck`
  MODIFY `id_skck` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
