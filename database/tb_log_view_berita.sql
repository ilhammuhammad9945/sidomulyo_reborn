-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_log_view_berita`
--

CREATE TABLE `tb_log_view_berita` (
  `id_log` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip` varchar(40) NOT NULL,
  `id_berita` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_log_view_berita`
--

INSERT INTO `tb_log_view_berita` (`id_log`, `tanggal`, `ip`, `id_berita`) VALUES
(1, '2021-06-27 06:24:54', '::1', 'B0001'),
(2, '2021-07-05 14:06:24', '::1', 'B0005'),
(3, '2021-10-03 12:55:33', '::1', 'B0001'),
(4, '2021-10-03 12:58:02', '::1', 'B0006'),
(5, '2021-10-19 13:12:42', '::1', 'B0001'),
(6, '2021-11-02 15:31:06', '::1', 'B0006'),
(7, '2021-11-02 15:32:11', '::1', 'B0005'),
(8, '2021-11-03 01:06:29', '::1', 'B0004');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_log_view_berita`
--
ALTER TABLE `tb_log_view_berita`
  ADD PRIMARY KEY (`id_log`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_log_view_berita`
--
ALTER TABLE `tb_log_view_berita`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
