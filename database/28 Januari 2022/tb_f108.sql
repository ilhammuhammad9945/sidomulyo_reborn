-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2022 at 02:15 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_f108`
--

CREATE TABLE `tb_f108` (
  `id_surat` int(11) NOT NULL,
  `f108_no_kk` int(100) NOT NULL,
  `f108_nama_kep_kel` varchar(100) NOT NULL,
  `f108_alamat_asal` varchar(500) NOT NULL,
  `f108_desa_asal` varchar(100) NOT NULL,
  `f108_kec_asal` varchar(100) NOT NULL,
  `f108_kab_asal` varchar(100) NOT NULL,
  `f108_prov_asal` varchar(100) NOT NULL,
  `f108_alasan_pindah` varchar(100) NOT NULL,
  `f108_alamat_pindah` varchar(500) NOT NULL,
  `f108_desa_pindah` varchar(100) NOT NULL,
  `f108_kec_pindah` varchar(100) NOT NULL,
  `f108_kab_pindah` varchar(100) NOT NULL,
  `f108_prov_pindah` varchar(100) NOT NULL,
  `f108_kode_pos_pindah` varchar(100) NOT NULL,
  `f108_jml_keluarga_pindah` int(11) NOT NULL,
  `f108_klasifikasi_pindah` varchar(100) NOT NULL,
  `f108_jenis_pindah` varchar(100) NOT NULL,
  `f108_status_no_kk` varchar(100) NOT NULL,
  `f108_status_no_kk_pindah` varchar(100) NOT NULL,
  `f108_tgl_pindah` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_f108`
--

INSERT INTO `tb_f108` (`id_surat`, `f108_no_kk`, `f108_nama_kep_kel`, `f108_alamat_asal`, `f108_desa_asal`, `f108_kec_asal`, `f108_kab_asal`, `f108_prov_asal`, `f108_alasan_pindah`, `f108_alamat_pindah`, `f108_desa_pindah`, `f108_kec_pindah`, `f108_kab_pindah`, `f108_prov_pindah`, `f108_kode_pos_pindah`, `f108_jml_keluarga_pindah`, `f108_klasifikasi_pindah`, `f108_jenis_pindah`, `f108_status_no_kk`, `f108_status_no_kk_pindah`, `f108_tgl_pindah`) VALUES
(17, 123, 'dcasdc', 'sdasd', 'vsdfv', 'sdfs', 'sdf', 'sdf', 'Pekerjaan', 'sdfs', 'sdf', 'sdf', 'sdfs', 'sdfs', '32', 2, 'Dalam Satu Desa / Kelurahan', 'Kepala Keluarga', 'Numpang KK', 'Numpang KK', '2022-01-22');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
