-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:03 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk_unggulan`
--

CREATE TABLE `tb_produk_unggulan` (
  `id_pr` varchar(50) NOT NULL,
  `nama_pr` varchar(255) NOT NULL,
  `umkm_pr` varchar(50) NOT NULL,
  `foto_produk` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_produk_unggulan`
--

INSERT INTO `tb_produk_unggulan` (`id_pr`, `nama_pr`, `umkm_pr`, `foto_produk`) VALUES
('60cb71e2359af', 'Nasi Goreng', '60cb6f974a959', '60cb71e2359af.jpeg'),
('60cb71f34e133', 'Terang Bulan', '60cb6fc518763', '60cb71f34e133.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_produk_unggulan`
--
ALTER TABLE `tb_produk_unggulan`
  ADD PRIMARY KEY (`id_pr`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
