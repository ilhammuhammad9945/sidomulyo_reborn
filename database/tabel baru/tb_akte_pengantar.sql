-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2021 at 11:31 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akte_pengantar`
--

CREATE TABLE `tb_akte_pengantar` (
  `id_akte_pengantar` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `ank_nama` varchar(100) NOT NULL,
  `ank_gender` varchar(100) NOT NULL,
  `ank_tempat_lahir` varchar(100) NOT NULL,
  `ank_tgl_lahir` date NOT NULL,
  `ank_jenis_kelahiran` varchar(100) NOT NULL,
  `ank_anak_ke` int(11) NOT NULL,
  `ortu_nama_ayah` varchar(100) NOT NULL,
  `ortu_nama_ibu` varchar(100) NOT NULL,
  `ortu_nama_saksi1` varchar(100) NOT NULL,
  `ortu_nama_saksi2` varchar(100) NOT NULL,
  `brk_suket_lahir` varchar(100) NOT NULL,
  `brk_surat_nikah` varchar(100) NOT NULL,
  `brk_ktp` varchar(100) NOT NULL,
  `brk_kk` varchar(100) NOT NULL,
  `brk_ijazah` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akte_pengantar`
--

INSERT INTO `tb_akte_pengantar` (`id_akte_pengantar`, `id_surat`, `ank_nama`, `ank_gender`, `ank_tempat_lahir`, `ank_tgl_lahir`, `ank_jenis_kelahiran`, `ank_anak_ke`, `ortu_nama_ayah`, `ortu_nama_ibu`, `ortu_nama_saksi1`, `ortu_nama_saksi2`, `brk_suket_lahir`, `brk_surat_nikah`, `brk_ktp`, `brk_kk`, `brk_ijazah`) VALUES
(1, 2, 'Tes', 'L', 'Jember', '2021-12-12', 'Tunggal', 1, 'Bapak Tes', 'Ibu Tes', 'Saksi Tes', 'Saksi Tes', '513f05ec9ab945e5faff253b60bdaa98.jpg', 'd7cb5a8bb052ebaee34ed13b654d3b5a.jpg', '19e8f741240c4e159bb122bd83de05d7.jpg', 'c49e9905c878b5286c0519a4d887bc62.jpg', '91802042d3cb21436ce9010143197d8c.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_akte_pengantar`
--
ALTER TABLE `tb_akte_pengantar`
  ADD PRIMARY KEY (`id_akte_pengantar`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_akte_pengantar`
--
ALTER TABLE `tb_akte_pengantar`
  MODIFY `id_akte_pengantar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
