-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2021 at 11:30 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akte_f_dua`
--

CREATE TABLE `tb_akte_f_dua` (
  `id_akte_f_dua` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `kep_keluarga_nama` varchar(100) NOT NULL,
  `nomor_kk` varchar(100) NOT NULL,
  `ank_pukul_lahir` time NOT NULL,
  `ank_penolong_lahir` varchar(100) NOT NULL,
  `ank_berat` int(11) NOT NULL,
  `ank_panjang` int(11) NOT NULL,
  `plp_nik` varchar(100) NOT NULL,
  `plp_nama` varchar(100) NOT NULL,
  `plp_umur` int(11) NOT NULL,
  `plp_gender` varchar(100) NOT NULL,
  `plp_pekerjaan` varchar(100) NOT NULL,
  `plp_alamat` varchar(100) NOT NULL,
  `saksi1_nik` varchar(100) NOT NULL,
  `saksi1_umur` int(11) NOT NULL,
  `saksi1_pekerjaan` varchar(100) NOT NULL,
  `saksi1_alamat` varchar(100) NOT NULL,
  `saksi2_nik` varchar(100) NOT NULL,
  `saksi2_umur` int(11) NOT NULL,
  `saksi2_pekerjaan` varchar(100) NOT NULL,
  `saksi2_alamat` varchar(100) NOT NULL,
  `ibu_nik` varchar(100) NOT NULL,
  `ibu_tgl_lahir` date NOT NULL,
  `ibu_umur` int(11) NOT NULL,
  `ibu_pekerjaan` varchar(100) NOT NULL,
  `ibu_alamat` varchar(100) NOT NULL,
  `ibu_kwn` varchar(100) NOT NULL,
  `ibu_kebangsaan` varchar(100) NOT NULL,
  `ibu_tgl_kawin` date NOT NULL,
  `ibu_umur_kawin` int(11) NOT NULL,
  `ayah_nik` varchar(100) NOT NULL,
  `ayah_tgl_lahir` date NOT NULL,
  `ayah_umur` int(11) NOT NULL,
  `ayah_pekerjaan` varchar(100) NOT NULL,
  `ayah_alamat` varchar(100) NOT NULL,
  `ayah_kwn` varchar(100) NOT NULL,
  `ayah_kebangsaan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akte_f_dua`
--

INSERT INTO `tb_akte_f_dua` (`id_akte_f_dua`, `id_surat`, `kep_keluarga_nama`, `nomor_kk`, `ank_pukul_lahir`, `ank_penolong_lahir`, `ank_berat`, `ank_panjang`, `plp_nik`, `plp_nama`, `plp_umur`, `plp_gender`, `plp_pekerjaan`, `plp_alamat`, `saksi1_nik`, `saksi1_umur`, `saksi1_pekerjaan`, `saksi1_alamat`, `saksi2_nik`, `saksi2_umur`, `saksi2_pekerjaan`, `saksi2_alamat`, `ibu_nik`, `ibu_tgl_lahir`, `ibu_umur`, `ibu_pekerjaan`, `ibu_alamat`, `ibu_kwn`, `ibu_kebangsaan`, `ibu_tgl_kawin`, `ibu_umur_kawin`, `ayah_nik`, `ayah_tgl_lahir`, `ayah_umur`, `ayah_pekerjaan`, `ayah_alamat`, `ayah_kwn`, `ayah_kebangsaan`) VALUES
(1, 2, 'Bapak Tes', '123', '00:25:00', 'Bidan/Perawat', 1, 30, '555', 'Tes Pelapor', 40, 'L', 'Guru', 'Ajung', '454', 60, 'Tani', 'Ajung', '456', 55, 'Penjahit', 'Ajung', '123', '2021-12-13', 50, 'Guru', 'Ajung', 'WNI', 'Indonesia', '2021-12-13', 20, '125', '2021-12-13', 54, 'PNS', 'Ajung', 'WNI', 'Indonesia');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
