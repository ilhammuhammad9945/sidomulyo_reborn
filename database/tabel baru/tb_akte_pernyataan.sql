-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2021 at 11:31 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akte_pernyataan`
--

CREATE TABLE `tb_akte_pernyataan` (
  `id_skt_pernyataan` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `pmh_tempat_lahir` varchar(100) NOT NULL,
  `pmh_tgl_lahir` date NOT NULL,
  `pmh_pekerjaan` varchar(100) NOT NULL,
  `pmh_alamat` varchar(100) NOT NULL,
  `saksi_nama_1` varchar(100) NOT NULL,
  `saksi_alamat_1` varchar(100) NOT NULL,
  `saksi_hub_1` varchar(100) NOT NULL,
  `saksi_nama_2` varchar(100) NOT NULL,
  `saksi_alamat_2` varchar(100) NOT NULL,
  `saksi_hub_2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akte_pernyataan`
--

INSERT INTO `tb_akte_pernyataan` (`id_skt_pernyataan`, `id_surat`, `pmh_tempat_lahir`, `pmh_tgl_lahir`, `pmh_pekerjaan`, `pmh_alamat`, `saksi_nama_1`, `saksi_alamat_1`, `saksi_hub_1`, `saksi_nama_2`, `saksi_alamat_2`, `saksi_hub_2`) VALUES
(1, 2, 'Jember', '2021-12-01', 'PNS', 'Ajung', 'Saksi Tes', 'Ajung', 'Paman', 'Saksi Tes', 'Ajung', 'Pakde');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_akte_pernyataan`
--
ALTER TABLE `tb_akte_pernyataan`
  ADD PRIMARY KEY (`id_skt_pernyataan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_akte_pernyataan`
--
ALTER TABLE `tb_akte_pernyataan`
  MODIFY `id_skt_pernyataan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
