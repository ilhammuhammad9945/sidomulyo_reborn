-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2021 at 12:49 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_sktm`
--

CREATE TABLE `tb_sktm` (
  `id_sktm` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `pmh_gender` varchar(100) NOT NULL,
  `pmh_umur` int(11) NOT NULL,
  `pmh_agama` varchar(100) NOT NULL,
  `pmh_status_kawin` varchar(100) NOT NULL,
  `pmh_pekerjaan` varchar(100) NOT NULL,
  `pmh_alamat` varchar(100) NOT NULL,
  `pmh_no_kk` varchar(100) NOT NULL,
  `pmh_kepala_keluarga` varchar(100) NOT NULL,
  `pmh_kebutuhan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sktm`
--

INSERT INTO `tb_sktm` (`id_sktm`, `id_surat`, `pmh_gender`, `pmh_umur`, `pmh_agama`, `pmh_status_kawin`, `pmh_pekerjaan`, `pmh_alamat`, `pmh_no_kk`, `pmh_kepala_keluarga`, `pmh_kebutuhan`) VALUES
(1, 6, '', 0, '', '', '', 'Alamat SKtm', '', 'Kk Sktm', ''),
(2, 7, '', 0, '', '', '', 'Alamat SKtm', '', 'Kk Sktm', ''),
(3, 9, '', 0, '', '', '', 'Gebang', '', 'Budi', ''),
(4, 11, 'L', 12, 'Islam', 'Kawin', 'Guru', 'Jember', '9836749', 'Tes KK 2', 'Beasiswa 2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_sktm`
--
ALTER TABLE `tb_sktm`
  ADD PRIMARY KEY (`id_sktm`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_sktm`
--
ALTER TABLE `tb_sktm`
  MODIFY `id_sktm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
