-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2021 at 12:49 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_foto_sktm`
--

CREATE TABLE `tb_foto_sktm` (
  `id_surat` int(11) NOT NULL,
  `keterangan_foto` int(11) NOT NULL,
  `foto_sktm` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_foto_sktm`
--

INSERT INTO `tb_foto_sktm` (`id_surat`, `keterangan_foto`, `foto_sktm`) VALUES
(5, 0, 'acc5b55a71db3f332c15dad171821464.jpg'),
(5, 0, '1df843a6e9ada525f5942608c582de11.jpg'),
(5, 0, '9c2000f5893a436c16141ec3da661c01.jpg'),
(6, 0, '07300cae32ee9ace265e9adec3600b92.jpg'),
(6, 0, '85fe31d9858e36cdf89c7ffcf9aa0acc.jpg'),
(6, 0, '73bffdc6bb65744e991eff21d7f2c7e5.jpg'),
(7, 0, 'df5e938c6dc17a18639d17019877c11f.jpg'),
(7, 0, '6810a95a15dd253d0e0ef01516a04277.jpg'),
(7, 0, 'd80f3b4d654160524dc47d76813429e9.jpg'),
(9, 0, 'd14c5cc4cb70fc13aa2db20523c8dc1d.jpg'),
(9, 0, 'f1ba7dd2c3e8fc64fd1e4e5b138497e9.jpg'),
(9, 0, '7165b2bf44093edbcd8f55ed04f92699.jpg'),
(10, 1, 'ff4052bcbf8eb8a52f501616cfda2328.jpg'),
(10, 2, 'df9a3433c2ca4c4ff35b48a2cbc9e962.jpg'),
(10, 3, 'ed98d5ff80aef8c153181e1b36695480.jpg'),
(11, 1, '58e98634e995ac4307df7d1bdd5136d2.jpg'),
(11, 2, '1390b9f75a9abf40bd12ee27861a98a0.jpeg'),
(11, 3, '4faa7431ed7ea611b7c62b14855dd826.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
