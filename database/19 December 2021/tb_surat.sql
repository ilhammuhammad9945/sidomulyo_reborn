-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2021 at 12:48 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_surat`
--

CREATE TABLE `tb_surat` (
  `id_surat` int(11) NOT NULL,
  `jenis_surat` varchar(100) NOT NULL,
  `nik_pemohon` varchar(100) NOT NULL,
  `tgl_pengajuan` datetime NOT NULL,
  `nama_pemohon` varchar(100) NOT NULL,
  `progress_pembuatan` int(11) NOT NULL,
  `status_pembuatan` varchar(100) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_surat`
--

INSERT INTO `tb_surat` (`id_surat`, `jenis_surat`, `nik_pemohon`, `tgl_pengajuan`, `nama_pemohon`, `progress_pembuatan`, `status_pembuatan`, `aktif`) VALUES
(2, 'Akte Kelahiran', '0123456', '2021-12-12 13:00:00', 'Bapak Tes', 100, 'Dalam Proses', 1),
(4, 'sktm', '2323', '2021-12-18 12:35:51', 'Tes Sktm', 0, 'Dalam Proses', 1),
(5, 'sktm', '2323', '2021-12-18 12:36:14', 'Tes Sktm', 0, 'Dalam Proses', 1),
(6, 'sktm', '2323', '2021-12-18 12:37:45', 'Tes Sktm', 0, 'Dalam Proses', 1),
(7, 'sktm', '2323', '2021-12-18 12:39:43', 'Tes Sktm1', 100, 'Dalam Proses', 1),
(8, 'Akte Kelahiran', '232333', '2021-12-18 14:27:57', 'Ayah Rosid', 100, 'Dalam Proses', 1),
(9, 'sktm', '13241324', '2021-12-18 15:02:04', 'Udin', 100, 'Dalam Proses', 1),
(10, 'sktm', '13241324', '2021-12-19 06:51:20', 'Tes Sktm 2', 0, 'Dalam Proses', 1),
(11, 'sktm', '13241324', '2021-12-19 06:52:28', 'Tes Sktm 2', 100, 'Dalam Proses', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_surat`
--
ALTER TABLE `tb_surat`
  ADD PRIMARY KEY (`id_surat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_surat`
--
ALTER TABLE `tb_surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
