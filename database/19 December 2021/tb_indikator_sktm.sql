-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2021 at 12:49 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_indikator_sktm`
--

CREATE TABLE `tb_indikator_sktm` (
  `id_surat` int(11) NOT NULL,
  `indikator` int(11) NOT NULL,
  `kriteria` int(11) NOT NULL,
  `hasil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_indikator_sktm`
--

INSERT INTO `tb_indikator_sktm` (`id_surat`, `indikator`, `kriteria`, `hasil`) VALUES
(7, 1, 1, 1),
(7, 2, 2, 0),
(7, 3, 3, 1),
(7, 4, 4, 0),
(7, 5, 5, 1),
(7, 6, 6, 0),
(7, 7, 7, 1),
(7, 8, 8, 1),
(9, 1, 1, 1),
(9, 2, 2, 0),
(9, 3, 3, 1),
(9, 4, 4, 0),
(9, 5, 5, 1),
(9, 6, 6, 0),
(9, 7, 7, 1),
(9, 8, 8, 0),
(11, 1, 1, 0),
(11, 2, 2, 1),
(11, 3, 3, 0),
(11, 4, 4, 1),
(11, 5, 5, 0),
(11, 6, 6, 1),
(11, 7, 7, 0),
(11, 8, 8, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
