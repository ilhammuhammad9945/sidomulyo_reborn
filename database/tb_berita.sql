-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id_berita` varchar(5) NOT NULL,
  `judul` varchar(500) NOT NULL,
  `headline` text NOT NULL,
  `isi` text NOT NULL,
  `editor` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `tampil` int(11) NOT NULL COMMENT '0 = tidak tampil ; 1 = tampil',
  `tgl_terbit` datetime NOT NULL,
  `views` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_berita`
--

INSERT INTO `tb_berita` (`id_berita`, `judul`, `headline`, `isi`, `editor`, `gambar`, `tampil`, `tgl_terbit`, `views`, `date_created`, `date_updated`) VALUES
('B0001', 'Kegiatan Ngaji Bareng Pak Ustad', '<p>That dominion stars lights dominion divide years for fourth have don&#39;t stars is that he earth it first without heaven in place seed it second morning saying.</p>\r\n', '<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n\r\n<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n', 2, '6228b849ac2c3cd46955ea421b709adf.jpg', 1, '2021-06-27 13:43:33', 5, '2021-06-05 20:13:42', '2021-06-27 13:43:33'),
('B0002', 'Acara Kenduri', '<p>That dominion stars lights dominion divide years for fourth have don&#39;t stars is that he earth it first without heaven in place seed it second morning saying.</p>\r\n', '<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n\r\n<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n', 2, '6228b849ac2c3cd46955ea421b709adf.jpg', 1, '2021-06-27 13:59:45', 0, '2021-06-05 21:20:22', '2021-06-27 13:59:45'),
('B0003', 'Peringatan Idul Adha f', '<p>That dominion stars lights dominion divide years for fourth have don&#39;t stars is that he earth it first without heaven in place seed it second morning saying.</p>\r\n', '<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n\r\n<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n', 2, '6228b849ac2c3cd46955ea421b709adf.jpg', 1, '2021-06-27 14:00:24', 0, '2021-06-08 20:35:11', '2021-06-27 14:00:24'),
('B0004', 'Idul Fitri Idul Fitri Idul Fitri Idul Fitri', '<p>That dominion stars lights dominion divide years for fourth have don&#39;t stars is that he earth it first without heaven in place seed it second morning saying.</p>\r\n', '<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n\r\n<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n', 2, '6228b849ac2c3cd46955ea421b709adf.jpg', 1, '2021-06-27 14:00:47', 6, '2021-06-11 06:41:02', '2021-06-27 14:00:47'),
('B0005', 'moto gp moto gp moto gp moto gp moto gp moto gp moto gp moto gp moto gp', '<p>That dominion stars lights dominion divide years for fourth have don&#39;t stars is that he earth it first without heaven in place seed it second morning saying.</p>\r\n', '<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n\r\n<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n', 2, '6228b849ac2c3cd46955ea421b709adf.jpg', 1, '2021-06-27 14:01:03', 7, '2021-06-11 06:04:52', '2021-06-27 14:01:03'),
('B0006', 'Tasyakuran Tasyakuran Tasyakuran', '<p>That dominion stars lights dominion divide years for fourth have don&#39;t stars is that he earth it first without heaven in place seed it second morning saying.</p>\r\n', '<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n\r\n<p>My hero when I was a kid was my mom. Same for everyone I knew. Moms are untouchable. They&rsquo;re elegant, smart, beautiful, kind&hellip;everything we want to be. At 29 years old, my favorite compliment is being told that I look like my mom. Seeing myself in her image, like this daughter up top, makes me so proud of how far I&rsquo;ve come, and so thankful for where I come from. the refractor telescope uses a convex lens to focus the light on the eyepiece. The reflector telescope has a concave lens which means it telescope sits on. The mount is the actual tripod and the wedge is the device that lets you attach the telescope to the mount. Moms are like&hellip;buttons? Moms are like glue. Moms are like pizza crusts. Moms are the ones who make sure things happen&mdash;from birth to school lunch.</p>\r\n', 2, '6228b849ac2c3cd46955ea421b709adf.jpg', 1, '2021-06-27 14:01:20', 2, '2021-06-11 05:06:24', '2021-06-27 14:01:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id_berita`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
