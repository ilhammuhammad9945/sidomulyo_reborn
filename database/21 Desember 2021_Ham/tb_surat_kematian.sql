-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2021 at 12:35 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_surat_kematian`
--

CREATE TABLE `tb_surat_kematian` (
  `id_surat_kematian` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `umur_meninggal` int(11) NOT NULL,
  `gender_meninggal` varchar(100) NOT NULL,
  `alamat_meninggal` varchar(500) NOT NULL,
  `tanggal_meninggal` date NOT NULL,
  `tempat_meninggal` varchar(100) NOT NULL,
  `penyebab_meninggal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_surat_kematian`
--

INSERT INTO `tb_surat_kematian` (`id_surat_kematian`, `id_surat`, `umur_meninggal`, `gender_meninggal`, `alamat_meninggal`, `tanggal_meninggal`, `tempat_meninggal`, `penyebab_meninggal`) VALUES
(1, 14, 22, 'L', 'asdfas', '2021-12-20', 'asda', 'asfdas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_surat_kematian`
--
ALTER TABLE `tb_surat_kematian`
  ADD PRIMARY KEY (`id_surat_kematian`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_surat_kematian`
--
ALTER TABLE `tb_surat_kematian`
  MODIFY `id_surat_kematian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
