-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:03 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_warga_sekolah`
--

CREATE TABLE `tb_warga_sekolah` (
  `id_warga_sekolah` int(11) NOT NULL,
  `id_pend` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `foto_diri` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_warga_sekolah`
--

INSERT INTO `tb_warga_sekolah` (`id_warga_sekolah`, `id_pend`, `nama`, `jabatan`, `foto_diri`) VALUES
(1, 1, 'Sutoyo M.Pd', 'kepsek', ''),
(2, 1, 'Ainun S.Pd', 'wakepsek', ''),
(3, 1, 'Rozak S.Pd', 'pengajar', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_warga_sekolah`
--
ALTER TABLE `tb_warga_sekolah`
  ADD PRIMARY KEY (`id_warga_sekolah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_warga_sekolah`
--
ALTER TABLE `tb_warga_sekolah`
  MODIFY `id_warga_sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
