-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2022 at 11:42 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kenal_lahir`
--

CREATE TABLE `tb_kenal_lahir` (
  `id_knl_lhir` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `knl_anak` varchar(100) NOT NULL,
  `knl_jkel` char(1) NOT NULL,
  `knl_kwn` varchar(20) NOT NULL,
  `knl_agama` varchar(20) NOT NULL,
  `knl_status_kwn` varchar(20) NOT NULL,
  `knl_tempat_lahir` varchar(50) NOT NULL,
  `knl_tgl_lahir` date NOT NULL,
  `knl_nokk` varchar(20) NOT NULL,
  `knl_nik` varchar(20) NOT NULL,
  `knl_pekerjaan` varchar(50) NOT NULL,
  `knl_pendidikan` varchar(10) NOT NULL,
  `knl_ayah` varchar(100) NOT NULL,
  `knl_ibu` varchar(100) NOT NULL,
  `knl_anakke` varchar(20) NOT NULL,
  `knl_alamat` varchar(255) NOT NULL,
  `knl_keterangan` text NOT NULL,
  `knl_kades` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kenal_lahir`
--

INSERT INTO `tb_kenal_lahir` (`id_knl_lhir`, `id_surat`, `knl_anak`, `knl_jkel`, `knl_kwn`, `knl_agama`, `knl_status_kwn`, `knl_tempat_lahir`, `knl_tgl_lahir`, `knl_nokk`, `knl_nik`, `knl_pekerjaan`, `knl_pendidikan`, `knl_ayah`, `knl_ibu`, `knl_anakke`, `knl_alamat`, `knl_keterangan`, `knl_kades`, `created_at`, `updated_at`) VALUES
(1, 11, 'QUENZINO AL FATHAN HAIDAR JAVIER', 'L', 'Indonesia', 'Islam', 'Belum Kawin', 'Jember', '2022-01-01', '3509070610160001', '3509072504190002', '-', '-', 'OKKY WAHYU HIDAYAT', 'NOVI RADRIYAH', '2 (Dua)', 'Dsn. Pucuan RT.002 RW.001 Desa Sidomulyo', 'Persyaratan pembuatan akta kelahiran anak', 'Wasiso, S.I.P', '2022-01-02 20:53:17', '2022-01-02 21:42:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kenal_lahir`
--
ALTER TABLE `tb_kenal_lahir`
  ADD PRIMARY KEY (`id_knl_lhir`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kenal_lahir`
--
ALTER TABLE `tb_kenal_lahir`
  MODIFY `id_knl_lhir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
