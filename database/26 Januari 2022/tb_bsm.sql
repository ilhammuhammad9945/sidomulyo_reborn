-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2022 at 11:42 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_bsm`
--

CREATE TABLE `tb_bsm` (
  `id_bsm` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `bsm_umur` int(11) NOT NULL,
  `bsm_jkel` char(1) NOT NULL,
  `bsm_agama` varchar(20) NOT NULL,
  `bsm_status_kwn` varchar(20) NOT NULL,
  `bsm_pekerjaan` varchar(50) NOT NULL,
  `bsm_penghasilan` double NOT NULL,
  `bsm_alamat` varchar(255) NOT NULL,
  `bsm_nama_ank` varchar(100) NOT NULL,
  `bsm_sekolah` varchar(255) NOT NULL,
  `bsm_kades` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bsm`
--

INSERT INTO `tb_bsm` (`id_bsm`, `id_surat`, `bsm_umur`, `bsm_jkel`, `bsm_agama`, `bsm_status_kwn`, `bsm_pekerjaan`, `bsm_penghasilan`, `bsm_alamat`, `bsm_nama_ank`, `bsm_sekolah`, `bsm_kades`, `created_at`, `updated_at`) VALUES
(1, 9, 43, 'L', 'Islam', 'Kawin', 'Petani', 1000000, 'Dusun  Rowotengu RT 01  RW 06  - Desa Sidomulyo', 'FERDI PURNOMO', 'POLITEKNIK NEGRI JEMBER', 'WASISO, S.IP', '2022-01-01 16:33:24', '2022-01-01 16:40:25'),
(2, 10, 54, 'L', 'Islam', 'Kawin', 'Petani', 1100000, 'Dusun  Rowotengu RT 03  RW 07  - Desa Sidomulyo', 'MUHAMMAD RIZA AZIZI MUSTOFA', 'PONPES DARUSSALAM BLOK AGUNG TEGAL SARI BANYUWANGI', 'WASISO, S.IP', '2022-01-01 16:37:34', '2022-01-02 16:58:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_bsm`
--
ALTER TABLE `tb_bsm`
  ADD PRIMARY KEY (`id_bsm`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bsm`
--
ALTER TABLE `tb_bsm`
  MODIFY `id_bsm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
