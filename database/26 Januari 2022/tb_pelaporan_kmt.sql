-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2022 at 11:42 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelaporan_kmt`
--

CREATE TABLE `tb_pelaporan_kmt` (
  `id_pelaporan_kmt` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `kmt_umur` int(11) NOT NULL,
  `kmt_pekerjaan` varchar(50) NOT NULL,
  `kmt_alamat` varchar(255) NOT NULL,
  `kmt_hubungan` varchar(255) NOT NULL,
  `kmt_nama_mati` varchar(100) NOT NULL,
  `kmt_nik_mati` varchar(20) NOT NULL,
  `kmt_jkel_mati` char(1) NOT NULL,
  `kmt_tgl_lahir_mati` date NOT NULL,
  `kmt_umur_mati` int(11) NOT NULL,
  `kmt_agama_mati` varchar(20) NOT NULL,
  `kmt_alamat_mati` varchar(255) NOT NULL,
  `kmt_tgl_mati` date NOT NULL,
  `kmt_pukul_mati` time NOT NULL,
  `kmt_tempat_mati` varchar(100) NOT NULL,
  `kmt_penyebab` varchar(255) NOT NULL,
  `kmt_bukti_mati` varchar(255) NOT NULL,
  `kmt_keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pelaporan_kmt`
--

INSERT INTO `tb_pelaporan_kmt` (`id_pelaporan_kmt`, `id_surat`, `kmt_umur`, `kmt_pekerjaan`, `kmt_alamat`, `kmt_hubungan`, `kmt_nama_mati`, `kmt_nik_mati`, `kmt_jkel_mati`, `kmt_tgl_lahir_mati`, `kmt_umur_mati`, `kmt_agama_mati`, `kmt_alamat_mati`, `kmt_tgl_mati`, `kmt_pukul_mati`, `kmt_tempat_mati`, `kmt_penyebab`, `kmt_bukti_mati`, `kmt_keterangan`, `created_at`, `updated_at`) VALUES
(1, 15, 30, 'Wiraswasta', 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', 'Saudara Kandung', 'WIDARTO', '350917080960005', 'L', '2022-01-09', 56, 'Hindu', 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', '2022-01-09', '10:18:00', 'Rumah Sakit', 'Sakit', '651a977d01762dc434d49d96032e568b.png', 'Surat dari Rumah Sakit', '2022-01-06 05:18:43', '2022-01-09 16:36:25'),
(2, 16, 30, 'Pengusaha', 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', 'Paman', 'DISCO', '3509072504190006', 'L', '2022-01-10', 20, 'Katolik', 'Dusun Makmur  RT. 002 RW. 024 Desa Sidomulyo', '2022-01-12', '21:44:00', 'Rumah Sakit', 'Kecelakaan', 'b5a54f091a4c69c6e77b73799effbd94.PNG', 'Surat dari Rumah Sakit', '2022-01-09 16:45:11', '2022-01-09 16:45:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_pelaporan_kmt`
--
ALTER TABLE `tb_pelaporan_kmt`
  ADD PRIMARY KEY (`id_pelaporan_kmt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_pelaporan_kmt`
--
ALTER TABLE `tb_pelaporan_kmt`
  MODIFY `id_pelaporan_kmt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
