-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2022 at 11:43 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_belum_nikah`
--

CREATE TABLE `tb_belum_nikah` (
  `id_blm_nikah` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `nkh_jkel` char(1) NOT NULL,
  `nkh_tempat_lahir` varchar(50) NOT NULL,
  `nkh_tgl_lahir` date NOT NULL,
  `nkh_kwn` varchar(50) NOT NULL,
  `nkh_pekerjaan` varchar(50) NOT NULL,
  `nkh_agama` varchar(20) NOT NULL,
  `nkh_pendidikan` varchar(10) NOT NULL,
  `nkh_status_kwn` varchar(20) NOT NULL,
  `nkh_alamat` varchar(255) NOT NULL,
  `nkh_persyaratan` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_belum_nikah`
--

INSERT INTO `tb_belum_nikah` (`id_blm_nikah`, `id_surat`, `nkh_jkel`, `nkh_tempat_lahir`, `nkh_tgl_lahir`, `nkh_kwn`, `nkh_pekerjaan`, `nkh_agama`, `nkh_pendidikan`, `nkh_status_kwn`, `nkh_alamat`, `nkh_persyaratan`, `created_at`, `updated_at`) VALUES
(1, 18, 'L', 'Jember', '2022-01-10', 'Indonesia', 'Wiraswasta', 'Islam', 'S1/D4', 'Belum Kawin', 'Dusun Rowotengu RT.002 RW.007   Desa Sidomulyo Kec.Semboro', 'Belum Nikah', '2022-01-09 20:24:54', '2022-01-09 20:37:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_belum_nikah`
--
ALTER TABLE `tb_belum_nikah`
  ADD PRIMARY KEY (`id_blm_nikah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_belum_nikah`
--
ALTER TABLE `tb_belum_nikah`
  MODIFY `id_blm_nikah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
