-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_organisasi`
--

CREATE TABLE `tb_organisasi` (
  `id_org` int(11) NOT NULL,
  `nama_org` varchar(100) NOT NULL,
  `keterangan_org` varchar(100) NOT NULL,
  `lain_lain` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_organisasi`
--

INSERT INTO `tb_organisasi` (`id_org`, `nama_org`, `keterangan_org`, `lain_lain`) VALUES
(1, 'Pokja 1', 'Ket. PKK', NULL),
(2, 'Pokja 2', 'Ket. PKK', NULL),
(3, 'Pokja 3', 'Ket. PKK', NULL),
(4, 'Pokja 4', 'Ket. PKK', NULL),
(5, 'PKK', 'Ket. PKK', NULL),
(8, 'Posyandu A', 'Asal Posyandu', 'Dusun X RT 002 / RW 002'),
(9, 'Posyandu B', 'Asal Posyandu', 'Dusun Z RT 003 / RW 003'),
(10, 'Seksi Pendidikan Dan Pelatihan', 'Ket. Karang Taruna', NULL),
(11, 'Seksi Pengembangan Usaha Kegiatan Sosial', 'Ket. Karang Taruna', NULL),
(12, 'Seksi Usaha Ekonomi Produktif', 'Ket. Karang Taruna', NULL),
(13, 'Seksi Pengembangan Kegiatan Kerohanian Dan Pembinaan Mental', 'Ket. Karang Taruna', NULL),
(14, 'Seksi Pengembangan Kegiatan Olahraga Dan Seni Budaya', 'Ket. Karang Taruna', NULL),
(15, 'Seksi Lingkungan Hidup dan Pariwisata', 'Ket. Karang Taruna', NULL),
(16, 'Seksi Biro Organisasi, Pengembangan Hubungan Kerja', 'Ket. Karang Taruna', NULL),
(17, 'Seksi Keamanan dan Pendamping Kegiatan', 'Ket. Karang Taruna', NULL),
(18, 'Seksi Hubungan Masyarakat', 'Ket. Karang Taruna', NULL),
(19, 'Seksi Logistik / Perlengkapan', 'Ket. Karang Taruna', NULL),
(20, 'Kader Kesehatan Ibu dan Anak', 'Ket. Posyandu', NULL),
(21, 'Kader Keluarga Berencana', 'Ket. Posyandu', NULL),
(22, 'Kader Imunisasi', 'Ket. Posyandu', NULL),
(23, 'Kader Gizi', 'Ket. Posyandu', NULL),
(24, 'RW 001', 'Asal RW', 'Dusun A'),
(25, 'RW 002', 'Asal RW', 'Dusun B'),
(26, 'RW 003', 'Asal RW', 'Dusun C'),
(27, 'RT 001', 'Asal RT', NULL),
(28, 'RT 002', 'Asal RT', NULL),
(29, 'RT 003', 'Asal RT', NULL),
(30, 'RT 004', 'Asal RT', NULL),
(31, 'Karang Werda A', 'Asal Karang Werda', NULL),
(32, 'Karang Werda B', 'Asal Karang Werda', NULL),
(33, 'Muslimat A', 'Asal Muslimat', 'Dusun A RT 001 / RW 001'),
(34, 'Muslimat B', 'Asal Muslimat', 'Dusun A RT 001 / RW 002'),
(35, 'Aisyiyah A', 'Asal Aisyiyah', 'Dusun A RT 001 / RW 001'),
(36, 'Aisyiyah B', 'Asal Aisyiyah', 'Dusun A RT 001 / RW 002'),
(37, 'Sanggar Seni A', 'Asal SSB', 'Dusun A RT 001 / RW 002'),
(38, 'Sanggar Seni B', 'Asal SSB', 'Dusun A RT 001 / RW 003'),
(39, 'LSM A', 'Asal LSM', 'Dusun A RT 001 / RW 003'),
(40, 'LSM B', 'Asal LSM', 'Dusun A RT 001 / RW 004'),
(41, 'Karang Taruna', 'Ket. Karang Taruna', NULL),
(42, 'Posyandu', 'Ket. Posyandu', NULL),
(43, 'PPKBD', 'Ket. PPKBD', NULL),
(44, 'Sub A', 'Ket. PPKBD', NULL),
(45, 'Sub B', 'Ket. PPKBD', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_organisasi`
--
ALTER TABLE `tb_organisasi`
  ADD PRIMARY KEY (`id_org`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_organisasi`
--
ALTER TABLE `tb_organisasi`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
