-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2021 at 09:36 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_sk_usaha`
--

CREATE TABLE `tb_sk_usaha` (
  `id_sk_usaha` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_pengajuan_sk` varchar(50) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `jkel_pemohon` char(1) NOT NULL,
  `kewarganegaraan` varchar(20) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `status_perkawinan` varchar(20) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `pendidikan` varchar(10) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `kepala_desa` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sk_usaha`
--

INSERT INTO `tb_sk_usaha` (`id_sk_usaha`, `id_surat`, `id_pengajuan_sk`, `no_surat`, `jkel_pemohon`, `kewarganegaraan`, `agama`, `status_perkawinan`, `tempat_lahir`, `tgl_lahir`, `pendidikan`, `pekerjaan`, `alamat`, `keterangan`, `kepala_desa`, `created_at`, `updated_at`) VALUES
(1, 2, '1912210001', '001', 'L', 'Indonesias', 'Protestan', 'Belum Kawin', 'Jembersz', '1991-12-12', 'D1', 'Wiraswastas', 'Dsn. Rowotengu RT.01 RW.07 Desa Sidomulyos', 'Petani Jeruk', 'WASISO, S.IP', '2021-12-19 09:29:53', '2021-12-20 12:47:58'),
(8, 8, '1912210002', '003', 'P', 'Indonesia', 'Hindu', 'Cerai Hidup', 'sdfsdf', '2021-12-02', 'SMA/SMK', 'dfsdf', 'hhfh', 'srtrt', 'WASISO, S.IP', '2021-12-19 22:28:45', '2021-12-19 22:28:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_sk_usaha`
--
ALTER TABLE `tb_sk_usaha`
  ADD PRIMARY KEY (`id_sk_usaha`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_sk_usaha`
--
ALTER TABLE `tb_sk_usaha`
  MODIFY `id_sk_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
