-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2021 at 09:36 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_sk_domisili`
--

CREATE TABLE `tb_sk_domisili` (
  `id_sk_domisili` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_pengajuan_skdomisili` varchar(50) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `jkel_pemohon` char(1) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kewarganegaraan` varchar(20) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `pendidikan` varchar(10) NOT NULL,
  `status_perkawinan` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `kepala_desa` varchar(50) NOT NULL,
  `nama_camat` varchar(100) NOT NULL,
  `nip_camat` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sk_domisili`
--

INSERT INTO `tb_sk_domisili` (`id_sk_domisili`, `id_surat`, `id_pengajuan_skdomisili`, `no_surat`, `jkel_pemohon`, `tempat_lahir`, `tgl_lahir`, `kewarganegaraan`, `pekerjaan`, `agama`, `pendidikan`, `status_perkawinan`, `alamat`, `keterangan`, `kepala_desa`, `nama_camat`, `nip_camat`, `created_at`, `updated_at`) VALUES
(9, 9, '2012210001', '004', 'L', 'Jember', '2021-11-28', 'Indonesia', 'Wiraswasta', 'Islam', 'SMA/SMK', 'Kawin', 'Dusun Pucuan RT. 005 RW. 002 Desa Sidomulyo', 'Yang namanya tersebut di atas benar-benar penduduk Desa Sidomulyo Kecamatan Semboro Kabupaten Jember Provinsi Jawa Timur', 'WASISO, S.IP', 'MUH. ARIS DERMAWAN, S,Sos', '19640102 199305 1 001', '2021-12-20 05:08:11', '2021-12-20 05:08:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_sk_domisili`
--
ALTER TABLE `tb_sk_domisili`
  ADD PRIMARY KEY (`id_sk_domisili`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_sk_domisili`
--
ALTER TABLE `tb_sk_domisili`
  MODIFY `id_sk_domisili` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
