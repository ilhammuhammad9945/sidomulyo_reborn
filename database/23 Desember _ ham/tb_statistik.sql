-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2021 at 12:05 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_statistik`
--

CREATE TABLE `tb_statistik` (
  `id_statistik` int(11) NOT NULL,
  `judul_statistik` varchar(100) NOT NULL,
  `parameter_statistik` varchar(100) NOT NULL,
  `nilai_statistik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_statistik`
--

INSERT INTO `tb_statistik` (`id_statistik`, `judul_statistik`, `parameter_statistik`, `nilai_statistik`) VALUES
(1, 'All', 'Penduduk', 6712),
(2, 'Gender', 'Laki-laki', 3250),
(3, 'Gender', 'Perempuan', 3462),
(4, 'Pekerjaan', 'Petani', 2225),
(5, 'Pekerjaan', 'Buruh Tani', 2158),
(6, 'Pekerjaan', 'PNS', 37),
(7, 'Pekerjaan', 'TNI/POLRI', 30),
(8, 'Pekerjaan', 'Wiraswasta', 69),
(9, 'Pekerjaan', 'Buruh Pabrik', 24),
(10, 'Usia', '< 1 tahun', 162),
(11, 'Usia', '1-4 tahun', 429),
(12, 'Usia', '5-14 tahun', 2031),
(13, 'Usia', '15-39 tahun', 2944),
(14, 'Usia', '40-64 tahun', 951),
(15, 'Usia', '65 tahun keatas', 205);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_statistik`
--
ALTER TABLE `tb_statistik`
  ADD PRIMARY KEY (`id_statistik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_statistik`
--
ALTER TABLE `tb_statistik`
  MODIFY `id_statistik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
