-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `id_jbt` int(11) NOT NULL,
  `nama_jbt` varchar(100) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id_jbt`, `nama_jbt`, `keterangan`) VALUES
(1, 'Kepala Desa', 'Aparatur Desa'),
(2, 'Ketua', 'Aparatur BPD'),
(3, 'Kepala Urusan Keuangan', 'Aparatur Desa'),
(4, 'Kepala Urusan Perencanaan', 'Aparatur Desa'),
(5, 'Kepala Urusan Tata Usaha dan Umum', 'Aparatur Desa'),
(6, 'Staf Bendahara', 'Aparatur Desa'),
(7, 'Staf Kebersihan', 'Aparatur Desa'),
(8, 'Kepala Seksi Pemerintahan', 'Aparatur Desa'),
(9, 'Kepala Seksi Kesejahteraan', 'Aparatur Desa'),
(10, 'Kepala Seksi Pelayanan', 'Aparatur Desa'),
(11, 'Staff Trantib', 'Aparatur Desa'),
(12, 'Kepala Dusun', 'Aparatur Desa'),
(13, 'Wakil Ketua', 'Aparatur BPD'),
(14, 'Sekretaris', 'Aparatur BPD'),
(15, 'Bidang Pembangunan dan Pemberdayaan Masyarakat', 'Aparatur BPD'),
(16, 'Bidang Pemerintahan dan Pembinaan Masyarakat', 'Aparatur BPD'),
(17, 'Sekretaris', 'Aparatur Desa'),
(18, 'Pembina', 'PKK'),
(19, 'Ketua', 'PKK'),
(20, 'Sekretaris', 'PKK'),
(21, 'Bendahara', 'PKK'),
(22, 'Pos KB', 'PKK'),
(23, 'Dasawisma', 'PKK'),
(24, 'Ketua', 'LPMD'),
(25, 'Sekretaris', 'LPMD'),
(26, 'Bendahara', 'LPMD'),
(27, 'Seksi Pembangunan', 'LPMD'),
(28, 'Seksi Pendidikan dan Kerohanian', 'LPMD'),
(29, 'Seksi Kesehatan', 'LPMD'),
(30, 'Seksi Kepemudaan', 'LPMD'),
(31, 'Seksi Pemberdayaan Perempuan', 'LPMD'),
(32, 'Pembina Umum', 'Karang Taruna'),
(33, 'Pembina Fungsional', 'Karang Taruna'),
(34, 'Pembina Teknis', 'Karang Taruna'),
(35, 'Penasehat', 'Karang Taruna'),
(36, 'Ketua', 'Karang Taruna'),
(37, 'Sekretaris', 'Karang Taruna'),
(38, 'Bendahara', 'Karang Taruna'),
(39, 'Koordinator', 'Karang Taruna'),
(40, 'Anggota', 'Karang Taruna'),
(41, 'Ketua', 'Posyandu'),
(42, 'Wakil Ketua', 'Posyandu'),
(43, 'Sekretaris', 'Posyandu'),
(44, 'Bendahara', 'Posyandu'),
(52, 'Ketua', 'Karang Werda'),
(53, 'Wakil Ketua', 'Karang Werda'),
(54, 'Sekretaris', 'Karang Werda'),
(55, 'Bendahara', 'Karang Werda'),
(56, 'Anggota', 'Karang Werda'),
(57, 'Ketua', 'Rumah Desa Sehat'),
(58, 'Wakil Ketua', 'Rumah Desa Sehat'),
(59, 'Sekretaris', 'Rumah Desa Sehat'),
(60, 'Bendahara', 'Rumah Desa Sehat'),
(61, 'Anggota', 'Rumah Desa Sehat'),
(62, 'Ketua', 'Muslimat'),
(63, 'Wakil Ketua', 'Muslimat'),
(64, 'Sekretaris', 'Muslimat'),
(65, 'Bendahara', 'Muslimat'),
(66, 'Anggota', 'Muslimat'),
(67, 'Ketua', 'Aisyiyah'),
(68, 'Wakil Ketua', 'Aisyiyah'),
(69, 'Sekretaris', 'Aisyiyah'),
(70, 'Bendahara', 'Aisyiyah'),
(71, 'Anggota', 'Aisyiyah'),
(72, 'Ketua', 'SSB'),
(73, 'Wakil Ketua', 'SSB'),
(74, 'Sekretaris', 'SSB'),
(75, 'Bendahara', 'SSB'),
(76, 'Anggota', 'SSB'),
(77, 'Ketua', 'LSM'),
(78, 'Wakil Ketua', 'LSM'),
(79, 'Sekretaris', 'LSM'),
(80, 'Bendahara', 'LSM'),
(81, 'Anggota', 'LSM'),
(82, 'Ketua', 'KPMD'),
(83, 'Wakil Ketua', 'KPMD'),
(84, 'Sekretaris', 'KPMD'),
(85, 'Bendahara', 'KPMD'),
(86, 'Anggota', 'KPMD'),
(87, 'Ketua', 'KPM'),
(88, 'Wakil Ketua', 'KPM'),
(89, 'Sekretaris', 'KPM'),
(90, 'Bendahara', 'KPM'),
(91, 'Anggota', 'KPM'),
(93, 'Ketua RT', 'RT'),
(94, 'Ketua RW', 'RW'),
(95, 'Anggota', 'Posyandu'),
(96, 'Ketua', 'Kader Teknik'),
(97, 'Wakil Ketua', 'Kader Teknik'),
(98, 'Sekretaris', 'Kader Teknik'),
(99, 'Bendahara', 'Kader Teknik'),
(100, 'Anggota', 'Kader Teknik'),
(101, 'Ketua', 'PPKBD'),
(102, 'Wakil Ketua', 'PPKBD'),
(103, 'Sekretaris', 'PPKBD'),
(104, 'Bendahara', 'PPKBD'),
(105, 'Koordinator', 'PPKBD'),
(106, 'Anggota', 'PPKBD'),
(107, 'Ketua', 'TB'),
(108, 'Wakil Ketua', 'TB'),
(109, 'Sekretaris', 'TB'),
(110, 'Bendahara', 'TB'),
(111, 'Anggota', 'TB'),
(112, 'Kepala Puskesmas', 'Ponkesdes'),
(113, 'Penanggung Jawab Puskesmas Pembantu', 'Ponkesdes'),
(114, 'Koordinator Ponkesdes', 'Ponkesdes'),
(115, 'Pelaksana', 'Ponkesdes'),
(117, 'Pengawas', 'Bumdes'),
(118, 'Komisaris Kepala Desa', 'Bumdes'),
(119, 'Direktur', 'Bumdes'),
(120, 'Sekretaris', 'Bumdes'),
(121, 'Bendahara', 'Bumdes'),
(122, 'Manajer Unit Usaha', 'Bumdes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`id_jbt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `id_jbt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
