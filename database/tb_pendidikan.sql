-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_pendidikan`
--

CREATE TABLE `tb_pendidikan` (
  `id_pend` int(11) NOT NULL,
  `kat_jenjang` int(11) NOT NULL,
  `nama_sekolah` varchar(100) NOT NULL,
  `visi_misi` text NOT NULL,
  `alamat_sekolah` varchar(500) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `foto_utama` varchar(100) NOT NULL,
  `struktur_organisasi` varchar(100) NOT NULL,
  `jumlah_murid` int(11) NOT NULL,
  `fasilitas` text NOT NULL,
  `prestasi` text NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pendidikan`
--

INSERT INTO `tb_pendidikan` (`id_pend`, `kat_jenjang`, `nama_sekolah`, `visi_misi`, `alamat_sekolah`, `telp`, `foto_utama`, `struktur_organisasi`, `jumlah_murid`, `fasilitas`, `prestasi`, `date_created`, `date_updated`) VALUES
(1, 4, 'SMP Asahan', '<p><strong>Visi :</strong></p>\r\n\r\n<p>Berakhlak mulia,&nbsp;berprestasi tinggi, tangguh dalam kompetisi, berwawasan lingkungan dengan berlandaskan budaya nasional dan kearifan lokal</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Misi :</strong></p>\r\n\r\n<ol>\r\n	<li>Memantapkan penghayatan dan pengamalan ajaran agama.</li>\r\n	<li>Melaksanakan kurikulum berwawasan lingkungan dengan berlandaskan budaya nasional dan kearifan lokal</li>\r\n</ol>\r\n', '<p>Jl. Semarang No.5, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145</p>\r\n', '081009009009', '2244ecb99d5afce8ecccbed6dc702214.jpg', '72c0c900a7537ade1ed7fca6d325639f.jpg', 400, '<ol>\r\n	<li>Gedung sekolah</li>\r\n	<li>Ruang kelas</li>\r\n	<li>Ruang guru</li>\r\n	<li>Ruang pimpinan</li>\r\n	<li>Tempat beribadah</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Juara Tartil</li>\r\n	<li>Cerdas Cermat Kabupaten Jember</li>\r\n</ol>\r\n', '2021-07-04 10:35:44', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_pendidikan`
--
ALTER TABLE `tb_pendidikan`
  ADD PRIMARY KEY (`id_pend`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_pendidikan`
--
ALTER TABLE `tb_pendidikan`
  MODIFY `id_pend` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
