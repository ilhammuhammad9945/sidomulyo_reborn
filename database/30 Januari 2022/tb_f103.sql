-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2022 at 03:42 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_f103`
--

CREATE TABLE `tb_f103` (
  `id_surat` int(11) NOT NULL,
  `wil_nama_prov` varchar(100) NOT NULL,
  `wil_nama_kec` varchar(100) NOT NULL,
  `wil_nama_kab` varchar(100) NOT NULL,
  `wil_nama_desa` varchar(100) NOT NULL,
  `kel_nama_kep_kel` varchar(100) NOT NULL,
  `kel_no_kk` varchar(100) NOT NULL,
  `kel_alamat_kel` varchar(500) NOT NULL,
  `kel_desa` varchar(100) NOT NULL,
  `kel_kec` varchar(100) NOT NULL,
  `kel_kab` varchar(100) NOT NULL,
  `kel_prov` varchar(100) NOT NULL,
  `kel_kode_pos` varchar(100) NOT NULL,
  `kel_no_telp` varchar(100) NOT NULL,
  `ind_nama` varchar(100) NOT NULL,
  `ind_nik` varchar(100) NOT NULL,
  `ind_alamat` varchar(500) NOT NULL,
  `ind_desa` varchar(100) NOT NULL,
  `ind_kec` varchar(100) NOT NULL,
  `ind_kab` varchar(100) NOT NULL,
  `ind_prov` varchar(100) NOT NULL,
  `ind_kode_pos` varchar(100) NOT NULL,
  `ind_no_telp` varchar(100) NOT NULL,
  `ind_no_paspor` varchar(100) DEFAULT NULL,
  `ind_tgl_exp_paspor` date DEFAULT NULL,
  `ind_gender` varchar(100) NOT NULL,
  `ind_tempat_lahir` varchar(100) NOT NULL,
  `ind_tanggal_lahir` date NOT NULL,
  `ind_umur` int(11) NOT NULL,
  `ind_akte_lahir` varchar(100) NOT NULL,
  `ind_no_akte_lahir` varchar(100) DEFAULT NULL,
  `ind_gol_darah` varchar(10) NOT NULL,
  `ind_agama` varchar(100) NOT NULL,
  `ind_status_kawin` varchar(100) NOT NULL,
  `ind_akta_kawin` varchar(100) NOT NULL,
  `ind_no_akta_kawin` varchar(100) DEFAULT NULL,
  `ind_tgl_kawin` date DEFAULT NULL,
  `ind_akta_cerai` varchar(100) NOT NULL,
  `ind_no_akta_cerai` varchar(100) DEFAULT NULL,
  `ind_tgl_cerai` date DEFAULT NULL,
  `ind_shdk` varchar(100) NOT NULL,
  `ind_kelainan_fisik` varchar(100) NOT NULL,
  `ind_cacat` varchar(100) NOT NULL,
  `ind_pendidikan` varchar(100) NOT NULL,
  `ind_pekerjaan` varchar(100) NOT NULL,
  `ortu_nik_ibu` varchar(100) NOT NULL,
  `ortu_nama_ibu` varchar(100) NOT NULL,
  `ortu_nik_ayah` varchar(100) NOT NULL,
  `ortu_nama_ayah` varchar(100) NOT NULL,
  `adm_ketua_rt` varchar(100) NOT NULL,
  `adm_ketua_rw` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_f103`
--

INSERT INTO `tb_f103` (`id_surat`, `wil_nama_prov`, `wil_nama_kec`, `wil_nama_kab`, `wil_nama_desa`, `kel_nama_kep_kel`, `kel_no_kk`, `kel_alamat_kel`, `kel_desa`, `kel_kec`, `kel_kab`, `kel_prov`, `kel_kode_pos`, `kel_no_telp`, `ind_nama`, `ind_nik`, `ind_alamat`, `ind_desa`, `ind_kec`, `ind_kab`, `ind_prov`, `ind_kode_pos`, `ind_no_telp`, `ind_no_paspor`, `ind_tgl_exp_paspor`, `ind_gender`, `ind_tempat_lahir`, `ind_tanggal_lahir`, `ind_umur`, `ind_akte_lahir`, `ind_no_akte_lahir`, `ind_gol_darah`, `ind_agama`, `ind_status_kawin`, `ind_akta_kawin`, `ind_no_akta_kawin`, `ind_tgl_kawin`, `ind_akta_cerai`, `ind_no_akta_cerai`, `ind_tgl_cerai`, `ind_shdk`, `ind_kelainan_fisik`, `ind_cacat`, `ind_pendidikan`, `ind_pekerjaan`, `ortu_nik_ibu`, `ortu_nama_ibu`, `ortu_nik_ayah`, `ortu_nama_ayah`, `adm_ketua_rt`, `adm_ketua_rw`) VALUES
(22, 'asda', 'asda', 'asda', 'asda', 'asda', '1234', 'asda', 'asda', 'asda', 'asda', 'asd', '123', '7878', 'asda', '12', 'asda', 'asda', 'asd', 'aswd', 'asd', '12', '9898', '12', '2022-01-22', 'L', 'asda', '2022-01-22', 12, 'Tidak Ada', '', 'A', 'Islam', 'Belum Kawin', 'Ada', 'dfs', '2022-01-22', 'Ada', 'dsfasd', '2022-01-22', 'Kepala Keluarga', 'Tidak Ada', 'Cacat Fisik', 'Tidak Sekolah', 'Belum / Tidak Bekerja', '231', 'asda', '321', 'asda', 'asda', 'asda');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
