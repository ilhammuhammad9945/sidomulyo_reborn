-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2022 at 01:59 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_bpwni`
--

CREATE TABLE `tb_bpwni` (
  `id_bpwni` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_pengikut` int(11) NOT NULL,
  `bpwni_no_kk` varchar(100) NOT NULL,
  `bpwni_tempat_lahir` varchar(100) NOT NULL,
  `bpwni_tanggal_lahir` date NOT NULL,
  `bpwni_gender` varchar(100) NOT NULL,
  `bpwni_gol_darah` varchar(2) NOT NULL,
  `bpwni_agama` varchar(100) NOT NULL,
  `bpwni_pendidikan_terakhir` varchar(100) NOT NULL,
  `bpwni_pekerjaan` varchar(100) NOT NULL,
  `bpwni_cacat` varchar(100) NOT NULL,
  `bpwni_status_kawin` varchar(100) NOT NULL,
  `bpwni_status_hub_keluarga` varchar(100) NOT NULL,
  `bpwni_nik_ibu` varchar(100) NOT NULL,
  `bpwni_nama_ibu` varchar(100) NOT NULL,
  `bpwni_nik_ayah` varchar(100) NOT NULL,
  `bpwni_nama_ayah` varchar(100) NOT NULL,
  `bpwni_alamat_lama` varchar(500) NOT NULL,
  `bpwni_alamat_sekarang` varchar(500) NOT NULL,
  `bpwni_desa_as` varchar(100) NOT NULL,
  `bpwni_kec_as` varchar(100) NOT NULL,
  `bpwni_kab_as` varchar(100) NOT NULL,
  `bpwni_provinsi_as` varchar(100) NOT NULL,
  `bpwni_no_paspor` varchar(100) NOT NULL,
  `bpwni_tgl_exp_paspor` date NOT NULL,
  `bpwni_no_akta` varchar(100) NOT NULL,
  `bpwni_no_perkawinan` varchar(100) NOT NULL,
  `bpwni_tgl_kawin` date NOT NULL,
  `bpwni_no_perceraian` varchar(100) DEFAULT NULL,
  `bpwni_tgl_cerai` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bpwni`
--

INSERT INTO `tb_bpwni` (`id_bpwni`, `id_surat`, `id_pengikut`, `bpwni_no_kk`, `bpwni_tempat_lahir`, `bpwni_tanggal_lahir`, `bpwni_gender`, `bpwni_gol_darah`, `bpwni_agama`, `bpwni_pendidikan_terakhir`, `bpwni_pekerjaan`, `bpwni_cacat`, `bpwni_status_kawin`, `bpwni_status_hub_keluarga`, `bpwni_nik_ibu`, `bpwni_nama_ibu`, `bpwni_nik_ayah`, `bpwni_nama_ayah`, `bpwni_alamat_lama`, `bpwni_alamat_sekarang`, `bpwni_desa_as`, `bpwni_kec_as`, `bpwni_kab_as`, `bpwni_provinsi_as`, `bpwni_no_paspor`, `bpwni_tgl_exp_paspor`, `bpwni_no_akta`, `bpwni_no_perkawinan`, `bpwni_tgl_kawin`, `bpwni_no_perceraian`, `bpwni_tgl_cerai`) VALUES
(2, 1, 8, 'sdfs', 'sdfs', '2022-01-26', 'L', 'sd', 'Islam', 'SD', 'sdfs', 'sdfs', 'Kawin', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdf', 'sdfs', '2022-01-26', 'sdfs', 'sdfs', '2022-01-26', 'sdfs', '2022-01-26'),
(3, 1, 9, 'sdfs', 'sdfs', '2022-01-26', 'L', 'sd', 'Islam', 'SMP', 'sdfsd', 'sdfsd', 'Belum Kawin', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', 'sdfs', '2022-01-26', 'sdfs', 'sdfs', '2022-01-26', 'sdfs', '2022-01-26'),
(4, 1, 10, 'sdfs', 'sdfsdf', '2022-01-26', 'L', 'sd', 'Islam', 'SD', 'sdffs', 'sdfs', 'Belum Kawin', 'sdfs', 'sdfs', 'sdf', 'sdf', 'sdf', 'sdfs', 'sdfs', 'sdfs', 'sdf', 'sdf', 'sdf', 'sdfs', '2022-01-26', 'sdfs', 'sdfs', '2022-01-26', 'sdfs', '2022-01-26'),
(5, 1, 11, 'sdfs', 'sdfsd', '2022-01-26', 'L', 'fs', 'Islam', 'SMP', 'sdfs', 'sdfs', 'Belum Kawin', 'sdfs', 'sdfs', 'sdf', 'sdf', 'sdf', 'sdfs', 'sdf', 'sdfs', 'sdfs', 'fsd', 's', 'sdfs', '2022-01-26', 'sdfs', 'sdf', '2022-01-26', 'sdfs', '2022-01-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_bpwni`
--
ALTER TABLE `tb_bpwni`
  ADD PRIMARY KEY (`id_bpwni`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bpwni`
--
ALTER TABLE `tb_bpwni`
  MODIFY `id_bpwni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
