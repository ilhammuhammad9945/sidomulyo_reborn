-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2022 at 01:59 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengikut`
--

CREATE TABLE `tb_pengikut` (
  `id_pengikut` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `pengikut_nama` varchar(100) NOT NULL,
  `pengikut_gender` varchar(100) NOT NULL,
  `pengikut_umur` int(11) NOT NULL,
  `pengikut_status_kawin` varchar(100) NOT NULL,
  `pengikut_pendidikan` varchar(100) NOT NULL,
  `pengikut_nik` varchar(100) NOT NULL,
  `pengikut_keterangan` varchar(100) NOT NULL,
  `pengikut_shdk` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengikut`
--

INSERT INTO `tb_pengikut` (`id_pengikut`, `id_surat`, `pengikut_nama`, `pengikut_gender`, `pengikut_umur`, `pengikut_status_kawin`, `pengikut_pendidikan`, `pengikut_nik`, `pengikut_keterangan`, `pengikut_shdk`) VALUES
(1, 11, 'asd', 'L', 12, 'Belum Kawin', 'SMA', '12', 'asd', NULL),
(2, 11, 'asdas', 'L', 21, 'Belum Kawin', 'Akademik', '1231', 'asda', NULL),
(3, 11, 'zdczsdf', 'P', 32, 'Kawin', 'SMA', '12312', 'xdcfs', NULL),
(4, 1, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd', NULL),
(5, 1, 'asd', 'L', 22, 'Belum Kawin', 'SMA', '13', 'asd', NULL),
(6, 1, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd', NULL),
(7, 1, 'asd', 'L', 10, 'Belum Kawin', 'SMP', '13', 'asd', NULL),
(8, 15, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd', NULL),
(9, 15, 'asd', 'L', 10, 'Belum Kawin', 'SMP', '13', 'asd', NULL),
(10, 15, 'asd', 'L', 22, 'Belum Kawin', 'SD', '12', 'asd', NULL),
(11, 15, 'asd', 'L', 10, 'Belum Kawin', 'SMP', '13', 'asd', NULL),
(12, 17, 'asda', '', 0, '', '', '1231', '', 'Istri'),
(13, 17, 'asd', '', 0, '', '', 'dasd', '', 'Suami');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_pengikut`
--
ALTER TABLE `tb_pengikut`
  ADD PRIMARY KEY (`id_pengikut`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_pengikut`
--
ALTER TABLE `tb_pengikut`
  MODIFY `id_pengikut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
