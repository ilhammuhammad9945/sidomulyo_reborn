-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2021 at 03:02 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_log_pengunjung`
--

CREATE TABLE `tb_log_pengunjung` (
  `id_log_pengunjung` int(11) NOT NULL,
  `tanggal_kunjungan` timestamp NOT NULL DEFAULT current_timestamp(),
  `ip_pengunjung` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_log_pengunjung`
--

INSERT INTO `tb_log_pengunjung` (`id_log_pengunjung`, `tanggal_kunjungan`, `ip_pengunjung`) VALUES
(1, '2021-11-11 22:18:26', '::1'),
(2, '2021-11-10 17:00:00', '192.168.1.1'),
(3, '2021-10-12 17:00:00', '192.168.1.12'),
(4, '2021-11-14 11:14:27', '::1'),
(5, '2021-11-15 13:13:16', '::1'),
(6, '2021-11-16 12:54:38', '::1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_log_pengunjung`
--
ALTER TABLE `tb_log_pengunjung`
  ADD PRIMARY KEY (`id_log_pengunjung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_log_pengunjung`
--
ALTER TABLE `tb_log_pengunjung`
  MODIFY `id_log_pengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
