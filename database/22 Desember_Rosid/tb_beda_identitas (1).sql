-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2021 at 11:16 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_beda_identitas`
--

CREATE TABLE `tb_beda_identitas` (
  `id_beda_identitas` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_pengajuan_identitas` varchar(50) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `jkel_lama` char(1) NOT NULL,
  `tempat_lahir_lama` varchar(50) NOT NULL,
  `tgl_lahir_lama` date NOT NULL,
  `agama_lama` varchar(20) NOT NULL,
  `no_kk_lama` varchar(20) NOT NULL,
  `alamat_lama` varchar(255) NOT NULL,
  `nama_baru` varchar(100) NOT NULL,
  `nik_baru` varchar(20) NOT NULL,
  `jkel_baru` char(1) NOT NULL,
  `tempat_lahir_baru` varchar(50) NOT NULL,
  `tgl_lahir_baru` date NOT NULL,
  `agama_baru` varchar(20) NOT NULL,
  `no_kk_baru` varchar(20) NOT NULL,
  `alamat_baru` varchar(255) NOT NULL,
  `datakk_dan_data` varchar(100) NOT NULL,
  `utk_syarat` varchar(255) NOT NULL,
  `kepala_desa` varchar(50) NOT NULL,
  `nama_camat` varchar(100) NOT NULL,
  `nip_camat` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_beda_identitas`
--

INSERT INTO `tb_beda_identitas` (`id_beda_identitas`, `id_surat`, `id_pengajuan_identitas`, `no_surat`, `jkel_lama`, `tempat_lahir_lama`, `tgl_lahir_lama`, `agama_lama`, `no_kk_lama`, `alamat_lama`, `nama_baru`, `nik_baru`, `jkel_baru`, `tempat_lahir_baru`, `tgl_lahir_baru`, `agama_baru`, `no_kk_baru`, `alamat_baru`, `datakk_dan_data`, `utk_syarat`, `kepala_desa`, `nama_camat`, `nip_camat`, `created_at`, `updated_at`) VALUES
(11, 16, 'BI-2112210001', '001', 'L', 'Jember', '2021-11-28', 'Islam', '1602181403880002', 'RT 04 RW 12 Dusun Rowotengu - Desa Sidomulyo', 'EKO WAHYUDI S.', '3509072504190001', 'L', 'Jember', '2021-11-28', 'Islam', '1602181403880002', 'RT 04 RW 12 Dusun Rowotengu - Desa Sidomulyo', 'Buku Nikah', 'Bantuan UMKM', 'WASISO, S.IP', 'MUH. ARIS DERMAWAN, S,Sos', '19640102 199305 1 001', '2021-12-21 09:03:01', '2021-12-21 19:55:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_beda_identitas`
--
ALTER TABLE `tb_beda_identitas`
  ADD PRIMARY KEY (`id_beda_identitas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_beda_identitas`
--
ALTER TABLE `tb_beda_identitas`
  MODIFY `id_beda_identitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
