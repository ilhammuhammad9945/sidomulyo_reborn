-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_lemmas`
--

CREATE TABLE `tb_lemmas` (
  `id_lm` varchar(50) NOT NULL,
  `nama_lm` varchar(100) NOT NULL,
  `jkel_lm` varchar(10) NOT NULL,
  `no_telp` char(12) DEFAULT NULL,
  `alamat_lm` varchar(255) NOT NULL,
  `jabatan_lm` int(11) NOT NULL,
  `keterangan_jabatan` varchar(255) DEFAULT NULL,
  `asal_organisasi` varchar(255) DEFAULT NULL,
  `lain_lain` varchar(255) DEFAULT NULL,
  `foto_lm` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_lemmas`
--

INSERT INTO `tb_lemmas` (`id_lm`, `nama_lm`, `jkel_lm`, `no_telp`, `alamat_lm`, `jabatan_lm`, `keterangan_jabatan`, `asal_organisasi`, `lain_lain`, `foto_lm`) VALUES
('60b8018dd6a6d', 'Japar Sidik,ST', 'Laki-laki', '081234567890', 'Jl. Jalan', 18, 'PKK', NULL, NULL, '60b8018dd6a6d.jpg'),
('60b801a24a55a', 'Elanda Nurhafizh R,S.Pd', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'PKK', NULL, NULL, '60b801a24a55a.jpg'),
('60b801c101984', 'Herni Kusmiati', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'PKK', NULL, NULL, '60b801c101984.jpg'),
('60b801d51e7f5', 'Imas Sopiah', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'PKK', NULL, NULL, '60b801d51e7f5.jpg'),
('60b801eb51bce', 'Nunung Elia', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 1', NULL, NULL, '60b801eb51bce.jpg'),
('60b80200c7023', 'Siti Mariam', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 1', NULL, NULL, '60b80200c7023.jpg'),
('60b80218cd678', 'Suryati', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 1', NULL, NULL, '60b80218cd678.jpg'),
('60baa37689e5c', 'Sri Mulyati', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 2', NULL, NULL, '60baa37689e5c.jpg'),
('60baa39073b14', 'Evi Riani', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 2', NULL, NULL, '60baa39073b14.jpg'),
('60baa3ab2149a', 'Siti Maesaroh', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 2', NULL, NULL, '60baa3ab2149a.jpg'),
('60baa3bdca9d7', 'Entin Rostini', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 3', NULL, NULL, '60baa3bdca9d7.jpg'),
('60baa3d0a1a8c', 'Imas', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 3', NULL, NULL, '60baa3d0a1a8c.jpg'),
('60baa3e37d93b', 'Titin', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 3', NULL, NULL, '60baa3e37d93b.jpg'),
('60baa3fc210d1', 'Neni', 'Perempuan', '081234567890', 'Jl. Jalan', 19, 'Pokja 4', NULL, NULL, '60baa3fc210d1.jpg'),
('60baa41c574ce', 'Mila Purwantika', 'Perempuan', '081234567890', 'Jl. Jalan', 20, 'Pokja 4', NULL, NULL, '60baa41c574ce.jpg'),
('60baa42d32862', 'Rani', 'Perempuan', '081234567890', 'Jl. Jalan', 21, 'Pokja 4', NULL, NULL, '60baa42d32862.jpg'),
('60baa44327671', 'Enah', 'Perempuan', '081234567890', 'Jl. Jalan', 22, 'PKK', NULL, NULL, '60baa44327671.jpg'),
('60baa45290e65', 'Leni', 'Perempuan', '081234567890', 'Jl. Raung Dusun Klanceng, RT 003 / RW 003, Kelurahan Ajung, Kecamatan Ajung, Kabupaten Jember, Provinsi Jawa Timur, Indonesia', 23, 'PKK', NULL, NULL, '60baa45290e65.jpg'),
('60bbf64813943', 'Uyun Sumarna', 'Perempuan', '081234567891', 'Jl. Jalan', 24, 'LPMD', NULL, NULL, '60bbf64813943.jpg'),
('60bbf73a215ff', 'Mansyur Suryana SPd', 'Laki-laki', '081234567891', 'Jl. Jalan', 25, 'LPMD', NULL, NULL, '60bbf73a215ff.jpg'),
('60bbf753d95e7', 'Ohan Setia Permana', 'Laki-laki', '081234567891', 'Jl. Jalan', 26, 'LPMD', NULL, NULL, '60bbf753d95e7.jpg'),
('60bbf7700d764', 'Cecep Edih', 'Laki-laki', '081234567891', 'Jl. Jalan', 27, 'LPMD', NULL, NULL, '60bbf7700d764.jpg'),
('60bbf78781b2e', 'Wardi SPd', 'Laki-laki', '081234567891', 'Jl. Jalan', 28, 'LPMD', NULL, NULL, '60bbf78781b2e.jpg'),
('60bbf796690cb', 'Kadir', 'Laki-laki', '081234567891', 'Jl. Jalan', 29, 'LPMD', NULL, NULL, '60bbf796690cb.jpg'),
('60bbf7b0abf0c', 'Teten Sutendi', 'Laki-laki', '081234567891', 'Jl. Jalan', 30, 'LPMD', NULL, NULL, '60bbf7b0abf0c.jpg'),
('60bbf7c0933c7', 'Enah', 'Laki-laki', '081234567891', 'Jl. Jalan', 31, 'LPMD', NULL, NULL, '60bbf7c0933c7.jpg'),
('60bc4da29ee34', 'Bambang', 'Laki-laki', '081234567890', 'Jl. Jalan', 32, 'Karang Taruna', NULL, NULL, '60bc4da29ee34.jpg'),
('60bc4dc39283a', 'Rudi', 'Laki-laki', '081234567890', 'Jl. Jalan', 33, 'Karang Taruna', NULL, NULL, '60bc4dc39283a.jpg'),
('60bc4e29b1fd7', 'Hartono', 'Laki-laki', '081234567890', 'Jl. Jalan', 34, 'Karang Taruna', NULL, NULL, '60bc4e29b1fd7.jpg'),
('60bc4e3dce807', 'Ali Sunan', 'Laki-laki', '081234567890', 'Jl. Jalan', 35, 'Karang Taruna', NULL, NULL, '60bc4e3dce807.jpg'),
('60bc4e4f8464a', 'Mamat', 'Laki-laki', '081234567890', 'Jl. Jalan', 36, 'Karang Taruna', NULL, 'Dusun A RT 001 RW  001', '60bc4e4f8464a.jpg'),
('60bc4e5ddc129', 'Bahul', 'Laki-laki', '081234567890', 'Jl. Jalan', 37, 'Karang Taruna', NULL, NULL, '60bc4e5ddc129.jpg'),
('60bc4e6e0aad5', 'Yasin', 'Laki-laki', '081234567890', 'Jl. Jalan', 38, 'Karang Taruna', NULL, NULL, '60bc4e6e0aad5.jpg'),
('60bc4e8dc8848', 'Senal', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pendidikan Dan Pelatihan', NULL, NULL, '60bc4e8dc8848.jpg'),
('60bc4e9cd7944', 'Hairul', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pendidikan Dan Pelatihan', NULL, NULL, '60bc4e9cd7944.jpg'),
('60bc51ce4718c', 'Fandi', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pengembangan Usaha Kegiatan Sosial', NULL, NULL, '60bc51ce4718c.jpg'),
('60bc51fc2fac7', 'Holik', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pengembangan Usaha Kegiatan Sosial', NULL, NULL, '60bc51fc2fac7.jpg'),
('60bc521236a30', 'Muhtar', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Usaha Ekonomi Produktif', NULL, NULL, '60bc521236a30.jpg'),
('60bc5222cb29b', 'Hadi', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Usaha Ekonomi Produktif', NULL, NULL, '60bc5222cb29b.jpg'),
('60bc523263ce3', 'Dedi', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pengembangan Kegiatan Kerohanian Dan Pembina', NULL, NULL, '60bc523263ce3.jpg'),
('60bc524013361', 'Heri', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pengembangan Kegiatan Kerohanian Dan Pembina', NULL, NULL, '60bc524013361.jpg'),
('60bc524f06e40', 'Hari', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Pengembangan Kegiatan Olahraga Dan Seni Buda', NULL, NULL, '60bc524f06e40.jpg'),
('60bc5264cb6b7', 'Afiff', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Pengembangan Kegiatan Olahraga Dan Seni Buda', NULL, NULL, '60bc5264cb6b7.jpg'),
('60bc52882b52a', 'Adit', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Lingkungan Hidup dan Pariwisata', NULL, NULL, '60bc52882b52a.jpg'),
('60bc5296cc0e7', 'Helmi', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Lingkungan Hidup dan Pariwisata', NULL, NULL, '60bc5296cc0e7.jpg'),
('60bc52b216028', 'Disma', 'Perempuan', '081234567890', 'Jl. Jalan', 39, 'Seksi Biro Organisasi, Pengembangan Hubungan Kerja', NULL, NULL, '60bc52b216028.jpg'),
('60bc52c75f201', 'Nanda', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Biro Organisasi, Pengembangan Hubungan Kerja', NULL, NULL, '60bc52c75f201.jpg'),
('60bc52da897ff', 'Angga', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Keamanan dan Pendamping Kegiatan', NULL, NULL, '60bc52da897ff.jpg'),
('60bc52e800c4a', 'Isbi', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Keamanan dan Pendamping Kegiatan', NULL, NULL, '60bc52e800c4a.jpg'),
('60bc52fa4a456', 'Akhyar', 'Laki-laki', '081234567890', 'Jl. Jalan', 39, 'Seksi Hubungan Masyarakat', NULL, NULL, '60bc52fa4a456.jpg'),
('60bc530a43db5', 'Wanda', 'Perempuan', '081234567890', 'Jl. Jalan', 40, 'Seksi Hubungan Masyarakat', NULL, NULL, '60bc530a43db5.jpg'),
('60bc535458d7c', 'Ajeng', 'Perempuan', '081234567890', 'Jl. Jalan', 39, 'Seksi Logistik / Perlengkapan', NULL, NULL, '60bc535458d7c.jpg'),
('60bc536452b40', 'Edwiend', 'Laki-laki', '081234567890', 'Jl. Jalan', 40, 'Seksi Logistik / Perlengkapan', NULL, NULL, '60bc536452b40.jpg'),
('60bc73cad74b5', 'Menik', 'Perempuan', '081234567890', 'Jl. Jalan', 41, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 001', '60bc73cad74b5.jpg'),
('60bc793a3260c', 'Yuni', 'Perempuan', '081234567890', 'Jl. Jalan', 42, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 002', '60bc793a3260c.jpg'),
('60bc794e38c87', 'Lily', 'Perempuan', '081234567890', 'Jl. Jalan', 43, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 003', '60bc794e38c87.jpg'),
('60bc7962b30d0', 'Puteri', 'Perempuan', '081234567890', 'Jl. Jalan', 44, 'Posyandu', 'Posyandu A', 'Dusun A RT 001 RW 002', '60bc7962b30d0.jpg'),
('60bc798208bee', 'Arum', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Kesehatan Ibu dan Anak', 'Posyandu A', 'Dusun A RT 001 RW 001', '60bc798208bee.jpg'),
('60bc799796959', 'Ratih', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Keluarga Berencana', 'Posyandu A', 'Dusun A RT 001 RW 003', '60bc799796959.jpg'),
('60bc79be9b0ac', 'Fenti', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Imunisasi', 'Posyandu A', 'Dusun A RT 001 RW 004', '60bc79be9b0ac.jpg'),
('60bc79d48275e', 'Feril', 'Perempuan', '081234567890', 'Jl. Jalan', 95, 'Kader Gizi', 'Posyandu A', 'Dusun A RT 001 RW 001', '60bc79d48275e.jpg'),
('60bc79ec2a58c', 'Ike', 'Perempuan', '081234567890', 'Jl. Jalan', 41, 'Posyandu', 'Posyandu B', 'Dusun A RT 001 RW 004', '60bc79ec2a58c.jpg'),
('60bccab7871f6', 'Bapak Supri', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 001', 'RW 001', '60bccab7871f6.jpg'),
('60bcd3a32bd0e', 'Bapak Aziz', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 002', 'RW 001', '60bcd3a32bd0e.jpg'),
('60bcd3bc2eef8', 'Bapak Fikri', 'Laki-laki', '081234567890', 'Jl. Jalan', 94, 'RW', 'RW 002', 'Dusun A', '60bcd3bc2eef8.jpg'),
('60bcd602088bb', 'Bapak Firza', 'Laki-laki', '081234567890', 'Jl. Jalan', 94, 'RW', 'RW 001', 'Dusun B', '60bcd602088bb.jpg'),
('60bcdbebd725f', 'Bapak Udin', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 001', 'RW 002', '60bcdbebd725f.jpg'),
('60bcdc1dda5f4', 'Bapak Alfi', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 003', 'RW 002', '60bcdc1dda5f4.jpg'),
('60bcdc347151f', 'Bapak Rozak', 'Laki-laki', '081234567890', 'Jl. Jalan', 93, 'RT', 'RT 002', 'RW 003', '60bcdc347151f.jpg'),
('60be274bb4431', 'Saiful', 'Laki-laki', '081234567890', 'Jl. Jalan', 52, 'Karang Werda', NULL, NULL, '60be274bb4431.jpg'),
('60be277323e4d', 'Devian', 'Laki-laki', '081234567891', 'Jl. Jalan', 53, 'Karang Werda', NULL, NULL, '60be277323e4d.jpg'),
('60be27872fbe3', 'Lihin', 'Laki-laki', '081234567890', 'Jl. Jalan', 54, 'Karang Werda', NULL, NULL, '60be27872fbe3.jpg'),
('60be27936f998', 'Alfan', 'Laki-laki', '081234567890', 'Jl. Jalan', 55, 'Karang Werda', NULL, NULL, '60be27936f998.jpg'),
('60be279e3ea24', 'Agus', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be279e3ea24.jpg'),
('60be27a8de3e5', 'Fendi', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be27a8de3e5.jpg'),
('60be27ba818ed', 'Hisbul', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be27ba818ed.jpg'),
('60be27ccd60b1', 'Riski', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be27ccd60b1.jpg'),
('60be27da02d14', 'Aswin', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be27da02d14.jpg'),
('60be27e4be286', 'Bashori', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be27e4be286.jpg'),
('60be284e1a8bd', 'Lukman', 'Laki-laki', '081234567890', 'Jl. Jalan', 56, 'Karang Werda', NULL, NULL, '60be284e1a8bd.jpg'),
('60be37f78baa1', 'Bagus', 'Laki-laki', '081234567890', 'Jl. Jalan', 57, 'RDS', NULL, NULL, '60be37f78baa1.jpg'),
('60be3832a9f60', 'Muzakki', 'Laki-laki', '081234567890', 'Jl. Jalan', 58, 'RDS', NULL, NULL, '60be3832a9f60.jpg'),
('60be38451a946', 'Irfan', 'Laki-laki', '081234567890', 'Jl. Jalan', 59, 'RDS', NULL, NULL, '60be38451a946.jpg'),
('60be384d26e30', 'Irvan', 'Laki-laki', '081234567890', 'Jl. Jalan', 60, 'RDS', NULL, NULL, '60be384d26e30.jpg'),
('60be3866b0417', 'Alwin', 'Laki-laki', '081234567890', 'Jl. Jalan', 61, 'RDS', NULL, NULL, '60be3866b0417.jpg'),
('60be409caff43', 'Indah', 'Perempuan', '081234567890', 'Jl. Jalan', 62, 'Muslimat', 'Muslimat A', NULL, '60be409caff43.jpg'),
('60be40ada838f', 'Nur Haqiqi', 'Perempuan', '081234567890', 'Jl. Jalan', 63, 'Muslimat', 'Muslimat A', NULL, '60be40ada838f.jpg'),
('60be40bbb6bac', 'Rizka', 'Perempuan', '081234567890', 'Jl. Jalan', 64, 'Muslimat', 'Muslimat A', NULL, '60be40bbb6bac.jpg'),
('60be40ca09c10', 'Ingka', 'Perempuan', '081234567890', 'Jl. Jalan', 65, 'Muslimat', 'Muslimat A', NULL, '60be40ca09c10.jpg'),
('60be40d627313', 'Mita', 'Perempuan', '081234567890', 'Jl. Jalan', 66, 'Muslimat', 'Muslimat A', NULL, '60be40d627313.jpg'),
('60be4137e0fa0', 'Linda', 'Perempuan', '081234567890', 'Jl. Jalan', 62, 'Muslimat', 'Muslimat B', NULL, '60be4137e0fa0.jpg'),
('60be4145dfb59', 'Evi', 'Perempuan', '081234567890', 'Jl. Jalan', 63, 'Muslimat', 'Muslimat B', NULL, '60be4145dfb59.jpg'),
('60be4153056de', 'Iva', 'Perempuan', '081234567890', 'Jl. Jalan', 64, 'Muslimat', 'Muslimat B', NULL, '60be4153056de.jpg'),
('60be416bd7b6b', 'Sofi', 'Perempuan', '081234567890', 'Jl. Jalan', 65, 'Muslimat', 'Muslimat B', NULL, '60be416bd7b6b.jpg'),
('60be417d255cc', 'Vicky', 'Perempuan', '081234567890', 'Jl. Jalan', 66, 'Muslimat', 'Muslimat B', NULL, '60be417d255cc.jpg'),
('60bec1925a1c0', 'Yunita', 'Perempuan', '081234567890', 'Jl. Jalan', 67, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1925a1c0.jpg'),
('60bec19d18f22', 'Ninik', 'Perempuan', '081234567890', 'Jl. Jalan', 68, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec19d18f22.jpg'),
('60bec1a785277', 'Nuril', 'Laki-laki', '081234567890', 'Jl. Jalan', 69, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1a785277.jpg'),
('60bec1b2003dc', 'Aji', 'Laki-laki', '081234567890', 'Jl. Jalan', 70, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1b2003dc.jpg'),
('60bec1c1923f1', 'Surateno', 'Laki-laki', '081234567890', 'Jl. Jalan', 71, 'Aisyiyah', 'Aisyiyah A', NULL, '60bec1c1923f1.jpg'),
('60bec1dfa5188', 'Maya', 'Perempuan', '081234567890', 'Jl. Jalan', 67, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec1dfa5188.jpg'),
('60bec1ef58156', 'Bety', 'Perempuan', '081234567890', 'Jl. Jalan', 68, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec1ef58156.jpg'),
('60bec1fbc8fe2', 'Ulva', 'Perempuan', '081234567890', 'Jl. Jalan', 69, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec1fbc8fe2.jpg'),
('60bec207a5eed', 'Nurma', 'Perempuan', '081234567890', 'Jl. Jalan', 70, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec207a5eed.jpg'),
('60bec21119701', 'Ika', 'Perempuan', '081234567890', 'Jl. Jalan', 71, 'Aisyiyah', 'Aisyiyah B', NULL, '60bec21119701.jpg'),
('60bf3d54f2972', 'Ami', 'Perempuan', '081234567890', 'Jl. Jalan', 72, 'SSB', 'Sanggar Seni A', NULL, '60bf3d54f2972.jpg'),
('60bf3d5f7adf1', 'Isa', 'Perempuan', '081234567890', 'Jl. Jalan', 73, 'SSB', 'Sanggar Seni A', NULL, '60bf3d5f7adf1.jpg'),
('60bf3d6c4ab97', 'Wati', 'Perempuan', '081234567890', 'Jl. Jalan', 74, 'SSB', 'Sanggar Seni A', NULL, '60bf3d6c4ab97.jpg'),
('60bf3d7a82810', 'Hanifa', 'Perempuan', '081234567890', 'Jl. Jalan', 75, 'SSB', 'Sanggar Seni A', NULL, '60bf3d7a82810.jpg'),
('60bf3d88587a7', 'Vira', 'Perempuan', '081234567890', 'Jl. Jalan', 76, 'SSB', 'Sanggar Seni A', NULL, '60bf3d88587a7.jpg'),
('60bf3daa9cce2', 'Safira', 'Perempuan', '081234567890', 'Jl. Jalan', 72, 'SSB', 'Sanggar Seni B', NULL, '60bf3daa9cce2.jpg'),
('60bf3db7122d3', 'Nisa', 'Perempuan', '081234567890', 'Jl. Jalan', 73, 'SSB', 'Sanggar Seni B', NULL, '60bf3db7122d3.jpg'),
('60bf3dd754670', 'Ayin', 'Perempuan', '081234567890', 'Jl. Jalan', 74, 'SSB', 'Sanggar Seni B', NULL, '60bf3dd754670.jpg'),
('60bf3de8982e1', 'Afi', 'Perempuan', '081234567890', 'Jl. Jalan', 75, 'SSB', 'Sanggar Seni B', NULL, '60bf3de8982e1.jpg'),
('60bf3dfb0e57d', 'Eva', 'Perempuan', '081234567890', 'Jl. Jalan', 76, 'SSB', 'Sanggar Seni B', NULL, '60bf3dfb0e57d.jpg'),
('60bf4124d6a8e', 'Hayu', 'Perempuan', '081234567891', 'Jl. Jalan', 77, 'LSM', 'LSM A', NULL, '60bf4124d6a8e.jpg'),
('60bf414dd7b2d', 'Vanny', 'Perempuan', '081234567890', 'Jl. Jalan', 78, 'LSM', 'LSM A', NULL, '60bf414dd7b2d.jpg'),
('60bf4162109e2', 'Rahma', 'Perempuan', '081234567890', 'Jl. Jalan', 79, 'LSM', 'LSM A', NULL, '60bf4162109e2.jpg'),
('60bf41761a8dd', 'Safitri', 'Perempuan', '081234567890', 'Jl. Jalan', 80, 'LSM', 'LSM A', NULL, '60bf41761a8dd.jpg'),
('60bf4182d6864', 'Ayunda', 'Perempuan', '081234567890', 'Jl. Jalan', 81, 'LSM', 'LSM A', NULL, '60bf4182d6864.jpg'),
('60bf419861fe6', 'Lintang', 'Laki-laki', '081234567890', 'Jl. Jalan', 77, 'LSM', 'LSM B', NULL, '60bf419861fe6.jpg'),
('60bf41a72aab9', 'Andys', 'Laki-laki', '081234567890', 'Jl. Jalan', 78, 'LSM', 'LSM B', NULL, '60bf41a72aab9.jpg'),
('60bf41b4c1331', 'Robert', 'Laki-laki', '081234567890', 'Jl. Jalan', 79, 'LSM', 'LSM B', NULL, '60bf41b4c1331.jpg'),
('60bf41c2593ff', 'Habibi', 'Laki-laki', '081234567890', 'Jl. Jalan', 80, 'LSM', 'LSM B', NULL, '60bf41c2593ff.jpg'),
('60bf41cfa1e01', 'Iqbal', 'Laki-laki', '081234567890', 'Jl. Jalan', 81, 'LSM', 'LSM B', NULL, '60bf41cfa1e01.jpg'),
('60bf4ad25e71e', 'Hesti', 'Perempuan', '081234567890', 'Jl. Jalan', 82, 'KPMD', NULL, 'Dusun A', '60bf4ad25e71e.jpg'),
('60bf4b0c93c9f', 'Ela', 'Perempuan', '081234567890', 'Jl. Jalan', 83, 'KPMD', NULL, 'Dusun A', '60bf4b0c93c9f.jpg'),
('60bf4b1d361a5', 'Yuli', 'Perempuan', '081234567890', 'Jl. Jalan', 84, 'KPMD', NULL, 'Dusun B', '60bf4b1d361a5.jpg'),
('60bf4b2d65dc2', 'Ira', 'Perempuan', '081234567890', 'Jl. Jalan', 85, 'KPMD', NULL, 'Dusun C', '60bf4b2d65dc2.jpg'),
('60bf4b451554a', 'Silvi', 'Perempuan', '081234567890', 'Jl. Jalan', 86, 'KPMD', NULL, 'Dusun B', '60bf4b451554a.jpg'),
('60bf55959db9c', 'Inema', 'Perempuan', '081234567890', 'Jl. Jalan', 87, 'KPM', NULL, 'Dusun X', '60bf55959db9c.jpg'),
('60bf55a8e6796', 'Marta', 'Perempuan', '081234567890', 'Jl. Jalan', 88, 'KPM', NULL, 'Dusun Y', '60bf55a8e6796.jpg'),
('60bf55ba85739', 'Brigia', 'Perempuan', '081234567890', 'Jl. Jalan', 89, 'KPM', NULL, 'Dusun X', '60bf55ba85739.jpg'),
('60bf55cc2f8d0', 'Dian', 'Perempuan', '081234567890', 'Jl. Jalan', 90, 'KPM', NULL, 'Dusun Y', '60bf55cc2f8d0.jpg'),
('60bf55e66904f', 'Clara', 'Perempuan', '081234567890', 'Jl. Jalan', 91, 'KPM', NULL, 'Dusun Z', '60bf55e66904f.jpg'),
('60c52d2417c28', 'Bapak Munir', 'Laki-laki', '081234567890', 'Jl. Jalan', 94, 'RW', 'RW 003', 'Dusun C', '60c52d2417c28.jpg'),
('60cb65dcb3c88', 'Holiful', 'Laki-laki', '081234567890', 'Jl. Jalan', 97, 'Kader Teknik', NULL, NULL, '60cb65dcb3c88.jpg'),
('60cb65ffdc7d0', 'Ahmad', 'Laki-laki', '081234567890', 'Jl. Jalan', 98, 'Kader Teknik', NULL, NULL, '60cb65ffdc7d0.jpg'),
('60cb661c2fae4', 'Dista', 'Laki-laki', '081234567890', 'Jl. Jalan', 99, 'Kader Teknik', NULL, NULL, '60cb661c2fae4.jpg'),
('60cb66338f628', 'Teguh', 'Laki-laki', '081234567890', 'Jl. Jalan', 100, 'Kader Teknik', NULL, NULL, '60cb66338f628.jpg'),
('60cb67834599f', 'Uri', 'Laki-laki', '081234567890', 'Jl. Jalan', 101, 'PPKBD', NULL, NULL, '60cb67834599f.jpg'),
('60cb67b8e6929', 'Prasetya', 'Laki-laki', '081234567890', 'Jl. Jalan', 102, 'PPKBD', NULL, NULL, '60cb67b8e6929.jpg'),
('60cb67d06c211', 'Jimas', 'Laki-laki', '081234567890', 'Jl. Jalan', 103, 'PPKBD', NULL, NULL, '60cb67d06c211.jpg'),
('60cb67e7d7fe7', 'Alwan', 'Laki-laki', '081234567890', 'Jl. Jalan', 104, 'PPKBD', NULL, NULL, '60cb67e7d7fe7.jpg'),
('60cb6800b3d93', 'Judin', 'Laki-laki', '081234567890', 'Jl. Jalan', 105, 'Sub A', NULL, NULL, '60cb6800b3d93.jpg'),
('60cb6814e0df6', 'Zelvi', 'Laki-laki', '081234567890', 'Jl. Jalan', 106, 'Sub A', NULL, NULL, '60cb6814e0df6.jpg'),
('60cb696cd81c8', 'Wildan', 'Laki-laki', '081234567890', 'Jl. Jalan', 107, 'Kader TB', NULL, NULL, '60cb696cd81c8.jpg'),
('60cb6987acb58', 'Pandu', 'Laki-laki', '081234567890', 'Jl. Jalan', 108, 'Kader TB', NULL, NULL, '60cb6987acb58.jpg'),
('60cb699aa282c', 'Fahrul', 'Laki-laki', '081234567890', 'Jl. Jalan', 109, 'Kader TB', NULL, NULL, '60cb699aa282c.jpg'),
('60cb69b780fd9', 'Rico', 'Laki-laki', '081234567890', 'Jl. Jalan', 110, 'Kader TB', NULL, NULL, '60cb69b780fd9.jpg'),
('60cb69dacdd4e', 'Juniar', 'Laki-laki', '081234567890', 'Jl. Jalan', 111, 'Kader TB', NULL, NULL, '60cb69dacdd4e.jpg'),
('60cb6bbf8cd18', 'Dayat', 'Laki-laki', '081234567890', 'Jl. Jalan', 105, 'Sub B', NULL, NULL, '60cb6bbf8cd18.jpg'),
('60cb6bd478f3c', 'Roby', 'Laki-laki', '081234567890', 'Jl. Jalan', 106, 'Sub B', NULL, NULL, '60cb6bd478f3c.jpg'),
('60dbb86be87ee', 'Suyoto', 'Laki-laki', '081234567890', 'Jl. Jalan', 112, 'Ponkesdes', NULL, NULL, '60dbb86be87ee.jpg'),
('60dbbaea290f7', 'Sutini', 'Perempuan', '081234567890', 'Jl. Jalan', 113, 'Ponkesdes', NULL, NULL, '60dbbaea290f7.jpg'),
('60dbc069f2f11', 'Sukarni', 'Perempuan', '081234567890', 'Jl. Jalan', 114, 'Ponkesdes', NULL, NULL, '60dbc069f2f11.jpg'),
('60dbc1c88a810', 'Suratna', 'Perempuan', '081234567890', 'Jl. Jalan', 115, 'Ponkesdes', NULL, NULL, '60dbc1c88a810.jpg'),
('60dbc1e168470', 'Heriyanto', 'Laki-laki', '081234567890', 'Jl. Jalan', 115, 'Ponkesdes', NULL, NULL, '60dbc1e168470.jpg'),
('60dbc1ff2a17b', 'Mat', 'Laki-laki', '081234567891', 'Jl. Jalan', 115, 'Ponkesdes', NULL, NULL, '60dbc1ff2a17b.jpg'),
('6136bc190c5e4', 'Lorem Ipsum', 'Laki-laki', '081233846841', 'Jl. Jalan', 117, 'Bumdes', NULL, NULL, '6136bc190c5e4.jpg'),
('6136bc2902b50', 'Lorem Ipsum', 'Perempuan', '081233846841', 'Jl. Jalan', 118, 'Bumdes', NULL, NULL, '6136bc2902b50.jpg'),
('6136bc371be7b', 'Lorem Ipsum', 'Laki-laki', '081233846841', 'Jl. Jalan', 119, 'Bumdes', NULL, NULL, '6136bc371be7b.jpg'),
('6136bc4661e61', 'Lorem Ipsum', 'Perempuan', '081233846841', 'Jl. Jalan', 120, 'Bumdes', NULL, NULL, '6136bc4661e61.jpg'),
('6136bc784356e', 'Lorem Ipsum', 'Laki-laki', '081233846841', 'Jl. Jalan', 121, 'Bumdes', NULL, NULL, '6136bc784356e.jpg'),
('6136bc87ab1e0', 'Lorem Ipsum', 'Laki-laki', '081233846841', 'Jl. Jalan', 122, 'Bumdes', NULL, NULL, '6136bc87ab1e0.jpg'),
('6182a32d21279', 'testings', 'Perempuan', '081233846842', 'Jl. Raung, Dusun Klanceng RT 003 / RW 004', 55, 'Karang Werda', NULL, NULL, '6182a32d21279.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_lemmas`
--
ALTER TABLE `tb_lemmas`
  ADD PRIMARY KEY (`id_lm`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
