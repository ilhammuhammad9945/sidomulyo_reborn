-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2021 at 05:02 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidomulyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_konten`
--

CREATE TABLE `tb_jenis_konten` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jenis_konten`
--

INSERT INTO `tb_jenis_konten` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Sejarah Desa'),
(2, 'Struktur Organisasi Desa'),
(3, 'Struktur Organisasi BPD'),
(4, 'PKK'),
(5, 'LPMD'),
(6, 'Karang Taruna'),
(7, 'Posyandu'),
(8, 'RT / RW'),
(9, 'Karang Werda'),
(10, 'Rumah Desa Sehat'),
(11, 'Muslimat'),
(12, 'Aisyiyah'),
(13, 'Sanggar Seni Budaya'),
(14, 'LSM'),
(15, 'KPMD'),
(16, 'KPM'),
(17, 'Kader Teknik'),
(18, 'Kader dan Sub PPKBD'),
(19, 'Kader TB'),
(20, 'UMKM'),
(21, 'Produk Unggulan'),
(22, 'Ponkesdes'),
(23, 'Bum Desa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_jenis_konten`
--
ALTER TABLE `tb_jenis_konten`
  ADD PRIMARY KEY (`id_jenis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_jenis_konten`
--
ALTER TABLE `tb_jenis_konten`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
